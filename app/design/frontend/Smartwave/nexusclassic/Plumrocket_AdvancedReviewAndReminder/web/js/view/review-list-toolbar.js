/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'underscore',
    'ko',
    'uiComponent',
    'jquery',
    'prReviewStorageService',
    'prReviewUtils',
    'Magento_Ui/js/modal/modal',
    'mage/translate'
], function (_, ko, Component, $, reviewStorageService, prReviewUtils, modal) {
    'use strict';

    /** @const */ var FILTER_VALUES_DELIMITER = '-';

    return Component.extend({
        defaults: {
            template: 'Plumrocket_AdvancedReviewAndReminder/review-list-toolbar'
        },

        configuration: {},

        /**
         * @type {{{value: string|integer, defaultValue: string|integer, options: Object[]}}}
         */
        filterList: {
            general: {},
            rating: {},
            search: {},
        },

        /**
         * @type {{value: string|integer, defaultValue: string|integer, options: Object[]}}
         */
        sort: {},

        /**
         * Define if render mobile version or not
         */
        isMobile: true,

        /**
         * List of active filters, may contains one group twice
         * [
         *    {group: value},
         *    {group: value}
         * ]
         */
        activeFilters: ko.observableArray([]),

        /**
         * Count of filtered reviews
         */
        showingReviewCount: ko.observable(null),
        showingReviewNumbers: ko.observable(''),
        showingReviewNumbersText: ko.observable(''),
        showingReviewNumbersDefaultText: $.mage.__('%1 of %2 reviews'),

        /** @inheritdoc */
        initialize: function () {
            this.isMobile = prReviewUtils.isMobile();
            this.reviewStorageService = reviewStorageService;
            this._super();

            var initPrams = this.reviewStorageService.getInitParamsFromUrl();
            this.updateInitValues(initPrams.filterState, initPrams.sortState);

            this.bindFilterValues();
            this.bindSortValue();
            this.onFilterStateChange(this.reviewStorageService.getFilterState()()); // Draw initial filters as active

            this.reviewStorageService.getFilterState().subscribe(this.onFilterStateChange.bind(this));
            this.reviewStorageService.getSortState().subscribe(this.onSortStateChange.bind(this));
            this.activeFilters.subscribe(this.updateFiltersView.bind(this));

            this.bindShowingReviewText();

            this.getMobileFilterForm = _.once(this.getMobileFilterForm);

            if (false === this.isTwoPageDesign) {
                this.changeHistogramAction();
            }
        },

        changeHistogramAction: function () {
            $('.prar-histogram>a').on('click', function (event) {
                event.preventDefault();
                var star = event.currentTarget.dataset.ratingStar;
                if (star) {
                    this.reviewStorageService.setFilter('rating', star);
                    document.getElementById('customer-reviews-box').scrollIntoView({behavior: 'smooth'});
                }
            }.bind(this));
        },

        /**
         * Init listeners for filters
         *
         * @return {exports}
         */
        bindFilterValues: function () {
            if (! this.isMobile) {
                this.bindFilterGroupValue('general');
                this.bindFilterGroupValue('rating');
                this.bindFilterGroupValue('search');
            } // use separate logic for mobile filter

            return this;
        },

        /**
         * @param {string} group
         */
        bindFilterGroupValue: function (group) {
            this.filterList[group].value = ko.observable(this.filterList[group].initialValue);
            this.filterList[group].value.prIgnorePokeSubscribe(function (filterValue) {
                if (filterValue === this.filterList[group].defaultValue) {
                    reviewStorageService.removeFilter(group);
                } else {
                    reviewStorageService.setFilter(group, filterValue, false);
                }
            }.bind(this));
        },

        /**
         * Init listeners for sorting
         *
         * @return {exports}
         */
        bindSortValue: function () {
            this.sort.value = ko.observable(this.sort.initialValue);
            this.sort.value.subscribe(function (sortValue) {
                if (sortValue === this.sort.defaultValue) {
                    reviewStorageService.removeSort();
                } else {
                    reviewStorageService.setSort(sortValue);
                }
            }.bind(this));

            return this;
        },

        /**
         * Clear all filters
         */
        clearAllFilters: function () {
            if (this.isMobile) {
                this.reviewStorageService.removeAllFilters();
            } else {
                _.each(this.activeFilters(), function (filter) {
                    this.filterList[filter.group].value.prPoke(this.filterList[filter.group].defaultValue);
                }.bind(this));
                this.reviewStorageService.removeAllFilters();
            }
        },

        /**
         * Remove one filter
         *
         * @param group
         * @param value
         */
        clearFilter: function (group, value) {
            if (this.isMobile) {
                reviewStorageService.removeFilter(group, value);
            } else {
                this.filterList[group].value(this.filterList[group].defaultValue);
            }
        },

        /**
         * @param state
         */
        onFilterStateChange: function (state) {
            var activeFilters = [];
            _.each(state, function (filters, group) {
                if (!_.isEmpty(filters)) {
                    _.each(filters, function (status, filterName) {
                        if (status) {
                            activeFilters.push({group: group, value: filterName});
                        }
                    }, this);
                }
            }, this);

            this.activeFilters(activeFilters);
        },

        /**
         * @param {string} sort
         */
        onSortStateChange: function (sort) {
            this.updateSortUrlParam(sort);
        },

        /**
         * @param {String} group
         * @param value
         * @return {*}
         */
        getFilterLabel: function (group, value) {
            if ('search' === group) {
                return value;
            }

            var option = _.find(this.filterList[group].options, function (filterOption) {
                return filterOption.value == value;
            });

            return option.label;
        },

        /**
         * @param {Object} filterState
         * @param {Object} sortState
         */
        updateInitValues: function (filterState, sortState) {
            _.each(filterState, function (values, group) {
                _.each(values, function (value, filterName) {
                    this.filterList[group].initialValue = filterName;
                }, this);
            }, this);

            if (sortState) {
                this.sort.initialValue = sortState;
            }

            return filterState;
        },

        /**
         * Update filters visual indicators
         *
         * @param {Object} activeFilters
         * @return {string}
         */
        updateFiltersView: function (activeFilters) {
            this.updateFiltersSelects(activeFilters);
            this.updateFilterUrlParam(activeFilters);
        },

        /**
         * Change selected select or checkboxes (only for active filters)
         *
         * @param {Object} activeFilters
         * @return {string}
         */
        updateFiltersSelects: function (activeFilters) {
            if (this.isMobile) {
                this.getMobileFilterForm().reset();
                this.updateMobileFilterValues();
            } else {
                _.each(activeFilters, function (filter) {
                    this.filterList[filter.group].value.prPoke(filter.value);
                }, this);
            }
        },

        /**
         * Add/update/remove filter param in url
         *
         * @param {Object} activeFilters
         * @return {string}
         */
        updateFilterUrlParam: function (activeFilters) {
            if (! _.isEmpty(activeFilters)) {
                var urlParamParts = {};
                _.each(activeFilters, function (filter) {
                    if (typeof urlParamParts[filter.group] === 'undefined') {
                        urlParamParts[filter.group] = [];
                    }
                    urlParamParts[filter.group].push(filter.value);
                }, this);

                _.each(urlParamParts, function (values, group) {
                    reviewStorageService.updateUrlParam(group, values.join(FILTER_VALUES_DELIMITER));
                });
            } else {
                reviewStorageService.updateUrlParam('search', '');
                reviewStorageService.updateUrlParam('general', '');
                reviewStorageService.updateUrlParam('rating', '');
            }

            return this;
        },

        /**
         * Add/update/remove sort param in url
         *
         * @param {string} urlParam
         * @return {string}
         * @return {exports}
         */
        updateSortUrlParam: function (urlParam) {
            reviewStorageService.updateUrlParam('sort', urlParam);
            return this;
        },

        /**
         * @return {exports}
         */
        bindShowingReviewText: function () {
            this.showingReviewNumbersText = ko.observable(this.showingReviewNumbersDefaultText);

            this.showingReviewCount = reviewStorageService.getFilterTotalReviewCount();
            this.showingReviewNumbers = reviewStorageService.getShowingReviewNumbers();
            this.showingReviewCount.subscribe(this.updateShowingReviewText.bind(this));
            this.showingReviewNumbers.subscribe(this.updateShowingReviewText.bind(this));

            return this;
        },

        /**
         * @return {exports}
         */
        updateShowingReviewText: function () {
            this.showingReviewNumbersText(
                prReviewUtils.applyTextToTranslate(
                    this.showingReviewNumbersDefaultText,
                    [this.showingReviewNumbers(), this.showingReviewCount()]
                )
            );

            return this;
        },

        /**
         * Retrieve form as element
         *
         * @return {HTMLElement}
         */
        getMobileFilterForm: function () {
            return document.getElementById('pr_review_mobile_filter_form');
        },

        /**
         * @return {exports}
         */
        showMobileFilter: function () {
            this.popup.modal('openModal');
            return this;
        },

        /**
         * Init modal window for mobile filter
         */
        initFilterPopup: function () {
            var self = this;
            var options = {
                type: 'popup',
                closeText: $.mage.__('Back'),
                responsive: true,
                innerScroll: true,
                modalClass: 'prar-mobile-filter-wrp',
                buttons: [{
                    text: $.mage.__('Apply Filters'),
                    class: '',
                    click: function () {
                        self.applyMobileFilters();
                        this.closeModal();
                    }
                }]
            };

            this.popup = $('#pr_review_mobile_filter');
            modal(options, this.popup);

            return this;
        },

        /**
         * Set all selected filters as active
         *
         * @return {boolean}
         */
        applyMobileFilters: function () {
            var nextState = {};

            $(this.getMobileFilterForm()).serializeArray().forEach(function (filterData) {
                if (! nextState[filterData.name]) {
                    nextState[filterData.name] = {};
                }

                nextState[filterData.name][filterData.value] = 1;
            });

            this.reviewStorageService.setFilterState(nextState);

            return false;
        },

        /**
         * Set values in filter form
         * Don't use bind value in order to working reset button
         *
         * @return {exports}
         */
        updateMobileFilterValues: function () {
            _.each(this.activeFilters(), function (filter) {
                this.setMobileFilterValue(filter.group, filter.value, true);
            }.bind(this));
            return this;
        },

        /**
         * @param {String} group
         * @param {String} value
         * @param {Boolean} status
         */
        setMobileFilterValue: function (group, value, status) {
            var elem = this.getMobileFilterForm().querySelector(
                '[name="' + group + '"][value="' + value + '"]'
            );

            if (elem) {
                elem.checked = status;
            } else {
                console.warn('Cannot find filter input');
            }

            return this;
        }
    });
});
