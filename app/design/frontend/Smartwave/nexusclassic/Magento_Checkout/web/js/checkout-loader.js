/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
 define([
  'jquery',
  'rjsResolver'
], function ($,resolver) {
  'use strict';

  /**
   * Removes provided loader element from DOM.
   *
   * @param {HTMLElement} $loader - Loader DOM element.
   */



  function hideLoader($loader) {

$("#onepage-checkout-shipping-method-additional-load").prependTo( ".column-checkout1 #opc-sidebar" );
$('.shipping-address-items .select').append('<option id="add-new-address" value="add-new-address">Add New Address</option>');


$(document).on("change",".shipping-address-items .select",function() {
if($(this).val() == 'add-new-address'){
$(".checkout-index-index .checkout-shipping-address div#checkout-step-shipping .billing-address-same-as-shipping-block").hide();  
$('.plus-new-address-button button.action.action-show-popup').trigger("click");       
  }
else{
  $(".checkout-index-index .checkout-shipping-address div#checkout-step-shipping .billing-address-same-as-shipping-block").show();  
  $("#opc-new-shipping-address").hide();
}   
  });

  if($(window).width() < 768){
  $(".checkout-second-column").addClass("addmob-checkout-second-column");
  $(".checkout-first-column .opc-block-summary .block.items-in-cart").insertAfter(".checkout-second-column div#opc-sidebar");
  }


  $(window).scroll(function() {
    if($(window).width() > 767){
   var height1 = $(window).scrollTop();
     height1 = height1 -90;
     var zeroheight1 = 0;
   if ($(this).scrollTop() >= 10) {
     if($(document).scrollTop() < $('.checkout-index-index .checkout-first-column #co-place-order-area').offset().top - 465){   
      $(".column-checkout2 div#opc-sidebar").addClass("position-fixed");
      $(".column-checkout2 div#opc-sidebar").css({
        "top": height1 + 110 + 'px',
        "position":"relative"  
      }); 
    }
    else if($(document).scrollTop() < $('.checkout-index-index .checkout-first-column #co-place-order-area').offset().top - 465){
      $(".column-checkout2 div#opc-sidebar").css({
      "top": height1 + 110 + 'px',
      "position":"relative"

    });
    }
    else if($(document).scrollTop() > $('.checkout-index-index .checkout-first-column #co-place-order-area').offset().top - 465){
      $(".column-checkout2 div#opc-sidebar").removeClass("position-fixed");
    }
    }
    else if($(this).scrollTop() < 10) {
   if($(document).scrollTop() < $('.checkout-index-index .checkout-first-column #co-place-order-area').offset().top - 465){   
    $(".column-checkout2 div#opc-sidebar").removeClass("position-fixed");
         $(".column-checkout2 div#opc-sidebar").css({
           "top": zeroheight1 + 'px',
           "position": "relative"
         });
      }
    }
   
   }
   
   });
   




$(document).ready(function(){


// if ($('#billing-address-same-as-shipping').is(':checked')) {
//     setTimeout(function(){ 
//   $(".checkout-billing-address").addClass("pointer-none");   
// }, 3000);
// }


//     $('#billing-address-same-as-shipping').change(function() {
//         if(this.checked) {
//         $(".checkout-billing-address").addClass("pointer-none");   
//         }
//         else{
//  $(".checkout-billing-address").removeClass("pointer-none");
//         }
//     });




// $(document).on('click',".table-checkout-shipping-method-select-option .row" , function(){

//       var del_val1 = $("span.col-price",this).text();
//       var del_val2 = $("span.minus",this).text();
//       var del_val3 = $(".col-method.method2",this).text();
//       var del_val4 = $("span.col-carrier",this).text();
//       var total_val = del_val1 + del_val2 + del_val3 + del_val4; 

// $(".checkout-step-shipping_method-select").text(total_val);
// $(".checkout-step-shipping_method-select").removeClass("active");
// $(".checkout-index-index form#co-shipping-method-form").hide();

//     });



$(".checkout-index-index .field.street .control .field._required label span").replaceWith("Address Line 1 ");
$(".checkout-index-index .field.street .control .field.additional label span").replaceWith("Address Line 2 ");

});


// $(".checkout-step-shipping_method-select").click(function(){
//   $(".checkout-step-shipping_method-select").toggleClass("active");
//   $("form#co-shipping-method-form").toggle();
// });


$(".column-checkout1 table.data.table.table-totals tr.grand.totals td.amount strong span.price").prependTo(".column-checkout1 div#co-place-order-area .actions-toolbar .submit-order-with-price .order-total span");


$(".checkout-second-column .table.table-totals .totals.charge .amount .price").appendTo(".checkout-second-column .opc-block-summary .block.items-in-cart .total-item-prices");


      $loader.parentNode.removeChild($loader);
  }

  /**
   * Initializes assets loading process listener.
   *
   * @param {Object} config - Optional configuration
   * @param {HTMLElement} $loader - Loader DOM element.
   */
  function init(config, $loader) {
     
      resolver(hideLoader.bind(null, $loader));
  }

  return init;
});



