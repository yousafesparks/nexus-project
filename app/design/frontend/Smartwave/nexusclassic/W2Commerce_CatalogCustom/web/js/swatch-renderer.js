define([
    'jquery',
    'underscore',
    'mage/translate',
    'jquery/ui',
    'jquery/jquery.parsequery'
], function ($, _, $t) {
    'use strict';
    return function (widget) {

        $.widget('w2commerce.catalogcustom', widget, {

            /**
             * @inheritDoc
             */
            _EventListener: function () {
                var $widget = this;
                 // custom code if whole configurable product and its child out of stock
                this.inProductList = this.element.parents('.product-item-info').length > 0 ? true : false;
                $widget._super();
                
                var dataGallery = $($widget.options.mediaGallerySelector);

                if (dataGallery.length) {
                    dataGallery.on('gallery:loaded', function () {
                        $widget._preSelectOpt();
                    });
                } else {
                    $widget._preSelectOpt();
                }
                jQuery(".swatch-option").on("hover",function(){
                    var isProductListingPage = jQuery(this).parents('.catalog-category-view .product_addtional_data').length;
                    if(isProductListingPage){
                        jQuery(this).trigger('click');
                    }else{
                        jQuery(this).attr('title', jQuery(this).attr('option-label'));
                    }
                });
				
				if($widget.options.jsonConfig.prices.oldPrice.amount > $widget.options.jsonConfig.prices.finalPrice.amount){
					var save_percent = 100-Math.round(($widget.options.jsonConfig.prices.finalPrice.amount/$widget.options.jsonConfig.prices.oldPrice.amount)*100);
					$widget.element.parents('.product-item-info').find('.product-labels .sale-label').html("<span>SALE</span>-"+save_percent+'%');
					$widget.element.parents('.product-item-info').find('.product-labels .sale-label').css({ display: "block" });
				}else{
					$widget.element.parents('.product-item-info').find('.product-labels .sale-label').css({ display: "none" });
					$widget.element.parents('.product-item-info').find('.product-labels .sale-label').html("<span>SALE</span>-0%");
				}
            },

            /**
             * Event for swatch options
             *
             * @param {Object} $this
             * @param {Object} $widget
             * @private
             */
            _OnClick: function ($this, $widget) {
                var $parent = $this.parents('.' + $widget.options.classes.attributeClass),
                    $wrapper = $this.parents('.' + $widget.options.classes.attributeOptionsWrapper),
                    $label = $parent.find('.' + $widget.options.classes.attributeSelectedOptionLabelClass),
                    attributeId = $parent.attr('attribute-id'),
                    $input = $parent.find('.' + $widget.options.classes.attributeInput),
                    checkAdditionalData = JSON.parse(this.options.jsonSwatchConfig[attributeId]['additional_data']);
                    console.log($widget.inProductList);
                    if ($widget.inProductList) {
                        $input = $widget.productForm.find(
                            '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                        );
                        var product_item_details= $this.parents('.product-item-details');
                        var product_item_info= $this.parents('.product-item-info');
                        if(product_item_details.length){
                            var productLink = product_item_details.find('.product-item-link').attr('href');
                            
                           if(productLink.indexOf('?') >= 0){
                            var res = productLink.split("?");
                            productLink =res[0];
                           }
                            productLink = productLink+"?color="+$this.attr('option-id');
                            product_item_details.find('.product-item-link').attr('href',productLink);
                            product_item_info.find('.product-item-photo a').attr('href',productLink);
                            $this.parents('.product-item-info').find('.product_addtional_data .custom-option-validation').remove();
                        }
                    }else{
                        $this.parents('.product-info-main').find('.product-options-wrapper .fieldset .custom-option-validation').remove();
                    }

                if ($this.hasClass('disabled')) {
                    return;
                }

                if (!$this.hasClass('selected')) {
                    $parent.attr('option-selected', $this.attr('option-id')).find('.selected').removeClass('selected');
                    $label.text($this.attr('option-label'));
                    $input.val($this.attr('option-id'));
                    $input.attr('attr-name', this._getAttributeCodeById(attributeId));
                    $this.addClass('selected');
					var productListing = true;
                    if($this.parents('.product-options-wrapper .fieldset').length){
						productListing = false;
					}
					if(productListing){
						if($this.parents('.product_addtional_data').find('.color-desc').length == 0){
							var colorDiv = '<span class="color-desc">';
							colorDiv += $t('<div class="color-section"><span class="color-label">Color: </span><span class="color-value">'+$this.attr('aria-label')+'</span></div>');
							colorDiv += '</span>';
							$(colorDiv).insertBefore($this.parent().parent().parent());
						}else{
							$this.parents('.product_addtional_data').find('.color-desc').remove();
							var colorDiv = '<span class="color-desc">';
							colorDiv += $t('<div class="color-section"><span class="color-label">Color: </span><span class="color-value">'+$this.attr('aria-label')+'</span></div>');
							colorDiv += '</span>';
							$(colorDiv).insertBefore($this.parent().parent().parent());
						}
					}else{
						if($this.parents('.product-options-wrapper .fieldset').find('.color-desc') == 0){
							var colorDiv = '<span class="color-desc">';
							colorDiv += $t('<div class="color-section"><span class="color-label">Color: </span><span class="color-value">'+$this.attr('aria-label')+'</span></div>');
							colorDiv += '</span>';
							$(colorDiv).insertBefore($this.parent().parent().parent());
						}else{
							$this.parents('.product-options-wrapper .fieldset').find('.color-desc').remove();
							var colorDiv = '<span class="color-desc">';
							colorDiv += $t('<div class="color-section"><span class="color-label">Color: </span><span class="color-value">'+$this.attr('aria-label')+'</span></div>');
							colorDiv += '</span>';
							$(colorDiv).insertBefore($this.parent().parent().parent());
						}
					}
                    
                   
                    
                    $widget._toggleCheckedAttributes($this, $wrapper);
                }

                $widget._Rebuild();

                // custom start
                if ($this.hasClass('selected')) {
                    var outOfStock = $widget.options.jsonConfig.outOfStock;
                    var optionId = $this.attr('option-id');
                    var products = $widget.optionsMap[attributeId][optionId]['products'];
                    if (products.length == 1) {
                        var productId = products[0];
                        if ($widget.options.jsonConfig.productNames[productId]) {
                            if ($('#product_addtocart_form').length == 0) {
                                    if($widget.options.jsonConfig.optionPrices[productId].oldPrice.amount > $widget.options.jsonConfig.optionPrices[productId].finalPrice.amount){
                                        var save_percent = 100-Math.round(($widget.options.jsonConfig.optionPrices[productId].finalPrice.amount/$widget.options.jsonConfig.optionPrices[productId].oldPrice.amount)*100);
                                        $this.parents('.product-item-info').find('.product-labels .sale-label').html("<span>SALE</span>-"+save_percent+'%');
                                        $this.parents('.product-item-info').find('.product-labels .sale-label').css({ display: "block" });
                                    }else{
										$this.parents('.product-item-info').find('.product-labels .sale-label').css({ display: "none" });
                                        $this.parents('.product-item-info').find('.product-labels .sale-label').html("<span>SALE</span>-0%");
                                        
                                    }
                                $widget.element.closest('.product-item-details').find('.product-item-link').text($widget.options.jsonConfig.productNames[productId]);
                            } else {
                                var childSku = $widget.options.jsonConfig.productSkus[productId];
                                var newString = childSku.split(':');
                                var newSku = ''; 
                                if(newString.length > 1)
                                    newSku = newString[newString.length - 1];
                                else
                                    newSku = childSku;

                                $widget.element.closest('.product-info-main').find('h1.page-title span').text($widget.options.jsonConfig.productNames[productId]);
								$widget.element.closest('.product-info-main').find('.product-line-sku-value').text(newSku);
                            }
                        }
						 
                        if (outOfStock && outOfStock.length) {
                            var duplicate = _.intersection(outOfStock, products);
                            var wrapEl = $widget.element.closest('.product-item-details');
							
                            if ($('#product_addtocart_form').length) {
                                //wrapEl = $widget.element.closest('#product-options-wrapper');
								wrapEl = $widget.element.closest('.product-info-main');
                            }
                            if (duplicate.length && !wrapEl.find('.show-login-form').length /*&& !wrapEl.find('.mess-out-of-stock').length*/) {
                               // wrapEl.find('.product-item-inner').hide();
                                wrapEl.find('.product-options-bottom').hide();
                                var out_product_id = duplicate[0];
                                var mess = '<span class="mess-out-of-stock gfg">';
                                mess += $t('<div class="notification-section product actions product-item-actions"><button type="button" out-of-stock-product-id="'+out_product_id+'" class="action notify-product primary"><span>Notify me</span></button></div>');
                                mess += '</span>';
								wrapEl.find('.mess-out-of-stock').remove();
                                if ($('#product_addtocart_form').length == 0) {
									
                                    $(mess).insertAfter($('.product-info-price'));
                                } else {
                                     $(mess).insertAfter($('.product-info-price'));
                                }
                            }else{
                                if(!wrapEl.find('.show-login-form').length){
                                    wrapEl.find('.product-item-inner').show();
                                    wrapEl.find('.product-options-bottom').show();
                                }
                                jQuery('.mess-out-of-stock').remove();
                            }
                        }
                    }
                }
                // custom end

                if ($widget.element.parents($widget.options.selectorProduct)
                        .find(this.options.selectorProductPrice).is(':data(mage-priceBox)')
                ) {
                    $widget._UpdatePrice();
                }

                $(document).trigger('updateMsrpPriceBlock',
                    [
                        _.findKey($widget.options.jsonConfig.index, $widget.options.jsonConfig.defaultValues),
                        $widget.options.jsonConfig.optionPrices
                    ]);

                if (parseInt(checkAdditionalData['update_product_preview_image'], 10) === 1) {
                    $widget._loadMedia();
                }

                $input.trigger('change');
            },

            /**
             * Pre-select first option
             *
             * @private
             */
            _preSelectOpt: function () {
                var $widget = this;

                $widget.element.find('.swatch-option').each(function () {
                    if ($(this).hasClass('selected')) {
                        return false;
                    }
                    var attributeId = parseInt($(this).closest('.swatch-attribute').attr('attribute-id'));
					//console.log($widget.optionsMap);
					
                    var outOfStock = $widget.options.jsonConfig.outOfStock;
                    var optionId = $(this).attr('option-id');
                    if (outOfStock && outOfStock.length) {
                        var products = $widget.optionsMap[attributeId][optionId]['products'];
                        var duplicate = _.intersection(outOfStock, products);
                        if (duplicate.length) {
                            return;
                        }
                    }


                     $(this).trigger('click');
					 //$(this).parents('.product-item-info').find('.product-image-photo').css({ display: "block" });
                    return false;
                });

                $('.product.media .fotorama').on('fotorama:load', function (e, fotorama, extra) {
                    $('.product.media .fotorama__stage .fotorama__active').zoom({
                        touch:false
                    });
                    setTimeout(function(){
                        if ($('.product.media:hover').length) {
                            $('.product.media .fotorama__stage .fotorama__active').trigger('mouseenter.zoom');
                        }
                    }, 0);
                });

                $('.product.media .fotorama').on('fotorama:showend', function (e, fotorama, extra) {
                    $('.product.media .fotorama__stage .fotorama__active').zoom({
                        touch:false
                    });
                    $('.product.media .fotorama__stage .fotorama__active').mouseover(function () {
                        $('.product.media .fotorama__stage .fotorama__active').trigger('mouseenter.zoom');
                    });
                    $('.product.media .fotorama__stage .fotorama__active').mouseout(function () {
                        $('.product.media .fotorama__stage .fotorama__active').trigger('mouseleave.zoom');
                    });
                });
            }
        });

        return $.w2commerce.catalogcustom;
    }
});