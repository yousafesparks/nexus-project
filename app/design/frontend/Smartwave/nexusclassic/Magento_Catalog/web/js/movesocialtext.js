
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 
 require([ "jquery" ], function($){
   
   $(document).ready(function($){

        window.onresize = move_sidebar;
        window.onload = move_sidebar;
        function move_sidebar(event) {
            
            if(window.outerWidth < 999) { 
                
                  jQuery(".addthis_toolbox").insertBefore(".product.info.detailed");
                  jQuery(".product-reviews-summary").insertAfter(".breadcrumbs");
                  jQuery(".product-line-sku-container").insertAfter(".breadcrumbs");
                  jQuery(".page-title-wrapper").insertAfter(".breadcrumbs");

                }

        }
   });

});

}




