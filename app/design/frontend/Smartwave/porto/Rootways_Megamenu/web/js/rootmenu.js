function rootFunction() {
    document.getElementById("megamenu_contact_form").reset();
}
require(['jquery','Rootways_Megamenu/js/masonry.pkgd.min', 'matchMedia'],function($,Masonry, mediaCheck){
    $(document).ready(function() {

        // new code by Grish sir

    jQuery(".rootmenu .tabmenu .vertical-menu .tabimgwpr .tabbing_lev4 li ul").each(function(){
        if(!jQuery(this).parent().find(".backButton").length){
            var lbl = jQuery(this).parent().find(" > a").text();
            jQuery(this).prepend("<a href='#' class='backButton'>"+lbl+"</a>");
        }
    });
    jQuery("body").on("click", ".backButton", function(e){
        e.preventDefault();
        jQuery("body").removeClass("lastLevelVisible");
        jQuery(this).parent().hide();
       jQuery(".subMenuVisible").hide();
		jQuery('ul.tabbing_lev4 > li > a').removeClass('removeborderbottom');
       /*jQuery("body").removeClass("lastLevelVisible");
        jQuery(this).parent().removeClass("visible");
        jQuery(".subMenuVisible").removeClass("subMenuVisible");*/
        return false;
    });
    
    jQuery("body").on("click", ".rootmenu .tabmenu .vertical-menu .tabimgwpr .tabbing_lev4 li a", function(e){
        if(jQuery(this).parent().find(".subarrowmnu").length){
			jQuery(this).parent().find("ul").addClass('final-dest');
            jQuery("body").addClass("lastLevelVisible");
            jQuery(this).parent().find(".subarrowmnu").show();
            jQuery(this).parents(".tabbing_lev4").show();
            /*jQuery("body").addClass("lastLevelVisible");
            jQuery(this).parent().find(".subarrowmnu").addClass("visible");
            jQuery(this).parents(".tabbing_lev4").addClass("subMenuVisible");*/
        }
    });

//End
		
        jQuery('.tabmenu .tabbing_lev4').on( 'mousewheel DOMMouseScroll', function (e) { 
            var e0 = e.originalEvent;
            var delta = e0.wheelDelta || -e0.detail;
          
            this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
            e.preventDefault();  
        });
        jQuery('#rw-menutop .megamenu').on( 'mousewheel DOMMouseScroll', function (e) { 
            var e0 = e.originalEvent;
            var delta = e0.wheelDelta || -e0.detail;
          
            this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
            e.preventDefault();  
        });
        jQuery('.sidebar-main').on( 'mousewheel scroll DOMMouseScroll', function (e) { 
            if(jQuery(this).find('#layered-filter-block').length > 0 && jQuery('#layered-filter-block').height() >= 650){
            $('body').css('overflow','hidden');
            }
        });
        jQuery('.column.main').on('hover scroll mousewheel DOMMouseScroll', function (e) {
            $('body').removeAttr('style');
        });

        if ($('#maincontent .sidebar.sidebar-main').length) {
            $('#maincontent').on('mouseleave', '.sidebar.sidebar-main', function () {
                if ($('body').css('overflow') == 'hidden') {
                    $('body').css('overflow', '');
                }
            });
        }
        var name = jQuery('.header-welcome-customer-name span').text();
        if (name.length > 20) {
            var shortname = name.substring(0, 20) + "..";
            jQuery('.header-welcome-customer-name span').replaceWith(shortname);
        }
        window.onscroll = function() {myFunction()};
        var navbar = document.getElementsByClassName("nav-sections")[0];
        var sidebar = document.getElementsByClassName("sidebar")[0];
        var page_footer = document.getElementsByClassName("page-footer")[0];
        var sticky = '';
        var sidebar_sticky = '';
        if (navbar != null) {
            sticky = navbar.offsetTop;   
        }
        if (sidebar != null) {
            sidebar_sticky = sidebar.offsetTop;   
        }
        
        function myFunction() {
            if (navbar != null) {
                if($(window).width()>=1000){
                    if (window.pageYOffset >= sticky) {
                        navbar.classList.add("sticky");
                    } else {
                        navbar.classList.remove("sticky");
                    }
                }
            }
            if(jQuery('body').hasClass('page-products')){
            if (sidebar != null) {
                if(window.pageYOffset <= 45) {
                    jQuery('.sidebar').removeAttr("style");
                    jQuery('.breadcrumbs').removeAttr("style");
                    sidebar.classList.remove("sticky_fixed");
                    sidebar.classList.remove("sticky");
                    jQuery('.breadcrumbs').removeClass("sticky_fixed");
                }
                else if(window.pageYOffset >= (page_footer.offsetTop-850)){
                    var topOffset = window.pageYOffset;
                    if(topOffset > 50)
                    topOffset = topOffset -120;
                    jQuery('.breadcrumbs').addClass("sticky_fixed");
                    sidebar.classList.remove("sticky_fixed");
                    sidebar.classList.add("sticky");
                     jQuery('.sidebar').attr('style', 'top:'+(page_footer.offsetTop-960)+'px');
                }
                else if (window.pageYOffset >= 50 && window.pageYOffset <= (page_footer.offsetTop-850)) {
                 
                    sidebar.classList.add("sticky_fixed");
                    jQuery('.breadcrumbs').addClass("sticky_fixed");
                    sidebar.classList.remove("sticky");
                    jQuery('.sidebar').removeAttr('style');
                }

                if(window.pageYOffset >= (page_footer.offsetTop-850)) {
                    updateMaxHeight(true);
                } else {
                    updateMaxHeight();
                }
              }  
            }
        }
        if (('ontouchstart' in window)) {
            $('.rootmenu-list li a').click(function(e) {
                if(window.innerWidth >= 999) {
                    if (($(this).closest("li").children(".halfmenu").length) ||
                        ($(this).closest("li").children(".megamenu").length) ||
                        ($(this).closest("li").children("ul").length) ||
                        ($(this).closest("li").children(".verticalopen").length) ||
                        ($(this).closest("li").children(".verticalopen02").length)
                      )
                    {
                        var clicks = $(this).data('clicks');
                        if (clicks) {
                        } else {
                            e.preventDefault();
                        }
                        $(this).data("clicks", !clicks);
                    }
                }
            });
        }
        //if (('ontouchstart' in window)) {
            $('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 .arrow').click(function(e) {
                var w_height = parseInt(jQuery( window ).width());
                 if(window.w_height >= 999) {
                    if(jQuery(this).hasClass('down')){
                        jQuery(this).removeClass('down');
                        jQuery(this).addClass('up');
                    }else{
                        jQuery(this).removeClass('up');
                        jQuery(this).addClass('down');
                    }
                    jQuery(this).next().slideToggle('fast');
                }
            });
            jQuery(document).on("click", '#megamenu-brands-slider-demo-22 .owl-item .item',function(event){
                if(jQuery(this).data('target'))
                    window.location.href = jQuery(this).data('target');
            });
            jQuery(document).on("click", '.am-filter-items-attr_custitem_f3_item_nested_brand li',function(event){
                var id = jQuery(this).data('id');
                 if(jQuery(this).find('.nested-am-collapse-icon').hasClass('_active')){
                    jQuery(this).find('.nested-am-collapse-icon').removeClass('_active');
                 }else{
                    jQuery(this).find('.nested-am-collapse-icon').addClass('_active');
                 }
                if(jQuery('.am-filter-items-attr_custitem_f3_item_nested_brand').find("[data-parent-id='" + id + "']").hasClass('in')){
                    jQuery('.am-filter-items-attr_custitem_f3_item_nested_brand').find("[data-parent-id='" + id + "']").not(".nested_cat_1").slideUp('fast');
                    jQuery('.am-filter-items-attr_custitem_f3_item_nested_brand').find("[data-parent-id='" + id + "']").not(".nested_cat_1").removeClass('in');
                }else{
                    jQuery('.am-filter-items-attr_custitem_f3_item_nested_brand').find("[data-parent-id='" + id + "']").not(".nested_cat_1").slideDown('fast');
                    jQuery('.am-filter-items-attr_custitem_f3_item_nested_brand').find("[data-parent-id='" + id + "']").not(".nested_cat_1").addClass('in');
                }
            });

            
            $('html,body').click(function (event) {
                var clickover = $(event.target);
                var _opened = $(".toogle-header").hasClass("active");
                if (_opened === true && !clickover.hasClass("toogle-header") && !clickover.hasClass('swatch-option')) {
                    $(".toogle-header").removeClass('active');
                    // $(".toogle-header").next('ul.header-menu-level-container').hide();
                }
            });
           /* $('.toogle-header').each(function () {
                var currentElement = jQuery(this);
                currentElement.click(function(){
                
                $('.toogle-header').next('ul').hide();
                if(jQuery(this).hasClass('active')){
                    jQuery(this).removeClass('active');
                    $(this).next('ul').hide();
                    $('.toogle-header').removeClass('active');
                }else{
                    $('.toogle-header').removeClass('active');
                    jQuery(this).addClass('active');
                    $(this).next('ul').show();
                }
            });*/
            $('.header.links li').mouseenter(function(){
                $(this).find('a').addClass('active');
                $(this).find('ul.header-menu-level-container').fadeIn(100);
            });
            $('.header.links li').mouseleave(function(){
                $(this).find('a').removeClass('active');
                $(this).find('ul.header-menu-level-container').fadeOut(100);
            });
       // }<span class="cat-arrow"></span>
        /* jQuery('.rootmenu-list li').has('.rootmenu-submenu, .rootmenu-submenu-sub, .rootmenu-submenu-sub-sub').prepend('<span class="cat-arrow"></span>');
        jQuery('#.rootmenu-list li a').has('.megamenu').insertAfter('<span class="cat-arrow"></span>'); */
        //jQuery('.rootmenu-list li').has('.halfmenu').prepend('<span class="cat-arrow"></span>');
        //jQuery('.tabmenu02 li a').has('.verticalopen02').prepend('<span class="cat-arrow"></span>');
        /* jQuery('.rootmenu-list li').has('.rootmenu-submenu, .rootmenu-submenu-sub, .rootmenu-submenu-sub-sub').prepend('<span class="rootmenu-click"><i class="rootmenu-arrow"></i></span>');
        jQuery('.rootmenu-list li').has('.megamenu').prepend('<span class="rootmenu-click"><i class="rootmenu-arrow"></i></span>');
        jQuery('.rootmenu-list li').has('.halfmenu').prepend('<span class="rootmenu-click"><i class="rootmenu-arrow"></i></span>');
        jQuery('.tabmenu02 li').has('.verticalopen02').prepend('<span class="rootmenu-click"><i class="rootmenu-arrow"></i></span>'); */
        
        /* For All Categories mega menu */
        jQuery('.verticalmenu02 .vertical-list li').has('.varticalmenu_main').prepend('<span class="desktop-vertical-click"><i class="verticalmenu-arrow fa fa-angle-right" aria-hidden="true"></i></span><span class="vertical-click"><i class="verticalmenu-arrow fa fa-angle-down" aria-hidden="true"></i></span>');
        jQuery('.vertical-click').click(function(){
            jQuery(this).siblings('.varticalmenu_main').slideToggle('slow');
            jQuery(this).siblings('.level3-popup').slideToggle('slow');
            jQuery(this).children('.verticalmenu-arrow').toggleClass('verticalmenu-rotate');
            loadMasonry();
            setTimeout(function() {
                loadMasonry();
            }, 500);
        });
       
       /*  jQuery('.rootmenu-click').click(function() {
            jQuery(this).siblings('.rootmenu-submenu').slideToggle('slow');
            jQuery(this).children('.rootmenu-arrow').toggleClass('rootmenu-rotate');
            jQuery(this).siblings('.rootmenu-submenu-sub').slideToggle('slow');
            jQuery(this).siblings('.rootmenu-submenu-sub-sub').slideToggle('slow');
            jQuery(this).siblings('.megamenu').slideToggle('slow');     
            jQuery(this).siblings('.halfmenu').slideToggle('slow');
            jQuery(this).siblings('.verticalopen02').slideToggle('slow'); 
            jQuery(this).siblings('.verticalmenu02').slideToggle('slow');  // All Categories menu.
            jQuery(this).siblings('.colultabtwo').slideToggle('slow');
            jQuery(this).siblings('.colultabthree').slideToggle('slow');
            jQuery(this).siblings('.resultdiv').slideToggle('slow');
            loadMasonry();
            jQuery(this).siblings('.level4-listing').slideToggle('slow');
            setTimeout(function() {
                loadMasonry();
            }, 500);
        }); */
        jQuery('.level2-popup .rootmenu-click').click(function(){
            jQuery(this).closest('li').find('.level3-popup').slideToggle('slow');
        });
        jQuery('.level3-popup .rootmenu-click').click(function(){
            jQuery(this).closest('li').find('.level4-popup').slideToggle('slow');
        });
        
    
        jQuery(".nav-toggle").click(function() {
            if (jQuery("html").hasClass("nav-open")) { 
                jQuery("html").removeClass('nav-before-open nav-open');
            } else {
                jQuery("html").addClass('nav-before-open nav-open');  
            } 
        });

       jQuery(".minicart-wrapper a").click(function() { 
            if (jQuery("body").hasClass("mincart-open")) {  
                jQuery("body").removeClass('mincart-open');
            } else {
                jQuery("body").addClass('mincart-open');    
            } 
        }); 
        
        setmenuheight();
        setmenuheight_horizontal();
        setmenuheight_multitab('1');
        setAllCategoryMenuHeight();
        jQuery(window).bind("load resize", function() { 
            var w_height = parseInt(jQuery( window ).width());
            if(w_height <= 1000) {
                //jQuery(".store-info").hide();
                jQuery(".grid-item-3 .cat-arrow").show();
                jQuery(".tabimgwpr .cat-arrow").show();
                jQuery('.verticalopen').find('.model-suggestions').hide();
                jQuery(".tabmenu").css('height','100%');
                jQuery(".tabmenu02").css('height','100%');
                jQuery(".verticalopen").css('height','100%');
                jQuery('#rw-menutop .category-item > a').click(function(event){
                    if(!jQuery(this).hasClass('feature-brand'))
                    event.preventDefault();
                    //jQuery('.mob-Menu >a ').after(("<a class='nav-sections-item-switch mobile-back' href='#'>Back to "+jQuery(this).text()+"</a>"));
                   // jQuery(this).next().animate({ width: '100%' });
                    jQuery(this).next().show();
                    jQuery('.vertical-menu02 li a span').hide();//hide images
                    jQuery('.rootmegamenu_block .vertical-menu02 li a span').show();
                    jQuery('.vertical-menu02 li a span.cat-arrow').show();//hide images
                });
                
               /* jQuery('.vertical-menu .level1-cat a').click(function(event){
                    if(typeof(jQuery(this).attr('class')) != "undefined"){
						event.preventDefault();
						//jQuery(this).next().animate({ width: '100%' },100);
						jQuery(this).next().css('height','auto !important');
						//jQuery(this).next().stop(true,true).show("slide", { direction: "right" }, 400);
                       // jQuery('ul#rw-menutop li.active-parts>a').next().css('display','none');
                         jQuery(this).next().show();
                        //jQuery(this).next().show("slide", { direction: "right" }, 100);
                        
                    }
                    jQuery('.parts_back_main').hide();
                });*/
                jQuery('.mobile-sub .category-item .rootmenu-click').click(function(event){
                    event.preventDefault();
                    jQuery(this).parent().find(".megamenu .submenu-boxes-subsection-title").hide();
                    //jQuery(this).next().next().animate({ width: "toggle"},1);
                    //jQuery(this).next().next().animate({ width: '100%' });
                    jQuery(this).next().next().show();
                    jQuery('.vertical-menu02 li a span').hide();//hide images
                    //jQuery('ul#rw-menutop li.active-parts>a').next().css('display','none');
                    jQuery('..rootmegamenu_block .vertical-menu02  li a span').show();
                    jQuery('.vertical-menu02 li a span.cat-arrow').show();//hide images
                });
                
                /*jQuery(document).on("click", '.tabbing_lev4 .back-button',function(event){
                    jQuery(this).parent().animate({ width: "100%"},1);
                    jQuery(this).parent().hide();
                });
                jQuery(document).on("click", '.vertical-menu02 > .back-button',function(event){
                    jQuery(this).parent().parent().parent().animate({ width: "100%"},1);
                    jQuery(this).parent().parent().parent().hide();
                });
                jQuery(document).on("click", '.content-only .vertical-menu02 > .back-button',function(event){
                    jQuery(this).parent().parent().parent().parent().parent().animate({ width: "100%"},1);
                    jQuery(this).parent().parent().parent().parent().parent().hide();
                });
                jQuery('.level1-cat.back-button').click(function(event){
                    jQuery(this).parent().parent().parent().animate({ width: "100%"},1);
                    jQuery(this).parent().parent().parent().hide();
                    
                });
                jQuery('.level3-cat.back-button').click(function(event){
                    jQuery(this).parent().parent().parent().animate({ width: "100%"},1);
                    jQuery(this).parent().parent().parent().hide();
                    
                });*/
                jQuery(".quick-add-wrapper").hide();
                //jQuery(".main-panel-inner .header.links").attr('style', 'display: none !important');
                jQuery(".header.panel > .header.links .authorization-link").show();
                } else {
                    setmenuheight();
                    jQuery('.verticalopen').find('.back-button').remove();
                    jQuery('.verticalopen .level3-cat').find('.cat-arrow').remove();
                    jQuery('.verticalopen').find('.model-suggestions').show();
                    //jQuery(".rootmenu > .rootmenu-list .tabmenu .cat-arrow").hide();
                    jQuery(".grid-item-3 .cat-arrow").hide();
                    jQuery(".grid-item-4 .cat-arrow").hide();
                    jQuery(".grid-item-5 .cat-arrow").hide();
                }
                // if(w_height >= 768 &&  w_height < 800) {
    //                 //jQuery(".store-info").hide();
    //                 jQuery(".quick-add-wrapper").hide();
    //                 //jQuery(".main-panel-inner .header.links").attr('style', 'display: none !important');
    //             jQuery(".header.panel > .header.links .authorization-link").show();
    //                 jQuery(".rootmenu > .rootmenu-list .tabmenu .cat-arrow").hide();
                //  var el = $('.rootmenu .megamenu .root-sub-col-8');
                //  el.addClass('root-sub-col-12');
                //  el.removeClass('root-sub-col-8');
                //  jQuery('.megamenu').find('.rootmegamenu_block').hide();
                //  jQuery('.vertical-menu02 li a span').hide();
                //  jQuery('.vertical-menu02').find('.back-button').remove();
                // }
                if(w_height > 1024){
                    //jQuery(".store-info").show();
                    //jQuery(".main-panel-inner .header.links").attr('style', 'display: inline-block !important');
                    jQuery(".header.panel > .header.links .authorization-link").show();
                    jQuery(".quick-add-wrapper").css('display','inline-block');
                    var el = $('.rootmenu .megamenu .root-sub-col-12');
                    if(!el.hasClass('Devices')){
                        el.addClass('root-sub-col-8');
                        el.removeClass('root-sub-col-12');
                    }
                    
                    jQuery('.megamenu').find('.rootmegamenu_block').hide();
                    jQuery('.megamenu').find('.rootmegamenu_block').show();
                    jQuery(this).parent().find(".megamenu .submenu-boxes-subsection-title").hide();
                    jQuery('.vertical-menu02 li a span').show();
                    jQuery('.vertical-menu02').find('.back-button').remove();
                }
              
               
            setAllCategoryMenuHeight();
            //loadMasonryOnResize();
        });
        
        jQuery(".rootmenu-list > li").hover(
            function() {
                jQuery( this ).addClass("hover");
                // $(".toogle-header").next('ul.header-menu-level-container').hide();
                $(".toogle-header").removeClass('active');
                setmenuheight();
                loadMasonry();
               
            }, function() {
                jQuery(this).removeClass("hover");
            }
        );
      
        
        jQuery(".vertical-menu > li").hover(
            function() {
                loadMasonry();
            }, function() {
            }
        );
        
        jQuery(".all-category-wrapper .verticalmenu02 .vertical-list > li, .all-category-wrapper").hover(
            function() {
                loadMasonry();
                setAllCategoryMenuHeight();
                jQuery(this).addClass("hover_allcategories");
            }, function() {
                jQuery(this).removeClass("hover_allcategories");
            }
        );
        
        jQuery(".vertical-menu02 > li > a").hover(
            function() {
                setmenuheight_horizontal();
            }, function() {
            }
        );
        var event = ('ontouchstart' in window) ? 'click' : 'mouseenter mouseleave';
        jQuery('.vertical-menu02 > li').on(event, function () {
            var w_height = parseInt(jQuery( window ).width());
            if(w_height > 999) {
                jQuery('.hover .vertical-menu02 > li').removeClass('main_openactive02');
                jQuery(this).addClass('main_openactive02');
            }
        });
       jQuery('.vertical-menu > li').on(event, function () {
            var w_height = parseInt(jQuery( window ).width());
            if(w_height > 999) {
                jQuery('.hover .vertical-menu > li').removeClass('main_openactive01');
                jQuery(this).addClass('main_openactive01');
                setmenuheight();
            }
        });

        jQuery('.vertical-menu > li').on('mouseenter', function () {
            var w_height = parseInt(jQuery( window ).width());
            if(w_height > 999) {
                setTimeout(function(){
                    jQuery('.hover .vertical-menu > li:not(.main_openactive01)').attr('style', 'pointer-events: none');
                }, 100);
            }
        });
        jQuery('.vertical-menu > li').on('mouseleave', function () {
            var w_height = parseInt(jQuery( window ).width());
            if(w_height > 999) {
                setTimeout(function(){
                    jQuery('.hover .vertical-menu > li').removeAttr('style');
                }, 100);
            }
        });

        jQuery('#rw-menutop > li').on('mouseenter', function () {
            var w_height = parseInt(jQuery( window ).width());
            if(w_height > 999) {
                setTimeout(function(){
                    jQuery('.hover .vertical-menu > li').removeAttr('style');
                }, 100);
            }
        });
        
        jQuery(".colultabone > li > a").hover(
            function() {
                setmenuheight_multitab('1');
            }, function() {

            }
        );
        jQuery('.colultabone > li').on(event, function () {
            jQuery('.colultabone > li').removeClass('main_openactive03');
            jQuery('.colultabtwo > li').removeClass('main_openactive03_sub1');
            jQuery('.colultabthree > li').removeClass('main_openactive03_sub2');
            jQuery(this).addClass('main_openactive03');
            setmenuheight_multitab('1');
        });
        jQuery('.colultabtwo > li').on(event, function () {
            jQuery('.colultabtwo > li').removeClass('main_openactive03_sub1');
            jQuery('.colultabthree > li').removeClass('main_openactive03_sub2');
            jQuery(this).addClass('main_openactive03_sub1');
            setmenuheight_multitab('2');
        });
         jQuery('.colultabthree > li').on(event, function () {
            jQuery('.colultabthree > li').removeClass('main_openactive03_sub2');
            jQuery(this).addClass('main_openactive03_sub2');
             setmenuheight_multitab('3')
        });

         /* code*/
        jQuery('.vertical-menu > li > a').on('click', function () {
			removeFourthLevelLiClass();
            // setTimeout(function(){
            //     jQuery('.hover .vertical-menu > li:not(.main_openactive01)').attr('style', 'display: none');
            // }, 100);
            //jQuery(this).text(jQuery(this).text().replace('Parts',''));
            jQuery(".parts_back").remove();
            jQuery(this).parent().addClass('active-parts');
            jQuery(this).parent().addClass('main_openactive01');
            //jQuery(this).text(jQuery(this).text()+" Parts");
			jQuery(this).find('.cat-arrow').before( "<span class='parts' style='float: none'> Parts</span>" );
            jQuery(".active-hover").addClass("hide-parts");
            var lbl = jQuery(this).text().replace("Parts", "");
            jQuery(".parts_back_main").after("<div class='parts_back'>"+lbl+"</div>");
            jQuery(".parts_back").attr('style','margin:10px');
            jQuery("#hotlinemobile").hide();
            // jQuery(".tabimgwpr ul.tabbing_lev4").attr('style','display:block');
            jQuery(".tabimgwpr").attr('style','display:block');
//             setTimeout(function(){
                jQuery(".vertical-menu > li.active-parts").attr('style','display:block');
                jQuery(".vertical-menu > li").attr('style','display:none');
          //  }, 100);
		  // second time click 
			//jQuery(this).next().animate({ width: '100%' },100);
			jQuery(this).next().css('height','auto !important');
			//jQuery(this).next().stop(true,true).show("slide", { direction: "right" }, 400);
		   // jQuery('ul#rw-menutop li.active-parts>a').next().css('display','none');
			 jQuery(this).next().show();
			//jQuery(this).next().show("slide", { direction: "right" }, 100);
			jQuery('.parts_back_main').hide();
			});
		
        jQuery(".tabimgwpr > a").click(function(event){
			var ele = jQuery(this);
			var w_width = parseInt(jQuery( window ).width());
			console.log(ele.find('.tabimtag').next().length);
			if(ele.next().length > 0 && w_width < 1000)
				event.preventDefault();
			removeFourthLevelLiClass();
            jQuery(".tabimgwpr ").attr('style', 'display: none');
            jQuery('.tabimgwpr').removeClass("active-fourth");
            jQuery('.active-parts').addClass('active-last')
            ele.parent().attr('style', 'display: block');
            ele.parent().addClass('active-fourth');
            ele.parent().children('.tabimgtext').addClass("last-part")
             jQuery(".last_level").remove();
             var label = ele.parent().find(" > .tabimtag > .tabimgtext").text();
            jQuery(".parts_back").after("<div class='last_level'>"+label+"</div>");
            jQuery(".last_level").attr('style','margin:10px');
            jQuery('.parts_back').hide();
			// second time click line no :309
			//jQuery(this).next().animate({ width: '100%' },100);
			ele.next().css('height','auto !important');
			//jQuery(this).next().stop(true,true).show("slide", { direction: "right" }, 400);
		   // jQuery('ul#rw-menutop li.active-parts>a').next().css('display','none');
			 ele.next().show();
			//jQuery(this).next().show("slide", { direction: "right" }, 100);
        });

        function LastLevelBackFunction() {
			removeFourthLevelLiClass();
            console.log("last label");
            jQuery('.active-parts').removeClass('active-last')
            jQuery("last-part").text(jQuery(".last-part").text().replace('Parts',''));
			setTimeout(function(){
				jQuery(".tabimgwpr ul.tabbing_lev4").hide();
			}, 100);
            setTimeout(function(){
            jQuery(".last_level").remove();
            jQuery(".end_level").remove();               
               
            }, 400);
            jQuery(".tabimgwpr > ul").hide();
            jQuery('.tabimgwpr.active-fourth').attr('style', 'display: none');
             jQuery('.tabimgwpr').removeClass("active-fourth");
			//jQuery(".tabimgwpr").addClass('selected').show("slide", { direction: "left" }, 100);
			jQuery(".tabimgwpr").addClass('selected').show();
            jQuery('.tabimgtext').removeClass("last-part");

            jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
            setTimeout(function(){
            
               //jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').attr('style', 'display: block');
               jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
                jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").removeClass('up');
                jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").addClass('down');
            }, 100);

            //jQuery('.tabimgwpr.active-fourth').hide();
            /*if (jQuery('.tabimgwpr.active-fourth').parent().children().length == 1) {
                jQuery('.tabimgwpr').show("slide", { direction: "left" }, 400);
            } else {
               jQuery('.tabimgwpr.active-fourth').show("slide", { direction: "left" }, 4000);
            }*/
            //jQuery('.tabimgwpr').removeClass("active-fourth");
            jQuery('.tabimgtext').removeClass("end-part");
        }

        function EndLevelBackFunction() {
             console.log("end label");
              jQuery('.vertical-menu .active-parts').removeClass('end-open');
             jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
                setTimeout(function(){
                jQuery("rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li ul").removeClass('active-air');
                   jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').addClass('selected').show();
                   jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
                   jQuery('tabbing_lev4').removeClass('air-level');
                    jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").removeClass('up');
                    jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").addClass('down');
                }, 100);
                jQuery('.tabimgwpr.active-fourth').removeClass('air-open');
                 jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
                    setTimeout(function(){
                       jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").removeClass('final-dest');
                        jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
                    },100);
                jQuery('.tabimgtext').removeClass("end-part");
                jQuery(".back_air_level").remove();
                jQuery(".end_level").remove();
        }
		function removeFourthLevelLiClass(){
			if(jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').hasClass('selected')){				
				jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').removeClass('selected');
			}
			if(jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').hasClass('dismiss')){				
				jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').removeClass('dismiss');
			}
			if(jQuery("li.category-item.active-hover").hasClass('selected')){				
				jQuery("li.category-item.active-hover").removeClass('selected');
			}
			if(jQuery(".vertical-menu > li").hasClass('selected')){				
				jQuery(".vertical-menu > li").removeClass('selected');
			}
			if(jQuery(".tabimgwpr").hasClass('selected')){				
				jQuery(".tabimgwpr").removeClass('selected');
			}
			
		}
        function PartsBackFunction() {
           console.log("parts back");
		   removeFourthLevelLiClass();
            //jQuery(".active-parts > a").text(jQuery(".active-parts > a").text().replace('Parts',''));
            setTimeout(function(){
                jQuery(".vertical-menu > li").removeClass('active-parts');
                jQuery(".vertical-menu > li").removeClass('main_openactive01');
                jQuery(".parts_back").remove();
                jQuery(".parts").remove();
                jQuery(".last_level").remove();
                jQuery(".end_level").remove();
                jQuery(".active-hover").removeClass("hide-parts");
                // jQuery(".megamenu.fullmenu.tabmenu").hide();
                //jQuery("last-part").text(jQuery(".last-part").text().replace('Parts',''));
                jQuery(".verticalopen").hide();
                jQuery(".tabimgwpr ul.tabbing_lev4").attr('style','display:none');
                jQuery(".tabimgwpr").attr('style','display:none');
                jQuery(".vertical-menu > li > a").attr('style', 'display: block;top: 0;');
                //jQuery(".vertical-menu > li").addClass('selected').show("slide", { direction: "left" }, 100);
                jQuery(".vertical-menu > li").addClass('selected').show();
                if (jQuery(".vertical-menu > li > a").attr('style') == 'display: block;top: 0;') {
                    jQuery(".vertical-menu > li > a").removeAttr("style");
                }
                jQuery('.hover .vertical-menu > li').attr('style', 'display: block');
                jQuery('.hover .vertical-menu > li .verticalopen').attr('style', 'display: none');
             }, 100);

            jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
            setTimeout(function(){
        
               jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').attr('style', 'display: block');
               jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
                jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").removeClass('up');
                jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").addClass('down');
            }, 100);
            jQuery('.tabimgwpr.active-fourth').removeClass('air-open');
                 jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
                    setTimeout(function(){
                       jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").removeClass('final-dest');
                        jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
                    },100);
            jQuery('.tabimgtext').removeClass("end-part");
            jQuery(".back_air_level").remove();
        }

        jQuery('body').on('click', '.last_level', function () {
			if(jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').hasClass('selected')){
				
			jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').removeClass('selected');
			}
			removeFourthLevelLiClass();
            LastLevelBackFunction();
            jQuery('.last_level').hide();
            jQuery('.parts_back').show();

            //EndLevelBackFunction();
        });

         jQuery('body').on('click', '.end_level', function () {
			removeFourthLevelLiClass();
            EndLevelBackFunction();
            // jQuery('.parts_back').show();
            //jQuery('.parts_back_main').show();
            jQuery(".parts_back").hide();
            jQuery(".parts_back_main").hide();
            jQuery('.last_level').show();
             jQuery(this).parent().show();
            //jQuery(this).parent().show("slide", { direction: "left" }, 100);
            
        });

       jQuery('body').on('click', '.parts_back', function () {
         jQuery(".parts_back").hide();
		   jQuery('.parts_back_main').show();
            PartsBackFunction();
            LastLevelBackFunction();
            EndLevelBackFunction();
			removeFourthLevelLiClass();
        }); 

        //Back to menu
        jQuery('body').on('click', '.parts_back_main', function () {
            console.log("parts back main");
			removeFourthLevelLiClass();
            PartsBackFunction();
            LastLevelBackFunction();
            EndLevelBackFunction()
            jQuery("li.category-item > a").css('display', 'block');
            jQuery("li.category-item").show();
            //jQuery("li.category-item").show("slide", { direction: "left" }, 100);
            jQuery(".menu-backlist").remove();
            jQuery(".parts_back").remove();
            jQuery(".last_level").remove();
            jQuery(".back_air_level").remove();
            jQuery(".end_level").remove();
            jQuery("#hotlinemobile").show();
            jQuery(".mobile-regis").attr('style','display:block !important');
            jQuery(".mobile-contact").attr('style','display:block !important');
            jQuery(".nav-sections-item-content:last").attr('style','display:block !important');
            jQuery(".custom-menus").show();
            //jQuery(".custom-menus").show("slide", { direction: "left" }, 100);
              jQuery(".active-parts > a").text(jQuery(".active-parts > a").text().replace('Parts',''));
              jQuery(".active-hover").removeClass("hide-parts");
              jQuery(".mobile-sub.rootmenu-list > li > div.megamenu").hide();
              jQuery(".rootmenu-list > li.category-item.active-hover").hide();
              jQuery(".rootmenu-list > li.category-item").removeClass('active-hover');
             // jQuery(".rootmenu-list > li.category-item").show("slide", { direction: "left" }, 100);
              jQuery(".rootmenu-list > li.category-item").show();
            jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
            jQuery('.tabimgwpr.active-fourth').removeClass('air-open');
                 jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
                    setTimeout(function(){
                       jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").removeClass('final-dest');
                        jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
                    },100);
            setTimeout(function(){
         
               jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').attr('style', 'display: block');
               jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
                jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").removeClass('up');
                jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a').parent().find(".arrow").addClass('down');
            }, 100);
            jQuery('.tabimgtext').removeClass("end-part");

        });
        //Back to Parts
       
     /*  code*/
     
     
    
     jQuery('body').on('click', '.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li > a', function () {
		 removeFourthLevelLiClass();
                jQuery("rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li ul").removeClass('active-air');
                 jQuery('.vertical-menu .active-parts').addClass('end-open');
                 jQuery('tabbing_lev4').removeClass('air-level');
                 //jQuery(".last_level").hide();
                   // jQuery(".parts_back").hide();
                    //jQuery(".parts_back_main").hide();
                 //jQuery('ul.tabbing_lev4 > li > a').addClass('removeborderbottom');
                if(jQuery(this).parent().find(".arrow").hasClass('down')){
                    //jQuery(this).parent().find(".arrow").next().slideToggle('fast');
                    jQuery(".end_level").remove();
                    jQuery(this).parent().parent().addClass('air-level');
					jQuery('ul.tabbing_lev4.air-level > li:last-child a').addClass('removeborderbottom');
                    jQuery('.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4 > li').attr('style','display:none');
                    //jQuery(this).parent().attr('style','display:block');
                    jQuery(this).parent().show();
                    //jQuery(this).parent().show("slide", { direction: "right" }, 100);
                    jQuery(this).closest('.tabimgwpr').find('a.tabimtag > .tabimgtext').addClass('end_part');
                    
                    //jQuery(".last_level").after("<div class='end_level'>Back to "+jQuery(".rootmenu-list .mainmenuwrap .tabimgwpr.active-fourth .tabimtag .end_part").text()+"");
                    jQuery(".end_level").attr('style','margin:10px');
                    

                    jQuery(this).parent().find(".arrow").removeClass('down');
                    // jQuery(this).parent().find(".arrow").addClass('up');
                    jQuery(this).parent().find("ul").addClass('active-air');
                    return false;
                }

                else if(jQuery(this).parent().find(".arrow").hasClass('up'))
                {
                    
                    return false;
                }
                else {
                    jQuery(this).parent().find(".arrow").removeClass('up');
                    jQuery(this).parent().find(".arrow").addClass('down');

                }
                
            });
        
        /*** air level****/
        jQuery('body').on('click', '.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4.air-level > li > a', function () {
            jQuery(".back_air_level ").remove();
			removeFourthLevelLiClass();
                 jQuery('.tabimgwpr.active-fourth').addClass('air-open');
                if(jQuery(this).parent().find("i").hasClass('arrow')){
                    // jQuery(".end_level").after("<div class='back_air_level'>Back to "+jQuery(".rootmenu-list .mainmenuwrap .tabimgwpr .tabimtag .tabbing_lev4.air-level .level4-name").text()+" Parts");
                    jQuery(".back_air_level").attr('style','margin:10px');
                    //jQuery(this).parent().find("ul").attr('style','display:block');
                    jQuery(this).parent().find("ul").show();
                    //jQuery(this).parent().find("ul").show("slide", { direction: "right" }, 100);
                    jQuery(this).parent().find("ul").addClass('final-dest');
                    //jQuery(this).parent().find("i").toggleClass('up');
                    //jQuery(this).parent().toggleClass('jtstcls');
                    return false;
                }

                else if(jQuery(this).parent().find(".arrow").hasClass('up'))
                {
                    
                    return false;
                }
                
            });

        jQuery('body').on('click', '.back_air_level', function () {
			removeFourthLevelLiClass();
            console.log("back_air_level label");
            jQuery('.tabimgwpr.active-fourth').removeClass('air-open');
         jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
            setTimeout(function(){
               jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").removeClass('final-dest');
                jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
               jQuery(".back_air_level").remove();
            },100);
        });
        /****** air levle****/

        /*** air level****/
        // jQuery('body').on('click', '.rootmenu-list .mainmenuwrap .tabimgwpr .tabbing_lev4.air-level > li > a', function () {
        //          jQuery('.tabimgwpr.active-fourth').addClass('air-open');
        //         if(jQuery(this).parent().find("i").hasClass('arrow')){
        //             jQuery(".end_level").after("<div class='back_air_level'>Back to "+jQuery(".rootmenu-list .mainmenuwrap .tabimgwpr .tabimtag .end_part").text()+" Parts");
        //             jQuery(".back_air_level").attr('style','margin:10px');
        //             jQuery(this).parent().find("ul").attr('style','display:block');
        //             jQuery(this).parent().find("ul").addClass('final-dest');
        //             return false;
        //         }

        //         else if(jQuery(this).parent().find(".arrow").hasClass('up'))
        //         {
                    
        //             return false;
        //         }
                
        //     });

        // jQuery('body').on('click', '.back_air_level', function () {
        //     console.log("back_air_level label");
        //     jQuery('.tabimgwpr.active-fourth').removeClass('air-open');
        //  jQuery("end_part").text(jQuery(".end_part").text().replace('Parts',''));
        //     setTimeout(function(){
        //        jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").removeClass('final-dest');
        //         jQuery(".tabimgwpr ul.tabbing_lev4 > li > ul").attr('style','display:none');
        //        jQuery(".back_air_level").remove();
        //     },100);
        // });
		//Hide menu click on search
		jQuery('body').on('click', '#search', function () {
			var w_height = parseInt(jQuery( window ).width());
			if(w_height < 1000){
				if(jQuery('html').hasClass('nav-open')){
					jQuery('html').removeClass('nav-open');
				}
				if(jQuery('html').hasClass('nav-before-open')){
					jQuery('html').removeClass('nav-before-open');
				}
			}
		});
        /****** air levle****/
     //Main menu  click
     
        jQuery('body').on('click', '.rootmenu-list > li.category-item > a', function (event) {
            // if(jQuery(this).parent().hasClass('active-hover')){
            //     jQuery(this).parent().find("ul").toggle();
            // }
			var w_height = parseInt(jQuery( window ).width());
            if(w_height <= 1000) {
				event.preventDefault();
				jQuery(this).parent().addClass('active-hover');
				removeFourthLevelLiClass();
				var label = jQuery(this).text();
				jQuery(".menu-backlist").remove();
				jQuery(".parts_back_main").remove();
                /*jQuery(jQuery(this).parent().find("ul")).animate({
                    width: "100%"
                });*/
                //if(w_height < 1000){
                     jQuery("li.category-item").hide();
                     //jQuery("li.category-item.active-hover").animate({ width: '100%' },100);
                     jQuery("li.category-item.active-hover").addClass('selected').show();
                     jQuery("li.category-item.active-hover > a").hide();
                     jQuery(".mobile-regis").attr('style','display:none !important');
                       jQuery(".mobile-contact").attr('style','display:none !important');
                       jQuery(".custom-menus").attr('style','display:none !important');
                        jQuery(".nav-sections-item-content:last").attr('style','display:none !important');
                    jQuery(".mobile-sub.rootmenu-list").before("<div class='menu-backlist'><div class='parts_back_main'>"+label+"</div></div>");
                    jQuery(".parts_back_main").attr('style','margin:10px');
                    jQuery("#hotlinemobile").attr('style','display:none;');
                    // second time click
					 jQuery(this).next().show();
                    jQuery('.vertical-menu02 li a span').hide();//hide images
                    jQuery('.rootmegamenu_block .vertical-menu02 li a span').show();
                    jQuery('.vertical-menu02 li a span.cat-arrow').show();//hide images
                 //}
			}
		  }
        );
		jQuery('body').on('click', '.toogle-header', function () {
			var w_height = parseInt(jQuery( window ).width());
			if(w_height < 1000){
				if(jQuery(this).parent().siblings().find('ul.header-menu-level-container').is(":visible")){
					// jQuery(this).parent().siblings().find('ul.header-menu-level-container').toggle();
				}
				jQuery(this).next().toggle();
                jQuery(this).toggleClass('maintsf');
                
			}
		});
        jQuery("#megamenu_submit").click(function(){
            var name = jQuery("#name").val();
            var menuemail = document.getElementById('menuemail');
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(menuemail.value)) {
                alert('Please provide a valid email address');
                menuemail.focus;
                return false;
            }
             
            var menuemail = jQuery("#menuemail").val();
            var comment = jQuery("#comment").val();
            var telephone = jQuery("#telephone").val();
            var hideit = jQuery("#hideit").val();
            var base_url = jQuery("#base_url").val();
            
            var dataString = 'name='+ name + '&email='+ menuemail + '&comment='+ comment + '&telephone='+ telephone + '&hideit='+ hideit;
            if(name==''||menuemail==''||comment==''){
                alert("Please Fill All Fields");
            } else {
                jQuery('#megamenu_submit').attr('id','menu_submit_loader');
                jQuery.ajax({
                    type: "POST",
                    url: base_url+"contact/index/post/",
                    data: dataString,
                    cache: false,
                    success: function(result){
                        alert('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.');
                        jQuery('#menu_submit_loader').attr('id','megamenu_submit');
                    }
                });
            }
            return false;
        });
        jQuery(".categoriesmenu li > a").hover(
            function() {
                var c_block = jQuery(this).closest('li').find('.categoryblockcontent').html();
                var m_c_block = jQuery(this).closest('.megamenu').find('.main_categoryblockcontent').html();
                if (c_block){
                    jQuery(this).closest('.megamenu').find('.right.rootmegamenu_block').html(c_block);
                } else {
                    jQuery(this).closest('.megamenu').find('.right.rootmegamenu_block').html(m_c_block);
                }
                
            }, function() {
                //jQuery( this ).removeClass( "hover" );
            }
        );

        var layeredFilter = $('.sidebar.sidebar-main #layered-filter-block');
        if (layeredFilter.length) {
            mediaCheck({
                media: '(min-width: 1000px)',
                entry: function () {
                    setTimeout(function () {
                        updateMaxHeight();
                    }, 500);
                },
                exit: function () {
                    if (layeredFilter.css('max-height')) {
                        layeredFilter.css('max-height', '');
                        $(window).on('resize', function () {
                            layeredFilter.css('max-height', '');
                        });
                    }
                }
            });
        }

        function updateMaxHeight(flag) {
            var scrollTop = $(window).scrollTop(),
                filterTop = layeredFilter.offset().top,
                windowH = $(window).height(),
                filterMaxH = windowH - filterTop + scrollTop;
            if (flag) {
                filterMaxH = '';
            }
            layeredFilter.css('max-height', filterMaxH);
        }
    });
    
    var msnry;
    function loadMasonry() {
        if (document.getElementsByClassName("rootmenu")[0]) {
            var rootmenu_cls = document.getElementsByClassName("rootmenu")[0];
            var elem = rootmenu_cls.getElementsByClassName("grid");
            var n = elem.length;
            for (var i = 0; i < n; i++) {
                msnry = new Masonry( elem[i], {
                    
                });
                if(window.innerWidth < 1000) {
                    msnry.destroy();
                }
            }
        }
    }
    loadMasonry();
        
});

function setmenuheight() {
    var w_inner_width = window.innerWidth;
    if(w_inner_width <= 1000){
        jQuery(".tabmenu").css('height','100%');
        jQuery(".tabmenu02").css('height','100%');
        jQuery(".verticalopen").css('height','100%');
        
        
    } else {
        var MultitabMaxHeight = jQuery(".hover .tabmenu .mainmenuwrap").innerHeight();
        var MultitabMaxHeight2 = jQuery(".hover .tabmenu .main_openactive01 .verticalopen").innerHeight();
        MultitabMaxHeight = MultitabMaxHeight2 > MultitabMaxHeight ? MultitabMaxHeight2 : MultitabMaxHeight;
        jQuery(".hover .tabmenu .main_openactive01 .verticalopen").css('min-height',MultitabMaxHeight);
        jQuery(".hover .tabmenu").css('height',MultitabMaxHeight+20);
    }
}

function setmenuheight_horizontal() {
    var w_inner_width = window.innerWidth;
    if(w_inner_width <= 1000){
        jQuery(".tabmenu02").css('height','100%');
        jQuery(".verticalopen02").css('height','100%');
    } else {
        var final_hor_width = jQuery('.main_openactive02 .verticalopen02').innerHeight();
        jQuery(".main_openactive02 .verticalopen02").css('height',final_hor_width);
        //jQuery(".hover .tabmenu02").css('height',final_hor_width+80);
    }
}


function setmenuheight_multitab(val) {
    var w_inner_width = window.innerWidth;
    if(w_inner_width <= 1000){
        jQuery(".fourcoltab").css('height','100%');
    } else {
        var MultitabMaxHeight = jQuery(".hover .fourcoltab .colultabone").innerHeight();
        var MultitabMaxHeight2 = jQuery(".hover .fourcoltab .main_openactive03 .colultabtwo").innerHeight();
        var MultitabMaxHeight3;
        
        MultitabMaxHeight = MultitabMaxHeight2 > MultitabMaxHeight ? MultitabMaxHeight2 : MultitabMaxHeight;
        
        if (val == '2') {
            MultitabMaxHeight3 = jQuery(".hover .fourcoltab .main_openactive03_sub1 .colultabthree").innerHeight();
            MultitabMaxHeight = MultitabMaxHeight3 > MultitabMaxHeight ? MultitabMaxHeight3 : MultitabMaxHeight;
            
            jQuery(".hover .fourcoltab .main_openactive03_sub1 .colultabthree").css('min-height',MultitabMaxHeight);
            jQuery(".hover .fourcoltab .colultabonenofound").css('min-height',MultitabMaxHeight);
            jQuery(".hover .fourcoltab").css('height',MultitabMaxHeight+20);
        } else if (val == '3') {

            MultitabMaxHeight3 = jQuery(".hover .fourcoltab .main_openactive03_sub1 .colultabthree").innerHeight();
            MultitabMaxHeight = MultitabMaxHeight3 > MultitabMaxHeight ? MultitabMaxHeight3 : MultitabMaxHeight;

            var MultitabMaxHeight4 = jQuery(".hover .fourcoltab .main_openactive03_sub1 .colultabthree .main_openactive03_sub2 .resultdiv").innerHeight();
            MultitabMaxHeight = MultitabMaxHeight4 > MultitabMaxHeight ? MultitabMaxHeight4 : MultitabMaxHeight;
            
            jQuery(".hover .fourcoltab .main_openactive03_sub1 .resultdiv").css('min-height',MultitabMaxHeight);
            jQuery(".hover .fourcoltab .main_openactive03_sub1 .colultabonenofound").css('min-height',MultitabMaxHeight);
            jQuery(".hover .fourcoltab").css('height',MultitabMaxHeight+20);
        } else {
            jQuery(".hover .fourcoltab .main_openactive03 .colultabtwo").css('min-height',MultitabMaxHeight);
            jQuery(".hover .fourcoltab").css('height',MultitabMaxHeight+20);
        }
    }
}
function setAllCategoryMenuHeight() {
    var wrapper_width =  jQuery(".rootmenu").innerWidth();
    var left_side_width =  jQuery(".verticalmenu02").innerWidth();
    if (jQuery( window ).width() < 1000) {
        jQuery(".varticalmenu_main.fullwidth").css('margin-left',0);
        jQuery(".varticalmenu_main.fullwidth").css('width','100%');
        jQuery(".varticalmenu_main.fullwidth").css('min-height','auto');
        jQuery(".varticalmenu_main.halfwidth").css('margin-left',0);
        jQuery(".varticalmenu_main.halfwidth").css('width','100%');
        jQuery(".varticalmenu_main.halfwidth").css('min-height','auto');
    } else {
        var MultitabMaxHeight = jQuery(".all-category-wrapper ul.vertical-list").innerHeight();
        var MultitabMaxHeight2 = jQuery(".all-category-wrapper ul.vertical-list .hover_allcategories").innerHeight();
        MultitabMaxHeight = MultitabMaxHeight2 > MultitabMaxHeight ? MultitabMaxHeight2 : MultitabMaxHeight;
        
        jQuery('.all-category-wrapper .varticalmenu_main').css("display", "");
        jQuery(".all-category-wrapper .varticalmenu_main").css('min-height', MultitabMaxHeight);
        jQuery(".all-category-wrapper .varticalmenu_main").css('margin-left', left_side_width);
        jQuery(".all-category-wrapper .varticalmenu_main.halfwidth").css('width', wrapper_width/2);
        jQuery(".all-category-wrapper .varticalmenu_main.fullwidth").css('width', wrapper_width-left_side_width);
    }
}
