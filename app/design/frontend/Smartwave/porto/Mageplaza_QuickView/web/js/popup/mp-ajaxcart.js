/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'mageplaza/core/jquery/popup'
], function ($, customerData) {
    'use strict';

    $.widget('mageplaza.ajaxCart', {
        options: {
            processStart: null,
            processStop: null,
            bindSubmit: true,
            minicartSelector: '[data-block="minicart"]',
            messagesSelector: '[data-placeholder="messages"]',
            productStatusSelector: '.stock.available',
            addToCartButtonSelector: '.action.tocart',
            addToCartButtonDisabledClass: 'disabled',
            addToCartButtonTextWhileAdding: '',
            addToCartButtonTextAdded: '',
            addToCartButtonTextDefault: ''
        },

        /**
         * @inheritDoc
         */
        _create: function () {
            this.addToCart();
            this.addToCartInPopup();
            this._eventListener();
        },

        _eventListener: function () {
            $('.mp-btnclose, .continue-shopping').on('click', function () {
                $('#mpajaxcart-popup').hide();
                $('#mpajaxcart-popup-content').html('');
                $('.mpajaxcart-overlay').hide();
            });
        },

        /**
         * Add to cart on popup don't choose option
         */
        addToCartInPopup: function () {
            var self = this;

            $('body').delegate('button.action.mptocart', 'click', function (e) {
                var actionUrl = '',
                    content   = $('#mpajaxcart-popup-content'),
                    form,
                    checkOption;

                e.preventDefault();

                if (content.find('.product-info-main').length < 1) {
                    return;
                }
                e.preventDefault();
                form        = content.find('form#product_addtocart_form');
                checkOption = self.checkSelectOption();

                if (checkOption) {
                    actionUrl = form.attr('action');

                    $.ajax({
                        url: actionUrl + '?mppopup=1',
                        data: new FormData(form[0]),
                        type: 'post',
                        cache: true,
                        contentType: false,
                        processData: false,
                        showLoader: true,
                        success: function (res) {
                            self.addToCartInPopupSuccess(self, res, form);
                        }
                    });
                }
            });
        },

        addToCartInPopupSuccess: function (self, res, form) {
            var message = $('.mpquickview-message'),
                popup   = $('#mpajaxcart-popup'),
                content = $('#mpajaxcart-popup-content'),
                action  = $('.mpajaxcart-actions');

            if (res.error) {
                message.remove();
                action.show();
                $.each(res.message, function (key, value) {
                    $('#mpajaxcart-popup-main').prepend(
                        '<div class="mpquickview-message message-error error message">' + value + '</div>');
                });
            }

            if (res.success) {
                if (self.options.isAnimate === '1') {
                    self.flyToCart(form);
                }

                message.remove();
                content.html(res.html);
                action.hide();
                popup.trigger('contentUpdated');
            }
        },

        /**
         * Ajax add to cart
         */
        addToCart: function () {
            var self    = this,
                formKey = $('input[name="form_key"]').val(),
                popup   = $('#mpajaxcart-popup'),
                content = $('#mpajaxcart-popup-content'),
                action  = $('.mpajaxcart-actions'),
                overlay = $('.mpajaxcart-overlay');

            $('body').delegate('button.action.tocart', 'click', function (e) {
                var el        = $(this),
                    dataPost  = el.data('post'),
                    actionUrl = '',
                    params    = '',
                    form      = '',
                    url       = '',
                    dataForm,
                    validate,
                    dataFormQV,
                    validateQV,
                    productId;

                /** check checkout cart page **/
                if ($('.checkout-cart-index').length) {
                    return;
                }

                /** Check apply for quick view popup **/
                if (el.closest('#mpquickview-popup #maincontent').length && !self.options.isApplyQV) {
                    return;
                }

                /** Check apply for action name page */
                if (!el.closest('#mpquickview-popup #maincontent').length && !self.options.isApplyPage) {
                    return;
                }

                /** check for detail page **/
                if (self.options.isApplyPage
                    && (el.closest('.product-info-main').length
                        || el.closest('.bundle-options-container').length)) {
                    e.preventDefault();
                    dataForm = $('form#product_addtocart_form');
                    validate = dataForm.validation('isValid');

                    if (validate) {
                        form = $(this).closest('form');
                        url  = form.attr('action');

                        params = form.serialize();

                        self.showPopup(params, url, form);
                    }
                    return;
                }

                if (dataPost) {
                    actionUrl = dataPost.action;

                    if (actionUrl.indexOf('wishlist/index/cart') !== -1) {
                        return;
                    }

                    params          = dataPost.data;
                    params.form_key = formKey;

                    e.stopPropagation();
                    self.showPopup(params, actionUrl, el);
                } else {
                    dataFormQV = $('form#product_addtocart_form');
                    validateQV = dataFormQV.validation('isValid');
                    form       = el.closest('form');

                    if (!validateQV) {
                        return;
                    }
                    if (form.length) {
                        actionUrl = form.attr('action');
                        params = form.serialize();
                    }
                    if (actionUrl !== '' && actionUrl.indexOf('checkout/cart/addgroup') !== -1) {
                        return;
                    }

                    if (actionUrl !== '' && actionUrl.indexOf('options=cart') !== -1) {
                        productId = parseInt(form.find('input[name="product"]').val(), 10);

                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: self.options.detailUrl + 'id/' + productId + '?mpquickview=cart',
                            showLoader: true,
                            success: function (html) {
                                content.html(html);
                                content.find('button.action.tocart').remove();
                                action.show();
                                popup.show();
                                overlay.show();
                                popup.trigger('contentUpdated');
                            }
                        });

                        return;
                    }

                    e.preventDefault();
                    var unserialData = self.unserializeFormData(params);
                   if(unserialData.color == undefined){ //not color option found
                    self.showPopup(params, actionUrl, form);
                   }else{ ////found color option found
                       if(unserialData.color.length == 0){
                           if(el.parents('.product-item-info').find('.product_addtional_data .custom-option-validation').length == 0)
                            el.parents('.product-item-info').find('.product_addtional_data').append('<div class="custom-option-validation" style="margin:3px 0;color:#e02b27;font-size: 1.2rem;">This is a required field.</div>');
                            else
                            el.parents('.product-item-info').find('.product_addtional_data .custom-option-validation').show();
                        }else{
                            if(el.parents('.product-item-info').find('.product_addtional_data .custom-option-validation').length > 0)
                                el.parents('.product-item-info').find('.product_addtional_data .custom-option-validation').hide(); 
                        self.showPopup(params, actionUrl, form);
                       }
                   }
                }
            });
        },
        unserializeFormData:function(data) {
            data = data.split('&');
            var response = {};
            for (var k in data){
                var newData = data[k].split('=');
                if(newData[0] == "super_attribute%5B93%5D"){
                response['color'] = newData[1];
                }
                else{
                response[newData[0]] = newData[1];
                }
            }
            return response;
        },
        showPopup: function (params, actionUrl, form) {
            var self = this;

            if (form.find('input[type="file"]').length > 0) {
                $.ajax({
                    url: actionUrl + '?mpajaxcart=1',
                    data: new FormData(form[0]),
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    showLoader: true,
                    success: function (res) {
                        self.showPopupSuccess(self, res, form);
                    }
                });
            } else {
                $.ajax({
                    url: actionUrl + '?mpajaxcart=1',
                    data: form.serialize(),
                    type: 'post',
                    processData: 'json',
                    showLoader: true,
                    success: function (res) {
                        self.showPopupSuccess(self, res, form);
                        if (res.error) {
                            $.each(res.message, function (key, value) {
                                $('#mpajaxcart-popup-main').prepend(
                                    '<div class="mpquickview-message message-error error message">' + value + '</div>');
                            });
                        }
                    }
                });
            }

        },

        showPopupSuccess: function (self, res, form) {
            var magnificPopup = $.magnificPopup.instance,
                message       = $('.mpquickview-message'),
                popup         = $('#mpajaxcart-popup'),
                content       = $('#mpajaxcart-popup-content'),
                action        = $('.mpajaxcart-actions'),
                overlay       = $('.mpajaxcart-overlay'),
                sections;

            magnificPopup.close();
            if (res.detailPage) {
                $.ajax({
                    type: "POST",
                    url: res.detailPage.url + 'id/' + res.detailPage.productId + '?mpquickview=cart',
                    showLoader: true,
                    success: function (html) {
                        message.remove();
                        content.html(html);
                        content.find('button.action.tocart').remove();
                        action.show();
                        popup.show();
                        overlay.show();
                        popup.trigger('contentUpdated');
                    }
                });
            } else {
                if (self.options.isAnimate === '1') {
                    self.flyToCart(form);
                }
                message.remove();
                content.html(res.html_popup);
                popup.show();
                overlay.show();
                action.hide();
                popup.trigger('contentUpdated');
            }

            sections = ['cart'];
            customerData.invalidate(sections);
            customerData.reload(sections, true);
        },

        /**
         * Check select option
         * @returns {boolean}
         */
        checkSelectOption: function () {
            $('#mpajaxcart-popup-content .swatch-attribute').each(function () {
                var el  = $(this),
                    elm = el.attr('option-selected');

                if (!elm) {
                    return false;
                }
            });

            return true;
        },

        /**
         * Effect fly to mini cart
         * @param form
         */
        flyToCart: function (form) {
            var productEl = form.closest('.product-item, .product.info, .item, .main'),
                wrapper   = productEl.find('div.fotorama__active, .product-image-wrapper, .product-item-photo'),
                flyer     = wrapper.find('img'),
                minicart  = $('[data-block="minicart"]'),
                posFlyer  = flyer.offset(),
                posCart   = minicart.offset(),
                flyTo     = $('<div id="mp-fly-tocart"></div>'),
                flyerClone;

            if (flyer.length && minicart.length) {
                flyer          = $(flyer[0]);
                flyerClone = $(flyer).clone();

                $(flyerClone).css({
                    'opacity': 1,
                    'maxWidth': '100%',
                    'position': 'relative'
                });

                flyTo.append(flyerClone);
                flyer.append(flyTo);

                $('body').append(
                    flyTo.css({
                        top: posFlyer.top,
                        left: posFlyer.left
                    })
                );

                flyTo.animate({
                    opacity: 0.4,
                    left: posCart.left + 'px',
                    top: posCart.top + 'px',
                    width: 0,
                    height: 0
                }, 1400, function () {
                    $(minicart).fadeOut('fast', function () {
                        $(minicart).fadeIn('fast', function () {
                            $(flyTo).fadeOut('fast', function () {
                                $(flyTo).remove();
                            });
                        });
                    });
                });
            }
        }
    });

    return $.mageplaza.ajaxCart;
});

