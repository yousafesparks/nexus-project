/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'mage/translate',
    'mageplaza/core/jquery/popup'
], function ($, $t) {
    'use strict';

    $.widget('mageplaza.quickview', {
        options: {
            processStart: null,
            processStop: null,
            bindSubmit: true,
            minicartSelector: '[data-block="minicart"]',
            messagesSelector: '[data-placeholder="messages"]',
            productStatusSelector: '.stock.available',
            addToCartButtonSelector: '.action.tocart',
            addToCartButtonDisabledClass: 'disabled',
            addToCartButtonTextWhileAdding: '',
            addToCartButtonTextAdded: '',
            addToCartButtonTextDefault: ''
        },

        /**
         * @inheritDoc
         */
        _create: function () {
            this.initButton();
            this.editCart();
        },

        /**
         * set style for content on quickview popup
         */
        styleContent: function () {
            var productImgQv = $('.mpquickview-popup .product.media'),
                productImgEc = $('.mpquickview-popup-editcart .product.media');

            if (productImgQv.is(':hidden')) {
                $('.mpquickview-popup .product-info-main').css({'float': 'left', 'width': '100%'});
            }

            if (productImgEc.is(':hidden')) {
                $('.mpquickview-popup-editcart .product-info-main').css({'float': 'left', 'width': '100%'});
            }

        },

        /**
         * Add button quickview
         * Ajax and show popup quickview product when click button quickview
         */
        initButton: function () {
            var self       = this,
                product    = $('ol.items.product-items li.product-item, ol.widget-product-grid li.product-item, .item.product.product-item'),
                buttonHtml = this.options.buttonHtml;

            product.each(function () {
                var el        = $(this),
                    container = el.children('.product-item-info').data('container'),
                    productId = el.find('.price-box.price-final_price').attr('data-product-id');

                el.find('.product-item-info').append(buttonHtml);

                //style on sidebar porto
                $('.owl-stage .item.product-item .related-available .mpquickview-button').css({
                    'transform': '',
                    'top': '50%',
                    'left': '15%'
                });

                $('.products-related .owl-stage .item.product-item .mpquickview-button').css({
                    'transform': '',
                    'top': '35%',
                    'left': '20%'
                });

                $('.owl-stage .item.product-item .related-available .mpquickview-button span').css({'width': 'auto'});

                if (container === 'product-list') {
                    el.css('position', 'relative');
                    el.find('.mpquickview-button').css('left', '12%');
                }

                el.find('.mpquickview-button').on('click', function () {
                    self.processQuickView(productId, false, false);
                });
            });
            this.initMagnificPopup(product);

            // compatible with salespop module
            $('#mpsalespop-popup').on('mpsalespopUpdated', function () {
                var lastPopup = $('.mpsalespop-popup-floating').last(),
                    productId = lastPopup.attr('data-product-id');

                if (productId) {
                    lastPopup.find('.mpsalespop-container').append(buttonHtml);
                    $('.mpsalespop-container').hover(
                        function () {
                            $(this).find('.mpsalespop-product-image').css("opacity", 0.5);
                        }, function () {
                            $(this).find('.mpsalespop-product-image').css("opacity", 1);
                        });

                    lastPopup.find('.mpquickview-button').on('click', function () {
                        self.processQuickView(productId, false, false);
                    });

                    self.initMagnificPopup(lastPopup);
                }
            });
        },

        /**
         * Use AJAX to get product page html
         * @param productId
         */
        processQuickView: function (productId, listCart, itemId) {
            var self            = this,
                urlOrigin       = this.options.url,
                productNextPrev = this.options.productNextPrev,
                type            = 'POST',
                popup           = $('#mpquickview-popup'),
                loader          = $('.mfp-preloader'),
                url,
                productNextIcon,
                productPrevIcon;

            if (listCart && itemId) {
                type = 'GET';
                url  = this.options.urlCart + 'id/' + itemId + '/product_id/' + productId + '?mpquickview=1';
            } else {
                url = urlOrigin + 'id/' + productId + '?mpquickview=1';
            }

            $.ajax({
                type: type,
                url: url,
                data: {'productId': productId},
                cache: false,
                success: function (html) {
                    var btnClose = '<button title="Close (Esc)" type="button" class="mfp-close">×</button>';

                    if (html.html_content) {
                        loader.hide();
                        popup.html(html.html_content);
                        $('.mpquickview-popup-editcart input#qty').val(html.qty);            // fix bug: null qty
                        popup.prepend(btnClose);
                        $('body').trigger('contentUpdated');

                        self.styleContent();
                        self.openProductTab();

                        if (!self.options.isCompareQV) {
                            self.compare(urlOrigin);
                        }
                    } else {
                        $('.mfp-preloader').hide();
                        popup.html(html);
                        popup.prepend(btnClose);
                        $('body').trigger('contentUpdated');

                        self.styleContent();
                        self.openProductTab();

                        if (!self.options.isApplyQV || $('.checkout-cart-index').length) {
                            self.addToCart();
                        }

                        if (!self.options.isCompareQV) {
                            self.compare(urlOrigin + 'id/' + productId);
                        }
                    }

                    productNextIcon = popup.find('.prev-next-products .product-next a');
                    productPrevIcon = popup.find('.prev-next-products .product-prev a');

                    productNextIcon.removeAttr('href');
                    productPrevIcon.removeAttr('href');

                    productNextIcon.on('click', function () {
                        self.getProductNextPrev(productNextPrev, productId, 'next');
                    });

                    productPrevIcon.on('click', function () {
                        self.getProductNextPrev(productNextPrev, productId, 'prev');
                    });
                }
            });
        },

        getProductNextPrev: function (productNextPrev, productId, prevNext) {
            var self = this;

            $.ajax({
                type: "POST",
                url: productNextPrev,
                data: {'productId': productId, 'prevNext': prevNext},
                cache: false,
                success: function (res) {
                    if (res.productId) {
                        self.processQuickView(res.productId, res.listCartVal, res.itemId);
                    }
                }
            });
        },

        initMagnificPopup: function (element) {
            var popup  = $('#mpquickview-popup'),
                loader = $('.mfp-preloader'),
                self   = this;

            element.magnificPopup({
                delegate: 'a.mpquickview-button',
                removalDelay: 500,
                callbacks: {
                    beforeOpen: function () {
                        popup.html('');
                        popup.addClass('mpquickview-popup');
                        popup.removeClass('mpquickview-popup-editcart');
                        this.st.mainClass = this.st.el.attr('data-effect');
                        this.wrap.removeAttr('tabindex');
                    },
                    open: function () {
                        loader.html('');
                        loader.append('<img src="' + self.options.urlLoader + '" alt="Loading...">');
                        loader.show();
                        loader.css('z-index', 2000);
                    },
                    afterClose: function () {
                        popup.html('');
                    }
                },
                midClick: true
            });
        },

        compare: function (productUrl) {
            $('body').delegate('#mpquickview-popup .action.tocompare', 'click', function (e) {
                e.stopPropagation();
                window.location.href = productUrl;
            });
        },

        /**
         * Ajax and show popup when click edit cart button
         */
        editCart: function () {
            var self            = this,
                listCart        = $('#shopping-cart-table'),
                editCartEl      = listCart.find('.action.action-edit'),
                popup           = $('#mpquickview-popup'),
                productNextPrev = this.options.productNextPrev,
                loader          = $('.mfp-preloader'),
                productNextIcon,
                productPrevIcon;


            editCartEl.each(function () {
                var el   = $(this),
                    href = el.attr('href');

                el.attr('href', '#mpquickview-popup');
                el.attr('data-url', href);
                el.attr('data-effect', self.options.mpeffect);

                el.on('click', function (e) {
                    var url = el.attr('data-url');

                    $.ajax({
                        type: "GET",
                        url: url + '?mpquickview=1',
                        cache: false,
                        success: function (res) {
                            var btnClose = '<button title="Close (Esc)" type="button" class="mfp-close">×</button>';

                            loader.hide();
                            popup.html(res.html_content);
                            $('.mpquickview-popup-editcart input#qty').val(res.qty);            // fix bug: null qty
                            popup.prepend(btnClose);
                            $('body').trigger('contentUpdated');

                            self.styleContent();
                            self.openProductTab();

                            if (!self.options.isCompareQV) {
                                self.compare(url);
                            }

                            productNextIcon = popup.find('.prev-next-products .product-next a');
                            productPrevIcon = popup.find('.prev-next-products .product-prev a');

                            productNextIcon.removeAttr('href');
                            productPrevIcon.removeAttr('href');

                            productNextIcon.on('click', function () {
                                self.getProductNextPrev(productNextPrev, res.productId, 'next');
                            });

                            productPrevIcon.on('click', function () {
                                self.getProductNextPrev(productNextPrev, res.productId, 'prev');
                            });
                        }
                    });

                    e.preventDefault();
                });
            });

            listCart.magnificPopup({
                delegate: 'a.action-edit',
                removalDelay: 500,
                callbacks: {
                    beforeOpen: function () {
                        popup.html('');
                        popup.addClass('mpquickview-popup-editcart');
                        popup.removeClass('mpquickview-popup');
                        this.st.mainClass = this.st.el.attr('data-effect');
                    },
                    open: function () {
                        loader.html('');
                        loader.append('<img src="' + self.options.urlLoader + '" alt="Loading...">');
                        loader.show();
                        loader.css('z-index', 2000);
                    }


                },
                midClick: true
            });
        },

        addToCart: function () {
            var self   = this,
                form   = $('#mpquickview-popup #product_addtocart_form'),
                action = form.attr('action'),
                popup  = $('#mpquickview-popup'),
                dataForm,
                validate;

            form.attr('action', action + '?mpquickview=1');

            form.find('.action.tocart').on('click', function (e) {
                var el = $(this);

                e.preventDefault();
                dataForm = $('form#product_addtocart_form');
                validate = dataForm.validation('isValid');

                if (validate) {
                    self.disableAddToCartButton(form);

                    $.ajax({
                        url: form.attr('action'),
                        data: form.serialize(),
                        type: 'post',
                        dataType: 'json',
                        success: function (res) {
                            var message = $('.mpquickview-message');

                            if (!$.isArray(res.error)) {
                                el.removeClass('disabled').find('span').html($t('Add to Cart'));

                                message.remove();
                                $.each(res.message, function (key, value) {
                                    popup.prepend('<div class="mpquickview-message message-error error message">'
                                        + value + '</div>');
                                });
                            } else {
                                if ($('.checkout-cart-index').length) {
                                    location.reload();
                                }

                                message.remove();
                                $.each(res.message, function (key, value) {
                                    if (value === '') {
                                        value = $t('Success!');
                                    }
                                    popup.prepend('<div class="mpquickview-message message-success success message">'
                                        + value + '</div>');
                                });

                                self.enableAddToCartButton(form);
                            }
                        }
                    });
                }
            });
        },

        /**
         * Auto open tab on quickview popup
         */
        openProductTab: function () {
            var tabEls = $('#mpquickview-popup .data.item.title');

            tabEls.each(function () {
                var elm = $(this);

                if (elm.is(':visible')) {
                    elm.addClass('active');
                    elm.attr('aria-selected','true');
                    elm.next().addClass('mptabs-active');
                    return false;
                }
            });

            tabEls.each(function () {
                var elm = $(this);

                elm.on('click', function () {
                    var oldElm = $(this).parent().find('.active');

                    oldElm.removeClass('active');
                    oldElm.next().removeClass('mptabs-active');
                    $(this).addClass('active');
                    $(this).next().addClass('mptabs-active');
                });
            });
        },

        /**
         * @param {String} form
         */
        disableAddToCartButton: function (form) {
            var addToCartButtonTextWhileAdding = this.options.addToCartButtonTextWhileAdding || $t('Adding...'),
                addToCartButton                = $(form).find(this.options.addToCartButtonSelector);

            addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
            addToCartButton.find('span').text(addToCartButtonTextWhileAdding);
            addToCartButton.attr('title', addToCartButtonTextWhileAdding);
        },

        /**
         * @param {String} form
         */
        enableAddToCartButton: function (form) {
            var addToCartButtonTextAdded = this.options.addToCartButtonTextAdded || $t('Added'),
                self                     = this,
                addToCartButton          = $(form).find(this.options.addToCartButtonSelector);

            addToCartButton.find('span').text(addToCartButtonTextAdded);
            addToCartButton.attr('title', addToCartButtonTextAdded);

            setTimeout(function () {
                var addToCartButtonTextDefault = self.options.addToCartButtonTextDefault || $t('Add to Cart');

                addToCartButton.removeClass(self.options.addToCartButtonDisabledClass);
                addToCartButton.find('span').text(addToCartButtonTextDefault);
                addToCartButton.attr('title', addToCartButtonTextDefault);
            }, 1000);
        }

    });

    return $.mageplaza.quickview;
});

