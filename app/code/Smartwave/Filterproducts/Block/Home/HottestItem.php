<?php

namespace Smartwave\Filterproducts\Block\Home;

use Magento\Catalog\Api\CategoryRepositoryInterface;

class HottestItem extends \Magento\Catalog\Block\Product\ListProduct {

    protected $_collection;

    protected $categoryRepository;

    protected $_resource;

    public function __construct(
    \Magento\Catalog\Block\Product\Context $context,
            \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
            \Magento\Catalog\Model\Layer\Resolver $layerResolver,
            CategoryRepositoryInterface $categoryRepository,
            \Magento\Framework\Url\Helper\Data $urlHelper,
            \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
            \Magento\Framework\App\ResourceConnection $resource,
            array $data = []
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->_collection = $collection;
        $this->_resource = $resource;

        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
    }

    protected function _getProductCollection() {
        return $this->getProducts();
    }
	
	protected function _prepareLayout()
    {
        $this->addChild(
            'details.renderers',
            \Magento\Framework\View\Element\RendererList::class
        );

        $this->getLayout()->getBlock($this->getNameInLayout() . '.details.renderers')
            ->addChild('default', \Magento\Framework\View\Element\Template::class);

        $configurableRenderer = $this->getLayout()->getBlock($this->getNameInLayout() . '.details.renderers')
            ->addChild('configurable', \Magento\Swatches\Block\Product\Renderer\Listing\Configurable::class);
        $configurableRenderer->setTemplate('Magento_Swatches::product/listing/renderer.phtml');

        parent::_prepareLayout();
    }
    public function getProducts() {
       
        $count = $this->getProductCount();
        $category_id = $this->getData("category_id");
        $collection = clone $this->_collection;
        $collection->clear()->getSelect()->reset(\Magento\Framework\DB\Select::WHERE)->reset(\Magento\Framework\DB\Select::ORDER)->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)->reset(\Magento\Framework\DB\Select::GROUP);

        if(!$category_id) {
            $category_id = $this->_storeManager->getStore()->getRootCategoryId();
        }
        $category = $this->categoryRepository->get($category_id);
        if(isset($category) && $category) {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_hottest_selling_item')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite() 
                ->addCategoryFilter($category)
				->addFieldToFilter('custitem_f3_hottest_selling_item', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
				
        } else {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_hottest_selling_item')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite()
				->addAttributeToFilter('custitem_f3_hottest_selling_item', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
        }

        $collection->getSelect()
                ->order('created_at','desc')
                ->limit($count);
				/*echo "<pre>";
				foreach ($collection as $product) {
    print_r($product->getData());     
    echo "<br>";
}
	
				die();*/
        return $collection;
    }
    public function getPartsHotsProducts() {
        $count = $this->getProductCount();
        $category_id = $this->getData("category_id");
        $collection = clone $this->_collection;
        $collection->clear()->getSelect()->reset(\Magento\Framework\DB\Select::WHERE)->reset(\Magento\Framework\DB\Select::ORDER)->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)->reset(\Magento\Framework\DB\Select::GROUP);

        if(!$category_id) {
            $category_id = $this->_storeManager->getStore()->getRootCategoryId();
        }
        $category = $this->categoryRepository->get($category_id);
        if(isset($category) && $category) {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_hottest_selling_item')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite() 
                ->addCategoryFilter($category)
				->addFieldToFilter('custitem_f3_hottest_selling_item', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
				
        } else {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_hottest_selling_item')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite()
				->addAttributeToFilter('custitem_f3_hottest_selling_item', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
        }

        $collection->getSelect()
                ->order('created_at','desc')
                ->limit(15);
				/*echo "<pre>";
				foreach ($collection as $product) {
    print_r($product->getData());     
    echo "<br>";
}
	
				die();*/
        return $collection;
    }

    public function getPartsNewProducts() {
        $count = $this->getProductCount();
        $category_id = $this->getData("category_id");
        $collection = clone $this->_collection;
        $collection->clear()->getSelect()->reset(\Magento\Framework\DB\Select::WHERE)->reset(\Magento\Framework\DB\Select::ORDER)->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)->reset(\Magento\Framework\DB\Select::GROUP);

        if(!$category_id) {
            $category_id = $this->_storeManager->getStore()->getRootCategoryId();
        }
        $category = $this->categoryRepository->get($category_id);
        if(isset($category) && $category) {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_new_arrival')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite() 
                ->addCategoryFilter($category)
				->addFieldToFilter('custitem_f3_new_arrival', 1)
				->addAttributeToFilter('visibility', 1)
                ->addAttributeToSort('created_at','desc');
				
        } else {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_new_arrival')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite()
				->addAttributeToFilter('custitem_f3_new_arrival', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
        }

        $collection->getSelect()
                ->order('created_at','desc')
                ->limit(15);
				/*echo "<pre>";
				foreach ($collection as $product) {
    print_r($product->getData());     
    echo "<br>";
}
	
				die();*/
        return $collection;
    }

    public function getPartsLowPriceProducts() {
        $count = $this->getProductCount();
        $category_id = $this->getData("category_id");
        $collection = clone $this->_collection;
        $collection->clear()->getSelect()->reset(\Magento\Framework\DB\Select::WHERE)->reset(\Magento\Framework\DB\Select::ORDER)->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)->reset(\Magento\Framework\DB\Select::GROUP);

        if(!$category_id) {
            $category_id = $this->_storeManager->getStore()->getRootCategoryId();
        }
        $category = $this->categoryRepository->get($category_id);
        if(isset($category) && $category) {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_low_price_item')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite() 
                ->addCategoryFilter($category)
				->addFieldToFilter('custitem_f3_low_price_item', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
				
        } else {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_low_price_item')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite()
				->addAttributeToFilter('custitem_f3_low_price_item', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
        }

        $collection->getSelect()
                ->order('created_at','desc')
                ->limit(15);
				/*echo "<pre>";
				foreach ($collection as $product) {
    print_r($product->getData());     
    echo "<br>";
}
	
				die();*/
        return $collection;
    }

    public function getPartsBackStockProducts() {
        $count = $this->getProductCount();
        $category_id = $this->getData("category_id");
        $collection = clone $this->_collection;
        $collection->clear()->getSelect()->reset(\Magento\Framework\DB\Select::WHERE)->reset(\Magento\Framework\DB\Select::ORDER)->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)->reset(\Magento\Framework\DB\Select::GROUP);

        if(!$category_id) {
            $category_id = $this->_storeManager->getStore()->getRootCategoryId();
        }
        $category = $this->categoryRepository->get($category_id);
        if(isset($category) && $category) {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_back_in_stock')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite() 
                ->addCategoryFilter($category)
				->addFieldToFilter('custitem_f3_back_in_stock', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
				
        } else {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('custitem_f3_back_in_stock')
                ->addAttributeToSelect('custitem_grade')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite()
				->addAttributeToFilter('custitem_f3_back_in_stock', 1)
				->addAttributeToFilter('visibility', 4)
                ->addAttributeToSort('created_at','desc');
        }

        $collection->getSelect()
                ->order('created_at','desc')
                ->limit(15);
				/*echo "<pre>";
				foreach ($collection as $product) {
    print_r($product->getData());     
    echo "<br>";
}
	
				die();*/
        return $collection;
    }
	  public function getProductPricetoHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null
    ) {
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');
        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product
            );
        }
        return $price;
    }
	  public function getProductDetailsHtml(\Magento\Catalog\Model\Product $product)
    {
        $renderer = $this->getDetailsRenderer($product->getTypeId());
        if ($renderer) {
            $renderer->setProduct($product);
            return $renderer->toHtml();
        }
        return '';
    }

    public function getDetailsRenderer($type = null)
    {
        if ($type === null) {
            $type = 'default';
        }
        $rendererList = $this->getDetailsRendererList();
        if ($rendererList) {
            return $rendererList->getRenderer($type, 'default');
        }
        return null;
    }

    protected function getDetailsRendererList()
    {
        return $this->getDetailsRendererListName() ? $this->getLayout()->getBlock(
            $this->getDetailsRendererListName()
        ) : $this->getChildBlock(
            'details.renderers'
        );
    }
    public function getLoadedProductCollection() {
        return $this->getProducts();
    }

    public function getPartsHotsLoadedProductCollection() {
        return $this->getPartsHotsProducts();
    }
    public function getPartsNewLoadedProductCollection() {
        return $this->getPartsNewProducts();
    }
    public function getPartsLowPriceLoadedProductCollection() {
        return $this->getPartsLowPriceProducts();
    }
    public function getPartsBackStockProductCollection() {
        return $this->getPartsBackStockProducts();
    }
    
    public function getProductCount() {
        $limit = $this->getData("product_count");
        if(!$limit)
            $limit = 10;
        return $limit;
    }
}
