<?php

namespace Smartwave\Filterproducts\Block\Home;

use Magento\Catalog\Api\CategoryRepositoryInterface;

class LatestList extends \Magento\Catalog\Block\Product\ListProduct {

    protected $_collection;

    protected $categoryRepository;

    protected $_resource;

    public function __construct(
    \Magento\Catalog\Block\Product\Context $context,
            \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
            \Magento\Catalog\Model\Layer\Resolver $layerResolver,
            CategoryRepositoryInterface $categoryRepository,
            \Magento\Framework\Url\Helper\Data $urlHelper,
            \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
            \Magento\Framework\App\ResourceConnection $resource,
            array $data = []
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->_collection = $collection;
        $this->_resource = $resource;

        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
    }

    protected function _getProductCollection() {
        return $this->getProducts();
    }
	  /* protected function _prepareLayout()
    {
		
       $this->addChild(
            'homepage.toprenderers',
            \Magento\Framework\View\Element\RendererList::class
        );

        $this->getLayout()->getBlock($this->getNameInLayout() . '.homepage.toprenderers')
            ->addChild('default', \Magento\Framework\View\Element\Template::class);

        $this->getLayout()->getBlock($this->getNameInLayout() . '.homepage.toprenderers')
            ->addChild('configurable', \Magento\Swatches\Block\Product\Renderer\Listing\Configurable::class);

        parent::_prepareLayout();
    } */
	protected function _prepareLayout()
    {
        $this->addChild(
            'details.renderers',
            \Magento\Framework\View\Element\RendererList::class
        );

        $this->getLayout()->getBlock($this->getNameInLayout() . '.details.renderers')
            ->addChild('default', \Magento\Framework\View\Element\Template::class);

        $configurableRenderer = $this->getLayout()->getBlock($this->getNameInLayout() . '.details.renderers')
            ->addChild('configurable', \Magento\Swatches\Block\Product\Renderer\Listing\Configurable::class);
        $configurableRenderer->setTemplate('Magento_Swatches::product/listing/renderer.phtml');

        parent::_prepareLayout();
    }
    public function getProducts() {
        $count = $this->getProductCount();
        $category_id = $this->getData("category_id");
        $collection = clone $this->_collection;
        $collection->clear()->getSelect()->reset(\Magento\Framework\DB\Select::WHERE)->reset(\Magento\Framework\DB\Select::ORDER)->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)->reset(\Magento\Framework\DB\Select::GROUP);
        $now = date('Y-m-d');
        if(!$category_id) {
            $category_id = $this->_storeManager->getStore()->getRootCategoryId();
        }
        $category = $this->categoryRepository->get($category_id);
        if(isset($category) && $category) {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
				->addAttributeToSelect('custitem_f3_hottest_selling_item')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite() 
                ->addCategoryFilter($category)
                
                ->addAttributeToSelect('news_from_date')
                ->addAttributeToSelect('news_to_date')
                ->addAttributeToFilter([
                    [
                        'attribute' => 'news_from_date',
                        'lteq' => date('Y-m-d G:i:s', strtotime($now)),
                        'date' => true,
                    ],
                    [
                        'attribute' => 'news_to_date',
                        'gteq' => date('Y-m-d G:i:s', strtotime($now)),
                        'date' => true,
                    ]
                ])
                ->addAttributeToSort('created_at','desc');
        } else {
            $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
				->addAttributeToSelect('custitem_f3_hottest_selling_item')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite()
                ->addAttributeToSelect('news_from_date')
                ->addAttributeToSelect('news_to_date')
                ->addAttributeToFilter([
                    [
                        'attribute' => 'news_from_date',
                        'lteq' => date('Y-m-d G:i:s', strtotime($now)),
                        'date' => true,
                    ],
                    [
                        'attribute' => 'news_to_date',
                        'gteq' => date('Y-m-d G:i:s', strtotime($now)),
                        'date' => true,
                    ]
                ])
                ->addAttributeToSort('created_at','desc');
        }

        $collection->getSelect()
                ->order('created_at','desc')
                ->limit($count);

        return $collection;
    }
	  public function getProductPricetoHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null
    ) {
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');
        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product
            );
        }
        return $price;
    }
	  public function getProductDetailsHtml(\Magento\Catalog\Model\Product $product)
    {
        $renderer = $this->getDetailsRenderer($product->getTypeId());
        if ($renderer) {
            $renderer->setProduct($product);
            return $renderer->toHtml();
        }
        return '';
    }

    public function getDetailsRenderer($type = null)
    {
        if ($type === null) {
            $type = 'default';
        }
        $rendererList = $this->getDetailsRendererList();
        if ($rendererList) {
            return $rendererList->getRenderer($type, 'default');
        }
        return null;
    }

    protected function getDetailsRendererList()
    {
        return $this->getDetailsRendererListName() ? $this->getLayout()->getBlock(
            $this->getDetailsRendererListName()
        ) : $this->getChildBlock(
            'details.renderers'
        );
    }
    public function getLoadedProductCollection() {
        return $this->getProducts();
    }

    public function getProductCount() {
        $limit = $this->getData("product_count");
        if(!$limit)
            $limit = 10;
        return $limit;
    }
}
