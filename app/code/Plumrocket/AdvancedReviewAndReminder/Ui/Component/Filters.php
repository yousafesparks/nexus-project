<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\Component;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory as ReviewCollectionFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\CollectionFactory;

class Filters extends \Magento\Ui\Component\Filters
{
    /**
     * @var CollectionFactory
     */
    private $advancedReviewCollectionFactory;

    /**
     * @var ReviewCollectionFactory
     */
    private $reiewCollectionFactory;

    /**
     * Filters constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param CollectionFactory $advancedReviewCollectionFactory
     * @param ReviewCollectionFactory $reiewCollectionFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CollectionFactory $advancedReviewCollectionFactory,
        ReviewCollectionFactory $reiewCollectionFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->advancedReviewCollectionFactory = $advancedReviewCollectionFactory;
        $this->reiewCollectionFactory = $reiewCollectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function prepare()
    {
        $config = $this->getData('config');
        $config['count_abused_reviews'] = $this->getCountAbusedReviews();
        $config['count_pending_reviews'] = $this->getCountPendingReviews();
        $this->setData('config', $config);
        return parent::prepare();
    }

    protected function getCountPendingReviews()
    {
        return $this->reiewCollectionFactory
            ->create()
            ->addFieldToFilter('status_id', \Magento\Review\Model\Review::STATUS_PENDING)
            ->count();
    }

    protected function getCountAbusedReviews()
    {
        return $this->advancedReviewCollectionFactory
            ->create()
            ->addFieldToFilter('abuse', 1)
            ->count();
    }
}
