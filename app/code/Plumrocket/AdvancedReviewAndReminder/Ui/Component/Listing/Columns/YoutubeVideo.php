<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Plumrocket\AdvancedReviewAndReminder\Helper\Review;
use Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface;

class YoutubeVideo extends Column
{
    /**
     * @var Review
     */
    private $reviewHelper;

    /**
     * Attachments constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Review $reviewHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Review $reviewHelper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->reviewHelper = $reviewHelper;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if ($item[$fieldName]) {
                    $fieldValue = $item[$fieldName];
                    $item[$fieldName . '_src'] = $this->reviewHelper->getImageUrlByYoutubeId($fieldValue);
                    $item[$fieldName . '_alt'] = __('youtube');
                    $item[$fieldName . '_orig_src']
                        = $this->reviewHelper->getVideoUrlByYoutubeId($fieldValue);
                }
            }
        }

        return $dataSource;
    }
}
