<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface;

class Attachments extends Column
{
    /**
     * @var ReviewImageFactoryInterface
     */
    protected $reviewImageFactory;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * Attachments constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ReviewImageFactoryInterface $reviewImageFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ReviewImageFactoryInterface $reviewImageFactory,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->reviewImageFactory = $reviewImageFactory;
        $this->serializer = $serializer;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');

            foreach ($dataSource['data']['items'] as & $item) {
                if ($item[$fieldName]) {
                    $images = $this->serializer->unserialize($item[$fieldName]);
                    $item[$fieldName] = [];
                    foreach ($images as $key => $image) {
                        $image = $this->reviewImageFactory->create(
                            $image['filename'],
                            ReviewImageFactoryInterface::ORIGIN_IMAGE_ID
                        );

                        $item[$fieldName][$key][$fieldName . '_src'] = $image->getUrl();
                        $item[$fieldName][$key][$fieldName . '_alt'] = $image->getFilename();
                        $item[$fieldName][$key][$fieldName . '_orig_src'] = $image->getUrl();
                    }
                }
            }
        }

        return $dataSource;
    }
}
