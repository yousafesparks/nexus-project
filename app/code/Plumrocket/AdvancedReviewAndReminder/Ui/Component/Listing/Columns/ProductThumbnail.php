<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing\Columns;

use Magento\Catalog\Helper\Image;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class ProductThumbnail extends Column
{
    /**
     * @var Image
     */
    private $imageHelper;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * ProductThumbnail constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param Image $imageHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        Image $imageHelper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->imageHelper = $imageHelper;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            $products = $this->getContext()->getDataProvider()->getConfigData()['products'];

            $dataSource['data']['items'] = array_map(function ($item) use ($products, $fieldName) {
                if (isset($products[$item['entity_pk_value']])) {
                    $product = $products[$item['entity_pk_value']];
                    $image = $this->imageHelper
                        ->init($products[$item['entity_pk_value']], 'product_listing_thumbnail');
                    $item[$fieldName . '_src'] = $image->getUrl();
                    $item[$fieldName . '_alt'] = $image->getLabel();
                    $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                        'catalog/product/edit',
                        ['id' => $product->getEntityId(), 'store' => $this->context->getRequestParam('store')]
                    );
                    $origImageHelper = $this->imageHelper->init($product, 'product_listing_thumbnail_preview');
                    $item[$fieldName . '_orig_src'] = $origImageHelper->getUrl();

                }

                return $item;
            }, $dataSource['data']['items']);
        }

        return $dataSource;
    }
}
