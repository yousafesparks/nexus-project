<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class ProductName extends Column
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * ProductName constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            $products = $this->getContext()->getDataProvider()->getConfigData()['products'];
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($products[$item['entity_pk_value']])) {
                    $item[$fieldName] = $products[$item['entity_pk_value']]->getName();
                    $item['productLink'] = $this->urlBuilder->getUrl(
                        'catalog/product/edit',
                        ['id' => $products[$item['entity_pk_value']]->getId()]
                    );
                }
            }
        }

        return $dataSource;
    }
}
