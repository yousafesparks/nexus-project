<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing;

use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Columns extends \Magento\Ui\Component\Listing\Columns
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Columns constructor.
     *
     * @param ContextInterface $context
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @inheritDoc
     */
    public function prepare()
    {
        $productIds = array_column($this->getContext()->getDataProvider()->getData()['items'], 'entity_pk_value');
        $customerIds = array_column($this->getContext()->getDataProvider()->getData()['items'], 'customer_id');
        $productIds = array_unique($productIds);
        $customerIds = array_unique($customerIds);
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('entity_id', $productIds, 'in')
            ->create();
        $products = $this->productRepository->getList($searchCriteria)->getItems();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('entity_id', $customerIds, 'in')
            ->create();
        $customers = $this->refactorCustomerItemsKey($this->customerRepository->getList($searchCriteria)->getItems());
        $configData = $this->getContext()->getDataProvider()->getConfigData();
        $configData['products'] = $products;
        $configData['customers'] = $customers;
        $this->getContext()->getDataProvider()->setConfigData($configData);
        parent::prepare();
    }

    /**
     * @param $customers
     * @return array
     */
    private function refactorCustomerItemsKey(array $customers)
    {
        $newItems = [];

        foreach ($customers as $customer) {
            $newItems[$customer->getId()] = $customer;
        }

        return $newItems;
    }
}
