<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\Frontend\DataProvider\Form\Modifier;

use Magento\Customer\Model\Context;
use Magento\Framework\ObjectManagerInterface;
use Plumrocket\AdvancedReviewAndReminder\Helper\Config\ReCaptcha;

/**
 * Create component for Magento reCAPTCHA if module is enabled
 *
 * @since 2.2.4
 */
class MagentoReCaptcha
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config\ReCaptcha
     */
    private $reCaptchaConfig;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    private $httpContext;

    /**
     * @param \Magento\Framework\ObjectManagerInterface                     $objectManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config\ReCaptcha $reCaptchaConfig
     * @param \Magento\Framework\App\Http\Context                           $httpContext
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ReCaptcha $reCaptchaConfig,
        \Magento\Framework\App\Http\Context $httpContext
    ) {
        $this->objectManager = $objectManager;
        $this->reCaptchaConfig = $reCaptchaConfig;
        $this->httpContext = $httpContext;
    }

    /**
     * Modify js layout of Form block
     *
     * @param array $jsLayout
     * @return array
     */
    public function modify(array $jsLayout): array
    {
        $jsLayout['components']['pr-review-form']['configuration']['magentoReCaptcha']['enabled'] = $this->isEnabled();
        if ($this->isEnabled()) {
            $jsLayout['components']['pr-review-form-re-captcha'] = [
                'component' => 'Magento_ReCaptchaFrontendUi/js/reCaptcha',
                'reCaptchaId' => 'pr-review-form-re-captcha',
                'enabled' => $this->isEnabled(),
                'settings' => $this->getRecaptchaSettings(),
            ];
        }
        return $jsLayout;
    }

    /**
     * @return bool
     */
    private function isEnabled(): bool
    {
        if ($this->reCaptchaConfig->isEnabledMagentoReCaptcha()) {
            if ($this->reCaptchaConfig->showOnlyForGuests()) {
                return ! $this->httpContext->getValue(Context::CONTEXT_AUTH);
            }
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getRecaptchaSettings(): array
    {
        /** @var \Magento\ReCaptchaUi\Model\UiConfigResolverInterface $uiConfigResolver*/
        $uiConfigResolver = $this->objectManager->get(\Magento\ReCaptchaUi\Model\UiConfigResolverInterface::class);
        return $this->fixForDisplayInPopup($uiConfigResolver->get('product_review'));
    }

    /**
     * Always show invisible reCaptcha inline, look better in popup
     *
     * @param array $settings
     * @return array
     */
    private function fixForDisplayInPopup(array $settings): array
    {
        if (isset($settings['rendering']['badge'])) {
            $settings['rendering']['badge'] = 'inline';
        }

        return $settings;
    }
}
