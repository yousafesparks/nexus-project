<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Ui\DataProvider\UnsubscribeReason\Form;

use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\UnsubscribeReason\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    private $loadedData = [];

    /**
     * DataProvider constructor.
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $reasonCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        CollectionFactory $reasonCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $reasonCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        $items = $this->collection->getItems();

        /** @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface $reason */
        foreach ($items as $reason) {
            $this->loadedData[$reason->getId()] = $reason->getData();
        }

        return $this->loadedData;
    }
}
