<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent;

/**
 * Util class for products and their forms in reminder
 */
class ReminderFormScripts
{
    /**
     * @param int $productId
     * @return string
     */
    public function isReviewHasBeenWritten(int $productId): string
    {
        return "'success' == reminder.product{$productId}.status";
    }

    /**
     * @param int $productId
     * @return string
     */
    public function isFormOpen(int $productId): string
    {
        return "{$productId} == reminder.form.showForProduct";
    }

    /**
     * @param int $productId
     * @return string
     */
    public function isFormClosed(int $productId): string
    {
        return "{$productId} != reminder.form.showForProduct";
    }

    /**
     * @param int $productId
     * @return string
     */
    public function openForm(int $productId): string
    {
        return "AMP.setState({reminder: {form: {showForProduct: {$productId}}}})";
    }

    /**
     * @param int $productId
     * @return string
     */
    public function showLoader(int $productId): string
    {
        return "AMP.setState({reminder: {product{$productId}: {loader: 'show' }}})";
    }

    /**
     * @param int $productId
     * @return string
     */
    public function submitSuccess(int $productId): string
    {
        return "AMP.setState({reminder: {product{$productId}: {status: 'success', loader: 'hide'}}})";
    }

    /**
     * @param int $productId
     * @return string
     */
    public function hideLoader(int $productId): string
    {
        return "AMP.setState({reminder: {product{$productId}: {loader: 'hide' }}})";
    }
}
