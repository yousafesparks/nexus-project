<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\ViewModel\Review;

use Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface as AdvancedReview;
use Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

/**
 * @since 2.0.0
 */
class ConvertToArray extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $session;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Review
     */
    private $reviewHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Catalog\Helper\Data
     */
    private $catalogHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo
     */
    private $customerLogoProvider;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface
     */
    private $getReviewSeoFriendlyUrl;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface
     */
    private $reviewImagesFactory;

    /**
     * @var \Magento\Review\Model\Rating\Option\VoteFactory
     */
    private $voteFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\IsSingleRatingInterface
     */
    private $isSingleRating;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\IsRatingEnabledInterface
     */
    private $isRatingEnabled;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\VerifiedTextGenerator
     */
    private $verifiedTextGenerator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Helpfulvote\CollectionFactory
     */
    private $helpfulVoteCollectionFactory;

    /**
     * @var null|array
     */
    private $helpfulVoteItems;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\Tags
     */
    private $tagsFilter;

    /**
     * ConvertToArray constructor.
     *
     * @param \Magento\Framework\View\Element\Context                                                 $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Review                                     $reviewHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                                     $configHelper
     * @param \Magento\Catalog\Helper\Data                                                            $catalogHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo                       $customerLogoProvider
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface              $getReviewSeoFriendlyUrl
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface        $reviewImagesFactory
     * @param \Magento\Review\Model\Rating\Option\VoteFactory                                         $voteFactory
     * @param \Magento\Store\Model\StoreManagerInterface                                              $storeManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\IsSingleRatingInterface                       $isSingleRating
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\IsRatingEnabledInterface                      $isRatingEnabled
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\VerifiedTextGenerator                $verifiedTextGenerator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Helpfulvote\CollectionFactory $helpfulVoteCollectionFactory
     * @param \Magento\Framework\Session\SessionManagerInterface                                      $customerSession
     * @param \Magento\Framework\Session\SessionManagerInterface                                      $session
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\Tags                   $tagsFilter
     * @param array                                                                                   $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Review $reviewHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Catalog\Helper\Data $catalogHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo $customerLogoProvider,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface $getReviewSeoFriendlyUrl,
        \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface $reviewImagesFactory,
        \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumrocket\AdvancedReviewAndReminder\Api\IsSingleRatingInterface $isSingleRating,
        \Plumrocket\AdvancedReviewAndReminder\Api\IsRatingEnabledInterface $isRatingEnabled,
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\VerifiedTextGenerator $verifiedTextGenerator,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Helpfulvote\CollectionFactory $helpfulVoteCollectionFactory,
        \Magento\Framework\Session\SessionManagerInterface $customerSession,
        \Magento\Framework\Session\SessionManagerInterface $session,
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\Tags $tagsFilter,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->reviewHelper = $reviewHelper;
        $this->configHelper = $configHelper;
        $this->catalogHelper = $catalogHelper;
        $this->customerLogoProvider = $customerLogoProvider;
        $this->getReviewSeoFriendlyUrl = $getReviewSeoFriendlyUrl;
        $this->reviewImagesFactory = $reviewImagesFactory;
        $this->voteFactory = $voteFactory;
        $this->storeManager = $storeManager;
        $this->isSingleRating = $isSingleRating;
        $this->isRatingEnabled = $isRatingEnabled;
        $this->verifiedTextGenerator = $verifiedTextGenerator;
        $this->helpfulVoteCollectionFactory = $helpfulVoteCollectionFactory;
        $this->customerSession = $customerSession;
        $this->session = $session;
        $this->tagsFilter = $tagsFilter;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface[] $reviews
     * @return array
     */
    public function execute(array $reviews) : array
    {
        $result = [];
        $votes = $this->getVisitorVotes($reviews);
        $customerId = $this->customerSession->getCustomerId();

        foreach ($reviews as $advancedReview) {
            $reviewData = [
                'id'               => $advancedReview->getId(),
                'detail'           => $advancedReview->getDetail(),
                'date'             => $this->reviewHelper->getFormattedDate($advancedReview->getCreatedAt()),
                'createAtTime'     => strtotime($advancedReview->getCreatedAt()),
                'ratingPercent'    => $advancedReview->getAggregateRatingPercent(),
                'rating'           => $advancedReview->getAggregateRating(),
                'nickname'         => $advancedReview->getNickname(),
                'url'              => $this->getReviewSeoFriendlyUrl->execute($advancedReview),
                'markedAsAbuse' => $customerId && $advancedReview->getAbusedCustomer() === $customerId,
            ];

            if (Option::NO !== $this->configHelper->getReviewSummaryOption()) {
                $reviewData['title'] = $advancedReview->getTitle();
            }

            if ($this->configHelper->isEnabledVerified()) {
                $reviewData['verified'] = [
                    'enable' => $advancedReview->getVerified(),
                    'text' => $this->verifiedTextGenerator->execute((int) $advancedReview->getDeliveryTime()),
                ];
            }

            if ($this->configHelper->isEnabledHelpful()) {
                $reviewData['helpfulPositive'] = $advancedReview->getHelpfulPositive();
                $reviewData['helpfulNegative'] = $advancedReview->getHelpfulNegative();
            }

            if (Option::NO !== $this->configHelper->getProsAndConsOption()) {
                $reviewData['pros'] = $advancedReview->getPros();
                $reviewData['cons'] = $advancedReview->getCons();
            }

            $reviewData['share']['enable'] = $this->configHelper->isEnabledShareButton();
            if ($reviewData['share']['enable']) {
                $reviewData['share']['facebook'] = [
                    'url' => $this->reviewHelper->getFacebookShareUrl($advancedReview),
                    'img' => $this->getViewFileUrl('Plumrocket_AdvancedReviewAndReminder::images/fb-32.png'),
                ];
                $reviewData['share']['twitter'] = [
                    'url' => $this->reviewHelper->getTwitterShareUrl($advancedReview),
                    'img' => $this->getViewFileUrl('Plumrocket_AdvancedReviewAndReminder::images/tw-32.png'),
                ];
            }

            if ($advancedReview->getAdminComment()) {
                $reviewData['adminComment'] = $this->catalogHelper->getPageTemplateProcessor()->filter(
                    $advancedReview->getAdminComment()
                );
                $reviewData['adminCommentDate'] = $this->reviewHelper->getFormattedDate(
                    $advancedReview->getAdminCommentDate()
                );
            }

            $reviewData['avatar'] = $this->customerLogoProvider->getLogoUrl($advancedReview->getCustomerId());

            if ($this->configHelper->isEnabledRecommendedToAFriend() && $recommend = $advancedReview->getRecommend()) {
                $reviewData['recommend'] = $recommend;
            }

            $reviewData['attachImages'] = $advancedReview->getAttachImage()
                ? $this->prepareImages($advancedReview)
                : [];

            if ($videoId = $advancedReview->getYoutubeId()) {
                $reviewData['videoId'] = $videoId;
            }

            if ($this->isRatingEnabled->execute() && ! $this->isSingleRating->execute()) {
                $reviewData['ratings'] = $this->prepareMultipleRatings($this->addVotes($advancedReview));
            }

            $vote = $votes[$advancedReview->getId()] ?? null;

            if (null !== $vote) {
                $reviewData['vote'] = $vote->getVote();
            }

            $reviewData = $this->tagsFilter->filter($reviewData);

            if ($this->configHelper->isAllowedFormattedTextOnFrontend()) {
                $reviewData['detail'] = nl2br($reviewData['detail']);
                if (isset($reviewData['pros'], $reviewData['cons'])) {
                    $reviewData['pros'] = nl2br($reviewData['pros']);
                    $reviewData['cons'] = nl2br($reviewData['cons']);
                }
                if (isset($reviewData['adminComment'])) {
                    $reviewData['adminComment'] = nl2br($reviewData['adminComment']);
                }
            }

            $result[] = $reviewData;
        }

        return $result;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $advancedReview
     * @return array
     */
    private function prepareImages(AdvancedReview $advancedReview) : array
    {
        $result = [];
        $attachImages = $this->reviewImagesFactory->create(
            $advancedReview,
            ReviewImageFactoryInterface::THUMBNAIL_IMAGE_ID
        );

        foreach ($attachImages as $id => $attachImage) {
            $result[$id]['thumbnail'] = $attachImage->getUrl();
            $result[$id]['orderNumber'] = $id;
        }

        $attachImages = $this->reviewImagesFactory->create(
            $advancedReview,
            ReviewImageFactoryInterface::LARGE_IMAGE_ID
        );

        foreach ($attachImages as $id => $attachImage) {
            $result[$id]['large'] = $attachImage->getUrl();
        }

        return $result;
    }

    /**
     * @param AdvancedReview|\Magento\Review\Model\Review $advancedReview
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     * @TODO Refactor it, as collection loading is slow
     */
    private function addVotes(AdvancedReview $advancedReview) : AdvancedReview
    {
        /** @var \Magento\Review\Model\ResourceModel\Rating\Option\Vote\Collection $votesCollection */
        $votesCollection = $this->voteFactory->create()->getResourceCollection()->setReviewFilter(
            $advancedReview->getId()
        )->setStoreFilter(
            $this->storeManager->getStore()->getId()
        )->addRatingInfo(
            $this->storeManager->getStore()->getId()
        )->load();
        $advancedReview->setRatingVotes($votesCollection);

        return $advancedReview;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $advancedReview
     * @return array[]
     */
    private function prepareMultipleRatings(AdvancedReview $advancedReview) : array
    {
        $ratings = [];
        foreach ($advancedReview->getRatingVotes() as $ratingVote) {
            $ratings[] = [
                'label' => $this->escapeHtml(__($ratingVote->getRatingCode())),
                'percent' => ceil($ratingVote->getPercent())
            ];
        }

        return $ratings;
    }

    /**
     * @param array $items
     * @return array|null
     */
    private function getVisitorVotes(array $items)
    {
        if ($this->configHelper->isEnabledHelpful() && $this->helpfulVoteItems === null) {
            $ids = $this->getAllIds($items);
            $customerId = $this->customerSession->getCustomerId();
            $visitorId = $this->session->getVisitorData()['visitor_id'] ?? null;

            /** @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Helpfulvote\Collection $helpfulCollection */
            $helpfulCollection = $this->helpfulVoteCollectionFactory
                ->create()
                ->addFieldToFilter('review_id', ['in' => $ids]);

            if ($customerId) {
                $helpfulCollection->addFieldToFilter('customer_id', $customerId);
            } elseif ($visitorId) {
                $helpfulCollection->addFieldToFilter('visitor_id', $visitorId);
            } else {
                return [];
            }

            $this->helpfulVoteItems = $helpfulCollection->setIdFieldName('review_id')->getItems();
        }

        return $this->helpfulVoteItems;
    }

    /**
     * @param array $items
     * @return array
     */
    private function getAllIds(array $items)
    {
        $ids = [];
        foreach ($items as $item) {
            $ids[] = $item->getId();
        }

        return $ids;
    }
}
