<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\ViewModel\Review;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Sorting as SortingSource;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\RatingFilter;

/**
 * @since 2.0.0
 */
class ConvertJsSearchCriteria
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * @var SortingSource
     */
    private $sortingSource;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * ConvertJsSearchCriteria constructor.
     *
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder      $sortOrderBuilder
     * @param ConfigHelper          $configHelper
     * @param SortingSource         $sortingSource
     * @param FilterBuilder         $filterBuilder
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        ConfigHelper $configHelper,
        SortingSource $sortingSource,
        FilterBuilder $filterBuilder
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->configHelper = $configHelper;
        $this->sortingSource = $sortingSource;
        $this->filterBuilder = $filterBuilder;
    }

    /**
     * @param array $jsSearchCriteria
     * @param int   $productId
     * @return SearchCriteria
     */
    public function execute(array $jsSearchCriteria, int $productId): SearchCriteria
    {
        $this->searchCriteriaBuilder->addFilter('entity_pk_value', $productId);
        $this->searchCriteriaBuilder->addFilter('status_id', \Magento\Review\Model\Review::STATUS_APPROVED);

        $this->applyFilters($this->searchCriteriaBuilder, $jsSearchCriteria);
        $this->applySort($this->searchCriteriaBuilder, $jsSearchCriteria['sort'] ?? '');
        $this->applyPagination(
            $this->searchCriteriaBuilder,
            (int) ($jsSearchCriteria['page']['number'] ?? 1),
            (int) ($jsSearchCriteria['page']['size'] ?? 10)
        );

        return $this->searchCriteriaBuilder->create();
    }

    /**
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param array                 $jsSearchCriteria
     * @return SearchCriteriaBuilder
     */
    public function applyFilters(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        array $jsSearchCriteria
    ): SearchCriteriaBuilder {
        $generalFilter = array_filter($jsSearchCriteria['general'] ?? []);
        if ($generalFilter) {
            if (isset($generalFilter['verified'])) {
                $searchCriteriaBuilder->addFilter('verified', 1);
            }
            if (isset($generalFilter['with_media'])) {
                $searchCriteriaBuilder->addFilter('attach_image', true, 'notnull');
            }
        }

        $ratingFilter = array_filter($jsSearchCriteria['rating'] ?? []);
        if ($ratingFilter) {
            $rating = array_keys($ratingFilter)[0];

            switch (true) {
                case is_numeric($rating):
                    $floor = $rating * 20;
                    $ceil = ($rating + 1) * 20;

                    $searchCriteriaBuilder->addFilter('percent', $floor, 'gteq');
                    $searchCriteriaBuilder->addFilter('percent', $ceil, 'lt');
                    break;
                case $rating === RatingFilter::POSITIVE_RATING_KEY:
                    $searchCriteriaBuilder->addFilter('percent', ConfigHelper::POSITIVE_RATING_BOUND, 'gteq');
                    break;
                case $rating === RatingFilter::CRITICAL_RATING_KEY:
                    $searchCriteriaBuilder->addFilter('percent', ConfigHelper::POSITIVE_RATING_BOUND, 'lt');
                    break;
                default:
                    // Don't filter if value unknown
            }
        }

        $searchFilter = array_filter($jsSearchCriteria['search'] ?? []);
        if ($searchFilter) {
            $searchString = array_keys($searchFilter)[0];
            $searchFilters = [];
            foreach ($this->configHelper->getEnabledForSearchFields() as $searchField) {
                $searchFilters[] = $this->filterBuilder
                    ->setField($searchField)
                    ->setValue("%$searchString%")
                    ->setConditionType('like')
                    ->create();
            }
            $searchCriteriaBuilder->addFilters($searchFilters);
        }

        return $searchCriteriaBuilder;
    }

    /**
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param string                $sort
     * @return SearchCriteriaBuilder
     */
    public function applySort(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        string $sort
    ): SearchCriteriaBuilder {
        if (! array_key_exists($sort, $this->sortingSource->toOptionHash())) {
            $sort = $this->configHelper->getDefaultReviewSorting();
        }

        switch ($sort) {
            case SortingSource::NEWEST:
                $sortOrder = $this->sortOrderBuilder
                    ->setField('created_at')
                    ->setDescendingDirection()
                    ->create();
                break;
            case SortingSource::OLDEST:
                $sortOrder = $this->sortOrderBuilder
                    ->setField('created_at')
                    ->setAscendingDirection()
                    ->create();
                break;
            case SortingSource::HELPFUL:
                $sortOrder = $this->sortOrderBuilder
                    ->setField('helpful_subtracted')
                    ->setDescendingDirection()
                    ->create();
                break;
            case SortingSource::HIGHEST_RATED:
                $sortOrder = $this->sortOrderBuilder
                    ->setField('aggregate_rating_percent')
                    ->setDescendingDirection()
                    ->create();
                break;
            case SortingSource::LOWEST_RATED:
                $sortOrder = $this->sortOrderBuilder
                    ->setField('aggregate_rating_percent')
                    ->setAscendingDirection()
                    ->create();
                break;
        }

        if (isset($sortOrder)) {
            $searchCriteriaBuilder->addSortOrder($sortOrder);
        }

        return $searchCriteriaBuilder;
    }

    /**
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param int                   $currentPage
     * @param int                   $pageSize
     * @return SearchCriteriaBuilder
     */
    private function applyPagination(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        int $currentPage,
        int $pageSize
    ): SearchCriteriaBuilder {
        $searchCriteriaBuilder->setPageSize($pageSize);
        $searchCriteriaBuilder->setCurrentPage($currentPage);
        return $searchCriteriaBuilder;
    }
}
