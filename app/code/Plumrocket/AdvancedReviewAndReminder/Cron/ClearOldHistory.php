<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Cron;

class ClearOldHistory
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder
     */
    private $reminder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * ClearOldHistory constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                        $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder       $reminder
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig              $reminderConfig
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder $reminder,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
    ) {
        $this->dataHelper = $dataHelper;
        $this->reminder = $reminder;
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * Delete old review reminders
     */
    public function execute()
    {
        if ($this->dataHelper->moduleEnabled() && $this->reminderConfig->getReviewReminderEraseHistoryTime()) {
            $timeTo = strftime('%F', time() - $this->reminderConfig->getReviewReminderEraseHistoryTime());

            $this->reminder->removeOld($timeTo);
        }
    }
}
