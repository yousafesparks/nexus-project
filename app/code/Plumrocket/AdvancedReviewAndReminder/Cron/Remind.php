<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Cron;

use Plumrocket\AdvancedReviewAndReminder\Model\Reminder;

class Remind
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $reminderCollectionFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $emailFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $storeManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * Remind constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                                    $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory                             $emailFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                                          $dateTime
     * @param \Magento\Store\Model\StoreManagerInterface                                           $storeManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig                          $reminderConfig
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory $emailFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
    ) {
        $this->dataHelper = $dataHelper;
        $this->emailFactory = $emailFactory;
        $this->reminderCollectionFactory = $reminderCollectionFactory;
        $this->dateTime = $dateTime;
        $this->storeManager = $storeManager;
        $this->reminderConfig = $reminderConfig;
    }

    /**
     *
     * @return void
     */
    public function execute()
    {
        $storeIds = [];

        foreach ($this->storeManager->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $storeId = $store->getId();
                    if ($this->reminderConfig->autoSending($storeId)
                        && $this->dataHelper->moduleEnabled($storeId)
                    ) {
                        $storeIds[] = $storeId;
                    }
                }
            }
        }

        if (empty($storeIds)) {
            return;
        }

        $currentDate = $this->dateTime->gmtDate('Y-m-d');
        $reminderCollection = $this->reminderCollectionFactory->create()
            ->addFieldToFilter('store_id', ['in' => $storeIds])
            ->addFieldToFilter('scheduled_at', ['lteq' => $currentDate])
            ->addFieldToFilter('status', Reminder::ADVANCEDRAR_STATUS_PENDING);

        foreach ($reminderCollection as $reminder) {
            $emailModel = $this->emailFactory->create();
            $emailModel->processReminder($reminder);
        }
    }
}
