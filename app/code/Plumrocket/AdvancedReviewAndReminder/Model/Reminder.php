<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Plumrocket\AdvancedReviewAndReminder\Helper\Data as DataHelper;
use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Sales\Model\OrderFactory;
use Magento\Review\Model\ReviewFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\SubscriberStatus;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\VisitorType;

/**
 * @method $this setOrderId(string $orderId)
 * @method $this setReminder(string $reminderType)
 * @method $this setCreatedAt(string $gmtDate)
 * @method $this setScheduledAt(string $gmtDate)
 * @method $this setStoreId(string $storeId)
 * @method $this setSecretKey(string $secretKey)
 * @method $this setStatus(string $reminderStatus)
 * @method $this getReminder()
 * @method \Magento\Catalog\Model\Product getProduct()
 * @method getStatus()
 * @method getReviewCount()
 * @method setReviewCount($count)
 * @method getBody()
 * @method getOrderId()
 */
class Reminder extends \Magento\Framework\Model\AbstractModel
{
    const ADVANCEDRAR_FIRST_REMINDER = 1;
    const ADVANCEDRAR_SECOND_REMINDER = 2;
    const ADVANCEDRAR_THIRD_REMINDER = 3;

    const ADVANCEDRAR_STATUS_PENDING = 1;
    const ADVANCEDRAR_STATUS_SENT = 2;
    const ADVANCEDRAR_STATUS_CANCELED = 3;
    const ADVANCEDRAR_STATUS_ERROR = 4;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface
     */
    protected $reminderSubscriberRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory
     */
    protected $reminderSubscriberFactory;

    /**
     * @var null
     */
    private $permissionHelper = null;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var ReviewFactory
     */
    private $reviewFactory;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var DataHelper
     */
    private $dataHelper;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var ReminderdoneFactory
     */
    private $reminderdoneFactory;

    /**
     * @var ResourceModel\Reminder
     */
    private $reminderResource;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface
     */
    private $guestSecretDataProvider;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * Reminder constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReminderdoneFactory $reminderdoneFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder $reminderResource
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface $guestSecretDataProvider
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory $reminderSubscriberFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        ReminderdoneFactory $reminderdoneFactory,
        ResourceModel\Reminder $reminderResource,
        DataHelper $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper,
        OrderFactory $orderFactory,
        ReviewFactory $reviewFactory,
        DateTime $date,
        ResourceConnection $resourceConnection,
        Context $context,
        Registry $registry,
        \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface $guestSecretDataProvider,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig,
        \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository,
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory $reminderSubscriberFactory,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        $this->permissionHelper = $permissionHelper;
        $this->orderFactory = $orderFactory;
        $this->reviewFactory = $reviewFactory;
        $this->reminderdoneFactory = $reminderdoneFactory;
        $this->date = $date;
        $this->resourceConnection = $resourceConnection;
        $this->reminderResource = $reminderResource;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
        $this->guestSecretDataProvider = $guestSecretDataProvider;
        $this->orderRepository = $orderRepository;
        $this->reminderConfig = $reminderConfig;
        $this->reminderSubscriberRepository = $reminderSubscriberRepository;
        $this->reminderSubscriberFactory = $reminderSubscriberFactory;
    }

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder::class);
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return [
            self::ADVANCEDRAR_STATUS_PENDING => __('Pending'),
            self::ADVANCEDRAR_STATUS_SENT => __('Sent'),
            self::ADVANCEDRAR_STATUS_CANCELED => __('Canceled'),
            self::ADVANCEDRAR_STATUS_ERROR => __('Error'),
        ];
    }

    /**
     * @return array
     */
    public function getReminderLabels()
    {
        return [
            self::ADVANCEDRAR_FIRST_REMINDER => __('First Reminder'),
            self::ADVANCEDRAR_SECOND_REMINDER => __('Second Reminder'),
            self::ADVANCEDRAR_THIRD_REMINDER => __('Third Reminder'),
        ];
    }

    /**
     * @param int                            $customerId
     * @param \Magento\Review\Model\Review   $review
     * @param \Magento\Catalog\Model\Product $product
     */
    public function updateReminder($customerId, $review, $product)
    {
        if (! $customerId) {
            $secretData = $this->guestSecretDataProvider->getData();
            if ($secretData->getKey() && $secretData->getReminderId()) {
                $this->reminderResource->load($this, $secretData->getReminderId());

                if ($this->getStatus() == self::ADVANCEDRAR_STATUS_PENDING) {
                    $review->setVerified(1);
                    $this->setStatus(self::ADVANCEDRAR_STATUS_CANCELED);
                    $this->reminderResource->save($this);
                }
            }
        } else {
            $this->setProduct($product);
            $orderCollection = $this->getCustomerOrdersWithPresetProduct($customerId);

            if ($orderCollection->getSize()) {
                $review->setVerified(1);
            }

            foreach ($orderCollection as $_order) {
                if ($this->isSameProduct($_order)
                    && (! $this->reminderConfig->sendIfNotAllProductsReviewed()
                    || $this->canCloseReminder($_order->getId(), $customerId))
                ) {
                    /**
                     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\Collection $reminderCollection
                     */
                    $reminderCollection = $this->getCollection()
                       ->addFieldToFilter('order_id', $_order->getId())
                       ->addFieldToFilter('status', self::ADVANCEDRAR_STATUS_PENDING)
                       ->setPageSize(1);

                    if ($reminderCollection->getSize()) {
                        $reminder = $reminderCollection->getFirstItem();

                        $reminder->setStatus(self::ADVANCEDRAR_STATUS_CANCELED);
                        $this->reminderResource->save($reminder);
                    }
                }
            }
        }
    }

    /**
     * get customer orders with preset product
     * @param  int $customerId
     * @param  int $orderId
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getCustomerOrdersWithPresetProduct($customerId = null, $orderId = null)
    {
        if (!$customerId && !$orderId) {
            return null;
        }

        $orderCollection = $this->orderFactory->create()->getCollection();

        if ($customerId) {
            $orderCollection->addFieldToFilter('customer_id', $customerId);
        }

        if ($orderId) {
            $orderCollection->addFieldToFilter('entity_id', $orderId);
        }

        $orderCollection->getSelect()
            ->join(
                ['sfoi' => $this->resourceConnection->getTableName('sales_order_item')],
                'main_table.entity_id = sfoi.order_id AND sfoi.product_id = '.$this->getProduct()->getId(),
                ['product_id', 'product_options']
            )
            ->group('main_table.entity_id');

        return $orderCollection;
    }

    /**
     * Checking is current product bought in preset order
     *
     * @param \Magento\Sales\Model\Order  $order
     * @return boolean
     */
    public function isSameProduct($order)
    {
        $productId = $this->getProduct()->getId();

        foreach ($order->getAllItems() as $item) {
            if ($item->getProductId() == $productId) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @throws \Exception
     */
    public function createReminder($order)
    {
        if (! $order || ! $order->getId()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Order is required to create reminder'));
        }

        if ($this->permissionHelper->canCreateReminder($order)) {
            $this->setOrderId($order->getId())
                ->setReminder(self::ADVANCEDRAR_FIRST_REMINDER)
                ->setCreatedAt($this->date->gmtDate())
                ->setScheduledAt($this->dataHelper->getFirstReminderDelay())
                ->setStoreId($order->getStoreId())
                ->setSecretKey($this->permissionHelper->generateSecretKey($order))
                ->setStatus(self::ADVANCEDRAR_STATUS_PENDING);

            try {
                $this->reminderResource->save($this);

                $this->reminderdoneFactory->create()
                    ->setReminder(self::ADVANCEDRAR_FIRST_REMINDER)
                    ->setOrderId($order->getId())
                    ->save();

                $this->createSubscriber($order);
            } catch (\Exception $e) {
                $this->_logger->debug($e->getMessage());
            }
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param null|string|int            $customerId
     * @return bool
     */
    public function canCloseReminder($order, $customerId = null)
    {
        if (! is_object($order)) {
            $order = $this->orderFactory->create()->load($order);
        }

        if (! $order->getId()) {
            return false;
        }

        if (! $customerId) {
            $customerId = $order->getCustomerId();
        }

        $orderProducts = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $orderProducts[] = $item->getProductId();
        }
        $orderProducts = array_unique($orderProducts);

        $review = $this->reviewFactory->create()->getCollection()
            ->addStoreFilter($order->getStoreId())
            ->addFieldToFilter(
                'entity_pk_value',
                [
                    'in' => [$orderProducts],
                ]
            )
            ->addFieldToFilter('customer_id', $customerId);

        $review->getSelect()->group('entity_pk_value');

        $this->setReviewCount(count($review));

        return count($orderProducts) === $this->getReviewCount();
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface|mixed|null
     */
    public function getOrder()
    {
        if ($order = $this->_getData('order')) {
            return $order;
        }

        if ($orderId = $this->getOrderId()) {
            try {
                return $this->orderRepository->get($orderId);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                return null;
            }
        }

        return null;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return $this
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function createSubscriber(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        try {
            $customerEmail = $order->getCustomerEmail();

            if ($customerEmail) {
                $this->reminderSubscriberRepository->getByEmail($customerEmail);
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $subscriber = $this->reminderSubscriberFactory->create();
            $subscriber->setData([
                SubscriberInterface::EMAIL => $customerEmail,
                SubscriberInterface::CUSTOMER_ID => $order->getCustomerId(),
                SubscriberInterface::WEBSITE => $order->getStore()->getWebsiteId(),
                SubscriberInterface::STATUS => SubscriberStatus::SUBSCRIBED,
                SubscriberInterface::VISITOR_TYPE => $order->getCustomerIsGuest()
                    ? VisitorType::GUEST
                    : VisitorType::CUSTOMER
            ]);

            $this->reminderSubscriberRepository->save($subscriber);
        }

        return $this;
    }
}
