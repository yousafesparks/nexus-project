<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class CustomerGroupsPermission extends Base
{
    /**
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * CustomerGroupsPermission constructor.
     *
     * @param \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder   $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->groupRepository = $groupRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     */
    public function getCustomerGroup()
    {
        $optionArray = [];

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('customer_group_id', 0, 'neq')
            ->create();

        $customerGroups = $this->groupRepository->getList($searchCriteria);

        /** @var \Magento\Customer\Model\Data\Group $group */
        foreach ($customerGroups->getItems() as $group) {
            $optionArray[$group->getId()] = $group->getCode();
        }

        return $optionArray;
    }

    /**
     * @return array
     */
    public function toOptionHash()
    {
        return $this->getCustomerGroup();
    }
}
