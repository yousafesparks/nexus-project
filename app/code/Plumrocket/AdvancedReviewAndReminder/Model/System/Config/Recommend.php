<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class Recommend extends Base
{
    const NOT_SPECIFIED = 0;
    const NO = 1;
    const YES = 2;

    /**
     * @return \Magento\Framework\Phrase[]
     */
    public function toOptionHash() : array
    {
        return [
            self::NOT_SPECIFIED => __('Not specified'),
            self::NO  => __('No'),
            self::YES => __('Yes'),
        ];
    }
}
