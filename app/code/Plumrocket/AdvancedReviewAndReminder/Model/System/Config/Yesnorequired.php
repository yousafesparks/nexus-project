<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class Yesnorequired extends Base
{
    const REQUIRED = 'required';
    const YES = 'yes';
    const NO = 'no';

    /**
     * @return \Magento\Framework\Phrase[]
     */
    public function toOptionHash()
    {
        return [
            self::REQUIRED => __('Yes (required field)'),
            self::YES      => __('Yes (optional field)'),
            self::NO       => __('No'),
        ];
    }
}
