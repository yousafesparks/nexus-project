<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class Sorting extends Base
{
    const NEWEST = 'newest';
    const OLDEST = 'oldest';
    const HELPFUL = 'helpful';
    const HIGHEST_RATED = 'high_rating';
    const LOWEST_RATED = 'low_rating';

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * Sorting constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     */
    public function __construct(\Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper)
    {
        $this->configHelper = $configHelper;
    }

    /**
     * @return string
     */
    public function getDefaultSorting() : string
    {
        return $this->configHelper->getDefaultReviewSorting();
    }

    /**
     * @return \Magento\Framework\Phrase[]
     */
    public function toOptionHash() : array
    {
        return [
            self::NEWEST => __('Newest'),
            self::OLDEST => __('Oldest'),
            self::HELPFUL => __('Most helpful'),
            self::HIGHEST_RATED => __('Highest rated'),
            self::LOWEST_RATED => __('Lowest rated'),
        ];
    }
}
