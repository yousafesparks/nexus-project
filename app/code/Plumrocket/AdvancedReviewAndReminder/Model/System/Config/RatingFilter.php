<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class RatingFilter extends Base
{
    const ALL_STARS = 'all';
    const FIVE_STAR_ONLY = 5;
    const FOUR_STAR_ONLY = 4;
    const THREE_STAR_ONLY = 3;
    const TWO_STAR_ONLY = 2;
    const ONE_STAR_ONLY = 1;
    const POSITIVE_RATING_KEY = 'positive';
    const CRITICAL_RATING_KEY = 'critical';

    /**
     * @return \Magento\Framework\Phrase[]
     */
    public function toOptionHash(): array
    {
        return [
            self::ALL_STARS => __('All stars'),
            self::FIVE_STAR_ONLY => __('5 star only'),
            self::FOUR_STAR_ONLY => __('4 star only'),
            self::THREE_STAR_ONLY => __('3 star only'),
            self::TWO_STAR_ONLY => __('2 star only'),
            self::ONE_STAR_ONLY => __('1 star only'),
            self::POSITIVE_RATING_KEY => __('All positive'),
            self::CRITICAL_RATING_KEY => __('All critical'),
        ];
    }
}
