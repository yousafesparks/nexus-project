<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class Orderstatus extends Base
{
    /**
     * @var \Magento\Sales\Model\Order\StatusFactory
     */
    private $statusFactory;

    /**
     * Orderstatus constructor.
     *
     * @param \Magento\Sales\Model\Order\StatusFactory $statusFactory
     */
    public function __construct(
        \Magento\Sales\Model\Order\StatusFactory $statusFactory
    ) {
        $this->statusFactory = $statusFactory;
    }

    /**
     * @return array
     */
    public function toOptionHash()
    {
        $statuses = $this->statusFactory
            ->create()
            ->getResourceCollection()
            ->getData();

        $arr = [];
        foreach ($statuses as $status) {
            $arr[$status['status']] = __($status['label']);
        }

        return $arr;
    }
}
