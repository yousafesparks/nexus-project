<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

use Magento\Framework\View\Asset\Repository;

/**
 * Class ReviewPageStructure
 * @since 2.0.0
 */
class ReviewPageStructure extends Base
{
    const ONE_PAGE_DESIGN = 'one_page';
    const TWO_PAGE_DESIGN = 'two_page';

    /**
     * @var Repository
     */
    private $viewAssetRepository;

    /**
     * ReviewPageStructure constructor.
     *
     * @param Repository $viewAssetRepository
     */
    public function __construct(Repository $viewAssetRepository)
    {
        $this->viewAssetRepository = $viewAssetRepository;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            self::ONE_PAGE_DESIGN => __('One page design'),
            self::TWO_PAGE_DESIGN => __('Two page design'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('One page design'),
                'value' => self::ONE_PAGE_DESIGN,
                'image' => $this->viewAssetRepository->getUrl(
                    'Plumrocket_AdvancedReviewAndReminder::images/one-page-design.png'
                ),
            ],
            [
                'label' => __('Two page design'),
                'value' => self::TWO_PAGE_DESIGN,
                'image' => $this->viewAssetRepository->getUrl(
                    'Plumrocket_AdvancedReviewAndReminder::images/two-page-design.png'
                ),
            ],
        ];
    }
}
