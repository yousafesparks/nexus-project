<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class AbstractPermission extends Base
{
    const PERMISSION_ANY = 0;
    const PERMISSION_ONLY_BOUGHT = 1;
    const PERMISSION_CANNOT_WRITE = 3;
}
