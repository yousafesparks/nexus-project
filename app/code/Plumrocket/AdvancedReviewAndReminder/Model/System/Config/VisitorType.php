<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class VisitorType extends Base
{
    const CUSTOMER = 1;
    const GUEST = 0;

    public function toOptionHash()
    {
        return [
            self::CUSTOMER => __('Customer'),
            self::GUEST => __('Guest'),
        ];
    }
}
