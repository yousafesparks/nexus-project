<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class Displayonproductpage extends Base
{
    public function toOptionHash()
    {
        return [
            1 => __('All reviews'),
            0 => __('Several reviews + link "See all reviews"'),
        ];
    }
}
