<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

/**
 * Class ChangeUrlParams
 * @since 2.0.0
 */
class ChangeUrlParams extends Base
{
    const ADD_PARAMS = 1;
    const DONT_CHANGE = 0;

    /**
     * @return array
     */
    public function toOptionHash() : array
    {
        return [
            self::ADD_PARAMS  => __('Add GET parameters'),
            self::DONT_CHANGE => __('Do not change page URL'),
        ];
    }
}
