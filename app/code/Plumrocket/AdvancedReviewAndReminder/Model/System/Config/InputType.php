<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class InputType extends Base
{
    const RADIO = 1;
    const TEXTAREA = 2;

    public function toOptionHash()
    {
        return [
            self::RADIO => __('Radio Button'),
            self::TEXTAREA => __('Textarea'),
        ];
    }
}
