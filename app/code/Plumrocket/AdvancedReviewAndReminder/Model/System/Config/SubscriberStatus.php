<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class SubscriberStatus extends Base
{
    const SUBSCRIBED = 1;
    const UNSUBSCRIBED = 0;

    public function toOptionHash()
    {
        return [
            self::SUBSCRIBED => __('Subscribed'),
            self::UNSUBSCRIBED => __('Unsubscribed'),
        ];
    }
}
