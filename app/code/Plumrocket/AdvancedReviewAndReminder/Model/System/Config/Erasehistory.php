<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class Erasehistory extends Base
{
    public function toOptionHash()
    {
        return [
            3 => __('Older than 3 months'),
            6 => __('Older than 6 months'),
            12 => __('Older than 1 year'),
            24 => __('Older than 2 years'),
            0 => __('Never'),
        ];
    }
}
