<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class Reviewstatus extends Base
{
    const REVIEW_STATUS_APPROVED = 'approved';
    const REVIEW_STATUS_PENDING = 'pending';
    const REVIEW_STATUS_RATING_APPROVED = 'rating_approved';

    /**
     * @return array
     */
    public function toOptionArray() : array
    {
        return [
            self::REVIEW_STATUS_APPROVED => __('Yes (Reviews with any rating)'),
            self::REVIEW_STATUS_RATING_APPROVED => __('Yes (Only positive reviews with rating 4 or 5 stars)'),
            self::REVIEW_STATUS_PENDING => __('No (I will approve each review manually)'),
        ];
    }
}
