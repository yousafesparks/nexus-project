<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

class CustomerPermission extends AbstractPermission
{
    /**
     * @return array
     */
    public function toOptionHash()
    {
        return [
            self::PERMISSION_ANY            => __('Yes (Enabled for any registered customer)'),
            self::PERMISSION_ONLY_BOUGHT    => __('Yes (Enabled for registered customers who bought the product)'),
            self::PERMISSION_CANNOT_WRITE   => __('No (Disabled for all registered customers)'),
        ];
    }
}
