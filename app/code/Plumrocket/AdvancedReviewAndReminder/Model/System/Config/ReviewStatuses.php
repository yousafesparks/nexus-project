<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

use Magento\Review\Helper\Data;

class ReviewStatuses extends Base
{
    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * ReviewStatuses constructor.
     *
     * @param Data $dataHelper
     */
    public function __construct(Data $dataHelper)
    {
        $this->dataHelper = $dataHelper;
    }

    /**
     * @return array
     */
    public function toOptionHash()
    {
        return $this->dataHelper->getReviewStatuses();
    }
}
