<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\System\Config;

/**
 * Class EnabledFor
 * @since 2.0.0
 */
class EnabledFor extends Base
{
    /**
     * Use numeric values for compatibility with old config
     */
    const CUSTOMERS_AND_GUESTS = 1;
    const REGISTERED_CUSTOMERS_ONLY = 0;

    /**
     * @return array
     */
    public function toOptionHash() : array
    {
        return [
            self::CUSTOMERS_AND_GUESTS      => __('All registered customers and guests'),
            self::REGISTERED_CUSTOMERS_ONLY => __('Registered customers only'),
        ];
    }
}
