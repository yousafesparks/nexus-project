<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Token;

class AutoLoginType implements \Plumrocket\Token\Api\TypeInterface
{
    const KEY = 'arar_auto_login';

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * PostReviewFromEmailType constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
     */
    public function __construct(\Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig)
    {
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * @return string
     */
    public function getKey() : string
    {
        return self::KEY;
    }

    /**
     * @return int
     */
    public function getLifetime() : int
    {
        return strtotime("{$this->getLifetimeDays()} day", 0);
    }

    /**
     * @return int
     */
    public function getLifetimeDays() : int
    {
        return $this->reminderConfig->getAutoLoginLifetime();
    }
}
