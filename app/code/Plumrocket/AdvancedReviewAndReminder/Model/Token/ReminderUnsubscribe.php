<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Token;

class ReminderUnsubscribe implements \Plumrocket\Token\Api\TypeInterface
{
    const KEY = 'arar_reminder_unsubscribe';

    /**
     * @inheritDoc
     */
    public function getKey(): string
    {
        return self::KEY;
    }

    /**
     * @inheritDoc
     */
    public function getLifetime(): int
    {
        return strtotime("{$this->getLifetimeDays()} day", 0);
    }

    /**
     * @inheritDoc
     */
    public function getLifetimeDays(): int
    {
        return 356;
    }
}
