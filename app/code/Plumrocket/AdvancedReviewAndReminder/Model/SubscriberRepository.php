<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface;

class SubscriberRepository implements SubscriberRepositoryInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory
     */
    protected $reminderSubscriberFactory;

    /**
     * @var ResourceModel\Subscriber
     */
    protected $resource;

    /**
     * @var ResourceModel\Subscriber\CollectionFactory
     */
    protected $subscriberCollectionFactory;

    /**
     * @var \Magento\Framework\Api\SearchResults
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * SubscriberRepository constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory $reminderSubscriberFactory
     * @param ResourceModel\Subscriber $resource
     * @param ResourceModel\Subscriber\CollectionFactory $subscriberCollectionFactory
     * @param \Magento\Framework\Api\SearchResults $searchResultsFactory
     * @param \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory $reminderSubscriberFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Subscriber $resource,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Subscriber\CollectionFactory $subscriberCollectionFactory,
        \Magento\Framework\Api\SearchResultsFactory $searchResultsFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
    ) {
        $this->reminderSubscriberFactory = $reminderSubscriberFactory;
        $this->resource = $resource;
        $this->subscriberCollectionFactory = $subscriberCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(SubscriberInterface $subscriber): SubscriberInterface
    {
        try {
            $this->resource->save($subscriber);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $subscriber;
    }

    /**
     * @inheritDoc
     */
    public function getById($id): SubscriberInterface
    {
        $subscriber = $this->reminderSubscriberFactory->create();
        $this->resource->load($subscriber, $id);

        if (!$subscriber->getId()) {
            throw new NoSuchEntityException(__('The subscriber with the "%1" ID doesn\'t exist.', $id));
        }

        return $subscriber;
    }

    /**
     * @inheritDoc
     */
    public function getByEmail($email): SubscriberInterface
    {
        $subscriber = $this->reminderSubscriberFactory->create();
        $this->resource->load($subscriber, $email, SubscriberInterface::EMAIL);

        if (!$subscriber->getId()) {
            throw new NoSuchEntityException(__('The subscribed customer with the "%1" email doesn\'t exist.', $email));
        }

        return $subscriber;
    }

    /**
     * @inheritDoc
     */
    public function delete(SubscriberInterface $subscriber): bool
    {
        try {
            $this->resource->delete($subscriber);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id): bool
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var ResourceModel\Subscriber\Collection $collection */
        $collection = $this->subscriberCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
