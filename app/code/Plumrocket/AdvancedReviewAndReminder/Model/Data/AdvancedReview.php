<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Data;

use Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface;

class AdvancedReview extends \Magento\Review\Model\Review implements AdvancedReviewInterface
{
    /**
     * @return \Magento\Review\Model\Review
     */
    protected function _afterLoad()
    {
        $this->initRating();
        return parent::_afterLoad();
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId() : int
    {
        return (int) $this->getData('customer_id');
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId(int $customerId) : AdvancedReviewInterface
    {
        return $this->setData('customer_id', $customerId);
    }

    /**
     * @inheritDoc
     */
    public function getProductId() : int
    {
        return (int) $this->getData('entity_pk_value');
    }

    /**
     * @inheritDoc
     */
    public function setProductId(int $productId) : AdvancedReviewInterface
    {
        return $this->setData('entity_pk_value', $productId);
    }

    /**
     * @inheritDoc
     */
    public function getNickname() : string
    {
        return (string) $this->getData('nickname');
    }

    /**
     * @inheritDoc
     */
    public function setNickname(string $nickname) : AdvancedReviewInterface
    {
        return $this->setData('nickname', $nickname);
    }

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return (string) $this->getData('title');
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title) : AdvancedReviewInterface
    {
        return $this->setData('title', $title);
    }

    /**
     * @inheritDoc
     */
    public function getDetail() : string
    {
        return (string) $this->getData('detail');
    }

    /**
     * @inheritDoc
     */
    public function setDetail(string $detail) : AdvancedReviewInterface
    {
        return $this->setData('detail', $detail);
    }

    /**
     * @inheritDoc
     */
    public function getPros() : string
    {
        return (string) $this->getData('pros');
    }

    /**
     * @inheritDoc
     */
    public function setPros(string $pros) : AdvancedReviewInterface
    {
        return $this->setData('pros', $pros);
    }

    /**
     * @inheritDoc
     */
    public function getCons() : string
    {
        return (string) $this->getData('cons');
    }

    /**
     * @inheritDoc
     */
    public function setCons(string $cons) : AdvancedReviewInterface
    {
        return $this->setData('cons', $cons);
    }

    /**
     * @inheritDoc
     */
    public function isVerified() : bool
    {
        return $this->getVerified();
    }

    /**
     * @inheritDoc
     */
    public function getVerified() : bool
    {
        return (bool) $this->getData('verified');
    }

    /**
     * @inheritDoc
     */
    public function setVerified(string $verified) : AdvancedReviewInterface
    {
        return $this->setData('verified', $verified);
    }

    /**
     * @param $deliveryTime
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setDeliveryTime($deliveryTime) : AdvancedReviewInterface
    {
        return $this->setData('delivery_time', $deliveryTime);
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRating() : int
    {
        return (int) $this->getData('aggregate_rating');
    }

    /**
     * @inheritDoc
     */
    public function setAggregateRating(int $aggregateRating) : AdvancedReviewInterface
    {
        return $this->setData('aggregate_rating', $aggregateRating);
    }

    /**
     * @inheritDoc
     */
    public function getAggregateRatingPercent() : int
    {
        return (int) $this->getData('aggregate_rating_percent');
    }

    /**
     * @inheritDoc
     */
    public function setAggregateRatingPercent(int $aggregateRatingPercent) : AdvancedReviewInterface
    {
        return $this->setData('aggregate_rating_percent', $aggregateRatingPercent);
    }

    /**
     * @inheritDoc
     */
    public function getHelpfulTotal() : int
    {
        return $this->getHelpfulPositive() - $this->getHelpfulNegative();
    }

    /**
     * @inheritDoc
     */
    public function getHelpfulPositive() : int
    {
        return (int) $this->getData('helpful_positive');
    }

    /**
     * @inheritDoc
     */
    public function setHelpfulPositive(string $helpfulPositive) : AdvancedReviewInterface
    {
        return $this->setData('helpful_positive', $helpfulPositive);
    }

    /**
     * @inheritDoc
     */
    public function getHelpfulNegative() : int
    {
        return (int) $this->getData('helpful_negative');
    }

    /**
     * @inheritDoc
     */
    public function setHelpfulNegative(string $helpfulNegative) : AdvancedReviewInterface
    {
        return $this->setData('helpful_negative', $helpfulNegative);
    }

    /**
     * @inheritDoc
     */
    public function getAdminComment() : string
    {
        return (string) $this->getData('admin_comment');
    }

    /**
     * @inheritDoc
     */
    public function setAdminComment(string $adminComment) : AdvancedReviewInterface
    {
        return $this->setData('admin_comment', $adminComment);
    }

    /**
     * @inheritDoc
     */
    public function getAdminCommentDate() : string
    {
        return (string) $this->getData('admin_comment_date');
    }

    /**
     * @inheritDoc
     */
    public function setAdminCommentDate(string $adminCommentDate) : AdvancedReviewInterface
    {
        return $this->setData('admin_comment_date', $adminCommentDate);
    }

    /**
     * @inheritDoc
     */
    public function getAttachImage() : array
    {
        return $this->getData('attach_image') ? json_decode($this->getData('attach_image'), true) : [];
    }

    /**
     * @inheritDoc
     */
    public function setAttachImage(string $attachImage) : AdvancedReviewInterface
    {
        return $this->setData('attach_image', $attachImage);
    }

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function initRating() : AdvancedReviewInterface
    {
        return $this->setAggregateRating((int) floor($this->getAggregateRatingPercent() / 20));
    }
}
