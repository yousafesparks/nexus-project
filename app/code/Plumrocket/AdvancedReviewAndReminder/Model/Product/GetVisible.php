<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Product;

use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\Framework\Exception\NoSuchEntityException;

class GetVisible
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * GetVisible constructor.
     *
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Store\Model\StoreManagerInterface      $storeManager
     * @param \Magento\Framework\App\RequestInterface         $request
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * Load product model with data by passed id.
     * Return false if product was not loaded or has incorrect status.
     *
     * @param int      $productId
     * @param int|null $storeId
     * @return bool|CatalogProduct|\Magento\Catalog\Api\Data\ProductInterface
     */
    public function execute(int $productId, int $storeId = null)
    {
        if (! $productId) {
            return false;
        }

        try {
            $product = $this->productRepository->getById($productId);
            $websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();

            if (! in_array($websiteId, $product->getWebsiteIds(), false)) {
                return false;
            }

            if (! $product->isVisibleInCatalog() || ! $product->isVisibleInSiteVisibility()) {
                return false;
            }
        } catch (NoSuchEntityException $noEntityException) {
            return false;
        }

        return $product;
    }
}
