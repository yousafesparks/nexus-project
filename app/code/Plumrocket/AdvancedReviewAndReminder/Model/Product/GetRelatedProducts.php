<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Product;

/**
 * Class GetRelatedProducts
 *
 * @since 1.2.0
 */
class GetRelatedProducts implements \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsRelatedProductsInterface
{
    /**
     * @var array
     */
    private $relatedByProducts = [];

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * GetProducts constructor.
     *
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder    $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param \Magento\Catalog\Model\Product[] $products
     * @return \Magento\Catalog\Model\Product[]
     */
    public function execute(array $products) : array
    {
        $productsIds = [];
        foreach ($products as $product) {
            $productsIds[] = $product->getId();
        }
        $productsIds = array_unique($productsIds);
        sort($productsIds);
        $localCacheKey = implode('|', $productsIds);

        if (! isset($this->relatedByProducts[$localCacheKey])) {
            $relatedProducts = [];
            $relatedIds = [];
            foreach ($products as $product) {
                $relatedIds[] = $product->getRelatedProductIds();
            }

            $relatedDifferentFormOrderProductsIds = array_diff(
                array_unique(array_merge(...$relatedIds)),
                $productsIds
            );

            if ($relatedDifferentFormOrderProductsIds) {
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter('entity_id', $relatedDifferentFormOrderProductsIds, 'in')
                    ->create();
                $searchResult = $this->productRepository->getList($searchCriteria);

                $relatedProducts = $searchResult->getItems();
            }

            $this->relatedByProducts[$localCacheKey] = $relatedProducts;
        }

        return $this->relatedByProducts[$localCacheKey];
    }
}
