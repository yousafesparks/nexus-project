<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface;

class UnsubscribeReasonRepository implements UnsubscribeReasonRepositoryInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterfaceFactory
     */
    protected $reasonFactory;

    /**
     * @var ResourceModel\UnsubscribeReason
     */
    protected $resource;

    /**
     * @var ResourceModel\UnsubscribeReason\CollectionFactory
     */
    protected $reasonCollectionFactory;

    /**
     * @var \Magento\Framework\Api\SearchResults
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * UnsubscribeReasonRepository constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterfaceFactory $reasonFactory
     * @param ResourceModel\UnsubscribeReason $resource
     * @param ResourceModel\UnsubscribeReason\CollectionFactory $reasonCollectionFactory
     * @param \Magento\Framework\Api\SearchResultsFactory $searchResultsFactory
     * @param \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterfaceFactory $reasonFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\UnsubscribeReason $resource,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\UnsubscribeReason\CollectionFactory $reasonCollectionFactory,
        \Magento\Framework\Api\SearchResultsFactory $searchResultsFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
    ) {
        $this->reasonFactory = $reasonFactory;
        $this->resource = $resource;
        $this->subscriberCollectionFactory = $reasonCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(UnsubscribeReasonInterface $reason): UnsubscribeReasonInterface
    {
        try {
            $this->resource->save($reason);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $reason;
    }

    /**
     * @inheritDoc
     */
    public function getById($id): UnsubscribeReasonInterface
    {
        $reason = $this->reasonFactory->create();
        $this->resource->load($reason, $id);

        if (!$reason->getId()) {
            throw new NoSuchEntityException(__('The subscriber with the "%1" ID doesn\'t exist.', $id));
        }

        return $reason;
    }

    /**
     * @inheritDoc
     */
    public function delete(UnsubscribeReasonInterface $reason): bool
    {
        try {
            $this->resource->delete($reason);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id): bool
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var ResourceModel\UnsubscribeReason\Collection $collection */
        $collection = $this->subscriberCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
