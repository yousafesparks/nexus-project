<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Provider;

use Magento\Framework\View\Asset\Repository;

class CustomerLogo
{
    /**
     * @var string[]
     */
    private $logoUrl = [];

    /**
     * @var int
     */
    private $customerId = 0;

    /**
     * @var Repository
     */
    private $viewAssetRepository;

    /**
     * CustomerLogo constructor.
     *
     * @param Repository $viewAssetRepository
     */
    public function __construct(Repository $viewAssetRepository)
    {
        $this->viewAssetRepository = $viewAssetRepository;
    }

    /**
     * @return int
     */
    public function getProcessCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return string
     */
    public function getLogoUrl($customerId)
    {
        $this->customerId = (int)$customerId;

        if (! isset($this->logoUrl[$customerId])) {
            $this->logoUrl[$customerId] = $this->generateLogoUrl($customerId);
        }

        $this->customerId = 0;

        return $this->logoUrl[$customerId];
    }

    /**
     * Will rewrite by social login pro
     *
     * For old magento versions, witch don't pass before param in after plugin
     * we create getProcessCustomerId() method
     *
     * @param int $customerId
     * @return string
     */
    public function generateLogoUrl($customerId)
    {
        return $this->viewAssetRepository->getUrl('Plumrocket_AdvancedReviewAndReminder::images/profile-picture.svg');
    }
}
