<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Provider\IsCheckRequired\Frontend;

use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;

class PostReview
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \MSP\ReCaptcha\Model\IsCheckRequired
     */
    private $isCheckRequiredModel;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config\ReCaptcha
     */
    private $reCaptchaConfig;

    /**
     * PostReview constructor.
     *
     * @param \Magento\Customer\Model\Session                                            $customerSession
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider $independenceObjectProvider
     * @param string                                                                     $area
     * @param string                                                                     $enableConfigFlag
     * @param null                                                                       $requireRequestParam
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider $independenceObjectProvider,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config\ReCaptcha $reCaptchaConfig,
        $area = \Magento\Framework\App\Area::AREA_FRONTEND,
        $enableConfigFlag = ConfigHelper::XML_PATH_ENABLED_LEAVE_REVIEW_GUEST_RECAPTCHA,
        $requireRequestParam = null
    ) {
        $this->customerSession = $customerSession;
        $this->isCheckRequiredModel = $independenceObjectProvider->createIsCheckRequiredModel(
            $area,
            $enableConfigFlag,
            $requireRequestParam
        );
        $this->reCaptchaConfig = $reCaptchaConfig;
    }

    /**
     * @return bool
     */
    public function execute()
    {
        if (! $this->isCheckRequiredModel || $this->isRegisterCustomer()) {
            return false;
        }

        return $this->isCheckRequiredModel->execute();
    }

    /**
     * @return bool
     */
    private function isRegisterCustomer()
    {
        return (bool)$this->customerSession->getCustomerId();
    }

    /**
     * Check if need validate magento reCaptcha
     *
     * @return bool
     * @throws \Magento\Framework\Exception\InputException
     */
    public function executeForMagentoReCaptcha(): bool
    {
        if ($this->reCaptchaConfig->isEnabledMagentoReCaptcha()) {
            if ($this->reCaptchaConfig->showOnlyForGuests()) {
                return ! $this->isRegisterCustomer();
            }
            return true;
        }
        return false;
    }
}
