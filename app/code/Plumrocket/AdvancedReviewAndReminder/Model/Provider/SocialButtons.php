<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Provider;

class SocialButtons
{
    /**
     * Will rewrite by Social Login Pro or Social Login Free
     *
     * @return array
     */
    public function getList()
    {
        return [];
    }
}
