<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Magento\Framework\Model\AbstractModel;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface;

class Subscriber extends AbstractModel implements SubscriberInterface
{
    protected $_idFieldName = 'subscriber_id';

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(ResourceModel\Subscriber::class);
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function getVisitorType(): int
    {
        return (int) $this->getData(self::VISITOR_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function getStatus(): int
    {
        return (int) $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function getReason(): string
    {
        return (string) $this->getData(self::REASON);
    }

    /**
     * @inheritDoc
     */
    public function getReasonLabel(): string
    {
        return (string) $this->getData(self::REASON_LABEL);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function getWebsiteId(): int
    {
        return (int) $this->getData(self::WEBSITE);
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): string
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId(): int
    {
        return (int) $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEmail(string $email): SubscriberInterface
    {
        $this->setData(self::EMAIL, $email);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setVisitorType(int $type): SubscriberInterface
    {
        $this->setData(self::VISITOR_TYPE, $type);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setStatus(int $status): SubscriberInterface
    {
        $this->setData(self::STATUS, $status);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setReason(string $reason): SubscriberInterface
    {
        $this->setData(self::REASON, $reason);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setReasonLabel(string $reasonLabel): SubscriberInterface
    {
        $this->setData(self::REASON_LABEL, $reasonLabel);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(string $createdAt): SubscriberInterface
    {
        $this->setData(self::CREATED_AT, $createdAt);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setWebsiteId(int $websiteId): SubscriberInterface
    {
        $this->setData(self::WEBSITE, $websiteId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt(string $updatedAt): SubscriberInterface
    {
        $this->setData(self::UPDATED_AT, $updatedAt);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId(int $customerId): SubscriberInterface
    {
        $this->setData(self::CUSTOMER_ID, $customerId);
        return $this;
    }
}
