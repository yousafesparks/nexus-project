<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\MSP;

class IndependenceObjectProvider
{
    /** @codingStandardsIgnoreFile */

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    private $moduleManager;

    /**
     * IndependenceClassProvider constructor.
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Module\Manager         $moduleManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->objectManager = $objectManager;
        $this->moduleManager = $moduleManager;
    }

    /**
     * @return \MSP\ReCaptcha\Model\Config|false
     */
    public function getReCaptchaConfig()
    {
        return $this->get('\MSP\ReCaptcha\Model\Config');
    }

    /**
     * @return \MSP\ReCaptcha\Model\Config|false
     */
    public function createIsCheckRequiredModel($area, $enableConfigFlag, $requireRequestParam)
    {
        return  $this->moduleManager->isEnabled('MSP_ReCaptcha')
            ? $this->objectManager->create(
                '\MSP\ReCaptcha\Model\IsCheckRequired',
                [
                    'area'                => $area,
                    'enableConfigFlag'    => $enableConfigFlag,
                    'requireRequestParam' => $requireRequestParam,
                ]
            )
            : false;
    }

    /**
     * @return false|\MSP\ReCaptcha\Model\Provider\ResponseProviderInterface
     */
    public function getResponseProvider()
    {
        return $this->get('\MSP\ReCaptcha\Model\Provider\ResponseProviderInterface');
    }

    /**
     * @return false|\MSP\ReCaptcha\Api\ValidateInterface
     */
    public function getValidate()
    {
        return $this->get('\MSP\ReCaptcha\Api\ValidateInterface');
    }

    /**
     * @return false|\MSP\ReCaptcha\Model\Provider\Failure\AjaxResponseFailure
     */
    public function getFailureProvider()
    {
        return $this->get('\MSP\ReCaptcha\Model\Provider\Failure\AjaxResponseFailure');
    }

    /**
     * @return false|\MSP\ReCaptcha\Model\LayoutSettings
     */
    public function getLayoutSettings()
    {
        return $this->get('\MSP\ReCaptcha\Model\LayoutSettings');
    }

    /**
     * @param string $className
     * @return bool|mixed
     */
    private function get($className)
    {
        return $this->moduleManager->isEnabled('MSP_ReCaptcha')
            ? $this->objectManager->get($className)
            : false;
    }
}
