<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Visitor;

/**
 * @method $this setHelpfulNegative($getHelpfulNegative)
 * @method $this setHelpfulPositive($getHelpfulPositive)
 * @method $this setReviewId(int $reviewId)
 * @method $this setCustomerId(int $customerId)
 * @method $this setCurrentVote(int $curVote)
 *
 * @method int|null getHelpfulPositive()
 * @method int|null getHelpfulNegative()
 */
class Helpfulvote extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var Visitor
     */
    private $customerVisitor;

    /**
     * Helpfulvote constructor.
     *
     * @param AdvancedreviewFactory $advancedreviewFactory
     * @param Session               $customerSession
     * @param Visitor               $customerVisitor
     * @param Context               $context
     * @param Registry              $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null       $resourceCollection
     * @param array                 $data
     */
    public function __construct(
        AdvancedreviewFactory $advancedreviewFactory,
        Session $customerSession,
        Visitor $customerVisitor,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->customerSession = $customerSession;
        $this->customerVisitor = $customerVisitor;

        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    public function _construct()
    {
        parent::_construct();
        $this->_init(\Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Helpfulvote::class);
    }

    public function saveHelpfulVote()
    {
        $reviewId = $this->getReviewId();
        $currentVote = $this->getCurrentVote();
        $customerId = $this->customerSession->getCustomerId();
        $visitorId = $this->_getVisitorId();
        $helpfulVoteCollection = $this->getCollection()
            ->addFieldToFilter('review_id', $reviewId)
            ->setPageSize(1);

        if ($customerId) {
            $helpfulVoteCollection->addFieldToFilter('customer_id', $customerId);
        } else {
            $helpfulVoteCollection->addFieldToFilter('visitor_id', $visitorId);
        }

        $helpfulVote = $helpfulVoteCollection->getFirstItem();

        if ($helpfulVote->getId()) {
            if ($currentVote == $helpfulVote->getVote()) {
                $helpfulVote->delete()->setVote(null);
                $positive = $currentVote ? -1 : 0;
                $negative = $currentVote ? 0 : -1;
            } else {
                $positive = $currentVote ? +1 : -1;
                $negative = $currentVote ? -1 : +1;
                $helpfulVote->setVote($currentVote)->save();
            }
        } else {
            list($positive, $negative) = [$currentVote, (int)!$currentVote];
            $helpfulVote->setReviewId($reviewId)
                ->setVote($currentVote)
                ->setCustomerId($customerId)
                ->setVisitorId($visitorId)
                ->save();
        }

        /**
         * @var \Plumrocket\AdvancedReviewAndReminder\Model\Advancedreview $advancedReview
         */
        $advancedReview = $this->advancedreviewFactory->create()->loadByReview($reviewId);
        $advancedReview->setHelpfulPositive($advancedReview->getHelpfulPositive() + $positive);
        $advancedReview->setHelpfulNegative($advancedReview->getHelpfulNegative() + $negative);
        $advancedReview->setReviewId($reviewId)->save();
        $this->setHelpfulNegative($advancedReview->getHelpfulNegative());
        $this->setHelpfulPositive($advancedReview->getHelpfulPositive());
        $this->setVote($helpfulVote->getVote());

        return $this;
    }

    protected function _getVisitorId()
    {
        $visitorId = $this->customerVisitor->getId();
        return $visitorId ? $visitorId : null;
    }
}
