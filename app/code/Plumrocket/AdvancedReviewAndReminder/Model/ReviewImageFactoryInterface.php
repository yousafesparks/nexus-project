<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Plumrocket\AdvancedReviewAndReminder\Model\Review\Image as ReviewImage;

/**
 * Interface ReviewImageFactoryInterface
 *
 * @since 2.0.0
 */
interface ReviewImageFactoryInterface
{
    /**
     * Constants what represent built in ids of images
     * You can find sizes in etc/view.xml
     */
    const ORIGIN_IMAGE_ID = 'origin';
    const THUMBNAIL_IMAGE_ID = 'item_thumbnail';
    const LARGE_IMAGE_ID = 'large';

    /**
     * Create resized images by file name and image type
     *
     * @param string $fileName
     * @param string $imageId
     * @return ReviewImage
     */
    public function create(
        string $fileName,
        string $imageId
    ) : ReviewImage;
}
