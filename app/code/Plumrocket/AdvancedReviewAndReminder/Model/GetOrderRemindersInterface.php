<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

/**
 * Interface GetOrdersRemindersInterface
 *
 * @since 1.2.0
 */
interface GetOrderRemindersInterface
{
    /**
     * Retrieve all reminders for order sorted by scheduled at
     *
     * @param \Magento\Sales\Model\Order $order
     * @param bool                       $forceReload
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\Reminder[]
     */
    public function execute(\Magento\Sales\Model\Order $order, bool $forceReload = false) : array;
}
