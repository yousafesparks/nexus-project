<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionSecretDataContainerInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product\OrderCollectionFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\AbstractPermission;

class BoughtProductValidator implements \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface
{
    /**
     * @var OrderCollectionFactory
     */
    private $ararOrderCollectionFactory;

    /**
     * @var ResourceModel\Reminder\CollectionFactory
     */
    private $reminderCollectionFactory;

    /**
     * @var array
     */
    private $customerCache = [];

    /**
     * @var array
     */
    private $guestCache = [];

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface
     */
    private $guestSecretDataProvider;

    /**
     * BoughtProductValidator constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product\OrderCollectionFactory $orderCollectionFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory     $reminderCollectionFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                                      $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface               $guestSecretDataProvider
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        OrderCollectionFactory $orderCollectionFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface $guestSecretDataProvider
    ) {
        $this->ararOrderCollectionFactory = $orderCollectionFactory;
        $this->reminderCollectionFactory = $reminderCollectionFactory;
        $this->configHelper = $configHelper;
        $this->guestSecretDataProvider = $guestSecretDataProvider;
    }

    /**
     * @param int $productId
     * @param int $customerId
     * @return bool
     */
    public function validate(int $productId, int $customerId)
    {
        if ($customerId
            && AbstractPermission::PERMISSION_ONLY_BOUGHT === $this->configHelper->getCustomerPermission()
        ) {
            return $this->isCustomerBoughtProduct(
                $productId,
                $customerId
            );
        }

        if (! $customerId
            && AbstractPermission::PERMISSION_ONLY_BOUGHT === $this->configHelper->getGuestPermission()
        ) {
            return $this->isGuestBoughtProduct(
                $productId,
                $this->guestSecretDataProvider->getData()
            );
        }

        return true;
    }

    /**
     * @param $productId
     * @param $customerId
     * @return bool
     */
    public function isCustomerBoughtProduct($productId, $customerId)
    {
        if (! $customerId || ! $productId) {
            throw new \InvalidArgumentException('Customer ID or Product ID not specified');
        }

        $cacheKey = $this->getCustomerCacheKey($productId, $customerId);

        if (! isset($this->customerCache[$cacheKey])) {
            /**
             * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product\OrderCollection $orderCollection
             */
            $orderCollection = $this->ararOrderCollectionFactory->create();

            $orderCollection->addFieldToFilter('customer_id', $customerId)
                            ->addFieldToFilter(
                                'state',
                                [
                                    'nin' => [
                                        \Magento\Sales\Model\Order::STATE_CANCELED,
                                        \Magento\Sales\Model\Order::STATE_NEW
                                    ]
                                ]
                            )
                            ->addProductFilter($productId, false);

            $orderCollection->setPageSize(1);

            $this->customerCache[$cacheKey] = (bool)$orderCollection->getSize();
        }

        return $this->customerCache[$cacheKey];
    }

    /**
     * @param                                        $productId
     * @param PermissionSecretDataContainerInterface $secret
     * @return bool
     */
    public function isGuestBoughtProduct($productId, PermissionSecretDataContainerInterface $secret)
    {
        $cacheKey = $this->getGuestCacheKey($productId, $secret);

        if (! isset($this->guestCache[$cacheKey])) {
            $reminder = $this->reminderCollectionFactory->create()
                ->addFieldToFilter('secret_key', $secret->getKey())
                ->addFieldToFilter('id', $secret->getReminderId())
                ->setPageSize(1)
                ->getFirstItem();

            if (! $reminder->getId() || ! $reminder->getOrderId()) {
                $this->guestCache[$cacheKey] = false;
            } else {
                $orderCollection = $this->ararOrderCollectionFactory->create();

                $orderCollection->addFieldToFilter('entity_id', $reminder->getOrderId());
                $orderCollection->addProductFilter($productId, false);
                $orderCollection->setPageSize(1);
                $this->guestCache[$cacheKey] = (bool)$orderCollection->getSize();
            }
        }

        return $this->guestCache[$cacheKey];
    }

    /**
     * @param int                                    $productId
     * @param PermissionSecretDataContainerInterface $secret
     * @return string
     */
    private function getGuestCacheKey($productId, PermissionSecretDataContainerInterface $secret)
    {
        return implode('_', [$secret->getKey(), $secret->getReminderId(), $productId]);
    }

    /**
     * @param int $productId
     * @param int $customerId
     * @return string
     */
    private function getCustomerCacheKey($productId, $customerId)
    {
        return implode('_', [$customerId, $productId]);
    }
}
