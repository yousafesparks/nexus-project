<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

class AdminNotificationSenderComposite implements AdminNotificationSenderInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var AdminNotificationSenderInterface[]
     */
    private $adminNotificationSenders;

    /**
     * AdminNotificationSenderComposite constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper
     * @param array                                             $adminNotificationSenders
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        array $adminNotificationSenders = []
    ) {
        $this->dataHelper = $dataHelper;
        $this->adminNotificationSenders = $adminNotificationSenders;
    }

    /**
     * @param \Magento\Review\Model\Review               $review
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return $this
     */
    public function sendNotificationsOfPendingReview(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product
    ) {
        if (! $this->dataHelper->moduleEnabled()) {
            return $this;
        }

        foreach ($this->adminNotificationSenders as $adminNotificationSender) {
            $adminNotificationSender->sendNotificationsOfPendingReview($review, $product);
        }

        return $this;
    }

    /**
     * @param \Magento\Review\Model\Review               $review
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param int                                        $customerId
     * @return $this
     */
    public function sendNotificationsOfAbuse(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $customerId
    ) {
        if (! $this->dataHelper->moduleEnabled()) {
            return $this;
        }

        foreach ($this->adminNotificationSenders as $adminNotificationSender) {
            $adminNotificationSender->sendNotificationsOfAbuse($review, $product, $customerId);
        }

        return $this;
    }
}
