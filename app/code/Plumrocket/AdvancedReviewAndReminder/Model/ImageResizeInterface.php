<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

/**
 * Interface ImageResizeInterface
 *
 * @since 2.0.0
 */
interface ImageResizeInterface
{
    /**
     * Resize image by sizes, support both sizes and only one
     *
     * @param string $image          image file name
     * @param int    $width          can be 0
     * @param int    $height         can be 0
     * @param string $path           path after pub/media/
     * @param string $additionalPath allow add/change/remove sub folder
     * @return bool|string
     */
    public function execute(string $image, int $width, int $height, string $path, string $additionalPath = 'resized');
}
