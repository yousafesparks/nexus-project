<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Plumrocket\AdvancedReviewAndReminder\Helper\Data as DataHelper;
use Plumrocket\AdvancedReviewAndReminder\Model\Token\AutoLoginType;

/**
 * Class AutoLoginManager
 * @since 1.2.0
 */
class AutoLoginManager implements \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
{
    /**
     * @var array
     */
    private $customersSecret = [];

    /**
     * @var \Magento\Framework\Url
     */
    private $frontUrlBuilder;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\App\DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\KeyFactory
     */
    private $keyFactory;

    /**
     * @var \Plumrocket\Token\Api\GenerateForCustomerInterface
     */
    private $generateTokenForCustomer;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Plumrocket\Token\Api\CustomerHashValidatorInterface
     */
    private $customerTokeHashValidator;

    /**
     * @var \Plumrocket\Token\Api\CustomerRepositoryInterface
     */
    private $customerTokenRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * AutoLoginManager constructor.
     *
     * @param \Magento\Framework\Url                                      $frontUrlBuilder
     * @param \Magento\Customer\Model\Session                             $customerSession
     * @param \Magento\Customer\Api\CustomerRepositoryInterface           $customerRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                 $dateTime
     * @param \Magento\Store\Model\StoreManagerInterface                  $storeManager
     * @param \Magento\Framework\App\DeploymentConfig                     $deploymentConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\KeyFactory      $keyFactory
     * @param \Plumrocket\Token\Api\GenerateForCustomerInterface          $generateTokenForCustomer
     * @param \Psr\Log\LoggerInterface                                    $logger
     * @param \Plumrocket\Token\Api\CustomerHashValidatorInterface        $customerTokeHashValidator
     * @param \Plumrocket\Token\Api\CustomerRepositoryInterface           $customerTokenRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
     */
    public function __construct(
        \Magento\Framework\Url $frontUrlBuilder,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\DeploymentConfig $deploymentConfig,
        \Plumrocket\AdvancedReviewAndReminder\Model\KeyFactory $keyFactory,
        \Plumrocket\Token\Api\GenerateForCustomerInterface $generateTokenForCustomer,
        \Psr\Log\LoggerInterface $logger,
        \Plumrocket\Token\Api\CustomerHashValidatorInterface $customerTokeHashValidator,
        \Plumrocket\Token\Api\CustomerRepositoryInterface $customerTokenRepository,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
    ) {
        $this->frontUrlBuilder = $frontUrlBuilder;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->dateTime = $dateTime;
        $this->deploymentConfig = $deploymentConfig;
        $this->storeManager = $storeManager;
        $this->keyFactory = $keyFactory;
        $this->generateTokenForCustomer = $generateTokenForCustomer;
        $this->logger = $logger;
        $this->customerTokeHashValidator = $customerTokeHashValidator;
        $this->customerTokenRepository = $customerTokenRepository;
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * @param string                                                    $route
     * @param array                                                     $params
     * @param \Magento\Customer\Model\Customer|int|null                 $customer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder|null $reminder
     * @return string
     */
    public function getUrl($route = '', $params = [], $customer = null, $reminder = null) : string
    {
        if ($this->reminderConfig->isEnabledAutoLogin()
            && ($secret = $this->getSecretByParams($customer, $reminder))
        ) {
            $params[DataHelper::LOGIN_KEY_QUERY_PARAM_NAME] = $secret;
        }

        return $this->frontUrlBuilder->getUrl($route, $params);
    }

    /**
     * @param string                                                    $url
     * @param \Magento\Customer\Model\Customer|int|null                 $customer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder|null $reminder
     * @return string
     */
    public function addSecretToUrl($url, $customer = null, $reminder = null) : string
    {
        if ($this->reminderConfig->isEnabledAutoLogin()
            && ($secret = $this->getSecretByParams($customer, $reminder))
        ) {
            $p1 = strpos($url, '?');
            $p2 = strpos($url, '#');

            $kv = DataHelper::LOGIN_KEY_QUERY_PARAM_NAME . '=' . $secret;

            if ($p1 !== false && $p2 !== false) {
                if ($p1 < $p2) {
                    $url = preg_replace('/\?/', '?' . '&', $url, 1);
                } else {
                    $url = preg_replace('/#/', '?' . $kv . '#', $url, 1);
                }
            } elseif ($p1 !== false) {
                $url = preg_replace('/\?/', '?' . $kv . '&', $url, 1);
            } elseif ($p2 !== false) {
                $url = preg_replace('/#/', '?' . $kv . '#', $url, 1);
            } else {
                $url .= '?' . $kv;
            }
        }

        return $url;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function loginByKey(string $key) : bool
    {
        if ($key && $this->reminderConfig->isEnabledAutoLogin()) {
            try {
                $isValidKey = $this->customerTokeHashValidator->validate($key);
                $token = $this->customerTokenRepository->get($key);
                $customerId = $token->getCustomerId();
            } catch (\Magento\Framework\Exception\ValidatorException $e) {
                /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Key $access */
                $access = $this->keyFactory->create();
                $access->removeOld()->load($key);
                $isValidKey = $access->getActiveTo() > $this->dateTime->date();
                $customerId = (int) $access->getCustomerId();
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                return false;
            }

            if (! $isValidKey || ! $customerId) {
                return false;
            }

            try {
                $customer = $this->customerRepository->getById($customerId);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                return false;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                return false;
            }

            try {
                $customer->setWebsiteId($this->storeManager->getStore()->getWebsiteId());
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                return false;
            }

            if ($this->isLoggedIn() && $customerId !== (int) $this->customerSession->getCustomer()->getId()) {
                $this->customerSession->logout()->regenerateId();
            }

            $this->customerSession->setCustomerId($customer->getId());
            if ($this->customerSession->loginById($customer->getId())) {
                $this->customerSession->regenerateId();
            }

            return true;
        }

        return false;
    }

    /**
     * @param int|\Magento\Customer\Model\Customer $customer
     * @return string
     */
    public function getSecret($customer) : string
    {
        $customerId = is_object($customer) ? (int) $customer->getId() : (int) $customer;

        if (! isset($this->customersSecret[$customerId])) {
            try {
                $hash = $this->generateTokenForCustomer->execute($customerId, '', AutoLoginType::KEY)->getHash();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->logger->critical($e->getMessage());
                $hash = '';
            }

            $this->customersSecret[$customerId] = $hash;
        }

        return $this->customersSecret[$customerId];
    }

    /**
     * @param \Magento\Customer\Model\Customer|int|null                 $customer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder|null $reminder
     * @return string
     */
    private function getSecretByParams($customer = null, $reminder = null) : string
    {
        if ($customer) {
            $secret = $this->getSecret($customer);
        } elseif ($reminder && $reminder->getId()) {
            $secret = $reminder->getSecretKey() . '_' . $reminder->getId();
        } else {
            $secret = '';
        }

        return $secret;
    }

    /**
     * @return bool
     */
    private function isLoggedIn() : bool
    {
        return (
            $this->deploymentConfig->isAvailable()
            && $this->customerSession->isLoggedIn()
            && ($customer = $this->customerSession->getCustomer())
            && $customer->getId()
        );
    }
}
