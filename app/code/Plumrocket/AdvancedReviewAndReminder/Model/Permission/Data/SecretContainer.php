<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Permission\Data;

class SecretContainer extends \Magento\Framework\Api\AbstractSimpleObject implements
    \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionSecretDataContainerInterface
{
    const KEY = 'key';
    const REMINDER_ID = 'reminder_id';

    /**
     * @return mixed|null
     */
    public function getKey()
    {
        return $this->_get(self::KEY);
    }

    /**
     * @param $key
     * @return SecretContainer
     */
    public function setKey($key)
    {
        return $this->setData(self::KEY, $key);
    }

    /**
     * @return mixed|null
     */
    public function getReminderId()
    {
        return $this->_get(self::REMINDER_ID);
    }

    /**
     * @param int $reminderId
     * @return SecretContainer
     */
    public function setReminderId($reminderId)
    {
        return $this->setData(self::REMINDER_ID, $reminderId);
    }
}
