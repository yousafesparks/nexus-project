<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Permission\Data;

use Plumrocket\AdvancedReviewAndReminder\Helper\Permission;

class Container extends \Magento\Framework\Api\AbstractSimpleObject implements
    \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionDataContainerInterface
{
    const REASON = 'reason';
    const CAN_SHOW_FORM_FOR_GUEST = 'can_show_form_for_guest';
    const IS_ALLOW = 'is_allow';

    /**
     * @return $this
     */
    public function resetData()
    {
        $this->setIsAllow(null);
        $this->setReason(Permission::REASON_NO_REASON);

        return $this;
    }

    /**
     * @return bool
     */
    public function isDefinedAllow()
    {
        return null !== $this->_get(self::IS_ALLOW);
    }

    /**
     * @return bool
     */
    public function isAllowed()
    {
        return (bool)$this->_get(self::IS_ALLOW);
    }

    /**
     * @param bool $allow
     * @return Container
     */
    public function setIsAllow($allow)
    {
        return $this->setData(self::IS_ALLOW, $allow);
    }

    /**
     * @return bool
     */
    public function canShowFormForGuest()
    {
        return (bool)$this->_get(self::CAN_SHOW_FORM_FOR_GUEST);
    }

    /**
     * @param bool $allow
     * @return Container
     */
    public function setCanShowFormForGuest($allow)
    {
        return $this->setData(self::CAN_SHOW_FORM_FOR_GUEST, $allow);
    }

    /**
     * @param int $reason
     * @return Container
     */
    public function setReason($reason)
    {
        return $this->setData(self::REASON, $reason);
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->_get(self::REASON);
    }
}
