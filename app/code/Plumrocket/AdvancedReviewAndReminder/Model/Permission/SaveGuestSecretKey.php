<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2021 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Permission;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;

/**
 * Save Guest Secret key in cookie to use it after redirect.
 *
 * @since 2.2.8
 */
class SaveGuestSecretKey
{

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     * @param \Magento\Framework\Stdlib\CookieManagerInterface       $cookieManager
     * @param \Magento\Framework\App\RequestInterface                $request
     */
    public function __construct(
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManager,
        RequestInterface $request
    ) {
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieManager = $cookieManager;
        $this->request = $request;
    }

    public function execute()
    {
        if ($this->request->getParam(GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER)) {
            $guestSecretKey = $this->request->getParam(
                GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER
            );

            $cookieMetadata = $this->cookieMetadataFactory->createSensitiveCookieMetadata()->setPath('/');
            $this->cookieManager->setSensitiveCookie(
                GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER,
                $guestSecretKey,
                $cookieMetadata
            );
        }
    }
}
