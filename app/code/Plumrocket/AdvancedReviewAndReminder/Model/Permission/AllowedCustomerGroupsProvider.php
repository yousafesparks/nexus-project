<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Permission;

use Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface;

class AllowedCustomerGroupsProvider implements AllowedCustomerGroupsProviderInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var null|array
     */
    private $trueAllowedCustomerGroups;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\CustomerGroupsPermission
     */
    private $customerGroupsPermission;

    /**
     * CustomerGroupsProvider constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                                $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\CustomerGroupsPermission $customerGroupsPermission
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\CustomerGroupsPermission $customerGroupsPermission
    ) {
        $this->configHelper = $configHelper;
        $this->customerGroupsPermission = $customerGroupsPermission;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->getTrueAllowedCustomerGroups();
    }

    /**
     * @return bool
     */
    public function isAllGroupsAllowed()
    {
        $allowedGroups = $this->getTrueAllowedCustomerGroups();

        return ! empty($allowedGroups)
            && count($allowedGroups) === count($this->customerGroupsPermission->toOptionHash());
    }

    /**
     * @return array
     */
    private function getTrueAllowedCustomerGroups()
    {
        if (null === $this->trueAllowedCustomerGroups) {
            $groupsFromConfig = explode(',', $this->configHelper->getCustomerGroupsPermission());

            // Config may contain old customer groups, if arar config not saved after delete customer group
            $this->trueAllowedCustomerGroups = array_intersect(
                $groupsFromConfig,
                array_keys($this->customerGroupsPermission->toOptionHash())
            );
        }

        return $this->trueAllowedCustomerGroups;
    }
}
