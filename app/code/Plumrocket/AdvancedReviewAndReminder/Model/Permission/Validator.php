<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Permission;

use Plumrocket\AdvancedReviewAndReminder\Api\PermissionValidatorInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\AbstractPermission as PermissionTypes;
use Plumrocket\AdvancedReviewAndReminder\Helper\Permission;

class Validator implements PermissionValidatorInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionDataContainerInterface
     */
    private $permissionDataContainer;

    /**
     * @var GuestSecretDataProvider
     */
    private $guestSecretDataProvider;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface
     */
    private $boughtProductValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface
     */
    private $allowedCustomerGroupsProvider;

    /**
     * Validator constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                              $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionDataContainerInterface  $permissionDataContainer
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface       $guestSecretDataProvider
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface        $boughtProductValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface $allowedCustomerGroupsProvider
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionDataContainerInterface $permissionDataContainer,
        \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface $guestSecretDataProvider,
        \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator,
        \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface $allowedCustomerGroupsProvider
    ) {
        $this->configHelper = $configHelper;
        $this->permissionDataContainer = $permissionDataContainer;
        $this->guestSecretDataProvider = $guestSecretDataProvider;
        $this->boughtProductValidator = $boughtProductValidator;
        $this->allowedCustomerGroupsProvider = $allowedCustomerGroupsProvider;
    }

    /**
     * @param null $productId
     * @param bool $skipBoughtValidation
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionDataContainerInterface
     */
    public function validateGuestPermission($productId = null, $skipBoughtValidation = false)
    {
        $this->permissionDataContainer->resetData();

        $guestPermission    = $this->configHelper->getGuestPermission();
        $customerPermission = $this->configHelper->getCustomerPermission();

        $this->baseCheckGuestPermission(
            $guestPermission,
            $customerPermission,
            $this->allowedCustomerGroupsProvider->isAllGroupsAllowed()
        );

        if (! $skipBoughtValidation && ! $this->permissionDataContainer->isDefinedAllow()) {
            $this->validateBoughtGuestPermission($guestPermission, $productId);
        }

        return $this->permissionDataContainer;
    }

    /**
     * @param integer $guestPermission
     * @param integer $customerPermission
     * @param bool    $isAllGroupsAllowed
     */
    private function baseCheckGuestPermission($guestPermission, $customerPermission, $isAllGroupsAllowed)
    {
        if ($guestPermission === PermissionTypes::PERMISSION_ANY) {
            $this->permissionDataContainer->setReason(Permission::REASON_NO_REASON);
            $this->permissionDataContainer->setIsAllow(true);
            return;
        }

        if ($guestPermission === PermissionTypes::PERMISSION_CANNOT_WRITE) {
            $this->permissionDataContainer->setReason(Permission::REASON_CANNOT_WRITE);

            if ($customerPermission === PermissionTypes::PERMISSION_ANY) {
                if ($isAllGroupsAllowed) {
                    $this->permissionDataContainer->setCanShowFormForGuest(true);
                    $this->permissionDataContainer->setReason(Permission::REASON_NO_REASON);
                }
            } elseif ($customerPermission === PermissionTypes::PERMISSION_ONLY_BOUGHT) {
                if ($isAllGroupsAllowed) {
                    $this->permissionDataContainer->setReason(Permission::REASON_ONLY_REGISTERED_BUYERS);
                } else {
                    $this->permissionDataContainer->setReason(Permission::REASON_CANNOT_WRITE);
                }
            } else {
                $this->permissionDataContainer->setReason(Permission::REASON_CANNOT_WRITE);
            }

            $this->permissionDataContainer->setIsAllow(false);
            return;
        }
    }

    /**
     * @param int $guestPermission
     * @param int $productId
     */
    private function validateBoughtGuestPermission($guestPermission, $productId)
    {
        if ($guestPermission === PermissionTypes::PERMISSION_ONLY_BOUGHT) {
            $this->permissionDataContainer->setReason(Permission::REASON_ONLY_BUYER);
            if (! $productId) {
                $this->permissionDataContainer->setIsAllow(false);
                return;
            }

            $secretData = $this->guestSecretDataProvider->getData();

            if ($this->boughtProductValidator->isGuestBoughtProduct($productId, $secretData)) {
                $this->permissionDataContainer->setReason(Permission::REASON_NO_REASON);
                $this->permissionDataContainer->setIsAllow(true);
            } else {
                $this->permissionDataContainer->setIsAllow(false);
            }
        }
    }

    /**
     * @param int|string $customerId
     * @param bool       $customerGroupId
     * @param            $productId
     * @param bool       $skipBoughtValidation
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionDataContainerInterface
     */
    public function validateCustomerPermission($customerId, $customerGroupId, $productId, $skipBoughtValidation = false)
    {
        $this->permissionDataContainer->resetData();

        $customerPermission = $this->configHelper->getCustomerPermission();

        $this->baseCheckCustomerPermission(
            $customerId,
            $customerPermission,
            $customerGroupId,
            $this->allowedCustomerGroupsProvider->getList()
        );

        if (! $skipBoughtValidation && ! $this->permissionDataContainer->isDefinedAllow()) {
            $this->validateBoughtCustomerPermission(
                $customerId,
                $customerPermission,
                $productId
            );
        }

        if (! $this->permissionDataContainer->isDefinedAllow()) {
            $this->permissionDataContainer->setIsAllow(self::IS_ALLOW_BY_DEFAULT);
        }

        return $this->permissionDataContainer;
    }

    /**
     * @param int $customerId
     * @param int $customerPermission
     * @param int $customerGroupId
     * @param array $permissionGroups
     */
    private function baseCheckCustomerPermission($customerId, $customerPermission, $customerGroupId, $permissionGroups)
    {
        if (! $customerId) {
            $this->permissionDataContainer->setReason(Permission::REASON_CANNOT_WRITE);
            $this->permissionDataContainer->setIsAllow(false);
            return;
        }

        if (! in_array($customerGroupId, $permissionGroups, false)) {
            $this->permissionDataContainer->setReason(Permission::REASON_CANNOT_WRITE);
            $this->permissionDataContainer->setIsAllow(false);
            return;
        }

        if (PermissionTypes::PERMISSION_ANY === $customerPermission) {
            $this->permissionDataContainer->setReason(Permission::REASON_NO_REASON);
            $this->permissionDataContainer->setIsAllow(true);
            return;
        }

        if (PermissionTypes::PERMISSION_CANNOT_WRITE === $customerPermission) {
            $this->permissionDataContainer->setReason(Permission::REASON_CANNOT_WRITE);
            $this->permissionDataContainer->setIsAllow(false);
            return;
        }
    }

    /**
     * @param int   $customerId
     * @param int   $customerPermission
     * @param int   $productId
     */
    private function validateBoughtCustomerPermission(
        $customerId,
        $customerPermission,
        $productId
    ) {
        if (PermissionTypes::PERMISSION_ONLY_BOUGHT === $customerPermission) {
            $this->permissionDataContainer->setReason(Permission::REASON_ONLY_BUYER);
            if (! $productId || ! $customerId) {
                $this->permissionDataContainer->setIsAllow(false);
                return;
            }

            $isCustomerBoughtProduct = $this->boughtProductValidator->isCustomerBoughtProduct(
                $productId,
                $customerId
            );

            if ($isCustomerBoughtProduct) {
                $this->permissionDataContainer->setReason(Permission::REASON_NO_REASON);
                $this->permissionDataContainer->setIsAllow(true);
            }
        }
    }
}
