<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Permission;

class GuestSecretDataProvider implements \Plumrocket\AdvancedReviewAndReminder\Api\GuestSecretDataProviderInterface
{
    const SECRET_KEY_FOR_GUEST_REMINDER = 'guest_sec';

    /**
     * @var Data\SecretContainer
     */
    private $secretContainer;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $httpRequest;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    private $cookieManager;

    /**
     * GuestSecretDataProvider constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionSecretDataContainerInterface $secretContainer
     * @param \Magento\Framework\App\RequestInterface                                               $httpRequest
     * @param \Magento\Framework\Stdlib\CookieManagerInterface                                      $cookieManager
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionSecretDataContainerInterface $secretContainer,
        \Magento\Framework\App\RequestInterface $httpRequest,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
    ) {
        $this->secretContainer = $secretContainer;
        $this->httpRequest = $httpRequest;
        $this->cookieManager = $cookieManager;
    }

    /**
     * @return Data\SecretContainer
     */
    public function getData()
    {
        $secretKey = $this->httpRequest->getParam(self::SECRET_KEY_FOR_GUEST_REMINDER);

        if (! $secretKey) {
            $secretKey = $this->cookieManager->getCookie(self::SECRET_KEY_FOR_GUEST_REMINDER);
        }

        if (! $secretKey) {
            return $this->secretContainer;
        }

        //Secret key consist from secret code and reminder id
        $secretData = explode('_', $secretKey);
        if (count($secretData) === 2) {
            $this->secretContainer->setKey($secretData[0])
                   ->setReminderId($secretData[1]);
        }

        return $this->secretContainer;
    }
}
