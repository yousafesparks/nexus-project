<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

/**
 * Interface GetProductsRelatedProductsInterface
 *
 * @since 1.2.0
 */
interface GetProductsRelatedProductsInterface
{
    /**
     * @param \Magento\Catalog\Model\Product[] $products
     * @return \Magento\Catalog\Model\Product[]
     */
    public function execute(array $products) : array;
}
