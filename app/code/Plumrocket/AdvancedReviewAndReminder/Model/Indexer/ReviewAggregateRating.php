<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Indexer;

use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingStoreViewsConfig;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer\GetReviewsRatingsVotesData;

/**
 * Class ReviewAggregateRating
 * @since 2.0.0
 */
class ReviewAggregateRating implements
    \Magento\Framework\Indexer\ActionInterface,
    \Magento\Framework\Mview\ActionInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingStoreViewsConfig
     */
    private $getRatingStoreViewsConfig;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\IsRatingEnabledInterface
     */
    private $isRatingEnabled;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer\GetReviewsRatingsVotesData
     */
    private $getReviewsRatingsVotesData;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer\Builder
     */
    private $ratingIndexerBuilder;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\Collection
     */
    private $reviewCollection;

    /**
     * ReviewAggregateRating constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Psr\Log\LoggerInterface                                                                            $logger
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingStoreViewsConfig          $getRatingStoreViewsConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\IsRatingEnabledInterface                                  $isRatingEnabled
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer\GetReviewsRatingsVotesData $getReviewsRatingsVotesData
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer\Builder                    $ratingIndexerBuilder
     * @param \Magento\Review\Model\ResourceModel\Review\Collection                                               $reviewCollection
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        GetRatingStoreViewsConfig $getRatingStoreViewsConfig,
        \Plumrocket\AdvancedReviewAndReminder\Api\IsRatingEnabledInterface $isRatingEnabled,
        GetReviewsRatingsVotesData $getReviewsRatingsVotesData,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer\Builder $ratingIndexerBuilder,
        \Magento\Review\Model\ResourceModel\Review\Collection $reviewCollection
    ) {
        $this->logger = $logger;
        $this->getRatingStoreViewsConfig = $getRatingStoreViewsConfig;
        $this->isRatingEnabled = $isRatingEnabled;
        $this->getReviewsRatingsVotesData = $getReviewsRatingsVotesData;
        $this->ratingIndexerBuilder = $ratingIndexerBuilder;
        $this->reviewCollection = $reviewCollection;
    }

    /**
     * @inheritdoc
     */
    public function executeFull()
    {
        $this->process([]);
    }

    /**
     * @inheritdoc
     */
    public function executeList(array $ids)
    {
        $this->process($ids);
    }

    /**
     * @inheritdoc
     */
    public function executeRow($id)
    {
        $this->process([$id]);
    }

    /**
     * @inheritdoc
     */
    public function execute($ids)
    {
        $this->process($ids);
    }

    /**
     * Main index processing action
     *
     * @param array $reviewIds
     * @throws \Exception
     */
    private function process(array $reviewIds)
    {
        if (! $this->isRatingEnabled->execute()) {
            return;
        }

        if (empty($reviewIds)) {
            $reviewIds = $this->reviewCollection->getAllIds();
            if (empty($reviewIds)) {
                return;
            }
        }

        try {
            $ratingData = $this->aggregateRatingData($reviewIds);
            $this->ratingIndexerBuilder->build($reviewIds, $ratingData);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            if (!$reviewIds) {
                throw $e;
            }
        }
    }

    /**
     * @param array $ids
     * @return array
     */
    private function aggregateRatingData(array $ids) : array
    {
        $reviewRatings = $this->getReviewsRatingsVotesData->execute(
            $ids,
            array_keys($this->getRatingStoreViewsConfig->execute(false)),
            true
        );

        $ratingsByStores = $this->getRatingStoreViewsConfig->execute();

        $data = [];
        foreach ($reviewRatings as $reviewId => $ratings) {
            foreach ($ratingsByStores as $storeId => $value) {
                $data[] = [
                    'review_id' => $reviewId,
                    'store_id' => $storeId,
                    'percent' => $this->getRating(array_keys($value), $ratings),
                ];
            }
        }

        return $data;
    }

    /**
     * Get average percent for store view
     *
     * @param array $ratings
     * @param array $percents
     * @return float|int
     */
    private function getRating(array $ratings, array $percents)
    {
        $rating = [];
        foreach ($ratings as $ratingId) {
            if (isset($percents[$ratingId])) {
                $rating[] = $percents[$ratingId];
            }
        }

        $count = count($rating);

        return $count ? array_sum($rating) / $count : 0;
    }
}
