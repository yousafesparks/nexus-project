<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

/**
 * Class GetAdvancedEnabledFields
 * @since 2.0.0
 */
class GetAdvancedEnabledFields
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview
     */
    private $advancedReviewResource;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * GetAdvancedEnabledFields constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview $advancedReviewResource
     * @param \Magento\Framework\App\ResourceConnection                                $resourceConnection
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                      $configHelper
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview $advancedReviewResource,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
    ) {
        $this->advancedReviewResource = $advancedReviewResource;
        $this->resourceConnection = $resourceConnection;
        $this->configHelper = $configHelper;
    }

    /**
     * @return array
     */
    public function execute()
    {
        $fields = $this->resourceConnection->getConnection()->describeTable(
            $this->advancedReviewResource->getMainTable()
        );

        unset($fields['abuse']); // this fields doesnt use on frontend

        if (! $this->configHelper->isEnabledVerified()) {
            unset($fields['verified']);
        }

        if (! $this->configHelper->isEnabledHelpful()) {
            unset($fields['helpful_positive'], $fields['helpful_negative']);
        }

        if (Option::NO === $this->configHelper->getProsAndConsOption()) {
            unset($fields['pros'], $fields['cons']);
        }

        return array_keys($fields);
    }
}
