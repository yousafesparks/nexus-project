<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

class GetCount implements \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface
{
    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    private $reviewCollectionFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * GetCount constructor.
     *
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
     */
    public function __construct(
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->reviewCollectionFactory = $reviewCollectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param int|null $productId
     * @param int|null $storeId
     * @param bool     $onlyApproved
     * @return int
     */
    public function execute(int $productId = null, int $storeId = null, bool $onlyApproved = true) : int
    {
        /** @var \Magento\Review\Model\ResourceModel\Review\Collection $collection */
        $collection = $this->reviewCollectionFactory->create();
        $collection->addFilter('main_table.entity_id', 1); // prevent sql join
        $collection->addStoreFilter($storeId ?? $this->storeManager->getStore()->getId());

        if ($productId) {
            $collection->addFieldToFilter('entity_pk_value', $productId);
        }

        if ($onlyApproved) {
            $collection->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED);
        }

        return (int) $collection->getSize();
    }
}
