<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

class RatingToStarsConverter
{
    const FULL_STAR = 'full';
    const EMPTY_STAR = 'empty';
    const HALF_STAR = 'half';

    /**
     * @param int $ratingPercent
     * @return string[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toStars($ratingPercent)
    {
        if (! is_numeric($ratingPercent)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Param "ratingPercent" must be numeric'));
        }

        $stars = [];
        for ($i = 0; $i < 5; $i++) {
            if ($ratingPercent >= 20 || $ratingPercent >= 10) {
                $stars[] = self::FULL_STAR;
            } elseif ($ratingPercent <= 0) {
                $stars[] = self::EMPTY_STAR;
            } else {
                $stars[] = self::HALF_STAR;
            }

            $ratingPercent -= 20;
        }

        return $stars;
    }
}
