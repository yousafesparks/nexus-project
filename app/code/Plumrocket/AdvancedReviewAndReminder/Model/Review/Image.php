<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

class Image
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $imageId;

    /**
     * @var string
     */
    private $path;

    /**
     * Image constructor.
     *
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param string                                     $filename
     * @param string                                     $path
     * @param string                                     $imageId
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        string $filename = '',
        string $path = 'advanced_review',
        string $imageId = ''
    ) {
        $this->storeManager = $storeManager;
        $this->filename = $filename;
        $this->imageId = $imageId;
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getUrl() : string
    {
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl . trim($this->getPath(), '/') . DIRECTORY_SEPARATOR . $this->getFilename();
    }

    /**
     * @return string
     */
    public function getFilename() : string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return $this
     */
    public function setFilename(string $filename) : self
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageId() : string
    {
        return $this->imageId;
    }

    /**
     * @return string
     */
    public function getPath() : string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Image
     */
    public function setPath(string $path) : Image
    {
        $this->path = $path;
        return $this;
    }
}
