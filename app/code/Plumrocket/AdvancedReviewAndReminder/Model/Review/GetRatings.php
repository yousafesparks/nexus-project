<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

/**
 * Class GetRatings
 *
 * @since 1.2.0
 */
class GetRatings implements \Plumrocket\AdvancedReviewAndReminder\Model\GetRatingsInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Review\Model\ResourceModel\Rating\CollectionFactory
     */
    private $ratingCollectionFactory;

    /**
     * GetRatings constructor.
     *
     * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
     * @param \Magento\Review\Model\ResourceModel\Rating\CollectionFactory $ratingCollectionFactory
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Review\Model\ResourceModel\Rating\CollectionFactory $ratingCollectionFactory
    ) {
        $this->storeManager = $storeManager;
        $this->ratingCollectionFactory = $ratingCollectionFactory;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute() : array
    {
        /** @var \Magento\Review\Model\ResourceModel\Rating\Collection $ratingCollection */
        $ratingCollection = $this->ratingCollectionFactory->create();

        return $ratingCollection->addEntityFilter(
            'product'
        )->setPositionOrder()->addRatingPerStoreName(
            $this->storeManager->getStore()->getId()
        )->setStoreFilter(
            $this->storeManager->getStore()->getId()
        )->setActiveFilter(
            true
        )->load()->addOptionToItems()->getItems();
    }
}
