<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Image;

class MultipleFactory implements \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface
     */
    private $reviewImageFactory;

    /**
     * MultipleFactory constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface $reviewImageFactory
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface $reviewImageFactory
    ) {
        $this->reviewImageFactory = $reviewImageFactory;
    }

    /**
     * @inheritDoc
     */
    public function create(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $advancedReview,
        string $imageId
    ) : array {
        $images = [];
        foreach ($advancedReview->getAttachImage() as $image) {
            $images[] = $this->reviewImageFactory->create($image['filename'], $imageId);
        }
        return $images;
    }
}
