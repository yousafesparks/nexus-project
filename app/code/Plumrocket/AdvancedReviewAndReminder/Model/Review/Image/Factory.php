<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Image;

/**
 * Class Factory
 *
 * @since 2.0.0
 */
class Factory implements \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\ImageFactory
     */
    private $imageFactory;

    /**
     * @var \Magento\Framework\View\ConfigInterface
     */
    private $presentationConfig;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ImageResizeInterface
     */
    private $imageResize;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * Factory constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\ImageFactory  $imageFactory
     * @param \Magento\Framework\View\ConfigInterface                          $presentationConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ImageResizeInterface $imageResize
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config              $configHelper
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\ImageFactory $imageFactory,
        \Magento\Framework\View\ConfigInterface $presentationConfig,
        \Plumrocket\AdvancedReviewAndReminder\Model\ImageResizeInterface $imageResize,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
    ) {
        $this->imageFactory = $imageFactory;
        $this->presentationConfig = $presentationConfig;
        $this->imageResize = $imageResize;
        $this->configHelper = $configHelper;
    }

    /**
     * @inheritDoc
     */
    public function create(string $fileName, string $imageId) : \Plumrocket\AdvancedReviewAndReminder\Model\Review\Image
    {
        if ($imageId !== \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface::ORIGIN_IMAGE_ID) {
            $viewImageConfig = $this->presentationConfig->getViewConfig()->getMediaAttributes(
                'Plumrocket_AdvancedReviewAndReminder',
                \Magento\Catalog\Helper\Image::MEDIA_TYPE_CONFIG_NODE,
                $imageId
            );

            $path = $this->imageResize->execute(
                $fileName,
                $viewImageConfig['width'] ?? 0,
                $viewImageConfig['height'] ?? 0,
                $this->configHelper->getBaseMediaPath(false)
            );
        } else {
            $path = $this->configHelper->getBaseMediaPath(false);
        }

        return $this->imageFactory->create(['filename' => $fileName, 'path' => $path, 'imageId' => $imageId]);
    }
}
