<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Image;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Resize
 *
 * @since 2.0.0
 */
class Resize implements \Plumrocket\AdvancedReviewAndReminder\Model\ImageResizeInterface
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    private $imageFactory;

    /**
     * ImageResize constructor.
     *
     * @param \Magento\Framework\Filesystem           $filesystem
     * @param \Magento\Framework\Image\AdapterFactory $imageFactory
     */
    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\AdapterFactory $imageFactory
    ) {
        $this->filesystem = $filesystem;
        $this->imageFactory = $imageFactory;
    }

    /**
     * @param string $image
     * @param int    $width
     * @param int    $height
     * @param string $path
     * @param string $additionalPath
     * @return bool|string
     */
    public function execute(string $image, int $width, int $height, string $path, string $additionalPath = 'resized')
    {
        $extendedPath = $additionalPath ? $path . DIRECTORY_SEPARATOR . $additionalPath : $path;

        $pathWithSizes = $extendedPath . DIRECTORY_SEPARATOR . "{$width}x{$height}";

        $absolutePath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath($path) . DIRECTORY_SEPARATOR . $image;

        if (! file_exists($absolutePath)) { // phpcs:ignore
            return false;
        }

        $imageResized = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath($pathWithSizes) . DIRECTORY_SEPARATOR . $image;

        if (! file_exists($imageResized)) { // phpcs:ignore
            $imageResize = $this->imageFactory->create();
            $imageResize->open($absolutePath);
            $imageResize->constrainOnly(true);
            $imageResize->keepTransparency(true);
            $imageResize->keepFrame(false);
            $imageResize->keepAspectRatio(true);
            $imageResize->resize($width ?: null, $height ?: null);
            $destination = $imageResized;
            $imageResize->save($destination);
        }

        return $pathWithSizes;
    }
}
