<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

use Plumrocket\AdvancedReviewAndReminder\Helper\Data as DataHelper;

/**
 * Class GetSeoFriendlyUrl
 *
 * @since 2.0.0
 */
class GetSeoFriendlyUrl implements \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\UpdateSeoFriendlyUrl
     */
    private $updateSeoFriendlyUrl;

    /**
     * @var \Magento\Framework\Url
     */
    private $frontUrlBuilder;

    /**
     * GetSeoFriendlyUrl constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\UpdateSeoFriendlyUrl $updateSeoFriendlyUrl
     * @param \Magento\Framework\Url                                                  $frontUrlBuilder
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\UpdateSeoFriendlyUrl $updateSeoFriendlyUrl,
        \Magento\Framework\Url $frontUrlBuilder
    ) {
        $this->updateSeoFriendlyUrl = $updateSeoFriendlyUrl;
        $this->frontUrlBuilder = $frontUrlBuilder;
    }

    /**
     * @param \Magento\Review\Model\Review $review
     * @param bool                         $updateReview
     * @return string
     */
    public function execute(\Magento\Review\Model\Review $review, bool $updateReview = true) : string
    {
        if (! $review->getReviewUrlKey()) {
            $this->updateSeoFriendlyUrl->execute($review, $updateReview);
        }

        if (! $review->getReviewUrlKey() || ! $review->getTitle()) {
            return $this->frontUrlBuilder->getUrl('review/product/view', ['id' => $review->getReviewId()]);
        }

        return $this->frontUrlBuilder->getUrl() . DataHelper::REVIEW_URL_ROUTE . '/' . $review->getReviewUrlKey();
    }
}
