<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

class VerifiedTextGenerator
{
    public function execute(int $inputSeconds)
    {
        $secondsInAMinute = 60;
        $secondsInAnHour = 60 * $secondsInAMinute;
        $secondsInADay = 24 * $secondsInAnHour;
        $secondsInAWeek = 7 * $secondsInADay;
        $secondsInAMonth = 30 * $secondsInAWeek;
        $secondsInAnYear = 12 * $secondsInAMonth;

        // Extract years
        $years = floor($inputSeconds / $secondsInAnYear);

        // Extract month
        $months = floor($inputSeconds / $secondsInAMonth);

        // Extract week
        $weeks = floor($inputSeconds / $secondsInAWeek);

        // Extract days
        $days = floor($inputSeconds / $secondsInADay);

        $sections = [
            'year' => (int) $years,
            'month' => (int) $months,
            'week' => (int) $weeks,
            'day' => (int) $days,
        ];

        foreach ($sections as $name => $value) {
            if ($value > 0) {
                $timeParts = $value . ' ' . $name . ($value == 1 ? '' : 's');
                break;
            }
        }

        if (! isset($timeParts)) {
            $timeParts = '1 day';
        }

        return __('Owned for %1 when reviewed.', $timeParts);
    }
}
