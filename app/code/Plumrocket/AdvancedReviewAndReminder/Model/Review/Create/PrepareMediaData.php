<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Create;

use Magento\Framework\Serialize\SerializerInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\ReviewPrepareMediaDataInterface;
use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;
use Plumrocket\AdvancedReviewAndReminder\Helper\File as FileHelper;

class PrepareMediaData implements ReviewPrepareMediaDataInterface
{
    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    /**
     * PrepareMediaData constructor.
     *
     * @param ConfigHelper                                     $configHelper
     * @param FileHelper                                       $fileHelper
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        ConfigHelper $configHelper,
        FileHelper $fileHelper,
        SerializerInterface $serializer
    ) {
        $this->configHelper = $configHelper;
        $this->fileHelper = $fileHelper;
        $this->serializer = $serializer;
    }

    /**
     * @param array $data
     * @return array
     */
    public function execute(array $data) : array
    {
        if (isset($data['file']) && $uploadImg = $this->fileHelper->moveUploadImage($data['file'])) {
            $data['attach_image'] = $this->serializer->serialize($uploadImg);
        }

        if (isset($data['video']) && $videoId = $this->configHelper->getYoutubeIdFromUrl($data['video'] ?? '')) {
            $data['youtube_id'] = $videoId;
        }

        return $data;
    }
}
