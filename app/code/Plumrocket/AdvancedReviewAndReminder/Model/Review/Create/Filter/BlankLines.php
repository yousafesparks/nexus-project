<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter;

/**
 * @since 2.0.1
 */
class BlankLines
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array
    {
        if (! empty($data['detail'])) {
            $data['detail'] = $this->removeBlankLines($data['detail']);
        }

        if (! empty($data['title'])) {
            $data['title'] = $this->removeBlankLines($data['title']);
        }

        if (! empty($data['pros'])) {
            $data['pros'] = $this->removeBlankLines($data['pros']);
        }

        if (! empty($data['cons'])) {
            $data['cons'] = $this->removeBlankLines($data['cons']);
        }

        return $data;
    }

    /**
     * @param string $string
     * @return string
     */
    private function removeBlankLines(string $string): string
    {
        return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", trim($string));
    }
}
