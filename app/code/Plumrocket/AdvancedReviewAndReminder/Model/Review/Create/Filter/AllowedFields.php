<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter;

use Plumrocket\AdvancedReviewAndReminder\Api\ReviewAllowPostDataFilterInterface;

class AllowedFields implements ReviewAllowPostDataFilterInterface
{
    /**
     * @var array
     */
    private $disallowedFields;

    /**
     * FieldFilter constructor.
     *
     * @param array $disallowedFields
     */
    public function __construct(array $disallowedFields = [])
    {
        $this->disallowedFields = array_filter(
            array_merge($this->getDefaultDisallowedFields(), $disallowedFields)
        );
    }

    /**
     * @return array
     */
    private function getDefaultDisallowedFields() : array
    {
        return [
            'helpful_positive' => true,
            'helpful_negative' => true,
            'abuse' => true,
            'abused_customer' => true,
            'admin_comment' => true,
            'admin_comment_date' => true,
            'review_url_key_origin' => true,
            'review_url_key' => true,
            'verified' => true,
            'attach_image' => true,
        ];
    }

    /**
     * {{@inheritDoc}}
     */
    public function filter(array $data) : array
    {
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $this->disallowedFields)) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
