<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired;

/**
 * @since 1.2.0
 */
class DisabledFields
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * SanitizeDisabledFieldsData constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     */
    public function __construct(\Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper)
    {
        $this->configHelper = $configHelper;
    }

    /**
     * @param array $data
     * @param null  $store
     * @return array
     */
    public function filter(array $data, $store = null) : array
    {
        if (! empty($data['title']) && Yesnorequired::NO === $this->configHelper->getReviewSummaryOption($store)) {
            unset($data['title']);
        }

        if (! empty($data['pros']) && Yesnorequired::NO === $this->configHelper->getProsAndConsOption($store)) {
            unset($data['pros']);
        }

        if (! empty($data['cons']) && Yesnorequired::NO === $this->configHelper->getProsAndConsOption($store)) {
            unset($data['cons']);
        }

        if (! empty($data['file']) && ! $this->configHelper->isEnabledImageUpload()) {
            unset($data['file']);
        }

        if (! empty($data['video']) && ! $this->configHelper->isEnabledVideoLink()) {
            unset($data['video']);
        }

        if (! empty($data['recommend']) && ! $this->configHelper->isEnabledRecommendedToAFriend()) {
            unset($data['recommend']);
        }

        return $data;
    }
}
