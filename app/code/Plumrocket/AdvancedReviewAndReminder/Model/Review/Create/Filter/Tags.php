<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter;

use Magento\Framework\Filter\StripTags;

/**
 * @since 2.0.1
 */
class Tags
{
    /**
     * @var \Magento\Framework\Filter\StripTags
     */
    private $stripTags;

    /**
     * SanitizeTags constructor.
     *
     * @param \Magento\Framework\Filter\StripTags $stripTags
     */
    public function __construct(StripTags $stripTags)
    {
        $this->stripTags = $stripTags;
    }

    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array
    {
        if (! empty($data['detail'])) {
            $data['detail'] = $this->stripTags->filter($data['detail']);
        }

        if (! empty($data['title'])) {
            $data['title'] = $this->stripTags->filter($data['title']);
        }

        if (! empty($data['pros'])) {
            $data['pros'] = $this->stripTags->filter($data['pros']);
        }

        if (! empty($data['cons'])) {
            $data['cons'] = $this->stripTags->filter($data['cons']);
        }

        return $data;
    }
}
