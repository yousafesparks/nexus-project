<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review\Create;

use Plumrocket\AdvancedReviewAndReminder\Api\ReviewAllowPostDataFilterInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\BlankLines as BlankLinesFilter;
use Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\DisabledFields as DisabledFieldsFilter;
use Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\Tags as TagsFilter;

/**
 * Clear review post data from admin fields, disabled fields, HTML tags
 *
 * @since 2.0.1
 */
class ReviewFilter implements \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\ReviewAllowPostDataFilterInterface
     */
    private $reviewAllowPostDataFilter;

    /**
     * @var DisabledFieldsFilter
     */
    private $disabledFieldsFilter;

    /**
     * @var TagsFilter
     */
    private $tagsFilter;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\BlankLines
     */
    private $blankLinesFilter;

    /**
     * ReviewFilter constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\ReviewAllowPostDataFilterInterface    $reviewAllowPostDataFilter
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\DisabledFields $disabledFieldsFilter
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\Tags           $tagsFilter
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\BlankLines     $blankLinesFilter
     */
    public function __construct(
        ReviewAllowPostDataFilterInterface $reviewAllowPostDataFilter,
        DisabledFieldsFilter $disabledFieldsFilter,
        TagsFilter $tagsFilter,
        BlankLinesFilter $blankLinesFilter
    ) {
        $this->reviewAllowPostDataFilter = $reviewAllowPostDataFilter;
        $this->disabledFieldsFilter = $disabledFieldsFilter;
        $this->tagsFilter = $tagsFilter;
        $this->blankLinesFilter = $blankLinesFilter;
    }

    /**
     * @param array $data
     * @param null  $store
     * @return array
     */
    public function filter(array $data, $store = null) : array
    {
        $data = $this->reviewAllowPostDataFilter->filter($data);
        $data = $this->disabledFieldsFilter->filter($data, $store);
        $data = $this->tagsFilter->filter($data);
        $data = $this->blankLinesFilter->filter($data);
        return $data;
    }
}
