<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

/**
 * Class FormValidator
 *
 * @since 1.2.0
 */
class FormValidator
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * SanitizeDisabledFieldsData constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     */
    public function __construct(\Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper)
    {
        $this->configHelper = $configHelper;
    }

    /**
     * @param array $data
     * @param null  $store
     * @return array|bool
     * @throws \Zend_Validate_Exception
     */
    public function detectMistakes(array $data, $store = null)
    {
        $errors = [];

        if (! isset($data['title']) && Option::REQUIRED === $this->configHelper->getReviewSummaryOption($store)) {
            $errors[] =__('Review summary can\'t be empty');
        } elseif (isset($data['title']) && ! empty($data['title']) && mb_strlen($data['title']) < 5) {
            $errors[] = __('Minimum title length is 5 characters');
        }

        if (isset($data['nickname'])) {
            if (!\Zend_Validate::is($data['nickname'], 'NotEmpty')) {
                $errors[] = __('Username can\'t be empty');
            } elseif (strlen($data['nickname']) < 2) {
                $errors[] = __('Minimum username length is 2 characters');
            }
        }

        if (isset($data['detail'])) {
            if (! \Zend_Validate::is($data['detail'], 'NotEmpty')) {
                $errors[] = __('Review can\'t be empty');
            } elseif (strlen($data['detail']) < 5) {
                $errors[] = __('Minimum review length is 5 characters');
            }
        } else {
            $errors[] = __('Review can\'t be empty');
        }

        if (isset($data['ratings'])) {
            if (! is_array($data['ratings'])) {
                $data['ratings'] = (array)$data['ratings'];
            }

            foreach ($data['ratings'] as $rating) {
                if (! \Zend_Validate::is($rating, 'NotEmpty')) {
                    $errors[] = __('Ratings must be selected');
                }
            }
        }

        if (Option::REQUIRED === $this->configHelper->getProsAndConsOption()) {
            if (! isset($data['pros']) || ! \Zend_Validate::is($data['pros'], 'NotEmpty')) {
                $errors[] = __('Pros can\'t be empty');
            }

            if (! isset($data['cons']) || ! \Zend_Validate::is($data['cons'], 'NotEmpty')) {
                $errors[] = __('Cons can\'t be empty');
            }
        }

        if (isset($data['recommend'])
            && ! in_array($data['recommend'], [Recommend::NOT_SPECIFIED, Recommend::NO, Recommend::YES], false)
        ) {
            $errors[] = __('Invalid recommend value');
        }

        return $errors;
    }
}
