<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Review;

use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\Collection as AdvancedReviewCollection;

/**
 * Class UpdateSeoFriendlyUrl
 *
 * @since 2.0.0
 */
class UpdateSeoFriendlyUrl
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * UpdateSeoFriendlyUrl constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                                            $productRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                                          $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\CollectionFactory $collectionFactory
     * @param \Psr\Log\LoggerInterface                                                                   $logger
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\CollectionFactory $collectionFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->productRepository = $productRepository;
        $this->dataHelper = $dataHelper;
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Review\Model\Review $review
     * @param bool                         $save
     * @return \Magento\Review\Model\Review
     */
    public function execute(\Magento\Review\Model\Review $review, bool $save = true) : \Magento\Review\Model\Review
    {
        if (! $review->getTitle() || $review->getReviewUrlKey()) {
            return $review;
        }

        try {
            $product = $this->productRepository->getById($review->getEntityPkValue(), false, $review->getStoreId());

            $productUrlKey = $product->getUrlKey();
            if (!$productUrlKey) {
                $productUrlKey = $this->dataHelper->slugifyUrl($product->getName());
            }

            $originUrlKey = $this->dataHelper->getReviewUrlKey($review->getTitle());
            $_originUrlKey = $productUrlKey . '/' . $originUrlKey;

            /** @var AdvancedReviewCollection $advancedReviewCollection */
            $advancedReviewCollection = $this->collectionFactory->create();

            $duplicates = $advancedReviewCollection->addFieldToFilter('review_url_key_origin', $_originUrlKey);

            $index = (int) $duplicates->getSize();

            if ($index) {
                $reviewUrlKey = $this->dataHelper->getReviewUrlKey($review->getTitle(), $index);
            } else {
                $reviewUrlKey = $originUrlKey;
            }

            try {
                $review->setReviewUrlKey($productUrlKey . '/' . $reviewUrlKey)
                       ->setReviewUrlKeyOrigin($_originUrlKey);

                if ($save) {
                    $review->save();
                }
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noEntityException) {
            $this->logger->debug($noEntityException->getMessage());
        }

        return $review;
    }
}
