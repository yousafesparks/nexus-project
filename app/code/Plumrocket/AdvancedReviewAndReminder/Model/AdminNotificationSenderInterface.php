<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

interface AdminNotificationSenderInterface
{
    /**
     * Send notification about pending review
     *
     * @param \Magento\Review\Model\Review                                              $review
     * @param \Magento\Catalog\Api\Data\ProductInterface|\Magento\Catalog\Model\Product $product
     */
    public function sendNotificationsOfPendingReview(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product
    );

    /**
     * Send email notification about abuse report
     *
     * @param \Magento\Review\Model\Review               $review
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param int                                        $customerId
     */
    public function sendNotificationsOfAbuse(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $customerId
    );
}
