<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\Email;

class SenderData
{
    /**
     * @var \Plumrocket\Base\Model\Utils\Config
     */
    private $configUtils;

    /**
     * @param \Plumrocket\Base\Model\Utils\Config $configUtils
     */
    public function __construct(\Plumrocket\Base\Model\Utils\Config $configUtils)
    {
        $this->configUtils = $configUtils;
    }

    /**
     * @param string|null $storeId
     * @return array|mixed|null
     */
    public function get($storeId = null)
    {
        return [
            'email' => $this->configUtils->getConfig('trans_email/ident_general/email', $storeId),
            'name'  => $this->configUtils->getConfig('trans_email/ident_general/name', $storeId),
        ];
    }
}
