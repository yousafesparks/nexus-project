<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Magento\Framework\Model\AbstractModel;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface;

class UnsubscribeReason extends AbstractModel implements UnsubscribeReasonInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @inheritDoc
     */
    protected $_idFieldName = ResourceModel\UnsubscribeReason::PRIMARY_FIELD;

    /**
     * UnsubscribeReason constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(ResourceModel\UnsubscribeReason::class);
    }

    /**
     * @inheritDoc
     */
    public function getLabel(): string
    {
        return $this->getData(self::LABEL);
    }

    /**
     * @inheritDoc
     */
    public function getInputType(): int
    {
        return (int) $this->getData(self::INPUT_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function getStatus(): int
    {
        return (int) $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function getStoreLabels(): array
    {
        return (array) $this->getData(self::STORE_LABEL);
    }

    /**
     * @inheritDoc
     */
    public function setLabel(string $label): UnsubscribeReasonInterface
    {
        $this->setData(self::LABEL, $label);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setInputType(int $type): UnsubscribeReasonInterface
    {
        $this->setData(self::INPUT_TYPE, $type);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setStatus(int $status): UnsubscribeReasonInterface
    {
        $this->setData(self::STATUS, $status);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setStoreLabels(array $labels): UnsubscribeReasonInterface
    {
        $this->setData(self::STORE_LABEL, $labels);
        return $this;
    }

    /**
     * Retrieve label by store
     *
     * @param null $storeId
     * @return mixed|string
     */
    public function getStoreLabel($storeId = null)
    {
        $storeLabels = $this->getStoreLabels();

        if (! $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }

        if (! empty($storeLabels[$storeId])) {
            return $storeLabels[$storeId];
        }

        return $this->getLabel();
    }
}
