<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Magento\Customer\Model\CustomerFactory;
use Magento\Email\Model\TemplateFactory as EmailTemplateFactory;
use Magento\Framework\App\Area;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Plumrocket\AdvancedReviewAndReminder\Helper\Data as DataHelper;
use Plumrocket\AdvancedReviewAndReminder\Helper\Develop as DevelopHelper;
use Plumrocket\AdvancedReviewAndReminder\Helper\Permission as PermissionHelper;

/**
 * @method \Magento\Sales\Model\Order getOrder()
 * @method $this setOrder(\Magento\Sales\Model\Order $order)
 * @method $this setReminder(Reminder $reminder)
 * @method Reminder getReminder()
 */
class Email extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Those fields contain temp information
     * They must been reset after each \Plumrocket\AdvancedReviewAndReminder\Model\Email::processReminder
     */
    private $productsFromOrder;
    private $customer;
    private $proccessedTemplate;
    private $templateVars;
    private $storeId;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Url
     */
    protected $urlHelper;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;

    /**
     * @var \Magento\Email\Model\TemplateFactory
     */
    private $emailTemplateFactory;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Permission
     */
    private $permissionHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory
     */
    private $reminderFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReminderdoneFactory
     */
    private $reminderdoneFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Email\SenderData
     */
    private $senderData;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsFromOrderInterface
     */
    private $getProductsFromOrder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsRelatedProductsInterface
     */
    private $getProductsRelatedProducts;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
     */
    private $autoLoginManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Develop
     */
    private $developHelper;

    /**
     * Email constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                               $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Permission                         $permissionHelper
     * @param \Magento\Store\Model\StoreManagerInterface                                      $storeManager
     * @param \Magento\Sales\Model\OrderFactory                                               $orderFactory
     * @param \Magento\Customer\Model\CustomerFactory                                         $customerFactory
     * @param \Magento\Framework\Translate\Inline\StateInterface                              $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder                               $transportBuilder
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                                     $date
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory                     $reminderFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReminderdoneFactory                 $reminderdoneFactory
     * @param \Magento\Email\Model\TemplateFactory                                            $emailTemplateFactory
     * @param \Magento\Framework\Model\Context                                                $context
     * @param \Magento\Framework\Registry                                                     $registry
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Email\SenderData                    $senderData
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsFromOrderInterface       $getProductsFromOrder
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsRelatedProductsInterface $getProductsRelatedProducts
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface             $autoLoginManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig                     $reminderConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Url                                $urlHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Develop                            $developHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null                    $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null                              $resourceCollection
     * @param array                                                                           $data
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        DataHelper $dataHelper,
        PermissionHelper $permissionHelper,
        StoreManagerInterface $storeManager,
        OrderFactory $orderFactory,
        CustomerFactory $customerFactory,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        DateTime $date,
        ReminderFactory $reminderFactory,
        ReminderdoneFactory $reminderdoneFactory,
        EmailTemplateFactory $emailTemplateFactory,
        Context $context,
        Registry $registry,
        \Plumrocket\AdvancedReviewAndReminder\Model\Email\SenderData $senderData,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsFromOrderInterface $getProductsFromOrder,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsRelatedProductsInterface $getProductsRelatedProducts,
        \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Url $urlHelper,
        DevelopHelper $developHelper,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->orderFactory = $orderFactory;
        $this->customerFactory = $customerFactory;
        $this->emailTemplateFactory = $emailTemplateFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->dataHelper = $dataHelper;
        $this->permissionHelper = $permissionHelper;
        $this->storeManager = $storeManager;
        $this->date = $date;
        $this->reminderFactory = $reminderFactory;
        $this->reminderdoneFactory = $reminderdoneFactory;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
        $this->senderData = $senderData;
        $this->getProductsFromOrder = $getProductsFromOrder;
        $this->getProductsRelatedProducts = $getProductsRelatedProducts;
        $this->autoLoginManager = $autoLoginManager;
        $this->reminderConfig = $reminderConfig;
        $this->urlHelper = $urlHelper;
        $this->developHelper = $developHelper;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder $reminder
     * @return $this
     */
    public function processReminder($reminder)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()->load($reminder->getOrderId());
        $this->setStoreId($reminder->getStoreId());

        if ((int)$reminder->getReminder() === Reminder::ADVANCEDRAR_FIRST_REMINDER
            && $this->reminderConfig->isSecondReminderEnabled()
        ) {
            $this->createSecondReminder($order);
        } elseif ((int)$reminder->getReminder() === Reminder::ADVANCEDRAR_SECOND_REMINDER
            && $this->reminderConfig->isThirdReminderEnabled()
        ) {
            $this->createThirdReminder($order);
        }

        $this->setOrder($order)
            ->setReminder($reminder)
            ->sendAndSave();

        return $this->reset();
    }

    private function canCreateReminder($order, $reminder): bool
    {
        if ($this->developHelper->isReminderSendingIsImmutable()) {
            return false;
        }

        $reminderDone = $this->reminderdoneFactory->create()->getCollection()
                                                  ->addFieldToFilter('order_id', $order->getId())
                                                  ->addFieldToFilter('reminder', $reminder);

        return ! $reminderDone->getSize() && $this->permissionHelper->isSubscribed($order->getCustomerEmail());
    }

    private function createThirdReminder(\Magento\Sales\Model\Order $order)
    {
        if (!$this->canCreateReminder($order, Reminder::ADVANCEDRAR_THIRD_REMINDER)) {
            return false;
        }

        $reminder = $this->reminderFactory->create()
            ->setOrderId($order->getId())
            ->setReminder(Reminder::ADVANCEDRAR_THIRD_REMINDER)
            ->setCreatedAt(gmdate('Y-m-d H:i:s'))
            ->setSecretKey($this->permissionHelper->generateSecretKey($order))
            ->setStoreId($order->getStoreId())
            ->setScheduledAt($this->dataHelper->getThirdReminderDelay($order->getStoreId()))
            ->setStatus(Reminder::ADVANCEDRAR_STATUS_PENDING);

        try {
            $reminder->save();

            $this->reminderdoneFactory->create()
                ->setOrderId($order->getId())
                ->setReminder(Reminder::ADVANCEDRAR_THIRD_REMINDER)
                ->save();
        } catch (\Exception $e) {
            $this->_logger->debug($e->getMessage());
        }

        return $this;
    }

    private function createSecondReminder(\Magento\Sales\Model\Order $order)
    {
        if (!$this->canCreateReminder($order, Reminder::ADVANCEDRAR_SECOND_REMINDER)) {
            return false;
        }

        $reminder = $this->reminderFactory->create()
            ->setOrderId($order->getId())
            ->setReminder(Reminder::ADVANCEDRAR_SECOND_REMINDER)
            ->setCreatedAt(gmdate('Y-m-d H:i:s'))
            ->setStoreId($order->getStoreId())
            ->setSecretKey($this->permissionHelper->generateSecretKey($order))
            ->setScheduledAt($this->dataHelper->getSecondReminderDelay($order->getStoreId()))
            ->setStatus(Reminder::ADVANCEDRAR_STATUS_PENDING);

        try {
            $reminder->save();

            $this->reminderdoneFactory->create()
                ->setOrderId($order->getId())
                ->setReminder(Reminder::ADVANCEDRAR_SECOND_REMINDER)
                ->save();
        } catch (\Exception $e) {
            $this->_logger->debug($e->getMessage());
        }

        return $this;
    }

    public function sendAndSave()
    {
        $order = $this->getOrder();
        $reminder = $this->getReminder();

        if (!$order || !$reminder) {
            return false;
        }

        $this->inlineTranslation->suspend();
        $vars = $this->getVars($order);
        $senderData = $this->senderData->get($order->getStoreId());

        if ($this->getCustomer()) {
            if ($this->reminderConfig->isEnabledAutoLogin($order->getStoreId())
                && $this->getCustomer()->getCustomerId()
            ) {
                $vars['alogin_secret'] = $this->autoLoginManager
                    ->addSecretToUrl(
                        $this->storeManager->getStore($order->getStoreId())->getBaseUrl(UrlInterface::URL_TYPE_LINK),
                        $this->getCustomer()
                    );
            }
        } else {
            $vars['alogin_secret'] = $this->autoLoginManager
                ->addSecretToUrl(
                    $this->storeManager->getStore($order->getStoreId())->getBaseUrl(UrlInterface::URL_TYPE_LINK),
                    null,
                    $reminder
                );
        }

         $emailTemplate = $this->_appState->emulateAreaCode(
             'frontend',
             [$this, 'getEmailTemplate'],
             [$this->getOrder()->getStoreId()]
         );

        $this->transportBuilder->setTemplateIdentifier(
            $emailTemplate->getData('template_id')
        )->setTemplateOptions(
            [
                'area'  => Area::AREA_FRONTEND,
                'store' => $this->storeManager->getStore()->getId(),
            ]
        )->setTemplateVars(
            $vars
        )->setFrom(
            $senderData
        )->addTo(
            $order->getCustomerEmail(),
            $order->getCustomerName()
        );

        try {
            $this->transportBuilder->getTransport()->sendMessage();
            $reminder->setStatus(Reminder::ADVANCEDRAR_STATUS_SENT);
        } catch (MailException $e) {
            $reminder->setStatus(Reminder::ADVANCEDRAR_STATUS_ERROR);
        }

        $this->inlineTranslation->resume();

        $this->saveReminder($reminder);

        return true;
    }

    private function saveReminder($reminder)
    {
        if ($this->developHelper->isReminderSendingIsImmutable()) {
            return;
        }

        $senderData = $this->senderData->get($this->getOrder()->getStoreId());
        $emailTemplate = $this->_appState->emulateAreaCode(
            'frontend',
            [$this, 'getEmailTemplate'],
            [$this->getOrder()->getStoreId()]
        );

        $emailTemplate->setSenderName($senderData['name'])
                      ->setSenderEmail($senderData['email'])
            ->setReplyTo($senderData['email']);

        $this->_appState->emulateAreaCode(
            'frontend',
            [$this, 'getProcessedTemplate'],
            [$emailTemplate, $this->getVars($this->getOrder())]
        );

        $reminder->setSubject($this->proccessedTemplate['subject'])
            ->setBody($this->proccessedTemplate['body'])
            ->setScheduledAt($this->date->gmtDate('Y-m-d'))
            ->save();
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return array|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getVars($order)
    {
        if (null === $this->templateVars) {
            $products = $this->getProductsFromOrder($order);

            $customerName = $order->getBillingAddress()
                ? $order->getBillingAddress()->getName()
                : $order->getCustomerName();

            $this->templateVars = [
                'customer'        => $this->getCustomer(),
                'customerName'    => $customerName,
                'mostExpensive'   => $this->getMostExpensive(),
                'productName'     => $this->getProductName($order),
                'products'        => $products,
                'relatedProducts' => $this->getProductsRelatedProducts->execute($products),
                'reminder'        => $this->getReminder(),
                'reminderStore'   => $this->storeManager->getStore($order->getStoreId()),
                'unsubscribe_url' => $this->urlHelper->getUnsubscribeUrl($order),
            ];
        }

        return $this->templateVars;
    }

    private function getProductName($order)
    {
        foreach ($this->getAvailableOrderItems($order) as $item) {
            return $item->getName();
        }
    }

    public function setStoreId($storeId)
    {
        $this->storeId = (int)$storeId;
        return $this;
    }

    public function getStoreId()
    {
        if (null === $this->storeId) {
            $this->storeId = $this->storeManager->getStore()->getStoreId();
        }

        return $this->storeId;
    }

    public function getEmailTemplate($storeId = null)
    {
        if ($storeId === null) {
            $storeId = $this->getStoreId();
        }

        switch ($this->getReminder()->getReminder()) {
            case Reminder::ADVANCEDRAR_SECOND_REMINDER:
                $templateCode = $this->reminderConfig->getSecondReminderTemplate($storeId);
                break;
            case Reminder::ADVANCEDRAR_THIRD_REMINDER:
                $templateCode = $this->reminderConfig->getThirdReminderTemplate($storeId);
                break;
            default:
                $templateCode = $this->reminderConfig->getFirstReminderTemplate($storeId);
                break;
        }

        return $this->getEmailTemplateByCode($templateCode);
    }

    private function getEmailTemplateByCode($emailTemplateCode)
    {
        $emailTemplate = $this->emailTemplateFactory->create();

        if (is_numeric($emailTemplateCode)) {
            return $emailTemplate->load($emailTemplateCode);
        }

        return $emailTemplate->loadDefault($emailTemplateCode);
    }

    public function getProcessedTemplate($emailTemplate, $vars = null)
    {
        $this->proccessedTemplate['subject'] = $emailTemplate->getProcessedTemplateSubject($vars);
        $this->proccessedTemplate['body'] = $emailTemplate->getProcessedTemplate($vars);
    }

    private function getReminderLabel()
    {
        switch ($this->getReminder()->getReminder()) {
            case Reminder::ADVANCEDRAR_SECOND_REMINDER:
                $label = 'review_reminder/second_reminder';
                break;
            case Reminder::ADVANCEDRAR_THIRD_REMINDER:
                $label = 'review_reminder/third_reminder';
                break;
            default:
                $label = 'review_reminder/first_reminder';
                break;
        }
        return $label;
    }

    public function getAvailableOrderItems($order)
    {
        return $this->dataHelper->getAvailableOrderItems($order);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Catalog\Model\Product[]
     */
    public function getProductsFromOrder($order): array
    {
        if (! $this->productsFromOrder) {
            $expensive = 0;
            $expensiveProduct = false;
            foreach ($this->getAvailableOrderItems($order) as $item) {
                if ($item->getPrice() > $expensive) {
                    $expensive = $item->getPrice();
                    $expensiveProduct = $item->getName();
                }
            }

            $this->productsFromOrder = $this->getProductsFromOrder->execute($order);

            if ($expensiveProduct) {
                $this->setMostExpensive($expensiveProduct);
            }
        }

        return $this->productsFromOrder;
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        if (empty($this->customer)) {
            $order = $this->getOrder();
            if ($order->getCustomerId()) {
                $this->customer = $this->customerFactory->create()->load($order->getCustomerId());
            }
        }

        return $this->customer;
    }

    /**
     * Reset temporally information
     *
     * @return $this
     */
    private function reset(): self
    {
        $this->productsFromOrder = null;
        $this->customer = null;
        $this->proccessedTemplate = null;
        $this->templateVars = null;
        $this->storeId = null;

        return $this;
    }
}
