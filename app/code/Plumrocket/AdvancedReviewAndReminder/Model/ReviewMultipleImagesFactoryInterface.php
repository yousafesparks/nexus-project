<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Plumrocket\AdvancedReviewAndReminder\Model\Review\Image as ReviewImage;

/**
 * Interface ReviewImagesFactoryInterface
 *
 * @since 2.0.0
 */
interface ReviewMultipleImagesFactoryInterface
{
    /**
     * Create resized images by review and image type
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $advancedReview
     * @param string                                                                 $imageId
     * @return ReviewImage[]
     */
    public function create(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $advancedReview,
        string $imageId
    ) : array;
}
