<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product;

class OrderCollection extends \Magento\Sales\Model\ResourceModel\Order\Collection
{
    /**
     * @param      $productId
     * @param bool $groupByOrder
     * @return $this
     */
    public function addProductFilter($productId, $groupByOrder = true)
    {
        $this->getSelect()
            ->join(
                ['sfoi' => $this->getResource()->getTable('sales_order_item')],
                'main_table.entity_id = sfoi.order_id AND sfoi.product_id = ' . $productId,
                ['product_id', 'product_options']
            );

        if ($groupByOrder) {
            $this->getSelect()->group('main_table.entity_id');
        }

        return $this;
    }
}
