<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\UnsubscribeReason;

use Magento\Framework\Serialize\SerializerInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Collection constructor.
     *
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param SerializerInterface $serializer
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->serializer = $serializer;
    }

    public function _construct()
    {
        $this->_init(
            \Plumrocket\AdvancedReviewAndReminder\Model\UnsubscribeReason::class,
            \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\UnsubscribeReason::class
        );
    }

    /**
     * @inheritDoc
     */
    protected function beforeAddLoadedItem(\Magento\Framework\DataObject $item)
    {
        $storeLabel = $item->getData(UnsubscribeReasonInterface::STORE_LABEL);

        if (! empty($storeLabel)) {
            $item->setStoreLabels((array) $this->serializer->unserialize($storeLabel));
        }

        return parent::beforeAddLoadedItem($item);
    }
}
