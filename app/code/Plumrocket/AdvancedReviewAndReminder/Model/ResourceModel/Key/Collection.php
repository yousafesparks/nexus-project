<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Key;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        parent::_construct();
        $this->_init(
            \Plumrocket\AdvancedReviewAndReminder\Model\Key::class,
            \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Key::class
        );
    }
}
