<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data;

/**
 * Class SelectModifier
 * Contain joins for related tables
 */
class SelectModifier
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\GetAdvancedEnabledFields
     */
    private $getAdvancedEnabledFields;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Rating\IsSingleRating
     */
    private $isSingleRating;

    /**
     * SelectModifier constructor.
     *
     * @param \Magento\Store\Model\StoreManagerInterface                                  $storeManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\GetAdvancedEnabledFields $getAdvancedEnabledFields
     * @param \Magento\Framework\App\ResourceConnection                                   $resourceConnection
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Rating\IsSingleRating           $isSingleRating
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\GetAdvancedEnabledFields $getAdvancedEnabledFields,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Plumrocket\AdvancedReviewAndReminder\Model\Rating\IsSingleRating $isSingleRating
    ) {
        $this->storeManager = $storeManager;
        $this->getAdvancedEnabledFields = $getAdvancedEnabledFields;
        $this->resourceConnection = $resourceConnection;
        $this->isSingleRating = $isSingleRating;
    }

    /**
     * @param string                       $tableName
     * @param \Magento\Framework\DB\Select $select
     * @param int|null                     $storeId
     * @return $this
     */
    public function joinAggregateRating(
        string $tableName,
        \Magento\Framework\DB\Select $select,
        int $storeId = null
    ) : self {
        $ratingTable = $this->resourceConnection->getTableName('pl_advanced_review_aggregate_rating');
        if (null === $storeId) {
            $storeId = $this->isSingleRating->execute() ? 0 : $this->storeManager->getStore()->getId();
        }

        $select->joinLeft(
            ['aggregate' => $ratingTable],
            "aggregate.review_id = {$tableName}.review_id AND aggregate.store_id = $storeId",
            ['aggregate_rating_percent' => 'percent']
        );

        return $this;
    }

    /**
     * @param string                       $tableName
     * @param \Magento\Framework\DB\Select $select
     * @param bool                         $onlyEnabledFields
     * @return $this
     */
    public function joinAdvancedReviewTable(
        string $tableName,
        \Magento\Framework\DB\Select $select,
        bool $onlyEnabledFields = false
    ) : self {
        $select->joinLeft(
            ['advanced_review' => $this->resourceConnection->getTableName('pl_advanced_review')],
            $tableName . '.review_id = advanced_review.review_id',
            $onlyEnabledFields ? $this->getAdvancedEnabledFields->execute() : ['*']
        );
        $select->columns(
            'IF(advanced_review.review_id, advanced_review.review_id, ' . $tableName . '.review_id ) AS review_id'
        );
        $select->columns(
            ['helpful_subtracted' => new \Zend_Db_Expr('(helpful_positive - helpful_negative)')]
        );

        return $this;
    }
}
