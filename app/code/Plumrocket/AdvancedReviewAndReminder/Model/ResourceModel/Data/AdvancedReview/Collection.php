<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\AdvancedReview;

use Magento\Framework\DB\Select;

/**
 * Class Collection
 * @method \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface[] getItems()
 */
class Collection extends \Magento\Review\Model\ResourceModel\Review\Collection
{
    /**
     * @param int $value
     * @return \Magento\Framework\DB\Select
     */
    public function addMostHelpFulFilter($value): Select
    {
        return $this->getSelect()->where('(helpful_positive - helpful_negative) >= ?', $value);
    }

    protected function _construct()
    {
        $this->_init(
            \Plumrocket\AdvancedReviewAndReminder\Model\Data\AdvancedReview::class,
            \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\AdvancedReview::class
        );
    }

    /**
     * @return \Magento\Review\Model\ResourceModel\Review\Collection
     */
    protected function _afterLoad()
    {
        foreach ($this->getItems() as $review) {
            $review->initRating();
        }

        return parent::_afterLoad();
    }
}
