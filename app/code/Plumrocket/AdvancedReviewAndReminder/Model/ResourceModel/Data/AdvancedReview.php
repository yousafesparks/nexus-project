<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data;

class AdvancedReview extends \Magento\Review\Model\ResourceModel\Review
{
    /**
     * @var string
     */
    private $aggregateRatingTable;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\SelectModifier
     */
    private $selectModifier;

    /**
     * AdvancedReview constructor.
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context                             $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                                   $date
     * @param \Magento\Store\Model\StoreManagerInterface                                    $storeManager
     * @param \Magento\Review\Model\RatingFactory                                           $ratingFactory
     * @param \Magento\Review\Model\ResourceModel\Rating\Option                             $ratingOptions
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\SelectModifier $selectModifier
     * @param null                                                                          $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Review\Model\ResourceModel\Rating\Option $ratingOptions,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\SelectModifier $selectModifier,
        $connectionName = null
    ) {
        parent::__construct($context, $date, $storeManager, $ratingFactory, $ratingOptions, $connectionName);
        $this->selectModifier = $selectModifier;
    }

    /**
     * Define main table. Define other tables name
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->aggregateRatingTable = $this->getTable('pl_advanced_review_aggregate_rating');
    }

    /**
     * @param string                                 $field
     * @param mixed                                  $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Framework\DB\Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        $this->selectModifier
            ->joinAdvancedReviewTable($this->getMainTable(), $select)
            ->joinAggregateRating($this->getMainTable(), $select);

        return $select;
    }
}
