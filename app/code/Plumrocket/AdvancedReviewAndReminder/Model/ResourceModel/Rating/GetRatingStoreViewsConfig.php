<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating;

/**
 * Class GetRatingStoreViewsConfig
 *
 * Retrieve array with stores on witch ratings enabled
 * [ storeId => [ratingId => true], storeId => [...] ]
 * for groupByStores - false
 * [ ratingId => [storeId => true], ratingId => [...] ]
 *
 * @since 2.0.0
 */
class GetRatingStoreViewsConfig
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var array
     */
    private $localCache = [];

    /**
     * GetRatingStoreViewsConfig constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(\Magento\Framework\App\ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param bool $groupByStores
     * @return array
     */
    public function execute(bool $groupByStores = true) : array
    {
        $cacheKey = (int) $groupByStores;

        if (! isset($this->localCache[$cacheKey])) {
            $connection = $this->resourceConnection->getConnection();

            $select = $connection->select()
                ->from(
                    ['main_table' => $this->resourceConnection->getTableName('rating')],
                    'rating_id'
                )
                ->joinLeft(
                    ['rating_store' => $this->resourceConnection->getTableName('rating_store')],
                    'main_table.rating_id = rating_store.rating_id',
                    ['store_ids' => new \Zend_Db_Expr('GROUP_CONCAT(rating_store.store_id)')]
                )
                ->where('main_table.is_active = 1')
                ->where('rating_store.store_id IS NOT NULL')
                ->group('main_table.rating_id');

            $data = $connection->fetchAll($select);

            $this->localCache[0] = $this->formatResult($data, false);
            $this->localCache[1] = $this->formatResult($data, true);
        }

        return $this->localCache[$cacheKey];
    }

    /**
     * Add rating id as keys and store_id as sub array keys for easier manipulation in future
     *
     * @param array $data
     * @param bool  $groupByStores
     * @return array
     */
    private function formatResult(array $data, bool $groupByStores) : array
    {
        $result = [];

        if ($groupByStores) {
            foreach ($data as $ratingData) {
                $storeIds = explode(',', $ratingData['store_ids']);
                foreach ($storeIds as $storeId) {
                    $result[$storeId][$ratingData['rating_id']] = true;
                }
            }
        } else {
            foreach ($data as $ratingData) {
                $storeIds = explode(',', $ratingData['store_ids']);
                $result[$ratingData['rating_id']] = array_fill_keys($storeIds, true);
            }
        }

        return $result;
    }
}
