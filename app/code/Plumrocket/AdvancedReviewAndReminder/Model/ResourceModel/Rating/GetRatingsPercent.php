<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating;

class GetRatingsPercent
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetReviewIds
     */
    private $getReviewIds;

    /**
     * GetRatingStoreViewsConfig constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection                $resourceConnection
     * @param \Magento\Store\Model\StoreManagerInterface               $storeManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetReviewIds $getReviewIds
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetReviewIds $getReviewIds
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->storeManager = $storeManager;
        $this->getReviewIds = $getReviewIds;
    }

    /**
     * @param int      $productId
     * @param int|null $storeId
     * @return array
     */
    public function execute(int $productId, int $storeId = null) : array
    {
        $connection = $this->resourceConnection->getConnection();
        $reviewIds = $this->getReviewIds->execute($productId);

        $select = $connection->select()
            ->from(
                ['main_table' => $this->resourceConnection->getTableName('pl_advanced_review_aggregate_rating')],
                ['percent']
            )
            ->where('review_id IN(?)', $reviewIds);

            if (!$this->storeManager->isSingleStoreMode()) {
                $storeId = $storeId ?: $this->storeManager->getStore()->getId();
                $select->where('store_id = ?', $storeId);
            }

        return $connection->fetchCol($select);
    }
}
