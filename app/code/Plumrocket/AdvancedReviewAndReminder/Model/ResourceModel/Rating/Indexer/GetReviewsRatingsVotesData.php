<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer;

class GetReviewsRatingsVotesData
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * GetRatingStoreViewsConfig constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(\Magento\Framework\App\ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param array $reviewIds
     * @param array $ratingIds
     * @param bool  $formatData
     * @return array
     */
    public function execute(array $reviewIds, array $ratingIds, bool $formatData = false) : array
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()
            ->from(
                ['main_table' => $this->resourceConnection->getTableName('rating_option_vote')],
                ['review_id', 'rating_id', 'percent']
            )
            ->where('rating_id IN(?)', $ratingIds)
            ->where('review_id IN(?)', $reviewIds);

        $data = $connection->fetchAll($select);

        return $formatData ? $this->formatData($data) : $data;
    }

    /**
     * @param array $data
     * @return array
     */
    private function formatData(array $data) : array
    {
        $result = [];
        foreach ($data as $voteData) {
            $result[$voteData['review_id']][$voteData['rating_id']] = $voteData['percent'];
        }
        return $result;
    }
}
