<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\Indexer;

class Builder
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * GetRatingStoreViewsConfig constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(\Magento\Framework\App\ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param array $ids
     * @param array $data
     * @return bool
     */
    public function build(array $ids, array $data) : bool
    {
        $this->clear($ids);

        return $this->write($data);
    }

    /**
     * @param array $ids
     * @return $this
     */
    public function clear(array $ids) : self
    {
        $connection = $this->resourceConnection->getConnection();

        $connection->delete(
            $this->resourceConnection->getTableName('pl_advanced_review_aggregate_rating'),
            ['review_id IN (?)' => $ids]
        );

        return $this;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function write(array $data) : bool
    {
        if (! $data) {
            return false;
        }

        $connection = $this->resourceConnection->getConnection();

        $affectedRows = (int) $connection->insertMultiple(
            $this->resourceConnection->getTableName('pl_advanced_review_aggregate_rating'),
            $data
        );

        return count($data) === $affectedRows;
    }
}
