<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Helpfulvote;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'id';

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(
            \Plumrocket\AdvancedReviewAndReminder\Model\Helpfulvote::class,
            \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Helpfulvote::class
        );
    }

    /**
     * @inheritDoc
     */
    public function setIdFieldName($fieldName)
    {
        return $this->_setIdFieldName($fieldName);
    }
}
