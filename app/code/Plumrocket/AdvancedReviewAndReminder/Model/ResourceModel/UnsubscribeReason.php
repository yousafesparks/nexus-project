<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface;

class UnsubscribeReason extends AbstractDb
{
    const MAIN_TABLE = 'pl_advanced_unsubscribe_reason';

    const PRIMARY_FIELD = 'reason_id';

    protected $_idFieldName = self::PRIMARY_FIELD;

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_serializableFields = [
            UnsubscribeReasonInterface::STORE_LABEL => [new \Zend_Db_Expr('NULL'), []]
        ];
        $this->_init(self::MAIN_TABLE, self::PRIMARY_FIELD);
    }
}
