<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @method \Plumrocket\AdvancedReviewAndReminder\Model\Reminder getFirstItem()
 */
class Collection extends AbstractCollection
{
    public function _construct()
    {
        parent::_construct();
        $this->_init(
            \Plumrocket\AdvancedReviewAndReminder\Model\Reminder::class,
            \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder::class
        );
    }
}
