<?php
/**
 * @package     Plumrocket_Advanced_Review_And_Reminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://www.plumrocket.com)
 * @license     https://www.plumrocket.com/license/  End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\Grid;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview;

class Collection extends SearchResult
{
    /**
     * @inheritDoc
     */
    protected function _initSelect()
    {
        $this->getSelect()->joinLeft(
            ['review' => $this->getResource()->getTable(Advancedreview::TABLE_NAME)],
            'main_table.review_id=review.review_id'
        )->joinLeft(
            ['review_detail' => $this->getResource()->getTable('review_detail')],
            'main_table.review_id=review_detail.review_id'
        )->joinLeft(
            ['product' => $this->getResource()->getTable('catalog_product_entity')],
            'main_table.entity_pk_value=product.entity_id',
            ['sku']
        )->joinLeft(
            ['rating_vote' => $this->getResource()->getTable('rating_option_vote')],
            'main_table.review_id=rating_vote.review_id',
            ['percent', 'value']
        )->joinLeft(
            ['customer' => $this->getResource()->getTable('customer_entity')],
            'review_detail.customer_id=customer.entity_id',
            ['email']
        )->group('main_table.review_id');

        $fieldsMap = [
            'review_id' => 'main_table.review_id',
            'name' => 'product.name',
            'customer_id' => 'customer.entity_id',
            'created_at' => 'main_table.created_at',
            'store_id' => 'review_detail.store_id',
        ];

        foreach ($fieldsMap as $filter => $alias) {
            $this->addFilterToMap($filter, $alias);
        }

        return parent::_initSelect();
    }
}
