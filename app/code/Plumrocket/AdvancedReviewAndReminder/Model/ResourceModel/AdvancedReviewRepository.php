<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Review\Model\ResourceModel\Review;
use Magento\Store\Model\StoreManagerInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\ReviewSearchResultsInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\ReviewSearchResultsInterfaceFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\Data\AdvancedReviewFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\AdvancedReview as ResourceDataAdvancedReview;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\AdvancedReview\CollectionFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\SelectModifier;

class AdvancedReviewRepository implements \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface[]
     */
    private $instances = [];

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ReviewSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SelectModifier
     */
    private $selectModifier;

    /**
     * @var ResourceDataAdvancedReview
     */
    private $resourceModel;

    /**
     * @var AdvancedReviewFactory
     */
    private $advancedReviewFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Review
     */
    private $magentoReviewResourceModel;

    /**
     * AdvancedReviewRepository constructor.
     * phpcs:disable Generic.Files.LineLength
     *
     * @param CollectionFactory             $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface  $collectionProcessor
     * @param SelectModifier                $selectModifier
     * @param ResourceDataAdvancedReview    $resourceModel
     * @param AdvancedReviewFactory         $advancedReviewFactory
     * @param StoreManagerInterface         $storeManager
     * @param Review                        $magentoReviewResourceModel
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SelectModifier $selectModifier,
        ResourceDataAdvancedReview $resourceModel,
        AdvancedReviewFactory $advancedReviewFactory,
        StoreManagerInterface $storeManager,
        Review $magentoReviewResourceModel
    ) {
        // phpcs:enable Generic.Files.LineLength
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->selectModifier = $selectModifier;
        $this->resourceModel = $resourceModel;
        $this->advancedReviewFactory = $advancedReviewFactory;
        $this->storeManager = $storeManager;
        $this->magentoReviewResourceModel = $magentoReviewResourceModel;
    }

    /**
     * Create review
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review
    ) : \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface {
        try {
            unset($this->instances[$review->getId()]);
            $this->resourceModel->save($review);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('The review was unable to be saved. Please try again.'),
                $e
            );
        }

        unset($this->instances[$review->getId()]);
        return $this->get($review->getId());
    }

    /**
     * Get info about review by review id
     *
     * @param int      $reviewId
     * @param bool     $editMode
     * @param int|null $storeId
     * @param bool     $forceReload
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(
        $reviewId,
        $editMode = false,
        $storeId = null,
        $forceReload = false
    ) : \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface {
        if (! $reviewId) {
            throw new NoSuchEntityException(
                __("The review that was requested doesn't exist. Verify the review and try again.")
            );
        }

        if (! isset($this->instancesById[$reviewId]) || $forceReload) {
            /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Data\AdvancedReview $review */
            $review = $this->advancedReviewFactory->create();
            $this->resourceModel->load($review, $reviewId);
            if (! $review->getId()) {
                throw new NoSuchEntityException(
                    __("The review that was requested doesn't exist. Verify the review and try again.")
                );
            }

            $this->instances[$review->getId()] = $review;
        }

        return $this->instances[$reviewId];
    }

    /**
     * Delete Review
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function delete(\Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review) : bool
    {
        $reviewId = $review->getId();
        try {
            unset($this->instances[$reviewId]);
            $this->magentoReviewResourceModel->delete($review);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('The "%1" review couldn\'t be removed.', $reviewId)
            );
        }

        unset($this->instances[$reviewId]);

        return true;
    }

    /**
     * @param int $id
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById(int $id) : bool
    {
        $advancedReview = $this->get($id);
        return $this->delete($advancedReview);
    }

    /**
     * Get review list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @param int                                            $storeId
     * @param bool                                           $onlyEnabledFields
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\ReviewSearchResultsInterface
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria,
        int $storeId = null,
        bool $onlyEnabledFields = false
    ) : \Magento\Framework\Api\SearchResultsInterface {
        $collection = $this->initCollectionForList($storeId, $onlyEnabledFields);

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var ReviewSearchResultsInterface $searchResult */
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

    /**
     * @param      $storeId
     * @param bool $onlyEnabledFields
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\AdvancedReview\Collection
     */
    private function initCollectionForList($storeId, bool $onlyEnabledFields) : Data\AdvancedReview\Collection
    {
        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Data\AdvancedReview\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->setItemObjectClass(\Plumrocket\AdvancedReviewAndReminder\Model\Data\AdvancedReview::class);
        $collection->addFilter('main_table.entity_id', 1); // prevent sql join
        $collection->addStoreFilter((int) $storeId ?: $this->storeManager->getStore()->getId());

        $select = $collection->getSelect();
        $this->selectModifier
            ->joinAdvancedReviewTable('main_table', $select, $onlyEnabledFields)
            ->joinAggregateRating('main_table', $select, $storeId);
        $select->group('main_table.review_id');

        return $collection;
    }
}
