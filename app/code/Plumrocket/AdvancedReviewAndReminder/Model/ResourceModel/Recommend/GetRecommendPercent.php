<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Recommend;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend;

class GetRecommendPercent
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetReviewIds
     */
    private $getReviewIds;

    /**
     * GetRecommendPercent constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection                $resourceConnection
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetReviewIds $getReviewIds
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetReviewIds $getReviewIds
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->getReviewIds = $getReviewIds;
    }

    public function execute(int $productId, int $storeId = null) : array
    {
        $connection = $this->resourceConnection->getConnection();
        $reviewIds = $this->getReviewIds->execute($productId);

        $select = $connection->select()
            ->from(
                ['main_table' => $this->resourceConnection->getTableName('pl_advanced_review')],
                ['recommend']
            )
            ->where('review_id IN(?)', $reviewIds)
            ->where('recommend !=?', Recommend::NOT_SPECIFIED);

        return $connection->fetchCol($select);
    }
}
