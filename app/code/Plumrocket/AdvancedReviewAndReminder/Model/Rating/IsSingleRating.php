<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Rating;

use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingStoreViewsConfig;

class IsSingleRating implements \Plumrocket\AdvancedReviewAndReminder\Api\IsSingleRatingInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingStoreViewsConfig
     */
    private $getRatingStoreViewsConfig;

    /**
     * IsSingleRating constructor.
     *
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param GetRatingStoreViewsConfig                  $getRatingStoreViewsConfig
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        GetRatingStoreViewsConfig $getRatingStoreViewsConfig
    ) {
        $this->storeManager = $storeManager;
        $this->getRatingStoreViewsConfig = $getRatingStoreViewsConfig;
    }

    /**
     * Check if enabled only one rating
     *
     * @return bool
     */
    public function execute() : bool
    {
        if ($this->storeManager->isSingleStoreMode()) {
            return true;
        }

        return count($this->getRatingStoreViewsConfig->execute()) === 1;
    }
}
