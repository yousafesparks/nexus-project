<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Rating;

use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingStoreViewsConfig;

class IsRatingEnabled implements \Plumrocket\AdvancedReviewAndReminder\Api\IsRatingEnabledInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingStoreViewsConfig
     */
    private $getRatingStoreViewsConfig;

    /**
     * IsRatingEnabled constructor.
     *
     * @param GetRatingStoreViewsConfig $getRatingStoreViewsConfig
     */
    public function __construct(
        GetRatingStoreViewsConfig $getRatingStoreViewsConfig
    ) {
        $this->getRatingStoreViewsConfig = $getRatingStoreViewsConfig;
    }

    /**
     * Check if active some of the ratings for store view
     *
     * @param int|null $storeId
     * @return bool
     */
    public function execute(int $storeId = null) : bool
    {
        return array_key_exists((int) $storeId, $this->getRatingStoreViewsConfig->execute());
    }
}
