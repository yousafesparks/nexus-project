<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Order;

class GetDeliveryTime
{
    protected $ararOrderCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * GetDeliveryTime constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product\OrderCollectionFactory $orderCollectionFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                                          $productRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                                              $date
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product\OrderCollectionFactory $orderCollectionFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->ararOrderCollectionFactory = $orderCollectionFactory;
        $this->productRepository = $productRepository;
        $this->date = $date;
    }

    /**
     * @param $customerId
     * @param $productId
     * @return bool|false|int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute($customerId, $productId)
    {
        if ($item = $this->getOrder($customerId, $productId)) {
            $product = $this->productRepository->getById($productId);

            if ($product->getIsVirtual()) {
                $item->getGrandTotal();
                foreach ($item->getInvoiceCollection() as $invoice) {
                    foreach ($invoice->getAllItems() as $item) {
                        if ($item->getProductId() == $productId) {
                            $startUse = $invoice->getCreatedAt();
                            break 2;
                        }
                    }
                }
            } else {
                foreach ($item->getShipmentsCollection() as $shipping) {
                    foreach ($shipping->getAllItems() as $item) {
                        if ($item->getProductId() == $productId) {
                            $startUse = $shipping->getCreatedAt();
                            break 2;
                        }
                    }
                }
            }

            if (isset($startUse)) {
                $nowTime = strtotime(date($this->date->gmtDate()));
                $startUse = strtotime(date($startUse));
                return $nowTime - $startUse;
            }
        }
        return false;
    }

    /**
     * @param $customerId
     * @param $productId
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product\OrderCollection
     */
    protected function getOrder($customerId, $productId)
    {
        /**
         * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Product\OrderCollection $orderCollection
         */
        $orderCollection = $this->ararOrderCollectionFactory->create();

        $orderCollection->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter(
                'state',
                [
                    'nin' => [
                        \Magento\Sales\Model\Order::STATE_CANCELED,
                        \Magento\Sales\Model\Order::STATE_NEW
                    ]
                ]
            )
            ->addProductFilter($productId, false);

        return $orderCollection->getFirstItem();
    }
}
