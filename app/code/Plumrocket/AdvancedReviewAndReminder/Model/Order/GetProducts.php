<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Order;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Model\Order;
use Plumrocket\AdvancedReviewAndReminder\Helper\Data;
use Plumrocket\AdvancedReviewAndReminder\Model\GetProductsFromOrderInterface;

/**
 * Class GetProducts
 *
 * @since 1.2.0
 */
class GetProducts implements GetProductsFromOrderInterface
{
    /**
     * @var array
     */
    private $productsFromOrder = [];

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * GetProducts constructor.
     *
     * @param \Magento\Catalog\Api\ProductRepositoryInterface   $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder      $searchCriteriaBuilder
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Data $dataHelper
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Catalog\Model\Product[]
     */
    public function execute(Order $order) : array
    {
        $orderId = (int) $order->getId();

        if (! isset($this->productsFromOrder[$orderId])) {
            $productIds = [];
            $products = [];
            /** @var \Magento\Sales\Model\Order\Item $item */
            foreach ($this->dataHelper->getAvailableOrderItems($order) as $item) {
                $productIds[] = $item->getProductId();
            }

            if ($productIds) {
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter('entity_id', $productIds, 'in')
                    ->create();
                $searchResult = $this->productRepository->getList($searchCriteria);

                $products = $searchResult->getItems();
            }

            $this->productsFromOrder[$orderId] = $products;
        }

        return $this->productsFromOrder[$orderId];
    }
}
