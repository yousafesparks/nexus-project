<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model\Order;

/**
 * Class GetReminders
 *
 * @since 1.2.0
 */
class GetReminders implements \Plumrocket\AdvancedReviewAndReminder\Model\GetOrderRemindersInterface
{
    /**
     * @var array
     */
    private $ordersReminders = [];

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory
     */
    private $reminderCollectionFactory;

    /**
     * GetReminders constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory
    ) {
        $this->reminderCollectionFactory = $reminderCollectionFactory;
    }

    /**
     * Retrieve all reminders for order sorted by scheduled at
     *
     * @param \Magento\Sales\Model\Order $order
     * @param bool                       $forceReload
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\Reminder[]
     */
    public function execute(\Magento\Sales\Model\Order $order, bool $forceReload = false) : array
    {
        $orderId = (int) $order->getId();

        if (! isset($this->ordersReminders[$orderId])) {
            /**
             * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\Collection $reminderCollection
             */
            $reminderCollection = $this->reminderCollectionFactory->create();

            $reminderCollection->addFieldToFilter('order_id', $orderId);
            $reminderCollection->setOrder('scheduled_at');

            $this->ordersReminders[$orderId] = array_values($reminderCollection->getItems());
        }

        return $this->ordersReminders[$orderId];
    }
}
