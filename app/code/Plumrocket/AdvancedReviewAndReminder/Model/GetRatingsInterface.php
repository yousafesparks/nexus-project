<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

/**
 * Interface GetRatingsInterface
 *
 * @since 1.2.0
 */
interface GetRatingsInterface
{
    /**
     * @return \Magento\Review\Model\Rating[]
     */
    public function execute() : array;
}
