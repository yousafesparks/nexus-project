<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Key as KeyResource;

/**
 * Class Key
 *
 * @deprecated since 1.2.0
 * @see \Plumrocket\Token\Api\CustomerRepositoryInterface
 * @see \Plumrocket\AdvancedReviewAndReminder\Model\Token\AutoLoginType
 */
class Key extends \Magento\Framework\Model\AbstractModel
{
    const LOGINKEY_QUERY_PARAM_NAME = 'alogin_secret';

    /**
     * @var KeyFactory
     */
    private $keyFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Magento\Framework\Math\Random
     */
    private $mathRandom;

    /**
     * @var KeyResource
     */
    private $keyResource;

    /**
     * @var ResourceModel\Key\CollectionFactory
     */
    private $keyCollectionFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * Key constructor.
     *
     * @param \Magento\Framework\Model\Context                                                $context
     * @param \Magento\Framework\Registry                                                     $registry
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\KeyFactory                          $keyFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Key                   $keyResource
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Key\CollectionFactory $keyCollectionFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                                     $date
     * @param \Magento\Framework\Math\Random                                                  $mathRandom
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig                     $reminderConfig
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null                    $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null                              $resourceCollection
     * @param array                                                                           $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        KeyFactory $keyFactory,
        KeyResource $keyResource,
        ResourceModel\Key\CollectionFactory $keyCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Math\Random $mathRandom,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->keyFactory = $keyFactory;
        $this->date = $date;
        $this->mathRandom = $mathRandom;
        $this->keyResource = $keyResource;
        $this->keyCollectionFactory = $keyCollectionFactory;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
        $this->reminderConfig = $reminderConfig;
    }

    protected function _construct()
    {
        $this->_init(\Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Key::class);
    }

    public function load($id, $field = null)
    {
        if (($field === null) && ! is_numeric($id)) {
            $field = 'secret';
        }

        return parent::load($id, $field);
    }

    public function generateNewKey($customerId)
    {
        if ($customerId) {
            $time = $this->date->timestamp();

            $secret = $this->getUnicSecret($customerId);
            try {
                $key = $this->keyFactory->create()->setData(
                    [
                        'customer_id'   => $customerId,
                        'secret'        => $secret,
                        'active_to'     => date('Y-m-d H:i:s', $time + $this->getActiveTime())
                    ]
                );

                $this->keyResource->save($key);

                return $key->getSecret();
            } catch (\Exception $e) {
                return false;
            }
        }

        return null;
    }

    /**
     * @param $customerId
     * @return string
     */
    protected function getUnicSecret($customerId)
    {
        do {
            $secret = $this->mathRandom->getRandomString(53);
            $key = $this->keyFactory->create()->load($secret);
        } while ($key->getId());

        return substr($secret, 0, 33) . $customerId . substr($secret, 33);
    }

    public function getActiveTime()
    {
        return $this->reminderConfig->getReviewReminderAutoLoginTimeFrame();
    }

    public function removeOld()
    {
        $time = $this->date->timestamp();

        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Key\Collection $collection */
        $collection = $this->keyCollectionFactory->create()
            ->addFieldToFilter('active_to', ['lt' => date('Y-m-d H:i:s', $time - $this->getActiveTime())])
            ->setPageSize(20);

        foreach ($collection as $item) {
            $item->delete();
        }

        return $this;
    }
}
