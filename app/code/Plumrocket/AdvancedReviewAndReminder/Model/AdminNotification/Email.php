<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\AdminNotification;

use Magento\Framework\App\Area;
use Magento\Framework\Exception\NoSuchEntityException;

class Email implements \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var int
     */
    private $storeId;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $appEmulation;

    /**
     * @var \Magento\Framework\View\Element\BlockFactory
     */
    private $blockFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Email\SenderData
     */
    private $senderData;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    private $backendUrlBuilder;

    /**
     * Email constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config          $configHelper
     * @param \Magento\Framework\Translate\Inline\StateInterface           $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder            $transportBuilder
     * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
     * @param \Magento\Store\Model\App\Emulation                           $appEmulation
     * @param \Magento\Framework\View\Element\BlockFactory                 $blockFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface            $customerRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Email\SenderData $senderData
     * @param \Magento\Backend\Model\UrlInterface                          $backendUrlBuilder
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Framework\View\Element\BlockFactory $blockFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Plumrocket\AdvancedReviewAndReminder\Model\Email\SenderData $senderData,
        \Magento\Backend\Model\UrlInterface $backendUrlBuilder
    ) {
        $this->configHelper = $configHelper;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->appEmulation = $appEmulation;
        $this->blockFactory = $blockFactory;
        $this->customerRepository = $customerRepository;
        $this->senderData = $senderData;
        $this->backendUrlBuilder = $backendUrlBuilder;
    }

    /**
     * @param \Magento\Review\Model\Review                                              $review
     * @param \Magento\Catalog\Api\Data\ProductInterface|\Magento\Catalog\Model\Product $product
     */
    public function sendNotificationsOfPendingReview(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product
    ) {
        if (! $this->configHelper->isEnabledPendingReviewEmailNotifications()) {
            return;
        }

        $emailAddresses = $this->configHelper->getPendingReviewEmailAddresses();
        $emailTemplate = $this->configHelper->getPendingReviewEmailNotificationTemplate();

        $templateVars = [
            'product' => $product,
            'productUrl' => $product->getProductUrl(),
            'review' => $review,
            'isRatings' => null !== $review->getRatings(),
            'review_url' => $this->backendUrlBuilder->getRouteUrl(
                'review/product/edit',
                ['id' => $review->getId(), 'productId' => $product->getId()]
            ),
            'pendingReviewsUrl' => $this->backendUrlBuilder->getRouteUrl(
                'review/product/pending',
                ['from_email' => 1]
            ),
            'productImageUrl' => $this->getProductImageUrl($product),
        ];

        foreach ($emailAddresses as $emailAddress) {
            $this->sendAdminEmailNotification($emailTemplate, $emailAddress, $templateVars);
        }
    }

    /**
     * @param \Magento\Review\Model\Review               $review
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param int                                        $customerId
     */
    public function sendNotificationsOfAbuse(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $customerId
    ) {
        if (! $this->configHelper->isEnabledReportAbuseEmailNotifications()) {
            return;
        }

        $emailAddresses = $this->configHelper->getReportAbuseEmailAddresses();
        $emailTemplate = $this->configHelper->getReportAbuseEmailNotificationTemplate();

        $customer = false;
        if ($customerId) {
            try {
                $customer = $this->customerRepository->getById($customerId);
            } catch (NoSuchEntityException $noSuchEntityException) {} //@codingStandardsIgnoreLine
        }

        $templateVars = [
            'product' => $product,
            'productUrl' => $product->getProductUrl(),
            'review' => $review,
            'review_url' => $this->backendUrlBuilder->getRouteUrl(
                'review/product/edit',
                ['id' => $review->getId(), 'productId' => $product->getId()]
            ),
            'productImageUrl' => $this->getProductImageUrl($product),
            'adminUrl' => $this->backendUrlBuilder->getRouteUrl('adminhtml'),
            'isGuest' => ! $customer,
            'isCustomer' => (bool) $customer,
            'customerFullName' => $customer ? $customer->getFirstname() . ' ' . $customer->getLastname() : '',
            'customerId' => $customer ? $customer->getId() : '',
        ];

        foreach ($emailAddresses as $emailAddress) {
            $this->sendAdminEmailNotification($emailTemplate, $emailAddress, $templateVars);
        }
    }

    /**
     * @param       $template
     * @param       $emailAddress
     * @param array $vars
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface
     */
    private function sendAdminEmailNotification($template, $emailAddress, array $vars = [])
    {
        $this->inlineTranslation->suspend();

        $this->transportBuilder->setTemplateIdentifier(
            $template
        )->setTemplateOptions(
            [
                'area'  => Area::AREA_FRONTEND,
                'store' => $this->storeManager->getStore()->getId(),
            ]
        )->setTemplateVars(
            $vars
        )->setFrom(
            $this->senderData->get($this->getStoreId())
        )->addTo(
            $emailAddress,
            $this->storeManager->getStore()->getName()
        );
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();

        $this->inlineTranslation->resume();

        return $this;
    }

    /**
     * @param        $product
     * @param string $imageType
     * @return string
     */
    private function getProductImageUrl($product, $imageType = 'product_page_image_medium')
    {
        $this->appEmulation->startEnvironmentEmulation($this->getStoreId(), Area::AREA_FRONTEND, true);

        /** @var \Magento\Catalog\Block\Product\ListProduct $imageBlock */
        $imageBlock = $this->blockFactory->createBlock(\Magento\Catalog\Block\Product\ListProduct::class);
        $productImage = $imageBlock->getImage($product, $imageType);
        $imageUrl = $productImage->getImageUrl();

        $this->appEmulation->stopEnvironmentEmulation();

        return $imageUrl;
    }

    /**
     * Method for change store id in email
     *
     * @param $storeId
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface
     */
    public function setStoreId($storeId)
    {
        $this->storeId = (int)$storeId;
        return $this;
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        if (null === $this->storeId) {
            $this->storeId = $this->storeManager->getStore()->getStoreId();
        }

        return $this->storeId;
    }
}
