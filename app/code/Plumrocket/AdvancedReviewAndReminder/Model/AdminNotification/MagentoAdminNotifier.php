<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model\AdminNotification;

use Magento\Framework\Exception\NoSuchEntityException;

class MagentoAdminNotifier implements \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Framework\Notification\NotifierInterface
     */
    private $adminNotifier;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    private $backendUrlBuilder;

    /**
     * MagentoAdminNotifier constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     * @param \Magento\Customer\Api\CustomerRepositoryInterface   $customerRepository
     * @param \Magento\Framework\Notification\NotifierInterface   $adminNotifier
     * @param \Magento\Backend\Model\UrlInterface                 $backendUrlBuilder
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Notification\NotifierInterface $adminNotifier,
        \Magento\Backend\Model\UrlInterface $backendUrlBuilder
    ) {
        $this->configHelper = $configHelper;
        $this->adminNotifier = $adminNotifier;
        $this->customerRepository = $customerRepository;
        $this->backendUrlBuilder = $backendUrlBuilder;
    }

    /**
     * @param \Magento\Review\Model\Review                                              $review
     * @param \Magento\Catalog\Api\Data\ProductInterface|\Magento\Catalog\Model\Product $product
     */
    public function sendNotificationsOfPendingReview(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product
    ) {
        if (! $this->configHelper->isEnabledMagentoNotifications()) {
            return;
        }

        $reviewUrl = $this->backendUrlBuilder->getRouteUrl(
            'review/product/edit',
            ['id' => $review->getId(), 'productId' => $product->getId()]
        );

        $this->adminNotifier->addNotice(
            __('New product review for %1 are available for your approval.', $product->getName()),
            __('Plumrocket Advanced Reviews Extension Notification'),
            $reviewUrl
        );
    }

    /**
     * @param \Magento\Review\Model\Review               $review
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param int                                        $customerId
     */
    public function sendNotificationsOfAbuse(
        \Magento\Review\Model\Review $review,
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $customerId
    ) {
        if (! $this->configHelper->isEnabledMagentoNotifications()) {
            return;
        }

        $customer = false;
        $customerFullName = '';

        if ($customerId) {
            try {
                $customer = $this->customerRepository->getById($customerId);
                $customerFullName = sprintf('%s %s', $customer->getFirstname(), $customer->getLastname());
            } catch (NoSuchEntityException $noSuchEntityException) {} //@codingStandardsIgnoreLine
        }

        $title = $customer
            ? __('Customer (%1, Customer ID: %2) reported an abusive review.', $customerFullName, $customerId)
            : __('Guest reported an abusive review.');

        $description = __(
            'This is an automated message notifying about review abuse complaints at your store. '
            . 'Plumrocket Advanced Reviews Extension Notification'
        );

        $reviewUrl = $this->backendUrlBuilder->getRouteUrl(
            'review/product/edit',
            ['id' => $review->getId(), 'productId' => $product->getId()]
        );

        $this->adminNotifier->addNotice($title, $description, $reviewUrl);
    }
}
