<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Model;

/**
 * Interface GetProductsFromOrderInterface
 *
 * @since 1.2.0
 */
interface GetProductsFromOrderInterface
{
    /**
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Catalog\Model\Product[]
     */
    public function execute(\Magento\Sales\Model\Order $order) : array;
}
