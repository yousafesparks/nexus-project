<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

use Magento\Review\Helper\Data as ReviewHelper;
use Plumrocket\AdvancedReviewAndReminder\Helper\Data as DataHelper;
use Magento\Review\Model\Review as ModelReview;
use Magento\Review\Model\ReviewFactory;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\Rating\Option\VoteFactory as RatingVoteFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Serialize\SerializerInterface as Serializer;

/**
 * Class Advancedreview
 *
 * @method string getReviewId()
 * @method string getHelpfulNegative()
 * @method string getHelpfulPositive()
 *
 * @method $this setReviewId($reviewId)
 * @method $this setAbuse($tinyInt)
 * @method $this setAbusedCustomer($customerId)
 * @method $this setProduct(\Magento\Catalog\Model\Product $product)
 */
class Advancedreview extends \Magento\Framework\Model\AbstractModel
{
    private $reviewHelper;
    private $reviewFactory;
    private $ratingFactory;
    private $ratingVoteFactory;
    private $dataHelper;
    private $storeManager;
    private $productFactory;
    private $urlBuilder;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review
     */
    private $reviewResource;

    /**
     * @var ReminderFactory
     */
    private $reminderFactory;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * Advancedreview constructor.
     *
     * @param ReviewHelper $reviewHelper
     * @param ReviewFactory $reviewFactory
     * @param RatingFactory $ratingFactory
     * @param RatingVoteFactory $ratingVoteFactory
     * @param DataHelper $dataHelper
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $urlBuilder
     * @param ProductFactory $productFactory
     * @param ReminderFactory $reminderFactory
     * @param Context $context
     * @param Registry $registry
     * @param \Magento\Review\Model\ResourceModel\Review $reviewResource
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param Serializer $serializer
     * @param array $data
     */
    public function __construct(
        ReviewHelper $reviewHelper,
        ReviewFactory $reviewFactory,
        RatingFactory $ratingFactory,
        RatingVoteFactory $ratingVoteFactory,
        DataHelper $dataHelper,
        StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder,
        ProductFactory $productFactory,
        ReminderFactory $reminderFactory,
        Context $context,
        Registry $registry,
        \Magento\Review\Model\ResourceModel\Review $reviewResource,
        Serializer $serializer,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->reviewHelper = $reviewHelper;
        $this->reviewFactory = $reviewFactory;
        $this->ratingFactory = $ratingFactory;
        $this->ratingVoteFactory = $ratingVoteFactory;
        $this->dataHelper = $dataHelper;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
        $this->productFactory = $productFactory;
        $this->reminderFactory = $reminderFactory;
        $this->reviewResource = $reviewResource;
        $this->serializer = $serializer;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    public function _construct()
    {
        parent::_construct();
        $this->_init(ResourceModel\Advancedreview::class);
    }

    /**
     * @param int $id
     * @return Advancedreview
     */
    public function loadByReview($id)
    {
        return $this->load($id, 'review_id');
    }

    /**
     * @return array
     */
    public function getAbuseStatuses()
    {
        return [
            0 => __('No'),
            1 => __('Yes'),
        ];
    }

    /**
     * Get review statuses as option array
     *
     * @return array
     */
    public function getReviewStatusesOptionArray()
    {
        $result = [];
        foreach ($this->reviewHelper->getReviewStatuses() as $k => $v) {
            $result[] = ['value' => $k, 'label' => $v];
        }

        return $result;
    }

    public function saveReview()
    {
        $data = $this->getData();
        $rating = $this->getRating();
        $customerId = $this->getCustomerId();
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->getProduct();
        /** @var \Magento\Review\Model\Review $review */
        $review = $this->reviewFactory->create()->setData($data);

        $review->setEntityId($review->getEntityIdByCode(ModelReview::ENTITY_PRODUCT_CODE))
            ->setEntityPkValue($product->getId())
            ->setStatusId(ModelReview::STATUS_PENDING)
            ->setCustomerId($customerId)
            ->setStoreId($this->storeManager->getStore()->getId())
            ->setStores(
                [
                    $this->storeManager->getStore()->getId(),
                ]
            );
        $this->reviewResource->save($review);

        //Review saved several times, that's why added variable for feature saves
        $review->setAlreadySaved(true);

        foreach ($rating as $ratingId => $optionId) {
            $this->ratingFactory->create()
                ->setRatingId($ratingId)
                ->setReviewId($review->getId())
                ->setCustomerId($customerId)
                ->addOptionVote($optionId, $product->getId());
        }

        $this->reviewResource->aggregate($review);

        $ratingVotes = $this->_getRatingVotes($review->getReviewId());
        $review->setRatingVotes($ratingVotes);

        $status = $this->dataHelper->getDefaultStatus($ratingVotes);

        $review->setQuestion(null)
            ->setStatusId($status);

        $this->reviewResource->save($review)
            ->aggregate($review);

        $this->_eventManager->dispatch(
            'pr_arar_save_review_after',
            [
                'arar_review' => $this,
                'review' => $review,
                'status' => $status,
                'product' => $product,
            ]
        );

        return $review;
    }

    protected function _getRatingVotes($reviewId)
    {
        $votesCollection = $this->ratingVoteFactory->create()
            ->getResourceCollection()
            ->setReviewFilter($reviewId)
            ->setStoreFilter($this->storeManager->getStore()->getId())
            ->addRatingInfo($this->storeManager->getStore()->getId())
            ->load();

        return $votesCollection;
    }

    /**
     * Update review url rewrite
     * @deprecated since 2.0.0
     * @see \Plumrocket\AdvancedReviewAndReminder\Model\Review\UpdateSeoFriendlyUrl::execute
     *
     * @param \Magento\Review\Model\Review | null $review
     * @param bool                                $save
     * @return bool|Advancedreview
     */
    public function updateReviewUrl($review = null, $save = true)
    {
        if (null === $review) {
            $review = $this;
        }

        if (!$review->getTitle() || $review->getReviewUrlKey()) {
            return $this;
        }
        $product = $this->productFactory->create()
            ->setStoreId($review->getStoreId())
            ->load($review->getEntityPkValue());

        if (!$product->getId()) {
            return false;
        }

        $productUrlKey = $product->getUrlKey();
        if (!$productUrlKey) {
            $productUrlKey = $this->dataHelper->slugifyUrl($product->getName());
        }

        $originUrlKey = $this->dataHelper->getReviewUrlKey($review->getTitle());
        $_originUrlKey = $productUrlKey . '/' . $originUrlKey;

        $duplicates = $this->getCollection()
            ->addFieldToFilter('review_url_key_origin', $_originUrlKey);

        $index = (int)$duplicates->getSize();

        if ($index) {
            $reviewUrlKey = $this->dataHelper->getReviewUrlKey($review->getTitle(), $index);
        } else {
            $reviewUrlKey = $originUrlKey;
        }

        try {
            $review->setReviewUrlKey($productUrlKey . '/' . $reviewUrlKey)
                ->setReviewUrlKeyOrigin($productUrlKey . '/' . $originUrlKey);

            if ($save) {
                $review->save();
            }
        } catch (\Exception $e) {
            $this->_logger->debug($e->getMessage());
        }

        return $this;
    }

    /**
     * Get review url
     * @deprecated since 2.0.0
     * @see \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface
     *
     * @param \Magento\Review\Model\Review | int $review
     * @return string review url
     */
    public function getReviewUrl($review)
    {
        if (!is_object($review)) {
            $review = $this->reviewFactory->create()->load($review);
        }

        if (!is_object($review) || !$review->getReviewId()) {
            return false;
        }

        if (!$review->getReviewUrlKey()) {
            $this->updateReviewUrl($review);
        }

        if (!$review->getReviewUrlKey() || !$review->getTitle()) {
            return $this->urlBuilder->getUrl('review/product/view', ['id' => $review->getReviewId()]);
        }

        return $this->urlBuilder->getUrl() . DataHelper::REVIEW_URL_ROUTE . '/' . $review->getReviewUrlKey();
    }

    /**
     * Review fail save if customer id equal 0, therefore we must return null for guest
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->getData('customer_id') ?: null;
    }

    /**
     * @return array|null
     */
    public function getAttachImage()
    {
        if ($attachImage = $this->getData('attach_image')) {
            return $this->serializer->unserialize($attachImage);
        }
        return $attachImage;
    }
}
