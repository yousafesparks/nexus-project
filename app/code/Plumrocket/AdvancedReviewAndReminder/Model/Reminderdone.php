<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Model;

class Reminderdone extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init(\Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminderdone::class);
    }
}
