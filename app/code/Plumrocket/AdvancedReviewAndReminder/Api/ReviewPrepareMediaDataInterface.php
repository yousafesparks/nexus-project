<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Api;

interface ReviewPrepareMediaDataInterface
{
    /**
     * Move images into permanent folder
     * Cut YouTube id from video link
     *
     * @param array $data
     * @return array
     */
    public function execute(array $data) : array;
}
