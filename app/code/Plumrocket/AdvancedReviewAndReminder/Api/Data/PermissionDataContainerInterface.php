<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api\Data;

interface PermissionDataContainerInterface
{
    /**
     * Retrieve is "isAllow" been set
     *
     * @return bool
     */
    public function isDefinedAllow();

    /**
     * @return bool
     */
    public function isAllowed();

    /**
     * @param bool $isAllow
     * @return $this
     */
    public function setIsAllow($isAllow);

    /**
     * @return $this
     */
    public function resetData();

    /**
     * @return int
     */
    public function getReason();

    /**
     * @param int $reason
     * @return $this
     */
    public function setReason($reason);

    /**
     * @return bool
     */
    public function canShowFormForGuest();

    /**
     * @param bool $allow
     * @return $this
     */
    public function setCanShowFormForGuest($allow);
}
