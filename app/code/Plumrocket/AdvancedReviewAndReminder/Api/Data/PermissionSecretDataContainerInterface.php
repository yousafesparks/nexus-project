<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api\Data;

interface PermissionSecretDataContainerInterface
{
    /**
     * @return mixed
     */
    public function getKey();

    /**
     * @param string $key
     * @return $this
     */
    public function setKey($key);

    /**
     * @return mixed
     */
    public function getReminderId();

    /**
     * @param int $reminderId
     * @return $this
     */
    public function setReminderId($reminderId);
}
