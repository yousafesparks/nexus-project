<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api\Data;

/**
 * @since 2.0.0
 */
interface UnsubscribeReasonInterface
{
    const LABEL = 'label';
    const INPUT_TYPE = 'input_type';
    const STATUS = 'status';
    const STORE_LABEL = 'store_label';

    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @return int
     */
    public function getInputType(): int;

    /**
     * @return int
     */
    public function getStatus(): int;

    /**
     * @return array
     */
    public function getStoreLabels(): array;

    /**
     * @param string $label
     * @return self
     */
    public function setLabel(string $label): UnsubscribeReasonInterface;

    /**
     * @param int $type
     * @return self
     */
    public function setInputType(int $type): UnsubscribeReasonInterface;

    /**
     * @param int $status
     * @return self
     */
    public function setStatus(int $status): UnsubscribeReasonInterface;

    /**
     * @param array $labels
     * @return self
     */
    public function setStoreLabels(array $labels): UnsubscribeReasonInterface;
}
