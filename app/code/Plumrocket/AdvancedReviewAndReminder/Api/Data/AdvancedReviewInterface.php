<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api\Data;

/**
 * Interface AdvancedReviewInterface
 *
 * @since 2.0.0
 *
 * @method null|\Magento\Review\Model\ResourceModel\Rating\Option\Vote\Collection getRatingVotes()
 * @method int getStatusId()
 *
 * @method $this setRatingVotes(\Magento\Review\Model\ResourceModel\Rating\Option\Vote\Collection $voteCollection)
 * @method $this setStatusId(int $value)
 */
interface AdvancedReviewInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $value
     * @return mixed
     */
    public function setId($value);

    /**
     * @return int
     */
    public function getCustomerId() : int;

    /**
     * @param int $customerId
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setCustomerId(int $customerId) : AdvancedReviewInterface;

    /**
     * Alias for getEntityPkValue
     *
     * @return int
     */
    public function getProductId() : int;

    /**
     * Alias for setEntityPkValue
     *
     * @param int $productId
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setProductId(int $productId) : AdvancedReviewInterface;

    /**
     * @return string
     */
    public function getNickname() : string;

    /**
     * @param string $nickname
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setNickname(string $nickname) : AdvancedReviewInterface;

    /**
     * @return string
     */
    public function getTitle() : string;

    /**
     * @param string $title
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setTitle(string $title) : AdvancedReviewInterface;

    /**
     * @return string
     */
    public function getDetail() : string;

    /**
     * @param string $detail
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setDetail(string $detail) : AdvancedReviewInterface;

    /**
     * @return string
     */
    public function getPros() : string;

    /**
     * @param string $pros
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setPros(string $pros) : AdvancedReviewInterface;

    /**
     * @return string
     */
    public function getCons() : string;

    /**
     * @param string $cons
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setCons(string $cons) : AdvancedReviewInterface;

    /**
     * @return bool
     */
    public function isVerified() : bool;

    /**
     * @return bool
     */
    public function getVerified() : bool;

    /**
     * @param string $verified
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setVerified(string $verified) : AdvancedReviewInterface;

    /**
     * @return int
     */
    public function getAggregateRating() : int;

    /**
     * @param int $aggregateRating
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setAggregateRating(int $aggregateRating) : AdvancedReviewInterface;

    /**
     * @return int
     */
    public function getAggregateRatingPercent() : int;

    /**
     * @param int $aggregateRatingPercent
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setAggregateRatingPercent(int $aggregateRatingPercent) : AdvancedReviewInterface;

    /**
     * Total helpfulness by positive and negative votes
     *
     * @return int
     */
    public function getHelpfulTotal();

    /**
     * @return int
     */
    public function getHelpfulPositive() : int;

    /**
     * @param string $helpfulPositive
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setHelpfulPositive(string $helpfulPositive) : AdvancedReviewInterface;

    /**
     * @return int
     */
    public function getHelpfulNegative() : int;

    /**
     * @param string $helpfulNegative
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setHelpfulNegative(string $helpfulNegative) : AdvancedReviewInterface;

    /**
     * @return string
     */
    public function getAdminComment() : string;

    /**
     * @param string $adminComment
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setAdminComment(string $adminComment) : AdvancedReviewInterface;

    /**
     * @return string
     */
    public function getAdminCommentDate() : string;

    /**
     * @param string $adminCommentDate
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setAdminCommentDate(string $adminCommentDate) : AdvancedReviewInterface;

    /**
     * @return array[]
     */
    public function getAttachImage() : array;

    /**
     * @param string $attachImage
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function setAttachImage(string $attachImage) : AdvancedReviewInterface;

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    public function initRating() : AdvancedReviewInterface;
}
