<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api\Data;

/**
 * Interface ReviewSearchResultsInterface
 *
 * @since 2.2.0
 */
interface ReviewSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get reviews list.
     *
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface[]
     */
    public function getItems();

    /**
     * Set reviews list.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
