<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api\Data;

/**
 * @since 2.0.0
 */
interface SubscriberInterface
{
    const EMAIL = 'email';
    const VISITOR_TYPE = 'type';
    const STATUS = 'status';
    const REASON = 'reason';
    const REASON_LABEL = 'reason_label';
    const CREATED_AT = 'created_at';
    const WEBSITE = 'website_id';
    const UPDATED_AT = 'updated_at';
    const CUSTOMER_ID = 'customer_id';

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return int
     */
    public function getVisitorType(): int;

    /**
     * @return int
     */
    public function getStatus(): int;

    /**
     * @return string
     */
    public function getReason(): string;

    /**
     * @return string
     */
    public function getReasonLabel(): string;

    /**
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * @return int
     */
    public function getWebsiteId(): int;

    /**
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * @return int
     */
    public function getCustomerId(): int;

    /**
     * @param string $email
     * @return SubscriberInterface
     */
    public function setEmail(string $email): SubscriberInterface;

    /**
     * @param int $type
     * @return SubscriberInterface
     */
    public function setVisitorType(int $type): SubscriberInterface;

    /**
     * @param int $status
     * @return SubscriberInterface
     */
    public function setStatus(int $status): SubscriberInterface;

    /**
     * @param string $reason
     * @return SubscriberInterface
     */
    public function setReason(string $reason): SubscriberInterface;

    /**
     * @param string $reasonLabel
     * @return SubscriberInterface
     */
    public function setReasonLabel(string $reasonLabel): SubscriberInterface;

    /**
     * @param string $createdAt
     * @return SubscriberInterface
     */
    public function setCreatedAt(string $createdAt): SubscriberInterface;

    /**
     * @param int $websiteId
     * @return SubscriberInterface
     */
    public function setWebsiteId(int $websiteId): SubscriberInterface;

    /**
     * @param string $updatedAt
     * @return SubscriberInterface
     */
    public function setUpdatedAt(string $updatedAt): SubscriberInterface;

    /**
     * @param int $customerId
     * @return SubscriberInterface
     */
    public function setCustomerId(int $customerId): SubscriberInterface;
}
