<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Api;

/**
 * Interface AutoLoginManagerInterface
 *
 * @since 1.2.0
 */
interface AutoLoginManagerInterface
{
    const QUERY_PARAM_NAME = 'alogin_secret';

    /**
     * @param string                                                    $route
     * @param array                                                     $params
     * @param \Magento\Customer\Model\Customer|int|null                 $customer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder|null $reminder
     * @return string
     */
    public function getUrl($route = '', $params = [], $customer = null, $reminder = null) : string;

    /**
     * @param string                                                    $url
     * @param \Magento\Customer\Model\Customer|int|null                 $customer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder|null $reminder
     * @return string
     */
    public function addSecretToUrl($url, $customer = null, $reminder = null) : string;

    /**
     * @param string $key
     * @return bool
     */
    public function loginByKey(string $key) : bool;

    /**
     * @param \Magento\Customer\Model\Customer|int $customer
     * @return string
     */
    public function getSecret($customer) : string;
}
