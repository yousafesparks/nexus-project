<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface;

/**
 * @since 2.0.0
 */
interface UnsubscribeReasonRepositoryInterface
{
    /**
     * Save reason
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface $reason
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface $reason
    ): UnsubscribeReasonInterface;

    /**
     * Get reason by id
     *
     * @param int $id
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id): UnsubscribeReasonInterface;

    /**
     * Delete reason
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface $reason
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface $reason): bool;

    /**
     * @param int $id
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id): bool;

    /**
     * Retrieve reasons matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
