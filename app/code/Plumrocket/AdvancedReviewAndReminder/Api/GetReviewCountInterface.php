<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Api;

interface GetReviewCountInterface
{
    /**
     * @param int|null $productId
     * @param int|null $storeId
     * @param bool     $onlyApproved
     * @return int
     */
    public function execute(int $productId = null, int $storeId = null, bool $onlyApproved = true) : int;
}
