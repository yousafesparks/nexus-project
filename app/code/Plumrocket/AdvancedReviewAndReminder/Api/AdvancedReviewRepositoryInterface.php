<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

use Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface;

/**
 * Interface ReviewRepositoryInterface
 *
 * @since 2.0.0
 */
interface AdvancedReviewRepositoryInterface
{
    /**
     * Create product
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review
    ) : AdvancedReviewInterface;

    /**
     * Get info about product by product id
     *
     * @param int $reviewId
     * @param bool $editMode
     * @param int|null $storeId
     * @param bool $forceReload
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($reviewId, $editMode = false, $storeId = null, $forceReload = false) : AdvancedReviewInterface;

    /**
     * Delete product
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface $review) : bool;

    /**
     * @param int $id
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById(int $id) : bool;

    /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @param int                                            $storeId
     * @param bool                                           $onlyEnabledFields
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\ReviewSearchResultsInterface
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria,
        int $storeId = 0,
        bool $onlyEnabledFields = false
    ) : \Magento\Framework\Api\SearchResultsInterface;
}
