<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

/**
 * Interface ReviewAllowPostDataFilterInterface
 *
 * For complete filtering use @see CreateReviewFilterInterface
 */
interface ReviewAllowPostDataFilterInterface
{
    /**
     * Remove disallowed field for security
     *
     * @param array $data
     * @return array
     */
    public function filter(array $data) : array;
}
