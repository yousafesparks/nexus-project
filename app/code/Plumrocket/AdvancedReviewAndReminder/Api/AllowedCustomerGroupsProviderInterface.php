<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

interface AllowedCustomerGroupsProviderInterface
{
    /**
     * @return array
     */
    public function getList();

    /**
     * @return bool
     */
    public function isAllGroupsAllowed();
}
