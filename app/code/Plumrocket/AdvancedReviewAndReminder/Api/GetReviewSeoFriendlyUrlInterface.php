<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

/**
 * Interface GetReviewSeoFriendlyUrlInterface
 *
 * @since 2.0.0
 */
interface GetReviewSeoFriendlyUrlInterface
{
    /**
     * Get or create review seo url
     *
     * If second param false - just form review seo utl without any changes in review model
     * If second param true and review hasn't seo url - create, set in review and save to data base
     *
     * @param \Magento\Review\Model\Review $review
     * @param bool                         $updateReview
     * @return string
     */
    public function execute(\Magento\Review\Model\Review $review, bool $updateReview = true) : string;
}
