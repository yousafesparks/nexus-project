<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

interface PermissionValidatorInterface
{
    const IS_ALLOW_BY_DEFAULT = false;

    /**
     * @param int|null $productId
     * @param bool     $skipBoughtValidation
     * @return Data\PermissionDataContainerInterface
     */
    public function validateGuestPermission($productId = null, $skipBoughtValidation = false);

    /**
     * @param int|string $customerId
     * @param int        $customerGroupId
     * @param int        $productId
     * @param bool       $skipBoughtValidation
     * @return Data\PermissionDataContainerInterface
     */
    public function validateCustomerPermission(
        $customerId,
        $customerGroupId,
        $productId,
        $skipBoughtValidation = false
    );
}
