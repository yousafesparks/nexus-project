<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

interface GuestSecretDataProviderInterface
{
    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\Permission\Data\SecretContainer
     */
    public function getData();
}
