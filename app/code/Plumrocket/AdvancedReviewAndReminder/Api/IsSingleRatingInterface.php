<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Api;

interface IsSingleRatingInterface
{
    /**
     * Check if enabled only one rating
     *
     * @return bool
     */
    public function execute() : bool;
}
