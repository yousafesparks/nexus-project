<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Api;

/**
 * Clear review post data from admin fields, disabled fields, HTML tags
 *
 * @since 2.0.1
 */
interface CreateReviewFilterInterface
{
    /**
     * @param array $data
     * @param null  $store
     * @return array
     */
    public function filter(array $data, $store = null): array;
}
