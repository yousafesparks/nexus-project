<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Api;

interface IsRatingEnabledInterface
{
    /**
     * Check if active some of the ratings for store view
     *
     * @param int|null $storeId
     * @return bool
     */
    public function execute(int $storeId = null) : bool;
}
