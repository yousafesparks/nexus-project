<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface;

/**
 * @since 2.0.0
 */
interface SubscriberRepositoryInterface
{
    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface $subscriber
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface $subscriber
    ): SubscriberInterface;

    /**
     * Get Subscriber by id
     *
     * @param int $id
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id): SubscriberInterface;

    /**
     * Get Subscriber by email
     *
     * @param string email
     * @return \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByEmail($email): SubscriberInterface;

    /**
     * Delete subscriber
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface $subscriber
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface $subscriber): bool;

    /**
     * @param int $id
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id): bool;

    /**
     * Retrieve subscribers matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
