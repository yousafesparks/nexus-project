<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Api;

/**
 * Interface BoughtProductValidatorInterface
 *
 * @since 1.0.0
 */
interface BoughtProductValidatorInterface
{
    /**
     * @since 1.2.0
     *
     * @param int $productId
     * @param int $customerId
     * @return mixed
     */
    public function validate(int $productId, int $customerId);

    /**
     * @param integer $productId
     * @param integer $customerId
     * @return bool
     */
    public function isCustomerBoughtProduct($productId, $customerId);

    /**
     * @param                                             $productId
     * @param Data\PermissionSecretDataContainerInterface $secret
     * @return bool
     */
    public function isGuestBoughtProduct($productId, Data\PermissionSecretDataContainerInterface $secret);
}
