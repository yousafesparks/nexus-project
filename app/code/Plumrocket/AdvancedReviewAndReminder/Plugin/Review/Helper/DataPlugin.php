<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Helper;

class DataPlugin
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Permission\Validator
     */
    private $permissionValidator;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $httpRequest;

    /**
     * Data constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\Permission\Validator $permissionValidator,
        \Magento\Framework\App\RequestInterface $httpRequest
    ) {
        $this->configHelper = $configHelper;
        $this->permissionValidator = $permissionValidator;
        $this->httpRequest = $httpRequest;
    }

    /**
     * @param \Magento\Review\Helper\Data $subject
     * @param bool                        $result
     * @return bool
     */
    public function afterGetIsGuestAllowToWrite(\Magento\Review\Helper\Data $subject, $result)
    {
        if ($this->configHelper->isModuleEnabled()) {
            return $this->permissionValidator->validateGuestPermission(
                $this->httpRequest->getParam('id')
            )->isAllowed();
        }

        return $result;
    }
}
