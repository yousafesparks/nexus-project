<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Block\Adminhtml;

use Magento\Framework\View\LayoutInterface;
use Magento\Review\Block\Adminhtml\Edit;
use Magento\Review\Helper\Action\Pager;
use Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing\Columns\EditAction;

class EditPlugin
{
    /**
     * @var Pager
     */
    private $reviewActionPager;

    /**
     * EditPlugin constructor.
     * @param Pager $reviewActionPager
     */
    public function __construct(Pager $reviewActionPager)
    {
        $this->reviewActionPager = $reviewActionPager;
    }

    /**
     * @param Edit $subject
     * @param $result
     * @param LayoutInterface $layout
     */
    public function afterSetLayout(Edit $subject, $result, LayoutInterface $layout)
    {
        if ($subject->getRequest()->getParam(EditAction::PLUMROCKET_REDIRECT) === '1') {
            $subject->updateButton(
                'back',
                'onclick',
                'setLocation(\'' . $subject->getUrl('advancedrar/reviews/index') . '\')'
            );

            $reviewId = $subject->getRequest()->getParam('id');
            $prevId = $this->reviewActionPager->getPreviousItemId($reviewId);
            $nextId = $this->reviewActionPager->getNextItemId($reviewId);

            if ($prevId) {
                $subject->updateButton(
                    'previous',
                    'onclick',
                    'setLocation(\'' . $subject->getUrl(
                        'review/*/*',
                        [
                            'id' => $prevId,
                            'ret' => $subject->getRequest()->getParam('ret'),
                            EditAction::PLUMROCKET_REDIRECT => 1
                        ]
                    ) . '\')'
                );
            }

            if ($nextId) {
                $subject->updateButton(
                    'previous',
                    'onclick',
                    'setLocation(\'' . $subject->getUrl(
                        'review/*/*',
                        [
                            'id' => $nextId,
                            'ret' => $subject->getRequest()->getParam('ret'),
                            EditAction::PLUMROCKET_REDIRECT => 1
                        ]
                    ) . '\')'
                );
            }
        }
    }
}
