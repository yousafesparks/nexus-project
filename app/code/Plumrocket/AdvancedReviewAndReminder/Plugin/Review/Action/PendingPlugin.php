<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Action;

use Magento\Review\Controller\Adminhtml\Product\Pending as ViewReviewPendingAction;

class PendingPlugin
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    private $urlModel;

    public function __construct(\Magento\Backend\Model\UrlInterface $urlModel)
    {
        $this->urlModel = $urlModel;
    }

    /**
     * Disable secret key for link from email
     *
     * @param ViewReviewPendingAction $subject
     */
    public function before_processUrlKeys(ViewReviewPendingAction $subject) //@codingStandardsIgnoreLine
    {
        if ($subject->getRequest()->getParam('from_email')) {
            $this->urlModel->turnOffSecretKey();
        }
    }
}
