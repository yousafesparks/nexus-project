<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Model\ResourceModel;

class ReviewPlugin
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Indexer\ReviewAggregateRating
     */
    private $reviewAggregateRating;

    /**
     * ReviewPlugin constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                       $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Indexer\ReviewAggregateRating $reviewAggregateRating
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\Indexer\ReviewAggregateRating $reviewAggregateRating
    ) {
        $this->configHelper = $configHelper;
        $this->reviewAggregateRating = $reviewAggregateRating;
    }

    /**
     * @param \Magento\Review\Model\ResourceModel\Review $subject
     * @param \Magento\Review\Model\Review               $object
     */
    public function afterAggregate(
        \Magento\Review\Model\ResourceModel\Review $subject,
        $result,
        \Magento\Review\Model\Review $object
    ) {
        if ($this->configHelper->isModuleEnabled() && $object->getReviewId()) {
            $this->reviewAggregateRating->executeRow($object->getReviewId());
        }
    }
}
