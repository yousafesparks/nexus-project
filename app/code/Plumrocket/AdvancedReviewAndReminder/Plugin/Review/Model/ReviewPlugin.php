<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Model;

class ReviewPlugin
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface
     */
    private $boughtProductValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Order\GetDeliveryTime
     */
    private $getDeliveryTime;

    /**
     * ReviewPlugin constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                       $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Order\GetDeliveryTime         $getDeliveryTime
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator,
        \Plumrocket\AdvancedReviewAndReminder\Model\Order\GetDeliveryTime $getDeliveryTime
    ) {
        $this->configHelper = $configHelper;
        $this->boughtProductValidator = $boughtProductValidator;
        $this->getDeliveryTime = $getDeliveryTime;
    }

    /**
     * @param \Magento\Review\Model\Review $subject
     */
    public function beforeBeforeSave(\Magento\Review\Model\Review $subject)
    {
        if (! $this->configHelper->isModuleEnabled()) {
            return;
        }

        if ($this->isGuestReview($subject)
            || ! $this->isNew($subject)
            || ! $this->isProductReview($subject)
        ) {
            return;
        }

        $productId = (int)$subject->getEntityPkValue();
        $customerId = (int) $subject->getCustomerId();

        if ($this->boughtProductValidator->isCustomerBoughtProduct($productId, $customerId)) {
            $subject->setVerified(1);
            if ($deliveryTime = $this->getDeliveryTime->execute($customerId, $productId)) {
                $subject->setDeliveryTime($deliveryTime);
            }

        }
    }

    /**
     * @param \Magento\Review\Model\Review $subject
     * @return bool
     */
    private function isNew(\Magento\Review\Model\Review $subject)
    {
        return !$subject->getId();
    }

    /**
     * @param \Magento\Review\Model\Review $subject
     * @return bool
     */
    private function isGuestReview(\Magento\Review\Model\Review $subject)
    {
        return ! $subject->getCustomerId();
    }

    /**
     * @param \Magento\Review\Model\Review $subject
     * @return bool
     */
    private function isProductReview(\Magento\Review\Model\Review $subject)
    {
        return (int)$subject->getEntityId() === 1;
    }
}
