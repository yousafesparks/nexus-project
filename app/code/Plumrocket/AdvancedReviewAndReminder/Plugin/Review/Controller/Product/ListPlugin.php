<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Controller\Product;

/**
 * @since 2.0.0
 */
class ListPlugin
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    protected $productRetriever;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * ListPlugin constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $productRetriever
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $productRetriever
    ) {
        $this->configHelper = $configHelper;
        $this->productRetriever = $productRetriever;
    }

    /**
     * @param \Magento\Review\Controller\Product\ListAction $subject
     * @param $result
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function afterExecute(\Magento\Review\Controller\Product\ListAction $subject, $result)
    {
        if ($this->configHelper->isModuleEnabled() && $product = $this->productRetriever->execute()) {
            $reviewPage = (int) $subject->getRequest()->getParam('reviewPage');
            $result->getConfig()->getTitle()->set(
                $this->configHelper->getReviewListPageMetaTitleFormat($product, $reviewPage)
            );
            $result->getConfig()->setDescription(
                $this->configHelper->getReviewListPageMetaDescriptionFormat($product, $reviewPage)
            );
        }

        return $result;
    }
}
