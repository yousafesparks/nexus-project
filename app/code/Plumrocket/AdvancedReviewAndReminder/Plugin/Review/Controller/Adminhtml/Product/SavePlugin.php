<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Controller\Adminhtml\Product;

use Magento\Review\Controller\Adminhtml\Product\Save;
use Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing\Columns\EditAction;

class SavePlugin
{
    /**
     * @param Save $subject
     * @param $result
     */
    public function afterExecute(Save $subject, $result)
    {
        $request = $subject->getRequest();
        $referer = $request->getServer('HTTP_REFERER');

        if (false !== strpos($referer, EditAction::PLUMROCKET_REDIRECT . '/1')) {
            $nextId = (int)$request->getParam('next_item');
            $productId = (int)$request->getParam('productId');
            $customerId = (int)$request->getParam('customerId');

            if ($nextId) {
                $result->setPath(
                    'review/*/edit',
                    [
                        'id' => $nextId,
                        'ret' => $request
                            ->getParam('ret'),
                        EditAction::PLUMROCKET_REDIRECT => 1
                    ]
                );
            } elseif ($request->getParam('ret') !== 'pending' && ! $productId && ! $customerId) {
                $result->setPath('advancedrar/reviews/index');
            }
        }

        return $result;
    }
}
