<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Review\Controller\Adminhtml\Product;

use Magento\Review\Controller\Adminhtml\Product\Delete;
use Plumrocket\AdvancedReviewAndReminder\Ui\Component\Listing\Columns\EditAction;

class DeletePlugin
{
    /**
     * @param Delete $subject
     * @param $result
     */
    public function afterExecute(Delete $subject, $result)
    {
        $referer = $subject->getRequest()->getServer('HTTP_REFERER');
        $ret = $subject->getRequest()->getParam('ret');

        if ('pending' !== $ret && false !== strpos($referer, EditAction::PLUMROCKET_REDIRECT . '/1')) {
            $result->setPath('advancedrar/reviews/index');
        }

        return $result;
    }
}
