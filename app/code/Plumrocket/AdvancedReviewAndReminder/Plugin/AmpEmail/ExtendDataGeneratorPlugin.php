<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\AmpEmail;

/**
 * Class ExtendDataGeneratorPlugin
 * Extend AmpEmail generated data for test emails
 *
 * @since 1.2.0
 */
class ExtendDataGeneratorPlugin
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsFromOrderInterface
     */
    private $getProductsFromOrder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsRelatedProductsInterface
     */
    private $getProductsRelatedProducts;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetOrderRemindersInterface
     */
    private $getOrderReminders;

    /**
     * ExtendDataGeneratorPlugin constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsFromOrderInterface       $getProductsFromOrder
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsRelatedProductsInterface $getProductsRelatedProducts
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetOrderRemindersInterface          $getOrderReminders
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsFromOrderInterface $getProductsFromOrder,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetProductsRelatedProductsInterface $getProductsRelatedProducts,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetOrderRemindersInterface $getOrderReminders
    ) {
        $this->getProductsFromOrder = $getProductsFromOrder;
        $this->getProductsRelatedProducts = $getProductsRelatedProducts;
        $this->getOrderReminders = $getOrderReminders;
    }

    /**
     * @param Object $subject
     * @param array  $result
     * @return array
     */
    public function afterGenerate($subject, array $result) : array
    {
        if (isset($result['order'])
            && $result['order'] instanceof \Magento\Sales\Model\Order
        ) {
            $result = $this->addOrderProducts($result);
            if (! isset($result['reminderStore'])) {
                $result['reminderStore'] = $result['store'];
            }

            if (! isset($result['customerName'])) {
                $result['customerName'] = $result['order']->getCustomerName();
            }

            if (! isset($result['reminder']) && ($reminders = $this->getOrderReminders->execute($result['order']))) {
                $result['reminder'] = $reminders[0];
            }
        }

        return $result;
    }

    /**
     * @param array $result
     * @return array
     */
    public function addOrderProducts(array $result) : array
    {
        if (! isset($result['products'])) {
            $result['products'] = $this->getProductsFromOrder->execute($result['order']);
            $result['relatedProducts'] = $this->getProductsRelatedProducts->execute($result['products']);
        }

        return $result;
    }
}
