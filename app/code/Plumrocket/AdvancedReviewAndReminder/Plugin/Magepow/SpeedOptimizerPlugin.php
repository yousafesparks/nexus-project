<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Plugin\Magepow;

class SpeedOptimizerPlugin
{

    public function afterAddLazyloadImage(
        \Magepow\SpeedOptimizer\Plugin\SpeedOptimizer $subject,
        $result
    ) {
        return preg_replace_callback(
            '/({"component":"Plumrocket_AdvancedReviewAndReminder(.+?)<\/script>)/',
            function($match) {
                return str_replace('class="lazyload lazyload"', 'class=\"lazyload lazyload\"', $match[0]);
            },
            $result
        );
    }
}

