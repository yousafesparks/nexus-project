<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Encryption\EncryptorInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\PermissionValidatorInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\Reminder;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminderdone\CollectionFactory as ReminderdoneCollectionFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\AbstractPermission as PermissionTypes;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\SubscriberStatus;

class Permission extends \Magento\Framework\App\Helper\AbstractHelper
{
    const ALL_REASONS = 'all';
    const REASON_NO_REASON = 0; //Allow write review
    const REASON_CANNOT_WRITE = 1; //Disallow write review
    const REASON_ONLY_BUYER = 2; // Only users, who bought product can write review
    const REASON_ONLY_REGISTERED_BUYERS = 4; // Only registered customers, who bought product can write review

    /**
     * Reason why user can't write review
     * @var int
     */
    private $reason;

    /**
     * @var ReminderdoneCollectionFactory
     */
    private $reminderdoneCollectionFactory;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\PermissionValidatorInterface
     */
    private $permissionValidator;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory
     */
    private $reminderCollectionFactory;

    /**
     * @var boolean
     */
    private $canShowFormForGuest = false;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface
     */
    private $allowedCustomerGroupsProvider;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface
     */
    private $reminderSubscriberRepository;

    /**
     * @var \Magento\Framework\Encryption\Encryptor
     */
    private $encryptor;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * Permission constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     *
     * @param \Magento\Customer\Model\Session                                                      $customerSession
     * @param ReminderdoneCollectionFactory                                                        $reminderdoneCollectionFactory
     * @param \Magento\Framework\App\Helper\Context                                                $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                                  $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\PermissionValidatorInterface               $permissionValidator
     * @param \Magento\Customer\Api\CustomerRepositoryInterface                                    $customerRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface     $allowedCustomerGroupsProvider
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig                          $reminderConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface              $reminderSubscriberRepository
     * @param \Magento\Framework\Encryption\EncryptorInterface                                     $encryptor
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                                    $dataHelper
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        CustomerSession $customerSession,
        ReminderdoneCollectionFactory $reminderdoneCollectionFactory,
        Context $context,
        Config $configHelper,
        PermissionValidatorInterface $permissionValidator,
        CustomerRepositoryInterface $customerRepository,
        CollectionFactory $reminderCollectionFactory,
        AllowedCustomerGroupsProviderInterface $allowedCustomerGroupsProvider,
        ReminderConfig $reminderConfig,
        SubscriberRepositoryInterface $reminderSubscriberRepository,
        EncryptorInterface $encryptor,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->reminderdoneCollectionFactory = $reminderdoneCollectionFactory;
        $this->permissionValidator = $permissionValidator;
        $this->customerRepository = $customerRepository;
        $this->reminderCollectionFactory = $reminderCollectionFactory;
        $this->allowedCustomerGroupsProvider = $allowedCustomerGroupsProvider;
        $this->reminderConfig = $reminderConfig;
        $this->reminderSubscriberRepository = $reminderSubscriberRepository;
        $this->encryptor = $encryptor;
        $this->configHelper = $configHelper;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Is user have permissions to write review
     *
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean
     */
    public function allowWriteReview($customer = null, $product = null): bool
    {
        if (null === $customer) {
            $customer = $this->customerSession->getCustomer();
        }

        $productId = null !== $product ? $product->getId() : 0;

        if ($customer && $customer->getId()) {
            $result = $this->permissionValidator->validateCustomerPermission(
                $customer->getId(),
                $customer->getGroupId(),
                $productId
            );
        } else {
            $result = $this->permissionValidator->validateGuestPermission($productId);
        }

        $this->canShowFormForGuest = $result->canShowFormForGuest();
        $this->reason = $result->getReason();

        return $result->isAllowed();
    }

    /**
     * @return bool
     */
    public function isLoginAndSubmitApproach(): bool
    {
        $isGuest = 0 === $this->customerSession->getCustomerGroupId();
        $guestCannotWrite = PermissionTypes::PERMISSION_CANNOT_WRITE === $this->configHelper->getGuestPermission();

        return $isGuest && $guestCannotWrite && $this->allowedCustomerGroupsProvider->isAllGroupsAllowed();
    }

    /**
     * Generate secret key for guest
     *
     * @param  \Magento\Sales\Model\Order $order
     * @return string
     */
    public function generateSecretKey($order): string
    {
        return $this->encryptor->hash(
            $order->getId(),
            Encryptor::HASH_VERSION_MD5
        );
    }

    /**
     * Get reason for user
     *
     * @return int
     */
    public function getReason()
    {
        if ($this->reason === null) {
            //Run checking of user permission
            $this->allowWriteReview();
        }

        return $this->reason;
    }

    /**
     * Get reason html. Return text of reason why user can't write review
     *
     * @param string|int|null $reason
     * @return string|array
     */
    public function getReasonHtml($reason = null)
    {
        if (self::REASON_NO_REASON === $reason) {
            throw new \LogicException('Something wrong. You try get reason html for no reason.');
        }

        $reasonsHtml = [
            self::REASON_CANNOT_WRITE           => __('You can not write a review'),
            self::REASON_ONLY_BUYER             => __('Only users who bought product can write review.'),
            self::REASON_ONLY_REGISTERED_BUYERS => __(
                'Only registered users who purchased the product can write a review.'
            ),
        ];

        if (self::ALL_REASONS === $reason) {
            return $reasonsHtml;
        }

        return null !== $reason ? $reasonsHtml[$reason] : $reasonsHtml[$this->getReason()];
    }

    /**
     * Does reminder can be created for current order and guest/customer
     *
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    public function canCreateReminder(\Magento\Sales\Model\Order $order) : bool
    {
        if (! $this->reminderConfig->isReviewReminderEnabled() || ! $this->isSubscribed($order->getCustomerEmail())) {
            return false;
        }

        if ($customerId = $order->getCustomerId()) {
            try {
                $customer = $this->customerRepository->getById($customerId);

                $permissionData = $this->permissionValidator->validateCustomerPermission(
                    $customerId,
                    $customer->getGroupId(),
                    null,
                    true
                );
            } catch (\Magento\Framework\Exception\NoSuchEntityException $noSuchEntityException) {
                 $permissionData = $this->permissionValidator->validateGuestPermission(null, true);
            }
        } else {
            $permissionData = $this->permissionValidator->validateGuestPermission(null, true);
        }

        $reasonAfterWhichNoAdditionalChecksRequired = [self::REASON_CANNOT_WRITE, self::REASON_ONLY_REGISTERED_BUYERS];

        if (in_array($permissionData->getReason(), $reasonAfterWhichNoAdditionalChecksRequired, true)) {
            return false;
        }

        //There is observer for checking available items for current order
        if (empty($this->dataHelper->getAvailableOrderItems($order))) {
            return false;
        }

        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\Collection $reminderCollection */
        $reminderCollection = $this->reminderCollectionFactory->create()
            ->addFieldToFilter('store_id', $order->getStoreId())
            ->addFieldToFilter('order_id', $order->getId());

        foreach ($order->getAllItems() as $item) {
            if (!$item->getProductId()) {
                return false;
            }
        }

        $statuses = $this->reminderConfig->getReviewReminderAllowedStatuses($order->getStoreId());
        //Does reminder for current order already created
        //Also checking order status
        if ($reminderCollection->getSize() || ! in_array($order->getStatus(), $statuses, true)) {
            return false;
        }

        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminderdone\Collection $reminderDone */
        $reminderDone = $this->reminderdoneCollectionFactory->create()
            ->addFieldToFilter('order_id', $order->getId())
            ->addFieldToFilter('reminder', Reminder::ADVANCEDRAR_FIRST_REMINDER);

        //Reminder can be removed, but it saved in "reminderdone" table
        if ($reminderDone->getSize()) {
            return false;
        }

        $currentCustomerGroupId = $order->getCustomerGroupId();
        $permissionGroup = explode(',', $this->configHelper->getCustomerGroupsPermission($order->getStoreId()));

        if ($currentCustomerGroupId) {
            if (in_array($currentCustomerGroupId, $permissionGroup, false)) {
                return true;
            }
        } elseif (PermissionTypes::PERMISSION_CANNOT_WRITE !== $this->configHelper->getGuestPermission()) {
            return true;
        }

        return false;
    }

    /**
     * Can show form for guest
     *
     * @return boolean
     */
    public function canShowReviewFormForGuest(): bool
    {
        return $this->canShowFormForGuest;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function isSubscribed(string $email): bool
    {
        try {
            /** @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface $subscriber */
            $subscriber = $this->reminderSubscriberRepository->getByEmail($email);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return true;
        }

        return $subscriber->getStatus() === SubscriberStatus::SUBSCRIBED;
    }
}
