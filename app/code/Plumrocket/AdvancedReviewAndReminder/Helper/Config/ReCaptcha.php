<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Helper\Config;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Plumrocket\Base\Helper\ConfigUtils;

/**
 * @since 2.2.4
 */
class ReCaptcha extends ConfigUtils
{
    const XML_PATH_ONLY_FOR_GUESTS = 'recaptcha_frontend/type_for/pr_product_review_only_for_guest';

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param \Magento\Framework\App\Helper\Context     $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(Context $context, ObjectManagerInterface $objectManager)
    {
        parent::__construct($context);
        $this->objectManager = $objectManager;
    }

    /**
     * @param null $scopeCode
     * @param null $scopeType
     * @return bool
     */
    public function showOnlyForGuests($scopeCode = null, $scopeType = null): bool
    {
        return (bool) $this->getConfig(self::XML_PATH_ONLY_FOR_GUESTS, $scopeCode, $scopeType);
    }

    /**
     * Check if magento recaptcha enabled for product review
     *
     * @return bool
     * @throws \Magento\Framework\Exception\InputException
     */
    public function isEnabledMagentoReCaptcha(): bool
    {
        if ($this->_moduleManager->isEnabled('Magento_ReCaptchaUi')) {
            /** @var \Magento\ReCaptchaUi\Model\IsCaptchaEnabledInterface $isCaptchaEnabled */
            $isCaptchaEnabled = $this->objectManager->get(\Magento\ReCaptchaUi\Model\IsCaptchaEnabledInterface::class);
            return $isCaptchaEnabled->isCaptchaEnabledFor('product_review');
        }
        return false;
    }
}
