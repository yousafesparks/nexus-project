<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

/**
 * @since 1.2.0
 */
class Review extends \Plumrocket\Base\Helper\ConfigUtils
{
    const YOUTUBE_EMBED_URL = 'https://www.youtube.com/embed/';

    const YOUTUBE_IMG_URL = 'https://img.youtube.com/vi/';

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Framework\Url
     */
    private $frontUrlBuilder;

    /**
     * @var \Magento\Framework\Escaper
     */
    private $escaper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface
     */
    private $getReviewSeoFriendlyUrl;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * @param \Magento\Framework\App\Helper\Context                                      $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                        $configHelper
     * @param \Magento\Framework\Url                                                     $frontUrlBuilder
     * @param \Magento\Framework\Escaper                                                 $escaper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface $getReviewSeoFriendlyUrl
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                                $dateTime
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever    $currentProductRetriever
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig                $reminderConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Framework\Url $frontUrlBuilder,
        \Magento\Framework\Escaper $escaper,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface $getReviewSeoFriendlyUrl,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
    ) {
        parent::__construct($context);
        $this->configHelper = $configHelper;
        $this->frontUrlBuilder = $frontUrlBuilder;
        $this->escaper = $escaper;
        $this->getReviewSeoFriendlyUrl = $getReviewSeoFriendlyUrl;
        $this->dateTime = $dateTime;
        $this->currentProductRetriever = $currentProductRetriever;
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param bool                           $onLoadValidate
     * @return string
     */
    public function getWriteReviewUrl(\Magento\Catalog\Model\Product $product, $onLoadValidate = false) : string
    {
        if ($this->reminderConfig->isSingleFormEnabled()) {
            $params['id'] = $product->getId();
            if ($onLoadValidate) {
                $params['_fragment'] = 'arar-form-validation';
            }

            return $this->frontUrlBuilder->getUrl('advancedrar/review/leavereview', $params);
        }

        return $product->getProductUrl() . ($onLoadValidate ? '#arar-form-validation' : '#arar-form-add');
    }

    /**
     * @param \Magento\Review\Model\Review $review
     * @return string
     */
    public function getTwitterShareUrl(\Magento\Review\Model\Review $review) : string
    {
        if ($review->getTitle()) {
            $text = $review->getTitle();
        } else {
            $text = substr($this->escaper->escapeHtml($review->getDetail()), 0, 120);
        }

        return "https://twitter.com/intent/tweet?text={$text} " . $this->getReviewSeoFriendlyUrl->execute($review);
    }

    /**
     * @param \Magento\Review\Model\Review $review
     * @return string
     */
    public function getFacebookShareUrl(\Magento\Review\Model\Review $review) : string
    {
        $url = 'https://www.facebook.com/dialog/feed?';
        $url .= 'app_id=' . $this->configHelper->getFacebookAppId();
        $url .= '&display=popup&caption=' . urlencode($this->escaper->escapeHtml($review->getDetail()));
        $url .= '&link='.urlencode($this->getReviewSeoFriendlyUrl->execute($review));
        $url .= '&redirect_uri=https://facebook.com/';

        return $url;
    }

    /**
     * @param string $date
     * @param string $format
     * @return string
     */
    public function getFormattedDate(string $date, $format = 'd, M, Y') : string
    {
        $time   = strtotime($date);
        $result = '';

        if ($format === 'Y-m-d') {
            $result .= $this->dateTime->date('Y', $time) . '-';
            $result .= $this->dateTime->date('m', $time) . '-';
            $result .= $this->dateTime->date('d', $time);
        } else {
            $result .= $this->dateTime->date('d', $time) . ' ';
            $result .= __($this->dateTime->date('M', $time)) . ', ';
            $result .= $this->dateTime->date('Y', $time);
        }

        return $result;
    }

    /**
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getPagerTemplate($store = null, $scope = null) : string
    {
        return $this->configHelper->useSeparatePage($store, $scope)
            ? 'Plumrocket_AdvancedReviewAndReminder/review-list-load-more'
            : 'Plumrocket_AdvancedReviewAndReminder/review-one-page-load-more';
    }

    /**
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getProductPageToolbarTemplate($store = null, $scope = null) : string
    {
        return $this->configHelper->useSeparatePage($store, $scope)
            ? 'Plumrocket_AdvancedReviewAndReminder/review-list-toolbar-light'
            : 'Plumrocket_AdvancedReviewAndReminder/review-list-toolbar';
    }

    /**
     * @param int|null $productId
     * @return string
     */
    public function getAllReviewsUrl(int $productId = null) : string
    {
        if (!$productId) {
            $currentProduct = $this->currentProductRetriever->execute();
            if (! $currentProduct) {
                return '';
            }
            $productId = $currentProduct->getId();
        }

        return $this->frontUrlBuilder->getUrl(
            'review/product/list',
            ['id' => $productId]
        );
    }

    /**
     * @param int $percentRatingSummary
     * @return float|int
     */
    public function convertRatingSummary($percentRatingSummary)
    {
        return $percentRatingSummary * 5 / 100;
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getProductRatingSummaryData(\Magento\Catalog\Model\Product $product) : array
    {
        if (is_object($product->getRatingSummary())) {
            $percentRatingSummary = (int) $product->getRatingSummary()->getRatingSummary();
            $reviewsCount = (int) $product->getRatingSummary()->getReviewsCount();
        } else {
            $percentRatingSummary = (int) $product->getRatingSummary();
            $reviewsCount = (int) $product->getReviewsCount();
        }

        return [
            'count' => $reviewsCount,
            'rating' => $this->convertRatingSummary($percentRatingSummary),
            'name' => $product->getName()
        ];
    }

    /**
     * @param string $id
     * @return string
     */
    public function getVideoUrlByYoutubeId(string $id)
    {
        return self::YOUTUBE_EMBED_URL . $id;
    }

    /**
     * @param string $id
     * @return string
     */
    public function getImageUrlByYoutubeId(string $id)
    {
        return self::YOUTUBE_IMG_URL . $id .  '/default.jpg';
    }
}
