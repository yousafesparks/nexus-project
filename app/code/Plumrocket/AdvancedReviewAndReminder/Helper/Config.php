<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

use Plumrocket\AdvancedReviewAndReminder\Helper\Data as HelperData;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\EnabledFor;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\ReviewPageStructure;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

/**
 * Class Config
 * @api
 */
class Config extends \Plumrocket\Base\Helper\ConfigUtils
{
    const GROUP_GENERAL = 'general';
    const GROUP_MAIN = 'main';
    const GROUP_DESIGN = 'design';

    const GROUP_REVIEW_FORM = 'review_form';
    const SUBGROUP_REVIEW_FORM_SETTINGS = 'form_settings';
    const SUBGROUP_REVIEW_FORM_MEDIA = 'media';

    const GROUP_REVIEW_LIST_AND_COMMENTS = 'review_list_and_comments';
    const SUBGROUP_REVIEW_LIST = 'review_list';
    const SUBGROUP_HELPFUL_VOTES = 'helpful_votes';
    const SUBGROUP_REPORT_ABUSE = 'report_abuse';
    const SUBGROUP_REVIEW_ITEM = 'review_item';

    const GROUP_SEO = 'seo';
    const SEO_SUBGROUP_PRODUCT_PAGE = 'product_page';
    const SEO_SUBGROUP_REVIEW_LIST_PAGE = 'review_list_page';
    const SEO_SUBGROUP_SINGLE_REVIEW_PAGE = 'single_review_page';

    const GROUP_ADMIN_NOTIFICATIONS = 'magento_notification';

    const DEVELOPER = 'developer';

    const XML_PATH_ENABLED_LEAVE_REVIEW_GUEST_RECAPTCHA = 'msp_securitysuite_recaptcha/frontend/enabled_arar_review';
    const PR_ARAR_FORM_CAPTCHA = 'pr_arar_form';
    const POSITIVE_RATING_BOUND = 80;
    const MIN_REVIEW_PAGE_SIZE = 3;

    /**
     * @var \MSP\ReCaptcha\Model\Config|false
     */
    private $reCaptchaConfig;

    /**
     * @var \Magento\Captcha\Helper\Data
     */
    private $captchaData;

    /**
     * @var \Magento\Framework\File\Size
     */
    private $fileSizeService;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * @param \Magento\Framework\App\Helper\Context                                      $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider $independenceObjectProvider
     * @param \Magento\Captcha\Helper\Data                                               $captchaData
     * @param \Magento\Framework\File\Size                                               $fileSize
     * @param \Magento\Framework\Filesystem                                              $filesystem
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider $independenceObjectProvider,
        \Magento\Captcha\Helper\Data $captchaData,
        \Magento\Framework\File\Size $fileSize,
        \Magento\Framework\Filesystem $filesystem
    ) {
        parent::__construct($context);
        $this->reCaptchaConfig = $independenceObjectProvider->getReCaptchaConfig();
        $this->captchaData = $captchaData;
        $this->fileSizeService = $fileSize;
        $this->filesystem = $filesystem;
    }

    /**
     * Receive magento config value
     *
     * @param  string      $group
     * @param  string      $path
     * @param  string|int  $scopeCode
     * @param  string|null $scope
     * @return mixed
     */
    public function getConfigByGroup($group, $path, $scopeCode = null, $scope = null)
    {
        return $this->getConfig(
            implode('/', [HelperData::SECTION_ID, $group, $path]),
            $scopeCode,
            $scope
        );
    }

    /**
     * Receive magento config value
     *
     * @param  string      $group
     * @param  string      $subGroup
     * @param  string      $path
     * @param  string|int  $scopeCode
     * @param  string|null $scope
     * @return mixed
     */
    public function getConfigBySubGroup($group, $subGroup, $path, $scopeCode = null, $scope = null)
    {
        return $this->getConfigByGroup($group . '/' . $subGroup, $path, $scopeCode, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isModuleEnabled($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigByGroup(self::GROUP_GENERAL, 'enabled', $store, $scope);
    }

    /* ============================= MAIN SETTINGS ============================= */

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getNewReviewStatus($store = null, $scope = null) : string
    {
        return (string) $this->getConfigByGroup(self::GROUP_MAIN, 'auto_approve', $store, $scope)
            ?: \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Reviewstatus::REVIEW_STATUS_PENDING;
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getCustomerPermission($store = null, $scope = null) : int
    {
        return (int) $this->getConfigByGroup(self::GROUP_MAIN, 'customer_permission', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getCustomerGroupsPermission($store = null, $scope = null) : string
    {
        return (string) $this->getConfigByGroup(self::GROUP_MAIN, 'customer_groups_permission', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getGuestPermission($store = null, $scope = null) : int
    {
        return (int) $this->getConfigByGroup(self::GROUP_MAIN, 'guest_permission', $store, $scope);
    }

    /* ============================= DESIGN SETTINGS ============================= */

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return mixed
     */
    public function useSeparatePage($store = null, $scope = null)
    {
        return ReviewPageStructure::TWO_PAGE_DESIGN === $this->getReviewPageStructure($store, $scope);
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewPageStructure($store = null, $scope = null) : string
    {
        return (string) $this->getConfigByGroup(self::GROUP_DESIGN, 'review_page_structure', $store, $scope)
            ?: ReviewPageStructure::ONE_PAGE_DESIGN;
    }

    /* ============================= REVIEW FORM SETTINGS ============================= */
    /* -------------------------------- FORM  -------------------------------- */

    /**
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewSummaryOption($store = null, $scope = null) : string
    {
        return (string) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_SETTINGS,
            'review_summary',
            $store,
            $scope
        ) ?: Option::NO;
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReviewSummaryMaxLength($store = null, $scope = null) : int
    {
        return (int) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_SETTINGS,
            'review_summary_length',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getProsAndConsOption($store = null, $scope = null) : string
    {
        return (string) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_SETTINGS,
            'pros_and_cons',
            $store,
            $scope
        ) ?: Option::NO;
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledRecommendedToAFriend($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_SETTINGS,
            'recommended',
            $store,
            $scope
        );
    }

    /* -------------------------------- MEDIA  -------------------------------- */

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledImageUpload($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_MEDIA,
            'enabled_upload_img',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param bool $inBytes
     * @param null $store
     * @param null $scope
     * @return int|float
     */
    public function getMaxImageSize($inBytes = false, $store = null, $scope = null)
    {
        $size = (int) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_MEDIA,
            'max_img_size',
            $store,
            $scope
        );

        $size *= 1024 * 1024;

        if (! $inBytes) {
            $size = $this->fileSizeService->getFileSizeInMb($size);
        }

        return $size;
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return array
     */
    public function getAllowedExtension($store = null, $scope = null) : array
    {
        $allowedExtensions = $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_MEDIA,
            'allowed_extensions',
            $store,
            $scope
        );
        $allowedExtensions = array_map('trim', explode(',', $allowedExtensions));

        return array_filter($allowedExtensions);
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getMaxImagesForOneReview($store = null, $scope = null) : int
    {
        return (int) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_MEDIA,
            'max_number_img',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledVideoLink($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_FORM,
            self::SUBGROUP_REVIEW_FORM_MEDIA,
            'enabled_video_link',
            $store,
            $scope
        );
    }

    /* ============================= REVIEW LIST AND COMMENTS SETTINGS ============================= */
    /* -------------------------------- REVIEW LIST  -------------------------------- */

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function showImageGallery($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REVIEW_LIST,
            'show_images_gallery',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReviewPageSize($store = null, $scope = null) : int
    {
        $pageSize = (int) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REVIEW_LIST,
            'page_size',
            $store,
            $scope
        );

        return $pageSize >= self::MIN_REVIEW_PAGE_SIZE ? $pageSize : self::MIN_REVIEW_PAGE_SIZE;
    }

    /**
     * @param null $store
     * @param null $scope
     * @return mixed
     */
    public function getDefaultReviewSorting($store = null, $scope = null) : string
    {
        return (string) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REVIEW_LIST,
            'sorting',
            $store,
            $scope
        );
    }

    /**
     * @return bool
     */
    public function isAllowedFormattedTextOnFrontend()
    {
        return true;
    }

    /* -------------------------------- HELPFUL VOTES  -------------------------------- */

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledHelpful($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_HELPFUL_VOTES,
            'enabled',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledHelpfulForGuest($store = null, $scope = null) : bool
    {
        return EnabledFor::CUSTOMERS_AND_GUESTS === $this->getHelpfulVotesEnabledFor($store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getHelpfulVotesEnabledFor($store = null, $scope = null) : int
    {
        return (int) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_HELPFUL_VOTES,
            'enabled_for',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnableMostHelpfulBlock($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_HELPFUL_VOTES,
            'most_helpful_reviews',
            $store,
            $scope
        );
    }

    /**
     * Config have been removed in order to keeping configuration simple
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getMostHelpfulBlockMinReviewCount($store = null, $scope = null) : int
    {
        return 10;
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getMostHelpfulBlockMinHelpfulVotes($store = null, $scope = null) : int
    {
        return (int) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_HELPFUL_VOTES,
            'most_helpful_votes_condition',
            $store,
            $scope
        );
    }

    /* -------------------------------- REPORT ABUSE -------------------------------- */

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledAbuse($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REPORT_ABUSE,
            'enabled',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledAbuseForGuest($store = null, $scope = null) : bool
    {
        return EnabledFor::CUSTOMERS_AND_GUESTS === $this->getReportAbuseEnabledFor($store, $scope);
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReportAbuseEnabledFor($store = null, $scope = null) : int
    {
        return (int) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REPORT_ABUSE,
            'enabled_for',
            $store,
            $scope
        );
    }

    /* -------------------------------- REVIEW ITEM -------------------------------- */

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledVerified($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REVIEW_ITEM,
            'enabled_verified',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledShareButton($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REVIEW_ITEM,
            'share_buttons',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getFacebookAppId($store = null, $scope = null) : string
    {
        return (string) $this->getConfigBySubGroup(
            self::GROUP_REVIEW_LIST_AND_COMMENTS,
            self::SUBGROUP_REVIEW_ITEM,
            'facebook_app_id',
            $store,
            $scope
        );
    }

    /* ============================= SEO SETTINGS ============================= */
    /* -------------------------------- PRODUCT PAGE -------------------------------- */

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledRichSnippetForProductPage($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_PRODUCT_PAGE,
            'rich_snippet',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function useUrlParamsForFilterOnProductPage($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_PRODUCT_PAGE,
            'use_url_params_for_filter',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledRichSnippetForReviewListPage($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_REVIEW_LIST_PAGE,
            'rich_snippet',
            $store,
            $scope
        );
    }

    /**
     * @since 2.2.0
     *
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewPageMetaTitlePattern($store = null, $scope = null) : string
    {
        return (string) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_SINGLE_REVIEW_PAGE,
            'review_page_meta_title_format',
            $store,
            $scope
        );
    }

    /**
     * @since 2.2.0
     *
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewPageMetaDescriptionPattern($store = null, $scope = null) : string
    {
        return (string) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_SINGLE_REVIEW_PAGE,
            'meta_description_format',
            $store,
            $scope
        );
    }

    /* -------------------------------- REVIEW LIST PAGE -------------------------------- */

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function useUrlParamsForFilterOnReviewListPage($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_REVIEW_LIST_PAGE,
            'use_url_params_for_filter',
            $store,
            $scope
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewListPageMetaTitleFormat(
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $reviewPage = null,
        $store = null,
        $scope = null
    ) : string {
        $metaTitle = (string) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_REVIEW_LIST_PAGE,
            'meta_title_format',
            $store,
            $scope
        );
        $category = $product->getCategory();

        return str_replace(
            ['[page]', '[product_name]', '[category]'],
            [
                $reviewPage,
                $product->getName(),
                $category ? $category->getName() : ''
            ],
            $metaTitle
        );
    }

    /**
     * @since 2.2.0
     *
     * @param null $review
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewPageMetaTitleFormat(
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $review = null,
        $store = null,
        $scope = null
    ) : string {
        $metaTitle = (string) $this->getReviewPageMetaTitlePattern($store, $scope);

        $category = $product->getCategory();

        return str_replace(
            ['[review_title]', '[product_name]', '[category]'],
            [
                $review ? $review->getTitle() : '',
                $product->getName(),
                $category ? $category->getName() : ''
            ],
            $metaTitle
        );
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewListPageMetaDescriptionFormat(
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $reviewPage = null,
        $store = null,
        $scope = null
    ) : string {
        $metaDescription = (string) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_REVIEW_LIST_PAGE,
            'meta_description_format',
            $store,
            $scope
        );

        $category = $product->getCategory();

        return str_replace(
            ['[page]', '[product_name]', '[category]', '[product_description]'],
            [
                $reviewPage,
                $product->getName(),
                $category ? $category->getName() : '',
                $product->getShortDescription()
            ],
            $metaDescription
        );
    }

    /**
     * @since 2.2.0
     *
     * @param null $review
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewPageMetaDescriptionFormat(
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $review = null,
        $store = null,
        $scope = null
    ) : string {
        $metaDescription = (string) $this->getReviewPageMetaDescriptionPattern($store, $scope);

        $category = $product->getCategory();

        return str_replace(
            ['[review_title]', '[review_description]', '[product_name]', '[product_short_description]', '[category]'],
            [
                $review ? $review->getTitle() : '',
                $review ? $review->getDetail() : '',
                $product->getName(),
                $product->getShortDescription(),
                $category ? $category->getName() : ''
            ],
            $metaDescription
        );
    }

    /* -------------------------------- SINGLE REVIEW PAGE -------------------------------- */

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledRichSnippetForSinglePage($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigBySubGroup(
            self::GROUP_SEO,
            self::SEO_SUBGROUP_SINGLE_REVIEW_PAGE,
            'rich_snippet',
            $store,
            $scope
        );
    }

    /* ============================= ADMIN NOTIFICATIONS SETTINGS ============================= */

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledMagentoNotifications($store = null, $scope = null) : bool
    {
        return (bool) $this->getConfigByGroup(
            self::GROUP_ADMIN_NOTIFICATIONS,
            'enable_magento_notification',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledPendingReviewEmailNotifications($store = null, $scope = null) : bool
    {
        return (bool)$this->getConfigByGroup(
            self::GROUP_ADMIN_NOTIFICATIONS,
            'enable_new_review_email_notification',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return array
     */
    public function getPendingReviewEmailAddresses($store = null, $scope = null) : array
    {
        $fieldValue = $this->getConfigByGroup(
            self::GROUP_ADMIN_NOTIFICATIONS,
            'new_review_email_addresses',
            $store,
            $scope
        );

        return $this->splitTextareaValueByLine((string) $fieldValue);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return string|int
     */
    public function getPendingReviewEmailNotificationTemplate($store = null, $scope = null)
    {
        return $this->getConfigByGroup(
            self::GROUP_ADMIN_NOTIFICATIONS,
            'new_review_template',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledReportAbuseEmailNotifications($store = null, $scope = null) : bool
    {
        return (bool)$this->getConfigByGroup(
            self::GROUP_ADMIN_NOTIFICATIONS,
            'enable_abuse_email_notification',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return array
     */
    public function getReportAbuseEmailAddresses($store = null, $scope = null) : array
    {
        $fieldValue = $this->getConfigByGroup(
            self::GROUP_ADMIN_NOTIFICATIONS,
            'report_abuse_email_addresses',
            $store,
            $scope
        );

        return $this->splitTextareaValueByLine((string) $fieldValue);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return string|int
     */
    public function getReportAbuseEmailNotificationTemplate($store = null, $scope = null)
    {
        return $this->getConfigByGroup(
            self::GROUP_ADMIN_NOTIFICATIONS,
            'abuse_review_template',
            $store,
            $scope
        );
    }

    /* ============================= DEVELOPER ============================= */

    /**
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getReviewTabButtonSelector($store = null, $scope = null)
    {
        return $this->getConfigByGroup(
            self::DEVELOPER,
            'review_tab_button_selector',
            $store,
            $scope
        );
    }

    /* ============================= CAPTCHA ============================= */

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledRecaptcha($store = null, $scope = null) : bool
    {
        return $this->reCaptchaConfig
            && $this->reCaptchaConfig->isEnabledFrontend()
            && $this->getConfig(
                self::XML_PATH_ENABLED_LEAVE_REVIEW_GUEST_RECAPTCHA,
                $store,
                $scope
            );
    }

    /**
     * @return bool
     */
    public function isEnabledNativeCaptcha() : bool
    {
        return in_array(self::PR_ARAR_FORM_CAPTCHA, $this->getTargetForms(), true);
    }

    /**
     * Retrieve list of forms where captcha must be shown
     *
     * For frontend this list is based on current website
     *
     * @return array
     */
    private function getTargetForms() : array
    {
        $formsString = (string)$this->captchaData->getConfig('forms');
        return explode(',', $formsString);
    }

    /* ============================= OTHER ============================= */

    /**
     * Filesystem directory path of temporary files
     * @since 2.0.0
     *
     * @param bool $full
     * @return string
     */
    public function getBaseTmpMediaPath($full = true) : string
    {
        $path = 'tmp/pr_review';
        if ($full) {
            $path = $this->filesystem
                ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                ->getAbsolutePath($path);
        }
        return $path;
    }

    /**
     * Filesystem directory path of stable files
     * @since 2.0.0
     *
     * @param bool $full
     * @return string
     */
    public function getBaseMediaPath($full = true) : string
    {
        $path = 'pr_review';
        if ($full) {
            $path = $this->filesystem
                ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                ->getAbsolutePath($path);
        }
        return $path;
    }

    /**
     * @since 2.0.0
     *
     * @param $url
     * @return string
     */
    public function getYoutubeIdFromUrl($url): string
    {
        if (! $url) {
            return '';
        }

        if (11 === strlen($url)) {
            return $url;
        }

        preg_match('/(http(s|):|)\/\/(www\.|)yout(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $results);

        return $results[6] ?? '';
    }

    /**
     * @since 2.0.0
     *
     * @param null $store
     * @param null $scope
     * @return string[]
     */
    public function getEnabledForSearchFields($store = null, $scope = null) : array
    {
        $fields = ['title', 'detail'];
        if (Option::NO !== $this->getProsAndConsOption($store, $scope)) {
            array_push($fields, 'pros', 'cons');
        }

        return $fields;
    }

    /**
     * Config have been removed in order to keeping configuration simple
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function showReviewHeader($store = null, $scope = null) : bool
    {
        return true;
    }
}
