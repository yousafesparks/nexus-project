<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Phrase;
use Magento\Review\Model\Review;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Reviewstatus;

class Data extends AbstractHelper
{
    const SECTION_ID = 'advancedrar';
    const WIKI_LINK = 'https://plumrocket.com/docs/magento-advanced-reviews-reminders/v2';
    const REMINDER_SECTION_ID = 'pr_reminder';

    const LOGIN_KEY_QUERY_PARAM_NAME = 'alogin_secret';

    const REVIEW_URL_ROUTE = 'productreview';

    protected $_configSectionId = self::SECTION_ID;

    /**
     * @var string
     */
    private $currentReviewStatus = Reviewstatus::REVIEW_STATUS_PENDING;

    /**
     * @var array
     */
    private $availableOrderItems = [];

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    private $dataCollectionFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    protected $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
     */
    private $autoLoginManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface                          $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                         $date
     * @param \Magento\Framework\Data\CollectionFactory                           $dataCollectionFactory
     * @param \Magento\Framework\App\Helper\Context                               $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                 $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig         $reminderConfig
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Data\CollectionFactory $dataCollectionFactory,
        \Magento\Framework\App\Helper\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
    ) {
        $this->storeManager = $storeManager;
        $this->date = $date;
        $this->dataCollectionFactory = $dataCollectionFactory;
        $this->configHelper = $configHelper;
        parent::__construct($context);
        $this->autoLoginManager = $autoLoginManager;
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * Don't rewrite this method. For customization rewrite Helper\Config
     *
     * @param null $store
     * @return bool
     */
    public function moduleEnabled($store = null)
    {
        return $this->configHelper->isModuleEnabled($store);
    }

    /**
     * @deprecated since 1.2.0
     * @see \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface::addSecretToUrl()
     *
     * @param string                                                    $url
     * @param \Magento\Customer\Model\Customer|null                     $customer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder|null $reminder
     * @return string|string[]|null
     */
    public function addSecretToUrl($url, $customer = null, $reminder = null)
    {
        return $this->autoLoginManager->addSecretToUrl($url, $customer, $reminder);
    }

    /**
     * @param null $votes
     * @return bool|int
     */
    public function getDefaultStatus($votes = null)
    {
        $status = $this->configHelper->getNewReviewStatus();
        if ($status === Reviewstatus::REVIEW_STATUS_APPROVED) {
            $this->currentReviewStatus = Review::STATUS_APPROVED;
        } elseif ($status === Reviewstatus::REVIEW_STATUS_PENDING) {
            $this->currentReviewStatus = Review::STATUS_PENDING;
        } elseif ($status === Reviewstatus::REVIEW_STATUS_RATING_APPROVED && null !== $votes) {
            $approved = true;
            foreach ($votes as $vote) {
                if ($vote['value'] <= 3) {
                    $approved = false;
                }
            }

            $this->currentReviewStatus = ! $approved ? Review::STATUS_PENDING : Review::STATUS_APPROVED;
        } else {
            return false;
        }

        return (int)$this->currentReviewStatus;
    }

    public function getCurrentReviewStatus() : int
    {
        return (int) $this->currentReviewStatus;
    }

    public function getDefaultStatusMessage()
    {
        $status = $this->getCurrentReviewStatus();
        if (Review::STATUS_APPROVED === $status) {
            return __('Thank you for posting the review!');
        }

        if (Review::STATUS_PENDING === $status) {
            return __('Your review has been accepted for moderation.');
        }

        return '';
    }

    /**
     * @param int $status
     * @return \Magento\Framework\Phrase
     */
    public function getStatusMessage(int $status): Phrase
    {
        return $status === Review::STATUS_PENDING
            ? __('Your review has been accepted for moderation.')
            : __('Thank you for posting the review!');
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * @param null $storeId
     * @return false|string
     */
    public function getFirstReminderDelay($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStoreId();
        }

        return $this->getScheduleDate($this->reminderConfig->getReviewReminderFirstReminderDelay($storeId));
    }

    /**
     * @param null $storeId
     * @return false|string
     */
    public function getSecondReminderDelay($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStoreId();
        }

        return $this->getScheduleDate($this->reminderConfig->getReviewReminderSecondReminderDelay($storeId));
    }

    public function getThirdReminderDelay($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStoreId();
        }

        return $this->getScheduleDate($this->reminderConfig->getReviewReminderThirdReminderDelay($storeId));
    }

    private function getScheduleDate($afterDays)
    {
        $time = strtotime($this->date->gmtDate('Y-m-d'));
        return date('Y-m-d', $time + $afterDays * 86400);
    }

    /**
     * @param $order
     * @return \Magento\Framework\Data\Collection
     */
    public function getAvailableOrderItems(\Magento\Sales\Model\Order $order)
    {
        if (!isset($this->availableOrderItems[$order->getId()])) {
            $this->availableOrderItems[$order->getId()] = $this->dataCollectionFactory->create();
            foreach ($order->getAllVisibleItems() as $item) {
                $this->availableOrderItems[$order->getId()]->addItem($item);
            }

            $this->_eventManager->dispatch(
                'advancedrar_get_available_order_items_after',
                [
                    'order' => $order,
                    'available_items' => $this->availableOrderItems[$order->getId()]
                ]
            );
        }

        return $this->availableOrderItems[$order->getId()];
    }

    /**
     * Getting format of review url key
     *
     * @param  string $str review summary
     * @param int $index
     * @return string      encoded url
     */
    public function getReviewUrlKey($str, $index = null)
    {
        $url = $this->slugifyUrl($str);
        if (null !== $index) {
            $url .= '-' . $index;
        }

        return $url;
    }

    /**
     * Convert string to url path
     * @param  string $text title of review
     * @return string
     */
    public function slugifyUrl($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        $text = trim($text, '-');
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        $text = strtolower($text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
