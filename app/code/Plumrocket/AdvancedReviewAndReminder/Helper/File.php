<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

class File extends \Plumrocket\Base\Helper\ConfigUtils
{
    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    private $uploaderFactory;

    /**
     * @var Config
     */
    private $configHelper;
    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $fileIo;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * File constructor.
     *
     * @param \Magento\Framework\App\Helper\Context            $context
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
     * @param Config                                           $configHelper
     * @param \Magento\Framework\Filesystem\Io\File            $fileIo
     * @param \Magento\Store\Model\StoreManagerInterface       $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Framework\Filesystem\Io\File $fileIo,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->uploaderFactory = $uploaderFactory;
        $this->configHelper = $configHelper;
        $this->fileIo = $fileIo;
        $this->storeManager = $storeManager;
    }

    /**
     * @param $fileId
     * @return array
     * @throws \Exception
     */
    public function upload($fileId) : array
    {
        /** @var \Magento\MediaStorage\Model\File\Uploader $uploader */
        $uploader = $this->uploaderFactory->create(['fileId' => $fileId]);
        $uploader->setAllowedExtensions($this->configHelper->getAllowedExtension());
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);

        $ds = DIRECTORY_SEPARATOR;

        // Check file size.
        $size = $uploader->getFileSize();
        if ($size > $this->configHelper->getMaxImageSize(true)) {
            throw new \Exception(__(
                'Sorry, your file is too large. Please, upload file up to %1 MB only.',
                $this->configHelper->getMaxImageSize()
            ), -1);
        }
        $data = $uploader->save($this->configHelper->getBaseTmpMediaPath());

        $data = array_merge($data,
            [
                'url' => $this->storeManager->getStore()
                        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
            . $this->configHelper->getBaseTmpMediaPath(false) . $ds . $data['file']]);
        return $data;
    }

    /**
     * @param $filesTmp
     * @return array
     * @throws \Exception
     */
    public function moveUploadImage($filesTmp) : array
    {
        $files = [];

        if (is_array($filesTmp)) {
            $ds = DIRECTORY_SEPARATOR;
            $available = $this->configHelper->getMaxImagesForOneReview();

            $this->checkAndCreateBaseFolder();

            foreach ($filesTmp as $data) {
                // Need to use basename, because path can contain ".." to navigate to any site files
                $filename = isset($data['filename']) ? $this->basename($data['filename']) : '';
                $file = $this->configHelper->getBaseTmpMediaPath() . $ds . ltrim($filename, $ds);

                // Check max files count.
                if (count($files) > $available) {
                    $this->fileIo->rm($file);
                    continue;
                }

                if ($filename = $this->take($file)) {
                    $data['filename'] = $filename;
                    $files[] = $data;
                }
            }
        }

        return $files;
    }

    public function take($file)
    {
        $ds = DIRECTORY_SEPARATOR;
        $baseMediaPath = $this->configHelper->getBaseMediaPath();
        $allowedExtensions = $this->configHelper->getAllowedExtension();

        $filename = basename($file);
        if (! is_string($filename) || ! trim($filename)) {
            return false;
        }

        if (! $this->fileIo->fileExists($file)) {
            return false;
        }

        // Check extension.
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        if (! $ext || ! in_array($ext, $allowedExtensions, true)) {
            $this->fileIo->rm($file);
            return false;
        }

        // Check max size.
        if (filesize($file) > $this->configHelper->getMaxImageSize(true)) {
            $this->fileIo->rm($file);
            return false;
        }

        // If file is exists, generate unique name.
        $fileDest = $baseMediaPath . $ds
            . ltrim($filename, $ds);

        $filename = \Magento\Framework\File\Uploader::getNewFileName($fileDest);

        $fileDest = $baseMediaPath . $ds
            . ltrim($filename, $ds);

        $this->fileIo->checkAndCreateFolder(dirname($fileDest));

        // Move file to stable storage.
        if ($this->fileIo->mv($file, $fileDest)
            && $this->fileIo->fileExists($fileDest)
        ) {
            return $filename;
        }

        return false;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function checkAndCreateBaseFolder()
    {
        $baseMediaPath = $this->configHelper->getBaseMediaPath();
        if (! $this->fileIo->fileExists($baseMediaPath, false)) {
            $this->fileIo->checkAndCreateFolder($baseMediaPath);
        }

        return $this;
    }

    /**
     * Retrieve file basename
     *
     * @param  string $path
     * @return string
     */
    public function basename($path): string
    {
        $name = basename((string) $path);
        if ('.' === $name || '..' === $name) {
            $name = '';
        }

        return $name;
    }
}
