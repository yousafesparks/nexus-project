<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

use Plumrocket\AdvancedReviewAndReminder\Helper\Data as HelperData;
use Plumrocket\Base\Helper\ConfigUtils;

/**
 * @api
 * @since 2.0.0
 */
class ReminderConfig extends ConfigUtils
{
    const REMINDER_GROUP_GENERAL = 'general';
    const REMINDER_GROUP_FIRST_REMINDER = 'first_reminder';
    const REMINDER_GROUP_SECOND_REMINDER = 'second_reminder';
    const REMINDER_GROUP_THIRD_REMINDER = 'third_reminder';

    /**
     * Receive magento config value for reminders
     * @since 2.0.0
     *
     * @param  string      $group
     * @param  string      $path
     * @param  string|int  $scopeCode
     * @param  string|null $scope
     * @return mixed
     */
    public function getReminderConfigByGroup($group, $path, $scopeCode = null, $scope = null)
    {
        return $this->getConfig(
            implode('/', [HelperData::REMINDER_SECTION_ID, $group, $path]),
            $scopeCode,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isReviewReminderEnabled($store = null, $scope = null) : bool
    {
        return (bool) $this->getReminderConfigByGroup(
            self::REMINDER_GROUP_GENERAL,
            'enabled_reminder',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function sendIfNotAllProductsReviewed($store = null, $scope = null) : bool
    {
        return (bool) $this->getReminderConfigByGroup(
            self::REMINDER_GROUP_GENERAL,
            'send_if_not_all_products_reviwed',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReviewReminderEraseHistoryTime($store = null, $scope = null) : int
    {
        return $this->getReminderConfigByGroup(self::REMINDER_GROUP_GENERAL, 'erase_history', $store, $scope)
            * 60 * 60 * 24 * 30;
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isEnabledAutoLogin($store = null, $scope = null) : bool
    {
        return (bool) $this->getReminderConfigByGroup(
            self::REMINDER_GROUP_GENERAL,
            'enable_auto_login',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReviewReminderAutoLoginTimeFrame($store = null, $scope = null) : int
    {
        return $this->getAutoLoginLifetime($store, $scope) * 86400;
    }

    /**
     * Retrieve count of days
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getAutoLoginLifetime($store = null, $scope = null) : int
    {
        return (int) $this->getReminderConfigByGroup(
            self::REMINDER_GROUP_GENERAL,
            'auto_login_timeframe',
            $store,
            $scope
        );
    }

    /**
     * Retrieve count of days
     *
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getEmailPostLifetime($store = null, $scope = null) : int
    {
        return (int) $this->getReminderConfigByGroup(
            self::REMINDER_GROUP_GENERAL,
            'email_post_token_lifetime',
            $store,
            $scope
        );
    }

    /**
     * Check if enabled separate light page for leave review
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isSingleFormEnabled($store = null, $scope = null) : bool
    {
        return (bool) $this->getReminderConfigByGroup(
            self::REMINDER_GROUP_GENERAL,
            'enable_single_form',
            $store,
            $scope
        );
    }

    /**
     * Check if enabled auto sending for reminders
     *
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function autoSending($store = null, $scope = null) : bool
    {
        return (bool) $this->getReminderConfigByGroup(
            self::REMINDER_GROUP_GENERAL,
            'send_automatically',
            $store,
            $scope
        );
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isSecondReminderEnabled($store = null, $scope = null) : bool
    {
        return (bool) $this->getReminderConfigByGroup(self::REMINDER_GROUP_SECOND_REMINDER, 'enabled', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function isThirdReminderEnabled($store = null, $scope = null) : bool
    {
        return (bool) $this->getReminderConfigByGroup(self::REMINDER_GROUP_FIRST_REMINDER, 'enabled', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReviewReminderFirstReminderDelay($store = null, $scope = null) : int
    {
        return (int) $this->getReminderConfigByGroup(self::REMINDER_GROUP_FIRST_REMINDER, 'delay', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReviewReminderSecondReminderDelay($store = null, $scope = null) : int
    {
        return (int) $this->getReminderConfigByGroup(self::REMINDER_GROUP_SECOND_REMINDER, 'delay', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int
     */
    public function getReviewReminderThirdReminderDelay($store = null, $scope = null) : int
    {
        return (int) $this->getReminderConfigByGroup(self::REMINDER_GROUP_THIRD_REMINDER, 'delay', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int|string
     */
    public function getFirstReminderTemplate($store = null, $scope = null)
    {
        return $this->getReminderConfigByGroup(self::REMINDER_GROUP_FIRST_REMINDER, 'template', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int|string
     */
    public function getSecondReminderTemplate($store = null, $scope = null)
    {
        return $this->getReminderConfigByGroup(self::REMINDER_GROUP_SECOND_REMINDER, 'template', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return int|string
     */
    public function getThirdReminderTemplate($store = null, $scope = null)
    {
        return $this->getReminderConfigByGroup(self::REMINDER_GROUP_THIRD_REMINDER, 'template', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return array
     */
    public function getReviewReminderAllowedStatuses($store = null, $scope = null) : array
    {
        return explode(
            ',',
            $this->getReminderConfigByGroup(self::REMINDER_GROUP_FIRST_REMINDER, 'orders_status', $store, $scope)
        );
    }
}
