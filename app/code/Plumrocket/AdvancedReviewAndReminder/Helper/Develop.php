<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

class Develop extends Config
{
    /**
     * Developer section configure from $_ENV
     */
    const GROUP_DEVELOPER = 'developer';
    const FIELD_IMMUTABLE_REMINDER_SEND = 'immutable_reminder_send';

    /**
     * If this option enabled, you can send any reminders many times without changing status
     *
     * @return bool
     */
    public function isReminderSendingIsImmutable(): bool
    {
        return (bool) $this->getConfigByGroup(self::GROUP_DEVELOPER, self::FIELD_IMMUTABLE_REMINDER_SEND);
    }
}
