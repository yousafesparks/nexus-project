<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

use \Plumrocket\AdvancedReviewAndReminder\Model\Token\ReminderUnsubscribe;

/**
 * @since 2.0.0
 */
class Url extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Plumrocket\Token\Api\GenerateForCustomerInterface
     */
    protected $tokenGenerator;

    /**
     * Url constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Plumrocket\Token\Api\GenerateForCustomerInterface $tokenGenerator
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Plumrocket\Token\Api\GenerateForCustomerInterface $tokenGenerator
    ) {
        parent::__construct($context);
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return string
     */
    public function getUnsubscribeUrl($order): string
    {
        $token = $this->tokenGenerator->execute(
            (int) $order->getCustomerId(),
            $order->getCustomerEmail(),
            ReminderUnsubscribe::KEY,
            ['website_id' => $order->getStore()->getWebsiteId()]
        )->getHash();

        return $this->_getUrl('advancedrar/reminder/unsubscribe', ['_query' => ['token' => $token]]);
    }
}
