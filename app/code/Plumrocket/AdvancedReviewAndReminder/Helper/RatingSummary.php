<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Helper;

use Magento\Store\Model\StoreManagerInterface;

class RatingSummary extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $reviewFactory;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * RatingSummary constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        parent::__construct($context);
        $this->objectManager = $objectManager;
        $this->reviewFactory = $reviewFactory;
        $this->productMetadata = $productMetadata;
        $this->storeManager = $storeManager;
    }

    /**
     * Apply rating summary to product if is not exists
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function initRatingSummary(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        if (null === $product->getRatingSummary()) {
            if (version_compare($this->productMetadata->getVersion(), '2.3.3', '<')) {
                $this->reviewFactory->create()->getEntitySummary(
                    $product,
                    (int) $this->storeManager->getStore()->getId()
                );
            } else {
                if ($product->getRatingSummary() === null) {
                    $this->objectManager->create('\Magento\Review\Model\ReviewSummary')
                        ->appendSummaryDataToObject(
                            $product,
                            (int) $this->storeManager->getStore()->getId()
                        );
                }
            }
        }
    }
}
