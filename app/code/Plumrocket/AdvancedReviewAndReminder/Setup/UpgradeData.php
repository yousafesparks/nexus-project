<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Setup;

use Magento\Framework\App\State;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\UnsubscribeReason;
use Plumrocket\AdvancedReviewAndReminder\Setup\Operation\CopyOldConfigs;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Setup\Operation\CopyOldConfigs
     */
    private $configMigration;

    /**
     * @param \Magento\Framework\App\State                                         $state
     * @param \Plumrocket\AdvancedReviewAndReminder\Setup\Operation\CopyOldConfigs $configMigration
     */
    public function __construct(
        State $state,
        CopyOldConfigs $configMigration
    ) {
        $this->configMigration = $configMigration;

        try {
            $state->setAreaCode('adminhtml');
        } catch (\Exception $e) {}
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '2.0.0', '<')) {
            $this->configMigration->execute();

            $defaultReasons = [
                [
                    'status' => 1,
                    'input_type' => 1,
                    'label' => 'I no longer want to receive these emails',
                ],
                [
                    'status' => 1,
                    'input_type' => 1,
                    'label' => 'I never signed up for this mailing list',
                ],
                [
                    'status' => 1,
                    'input_type' => 1,
                    'label' => 'The emails are inappropriate',
                ],
                [
                    'status' => 1,
                    'input_type' => 1,
                    'label' => 'The emails are spam',
                ],
                [
                    'status' => 1,
                    'input_type' => 2,
                    'label' => 'Other (fill in reason below)',
                ],
            ];

            $setup->getConnection()->insertMultiple($setup->getTable(UnsubscribeReason::MAIN_TABLE), $defaultReasons);
        }

        $setup->endSetup();
    }
}
