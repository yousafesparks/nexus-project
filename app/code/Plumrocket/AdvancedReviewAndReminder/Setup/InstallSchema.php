<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
        * Install review table
        */
        $reviewTable = $installer->getConnection()
            ->newTable($installer->getTable('pl_advanced_review'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity'  => true,
                    'nullable'  => false,
                    'primary'   => true,
                ],
                'Identity Id'
            )
            ->addColumn(
                'review_id',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => false,
                ],
                'Review Id'
            )
            ->addColumn(
                'helpful_positive',
                Table::TYPE_INTEGER,
                '5',
                [
                    'nullable'  => true,
                    'default'   => 0
                ],
                'Helpful_Positive'
            )
            ->addColumn(
                'helpful_negative',
                Table::TYPE_INTEGER,
                '5',
                [
                    'nullable'  => true,
                    'default'   => 0
                ],
                'Helpful_Negative'
            )
            ->addColumn(
                'abuse',
                Table::TYPE_INTEGER,
                '1',
                [
                    'nullable'  => true,
                    'default'   => 0
                ],
                'Abuse'
            )
            ->addColumn(
                'abused_customer',
                Table::TYPE_INTEGER,
                '1',
                [
                    'nullable'  => true,
                    'default'   => 0
                ],
                'Abused_customer'
            )
            ->addColumn(
                'pros',
                Table::TYPE_TEXT,
                '64k',
                [
                    'nullable'  => false
                ],
                'Pros'
            )
            ->addColumn(
                'cons',
                Table::TYPE_TEXT,
                '64k',
                [
                    'nullable'  => false
                ],
                'Cons'
            )
            ->addColumn(
                'admin_comment',
                Table::TYPE_TEXT,
                '64k',
                [
                    'nullable'  => false
                ],
                'Admin comment'
            )
            ->addColumn(
                'admin_comment_date',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable'  => false
                ],
                'Admin comment date'
            )
            ->addColumn(
                'review_url_key',
                Table::TYPE_TEXT,
                255,
                [],
                'Review Url Key'
            )
            ->addColumn(
                'review_url_key_origin',
                Table::TYPE_TEXT,
                255,
                [],
                'Review Url Key Origin'
            )
            ->addColumn(
                'verified',
                Table::TYPE_INTEGER,
                2,
                [
                    'nullable'  => false
                ],
                'Verified'
            )
            ->setComment('Plumrocket Reviews');

        $installer->getConnection()->createTable($reviewTable);

        /**
        * Install vote table
        */
        $voteTable = $installer->getConnection()
            ->newTable($installer->getTable('pl_advanced_review_helpful_vote'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity'  => true,
                    'nullable'  => false,
                    'primary'   => true,
                ],
                'ID'
            )
            ->addColumn(
                'review_id',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => false
                ],
                'Review Id'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => true
                ],
                'Cusomer Id'
            )
            ->addColumn(
                'visitor_id',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => true
                ],
                'Visitor Id'
            )
            ->addColumn(
                'vote',
                Table::TYPE_SMALLINT,
                '1',
                [
                    'nullable'  => true
                ],
                'Vote'
            )
            ->setComment('Vote Table');

        $installer->getConnection()->createTable($voteTable);

        /**
        * Install autologin table
        */
        $keyTable = $installer->getConnection()
            ->newTable($installer->getTable('pl_advanced_review_autologin_key'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity'  => true,
                    'nullable'  => false,
                    'primary'   => true,
                ],
                'ID'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => false
                ],
                'Cusomer Id'
            )
            ->addColumn(
                'secret',
                Table::TYPE_TEXT,
                '64',
                [
                    'nullable'  => true
                ],
                'Secret'
            )
            ->addColumn(
                'active_to',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable'  => false,
                ]
            )
            ->setComment('Plumrocket Autologin Table');

        $installer->getConnection()->createTable($keyTable);

        /**
        * Install reminder table
        */
        $reminderTable = $installer->getConnection()
            ->newTable($installer->getTable('pl_advanced_review_reminder'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'ID'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable'  => false,
                    'unsigned' => true,
                ],
                'Order Id'
            )
            ->addColumn(
                'reminder',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => true
                ],
                'Reminder'
            )
            ->addColumn(
                'subject',
                Table::TYPE_TEXT,
                255,
                [],
                'Email Subject'
            )
            ->addColumn(
                'body',
                Table::TYPE_TEXT,
                '64k',
                [
                    'nullable'  => false
                ],
                'Email Body'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable'  => false,
                ],
                'Reminder create date'
            )
            ->addColumn(
                'scheduled_at',
                Table::TYPE_DATE,
                null,
                [
                    'nullable'  => false,
                ],
                'Reminder send date'
            )
            ->addColumn(
                'status',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => false
                ],
                'Status'
            )
            ->addColumn(
                'store_id',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable'  => true,
                    'unsigned' => true
                ],
                'Store Id'
            )
            ->addColumn(
                'secret_key',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable'  => false,
                ],
                'Secret key for guests'
            )
            ->setComment('Plumrocket Reminders');

        $installer->getConnection()->createTable($reminderTable);

        /**
        * Install reminder done table
        */
        $doneReminderTable = $installer->getConnection()
            ->newTable($installer->getTable('pl_advanced_review_reminder_done'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity'  => true,
                    'nullable'  => false,
                    'primary'   => true,
                ],
                'ID'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => false
                ],
                'Order Id'
            )
            ->addColumn(
                'reminder',
                Table::TYPE_INTEGER,
                '10',
                [
                    'nullable'  => true
                ],
                'Reminder'
            )
            ->setComment('Plumrocket Reminders Done');

        $installer->getConnection()->createTable($doneReminderTable);

        $setup->endSetup();
    }
}
