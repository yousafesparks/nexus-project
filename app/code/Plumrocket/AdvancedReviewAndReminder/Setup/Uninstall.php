<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2017 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Setup;

use Plumrocket\Base\Setup\AbstractUninstall;

class Uninstall extends AbstractUninstall
{
    protected $_configSectionId = 'advancedrar';
    protected $_pathes = ['/app/code/Plumrocket/AdvancedReviewAndReminder'];
    protected $_tables =
    [
        'pl_advanced_review',
        'pl_advanced_review_helpful_vote',
        'pl_advanced_review_autologin_key',
        'pl_advanced_review_reminder',
        'pl_advanced_review_reminder_done',
        'pl_advanced_review_aggregate_rating',
        'pl_advanced_review_helpful_vote',
        'pl_advanced_reminder_subscriber',
        'pl_advanced_unsubscribe_reason'
    ];
}
