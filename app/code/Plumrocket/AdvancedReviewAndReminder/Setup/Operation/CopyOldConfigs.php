<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

// phpcs:ignoreFile

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Setup\Operation;

use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;
use Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig;

/**
 * Class CopyOldConfigs
 */
class CopyOldConfigs
{
    /**
     * @var \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory
     */
    private $configDataCollectionFactory;

    /**
     * @var array
     */
    private $pathMapping = [
        'advancedrar/general/rich_snippet' => [
            'advancedrar/' . ConfigHelper::GROUP_SEO .'/' . ConfigHelper::SEO_SUBGROUP_PRODUCT_PAGE . '/rich_snippet',
            'advancedrar/' . ConfigHelper::GROUP_SEO .'/' . ConfigHelper::SEO_SUBGROUP_REVIEW_LIST_PAGE . '/rich_snippet',
            'advancedrar/' . ConfigHelper::GROUP_SEO .'/' . ConfigHelper::SEO_SUBGROUP_SINGLE_REVIEW_PAGE . '/rich_snippet',
        ],
        'advancedrar/review_form/customer_permission' => 'advancedrar/' . ConfigHelper::GROUP_MAIN . '/customer_permission',
        'advancedrar/review_form/customer_groups_permission' => 'advancedrar/' . ConfigHelper::GROUP_MAIN . '/customer_groups_permission',
        'advancedrar/review_form/guest_permission' => 'advancedrar/' . ConfigHelper::GROUP_MAIN . '/guest_permission',
        'advancedrar/review_form/review_status' => 'advancedrar/' . ConfigHelper::GROUP_MAIN . '/auto_approve',
        'advancedrar/review_list/share_button' => 'advancedrar/' . ConfigHelper::GROUP_REVIEW_LIST_AND_COMMENTS . '/' . ConfigHelper::SUBGROUP_REVIEW_ITEM . '/share_buttons',
        'advancedrar/review_list/enable_verified' => 'advancedrar/' . ConfigHelper::GROUP_REVIEW_LIST_AND_COMMENTS . '/' . ConfigHelper::SUBGROUP_REVIEW_ITEM . '/enabled_verified',
        'advancedrar/review_list/enable_helpful' => 'advancedrar/' . ConfigHelper::GROUP_REVIEW_LIST_AND_COMMENTS . '/' . ConfigHelper::SUBGROUP_HELPFUL_VOTES . '/enabled',
        'advancedrar/review_list/enable_helpful_for_guest' => 'advancedrar/' . ConfigHelper::GROUP_REVIEW_LIST_AND_COMMENTS . '/' . ConfigHelper::SUBGROUP_HELPFUL_VOTES . '/enabled_for',
        'advancedrar/review_list/enable_abuse' => 'advancedrar/' . ConfigHelper::GROUP_REVIEW_LIST_AND_COMMENTS . '/' . ConfigHelper::SUBGROUP_REPORT_ABUSE . '/enabled',
        'advancedrar/review_list/enable_abuse_for_guest' => 'advancedrar/' . ConfigHelper::GROUP_REVIEW_LIST_AND_COMMENTS . '/' . ConfigHelper::SUBGROUP_REPORT_ABUSE . '/enabled_for',
        'advancedrar/review_list/enable_on_product_page' => 'advancedrar/' . ConfigHelper::GROUP_DESIGN . '/review_page_structure',
        'advancedrar/review_list/sorting' => 'advancedrar/' . ConfigHelper::GROUP_REVIEW_LIST_AND_COMMENTS . '/' . ConfigHelper::SUBGROUP_REVIEW_LIST . '/sorting',
        'advancedrar/review_reminder/enabled_reminder' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/enabled_reminder',
        'advancedrar/review_reminder/send_if_not_all_products_reviwed' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/send_if_not_all_products_reviwed',
        'advancedrar/review_reminder/erase_history' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/erase_history',
        'advancedrar/review_reminder/enable_auto_login' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/enable_auto_login',
        'advancedrar/review_reminder/auto_login_timeframe' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/auto_login_timeframe',
        'advancedrar/review_reminder/email_post_token_lifetime' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/email_post_token_lifetime',
        'advancedrar/review_reminder/enable_single_form' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/enable_single_form',
        'advancedrar/review_reminder/send_automatically' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_GENERAL . '/send_automatically',
        'advancedrar/review_reminder/first_reminder/delay' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_FIRST_REMINDER . '/delay',
        'advancedrar/review_reminder/first_reminder/orders_status' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_FIRST_REMINDER . '/orders_status',
        'advancedrar/review_reminder/first_reminder/template' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_FIRST_REMINDER . '/template',
        'advancedrar/review_reminder/second_reminder/enabled' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_SECOND_REMINDER . '/enabled',
        'advancedrar/review_reminder/second_reminder/delay' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_SECOND_REMINDER . '/delay',
        'advancedrar/review_reminder/second_reminder/template' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_SECOND_REMINDER . '/template',
        'advancedrar/review_reminder/third_reminder/enabled' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_THIRD_REMINDER . '/enabled',
        'advancedrar/review_reminder/third_reminder/delay' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_THIRD_REMINDER . '/delay',
        'advancedrar/review_reminder/third_reminder/template' => 'pr_reminder/' . ReminderConfig::REMINDER_GROUP_THIRD_REMINDER . '/template',
        'advancedrar/review_reminder/update_old/orders_from' => 'pr_reminder/update_old/orders_from',
        'advancedrar/review_reminder/update_old/orders_to' => 'pr_reminder/update_old/orders_to',
        'advancedrar/review_reminder/update_old/submit_process' => 'pr_reminder/update_old/submit_process',
    ];

    /**
     * @var array
     */
    private $valueMapping = [
        'advancedrar/review_list/enable_on_product_page' => [
            0 => \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\ReviewPageStructure::TWO_PAGE_DESIGN,
            1 => \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\ReviewPageStructure::ONE_PAGE_DESIGN,
        ],
    ];

    /**
     * @var array
     */
    private $configToDelete = [
        'advancedrar/general/rich_snippet',
        'advancedrar/review_form/enable_cms_block',
        'advancedrar/review_form/content',
    ];

    /**
     * CopyOldConfigs constructor.
     *
     * @param \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory $configDataCollectionFactory
     */
    public function __construct(
        \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory $configDataCollectionFactory
    ) {
        $this->configDataCollectionFactory = $configDataCollectionFactory;
    }

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\Setup\Operation\CopyOldConfigs
     */
    public function execute() : CopyOldConfigs
    {
        /** @var \Magento\Config\Model\ResourceModel\Config\Data\Collection $configDataCollection */
        $configDataCollection = $this->configDataCollectionFactory->create();
        $configDataCollection->addPathFilter('advancedrar');

        /** @var \Magento\Framework\App\Config\Value $config */
        foreach ($configDataCollection as $config) {
            $path = $config->getData('path');
            $value = $config->getData('value');
            $newPath = $this->getNewPath($path);

            if (is_array($newPath)) {
                foreach ($newPath as $path) {
                    $newConfig = clone $config;
                    $newConfig->addData(['path' => $path, 'value' => $this->getNewValue($path, $value)]);
                    $newConfig->setId(null);
                    $configDataCollection->addItem($newConfig);
                }
            } else {
                $config->setData('path', $newPath);
                $config->setData('value', $this->getNewValue($path, $value));
            }
        }

        $configDataCollection->save();

        /** @var \Magento\Config\Model\ResourceModel\Config\Data\Collection $configDataCollectionToDelete */
        $configDataCollectionToDelete = $this->configDataCollectionFactory->create();
        $configDataCollectionToDelete->addFieldToFilter('path', ['in' => $this->configToDelete]);
        $configDataCollectionToDelete->walk('delete');

        return $this;
    }

    /**
     * @param string $oldPath
     * @return string|array
     */
    private function getNewPath(string $oldPath)
    {
        return $this->pathMapping[$oldPath] ?? $oldPath;
    }

    /**
     * @param string $oldPath
     * @param string|null $oldValue
     * @return string|null
     */
    private function getNewValue(string $oldPath, $oldValue)
    {
        return $this->valueMapping[$oldPath][$oldValue] ?? $oldValue;
    }
}
