<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $wrongFkName = $setup->getFkName('pl_advanced_review_reminder', 'id', 'sales_order', 'entity_id');
            $rightFkName = $setup->getFkName('pl_advanced_review_reminder', 'order_id', 'sales_order', 'entity_id');
            $reminderTable = $setup->getTable('pl_advanced_review_reminder');

            $connection->dropForeignKey($reminderTable, $wrongFkName);

            $connection->changeColumn(
                $reminderTable,
                'order_id',
                'order_id',
                [
                    'type' => Table::TYPE_INTEGER,
                    'length' => 10,
                    'unsigned' => true,
                    'nullable' => false,
                ]
            );

            $connection->addIndex(
                $reminderTable,
                $setup->getIdxName($reminderTable, ['order_id'], true),
                ['order_id']
            );

            $connection->addForeignKey(
                $rightFkName,
                $reminderTable,
                'order_id',
                $setup->getTable('sales_order'),
                'entity_id',
                Table::ACTION_CASCADE
            );
        }

        if (version_compare($context->getVersion(), '2.0.0', '<')) {
            $connection->addColumn(
                $setup->getTable('pl_advanced_review'),
                'delivery_time',
                [
                    'type' => Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'Delivery time',
                ]
            );
            $connection->addColumn(
                $setup->getTable('pl_advanced_review'),
                'attach_image',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Attached images for review',
                ]
            );
            $connection->addColumn(
                $setup->getTable('pl_advanced_review'),
                'youtube_id',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'YouTube id',
                ]
            );
            $connection->addColumn(
                $setup->getTable('pl_advanced_review'),
                'recommend',
                [
                    'type' => Table::TYPE_BOOLEAN,
                    'nullable' => false,
                    'default' => Recommend::NOT_SPECIFIED,
                    'comment' => 'Recommendations this review',
                ]
            );

            /**
             * Install rating table
             */
            $ratingTable = $connection
                ->newTable($setup->getTable('pl_advanced_review_aggregate_rating'))
                ->addColumn(
                    'review_id',
                    Table::TYPE_BIGINT,
                    20,
                    [
                        'nullable'  => false,
                    ],
                    'Review Id'
                )
                ->addColumn(
                    'store_id',
                    Table::TYPE_SMALLINT,
                    5,
                    [
                        'nullable'  => false,
                    ],
                    'Store Id'
                )
                ->addColumn(
                    'percent',
                    Table::TYPE_SMALLINT,
                    3,
                    [
                        'nullable'  => false,
                        'default'   => 0
                    ],
                    'Average Votes Percent'
                )
                ->setComment('Plumrocket Aggregate Rating');

            $connection->createTable($ratingTable);

            $reasonTable = $connection
                ->newTable($setup->getTable('pl_advanced_unsubscribe_reason'))
                ->addColumn(
                    'reason_id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Unique identifier'
                )->addColumn(
                    'status',
                    Table::TYPE_BOOLEAN,
                    null,
                    [
                        'nullable'  => false,
                    ],
                    'Status'
                )
                ->addColumn(
                    'input_type',
                    Table::TYPE_SMALLINT,
                    null,
                    [
                        'default'   => 0
                    ],
                    'Input Type'
                )->addColumn(
                    'label',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable'  => false,
                    ],
                    'Label'
                )->addColumn(
                    'store_label',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable'  => true,
                    ],
                    'Store View Label'
                )
                ->setComment('Unsubscribe reasons');

            $connection->createTable($reasonTable);

            $subscriberTable = $connection
                ->newTable($setup->getTable('pl_advanced_reminder_subscriber'))
                ->addColumn(
                    'subscriber_id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Unique identifier'
                )->addColumn(
                    'email',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable'  => false,
                    ],
                    'Email'
                )->addColumn(
                    'type',
                    Table::TYPE_SMALLINT,
                    2,
                    [
                        'nullable'  => false,
                    ],
                    'Visitor Type'
                )->addColumn(
                    'status',
                    Table::TYPE_SMALLINT,
                    2,
                    [
                        'nullable'  => false,
                    ],
                    'Status'
                )->addColumn(
                    'reason',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable'  => false,
                    ],
                    'Reason'
                )->addColumn(
                    'reason_label',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable'  => false,
                    ],
                    'Reason Label'
                )->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable'  => true,
                        'unsigned' => true,
                    ],
                    'Customer Identifier'
                )->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable'  => false,
                        'default' => TABLE::TIMESTAMP_INIT,
                    ],
                    'Created At'
                )->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable'  => false,
                        'default' => TABLE::TIMESTAMP_INIT_UPDATE,
                    ],
                    'Updated At'
                )->addColumn(
                    'website_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable'  => false,
                        'unsigned' => true,
                    ],
                    'Website Id'
                )->addForeignKey(
                    $setup->getFkName(
                        $setup->getTable('pl_advanced_reminder_subscriber'),
                        'customer_id',
                        $setup->getTable('customer_entity'),
                        'entity_id'
                    ),
                    'customer_id',
                    $setup->getTable('customer_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )->setComment('Reminder Subscribers');

            $connection->createTable($subscriberTable);
        }

        $setup->endSetup();
    }
}
