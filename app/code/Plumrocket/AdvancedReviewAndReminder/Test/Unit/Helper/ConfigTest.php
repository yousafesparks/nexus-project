<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Helper;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;

class ConfigTest extends TestCase
{
    /**
     * @var ConfigHelper | MockObject
     */
    private $configHelper;

    /**
     * @return void
     */
    protected function setUp()
    {
        $this->configHelper = $this->getMockBuilder(ConfigHelper::class)
           ->setMethodsExcept(['getYoutubeIdFromUrl'])
           ->disableOriginalConstructor()
           ->getMock();
    }

    /**
     * @dataProvider youtubeUrlProvider()
     *
     * @param string $testCase
     * @param        $url
     * @param string $expect
     */
    public function testGetYoutubeIdFromUrl(
        $testCase,
        $url,
        string $expect
    ) {
        $this->assertSame(
            $expect,
            $this->configHelper->getYoutubeIdFromUrl($url),
            'Fail parse "' . $testCase . '"'
        );
    }

    public function youtubeUrlProvider()
    {
        yield [
            'testCase' => 'Url with only one param',
            'url' => 'https://www.youtube.com/watch?v=F3_-jTLaFSs',
            'expect' => 'F3_-jTLaFSs',
        ];

        yield [
            'testCase' => 'Url with params',
            'url' => 'https://www.youtube.com/watch?v=F3_-jTLaFSs&list=PLNYkxOF6rcIDfz8XEA3loxY32tYh7CI3m&index=18',
            'expect' => 'F3_-jTLaFSs',
        ];

        yield [
            'testCase' => 'Short url',
            'url' => 'https://youtu.be/F3_-jTLaFSs',
            'expect' => 'F3_-jTLaFSs',
        ];

        yield [
            'testCase' => 'Embed url',
            'url' => 'https://www.youtube.com/embed/F3_-jTLaFSs',
            'expect' => 'F3_-jTLaFSs',
        ];

        yield [
            'testCase' => 'Only video id',
            'url' => 'F3_-jTLaFSs',
            'expect' => 'F3_-jTLaFSs',
        ];

        yield [
            'testCase' => 'Null',
            'url' => null,
            'expect' => '',
        ];

        yield [
            'testCase' => 'Random string',
            'url' => 'qwerty1qwerty2qwerty3qwerty4qwerty5qwerty6',
            'expect' => '',
        ];
    }
}
