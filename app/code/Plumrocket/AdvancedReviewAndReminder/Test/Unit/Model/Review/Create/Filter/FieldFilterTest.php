<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Model\Review\Create\Filter;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\AllowedFields;

class FieldFilterTest extends TestCase
{
    /**
     * @dataProvider filterFieldsDataProvider
     *
     * @param string $testCase
     * @param array  $arguments
     * @param array  $postData
     * @param array  $expect
     */
    public function testFilterFields(
        string $testCase,
        array $arguments,
        array $postData,
        array $expect
    ) {
        $objectManager = new ObjectManager($this);

        /** @var AllowedFields $filter */
        $filter = $objectManager->getObject(
            AllowedFields::class,
            ['disallowedFields' => $arguments]
        );

        $this->assertSame($expect, $filter->filter($postData), $testCase);
    }

    /**
     * @return array
     */
    public function filterFieldsDataProvider()
    {
        return [
            [
                'testCase'  => 'Default fields',
                'arguments' => [],
                'postData'  => [
                    'verified' => true,
                    'title' => 'Cool',
                    'admin_rating' => 10,
                ],
                'expect'    => [
                    'title' => 'Cool',
                    'admin_rating' => 10,
                ]
            ],
            [
                'testCase'  => 'Additional field',
                'arguments' => [
                    'admin_rating'   => true,
                ],
                'postData'  => [
                    'verified' => true,
                    'title' => 'Cool',
                    'admin_rating' => 10,
                ],
                'expect' => [
                    'title' => 'Cool'
                ]
            ],
            [
                'testCase'  => 'Allow specific default field',
                'arguments' => [
                    'verified'   => false,
                ],
                'postData'  => [
                    'verified' => 1,
                    'title' => 'Cool',
                ],
                'expect' => [
                    'verified' => 1,
                    'title' => 'Cool',
                ]
            ],
        ];
    }
}
