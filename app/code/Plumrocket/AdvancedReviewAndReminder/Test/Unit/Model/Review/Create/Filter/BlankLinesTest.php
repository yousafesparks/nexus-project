<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Model\Review\Create\Filter;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Plumrocket\AdvancedReviewAndReminder\Model\Review\Create\Filter\BlankLines;

class BlankLinesTest extends TestCase
{
    /**
     * @dataProvider filterBlankLinesDataProvider
     *
     * @param string $testCase
     * @param array  $postData
     * @param array  $expect
     */
    public function testFilterBlankLines(
        string $testCase,
        array $postData,
        array $expect
    ) {
        $objectManager = new ObjectManager($this);

        /** @var BlankLines $filter */
        $filter = $objectManager->getObject(BlankLines::class);

        $this->assertSame($expect, $filter->filter($postData), $testCase);
    }

    /**
     * @return \Generator
     */
    public function filterBlankLinesDataProvider() : \Generator
    {
        yield [
            'testCase'  => 'Test empty string',
            'postData'  => [
                'detail' => '',
                'title' => 'Cool',
            ],
            'expect'    => [
                'detail' => '',
                'title' => 'Cool',
            ]
        ];

        yield [
            'testCase'  => 'Dont remove line brakes',
            'postData'  => [
                'detail' => 'a
                a
                a',
                'title' => 'Cool',
            ],
            'expect'    => [
                'detail' => 'a
                a
                a',
                'title' => 'Cool',
            ]
        ];

        yield [
            'testCase'  => 'Remove single enter \n',
            'postData'  => [
                'detail' => 'a

                a
                a',
                'title' => 'Cool',
            ],
            'expect'    => [
                'detail' => 'a
                a
                a',
                'title' => 'Cool',
            ]
        ];

        yield [
            'testCase'  => 'Remove enters \n',
            'postData'  => [
                'detail' => '

                1

                2




                3

                ',
            ],
            'expect'    => [
                'detail' => '1
                2
                3',
            ]
        ];

        yield [
            'testCase'  => 'Remove enters \r\n',
            'postData'  => [
                'detail' => "
                1\r\n
                2\r\n
                3\r\n
                ",
            ],
            'expect'    => [
                'detail' => '1
                2
                3',
            ]
        ];
    }
}
