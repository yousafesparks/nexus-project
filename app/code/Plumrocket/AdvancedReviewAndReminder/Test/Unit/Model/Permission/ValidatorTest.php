<?php
/**
 * Created by PhpStorm.
 * User: romix
 * Date: 1/12/19
 * Time: 9:57 AM
 */

namespace Model\Permission;

use PHPUnit\Framework\MockObject\MockObject;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\PermissionSecretDataContainerInterface;
use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;
use Plumrocket\AdvancedReviewAndReminder\Helper\Permission as PermissionHelper;
use Plumrocket\AdvancedReviewAndReminder\Model\Permission\AllowedCustomerGroupsProvider;
use Plumrocket\AdvancedReviewAndReminder\Model\BoughtProductValidator;
use Plumrocket\AdvancedReviewAndReminder\Model\Permission\Data\Container as PermissionDataContainer;
use Plumrocket\AdvancedReviewAndReminder\Model\Permission\GuestSecretDataProvider;
use Plumrocket\AdvancedReviewAndReminder\Model\Permission\Validator as PermissionValidator;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\AbstractPermission as PermissionTypes;

class ValidatorTest extends TestCase
{
    /**
     * @var PermissionValidator
     */
    private $validator;

    /**
     * @var ConfigHelper | MockObject
     */
    private $configHelper;

    /**
     * @var BoughtProductValidator | MockObject
     */
    private $boughtProductValidator;

    /**
     * @var AllowedCustomerGroupsProvider | MockObject
     */
    private $allowedCustomerGroupsProvider;

    /**
     * @return PermissionValidator|void
     */
    protected function setUp()
    {
        $objectManager = new ObjectManager($this);

        $permissionDataContainer = $objectManager->getObject(PermissionDataContainer::class);

        $configHelper                   = $this->createMock(ConfigHelper::class);
        $boughtProductValidator         = $this->createMock(BoughtProductValidator::class);
        $allowedCustomerGroupsProvider  = $this->createMock(AllowedCustomerGroupsProvider::class);

        /** @var PermissionSecretDataContainerInterface | MockObject $guestSecretDataProvider */
        $guestSecretDataProvider        = $this->createMock(GuestSecretDataProvider::class);
        $guestSecretDataProvider
            ->method('getData')
            ->willReturn(
                $this->getMockForAbstractClass(PermissionSecretDataContainerInterface::class)
            );

        $this->validator = $objectManager->getObject(
            PermissionValidator::class,
            [
                'configHelper'                  => $configHelper,
                'permissionDataContainer'       => $permissionDataContainer,
                'guestSecretDataProvider'       => $guestSecretDataProvider,
                'boughtProductValidator'        => $boughtProductValidator,
                'allowedCustomerGroupsProvider' => $allowedCustomerGroupsProvider,
            ]
        );

        $this->configHelper = $configHelper;
        $this->boughtProductValidator = $boughtProductValidator;
        $this->allowedCustomerGroupsProvider = $allowedCustomerGroupsProvider;
    }

    /**
     * @dataProvider validateGuestPermissionDataProvider
     *
     * @param string $testCase
     * @param int    $guestPermission
     * @param int    $customerPermission
     * @param int    $productId
     * @param bool   $isProductBought
     * @param array  $allowedGroups
     * @param array  $expect
     */
    public function testValidateGuestPermission(
        $testCase,
        $guestPermission,
        $customerPermission,
        $productId,
        $isProductBought,
        array $allowedGroups,
        array $expect
    ) {
        $this->configHelper
            ->method('getGuestPermission')
            ->willReturn($guestPermission);

        $this->configHelper
            ->method('getCustomerPermission')
            ->willReturn($customerPermission);

        $this->allowedCustomerGroupsProvider
            ->method('isAllGroupsAllowed')
            ->willReturn($allowedGroups['isAll']);

        $this->allowedCustomerGroupsProvider
            ->method('getList')
            ->willReturn($allowedGroups['list']);

        $this->boughtProductValidator
            ->method('isGuestBoughtProduct')
            ->willReturn($isProductBought);

        $permissionDataContainer = $this->validator->validateGuestPermission($productId);

        $this->assertSame(
            $expect['isAllowed'],
            $permissionDataContainer->isAllowed(),
            'Fail test case "' . $testCase . '" isAllowed'
        );

        $this->assertSame(
            $expect['canShowFormForGuest'],
            $permissionDataContainer->canShowFormForGuest(),
            'Fail test case "' . $testCase . '" canShowFormForGuest'
        );

        $this->assertSame(
            $expect['reason'],
            $permissionDataContainer->getReason(),
            'Fail test case "' . $testCase . '" getReason'
        );

        $this->assertTrue(
            $permissionDataContainer->isDefinedAllow(),
            'Fail test case "' . $testCase . '" isDefinedAllow'
        );
    }

    /**
     * @return array
     */
    public function validateGuestPermissionDataProvider()
    {
        return [
            // Guest ANY
            [
                'testCase'           => 'Guest ANY w/ product',
                'guestPermission'    => PermissionTypes::PERMISSION_ANY,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => true,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_NO_REASON,
                ],
            ],
            [
                'testCase'           => 'Guest ANY w/o product',
                'guestPermission'    => PermissionTypes::PERMISSION_ANY,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => false,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => true,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_NO_REASON,
                ],
            ],

            // Guest BOUGHT
            [
                'testCase'           => 'Guest BOUGHT w/ product',
                'guestPermission'    => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => true,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_NO_REASON,
                ],
            ],
            [
                'testCase'           => 'Guest BOUGHT w/o product',
                'guestPermission'    => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => false,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_ONLY_BUYER,
                ],
            ],

            // Guest CANNOT WRITE
            [
                'testCase'           => 'Guest CANNOT WRITE w/ product.  C - ANY, all groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => true,
                    'reason'              => PermissionHelper::REASON_NO_REASON,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/o product. C - ANY, all groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => false,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => true,
                    'reason'              => PermissionHelper::REASON_NO_REASON,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/ product. C - ANY, some groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => false,
                    'list'  => [1, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/o product. C - ANY, some groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 42,
                'isProductBought'    => false,
                'allowedGroups'      => [
                    'isAll' => false,
                    'list'  => [1, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/ product. C - BOUGHT, all groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'productId'          => 42,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_ONLY_REGISTERED_BUYERS,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/o product. C - BOUGHT, all groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'productId'          => 42,
                'isProductBought'    => false,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_ONLY_REGISTERED_BUYERS,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/ product. C - BOUGHT, some groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'productId'          => 42,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => false,
                    'list'  => [1, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/o product. C - BOUGHT, some groups',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'productId'          => 42,
                'isProductBought'    => false,
                'allowedGroups'      => [
                    'isAll' => false,
                    'list'  => [1, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/ product. C - CANNOT WRITE',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'productId'          => 42,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'           => 'Guest CANNOT WRITE w/o product. C - CANNOT WRITE',
                'guestPermission'    => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'customerPermission' => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'productId'          => 42,
                'isProductBought'    => false,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'           => 'Guest BOUGHT w/ product. Invalid product id ""',
                'guestPermission'    => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => '',
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_ONLY_BUYER,
                ],
            ],
            [
                'testCase'           => 'Guest BOUGHT w/ product. Invalid product id 0',
                'guestPermission'    => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'customerPermission' => PermissionTypes::PERMISSION_ANY,
                'productId'          => 0,
                'isProductBought'    => true,
                'allowedGroups'      => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'expect'             => [
                    'isAllowed'           => false,
                    'canShowFormForGuest' => false,
                    'reason'              => PermissionHelper::REASON_ONLY_BUYER,
                ],
            ],
        ];
    }

    /**
     * @dataProvider validateCustomerPermissionDataProvider
     *
     * @param string $testCase
     * @param        $guestPermission
     * @param        $customerPermission
     * @param array  $allowedGroups
     * @param        $customerId
     * @param        $customerGroupId
     * @param        $productId
     * @param bool   $isProductBought
     * @param        $skipBoughtValidation
     * @param array  $expect
     */
    public function testValidateCustomerPermission(
        $testCase,
        $guestPermission,
        $customerPermission,
        array $allowedGroups,
        $customerId,
        $customerGroupId,
        $productId,
        $isProductBought,
        $skipBoughtValidation,
        array $expect
    ) {
        $this->configHelper
            ->method('getGuestPermission')
            ->willReturn($guestPermission);

        $this->configHelper
            ->method('getCustomerPermission')
            ->willReturn($customerPermission);

        $this->allowedCustomerGroupsProvider
            ->method('isAllGroupsAllowed')
            ->willReturn($allowedGroups['isAll']);

        $this->allowedCustomerGroupsProvider
            ->method('getList')
            ->willReturn($allowedGroups['list']);

        $this->boughtProductValidator
            ->method('isCustomerBoughtProduct')
            ->willReturn($isProductBought);

        $permissionDataContainer = $this->validator->validateCustomerPermission(
            $customerId,
            $customerGroupId,
            $productId,
            $skipBoughtValidation
        );

        $this->assertSame(
            $expect['isAllowed'],
            $permissionDataContainer->isAllowed(),
            'Fail test case "' . $testCase . '" isAllowed'
        );

        $this->assertFalse(
            $permissionDataContainer->canShowFormForGuest(),
            'Fail test case "' . $testCase . '" canShowFormForGuest'
        );

        $this->assertSame(
            $expect['reason'],
            $permissionDataContainer->getReason(),
            'Fail test case "' . $testCase . '" getReason'
        );

        $this->assertTrue(
            $permissionDataContainer->isDefinedAllow(),
            'Fail test case "' . $testCase . '" isDefinedAllow'
        );
    }

    /**
     * @return array
     */
    public function validateCustomerPermissionDataProvider()
    {
        return [
            // Customer ANY
            [
                'testCase'             => 'Customer ANY w/ product',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ANY,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => true,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => true,
                    'reason'    => PermissionHelper::REASON_NO_REASON,
                ],
            ],
            [
                'testCase'             => 'Customer ANY w/o product',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ANY,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => false,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => true,
                    'reason'    => PermissionHelper::REASON_NO_REASON,
                ],
            ],
            [
                'testCase'             => 'Customer ANY invalid customer id',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ANY,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => '',
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => false,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'             => 'Customer ANY not allowed group',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ANY,
                'allowedGroups'        => [
                    'isAll' => false,
                    'list'  => [1, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => true,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],

            // Customer BOUGHT
            [
                'testCase'             => 'Customer BOUGHT w/ product',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => true,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => true,
                    'reason'    => PermissionHelper::REASON_NO_REASON,
                ],
            ],
            [
                'testCase'             => 'Customer BOUGHT w/o product',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => false,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_ONLY_BUYER,
                ],
            ],
            [
                'testCase'             => 'Customer BOUGHT w/o product, invalid product id',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => '',
                'isProductBought'      => false,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_ONLY_BUYER,
                ],
            ],
            [
                'testCase'             => 'Customer BOUGHT not allowed group',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_ONLY_BOUGHT,
                'allowedGroups'        => [
                    'isAll' => false,
                    'list'  => [1, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => true,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],

            // Customer CANNOT_WRITE
            [
                'testCase'             => 'Customer CANNOT w/ product',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => true,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'             => 'Customer CANNOT w/o product',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'allowedGroups'        => [
                    'isAll' => true,
                    'list'  => [1, 2, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => false,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
            [
                'testCase'             => 'Customer CANNOT not allowed group',
                'guestPermission'      => PermissionTypes::PERMISSION_ANY,
                'customerPermission'   => PermissionTypes::PERMISSION_CANNOT_WRITE,
                'allowedGroups'        => [
                    'isAll' => false,
                    'list'  => [1, 3],
                ],
                'customerId'           => 7,
                'customerGroupId'      => 2,
                'productId'            => 42,
                'isProductBought'      => true,
                'skipBoughtValidation' => false,
                'expect'               => [
                    'isAllowed' => false,
                    'reason'    => PermissionHelper::REASON_CANNOT_WRITE,
                ],
            ],
        ];
    }
}
