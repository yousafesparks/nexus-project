<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\ReviewList;

/**
 * @since 2.0.0
 */
class HeaderLine extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface
     */
    private $getReviewCount;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * HeaderLine constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                        $context
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface       $getReviewCount
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                     $configHelper
     * @param array                                                                   $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface $getReviewCount,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->currentProductRetriever = $currentProductRetriever;
        $this->getReviewCount = $getReviewCount;
        $this->configHelper = $configHelper;
    }

    /**
     * @return bool
     */
    private function canShow() : bool
    {
        return $this->currentProductRetriever->execute()
            && $this->getReviewCount->execute($this->currentProductRetriever->execute()->getId());
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        if (! $this->canShow()) {
            return '';
        }
        return parent::_toHtml();
    }

    /**
     * @return string
     */
    public function getAllReviewsUrl() : string
    {
        return $this->getUrl('review/product/list', ['id' => $this->currentProductRetriever->execute()->getId()]);
    }

    /**
     * @return int
     */
    public function getTotalCount() : int
    {
        return $this->getReviewCount->execute($this->currentProductRetriever->execute()->getId());
    }

    /**
     * @return bool
     */
    public function isTwoPageDesign() : bool
    {
        return $this->configHelper->useSeparatePage();
    }
}
