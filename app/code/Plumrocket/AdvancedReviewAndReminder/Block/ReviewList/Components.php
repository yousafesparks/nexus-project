<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\ReviewList;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

/**
 * Class ReviewList
 * @since 2.0.0
 * @method getShowTitleAsLink()
 */
class Components extends \Magento\Framework\View\Element\Template
{
    /**
     * Define if show one review or list of reviews
     */
    const TYPE_LIST = 'list';
    const TYPE_ITEM = 'item';

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    /**
     * @var string
     */
    private $toolbarHtml;

    /**
     * @var string
     */
    private $productViewHtml;

    /**
     * ReviewList constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context    $context
     * @param \Magento\Framework\Registry                         $coreRegistry
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     * @param \Magento\Customer\Model\Session                     $customerSession
     * @param \Magento\Framework\Serialize\SerializerInterface    $serializer
     * @param array                                               $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->configHelper = $configHelper;
        $this->customerSession = $customerSession;
        $this->serializer = $serializer;
    }

    /**
     * @return string
     */
    public function getToolbarJsHtml() : string
    {
        if (null === $this->toolbarHtml) {
            $this->toolbarHtml = $this->getChildHtml('review.list.toolbar');
        }
        return $this->toolbarHtml;
    }

    /**
     * @return string
     */
    public function getStickyProductBlockHtml(): string
    {
        if (null === $this->productViewHtml) {
            $productViewBlock = $this->getChildBlock('review.product.view');

            if ($productViewBlock) {
                $productViewBlock->setProduct($this->getProductInfo());
                $this->productViewHtml = $productViewBlock->toHtml();
            } else {
                $this->productViewHtml = '';
            }
        }
        return $this->productViewHtml;
    }

    /**
     * Retrieve currently viewed product object
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProductInfo() : \Magento\Catalog\Api\Data\ProductInterface
    {
        if (! $this->hasData('product')) {
            $this->setData('product', $this->coreRegistry->registry('product'));
        }
        return $this->getData('product');
    }

    /**
     * Retrieve current review model from registry
     *
     * @return \Magento\Review\Model\Review
     */
    public function getReview()
    {
        return $this->coreRegistry->registry('current_review');
    }

    /**
     * @return false|string
     */
    public function getJsLayout()
    {
        $layout = (array) $this->serializer->unserialize(parent::getJsLayout());

        if (isset($layout['components']['pr-review-list'])) {
            $layout['components']['pr-review-list'] = array_merge_recursive(
                $layout['components']['pr-review-list'],
                $this->getJsComponentConfig()
            );
        } else {
            $layout['components']['pr-review-list'] = $this->getJsComponentConfig();
        }

        return $layout ? $this->serializer->serialize($layout) : '';
    }

    /**
     * @return array
     */
    public function getJsComponentConfig() : array
    {
        if ($this->getRequest()->getFullActionName() === 'advancedrar_review_view') {
            $isShowTitleAsLink = false;
        } elseif (null === $this->getShowTitleAsLink()) {
            $isShowTitleAsLink = true;
        } else {
            $isShowTitleAsLink = $this->getShowTitleAsLink();
        }

        return [
            'configuration' => [
                'sourceUrl' => $this->getUrl(
                    'advancedrar/review/list',
                    ['product' => $this->getProductInfo()->getId()]
                ),
                'enabledSummary' => $this->configHelper->getReviewSummaryOption() !== Option::NO,
                'enabledVerified' => $this->configHelper->isEnabledVerified(),
                'enabledProsAndCons' => $this->configHelper->getProsAndConsOption() !== Option::NO,
                'enabledHelpful' => $this->configHelper->isEnabledHelpful(),
                'helpfulUrl' => $this->getUrl('advancedrar/review/helpful'),
                'canRateHelpful' => $this->canRate(),
                'canReportAbuse' => $this->canReportAbuse(),
                'abuseUrl' => $this->getUrl('advancedrar/review/abuse'),
                'showTitleAsLink' => $isShowTitleAsLink,
                'type' => $this->getPageType(),
                'reviewId' => ! $this->isList() ? (int) $this->getReview()->getId() : 0,
                'enableRecommend' => $this->configHelper->isEnabledRecommendedToAFriend(),
                'toolbar' => (bool) $this->getToolbarJsHtml(),
                'productView' => $this->getStickyProductBlockHtml(),
            ],
        ];
    }

    /**
     * @return bool
     */
    public function canRate() : bool
    {
        return $this->configHelper->isEnabledHelpful()
            && (0 !== $this->customerSession->getCustomerGroupId() || $this->configHelper->isEnabledHelpfulForGuest());
    }

    /**
     * @return bool
     */
    public function canReportAbuse() : bool
    {
        return $this->configHelper->isEnabledAbuse()
            && (0 !== $this->customerSession->getCustomerGroupId() || $this->configHelper->isEnabledAbuseForGuest());
    }

    /**
     * @return string
     */
    public function getPageType() : string
    {
        return $this->getData('list_type') ?: self::TYPE_LIST;
    }

    /**
     * Retrieve if render for one review or for list of reviews
     *
     * @return bool
     */
    public function isList() : bool
    {
        return self::TYPE_LIST === $this->getPageType();
    }
}
