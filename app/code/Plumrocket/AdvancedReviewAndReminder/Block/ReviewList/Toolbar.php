<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\ReviewList;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Plumrocket\AdvancedReviewAndReminder\Helper\Config;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\RatingFilter;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Sorting;

/**
 * @since 2.0.0
 */
class Toolbar extends Template
{
    /**
     * @var Config
     */
    private $configHelper;

    /**
     * @var Sorting
     */
    private $reviewSorting;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var RatingFilter
     */
    private $ratingFilter;

    /**
     * Toolbar constructor.
     *
     * @param Context             $context
     * @param Config              $configHelper
     * @param Sorting             $reviewSorting
     * @param SerializerInterface $serializer
     * @param RatingFilter        $ratingFilter
     * @param array               $data
     */
    public function __construct(
        Context $context,
        Config $configHelper,
        Sorting $reviewSorting,
        SerializerInterface $serializer,
        RatingFilter $ratingFilter,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->configHelper = $configHelper;
        $this->reviewSorting = $reviewSorting;
        $this->serializer = $serializer;
        $this->ratingFilter = $ratingFilter;
    }

    /**
     * @return string
     */
    public function getJsLayout(): string
    {
        $layout = (array) $this->serializer->unserialize(parent::getJsLayout());

        if (isset($layout['components']['pr-review-list-toolbar'])) {
            $layout['components']['pr-review-list-toolbar'] = array_merge_recursive(
                $layout['components']['pr-review-list-toolbar'],
                $this->getJsComponentConfig()
            );
        } else {
            $layout['components']['pr-review-list-toolbar'] = $this->getJsComponentConfig();
        }

        return $layout ? $this->serializer->serialize($layout) : '';
    }

    /**
     * @return array
     */
    private function getJsComponentConfig(): array
    {
        return [
            'filterList' => [
                'general' => [
                    'options' => $this->getGeneralFilterOptions(),
                    'defaultValue' => 'all',
                    'initialValue' => 'all',
                    'stateLabel' => __('Filter By:'),
                ],
                'rating' => [
                    'options' => $this->ratingFilter->toOptionArray(),
                    'defaultValue' => 'all',
                    'initialValue' => 'all',
                    'stateLabel' => __('Stars Filter:'),
                ],
                'search' => [
                    'defaultValue' => '',
                    'initialValue' => '',
                    'stateLabel' => __('Containing:'),
                ],
            ],
            'mobile' => [
                'filterList' => [
                    'general' => [
                        'options' => $this->getMobileGeneralFilterOptions(),
                    ],
                    'rating' => [
                        'options' => $this->getMobileRatingFilterOptions(),
                    ],
                ],
            ],
            'sort' => [
                'options' => $this->reviewSorting->toOptionArray(),
                'defaultValue' => $this->reviewSorting->getDefaultSorting(),
                'initialValue' => $this->reviewSorting->getDefaultSorting(),
            ]
        ];
    }

    /**
     * @return array
     */
    private function getGeneralFilterOptions(): array
    {
        $options = [
            [
                'value' => 'all',
                'label' => __('All reviews')
            ]
        ];

        if ($this->configHelper->isEnabledVerified()) {
            $options[] = [
                'value' => 'verified',
                'label' => __('Verified buyers only'),
            ];
        }

        if ($this->configHelper->isEnabledImageUpload()) {
            $options[] = [
                'value' => 'with_media',
                'label' => __('With image and video only'),
            ];
        }

        return count($options) > 1 ? $options : [];
    }

    /**
     * @return array
     */
    private function getMobileGeneralFilterOptions(): array
    {
        $options = [];

        if ($this->configHelper->isEnabledVerified()) {
            $options[] = [
                'value' => 'verified',
                'label' => __('Verified buyers only'),
            ];
        }

        if ($this->configHelper->isEnabledImageUpload()) {
            $options[] = [
                'value' => 'with_media',
                'label' => __('With image and video only'),
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    private function getMobileRatingFilterOptions(): array
    {
        return [
            [
                'value' => RatingFilter::POSITIVE_RATING_KEY,
                'label' => __('All positive'),
            ],
            [
                'value' => RatingFilter::CRITICAL_RATING_KEY,
                'label' => __('All critical'),
            ],
        ];
    }
}
