<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\ReviewHeader;

class Summary extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\RatingSummary
     */
    private $ratingSummaryHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * Summary constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\RatingSummary $ratingSummaryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\RatingSummary $ratingSummaryHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->setProduct($currentProductRetriever->execute());
        $this->configHelper = $configHelper;
        $this->ratingSummaryHelper = $ratingSummaryHelper;
    }

    /**
     * @return int
     */
    public function getReviewsCount() : int
    {
        $product = $this->getProduct();
        $this->ratingSummaryHelper->initRatingSummary($product);

        return is_object($product->getRatingSummary())
            ? (int) $product->getRatingSummary()->getReviewsCount()
            : (int) $product->getReviewsCount();
    }

    /**
     * @return int
     */
    public function getRatingSummary() : int
    {
        $product = $this->getProduct();
        $this->ratingSummaryHelper->initRatingSummary($product);

        return is_object($product->getRatingSummary())
            ? (int) $product->getRatingSummary()->getRatingSummary()
            : (int) $product->getRatingSummary();
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return round((int) $this->getRatingSummary() * 0.05, 1);
    }

    /**
     * @return string
     */
    public function getAllReviewsUrl() : string
    {
        return $this->getUrl('review/product/list', ['id' => $this->getProduct()->getId()]);
    }

    /**
     * @return bool
     */
    public function showReviewHeader() : bool
    {
        return $this->configHelper->showReviewHeader();
    }
}
