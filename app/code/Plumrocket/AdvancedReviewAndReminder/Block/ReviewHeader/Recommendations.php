<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\ReviewHeader;

class Recommendations extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Recommend\GetRecommendPercent
     */
    private $getRecommendPercent;

    /**
     * @var $allResponses
     */
    private $allResponses;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * Recommendations constructor.
     * phpcs:disable Generic.Files.LineLength
     * @param \Magento\Framework\View\Element\Template\Context                                        $context
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever                 $currentProductRetriever
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Recommend\GetRecommendPercent $getRecommendPercent
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                                     $configHelper
     * @param array                                                                                   $data
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Recommend\GetRecommendPercent $getRecommendPercent,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->currentProductRetriever = $currentProductRetriever;
        $this->getRecommendPercent = $getRecommendPercent;
        $this->configHelper = $configHelper;
    }

    /**
     * @return \Magento\Catalog\Model\Product|null
     */
    private function getProduct()
    {
        return $this->currentProductRetriever->execute();
    }

    /**
     * @return float
     */
    public function getRecommend() : float
    {
        if ($product = $this->getProduct()) {
            $allResponses = $this->getRecommendPercent->execute((int) $product->getId());
            $this->allResponses = count($allResponses);

            if ($this->allResponses) {
                $recommend = array_filter($allResponses, function ($response) {
                    return $response == \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend::YES;
                });

                return round(count($recommend) * 100 / $this->allResponses, 0);
            }
        }

        return 0;
    }

    /**
     * @return bool
     */
    public function showReviewHeader() : bool
    {
        return $this->configHelper->showReviewHeader();
    }
}
