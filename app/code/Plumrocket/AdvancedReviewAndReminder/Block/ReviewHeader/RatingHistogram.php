<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\ReviewHeader;

class RatingHistogram extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingsPercent
     */
    private $getRatingsPercent;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\RatingSummary
     */
    private $ratingSummaryHelper;

    /**
     * RatingHistogram constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingsPercent $getRatingsPercent
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\RatingSummary $ratingSummaryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Rating\GetRatingsPercent $getRatingsPercent,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\RatingSummary $ratingSummaryHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->getRatingsPercent = $getRatingsPercent;
        $this->currentProductRetriever = $currentProductRetriever;
        $this->configHelper = $configHelper;
        $this->ratingSummaryHelper = $ratingSummaryHelper;
    }

    /**
     * @return int[]
     */
    public function getGroupedRatingPercent() : array
    {
        $percents = [
            '5' => 0,
            '4' => 0,
            '3' => 0,
            '2' => 0,
            '1' => 0
        ];

        $product = $this->currentProductRetriever->execute();

        if ($product->getId()) {
            $ratings = $this->getRatingsPercent->execute((int) $product->getId());
            if ($countRatings = count($ratings)) {
                foreach ($ratings as $rating) {
                    $id = floor($rating * 0.05);
                    $percents[$id]++;
                }

                foreach ($percents as $id => $percent) {
                    $percents[$id] = round($percent * 100 / $countRatings);
                }
            }
        }

        return $percents;
    }

    /**
     * @return int
     */
    public function getRatingSummary() : int
    {
        $product = $this->currentProductRetriever->execute();

        if (! $product) {
            return 0;
        }

        $this->ratingSummaryHelper->initRatingSummary($product);

        return is_object($product->getRatingSummary())
            ? (int) $product->getRatingSummary()->getRatingSummary()
            : (int) $product->getRatingSummary();
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return round((int) $this->getRatingSummary() * 0.05, 1);
    }

    /**
     * @return bool
     */
    public function showReviewHeader() : bool
    {
        return $this->configHelper->showReviewHeader() && $this->getRating();
    }

    /**
     * @param int $star
     * @return string
     */
    public function getUrlWithFilter(int $star) : string
    {
        $product = $this->currentProductRetriever->execute();
        return $this->getUrl(
            'review/product/list',
            [
                'id' => $product->getId(),
                '_query' => 'reviewRatingFilter=' . $star,
                '_fragment' => 'customer-reviews-box'
            ]
        );
    }
}
