<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\UnsubscribeReason\Edit\Tab;

class StoreLabels extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * System store
     *
     * @var \Magento\Store\Model\System\Store
     */
    private $systemStore;

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * StoreLabels constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        array $data = []
    ) {
        $this->systemStore = $systemStore;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @inheritDoc
     */
    protected function _prepareForm()
    {
        $this->setForm($this->_formFactory->create());
        $this->addLabelsFieldset();
        return parent::_prepareForm();
    }

    protected function addLabelsFieldset()
    {
        $fieldset = $this->getForm()->addFieldset('store_labels', []);

        foreach ($this->systemStore->getStoreCollection() as $store) {
            $fieldset->addField(
                'store_label_' . $store->getId(),
                'text',
                [
                    'label' => $store->getName(),
                    'name' => 'store_label[' . $store->getId() . ']',
                    'data-form-part' => 'advancedrar_unsubscribe_reason_form',
                ]
            );
        }

        $this->setStoreLabels();
    }

    protected function setStoreLabels()
    {
        $reason = $this->dataPersistor->get('unsubscribe_reason');
        $this->dataPersistor->clear('unsubscribe_reason');

        if ($reason) {
            $this->getForm()->setValues($reason->getData());

            foreach ($reason->getStoreLabels() as $storeId => $label) {
                $element = $this->getForm()->getElement('store_label_' . $storeId);
                if ($element) {
                    $element->setValue($label);
                }
            }
        }
    }
}
