<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\UnsubscribeReason\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface;

class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var UnsubscribeReasonRepositoryInterface
     */
    protected $reasonRepository;

    /**
     * GenericButton constructor.
     *
     * @param Context $context
     * @param UnsubscribeReasonRepositoryInterface $reasonRepository
     */
    public function __construct(
        Context $context,
        UnsubscribeReasonRepositoryInterface $reasonRepository
    ) {
        $this->context = $context;
        $this->reasonRepository = $reasonRepository;
    }

    /**
     * Return CMS page ID
     *
     * @return int|null
     */
    public function getReasonId()
    {
        try {
            return $this->reasonRepository->getById(
                $this->context->getRequest()->getParam('reason_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
