<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Review\Add;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

class Form extends \Magento\Review\Block\Adminhtml\Add\Form
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    private $yesno;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    private $wysiwygConfig;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend
     */
    private $recommend;

    /**
     * @param \Magento\Backend\Block\Template\Context                             $context
     * @param \Magento\Framework\Registry                                         $registry
     * @param \Magento\Framework\Data\FormFactory                                 $formFactory
     * @param \Magento\Store\Model\System\Store                                   $systemStore
     * @param \Magento\Review\Helper\Data                                         $reviewData
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                 $configHelper
     * @param \Magento\Config\Model\Config\Source\Yesno                           $yesno
     * @param \Magento\Cms\Model\Wysiwyg\Config                                   $wysiwygConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend
     * @param array                                                               $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Review\Helper\Data $reviewData,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $systemStore, $reviewData, $data);
        $this->configHelper = $configHelper;
        $this->yesno = $yesno;
        $this->wysiwygConfig = $wysiwygConfig;
        $this->recommend = $recommend;
    }

    /**
     * Prepare add review form
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $fieldset = $form->addFieldset('add_review_form', ['legend' => __('Review Details')]);

        $fieldset->addField('product_name', 'note', ['label' => __('Product'), 'text' => 'product_name']);

        $fieldset->addField(
            'detailed-rating',
            'note',
            [
                'label' => __('Product Rating'),
                'required' => true,
                'text' => '<div id="rating_detail">' . $this->getLayout()->createBlock(
                    \Magento\Review\Block\Adminhtml\Rating\Detailed::class
                )->toHtml() . '</div>'
            ]
        );

        $fieldset->addField(
            'status_id',
            'select',
            [
                'label' => __('Status'),
                'required' => true,
                'name' => 'status_id',
                'values' => $this->_reviewData->getReviewStatusesOptionArray()
            ]
        );

        if ($this->configHelper->isEnabledVerified()) {
            $fieldset->addField(
                'verified',
                'select',
                [
                    'label'    => __('Verified'),
                    'required' => false,
                    'name'     => 'verified',
                    'values'   => $this->yesno->toOptionArray(),
                    'value'    => 0,
                ]
            );
        }

        /**
         * Check is single store mode
         */
        if (!$this->_storeManager->isSingleStoreMode()) {
            $field = $fieldset->addField(
                'select_stores',
                'multiselect',
                [
                    'label' => __('Visibility'),
                    'required' => true,
                    'name' => 'select_stores[]',
                    'values' => $this->_systemStore->getStoreValuesForForm()
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                \Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element::class
            );
            $field->setRenderer($renderer);
        }

        $fieldset->addField(
            'nickname',
            'text',
            [
                'name' => 'nickname',
                'title' => __('Nickname'),
                'label' => __('Nickname'),
                'maxlength' => '50',
                'required' => true
            ]
        );

        if (Option::NO !== $this->configHelper->getReviewSummaryOption()) {
            $fieldset->addField(
                'title',
                'text',
                [
                    'name' => 'title',
                    'title' => __('Summary of Review'),
                    'label' => __('Summary of Review'),
                    'maxlength' => '255',
                    'required' => true
                ]
            );
        }

        $fieldset->addField(
            'detail',
            'textarea',
            [
                'name' => 'detail',
                'title' => __('Review'),
                'label' => __('Review'),
                'required' => true
            ]
        );

        if ($this->configHelper->isEnabledAbuse()) {
            $fieldset->addField(
                'abuse',
                'select',
                [
                    'label'    => __('Abuse'),
                    'required' => false,
                    'name'     => 'abuse',
                    'values'   => $this->yesno->toOptionArray(),
                    'value'    => 0,
                ]
            );
        }

        if (Option::NO !== $this->configHelper->getProsAndConsOption()) {
            $fieldset->addField(
                'pros',
                'textarea',
                [
                    'label'    => __('Pros'),
                    'required' => false,
                    'name'     => 'pros',
                    'style'    => 'height:12em;',
                    'value'    => '',
                ]
            );

            $fieldset->addField(
                'cons',
                'textarea',
                [
                    'label'    => __('Cons'),
                    'required' => false,
                    'name'     => 'cons',
                    'style'    => 'height:12em;',
                    'value'    => '',
                ]
            );
        }

        if ($this->configHelper->isEnabledRecommendedToAFriend()) {
            $fieldset->addField(
                'recommend',
                'select',
                [
                    'label' => __('Does reviewer recommend this product?'),
                    'required' => false,
                    'name' => 'recommend',
                    'values' => $this->recommend->toOptionArray(),
                ]
            );
        }

        if ($this->configHelper->isEnabledHelpful()) {
            $fieldset->addField(
                'helpful_positive',
                'text',
                [
                    'label'    => __('Helpfulness (Positive Votes)'),
                    'required' => false,
                    'name'     => 'helpful_positive',
                    'value'    => 0,
                ]
            );

            $fieldset->addField(
                'helpful_negative',
                'text',
                [
                    'label'    => __('Helpfulness (Negative Votes)'),
                    'required' => false,
                    'name'     => 'helpful_negative',
                    'value'    => 0,
                ]
            );
        }

        $fieldset->addField(
            'admin_comment',
            'editor',
            [
                'label'    => __('Administrator Comment'),
                'required' => false,
                'name'     => 'admin_comment',
                'wysiwyg'  => true,
                'config'   => $this->wysiwygConfig->getConfig(),
                'value'    => '',
                'style'    => 'height:12em;',
            ]
        );

        $fieldset->addField('product_id', 'hidden', ['name' => 'product_id']);

        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('review/product/post'));

        $this->setForm($form);
    }
}
