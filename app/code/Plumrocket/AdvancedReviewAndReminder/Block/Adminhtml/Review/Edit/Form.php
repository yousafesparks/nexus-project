<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Review\Edit;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

class Form extends \Magento\Review\Block\Adminhtml\Edit\Form
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    private $yesno;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    private $wysiwygConfig;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend
     */
    private $recommend;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * Form constructor.
     *
     * @param \Magento\Backend\Block\Template\Context                             $context
     * @param \Magento\Framework\Registry                                         $registry
     * @param \Magento\Framework\Data\FormFactory                                 $formFactory
     * @param \Magento\Store\Model\System\Store                                   $systemStore
     * @param \Magento\Customer\Api\CustomerRepositoryInterface                   $customerRepository
     * @param \Magento\Catalog\Model\ProductFactory                               $productFactory
     * @param \Magento\Review\Helper\Data                                         $reviewData
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                 $configHelper
     * @param \Magento\Config\Model\Config\Source\Yesno                           $yesno
     * @param \Magento\Cms\Model\Wysiwyg\Config                                   $wysiwygConfig
     * @param \Magento\Framework\Serialize\SerializerInterface                    $serializer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend
     * * @param \Magento\Framework\App\ProductMetadataInterface                   $productMetadata
     * @param array                                                               $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Review\Helper\Data $reviewData,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $systemStore,
            $customerRepository,
            $productFactory,
            $reviewData,
            $data
        );
        $this->configHelper = $configHelper;
        $this->yesno = $yesno;
        $this->wysiwygConfig = $wysiwygConfig;
        $this->serializer = $serializer;
        $this->recommend = $recommend;
        $this->productMetadata = $productMetadata;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Form|\Magento\Review\Block\Adminhtml\Edit\Form
     */
    protected function _prepareForm()
    {
        if (! $this->configHelper->isModuleEnabled()) {
            return parent::_prepareForm();
        }

        $review = $this->_coreRegistry->registry('review_data');
        $product = $this->_productFactory->create()->load($review->getEntityPkValue());

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl(
                        'review/*/save',
                        [
                            'id' => $this->getRequest()->getParam('id'),
                            'ret' => $this->_coreRegistry->registry('ret')
                        ]
                    ),
                    'method' => 'post',
                ],
            ]
        );

        $fieldset = $form->addFieldset(
            'review_details',
            ['legend' => __('Review Details'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'product_name',
            'note',
            [
                'label' => __('Product'),
                'text' => '<a href="' . $this->getUrl(
                        'catalog/product/edit',
                        ['id' => $product->getId()]
                    ) . '" onclick="this.target=\'blank\'">' . $this->escapeHtml(
                        $product->getName()
                    ) . '</a>'
            ]
        );

        try {
            $customer = $this->customerRepository->getById($review->getCustomerId());
            $customerText = __(
                '<a href="%1" onclick="this.target=\'blank\'">%2 %3</a> <a href="mailto:%4">(%4)</a>',
                $this->getUrl('customer/index/edit', ['id' => $customer->getId(), 'active_tab' => 'review']),
                $this->escapeHtml($customer->getFirstname()),
                $this->escapeHtml($customer->getLastname()),
                $this->escapeHtml($customer->getEmail())
            );
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $customerText = ($review->getStoreId() == \Magento\Store\Model\Store::DEFAULT_STORE_ID)
                ? __('Administrator') : __('Guest');
        }

        $fieldset->addField('customer', 'note', ['label' => __('Author'), 'text' => $customerText]);

        /** @var \Magento\Review\Block\Adminhtml\Rating\Summary $ratingSummaryBlock */
        $ratingSummaryBlock = $this->getLayout()->createBlock(\Magento\Review\Block\Adminhtml\Rating\Summary::class);
        if (version_compare($this->productMetadata->getVersion(), '2.4', '>')) {
            $ratingSummaryBlock->setTemplate('Plumrocket_AdvancedReviewAndReminder::rating/stars/summary.phtml');
        }

        $fieldset->addField(
            'summary-rating',
            'note',
            [
                'label' => __('Summary Rating'),
                'text' => $ratingSummaryBlock->toHtml()
            ]
        );

        $fieldset->addField(
            'detailed-rating',
            'note',
            [
                'label' => __('Detailed Rating'),
                'required' => true,
                'text' => '<div id="rating_detail">' . $this->getLayout()->createBlock(
                        \Magento\Review\Block\Adminhtml\Rating\Detailed::class
                    )->toHtml() . '</div>'
            ]
        );

        $fieldset->addField(
            'status_id',
            'select',
            [
                'label' => __('Status'),
                'required' => true,
                'name' => 'status_id',
                'values' => $this->_reviewData->getReviewStatusesOptionArray()
            ]
        );

        if ($this->configHelper->isEnabledVerified()) {
            $fieldset->addField(
                'verified',
                'select',
                [
                    'label'    => __('Verified Buyer'),
                    'required' => false,
                    'name'     => 'verified',
                    'values'   => $this->yesno->toOptionArray(),
                    'value'    => $review->getVerified(),
                ]
            );
        }

        /**
         * Check is single store mode
         */
        if (!$this->_storeManager->hasSingleStore()) {
            $field = $fieldset->addField(
                'select_stores',
                'multiselect',
                [
                    'label' => __('Visibility'),
                    'required' => true,
                    'name' => 'stores[]',
                    'values' => $this->_systemStore->getStoreValuesForForm()
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                \Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element::class
            );
            $field->setRenderer($renderer);
            $review->setSelectStores($review->getStores());
        } else {
            $fieldset->addField(
                'select_stores',
                'hidden',
                ['name' => 'stores[]', 'value' => $this->_storeManager->getStore(true)->getId()]
            );
            $review->setSelectStores($this->_storeManager->getStore(true)->getId());
        }

        $fieldset->addField(
            'nickname',
            'text',
            ['label' => __('Nickname'), 'required' => true, 'name' => 'nickname']
        );

        if (Option::NO !== $this->configHelper->getReviewSummaryOption()) {
            $fieldset->addField(
                'title',
                'text',
                [
                    'label' => __('Summary of Review'),
                    'required' => Option::REQUIRED === $this->configHelper->getReviewSummaryOption(),
                    'name' => 'title'
                ]
            );
        }

        $fieldset->addField(
            'detail',
            'textarea',
            ['label' => __('Review'), 'required' => true, 'name' => 'detail', 'style' => 'height:24em;']
        );

        if ($this->configHelper->isEnabledAbuse()) {
            $fieldset->addField(
                'abuse',
                'select',
                [
                    'label'    => __('Abuse'),
                    'required' => false,
                    'name'     => 'abuse',
                    'values'   => $this->yesno->toOptionArray(),
                    'value'    => $review->getAbuse(),
                ]
            );
        }

        if (Option::NO !== $this->configHelper->getProsAndConsOption()) {
            $fieldset->addField(
                'pros',
                'textarea',
                [
                    'label'    => __('Pros'),
                    'required' => false,
                    'name'     => 'pros',
                    'style'    => 'height:12em;',
                    'value'    => $review->getPros(),
                ]
            );

            $fieldset->addField(
                'cons',
                'textarea',
                [
                    'label'    => __('Cons'),
                    'required' => false,
                    'name'     => 'cons',
                    'style'    => 'height:12em;',
                    'value'    => $review->getCons(),
                ]
            );
        }

        if ($this->configHelper->isEnabledRecommendedToAFriend()) {
            $fieldset->addField(
                'recommend',
                'select',
                [
                    'label' => __('Does reviewer recommend this product?'),
                    'required' => false,
                    'name' => 'recommend',
                    'values' => $this->recommend->toOptionArray(),
                ]
            );
        }

        if ($this->configHelper->isEnabledHelpful()) {
            $fieldset->addField(
                'helpful_positive',
                'text',
                [
                    'label'    => __('Helpfulness (Positive Votes)'),
                    'required' => false,
                    'name'     => 'helpful_positive',
                    'value'    => $review->getHelpfulPositive(),
                ]
            );

            $fieldset->addField(
                'helpful_negative',
                'text',
                [
                    'label'    => __('Helpfulness (Negative Votes)'),
                    'required' => false,
                    'name'     => 'helpful_negative',
                    'value'    => $review->getHelpfulNegative(),
                ]
            );
        }

        $fieldset->addField(
            'admin_comment',
            'editor',
            [
                'label'    => __('Administrator Comment'),
                'required' => false,
                'name'     => 'admin_comment',
                'wysiwyg'  => true,
                'config'   => $this->wysiwygConfig->getConfig(),
                'value'    => $review->getAdminComment(),
                'style'    => 'height:12em;',
            ]
        );

        if ($this->configHelper->isEnabledImageUpload() && $attachImages = $review->getAttachImage()) {
            $attachImages = $this->serializer->unserialize($attachImages);
            $label = count($attachImages) > 1 ? __('Attachment Images') : __('Attachment Image');

            $fieldset->addType(
                'attach_image',
                \Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Element\Images::class
            );

            $fieldset->addField(
                'attach_image',
                'attach_image',
                [
                    'label' => $label,
                    'required' => false,
                    'name' => 'attach_image[]',
                    'value' => '',
                    'style' => 'height:12em;',
                ]
            );

            $review->setData('attach_image', $attachImages);
        }

        if ($this->configHelper->isEnabledVideoLink()) {
            $videoId = $review->getYoutubeId();
            $fieldset->addType(
                'youtube_video',
                \Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Element\Video::class
            );

            $fieldset->addField(
                'youtube_video',
                'youtube_video',
                [
                    'label' =>  __('YouTube Video'),
                    'required' => false,
                    'name' => 'youtube_video',
                    'value' => '',
                    'style' => 'height:12em;',
                ]
            );

            $review->setData('youtube_video', $videoId);
        }

        $form->setUseContainer(true);
        $form->setValues($review->getData());
        $this->setForm($form);
        return \Magento\Backend\Block\Widget\Form::_prepareForm();
    }
}
