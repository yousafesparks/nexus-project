<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Widget;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Helper\Data;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Review\Model\ResourceModel\Review\Product\Collection;
use Magento\Review\Model\ResourceModel\Review\Product\CollectionFactory;
use Magento\Review\Model\Review;

/**
 * @since 2.2.0
 */
class ReviewChooser extends Extended
{
    /**
     * Review collection model factory
     *
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var array
     */
    private $selectedReviews = [];

    /**
     * @var Factory
     */
    private $elementFactory;

    /**
     * Store hidden review ids
     *
     * @var string
     */
    private $elementValueId = '';

    /**
     * ReviewChooser constructor
     *
     * @param Context $context
     * @param Data $backendHelper
     * @param CollectionFactory $collectionFactory
     * @param Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $collectionFactory,
        Factory $elementFactory,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Block construction, prepare grid params
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setUseAjax(true);
        $this->setDefaultFilter(['in_reviews' => 1]);
    }

    /**
     * @param AbstractElement $element
     * @return AbstractElement
     */
    public function prepareElementHtml(AbstractElement $element): AbstractElement
    {
        $this->elementValueId = $element->getId();
        if ($element->getValue()) {
            $this->selectedReviews = $element->getValue();
        }

        $reviewIds = $this->elementFactory->create('hidden', ['data' => $element->getData()]);
        $reviewIds->setId($this->elementValueId)->setForm($element->getForm());
        $reviewIdsHtml = $reviewIds->getElementHtml();

        $customCss = $this->getCustomCss();

        $element->setValue('')->setValueClass('value2');
        $element->setData('css_class', 'grid-chooser');
        $element->setData('after_element_html', $customCss . $reviewIdsHtml . $this->toHtml());
        $element->setData('no_wrap_as_addon', true);

        return $element;
    }

    /**
     * Prepare reviews collection
     *
     * @return Extended
     */
    protected function _prepareCollection(): Extended
    {
        /** @var $collection Collection */
        $collection = $this->collectionFactory->create();
        $collection->addStatusFilter(Review::STATUS_APPROVED);

        $collection->getSelect()->joinLeft(
            ['rating_vote' => $collection->getTable('rating_option_vote')],
            'rt.review_id=rating_vote.review_id',
            ['percent', 'value']
        );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Sets sorting order by some column
     *
     * @param Column $column
     * @return $this
     */
    protected function _setCollectionOrder($column): self
    {
        $collection = $this->getCollection();

        if ('rating' === $column->getId() && $collection) {
            $collection->getSelect()->order($column->getIndex() . ' ' . strtoupper($column->getDir()));
        } else {
            parent::_setCollectionOrder($column);
        }

        return $this;
    }

    /**
     * Prepare grid columns
     *
     * @return Extended
     */
    protected function _prepareColumns(): Extended
    {
        $this->addColumn(
            'in_reviews',
            [
               'header_css_class' => 'col-select',
               'column_css_class' => 'col-select',
               'type' => 'checkbox',
               'name' => 'in_reviews',
               'values' => $this->getSelectedReviews(),
               'field_name' => 'in_reviews',
               'index' => 'review_id',
               'filter_index' => 'rt.review_id',
               'use_index' => true
            ]
        );

        $this->addColumn(
            'review_id',
            [
               'header' => __('ID'),
               'filter_index' => 'rt.review_id',
               'index' => 'review_id',
               'header_css_class' => 'col-id',
               'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'name',
            [
               'header' => __('Product'),
               'type' => 'text',
               'index' => 'name',
               'escape' => true
            ]
        );

        $this->addColumn(
            'review_summary',
            [
               'header' => __('Summary'),
               'filter_index' => 'rdt.title',
               'index' => 'title',
               'type' => 'text',
               'truncate' => 50,
               'escape' => true
            ]
        );

        $this->addColumn(
            'nickname',
            [
               'header' => __('Nickname'),
               'filter_index' => 'rdt.nickname',
               'index' => 'nickname',
               'type' => 'text',
               'truncate' => 50,
               'escape' => true,
               'header_css_class' => 'col-name',
               'column_css_class' => 'col-name'
            ]
        );

        $this->addColumn(
            'rating',
            [
               'header' => __('Rating'),
               'type' => 'text',
               'index' => 'percent',
               'header_css_class' => 'col-name',
               'column_css_class' => 'col-name',
               'frame_callback' => [$this, 'decorateRating'],
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header' => __('Created'),
                'type' => 'date',
                'format' => \IntlDateFormatter::SHORT,
                'filter_index' => 'rt.created_at',
                'index' => 'review_created_at',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'index' => 'detail',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name',
                'filter' => false,
                'sortable' => false,
                'frame_callback' => [$this, 'decorateAction'],
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * Adds additional parameter to URL
     *
     * @return string
     */
    public function getGridUrl(): string
    {
        return $this->getUrl(
            'advancedrar/widget/chooser',
            [
                'uniq_id' => $this->getId(),
                'selected_reviews' => implode(",", $this->selectedReviews)
            ]
        );
    }

    /**
     * Grid Row JS Callback
     *
     * @return string
     */
    public function getRowClickCallback(): string
    {
        return '
           function (grid, event) {
               var hiddenReviewIds = document.getElementById(\''.
                   $this->elementValueId .
                   '\');
               if(! grid.selectedReviews) {
                   grid.selectedReviews = [];

                   if (hiddenReviewIds.value.length !== 0) {
                       var widgetSelectedReviews = hiddenReviewIds.value.split(",");
                       widgetSelectedReviews.forEach(function(element) {
                           grid.selectedReviews.push(element);
                       });
                   }
               }

               var trElement   = Event.findElement(event, "tr");
               var isInput     = Event.element(event).tagName == \'INPUT\';

               if (trElement && isInput) {
                   checkbox = Element.getElementsBySelector(trElement, \'input\');
                   if (checkbox[0].checked) {
                       grid.selectedReviews.push(checkbox[0].value);
                   } else {
                       for ( var i = 0; i < grid.selectedReviews.length; i++) {
                           if (grid.selectedReviews[i] === checkbox[0].value) {
                               grid.selectedReviews.splice(i, 1);
                           }
                       }
                   }

                   hiddenReviewIds.value = grid.selectedReviews.join();
               }
           }
       ';
    }

    /**
     * Checkbox Check JS Callback
     *
     * @return string
     */
    public function getCheckboxCheckCallback(): string
    {
        return 'function (grid, checkbox, checked) {
            if (checkbox.value == \'on\') {
                return;
            }
            var hiddenReviewIds = document.getElementById(\''.
                $this->elementValueId .
            '\');
            if(! grid.selectedReviews) {
               grid.selectedReviews = [];

               if (hiddenReviewIds.value.length !== 0) {
                   var widgetSelectedReviews = hiddenReviewIds.value.split(",");
                   widgetSelectedReviews.forEach(function(element) {
                       grid.selectedReviews.push(element);
                   });
                }
            }

            if (checkbox.checked) {
                grid.selectedReviews.push(checkbox.value);
            } else {
                for ( var i = 0; i < grid.selectedReviews.length; i++) {
                    if (grid.selectedReviews[i] === checkbox.value) {
                        grid.selectedReviews.splice(i, 1);
                    }
                }
            }

            hiddenReviewIds.value = grid.selectedReviews.join();
         }';
    }

    /**
     * Filter checked/unchecked rows in grid
     *
     * @param Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column): self
    {
        $selectedReviews = $this->getSelectedReviews();
        if ($column->getId() === 'in_reviews' && $selectedReviews) {
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('rt.review_id', ['in' => $selectedReviews]);
            } else {
                $this->getCollection()->addFieldToFilter('rt.review_id', ['nin' => $selectedReviews]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * @param $selectedReviews
     * @return $this
     */
    public function setSelectedReviews($selectedReviews): self
    {
        if (is_string($selectedReviews)) {
            $selectedReviews = explode(',', $selectedReviews);
        }

        $this->selectedReviews = $selectedReviews;

        return $this;
    }

    /**
     * Getter
     *
     * @return array
     */
    public function getSelectedReviews(): array
    {
        if ($selectedReviews = $this->getRequest()->getParam('selected_reviews', $this->selectedReviews)) {
            $this->setSelectedReviews($selectedReviews);
        }

        return $this->selectedReviews;
    }

    /**
     * @param $value
     * @return string
     */
    public function decorateRating($value): string
    {
        return '<div class="grid-rating-result">
                    <div class="rating-box">
                        <div class="rating" style="width: ' . $value . '%;"></div>
                    </div>
              </div>';
    }

    /**
     * @param $value
     * @return string
     */
    public function decorateAction($value, $row): string
    {
        return '<div class="admin__field-tooltip">
                    <a href="'
                        . $this->getUrl('review/product/edit', ['id' => $row->getData('review_id')])
                        . '" target="_blank">' . __('View Details') . '</a>
                    <div class="admin__field-tooltip-content">
                     ' . $value . '
                    </div>
                </div>';
    }

    /**
     * @return string
     */
    private function getCustomCss(): string
    {
        $ratingBackgroundImage = $this->getViewFileUrl(
            'Plumrocket_AdvancedReviewAndReminder::images/rating-bg.png'
        );

        $customCss = '<style>';
        $customCss .= '.grid-rating-result .rating-box {
                            background: url(' . $ratingBackgroundImage . ') repeat-x 0 0;
                            font-size: 0;
                            height: 13px;
                            line-height: 0;
                            margin: 4px 0 0;
                            overflow: hidden;
                            width: 90px;
                        }

                        .grid-rating-result .rating-box .rating {
                            background: url(' . $ratingBackgroundImage . ') repeat-x 0 -12px;
                            height: 13px;
                        }
                        div[class*=reviews_ids] > .admin__field-control.control {
                            min-width:75%;
                        }';

        $customCss .= '</style>';

        return $customCss;
    }
}
