<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\System\Config;

/**
 * @deprecated since 2.2.5
 */
class Version extends \Plumrocket\Base\Block\Adminhtml\System\Config\Form\Version
{
}
