<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\System\Config;

class Popuplogin extends Messages
{
    const POPUP_LOGIN_ROW_ID = 'row_advancedrar_review_form_popup_login';

    /**
     * @var \Plumrocket\Base\Api\ExtensionStatusInterface
     */
    private $extensionStatus;

    /**
     * @param \Magento\Backend\Block\Template\Context           $context
     * @param \Magento\Framework\Stdlib\CookieManagerInterface  $cookieManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper
     * @param \Plumrocket\Base\Api\ExtensionStatusInterface     $extensionStatus
     * @param array                                             $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\Base\Api\ExtensionStatusInterface $extensionStatus,
        array $data = []
    ) {
        parent::__construct($context, $cookieManager, $dataHelper, $data);
        $this->extensionStatus = $extensionStatus;
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = '';
        if ($this->showPopupLoginNotification()) {
            $message = __('Please note: It is recommended to add <a href="%1" target="_blank">Plumrocket Popup Login</a> Extension to increase review submission rate by allowing visitors to login and submit reviews on the same page. Get 10% discount on Popup Login Extension with promo code: <strong>ARSAVE10</strong>', $this->getExtensionLink());

            $html .= '<tr id="'. self::POPUP_LOGIN_ROW_ID .'">';
            $html .= '<td colspan="4"><div class="plum-promotion" style="'.$this->getMessageCss() .'">'. $this->getCloseBtn(self::POPUP_LOGIN_ROW_ID) . $message.'</div></td><tr>';
        }
        return $html;
    }

    public function getExtensionLink()
    {
        return 'https://plumrocket.com/magento-popup-login';
    }

    public function showPopupLoginNotification()
    {
         return $this->extensionStatus->isNotInstalled('Popuplogin')
             && ! $this->cookieManager->getCookie(self::POPUP_LOGIN_ROW_ID);
    }
}
