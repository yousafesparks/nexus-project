<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\System\Config;

use Magento\Framework\Data\Form\Element\AbstractElement;

class UpdateButton extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $context;

    /*
     * Set template
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->setTemplate('system/config/update_button.phtml');
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
            ->setData([
            'id'        => 'advancedrar_update_button',
            'label'     => __('Create Review Reminders'),
            'onclick'   => 'advancedrarUpdate()'
        ]);

        return $button->toHtml();
    }

    /**
     * Return ajax url for button
     *
     * @return string
     */
    public function getAjaxCheckUrl()
    {
        return $this->getUrl('advancedrar/update/update');
    }
}
