<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;

class Messages extends Field
{
    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    protected $dataHelper;

    /**
     * Messages constructor.
     *
     * @param \Magento\Backend\Block\Template\Context          $context
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->cookieManager = $cookieManager;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param $block
     * @return string
     */
    public function getCloseBtn($block)
    {
        return '<div onclick="advancedrarCloseMsg(\''
            . $block
            . '\')" style="position: absolute; right: 6px; color: #626262; cursor: pointer; top: 0px;">x</div>'
            . '<script>window.advancedrarCloseMsg = function(block) {
        var date = new Date;
		date.setDate(date.getDate() + 7);
		document.cookie = block + "=closed; expires=" + date.toUTCString() + \';path=/\';
		document.getElementById(block).remove();
}</script>';
    }

    public function getMessageCss()
    {
        return 'padding: 10px; padding-right: 15px; background-color: #E4F2FF;'
            . ' border: 1px solid #A9C9E7; margin-bottom: 7px; position: relative;';
    }
}
