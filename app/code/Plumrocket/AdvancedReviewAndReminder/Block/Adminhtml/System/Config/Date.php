<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Date as MagentoDate;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Backend\Block\Template\Context;

class Date extends Field
{
    protected $store;
    protected $factoryElement;
    protected $factoryCollection;
    protected $escaper;
    protected $localeDate;
    protected $context;

    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->factoryElement = $factoryElement;
        $this->factoryCollection = $factoryCollection;
        $this->context = $context;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $date = new MagentoDate(
            $this->factoryElement,
            $this->factoryCollection,
            $this->context->getEscaper(),
            $this->context->getLocaleDate()
        );

        $format = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);

        $data = [
            'name'      => $element->getName(),
            'html_id'   => $element->getId(),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
        ];
        $date->setData($data);
        $date->setFormat($format);
        $date->setForm($element->getForm());

        return $date->getElementHtml();
    }
}
