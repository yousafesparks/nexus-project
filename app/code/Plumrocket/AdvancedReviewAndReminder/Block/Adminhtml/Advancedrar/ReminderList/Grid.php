<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Advancedrar\ReminderList;

use Plumrocket\AdvancedReviewAndReminder\Model\Reminder;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var Reminder
     */
    private $reminder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory
     */
    private $emailFactory;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var string
     */
    private $emailSubject = '';

    /**
     * @param \Magento\Backend\Block\Template\Context                  $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder     $reminder
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory $emailFactory
     * @param \Magento\Sales\Model\OrderFactory                        $orderFactory
     * @param \Magento\Framework\App\ResourceConnection                $resourceConnection
     * @param \Magento\Backend\Helper\Data                             $backendHelper
     * @param array                                                    $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Model\Reminder $reminder,
        \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory $emailFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->reminder = $reminder;
        $this->emailFactory = $emailFactory;
        $this->orderFactory = $orderFactory;
        $this->resourceConnection = $resourceConnection;
    }

    public function _construct()
    {
        parent::_construct();

        $this->setId('manage_advancedrar_list_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = $this->reminder
            ->getCollection()
            ->addFilterToMap('status', 'main_table.status');

        $collection->getSelect()
            ->joinLeft(
                ['order' => $this->resourceConnection->getTableName('sales_order')],
                'main_table.order_id = order.entity_id',
                ['customer_id', 'customer_email', 'increment_id']
            );

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header'    => __('ID'),
                'align'     => 'left',
                'index'     => 'id',
                'type'      => 'text',
            ]
        );

        $this->addColumn(
            'customer_id',
            [
                'header'    => __('Customer Id'),
                'align'     => 'left',
                'index'     => 'customer_id',
                'type'      => 'text',
            ]
        );

        $this->addColumn(
            'customer_email',
            [
                'header'    => __('Recipient Email'),
                'align'     => 'left',
                'index'     => 'customer_email',
                'type'      => 'text',
                'frame_callback'
                            => [$this, 'decorateCustomerEmail'],
            ]
        );

        $this->addColumn(
            'increment_id',
            [
                'header'    => __('Order ID'),
                'align'     => 'left',
                'index'     => 'increment_id',
                'type'      => 'text',
                'frame_callback'
                            => [$this, 'decorateIncrementId'],
            ]
        );

        $this->addColumn(
            'reminder',
            [
                'header'    => __('Reminder'),
                'align'     => 'left',
                'index'     => 'reminder',
                'options'   => $this->reminder->getReminderLabels(),
                'type'      => 'options',
            ]
        );

        if (!$this->_storeManager->isSingleStoreMode()) {
            $this->addColumn(
                'store_id',
                [
                    'header'        => __('Store View'),
                    'index'         => 'store_id',
                    'type'          => 'store',
                    'store_all'     => true,
                    'store_view'    => true,
                    'sortable'      => false,
                    'filter_condition_callback'
                                    => [$this, '_filterStoreCondition'],
                ]
            );
        }

        $this->addColumn(
            'subject',
            [
                'header'    => __('Email Subject'),
                'align'     => 'left',
                'index'     => 'subject',
                'type'      => 'text',
                'frame_callback'
                            => [$this, 'decorateSubject'],
            ]
        );

        $this->addColumn(
            'action',
            [
                'header'    => __('Email Body'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => [
                    [
                        'caption'   => __('View'),
                        'url'       => ['base'=>'*/*/viewbody'],
                        'field'     => 'id',
                        'target'    =>'_blank',
                    ]
                ],
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'id',
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header'    => __('Created At'),
                'align'     => 'left',
                'index'     => 'created_at',
                'type'      => 'datetime',
                'filter_condition_callback'
                                => [$this, '_filterCreatedAtCondition'],
            ]
        );

        $this->addColumn(
            'scheduled_at',
            [
                'header'    => __('Scheduled At'),
                'align'     => 'left',
                'index'     => 'scheduled_at',
                'type'      => 'date',
                'timezone'  => false,
            ]
        );

        $this->addColumn(
            'status',
            [
                'header'    => __('Status'),
                'index'     => 'status',
                'type'      => 'options',
                'options'   => $this->reminder->getStatuses(),
                'width'     => '6%',
                'frame_callback'
                            => [$this, 'decorateStatus'],
            ]
        );

        return parent::_prepareColumns();
    }

    public function decorateIncrementId($value, $row, $column, $isExport)
    {
        return '<a target="_blank" href="' .
            $this->getUrl('sales/order/view/', ['order_id' => $row->getOrderId()]) .
            '">#' . $row->getIncrementId() . '</a>';
    }

    public function decorateCustomerEmail($value, $row, $column, $isExport)
    {
        if ($row->getCustomerId()) {
            return '<a target="_blank" href="' .
                $this->getUrl('customer/index/edit/', ['id' => $row->getCustomerId()]) .
                '">' . $row->getCustomerEmail() . '</a>';
        }

        return $row->getCustomerEmail();
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (! $value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addFieldToFilter('main_table.store_id', $value);
    }

    protected function _filterCreatedAtCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addFieldToFilter('main_table.created_at', $value);
    }

    public function decorateSubject($value, $row, $column, $isExport)
    {
        if (!$row->getSubject()) {
            $this->_appState->emulateAreaCode('frontend', [$this, 'prepareEmailSubject'], [$row]);
            $subject = $this->emailSubject;
        } else {
            $subject = $row->getSubject();
        }

        return $subject;
    }

    public function prepareEmailSubject($row)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()
            ->load($row->getOrderId());

        $emailModel = $this->emailFactory->create()
            ->setReminder($row)
            ->setOrder($order);

        $items = $order->getAllVisibleItems();
        $items = array_values($items);
        $productName = '';

        if (isset($items[0])) {
            $productName = $items[0]->getName();
        }

        $expensive = 0;
        $expensiveProduct = '';
        foreach ($items as $item) {
            if ($item->getPrice() > $expensive) {
                $expensive = $item->getPrice();
                $expensiveProduct = $item->getName();
            }
        }

        $customerName = $order->getBillingAddress()
            ? $order->getBillingAddress()->getName()
            : $order->getCustomerName();

        $vars =  [
            'customerName' => $customerName,
            'mostExpensive' => $expensiveProduct,
            'productName' => $productName,
            'store' => $this->_storeManager->getStore(),
            ];
        $template = $emailModel->getEmailTemplate($order->getStoreId());

        $this->emailSubject = $template->getProcessedTemplateSubject($vars);
    }

    public function decorateStatus($value, $row, $column, $isExport)
    {
        switch ($row->getStatus()) {
            case Reminder::ADVANCEDRAR_STATUS_SENT:
                $cell = '<span class="grid-severity-notice"><span>'.$value.'</span></span>';
                break;
            case Reminder::ADVANCEDRAR_STATUS_PENDING:
                $cell = '<span class="grid-severity-notice pending"><span>'.$value.'</span></span>';
                break;
            case Reminder::ADVANCEDRAR_STATUS_CANCELED:
                $cell = '<span class="grid-severity-minor"><span>'.$value.'</span></span>';
                break;
            case Reminder::ADVANCEDRAR_STATUS_ERROR:
                $cell = '<span class="grid-severity-critical"><span>'.$value.'</span></span>';
                break;
            default:
                $cell = '<span><span>'.$value.'</span></span>';
                break;
        }

        return $cell;
    }

    /**
     * Disabled edit reminder, they generate automatically and don't have edit form
     *
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label'    => __('Delete'),
                'url'      => $this->getUrl('*/*/delete'),
                'confirm'  => __('Are you sure?'),
            ]
        );

        $this->getMassactionBlock()->addItem(
            'cancel',
            [
                'label'    => __('Cancel'),
                'url'      => $this->getUrl('*/*/cancel'),
                'confirm'  => __('Are you sure?'),
            ]
        );

        $this->getMassactionBlock()->addItem(
            'send',
            [
                'label'    => __('Send'),
                'url'      => $this->getUrl('*/*/send'),
                'confirm'  => __('Are you sure?'),
            ]
        );

        return $this;
    }
}
