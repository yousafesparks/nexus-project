<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Advancedrar;

class ReminderList extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_Advancedrar_ReminderList';
        $this->_blockGroup = 'Plumrocket_AdvancedReviewAndReminder';
        $this->_headerText = __('Manage Scheduled Reminders');
        parent::_construct();

        $this->removeButton('add');
    }
}
