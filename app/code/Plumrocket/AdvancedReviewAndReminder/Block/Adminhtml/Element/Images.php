<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Element;

use Magento\Framework\UrlInterface;

class Images extends \Magento\Framework\Data\Form\Element\Image
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface
     */
    private $reviewImageFactory;

    /**
     * Images constructor.
     *
     * @param \Magento\Framework\Data\Form\Element\Factory                            $factoryElement
     * @param \Magento\Framework\Data\Form\Element\CollectionFactory                  $factoryCollection
     * @param \Magento\Framework\Escaper                                              $escaper
     * @param \Magento\Framework\UrlInterface                                         $urlBuilder
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface $reviewImageFactory
     * @param array                                                                   $data
     */
    public function __construct(
        \Magento\Framework\Data\Form\Element\Factory $factoryElement,
        \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection,
        \Magento\Framework\Escaper $escaper,
        UrlInterface $urlBuilder,
        \Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface $reviewImageFactory,
        array $data = []
    ) {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $urlBuilder, $data);
        $this->reviewImageFactory = $reviewImageFactory;
    }

    /**
     * @return string
     */
    public function getElementHtml()
    {
        $html = '';
        $attachImages = $this->getValue();

        foreach ($attachImages as $id => $attachImage) {
            $this->setHtmlId($this->getHtmlId() . '_' . $id);
            $idField = "attach_images[$id]";
            $url = $this->reviewImageFactory->create($attachImage['filename'], 'origin')->getUrl();

            $this->setValue($url);
            $this->setData('name', $idField);

            if (! preg_match("/^http\:\/\/|https\:\/\//", $url)) {
                $url = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $url;
            }

            $html .= '<div class="prar-img-wrap"><a href="' .
                $url .
                '"' .
                ' onclick="imagePreview(\'' .
                $idField .
                '_image\'); return false;" ' .
                $this->_getUiId(
                    'link'
                ) .
                '>' .
                '<img src="' .
                $url .
                '" id="' .
                $idField .
                '_image" title="' .
                $this->getValue() .
                '"' .
                ' alt="' .
                $this->getValue() .
                '" height="100" class="small-image-preview v-middle" ' .
                $this->_getUiId() .
                ' />' .
                '</a>';

            $html .= $this->_getDeleteCheckbox() . '</div>';
        }
        return $html;
    }
}
