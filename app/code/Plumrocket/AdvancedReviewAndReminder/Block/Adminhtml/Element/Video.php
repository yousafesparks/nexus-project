<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Element;

class Video extends \Magento\Framework\Data\Form\Element\Link
{
    public function getElementHtml()
    {
        $url = $this->getValue() ? 'https://www.youtube.com/embed/' . $this->getValue() : '';
        $html = '<input name="video" placeholder="https://www.youtube.com/embed/"
         class="input-text input-text admin__control-text validate-url" value="' . $url . '" />';

        if ($this->getValue()) {
            $html .= '<iframe src="' . $url . '" frameborder="0" width="310" height="180"></iframe>';
        }

        return $html;
    }
}
