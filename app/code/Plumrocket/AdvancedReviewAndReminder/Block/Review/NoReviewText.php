<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Review;

use Magento\Framework\View\Element\Template;

/**
 * @since 2.0.0
 */
class NoReviewText extends Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface
     */
    private $getReviewCount;

    /**
     * HeaderLine constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                        $context
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface       $getReviewCount
     * @param array                                                                   $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface $getReviewCount,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->currentProductRetriever = $currentProductRetriever;
        $this->getReviewCount = $getReviewCount;
    }

    /**
     * @inheritDoc
     */
    protected function _toHtml()
    {
        if ($this->getReviewCount->execute((int) $this->currentProductRetriever->execute()->getId())) {
            return '';
        }

        return parent::_toHtml();
    }
}
