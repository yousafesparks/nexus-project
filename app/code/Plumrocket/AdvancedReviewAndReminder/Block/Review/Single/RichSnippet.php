<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Review\Single;

class RichSnippet extends \Magento\Framework\View\Element\Template
{
    const PRODUCT_NAME_MAX_LEN = 99;
    const PRODUCT_IMAGE_WIDTH = 250;
    const PRODUCT_IMAGE_HEIGHT = 250;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentReviewRetriever
     */
    private $currentReviewRetriever;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $productImageHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    private $advancedReviewRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Review
     */
    private $reviewHelper;

    /**
     * RichSnippet constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                            $context
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentReviewRetriever      $currentReviewRetriever
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever     $currentProductRetriever
     * @param \Magento\Catalog\Helper\Image                                               $productImageHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Review                         $reviewHelper
     * @param array                                                                       $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentReviewRetriever $currentReviewRetriever,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Magento\Catalog\Helper\Image $productImageHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Review $reviewHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->currentReviewRetriever = $currentReviewRetriever;
        $this->currentProductRetriever = $currentProductRetriever;
        $this->productImageHelper = $productImageHelper;
        $this->advancedReviewRepository = $advancedReviewRepository;
        $this->reviewHelper = $reviewHelper;
    }

    /**
     * @return bool
     */
    protected function canShow() : bool
    {
        return $this->currentReviewRetriever->execute()
            && $this->currentProductRetriever->execute()
            && $this->isEnabledRichSnippet();
    }

    /**
     * @return bool
     */
    public function isEnabledRichSnippet() : bool
    {
        return (bool) $this->getData('enabled_rich_snippet');
    }

    /**
     * @return array
     */
    public function createData() : array
    {
        $data = [];
        $product = $this->currentProductRetriever->execute();
        $review = $this->currentReviewRetriever->execute();

        if ($product && $review) {
            $advancedReview = $this->advancedReviewRepository->get($review->getId());
            if ($product->getName()) {
                $data['productName'] = $this->escapeHtml(
                    mb_substr($product->getName(), 0, self::PRODUCT_NAME_MAX_LEN, 'UTF-8')
                );
            }
            $data['productSku'] = $product->getSku();
            $data['productImage'] = $this->productImageHelper->init($product, 'product_page_image_small')
                ->constrainOnly(true)
                ->keepAspectRatio(true)
                ->keepTransparency(true)
                ->keepFrame(false)
                ->resize(self::PRODUCT_IMAGE_WIDTH, self::PRODUCT_IMAGE_HEIGHT)
                ->getUrl();

            $productRatingSummary = $this->reviewHelper->getProductRatingSummaryData($product);

            $data['aggregateRating'] = [
                'ratingValue' => $productRatingSummary['rating'],
                'bestRating' => '5',
                'ratingCount' => $productRatingSummary['count'],
            ];
            $data['reviewRating'] = $advancedReview->getAggregateRating();
            $data['reviewTitle'] = $advancedReview->getTitle();
            $data['reviewDetail'] = $advancedReview->getDetail();
            $data['reviewerName'] = $advancedReview->getNickname();
        }

        return $data;
    }
}
