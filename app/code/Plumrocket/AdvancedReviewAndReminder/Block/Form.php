<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block;

use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\ObjectManagerInterface;
use Plumrocket\AdvancedReviewAndReminder\Helper\Permission as PermissionHelper;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;
use Plumrocket\AdvancedReviewAndReminder\Ui\Frontend\DataProvider\Form\Modifier\MagentoReCaptcha as MagentoReCaptchaModifier;
use Plumrocket\Base\Api\ExtensionStatusInterface;
use Plumrocket\Base\Api\GetExtensionStatusInterface;

class Form extends \Magento\Review\Block\Form
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Permission
     */
    private $permissionHelper;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var null|array
     */
    private $jsComponentConfig;

    /**
     * @var null|array
     */
    private $jsComponentConfigCaptcha;

    /**
     * @var null|array
     */
    private $jsComponentSocialSocial;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface
     */
    private $allowedCustomerGroupsProvider;

    /**
     * @var false|\MSP\ReCaptcha\Model\LayoutSettings
     */
    private $mspLayoutSettings;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Provider\SocialButtons
     */
    private $socialButtonsProvider;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend
     */
    private $recommend;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    /**
     * @var null|array
     */
    private $jsComponentGDPR;

    /**
     * @var \Plumrocket\Base\Api\GetExtensionStatusInterface
     */
    private $getExtensionStatus;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Ui\Frontend\DataProvider\Form\Modifier\MagentoReCaptcha
     */
    private $magentoReCaptchaModifier;

    /**
     * @var \Plumrocket\Base\Api\ExtensionStatusInterface
     */
    private $extensionStatus;

    /**
     * Form constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                                             $dataHelper
     * @param PermissionHelper                                                                              $permissionHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                                           $configHelper
     * @param ImageHelper                                                                                   $imageHelper
     * @param CustomerSession                                                                               $customerSession
     * @param \Magento\Framework\View\Element\Template\Context                                              $context
     * @param \Magento\Framework\Url\EncoderInterface                                                       $urlEncoder
     * @param \Magento\Review\Helper\Data                                                                   $reviewData
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                                               $productRepository
     * @param \Magento\Review\Model\RatingFactory                                                           $ratingFactory
     * @param \Magento\Framework\Message\ManagerInterface                                                   $messageManager
     * @param \Magento\Framework\App\Http\Context                                                           $httpContext
     * @param \Magento\Customer\Model\Url                                                                   $customerUrl
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface              $allowedCustomerGroupsProvider
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider                    $independenceObjectProvider
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\SocialButtons                            $socialButtonsProvider
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend                           $recommend
     * @param \Magento\Framework\Serialize\SerializerInterface                                              $serializer
     * @param \Plumrocket\Base\Api\GetExtensionStatusInterface                                              $getExtensionStatus
     * @param \Magento\Framework\ObjectManagerInterface                                                     $objectManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Ui\Frontend\DataProvider\Form\Modifier\MagentoReCaptcha $magentoReCaptchaModifier
     * @param \Plumrocket\Base\Api\ExtensionStatusInterface                                                 $extensionStatus
     * @param array                                                                                         $data
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        ImageHelper $imageHelper,
        CustomerSession $customerSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Review\Helper\Data $reviewData,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Url $customerUrl,
        \Plumrocket\AdvancedReviewAndReminder\Api\AllowedCustomerGroupsProviderInterface $allowedCustomerGroupsProvider,
        \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider $independenceObjectProvider,
        \Plumrocket\AdvancedReviewAndReminder\Model\Provider\SocialButtons $socialButtonsProvider,
        \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        GetExtensionStatusInterface $getExtensionStatus,
        ObjectManagerInterface $objectManager,
        MagentoReCaptchaModifier $magentoReCaptchaModifier,
        ExtensionStatusInterface $extensionStatus,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        $this->permissionHelper = $permissionHelper;
        $this->imageHelper = $imageHelper;
        $this->customerSession = $customerSession;
        parent::__construct(
            $context,
            $urlEncoder,
            $reviewData,
            $productRepository,
            $ratingFactory,
            $messageManager,
            $httpContext,
            $customerUrl,
            $data
        );
        $this->configHelper = $configHelper;
        $this->allowedCustomerGroupsProvider = $allowedCustomerGroupsProvider;
        $this->mspLayoutSettings = $independenceObjectProvider->getLayoutSettings();
        $this->socialButtonsProvider = $socialButtonsProvider;
        $this->recommend = $recommend;
        $this->serializer = $serializer;
        $this->getExtensionStatus = $getExtensionStatus;
        $this->objectManager = $objectManager;
        $this->magentoReCaptchaModifier = $magentoReCaptchaModifier;
        $this->extensionStatus = $extensionStatus;
    }

    /**
     * @return false|string
     */
    public function getJsLayout()
    {
        if (! $this->configHelper->isModuleEnabled()) {
            return parent::getJsLayout();
        }

        $jsLayout = (array) $this->serializer->unserialize(parent::getJsLayout());

        if (isset($jsLayout['components']['review-form'])) {
            unset($jsLayout['components']['review-form']);
        }

        $components = [
            'pr-review-form'          => $this->getJsComponentConfig(),
            'pr-review-form-captcha'  => $this->getJsComponentCaptchaConfig(),
            'pr-review-form-social'   => $this->getJsComponentSocialConfig(),
        ];

        $jsComponentGDPRConfig = $this->getJsComponentGDPRConfig();
        if ($jsComponentGDPRConfig) {
            $components['pr-review-form-gdpr'] = $jsComponentGDPRConfig;
        }

        if (isset($jsLayout['components'])) {
            $jsLayout['components'] = array_replace_recursive(
                $jsLayout['components'],
                $components
            );
        } else {
            $jsLayout['components'] = $components;
        }

        $jsLayout = $this->magentoReCaptchaModifier->modify($jsLayout);

        return $this->serializer->serialize($jsLayout);
    }

    /**
     * @return array
     */
    public function getJsComponentConfig() : array
    {
        if (null === $this->jsComponentConfig) {
            $this->jsComponentConfig = [
                'component' => 'Plumrocket_AdvancedReviewAndReminder/js/view/review-form',
                'actionUrl' => $this->escapeUrl($this->getAction()),
                'product'   => [
                    'imagePath' => $this->getProductImage(),
                    'name' => $this->escapeHtml($this->getProductInfo()->getName()),
                    'url' => $this->escapeUrl($this->getProductInfo()->getProductUrl()),
                ],
                'rating'   => [
                    'count' => $this->getRatings()->getSize(),
                    'items' => $this->getRatingsData(),
                    'image' => $this->getViewFileUrl('Plumrocket_AdvancedReviewAndReminder::images')
                ],
                'customerGroup' => $this->customerSession->getCustomerGroupId(),
                'configuration' => [
                    'summaryOption' => $this->configHelper->getReviewSummaryOption(),
                    'summaryMaxLength' => $this->configHelper->getReviewSummaryMaxLength(),
                    'permission' => [
                        'guest'    => $this->configHelper->getGuestPermission(),
                        'customer' => $this->configHelper->getCustomerPermission(),
                        'isAllowAllGroups' => $this->allowedCustomerGroupsProvider->isAllGroupsAllowed(),
                        'customerGroups' => $this->configHelper->getCustomerGroupsPermission(),
                        'permissionUrl' => $this->getUrl(
                            'advancedrar/permission/write',
                            ['product' => $this->getProductInfo()->getId()]
                        ),
                        'reasons' => $this->permissionHelper->getReasonHtml(PermissionHelper::ALL_REASONS),
                    ],
                    'prosAndCons' => $this->getProsAndConsOptions(),
                    'recommend' => [
                        'enabled' => $this->configHelper->isEnabledRecommendedToAFriend(),
                        'options' => $this->recommend ->toOptionArray(),
                    ],
                    'videoLink' => $this->configHelper->isEnabledVideoLink(),
                    'uploadImg' => [
                        'enabled' => $this->configHelper->isEnabledImageUpload(),
                        'actionUpload' => $this->getUploadImageAction(),
                        'allowedExtension' => $this->configHelper->getAllowedExtension(),
                        'maxFileSize' => $this->configHelper->getMaxImageSize(true),
                        'maxFileSizeMb' => $this->configHelper->getMaxImageSize(),
                    ],
                    'reCaptcha' => [
                        'enabled' => $this->configHelper->isEnabledRecaptcha(),
                        'settings' => $this->mspLayoutSettings ? $this->mspLayoutSettings->getCaptchaSettings() : [],
                    ],
                    'nativeCaptcha' => [
                        'enabled' => $this->configHelper->isEnabledNativeCaptcha(),
                    ],
                ],
                'refererQueryParamName' => \Magento\Customer\Model\Url::REFERER_QUERY_PARAM_NAME,
                'isPopupLoginEnabled' => $this->extensionStatus->isEnabled('Popuplogin'),
            ];

            $this->jsComponentConfig['configuration']['reCaptcha']['settings']['badge'] = 'inline';
        }

        return $this->jsComponentConfig;
    }

    /**
     * @return array
     */
    public function getJsComponentCaptchaConfig() : array
    {
        if (null === $this->jsComponentConfigCaptcha) {
            $this->jsComponentConfigCaptcha = [
                'component' => 'Plumrocket_AdvancedReviewAndReminder/js/model/reviewCaptcha',
                'formId' => \Plumrocket\AdvancedReviewAndReminder\Helper\Config::PR_ARAR_FORM_CAPTCHA,
                'configSource' => 'checkout',
            ];
        }

        return $this->jsComponentConfigCaptcha;
    }

    /**
     * @return array
     */
    public function getJsComponentSocialConfig() : array
    {
        if (null === $this->jsComponentSocialSocial) {
            /** Change an associative array into an indexed array because index array simply use in knockout */
            $socialButtonsList = array_values($this->socialButtonsProvider->getList());

            $this->jsComponentSocialSocial = [
                'component' => 'Plumrocket_AdvancedReviewAndReminder/js/view/social-buttons',
                'canShow' => ! empty($socialButtonsList),
                'buttons' => $socialButtonsList,
                'showFullButtons' => true,
            ];
        }

        return $this->jsComponentSocialSocial;
    }

    /**
     * @return array|null
     */
    public function getJsComponentGDPRConfig()
    {
        if ($this->getExtensionStatus->execute('GDPR') === GetExtensionStatusInterface::MODULE_STATUS_ENABLED) {
            /** @var \Plumrocket\GDPR\Helper\Checkboxes $helperCheckbox */
            $helperCheckbox = $this->objectManager->get(\Plumrocket\GDPR\Helper\Checkboxes::class);
            $location = 'pr_review';
            $this->jsComponentGDPR = [
                'component' => 'Plumrocket_GDPR/js/view/consent-checkbox',
                'locationKey' => $location,
                'checkboxes' => $helperCheckbox->serialize($helperCheckbox->getCheckboxes($location, false)),
            ];
        }

        return $this->jsComponentGDPR;
    }

    /**
     * @return string
     */
    public function getAction() : string
    {
        if (! $this->dataHelper->moduleEnabled()) {
            return parent::getAction();
        }

        return $this->getUrl('advancedrar/review/post', ['id' => $this->getProductInfo()->getId()]);
    }

    /**
     * @return string
     */
    private function getProductImage() : string
    {
        return $this->imageHelper->init($this->getProductInfo(), 'product_small_image')
            ->resize(126, 126)
            ->getUrl();
    }

    /**
     * @return array
     */
    private function getRatingsData() : array
    {
        $ratingItemsData = [];

        /** @var \Magento\Review\Model\Rating $rating */
        foreach ($this->getRatings() as $rating) {
            $ratingItemsData[] = [
                'ratingId' => $rating->getId(),
                'ratingCode' => $this->escapeHtml($rating->getRatingCode()),
                'ratingOptions' => $this->getOptionsValueString($rating),
            ];
        }

        return $ratingItemsData;
    }

    /**
     * @param \Magento\Review\Model\Rating $rating
     * @return string
     */
    private function getOptionsValueString($rating) : string
    {
        $options = [];
        /** @var \Magento\Review\Model\Rating\Option $_option */
        foreach ($rating->getOptions() as $_option) {
            $options[] = $_option->getId();
        }

        return implode(',', $options);
    }

    /**
     * @return array
     */
    private function getProsAndConsOptions() : array
    {
        return [
            'isDisabled' => Option::NO       === $this->configHelper->getProsAndConsOption(),
            'isRequired' => Option::REQUIRED === $this->configHelper->getProsAndConsOption(),
            'isEnabled'  => Option::NO       !== $this->configHelper->getProsAndConsOption(),
        ];
    }

    /**
     * @return string
     */
    public function getUploadImageAction() : string
    {
        return $this->getUrl('advancedrar/review/upload', ['_current' => true]);
    }

    /**
     * @return bool
     */
    public function isOpen() : bool
    {
        $config = $this->serializer->unserialize($this->getJsLayout());
        return (bool) ($config['components']['pr-review-form']['configuration']['open'] ?? false);
    }
}
