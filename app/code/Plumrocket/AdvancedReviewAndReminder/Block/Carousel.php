<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block;

use Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface as ReviewImageFactory;

class Carousel extends \Magento\Framework\View\Element\Template
{
    const COUNT_ITEMS = 10;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    private $advancedReviewRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface
     */
    private $reviewMultipleImagesFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var int $imgCount
     */
    private $imgCount;
    /**
     * Carousel constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     *
     * @param \Magento\Framework\View\Element\Template\Context                                 $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface      $advancedReviewRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever          $currentProductRetriever
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                                     $searchCriteriaBuilder
     * @param \Magento\Framework\Api\SortOrderBuilder                                          $sortOrderBuilder
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface $reviewMultipleImagesFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                              $configHelper
     * @param array                                                                            $data
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder,
        \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface $reviewMultipleImagesFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->advancedReviewRepository = $advancedReviewRepository;
        $this->currentProductRetriever = $currentProductRetriever;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->reviewMultipleImagesFactory = $reviewMultipleImagesFactory;
        $this->configHelper = $configHelper;
    }

    /**
     * @return array[]
     */
    public function getCustomerImages() : array
    {
        $product =  $this->currentProductRetriever->execute();
        if (! $product) {
            return [];
        }

        $attach = [];
        $countImg = 0;
        foreach ($this->getAllReviews($product) as $advancedReview) {
            $images = $this->reviewMultipleImagesFactory->create(
                $advancedReview,
                ReviewImageFactory::THUMBNAIL_IMAGE_ID
            );
            foreach ($images as $orderNumber => $image) {
                if ($countImg < self::COUNT_ITEMS) {
                    $attach[] = [
                        'reviewId' => $advancedReview->getId(),
                        'url' => $image->getUrl(),
                        'orderNumber' => $orderNumber,
                    ];
                }
                $countImg++;
            }
        }
        $this->setAllCustomerImages($countImg);
        return $attach;
    }

    /**
     * @return bool
     */
    public function showImageGallery() : bool
    {
        return $this->configHelper->showImageGallery();
    }

    /**
     * @return int
     */
    public function getAllCustomerImages() : int
    {
        return $this->imgCount;
    }

    /**
     * @param int $imgCount
     * @return $this
     */
    public function setAllCustomerImages(int $imgCount)
    {
        $this->imgCount = $imgCount;
        return $this;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]|\Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface[]
     */
    public function getAllReviews(\Magento\Catalog\Model\Product $product) : array
    {
        $this->searchCriteriaBuilder->addFilter('entity_pk_value', $product->getId());
        $this->searchCriteriaBuilder->addFilter('attach_image', true, 'notnull');
        $this->searchCriteriaBuilder->addFilter('status_id', \Magento\Review\Model\Review::STATUS_APPROVED);
        $sortOrder = $this->sortOrderBuilder
            ->setField('helpful_subtracted')
            ->setDescendingDirection()
            ->create();
        $this->searchCriteriaBuilder->addSortOrder($sortOrder);
        $sortOrder = $this->sortOrderBuilder
            ->setField('created_at')
            ->setDescendingDirection()
            ->create();
        $this->searchCriteriaBuilder->addSortOrder($sortOrder);

        $reviewSearchResults = $this->advancedReviewRepository->getList(
            $this->searchCriteriaBuilder->create(),
            0,
            true
        );

        return $reviewSearchResults->getItems();
    }
}
