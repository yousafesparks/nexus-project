<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block;

/**
 * @method $this     setIsProductPage(bool $true)
 * @method bool|null getIsProductPage()
 */
class Helper extends \Magento\Review\Block\Product\ReviewRenderer
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * Helper constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data   $dataHelper
     * @param \Magento\Framework\View\Element\Template\Context    $context
     * @param \Magento\Review\Model\ReviewFactory                 $reviewFactory
     * @param array                                               $data
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        parent::__construct(
            $context,
            $reviewFactory,
            $data
        );
    }

    public function _construct()
    {
        if ($this->dataHelper->moduleEnabled()) {
            $this->_availableTemplates = [
                self::FULL_VIEW  => 'Plumrocket_AdvancedReviewAndReminder::advancedrar/helper/summary.phtml',
                self::SHORT_VIEW => 'Magento_Review::helper/summary_short.phtml',
            ];
        }
    }

    /**
     * @param bool $useDirectLink
     * @return string
     */
    public function getReviewsUrl($useDirectLink = false)
    {
        if (! $this->dataHelper->moduleEnabled()) {
            return parent::getReviewsUrl($useDirectLink);
        }

        $module = $this->getRequest()->getModuleName();
        $fullActionName = $this->getRequest()->getFullActionName();

        if ('catalog' === $module && 'catalog_product_view' === $fullActionName) {
            $this->setIsProductPage(true);
            return '';
        }

        return parent::getReviewsUrl($useDirectLink);
    }

    /**
     * @return array[]
     */
    public function getJsComponentConfig()
    {
        $product = $this->getProduct();
        return [
            'component' => 'Plumrocket_AdvancedReviewAndReminder/js/view/review-summary',
            'review' => [
                'count' => $this->getReviewsCount(),
                'displayIfEmpty' => $this->getDisplayIfEmpty()
            ],
            'rating' => is_object($product->getRatingSummary())
                ? (int) $product->getRatingSummary()->getRatingSummary()
                : (int) $product->getRatingSummary(),
            'reviewsUrl' => $this->getReviewsUrl(),
            'isProductPage' => $this->getIsProductPage(),
        ];
    }
}
