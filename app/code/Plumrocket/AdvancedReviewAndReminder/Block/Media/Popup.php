<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Media;

use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Yesnorequired as Option;

/**
 * @since 2.2.0
 * @method getShowTitleAsLink()
 */
class Popup extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * Popup constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context    $context
     * @param \Magento\Framework\Serialize\SerializerInterface    $serializer
     * @param \Magento\Framework\Registry                         $coreRegistry
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
     * @param array                                               $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Framework\Registry $coreRegistry,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->serializer = $serializer;
        $this->coreRegistry = $coreRegistry;
        $this->configHelper = $configHelper;
    }

    /**
     * @return false|string
     */
    public function getJsLayout()
    {
        $layout = (array) $this->serializer->unserialize(parent::getJsLayout());
        $layout['components']['pr-review-popup'] = $this->getJsComponentConfig();
        return $this->serializer->serialize($layout);
    }

    /**
     * @return array
     */
    public function getJsComponentConfig() : array
    {
        if ($this->getRequest()->getFullActionName() === 'advancedrar_review_view') {
            $isShowTitleAsLink = false;
        } elseif (null === $this->getShowTitleAsLink()) {
            $isShowTitleAsLink = true;
        } else {
            $isShowTitleAsLink = $this->getShowTitleAsLink();
        }

        return [
            'component' => 'Plumrocket_AdvancedReviewAndReminder/js/view/review-media-popup',
            'configuration' => [
                'showTitleAsLink' => $isShowTitleAsLink,
                'sourceUrl' => $this->getUrl(
                    'advancedrar/review_media/list',
                    ['product' => $this->getProductInfo()->getId()]
                ),
                'productName' => $this->getProductInfo()->getName(),
                'enabledProsAndCons' => $this->configHelper->getProsAndConsOption() !== Option::NO,
                'enableRecommend' => $this->configHelper->isEnabledRecommendedToAFriend(),
            ]
        ];
    }

    /**
     * Retrieve currently viewed product object
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProductInfo() : \Magento\Catalog\Api\Data\ProductInterface
    {
        if (! $this->hasData('product')) {
            $this->setData('product', $this->coreRegistry->registry('product'));
        }
        return $this->getData('product');
    }
}
