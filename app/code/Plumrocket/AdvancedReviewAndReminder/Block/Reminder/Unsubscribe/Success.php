<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Reminder\Unsubscribe;

use Magento\Framework\View\Element\Template;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\InputType;

class Success extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface
     */
    protected $reasonRepository;

    /**
     * Success constructor.
     *
     * @param Template\Context $context
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->reasonRepository = $reasonRepository;
    }

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getActiveReasons()
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(UnsubscribeReasonInterface::STATUS, 1)
            ->create();

        return $this->reasonRepository->getList($searchCriteria)->getItems();
    }

    /**
     * @param UnsubscribeReasonInterface $reason
     * @return bool
     */
    public function isUseTextarea(UnsubscribeReasonInterface $reason): bool
    {
        return $reason->getInputType() === InputType::TEXTAREA;
    }

    /**
     * @return string
     */
    public function getSubmitReasonUrl()
    {
        return $this->_urlBuilder->getUrl('advancedrar/reminder/reason');
    }
}
