<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block;

class Config extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * Config constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config   $configHelper
     * @param \Magento\Framework\View\Element\Template\Context      $context
     * @param array                                                 $data
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->configHelper = $configHelper;
        parent::__construct($context, $data);
    }

    public function getReviewTabButtonSelector()
    {
        return $this->configHelper->getReviewTabButtonSelector();
    }
}
