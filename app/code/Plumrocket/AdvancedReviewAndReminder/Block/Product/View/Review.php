<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Product\View;

class Review extends \Magento\Review\Block\Product\Review
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Review
     */
    private $reviewHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface
     */
    private $getReviewCount;

    /**
     * ReviewList constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                  $context
     * @param \Magento\Framework\Registry                                       $registry
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory      $collectionFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Review               $reviewHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface $getReviewCount
     * @param array                                                             $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $collectionFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Review $reviewHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface $getReviewCount,
        array $data = []
    ) {
        parent::__construct($context, $registry, $collectionFactory, $data);
        $this->reviewHelper = $reviewHelper;
        $this->getReviewCount = $getReviewCount;
    }

    /**
     * @return array
     */
    public function getProductRatingSummaryData() : array
    {
        $result = ['count' => 0, 'rating' => 0, 'name' => ''];

        if ($this->getReviewCount->execute((int) $this->getProductId())) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->_coreRegistry->registry('product');
            $result = $this->reviewHelper->getProductRatingSummaryData($product);
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isEnabledRichSnippet() : bool
    {
        return (bool) $this->getData('enabled_rich_snippet');
    }

    /**
     * @inheritDoc
     */
    protected function _prepareLayout()
    {
        $pageTitleBlock = $this->getLayout()->getBlock('page.main.title');

        if ($pageTitleBlock && ! (string) $pageTitleBlock->getPageTitle()) {
            $pageTitleBlock->setPageTitle(__('Customer Ratings & Reviews'));
        }

        return parent::_prepareLayout();
    }
}
