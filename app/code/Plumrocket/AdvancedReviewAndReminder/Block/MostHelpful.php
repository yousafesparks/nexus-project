<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block;

use Plumrocket\AdvancedReviewAndReminder\Helper\Config as ConfigHelper;

class MostHelpful extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface|null|false
     */
    private $positiveReview;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface|null|false
     */
    private $criticalReview;

    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface
     */
    private $getReviewSeoFriendlyUrl;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Review
     */
    private $reviewHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface
     */
    private $getReviewCount;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    private $advancedReviewRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever
     */
    private $currentProductRetriever;

    /**
     * MostHelpful constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                            $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                         $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Review                         $reviewHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface  $getReviewSeoFriendlyUrl
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface           $getReviewCount
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                                $searchCriteriaBuilder
     * @param \Magento\Framework\Api\SortOrderBuilder                                     $sortOrderBuilder
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever     $currentProductRetriever
     * @param array                                                                       $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ConfigHelper $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Review $reviewHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface $getReviewSeoFriendlyUrl,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface $getReviewCount,
        \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\CurrentProductRetriever $currentProductRetriever,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->configHelper = $configHelper;
        $this->getReviewSeoFriendlyUrl = $getReviewSeoFriendlyUrl;
        $this->reviewHelper = $reviewHelper;
        $this->getReviewCount = $getReviewCount;
        $this->advancedReviewRepository = $advancedReviewRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->currentProductRetriever = $currentProductRetriever;
    }

    /**
     * @return bool
     */
    public function canShowBlock() : bool
    {
        if (! $this->configHelper->isEnableMostHelpfulBlock() || ! $this->currentProductRetriever->execute()) {
            return false;
        }

        $reviewCount = $this->getReviewCount->execute((int) $this->currentProductRetriever->execute()->getId());
        return $reviewCount >= $this->configHelper->getMostHelpfulBlockMinReviewCount();
    }

    /**
     * @return mixed
     */
    public function getPositiveReview()
    {
        if (null === $this->positiveReview) {
            $this->positiveReview = $this->getMostReview('positive');
        }

        return $this->positiveReview;
    }

    /**
     * @return mixed
     */
    public function getCriticalReview()
    {
        if (null === $this->criticalReview) {
            $this->criticalReview = $this->getMostReview('critical');
        }

        return $this->criticalReview;
    }

    /**
     * @param \Magento\Review\Model\Review $review
     * @return string
     */
    public function getReviewUrl(\Magento\Review\Model\Review $review) : string
    {
        return $this->getReviewSeoFriendlyUrl->execute($review);
    }

    /**
     * @param \Magento\Review\Model\Review $review
     * @return string
     */
    public function getFormattedDate(\Magento\Review\Model\Review $review) : string
    {
        return $this->reviewHelper->getFormattedDate($review->getCreatedAt());
    }

    /**
     * @param string $type
     * @return bool|\Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface
     */
    private function getMostReview($type)
    {
        $review = false;

        $this->searchCriteriaBuilder->addFilter(
            'entity_pk_value',
            $this->currentProductRetriever->execute()->getId()
        );

        $this->searchCriteriaBuilder->addFilter(
            'status_id',
            \Magento\Review\Model\Review::STATUS_APPROVED
        );

        $this->searchCriteriaBuilder->addFilter(
            'percent',
            ConfigHelper::POSITIVE_RATING_BOUND,
            $type === 'positive' ? 'gteq' : 'lt'
        );

        $sortOrder = $this->sortOrderBuilder
            ->setField('helpful_subtracted')
            ->setDescendingDirection()
            ->create();

        $this->searchCriteriaBuilder->addSortOrder($sortOrder);
        $this->searchCriteriaBuilder->setPageSize(1);
        $searchCriteria = $this->searchCriteriaBuilder->create();

        $reviewSearchResults = $this->advancedReviewRepository->getList($searchCriteria, 0, true);

        if ($reviewSearchResults->getTotalCount()) {
            $items = $reviewSearchResults->getItems();
            $review = reset($items);
            $review = ($review->getHelpfulTotal() >= $this->configHelper->getMostHelpfulBlockMinHelpfulVotes())
                ? $review
                : false;
        }

        return $review;
    }

    /**
     * @return array
     */
    public function getCacheKeyInfo() : array
    {
        return array_merge(
            parent::getCacheKeyInfo(),
            ['productid' => $this->currentProductRetriever->execute()->getId()]
        );
    }

    /**
     * @since 2.0.2
     * @param string $text
     * @return string
     */
    public function formatText(string $text) : string
    {
        return $this->configHelper->isAllowedFormattedTextOnFrontend() ? nl2br($text) : $text;
    }
}
