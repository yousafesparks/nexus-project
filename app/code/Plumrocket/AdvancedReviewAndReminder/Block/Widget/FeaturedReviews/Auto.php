<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Widget\FeaturedReviews;

use Plumrocket\AdvancedReviewAndReminder\Block\Widget\AbstractWidget;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Math\Random;
use Magento\Framework\View\Element\Template\Context;
use Magento\Review\Model\Review;
use Magento\Review\Model\ReviewFactory;
use Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo;
use Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Widget\Helper\Conditions;
use Magento\CatalogWidget\Model\Rule;
use Magento\Rule\Model\Condition\Sql\Builder;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\Product\Visibility;

/**
 * @since 2.2.0
 */
class Auto extends AbstractWidget
{
    const DEFAULT_MAX_REVIEW_PAGE_SIZE = 20;

    /**
     * @var ConvertJsSearchCriteria
     */
    private $convertJsSearchCriteria;

    /**
     * @var Resolver
     */
    private $layerResolver;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManagerInterface;

    /**
     * @var Conditions
     */
    private $conditionsHelper;

    /**
     * @var Rule
     */
    private $rule;

    /**
     * @var Builder
     */
    private $sqlBuilder;

    /**
     * Product collection factory
     *
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * Catalog product visibility
     *
     * @var Visibility
     */
    private $catalogProductVisibility;

    /**
     * Auto constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                               $context
     * @param \Magento\Catalog\Helper\Image                                                  $imageHelper
     * @param \Magento\Catalog\Model\ProductRepository                                       $productRepository
     * @param \Magento\Review\Model\ReviewFactory                                            $reviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface     $getReviewSeoFriendlyUrl
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo              $customerLogoProvider
     * @param \Magento\Framework\Math\Random                                                 $random
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface    $advancedReviewRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                                   $searchCriteriaBuilder
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria $convertJsSearchCriteria
     * @param \Magento\Catalog\Model\Layer\Resolver                                          $layerResolver
     * @param \Magento\Catalog\Model\CategoryRepository                                      $categoryRepository
     * @param \Magento\Store\Model\StoreManagerInterface                                     $storeManagerInterface
     * @param \Magento\Widget\Helper\Conditions                                              $conditionsHelper
     * @param \Magento\CatalogWidget\Model\Rule                                              $rule
     * @param \Magento\Rule\Model\Condition\Sql\Builder                                      $sqlBuilder
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory                 $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility                                      $catalogProductVisibility
     * @param array                                                                          $data
     */
    public function __construct(
        Context $context,
        Image $imageHelper,
        ProductRepository $productRepository,
        ReviewFactory $reviewFactory,
        GetReviewSeoFriendlyUrlInterface $getReviewSeoFriendlyUrl,
        CustomerLogo $customerLogoProvider,
        Random $random,
        AdvancedReviewRepositoryInterface $advancedReviewRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ConvertJsSearchCriteria $convertJsSearchCriteria,
        Resolver $layerResolver,
        CategoryRepository $categoryRepository,
        StoreManagerInterface $storeManagerInterface,
        Conditions $conditionsHelper,
        Rule $rule,
        Builder $sqlBuilder,
        CollectionFactory $productCollectionFactory,
        Visibility $catalogProductVisibility,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $imageHelper,
            $productRepository,
            $reviewFactory,
            $getReviewSeoFriendlyUrl,
            $customerLogoProvider,
            $random,
            $advancedReviewRepository,
            $searchCriteriaBuilder,
            $data
        );
        $this->convertJsSearchCriteria = $convertJsSearchCriteria;
        $this->layerResolver = $layerResolver;
        $this->categoryRepository = $categoryRepository;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->conditionsHelper = $conditionsHelper;
        $this->rule = $rule;
        $this->sqlBuilder = $sqlBuilder;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
    }

    /**
     * @return ExtensibleDataInterface[]|AdvancedReviewInterface[]|null
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getReviews()
    {
        $this->searchCriteriaBuilder->addFilter(
            'status_id',
            Review::STATUS_APPROVED
        );

        $widgetOptions = $this->getData();

        if ($widgetOptions['from_current_category']) {
            $categoryId =  $this->getCurrentCategoryId();
            if (! $categoryId) {
                return null;
            }

            $this->searchCriteriaBuilder = $this->filterCurrentCategoryReviews(
                $this->searchCriteriaBuilder,
                $categoryId
            );
        }

        $filters = [
            'rating' => [
                $widgetOptions['rating'] => $widgetOptions['rating'],
            ],
            'general' => [],
            'search' => []
        ];

        if (! empty($widgetOptions['verified'])) {
            $filters['general']['verified'] = $widgetOptions['verified'];
        }

        if (! empty($widgetOptions['with_media'])) {
            $filters['general']['with_media'] = $widgetOptions['with_media'];
        };

        $this->searchCriteriaBuilder = $this->convertJsSearchCriteria->applyFilters(
            $this->searchCriteriaBuilder,
            $filters
        );

        if (! empty($widgetOptions['min_number_helpful_vote'])) {
            $this->searchCriteriaBuilder->addFilter(
                'most_helpful',
                (int) $widgetOptions['min_number_helpful_vote']
            );
        }

        $productIds = $this->getProductIdsByConditions();
        if (false !== $productIds) {
            $this->searchCriteriaBuilder->addFilter('entity_pk_value', $productIds, 'in');
        }

        $sortOrderField = $widgetOptions['sort_by'];
        $this->searchCriteriaBuilder = $this->convertJsSearchCriteria->applySort(
            $this->searchCriteriaBuilder,
            $sortOrderField
        );

        $pageSize = ! empty($widgetOptions['max_review_count'])
            ? $widgetOptions['max_review_count']
            : self::DEFAULT_MAX_REVIEW_PAGE_SIZE;

        $this->searchCriteriaBuilder->setPageSize($pageSize);

        /** @var SearchCriteriaBuilder $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder->create();

        $reviewsList = $this->advancedReviewRepository->getList($searchCriteria);

        return $reviewsList->getItems();
    }

    /**
     * @return array|bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getProductIdsByConditions()
    {
        $conditions = $this->getData('conditions_encoded');

        if ($conditions) {
            $conditions = $this->conditionsHelper->decode($conditions);

            $this->rule->loadPost(['conditions' => $conditions]);
            $conditions = $this->rule->getConditions();
            if ($conditions->getConditions()) {
                /** @var $collection Collection */
                $collection = $this->productCollectionFactory->create();

                $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
                $conditions->collectValidatedAttributes($collection);

                $this->sqlBuilder->attachConditionToCollection($collection, $conditions);

                return $collection->getAllIds();
            }
        }

        return false;
    }

    /**
     * @return bool|int|null
     * @throws NoSuchEntityException
     */
    private function getCurrentCategoryId()
    {
        $rootCategoryId = (int) $this->storeManagerInterface->getStore()->getRootCategoryId();
        $currentCategoryId = (int) $this->layerResolver->get()->getCurrentCategory()->getId();

        return $currentCategoryId === $rootCategoryId ? false : $currentCategoryId;
    }

    /**
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param int                   $categoryId
     * @return SearchCriteriaBuilder
     */
    private function filterCurrentCategoryReviews(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        int $categoryId
    ): SearchCriteriaBuilder {
        $category = $this->categoryRepository->get($categoryId);
        $categoryProductCollection = $category->getProductCollection();

        if ($categoryProductCollection->getSize()) {

            $productIds = $categoryProductCollection->getAllIds();

            $searchCriteriaBuilder->addFilter(
                'entity_pk_value',
                $productIds,
                'in'
            );
        }

        return $searchCriteriaBuilder;
    }
}
