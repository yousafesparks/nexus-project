<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Widget\FeaturedReviews;

use Plumrocket\AdvancedReviewAndReminder\Block\Widget\AbstractWidget;
use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface;
use Magento\Review\Model\ReviewFactory;

/**
 * @since 2.2.0
 */
class Manual extends AbstractWidget
{
    /**
     * @return ExtensibleDataInterface[]|AdvancedReviewInterface[]
     */
    public function getReviews()
    {
        if ($this->getData('reviews_ids') !== null) {
            $reviewsIds = explode(',', $this->getData('reviews_ids'));

            /** @var SearchCriteriaBuilder $searchCriteria */
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('main_table.review_id', $reviewsIds, 'in')
                ->create();

            $reviewsList = $this->advancedReviewRepository->getList($searchCriteria);

            return $reviewsList->getItems();
        }

        return null;
    }
}
