<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Review\Model\ReviewFactory;
use Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo;
use Magento\Framework\Math\Random;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\AdvancedReviewInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * @since 2.2.0
 */
abstract class AbstractWidget extends Template implements BlockInterface
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    private $reviewFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface
     */
    private $getReviewSeoFriendlyUrl;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo
     */
    private $customerLogoProvider;

    /**
     * @var \Magento\Framework\Math\Random
     */
    private $random;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    protected $advancedReviewRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * AbstractWidget constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                            $context
     * @param \Magento\Catalog\Helper\Image                                               $imageHelper
     * @param \Magento\Catalog\Model\ProductRepository                                    $productRepository
     * @param \Magento\Review\Model\ReviewFactory                                         $reviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewSeoFriendlyUrlInterface  $getReviewSeoFriendlyUrl
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo           $customerLogoProvider
     * @param \Magento\Framework\Math\Random                                              $random
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                                $searchCriteriaBuilder
     * @param array                                                                       $data
     */
    public function __construct(
        Template\Context $context,
        Image $imageHelper,
        ProductRepository $productRepository,
        ReviewFactory $reviewFactory,
        GetReviewSeoFriendlyUrlInterface $getReviewSeoFriendlyUrl,
        CustomerLogo $customerLogoProvider,
        Random $random,
        AdvancedReviewRepositoryInterface $advancedReviewRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->imageHelper = $imageHelper;
        $this->productRepository = $productRepository;
        $this->reviewFactory = $reviewFactory;
        $this->getReviewSeoFriendlyUrl = $getReviewSeoFriendlyUrl;
        $this->customerLogoProvider = $customerLogoProvider;
        $this->random = $random;
        $this->advancedReviewRepository = $advancedReviewRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return mixed
     */
    abstract public function getReviews();

    /**
     * @return string
     */
    public function getTitle(): string
    {
        if (! $this->title) {
            $this->title = '';
            if ($this->getData('title') !== null) {
                $this->title = $this->getData('title');
            }
        }

        return $this->title;
    }

    /**
     * @param Product  $product
     * @param int      $width
     * @param int|null $height
     * @return string
     */
    public function getProductImageUrl(Product $product, int $width, int $height = null): string
    {
        return $this->imageHelper
            ->init($product, 'product_base_image')
            ->resize($width, $height)
            ->getUrl();
    }

    /**
     * @param int $productId
     * @return ProductInterface|mixed|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProduct(int $productId): ProductInterface
    {
        return $this->productRepository->getById($productId);
    }

    /**
     * @param int $reviewId
     * @return string
     */
    public function getReviewUrl(int $reviewId): string
    {
        /**
         * The getReviewSeoFriendlyUrl require \Magento\Review\Model\Review model,
         * therefore we cannot use \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
         */
        /** @var \Magento\Review\Model\Review $review */
        $review = $this->reviewFactory->create()->load($reviewId);
        return $this->getReviewSeoFriendlyUrl->execute($review);
    }

    /**
     * @param int $customerId
     * @return string
     */
    public function getCustomerLogo(int $customerId): string
    {
        return $this->customerLogoProvider->getLogoUrl($customerId);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getOwlCarouselClass(): string
    {
        return $this->random->getRandomString(10);
    }

    /**
     * @param AdvancedReviewInterface $review
     * @return string
     */
    public function decorateReviewDetail(AdvancedReviewInterface $review): string
    {
        $detail = $review->getDetail();
        $reviewUrl = $this->getReviewUrl((int) $review->getId());

        if (strlen($detail) > 75) {
            $detail = mb_substr($detail, 0, 75);
            $endPoint = mb_strrpos($detail, ' ');
            $detail = mb_substr($detail, 0, $endPoint);
            $detail .= ' ... <a href="' . $reviewUrl .'">' . __('read more') . '</a>';
        }

        return $this->escapeHtml($detail, ['a']);
    }

    /**
     * @return int
     */
    public function getCountPerSlide(): int
    {
        $countPerSlide = (int) $this->getData('count_per_slide');

        return $countPerSlide === 4 ? 2 : $countPerSlide;
    }
}
