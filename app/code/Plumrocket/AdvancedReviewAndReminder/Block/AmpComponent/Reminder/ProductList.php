<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\AmpComponent\Reminder;

use Magento\Catalog\Helper\Image;
use Magento\Framework\View\Element\BlockInterface;
use Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts;

/**
 * Class ProductList
 * @since 1.2.0
 */
class ProductList extends \Plumrocket\AmpEmailApi\Block\AbstractProductComponent
{
    /**
     * @var string
     */
    protected $styleFileId = 'Plumrocket_AdvancedReviewAndReminder::css/component/:version/reminder/product-list.css';

    /**
     * @var \Magento\Framework\View\ConfigInterface
     */
    private $viewConfig;

    /**
     * @var \Magento\Framework\Config\View
     */
    private $configView;

    /**
     * TODO: replace with ImageFactory after left support magento v2.2
     *
     * @var \Magento\Catalog\Block\Product\ImageBuilder
     */
    private $imageBuilder;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts
     */
    private $formScripts;

    /**
     * ProductList constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                                 $context
     * @param \Magento\Framework\Url                                                           $frontUrlBuilder
     * @param \Plumrocket\AmpEmailApi\Api\ComponentDataLocatorInterface                        $componentDataLocator
     * @param \Magento\Framework\View\Asset\Repository                                         $viewAssetRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                                  $productRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface                                $priceCurrency
     * @param \Plumrocket\AmpEmailApi\Api\InitFrontProductPriceInterface                       $initFrontProductPrice
     * @param \Magento\Framework\View\ConfigInterface                                          $configView
     * @param \Magento\Catalog\Block\Product\ImageBuilder                                      $imageBuilder
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts $formScripts
     * @param array                                                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Url $frontUrlBuilder,
        \Plumrocket\AmpEmailApi\Api\ComponentDataLocatorInterface $componentDataLocator,
        \Magento\Framework\View\Asset\Repository $viewAssetRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Plumrocket\AmpEmailApi\Api\InitFrontProductPriceInterface $initFrontProductPrice,
        \Magento\Framework\View\ConfigInterface $configView,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        ReminderFormScripts $formScripts,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $frontUrlBuilder,
            $componentDataLocator,
            $viewAssetRepository,
            $productRepository,
            $priceCurrency,
            $initFrontProductPrice,
            $data
        );
        $this->viewConfig = $configView;
        $this->imageBuilder = $imageBuilder;
        $this->formScripts = $formScripts;
    }

    /**
     * Set states in needed
     *
     * @param $html
     * @return string
     */
    protected function _afterToHtml($html)
    {
        if ($this->isSingleProduct()) {
            $showFormForProductState = ['form' => ['showForProduct' => $this->getProducts()[0]->getId()]];
            if (isset($this->ampStates['reminder'])) {
                $this->ampStates['reminder'] = array_merge($this->ampStates['reminder'], $showFormForProductState);
            } else {
                $this->ampStates['reminder'] = $showFormForProductState;
            }
        }

        return parent::_afterToHtml($html);
    }

    /**
     * @return bool
     */
    public function isSingleProduct() : bool
    {
        return 1 === count($this->getProducts());
    }

    /**
     * @return \Magento\Catalog\Model\Product[]
     */
    public function getProducts() : array
    {
        return array_values($this->getEmailTemplateVars('products'));
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return \Plumrocket\AmpEmail\Block\Component\Product\View
     */
    public function createFormBlock(\Magento\Catalog\Api\Data\ProductInterface $product) : BlockInterface
    {
        /** @var \Plumrocket\AdvancedReviewAndReminder\Block\AmpComponent\Reminder\Form $component */
        $component = $this->getLayout()->createBlock(
            \Plumrocket\AdvancedReviewAndReminder\Block\AmpComponent\Reminder\Form::class
        );
        $component->setData(
            [
                'product'           => $product,
                'version'           => $this->getVersion(),
                'is_single_product' => $this->isSingleProduct(),
            ]
        );
        $component->setEmailTemplateVars($this->getEmailTemplateVars());
        $component->setComponentPartsCollector($this->getComponentPartsCollector());

        return $component;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getProductImage(\Magento\Catalog\Model\Product $product) : \Magento\Catalog\Block\Product\Image
    {
        return $this->imageBuilder->create($product, 'pr_amp_email_product_view');
    }

    /**
     * Returns image attribute
     *
     * @param string $imageId
     * @param string $attributeName
     * @param string $default
     * @return string|int
     */
    public function getImageAttribute($imageId, $attributeName, $default = null)
    {
        $attributes = $this->getConfigView()
                           ->getMediaAttributes('Magento_Catalog', Image::MEDIA_TYPE_CONFIG_NODE, $imageId);
        return $attributes[$attributeName] ?? $default;
    }

    /**
     * Retrieve config view
     *
     * @return \Magento\Framework\Config\View
     */
    private function getConfigView() : \Magento\Framework\Config\View
    {
        if (! $this->configView) {
            $this->configView = $this->viewConfig->getViewConfig();
        }
        return $this->configView;
    }

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts
     */
    public function getFormScripts(): ReminderFormScripts
    {
        return $this->formScripts;
    }
}
