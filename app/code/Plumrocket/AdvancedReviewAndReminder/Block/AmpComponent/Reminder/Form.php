<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\AmpComponent\Reminder;

use Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface;
use Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig;
use Plumrocket\AdvancedReviewAndReminder\Model\Permission\GuestSecretDataProvider;
use Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts;

/**
 * Class Form
 *
 * @method \Magento\Catalog\Model\Product getProduct()
 * @method bool getIsSingleProduct()
 * @method setProduct($product)
 * @method setIsSingleProduct(bool $flag)
 * @since 1.2.0
 */
class Form extends \Plumrocket\AmpEmailApi\Block\AbstractComponent
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetRatingsInterface
     */
    private $getRatings;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
     */
    private $autoLoginManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend
     */
    private $recommend;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts
     */
    private $formScripts;

    /**
     * Form constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                                 $context
     * @param \Magento\Framework\Url                                                           $frontUrlBuilder
     * @param \Plumrocket\AmpEmailApi\Api\ComponentDataLocatorInterface                        $componentDataLocator
     * @param \Magento\Framework\View\Asset\Repository                                         $viewAssetRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetRatingsInterface                  $getRatings
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                              $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface              $autoLoginManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend              $recommend
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig                      $reminderConfig
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts $formScripts
     * @param array                                                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Url $frontUrlBuilder,
        \Plumrocket\AmpEmailApi\Api\ComponentDataLocatorInterface $componentDataLocator,
        \Magento\Framework\View\Asset\Repository $viewAssetRepository,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetRatingsInterface $getRatings,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        AutoLoginManagerInterface $autoLoginManager,
        \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend,
        ReminderConfig $reminderConfig,
        ReminderFormScripts $formScripts,
        array $data = []
    ) {
        parent::__construct($context, $frontUrlBuilder, $componentDataLocator, $viewAssetRepository, $data);
        $this->getRatings = $getRatings;
        $this->configHelper = $configHelper;
        $this->autoLoginManager = $autoLoginManager;
        $this->recommend = $recommend;
        $this->reminderConfig = $reminderConfig;
        $this->formScripts = $formScripts;
    }

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    public function getConfigHelper() : \Plumrocket\AdvancedReviewAndReminder\Helper\Config
    {
        return $this->configHelper;
    }

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\ViewModel\AmpComponent\ReminderFormScripts
     */
    public function getFormScripts(): ReminderFormScripts
    {
        return $this->formScripts;
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        if (! $this->getTemplate()) {
            $this->setTemplate('ampemail/component/:version/reminder/form.phtml');
        }

        return parent::_toHtml();
    }

    /**
     * @param int $productId
     * @return string
     */
    public function getSubmitReviewUrl(int $productId) : string
    {
        if ($reminder = $this->getEmailTemplateVars('reminder')) {
            $orderId = $reminder->getOrderId();
        } elseif ($order = $this->getEmailTemplateVars('order')) { // For AmpEmail manual testing
            $orderId = $order->getId();
        } else {
            $orderId = 0;
        }

        $params = [
            'product' => $productId,
            'order' => $orderId
        ];

        if ($reminder) { // secret key may doesn't work in AmpEmail manual testing
            $secretForGuest = $reminder->getSecretKey() . '_' . $reminder->getId();
            $params[GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER] = $secretForGuest;
        }

        return $this->getAmpApiUrl('advancedrar/V1/ampEmail_review_submit', $params);
    }

    /**
     * @return array
     */
    public function getRatingsData() : array
    {
        $ratingItemsData = [];

        /** @var \Magento\Review\Model\Rating $rating */
        foreach ($this->getRatings->execute() as $rating) {
            $id = (int) $rating->getId();
            $ratingItemsData[$id] = [
                'ratingId' => $id,
                'ratingCode' => $rating->getRatingCode(),
                'ratingOptions' => array_reverse($rating->getOptions()), // revers options for css fix
            ];
        }

        return $ratingItemsData;
    }

    /**
     * @return string
     */
    public function getVisitStoreUrl() : string
    {
        return $this->getFrontUrl('', ['_nosid' => 1]);
    }

    /**
     * @return string
     */
    public function getShareReviewUrl() : string
    {
        if ($this->reminderConfig->isSingleFormEnabled()) {
            $url = $this->autoLoginManager->getUrl(
                'advancedrar/review/leavereview',
                ['id' => $this->getProduct()->getId()],
                $this->getCustomer(),
                $this->getReminder()
            );
        } else {
            $url = $this->autoLoginManager->addSecretToUrl(
                $this->getProduct()->getProductUrl(),
                $this->getCustomer(),
                $this->getReminder()
            );
        }

        return $url;
    }

    /**
     * @return \Magento\Customer\Model\Customer|null
     */
    public function getCustomer()
    {
        return $this->getEmailTemplateVars('customer');
    }

    /**
     * @return \Magento\Customer\Model\Customer|null
     */
    public function getGuestName()
    {
        return $this->getEmailTemplateVars('customerName');
    }

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\Reminder|null
     */
    public function getReminder()
    {
        return $this->getEmailTemplateVars('reminder');
    }

    /**
     * @return array
     */
    public function getRecommendSelect() : array
    {
        return $this->recommend->toOptionHash();
    }
}
