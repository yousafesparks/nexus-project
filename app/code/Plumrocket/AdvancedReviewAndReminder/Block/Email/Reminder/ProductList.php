<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Email\Reminder;

/**
 * Class ProductList
 * @since 1.2.0
 *
 * @method \Magento\Catalog\Model\Product[] getProducts()
 */
class ProductList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
     */
    private $autoLoginManager;

    /**
     * ProductList constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                    $context
     * @param \Magento\Catalog\Helper\Image                                       $imageHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager
     * @param array                                                               $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->imageHelper = $imageHelper;
        $this->autoLoginManager = $autoLoginManager;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param int                            $width
     * @param null                           $height
     * @return string
     */
    public function getProductImageUrl(\Magento\Catalog\Model\Product $product, int $width, $height = null) : string
    {
        return $this->imageHelper
            ->init($product, 'product_base_image')
            ->setImageFile($product->getFile())
            ->resize($width, $height)
            ->getUrl();
    }

    /**
     * @return \Magento\Catalog\Model\Product[]
     */
    public function getRelatedProductList() : array
    {
        $relatedProducts = is_array($this->getRelatedProducts())
            ? $this->getRelatedProducts()
            : $this->getRelatedProducts()->getItems();

        return $relatedProducts;
    }

    /**
     * @param \Magento\Catalog\Model\Product[] $products
     * @return bool|\Magento\Catalog\Model\Product
     */
    public function getFeaturedProduct($products)
    {
        $relatedProduct = false;

        if ($products) {
            foreach ($products as $product) {
                if (! $product->getFinalPrice()) {
                    continue;
                }
                $relatedProduct = $product;
                break;
            }

            if (! $relatedProduct) {
                $relatedProduct = reset($products);
            }
        }

        return $relatedProduct;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getAutoLoginUrlForProduct(\Magento\Catalog\Model\Product $product) : string
    {
        return $this->autoLoginManager->addSecretToUrl(
            $product->getProductUrl(),
            $this->getCustomer(),
            $this->getReminder()
        );
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getFormBlockHtml(\Magento\Catalog\Model\Product $product) : string
    {
        try {
            $formBlock = $this->getLayout()->getBlock('arar.email.product.list.form');
            $formBlock->setProduct($product);
            $formBlock->setCustomer($this->getCustomer());
            $formBlock->setReminder($this->getReminder());
            return $formBlock->toHtml();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return '';
        }
    }
}
