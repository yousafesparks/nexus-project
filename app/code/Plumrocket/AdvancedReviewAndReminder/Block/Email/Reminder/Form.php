<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Block\Email\Reminder;

use Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface as AutoLoginManager;
use Plumrocket\AdvancedReviewAndReminder\Model\Permission\GuestSecretDataProvider;
use Plumrocket\AdvancedReviewAndReminder\Model\Token\PostReviewFromEmailType;

/**
 * Block for leave review form in HTML email
 *
 * @since 1.2.0
 *
 * @method \Magento\Catalog\Model\Product getProduct()
 * @method \Plumrocket\AdvancedReviewAndReminder\Model\Reminder getReminder()
 * @method \Magento\Customer\Model\Customer getCustomer()
 *
 * @method setProduct(\Magento\Catalog\Model\Product $product)
 * @method setReminder(\Plumrocket\AdvancedReviewAndReminder\Model\Reminder $reminder)
 * @method setCustomer(\Magento\Customer\Model\Customer $customer)
 */
class Form extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\GetRatingsInterface
     */
    private $getRatings;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
     */
    private $autoLoginManager;

    /**
     * @var \Magento\Framework\Url
     */
    private $frontUrlBuilder;

    /**
     * @var \Plumrocket\Token\Api\GenerateForCustomerInterface
     */
    private $generateTokenForCustomer;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend
     */
    private $recommend;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * Form constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                    $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\GetRatingsInterface     $getRatings
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                 $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager
     * @param \Magento\Framework\Url                                              $frontUrlBuilder
     * @param \Plumrocket\Token\Api\GenerateForCustomerInterface                  $generateTokenForCustomer
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig         $reminderConfig
     * @param array                                                               $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Model\GetRatingsInterface $getRatings,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        AutoLoginManager $autoLoginManager,
        \Magento\Framework\Url $frontUrlBuilder,
        \Plumrocket\Token\Api\GenerateForCustomerInterface $generateTokenForCustomer,
        \Plumrocket\AdvancedReviewAndReminder\Model\System\Config\Recommend $recommend,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->getRatings = $getRatings;
        $this->configHelper = $configHelper;
        $this->autoLoginManager = $autoLoginManager;
        $this->frontUrlBuilder = $frontUrlBuilder;
        $this->generateTokenForCustomer = $generateTokenForCustomer;
        $this->recommend = $recommend;
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * @return \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    public function getConfigHelper() : \Plumrocket\AdvancedReviewAndReminder\Helper\Config
    {
        return $this->configHelper;
    }

    /**
     * @param int $productId
     * @return string
     */
    public function getSubmitReviewUrl(int $productId) : string
    {
        if (($reminder = $this->getReminder()) && ($order = $reminder->getOrder())) {

            $params = [
                'id' => $productId,
                'order' => $order->getId(),
                '_nosid' => 1,
            ];

            $params['token'] = $this->generateTokenForCustomer->execute(
                (int) $order->getCustomerId(),
                (string) $order->getCustomerEmail(),
                PostReviewFromEmailType::KEY
            )->getHash();

            if ($order->getCustomerId()) {
                $params[AutoLoginManager::QUERY_PARAM_NAME] = $this->autoLoginManager->getSecret(
                    $order->getCustomerId()
                );
            } else {
                $secretForGuest = $reminder->getSecretKey() . '_' . $reminder->getId();
                $params[GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER] = $secretForGuest;
            }

            return $this->frontUrlBuilder->getUrl('advancedrar/review/emailPost', $params);
        }

        return '';
    }

    /**
     * @return array
     */
    public function getRatingsData() : array
    {
        $ratingItemsData = [];

        /** @var \Magento\Review\Model\Rating $rating */
        foreach ($this->getRatings->execute() as $rating) {
            $id = (int) $rating->getId();
            $ratingItemsData[$id] = [
                'ratingId' => $id,
                'ratingCode' => $rating->getRatingCode(),
                'ratingOptions' => $rating->getOptions(),
            ];
        }

        return $ratingItemsData;
    }

    /**
     * @return string
     */
    public function getShareReviewUrl() : string
    {
        if ($this->reminderConfig->isSingleFormEnabled()) {
            $url = $this->autoLoginManager->getUrl(
                'advancedrar/review/leavereview',
                ['id' => $this->getProduct()->getId()],
                $this->getCustomer(),
                $this->getReminder()
            );
        } else {
            $url = $this->autoLoginManager->addSecretToUrl(
                $this->getProduct()->getProductUrl(),
                $this->getCustomer(),
                $this->getReminder()
            );
        }

        return $url;
    }

    /**
     * @return array
     */
    public function getRecommendSelect() : array
    {
        return $this->recommend->toOptionHash();
    }
}
