<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block\Email;

/**
 * @method bool hasReviewId()
 * @method bool hasReview()
 */
class Rating extends \Magento\Framework\View\Element\Template
{
    /**
     * Rating summary template name
     *
     * @var string
     */
    protected $_template = 'Magento_Review::rating/stars/summary.phtml';

    /**
     * Rating model
     *
     * @var \Magento\Review\Model\RatingFactory
     */
    private $ratingFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\RatingToStarsConverter
     */
    private $ratingToStarsConverter;

    /**
     * Rating constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context                          $context
     * @param \Magento\Review\Model\RatingFactory                                       $ratingFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\RatingToStarsConverter $ratingToStarsConverter
     * @param array                                                                     $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\RatingToStarsConverter $ratingToStarsConverter,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->ratingFactory = $ratingFactory;
        $this->ratingToStarsConverter = $ratingToStarsConverter;
    }

    /**
     * Retrieve rating with all values
     *
     * @return \Magento\Review\Model\Rating
     */
    public function getRatingSummary()
    {
        return $this->ratingFactory->create()->getReviewSummary($this->getReviewId());
    }

    /**
     * @return string[]
     */
    public function getRatingStars()
    {
        $rating = $this->getRatingSummary();

        return $this->ratingToStarsConverter->toStars((int)$rating->getSum() / (int)$rating->getCount());
    }

    /**
     * @return int
     */
    public function getReviewId()
    {
        if ($this->hasReview()) {
            return (int)$this->getReview()->getId();
        }

        if ($this->hasReviewId()) {
            return (int)$this->getData('review_id');
        }

        return 0;
    }
}
