<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Block;

class View extends \Magento\Review\Block\View
{
    /**
     * @return string
     */
    public function getBackUrl() : string
    {
        return $this->getUrl('review/product/list', ['id' => $this->getProductData()->getId()]);
    }

    /**
     * @inheritDoc
     */
    protected function _prepareLayout()
    {
        $pageTitleBlock = $this->getLayout()->getBlock('page.main.title');

        if ($pageTitleBlock && ! (string) $pageTitleBlock->getPageTitle()) {
            $pageTitleBlock->setPageTitle(__('Review Details'));
        }

        return parent::_prepareLayout();
    }
}
