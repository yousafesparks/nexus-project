/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'Magento_Ui/js/grid/columns/thumbnail',
    'jquery',
    'mage/template',
    'text!Plumrocket_AdvancedReviewAndReminder/template/grid/cells/youtube/preview.html',
    'mage/translate'
], function (Column, $, mageTemplate, youtubePreviewTemplate) {
    'use strict';

    return Column.extend({
        /**
         * Build preview.
         *
         * @param {Object} row
         */
        preview: function (row) {
            if (!row.youtube_id_orig_src) {
                return;
            }

            var modalHtml = mageTemplate(youtubePreviewTemplate, {videoUrl: row.youtube_id_orig_src}),
                previewPopup = $('<div/>').html(modalHtml);

            previewPopup.modal({
                title: $.mage.__('Attached Youtube Video'),
                innerScroll: true,
                modalClass: '_youtube-box',
                buttons: []
            }).trigger('openModal');
        },
    });
});
