/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'Magento_Ui/js/grid/columns/thumbnail',
    'jquery',
    'mage/template',
    'text!Plumrocket_AdvancedReviewAndReminder/template/grid/cells/images/preview.html',
    'mage/translate'
], function (Column, $, mageTemplate, thumbnailPreviewTemplate) {
    'use strict';

    return Column.extend({
        /**
         * Build preview.
         *
         * @param {Object} row
         */
        preview: function (row) {
            if (!row.attach_image) {
                return;
            }

            var images = [];

            for (var image of row.attach_image) {
                images.push({src: this.getOrigSrc(image), alt: this.getAlt(image)});
            }

            var modalHtml = mageTemplate(
                thumbnailPreviewTemplate,
                {
                    images: images
                }
            ),
            previewPopup = $('<div/>').html(modalHtml);

            previewPopup.modal({
                title: $.mage.__('Attached Images'),
                innerScroll: true,
                modalClass: '_image-box',
                buttons: []
            }).trigger('openModal');
        },
    });
});
