/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'Magento_Ui/js/grid/filters/filters'
], function (Collection) {
    'use strict';

    return Collection.extend({
        applyAbuse: function () {
            this.setData({abuse: 1}, true);
            this.apply();
        },

        applyPending: function () {
            this.setData({status_id: 2}, true);
            this.apply();
        },

        getCountAbusedReviews: function () {
            return this.count_abused_reviews;
        },

        getCountPendingReviews: function () {
            return this.count_pending_reviews;
        },
    });
});
