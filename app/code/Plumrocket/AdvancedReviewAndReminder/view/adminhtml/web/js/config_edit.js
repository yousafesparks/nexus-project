/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */
requirejs([
    'jquery',
    'domReady!'
], function ($) {
    "use strict";

    window.advancedrarUpdate = function () {
        var fromDate = $('#pr_reminder_update_old_orders_from').val();
        var toDate = $('#pr_reminder_update_old_orders_to').val();

        if (!fromDate || !toDate) {
            alert('Please set date range');
        } else {
            new Ajax.Request(window.prArarAjaxCheckUrl, {
                method:     'get',
                parameters: {from: fromDate, to: toDate},
                onSuccess: function (transport) {
                    var result = JSON.parse(transport.responseText);
                    if (result.success) {
                        alert('Scheduled successfully');
                    } else {
                        alert('Something went wrong. ' + result.message);
                    }
                }
            });
        }
    };
});
