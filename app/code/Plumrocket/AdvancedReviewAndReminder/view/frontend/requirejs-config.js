var config = {
    map: {
        '*': {
            prarUploader: 'Plumrocket_AdvancedReviewAndReminder/js/uploader',
            youTubeValidationMethod: 'Plumrocket_AdvancedReviewAndReminder/js/youTubeValidationRule',
            prReviewUtils: 'Plumrocket_AdvancedReviewAndReminder/js/model/utils',
            prReviewStorageService: 'Plumrocket_AdvancedReviewAndReminder/js/model/review-storage-service',
            prOwlcarousel: 'Plumrocket_AdvancedReviewAndReminder/js/lib/owl.carousel.min',
            jQueryMark: 'Plumrocket_AdvancedReviewAndReminder/js/lib/jquery.mark.min',
            prWidgetCarousel: 'Plumrocket_AdvancedReviewAndReminder/js/widget-carousel',
        }
    },
    shim: {
        'varien/js': {
            deps: ['prototype']
        },
        'prOwlcarousel': {
            deps: ['jquery']
        }
    }
};
