/*
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define(
    [
        'jquery',
        'underscore',
        'Plumrocket_AdvancedReviewAndReminder/js/model/permission',
        'domReady!',
    ],
    function ($, _, permission) {
        "use strict";
        window._advancedReviewAndReminder = function (conf) {
            var $this = this;

            $this.conf = {};
            $this.postReviewCallbacks = [];

            for (var key in conf) {
                if (conf.hasOwnProperty(key)) {
                    $this.conf[key] = conf[key];
                }
            }

            $this.trySubmit = function (GET_Params) {
                var reviewForm = $('#review-form');
                if ($this.validateForm(reviewForm)) {
                    advancedrar.postReview(reviewForm, GET_Params);
                }
            };

            /**
             * Show error messages
             *
             * @return {Boolean}
             */
            $this.validateForm = function (reviewForm) {
                if (! reviewForm) {
                    reviewForm = $('#review-form');
                }

                return Boolean(reviewForm.validation() && reviewForm.validation('isValid'));
            };

            $this.postReview = function (form, GET_Params) {
                $('#pr-arar-submit-btn').prop('disabled', true);
                $($this.conf.loaderClass).css('display', 'inline-block');
                $this.submitButton.notactive();

                if (! form[0].dataset.initedPrListener) {
                    // Add listener to form in order to allow other scripts do they work before send form,
                    // for example reCAPTCHA performs request for token
                    form.submit(function (event) {
                        event.preventDefault(event);
                        $this.sendRequest(form, GET_Params);
                    });
                    form[0].dataset.initedPrListener = 'yes';
                }

                form.submit();

                return false;
            };

            $this.sendRequest = function (form, GET_Params) {
                $.ajax({
                    url : $this.addGuestSecretData(form[0].action, GET_Params),
                    data : form.serialize(),
                    method : 'post',
                    dataType : 'json'
                })
                    .success(function (data) {
                        $($this.conf.loaderClass).hide();
                        $this.submitButton.active();
                        $this._clearMessages();

                        if (! data.success) {
                            //login from social buttons
                            if (typeof $this.conf.prSocialLogin != 'undefined') {
                                var adv = $this.conf.prSocialLogin;
                                psLogin(adv.href, adv.width, adv.height);
                                $('#pr-arar-submit-btn').prop('disabled', false);
                                return;
                            }

                            if ($('#popup_login', $this.conf.reviewForm).length
                                && $('#popup_login', $this.conf.reviewForm).val() == 'true'
                                && typeof data.redirect !== 'undefined'
                            ) {
                                if (typeof window.plumrocket !== 'undefined'
                                    && typeof window.plumrocket.popupLoginControl !== 'undefined'
                                ) {
                                    window.plumrocket.popupLoginControl.openLoginForm();
                                    $('#pr-arar-submit-btn').prop('disabled', false);
                                    return;
                                }
                            }

                            if (typeof data.redirect !== 'undefined' && data.redirect.length) {
                                window.location = data.redirect;
                                return;
                            }

                            if ($.isArray(data.message)) {
                                $(data.message).each(function (key, value) {
                                    $this._printMessage(value, data.success);
                                });
                            } else {
                                $this._printMessage(data.message, data.success);
                            }

                            $this.scrollToMessages();
                        } else {
                            var reviewFormBox = $($this.conf.reviewFormBox);
                            $this.reviewSubmitted = true;
                            $this.showSuccessMessage(data.title, data.subtitle);
                            reviewFormBox.addClass('pr-success');
                            reviewFormBox.contents(':not(#arar-form-messages, #success_review_submit)').remove();
                            $this.removeNoReviewBlock();
                            $this.removePopupTitle();
                        }

                        $this.runPostReviewCallbacks(data);
                    })
                    .error(function () {
                        $this.submitButton.active();
                        $($this.conf.loaderClass).hide();
                        alert('Unexpected error. Please try again later.');
                    })
                    .always(function () {
                        $('#pr-arar-submit-btn').prop('disabled', false);
                    });
            };

            $this.addGuestSecretData = function (url, GET_Params) {
                var params = permission.getGuestSecretParams();

                if (typeof GET_Params === 'object' && _.size(GET_Params)) {
                    _.each(GET_Params, function (param, key) {
                        params[key] = param;
                    })
                }

                if (_.size(params)) {
                    if (-1 !== url.indexOf('?')) {
                        return url + '&' + _.map(params, function (param, key) {
                            return key + '=' + param;
                        }).join('&');
                    } else {
                        var urlParamConnector = ('/' !== url.slice(-1)) ? '/' : '';
                        return url + urlParamConnector + _.map(params, function (param, key) {
                            return key + '/' + param;
                        }).join('/') + '/';
                    }
                }

                return url;
            };

            $this.submitButton = {
                notactive : function () {
                    if (typeof $this.submitButton.button == 'object') {
                        $($this.submitButton.button).attr('disabled', true);
                    }
                },

                active : function () {
                    if (typeof $this.submitButton.button == 'object') {
                        $($this.submitButton.button).attr('disabled', false);
                    }
                },

                button : null
            };

            $this._clearMessages = function () {
                $($this.conf.messageBox).html('');
            };

            $this._printMessage = function (msg, success) {
                var msgClass = success ? 'success' : 'error',
                    html = '<div role="alert" class="messages">';

                html += '<div class="message message-' + msgClass + ' ' + msgClass + '">';
                html += '<div>';
                html += msg;
                html += '</div>';
                html += '</div>';
                html += '</div>';

                $($this.conf.messageBox).append(html);
                $($this.conf.reviewFormBox).closest('.modal-content').animate({scrollTop: 0}, 200)
            };

            /**
             * Show message either with icon or with only text
             *
             * @param {String} title
             * @param {String} subtitle
             * @return {Object}
             */
            $this.showSuccessMessage = function (title, subtitle) {
                var container = document.getElementById('success_review_submit');
                if (container && container.innerHTML.trim()) {
                    container.style.display = 'block';
                    var titleContainer = container.querySelector('.success-message__title');
                    if (titleContainer) {
                        titleContainer.innerText = title;
                    }
                    var subtitleContainer = container.querySelector('.success-message__text');
                    if (subtitleContainer && subtitle) {
                        subtitleContainer.innerText = subtitle;
                    }
                } else {
                    $this._printMessage(title + ' ' + subtitle, true)
                }

                return $this;
            };

            $this.runPostReviewCallbacks = function (data) {
                $this.postReviewCallbacks.forEach(function (callback) {
                    callback(data);
                });
            };

            $this.registerPostReviewCallback = function (callback) {
                if (typeof callback === 'function') {
                    $this.postReviewCallbacks.push(callback);
                } else {
                    console.error('Callback must be function');
                }
            };

            $this.scrollToMessages = function () {
                $('html, body').animate({
                    scrollTop: $('#arar-form-messages').offset().top - 20
                }, 450);
            };

            $this.submitAndLoginWithSocial = function (href, width, height) {
                $this.conf.prSocialLogin = {
                    href: href,
                    width: width,
                    height: height,
                };

                $this.trySubmit({referer: window.location.href});
            };

            this.removeNoReviewBlock = function () {
                var noReviewText = $('#reviews .prar-no-review');

                if (noReviewText.length) {
                    noReviewText.closest('#reviews').find('.prar-prod-rating').remove();
                }
            };

            this.removePopupTitle = function () {
                var title = $('.prar-form-popup header > h1');

                if (title.length) {
                    title.remove();
                }
            },

            this.initEvents = function () {
                $('body').on('click', function (event) {
                    if (event.target.dataset.prAction === 'show-review-form') {
                        event.preventDefault();
                        if ($this.reviewSubmitted) {
                            window.location.hash = '#arar-form-add';
                            window.location.reload();
                            return false;
                        }

                        if (window.prOpenReviewForm) {
                            window.prOpenReviewForm();
                            return false;
                        }
                    }

                    if (event.target.dataset.prAction === 'scroll-to-review-list') {
                        event.preventDefault();

                        $($this.conf.reviewTabButtonSelector).trigger('click');

                        $('html, body').animate({
                            scrollTop: $('#prar_review_list_header_anchor').offset().top
                        }, 300);
                        return false;
                    }

                    return true;
                });
            }
        }
    }
);
