/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'jquery',
    'prOwlcarousel',
    'domReady!'
], function ($) {
    'use-strict';
    return function (config) {
        var owlCarouselClass = config.owlCarouselClass;
        var template = config.template;
        var owlOptions = config.owlOptions;
        var itemGridFour = $('.container-four-item > .review-grid-item');

        function hideDots() {
            var carouselDots = $('.' + owlCarouselClass + '.owl-loaded .owl-dots');
            var dotsLength = carouselDots.children().length;
            if (dotsLength >= 8) {
                carouselDots.hide();
                $('.' + owlCarouselClass + '.owl-loaded .owl-nav').addClass('dotsHide');
                $('.'  + owlCarouselClass).prev('.title-container').addClass('margin-small');
            }
        }

        function setOwlStageHeight(item) {
            var maxHeight = 0;
            item.each(function () {
                var thisHeight = parseInt( $(this).outerHeight());
                maxHeight = (maxHeight >= thisHeight ? maxHeight:thisHeight);
            });
            item.css('height', maxHeight);
        }

        function refreshCarousel() {
            $('.' + owlCarouselClass + '.owl-loaded').parent('.reviews-content-wrapper').css({"overflow":"visible"});
            setTimeout(function () {
                if ($( window ).width() >= 1024) {
                    setOwlStageHeight(itemGridFour);
                }
                carousel.trigger('refresh.owl.carousel');
                $('.' + owlCarouselClass + '.owl-loaded').parent('.reviews-content-wrapper').css({"height":"auto","visibility": "visible"});
            },1000);
        }

        if ($('.' + owlCarouselClass).outerWidth() > 300 && template === 'grid') {
            owlOptions.autoHeight = false;
        }

        var carousel = $('.' + owlCarouselClass).owlCarousel(owlOptions);

        if (template === 'grid') {
            hideDots();
        }

        refreshCarousel();
    }
});
