/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */
define([
    'jquery',
    'uiElement',
    'mage/translate'
], function ($, Element) {
    'use strict';

    return Element.extend({
        initialize: function (config, element) {
            this._super();
            this.element = $(element);
            this.initEvents();
        },

        initEvents: function () {
            this.element.find('input[type="button"]').on('click', this.submit.bind(this));
            this.element.find('input[type="radio"]').on('change', this.onChangeReason.bind(this));
        },

        submit: function () {
            var self = this;

            if (this.isValidForm()) {
                this.element.submit($.ajax({
                    showLoader: true,
                    method: 'post',
                    data: self.element.serializeArray(),
                    url: self.element.attr('action'),
                    success: function (response) {
                        self.element.remove();
                        $('.unsubscribe-success-message').show();
                    },
                }));
            }
        },

        onChangeReason: function (event) {
            this.element.find('textarea').hide();
            var textarea = $(event.target).siblings('textarea');

            if (textarea) {
                textarea.show();
            }
        },

        isValidForm: function () {
            return this.element.validation() && this.element.validation('isValid');
        }
    });
});
