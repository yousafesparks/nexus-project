/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'underscore',
    'ko',
    'uiComponent',
    'jquery',
    'Plumrocket_AdvancedReviewAndReminder/js/model/review-storage-service',
    'prReviewUtils',
    'jQueryMark',
    'mage/translate'
], function (_, ko, Component, $, reviewStorageService, prReviewUtils) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Plumrocket_AdvancedReviewAndReminder/review-list'
        },

        reviewList: ko.observableArray([]),
        reviewListStatus: ko.observable('initial'),

        /**
         * @type {{type: string, sourceUrl: string, reviewId: integer}}
         */
        configuration: {},

        loadingState: {},

        /**
         * Will be used for highlighting search words
         */
        searchString: '',

        /**
         * Define if render mobile version or not
         */
        isMobile: true,

        /** @inheritdoc */
        initialize: function () {
            this.isMobile = prReviewUtils.isMobile();
            this._super();
            this.reviewStorageService = reviewStorageService;
            reviewStorageService.setOptions({sourceUrl: this.configuration.sourceUrl, useUrlParams: this.configuration.useUrlParams});

            var searchCriteria = {};
            if ('item' === this.configuration.type) {
                searchCriteria.review = this.configuration.reviewId;
                reviewStorageService.setPageSize(1);
            }

            var initPrams = this.reviewStorageService.getInitParamsFromUrl();
            this.onFilterStateChange(initPrams.filterState);
            this.reviewStorageService.getFilterState().subscribe(this.onFilterStateChange.bind(this));

            this.reviewList = reviewStorageService.getList(searchCriteria);
            this.reviewListStatus = reviewStorageService.getListStatus();
        },

        /**
         * Add/update helpful vote
         *
         * @param review
         * @param isPositive
         */
        rateHelpful: function (review, isPositive) {
            if (! this.configuration.canRateHelpful) {
                return;
            }

            $.ajax({
                url : this.configuration.helpfulUrl,
                data : {
                    helpful : isPositive,
                    reviewId : review.id
                },
                method : 'POST',
                dataType : 'json',
                showLoader: false,
                /** @inheritdoc */
                beforeSend: function () {
                    this.setLoading(review.id, 'helpful', true);
                }.bind(this),
                success: function (data) {
                    review.vote(data.vote);
                    review.helpfulPositive(data.positive);
                    review.helpfulNegative(data.negative);
                },
                error: $.proxy(this._onError, this),

                /** @inheritdoc */
                complete: function () {
                    this.setLoading(review.id, 'helpful', false);
                }.bind(this)
            });
        },

        /**
         * Log error message in console
         *
         * @param error
         * @private
         */
        _onError: function (error) {
            if (error.responseJSON) {
                console.warn(JSON.parse(error.responseJSON));
            } else {
                if (error.responseText) {
                    console.warn(error.responseText);
                }
            }
        },

        /**
         * Send request for submitting abuse report
         *
         * @param review
         */
        reportAbuse: function (review) {
            $.ajax({
                url : this.configuration.abuseUrl,
                data : {
                    reviewId : review.id
                },
                method : 'POST',
                dataType : 'json',
                showLoader: false,
                /** @inheritdoc */
                beforeSend: function () {
                    this.setLoading(review.id, 'abuse', true);
                }.bind(this),
                success: function (data) {
                    review.markedAsAbuse(true);
                },
                error: $.proxy(this._onError, this),
                /** @inheritdoc */
                complete: function () {
                    this.setLoading(review.id, 'abuse', false);
                }.bind(this)
            });
        },

        /**
         * Open window for share
         *
         * @param url
         */
        openShareButton: function (url) {
            window.open(url,'sharewindow','height=400, width=500');
        },

        /**
         * Return observable for checking loading progress
         *
         * @param {integer} reviewId
         * @param {string} type
         * @return {ko.observable}
         */
        getLoading: function (reviewId, type) {
            var key = type + reviewId;
            if (! this.loadingState.hasOwnProperty(key)) {
                this.loadingState[key] = ko.observable(false);
            }

            return this.loadingState[key];
        },

        /**
         * Set loading state
         *
         * @param reviewId
         * @param type
         * @param value
         * @return {exports}
         */
        setLoading: function (reviewId, type, value) {
            this.getLoading(reviewId, type)(value);
            return this;
        },

        /**
         * Save search string
         *
         * @param state
         */
        onFilterStateChange:  function (state) {
            if (state.search && ! _.isEmpty(state.search)) {
                this.searchString = _.keys(state.search)[0];
            } else {
                this.searchString = '';
            }

            if (this.reviewListStatus() !== 'loading') {
                _.each(this.reviewList(), function (review) {
                    this.highlightSearchTermOnReview(review.id);
                }.bind(this))
            }
        },

        /**
         * Highlight search terms in review
         *
         * @param reviewId
         */
        highlightSearchTermOnReview: function (reviewId) {
            var self = this;
            var elem = $('#review-item-' + reviewId);

            elem.unmark({done: function () {
                if (self.searchString) {
                    elem.mark(self.searchString, {separateWordSearch: false, accuracy: 'complementary'});
                }
            }});
        },

        hasMultyRatings: function (review) {
            return review.ratings && review.ratings.length > 1;
        },

        isFuteresEnabled: function () {
            return false;
        },

        getStickyProductBlockHtml: function () {
            var formKey = $.cookie('form_key');

            return this.configuration.productView.replace("{{form_key}}",formKey);
        },

        initScrollEvent: function (elem) {
            var listElem = document.getElementById('customer-reviews-box'), visible;
            elem = $(elem);

            $(window).on('scroll', function () {
                visible = elem.hasClass('visible');

                if (!visible && listElem.getBoundingClientRect().top < 0) {
                    elem.addClass('visible');
                } else if (visible && listElem.getBoundingClientRect().top > 0) {
                    elem.removeClass('visible');
                }
            });
        }
    });
});
