/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */
define([
    'jquery',
    'mage/template',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/lib/validation/validator',
    'mage/translate',
    'jquery/file-uploader',
    'varien/js'
], function ($, mageTemplate, uiAlert, validator) {
    'use strict';

    $.widget('plum.prarFileUploader', {
        /**
         *
         * @private
         */
        _create: function () {
            var self = this,
                progressTmpl = mageTemplate('[data-template="img-uploader"]');

            var $input = $('input[type="file"]', this.element);

            var allowedExtensions = $input.data('types').split(',').join(' ');
            var maxFileSize = $input.data('maxFileSize');

            $('input[type="file"]', this.element).fileupload({
                dataType: 'json',
                formData: {
                    'form_key': window.FORM_KEY
                },
                sequentialUploads: true,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: maxFileSize,

                /**
                 * @param {Object} e
                 * @param {Object} data
                 */
                add: function (e, data) {
                    var fileSize,
                        tmpl,
                        validationResult;

                    if (self.element.find('.file-row').size() >= self.options.maxFilesCount) {
                        uiAlert({
                            content: $.mage.__('Maximum count of attached files is ' + self.options.maxFilesCount)
                        });
                        return false;
                    }

                    data.files = data.files.filter(function (file) {
                        if (maxFileSize) {
                            validationResult = validator('validate-max-size', file.size, maxFileSize);

                            if (!validationResult.passed) {
                                uiAlert({
                                    content: validationResult.message
                                });

                                return false;
                            }
                        }

                        if (allowedExtensions) {
                            validationResult = validator('validate-file-type', file.name, allowedExtensions);

                            if (!validationResult.passed) {
                                uiAlert({
                                    content: validationResult.message
                                });

                                return false;
                            }
                        }

                        return true;
                    });

                    $.each(data.files, function (index, file) {
                        fileSize = typeof file.size == 'undefined' ?
                            $.mage.__('We could not detect a size.') :
                            byteConvert(file.size);

                        data.fileId = Math.random().toString(36).substr(2, 9);

                        tmpl = progressTmpl({
                            data: {
                                name: file.name,
                                size: fileSize,
                                id: data.fileId
                            }
                        });

                        if (!self.isTitleInited) {
                            self.isTitleInited = true;
                            tmpl = '<span class="image-title">' + $.mage.__('Photos') + '</span>' + tmpl;
                        }
                        $(tmpl).appendTo(self.element);
                    });

                    if (data.files.length) {
                        $(this).fileupload('process', data).done(function () {
                            data.submit();
                        });
                    }
                },

                /**
                 * @param {Object} e
                 * @param {Object} data
                 */
                done: function (e, data) {
                    var progressSelector = '#' + data.fileId + ' .progressbar-container .progressbar';

                    if (data.result && !data.result.error) {
                        $('#' + data.fileId).addClass('done');
                        $('#' + data.fileId).find('.img-upload').prepend(
                            '<div style="' +
                                'width: 62px; height: 46px; background-image: url('+data.result.url+'); ' +
                                'background-size: cover; background-position: center;" ' +
                            'title="' + data.result.name + '"></div>'
                        );
                        $(progressSelector).removeClass('upload-progress').addClass('upload-success');
                        self.element.find('#' + data.fileId + ' input.filename').val(data.result.file);
                        self.element.trigger('addItem', data.result);
                    } else {
                        var error = $.mage.__('We don\'t recognize or support this file extension type.');

                        if (-1 === data.result.errorcode && data.result.error) {
                            error = data.result.error;
                        }
                        uiAlert({
                            content: error
                        });
                        self.element.find('#' + data.fileId).remove();
                    }
                },

                /**
                 * @param {Object} e
                 * @param {{loaded: Number, total: Number, fileId: String}} data
                 */
                progress: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10),
                        progressSelector = '#' + data.fileId + ' .progressbar-container .progressbar';

                    self.element.find(progressSelector).css('width', progress + '%');
                },

                /**
                 * @param {Object} e
                 * @param {Object} data
                 */
                fail: function (e, data) {
                    var progressSelector = '#' + data.fileId;

                    self.element.find(progressSelector).removeClass('upload-progress').addClass('upload-failure')
                        .delay(2000)
                        .hide('highlight')
                        .remove();
                }
            });

            this.element.find('input[type=file]').fileupload('option', {
                process: [{
                    action: 'load',
                }, {
                    action: 'resize',
                }, {
                    action: 'save'
                }]
            });

            // File delete event.
            this.element.on('click', '.file-delete', function () {
                $(this).closest('.file-row').remove();
                if (!self.element.find('.file-row.comment-file').length) {
                    self.isTitleInited = false;
                    self.element.find('.image-title').remove();
                }

                return false;
            });

            // Autofill uploaded files after page refresh.
            if (this.element.fileList) {
                $.each(self.options.fileList, function (index, file) {
                    var tmpl = progressTmpl({
                        data: {
                            name: file.name,
                            size: file.size,
                            id: index,
                            filename: file.filename,
                            rowclass: 'done'
                        }
                    });

                    $(tmpl).appendTo($('#arar-upload'));
                });
            }
        },
    });

    return $.plum.prarFileUploader;
});
