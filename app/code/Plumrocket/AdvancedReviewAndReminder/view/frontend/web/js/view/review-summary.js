/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'ko',
    'uiComponent',
    'jquery',
    'Plumrocket_AdvancedReviewAndReminder/js/model/permission',
    'mage/translate'
], function (ko, Component, $, permission) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Plumrocket_AdvancedReviewAndReminder/review-summary'
        },

        /** @inheritdoc */
        initialize: function () {
            this._super();

            /**
             * Some component must set configuration into permission model for calculating
             * In this case, it's pr-review-form
             */
            permission.bindInfo(this);

            this.reviewCountLabel = $.mage.__('%1 Review(s)').replace('%1', this.review.count);
        },
    });
});
