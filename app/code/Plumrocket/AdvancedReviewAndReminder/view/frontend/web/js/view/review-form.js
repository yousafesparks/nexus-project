/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'ko',
    'jquery',
    'Magento_Review/js/view/review',
    'Plumrocket_AdvancedReviewAndReminder/js/model/permission',
    'Magento_Ui/js/modal/modal',
    'Magento_Customer/js/customer-data',
    'mage/validation',
    'Plumrocket_AdvancedReviewAndReminder/js/advancedrar',
    'Plumrocket_AdvancedReviewAndReminder/js/lib/jquery.raty',
    'mage/translate',
    'prarUploader'
], function (ko, $, ReviewComponent, permission, modal, customerData) {
    'use strict';

    return ReviewComponent.extend({
        defaults: {
            template: 'Plumrocket_AdvancedReviewAndReminder/review-form'
        },

        reviewSessionFields: ['nickname', 'title', 'detail', 'pros', 'cons'],
        ratingOptions: {},
        ratyHints: [
            $.mage.__('Bad'),
            $.mage.__('Poor'),
            $.mage.__('It\'s okay'),
            $.mage.__('I like it'),
            $.mage.__('I love it!'),
        ],

        recaptchaContainerReady: false,

        /**
         * @type {{summaryOption: string, permission: {}, prosAndCons: {}, reCaptcha: {}, summaryMaxLength: number}}
         */
        configuration: {},

        videoLink: ko.observable(''),
        leftCharacters: ko.observable(50),

        /**
         * Only for lazy load
         * Define if run rendering immediately or not, don't use for showing/hiding
         */
        wasOpen: ko.observable(false),

        /**
         * Hide or show form with save html for prevent second initialization
         */
        isVisible: ko.observable(false),

        /** @inheritdoc */
        initialize: function () {
            this._super();

            permission.setConfiguration(this.customerGroup, this.configuration.permission)
                .bindInfo(this);

            this.initRecaptchaCallback();

            this.prepareRatingItemsData();

            /**
             * From Magento v2.3.4 section review do not update by advancedrar/review/emailPost action.
             * As an alternative to sections.xml we use js reload to get review data from the review section.
             */
            if (window.location.hash === '#arar-form-validation') {
                customerData.reload(['review']);
            }
            this.review.subscribe(this.validateOnFormLoad.bind(this));

            this.initFormVisibility();
        },

        isSingleRating: function () {
            return 1 === this.rating.count;
        },

        isCustomerLoggedIn: function () {
            return ! permission.isGuest();
        },

        isGuest: function () {
            return permission.isGuest();
        },

        initSubmitListener: function () {
            var refererQueryParamName = this.refererQueryParamName;
            var GET_Params = {};

            $(document).on('click', '#pr-arar-submit-btn', function () {
                if (this.dataset.referer === 'currentPage') {
                    GET_Params[refererQueryParamName] = window.location.href;
                    window.advancedrar.trySubmit(GET_Params);
                } else {
                    window.advancedrar.trySubmit(GET_Params);
                }
            });
        },

        initFormFunctionality: function () {
            if (! this.canWriteReview) {
                return true;
            }

            this.initSubmitListener();
        },

        initUploadFunctionality: function () {
            $('#arar-upload').prarFileUploader();
        },

        initVideoUploader: function () {
            var self = this, options = {
                responsive: true,
                buttons: [{
                    text: $.mage.__('Submit'),
                    class: 'btn action primary',
                    click: self.submitYoutubeVideo.bind(self)
                }],
                modalClass: "prar-video-popup",
                title: $.mage.__('Add Video')
            };

            this.videoPopup = $('#video-attach-popup');
            modal(options, this.videoPopup);

            $('#arar-upload .prar-uploader-box .prar-uploader-item.video').on('click', function () {
                self.videoPopup.modal('openModal');
            });
        },

        removeVideo: function () {
            this.videoLink(false);
        },

        submitYoutubeVideo: function (component, event) {
            this.getPreview(this.videoValue);
            if (window.advancedrar.validateForm($('#video-attach-popup'))) {
                this.videoPopup.modal('closeModal');
            }
        },

        setYoutubeVideoValue: function (component, event) {
            this.videoValue = event.target.value;
        },

        /**
         * Calculate initial state an add opportunity to change from any js code
         */
        initFormVisibility: function () {
            this.formPopup = $(window.advancedrar.conf.reviewFormBox);

            if (this.configuration.open) {
                this.wasOpen(true);
                this.formPopup.loader('hide');
            } else if (window.location.hash === '#arar-form-add'
                || window.location.hash === '#arar-form-validation'
            ) {
                this.openForm();
            }

            window.prOpenReviewForm = this.openForm.bind(this);
            this.initSuccessReviewSubmitAction();
        },

        validateOnFormLoad: function () {
            if (! this.canWriteReview) {
                return true;
            }

            if (-1 !== window.location.pathname.indexOf('validate/1')
                || window.location.hash === '#arar-form-validation'
            ) {
                window.advancedrar.validateForm();
            }
        },

        /**
         * Simplify search item information in future
         */
        prepareRatingItemsData: function () {
            this.rating.items.forEach(function (item, index) {
                item.index = index;
            })
        },

        /**
         * @param {integer} index
         */
        initRating: function (index) {
            var self = this;

            /**
             * @type {{index: integer, ratingCode: string, ratingId: string, ratingOptions: string}}
             */
            var rating = self.rating.items[index];

            self.ratingOptions[rating.ratingId] = rating.ratingOptions.split(',');

            $('#rating-stars' + rating.ratingId).raty({
                number: rating.ratingOptions.split(',').length,
                path: this.rating.image,
                target : '#rating_form_text' + rating.ratingId,
                hints: this.ratyHints,
                click: function (score, evt) {
                    $('#rating-stars-value' + rating.ratingId).val(self.ratingOptions[rating.ratingId][score-1]);
                },
                mouseout: function (score, evt) {
                    $('#rating_form_text' + rating.ratingId).html(self.ratyHints[score - 1]);
                }
            });

            this.review.subscribe(this.syncRatingCallback.bind(this, rating));
        },

        /**
         * Update selected ratings after update "review" section
         *
         * @param rating
         * @param reviewData
         */
        syncRatingCallback: function (rating, reviewData) {
            var self = this;

            if (reviewData
                && reviewData.rating
                && reviewData.rating[rating.ratingId]
                && self.ratingOptions[rating.ratingId]
            ) {
                var position = self.ratingOptions[rating.ratingId].indexOf('' + reviewData.rating[rating.ratingId]);
                if (-1 !== position) {
                    var img = $('#rating-stars' + rating.ratingId + ' img:nth-child(' + (position + 1) + ')');
                    if (img.length) {
                        img.click().trigger('mouseover');
                        this.validateOnFormLoad();
                    }
                }
            }
        },

        initRecaptchaCallback: function () {
            $(window).on('recaptchaapiready', this.onloadCaptchaApiCallback.bind(this));
        },

        onloadCaptchaApiCallback: function () {
            this.grecaptchaRender = grecaptcha.render;

            if (true === this.recaptchaContainerReady) {
                this.renderRecaptcha();
            }
        },

        renderRecaptcha: function () {
            var self = this;
            this.recaptchaContainerReady = true;

            var reCaptchaConfig = this.configuration.reCaptcha;

            if (typeof this.grecaptchaRender !== 'function'
                && window.grecaptcha &&
                typeof window.grecaptcha.render === 'function'
            ) {
                this.grecaptchaRender = window.grecaptcha.render
            }

            if (typeof this.grecaptchaRender === 'function') {
                this.tokenField = $('<input type="text" name="token" style="display: none" />')[0];
                $('#review-form').append(this.tokenField);

                this.grecaptchaRender('prArar-g-recaptcha', {
                    'sitekey': reCaptchaConfig.settings.siteKey,
                    'theme': reCaptchaConfig.settings.theme,
                    'size': reCaptchaConfig.settings.size,
                    'badge': reCaptchaConfig.badge ? reCaptchaConfig.badge : reCaptchaConfig.settings.badge,
                    'callback': function (token) {
                        self.reCaptchaCallback(token);
                    }
                });
            }
        },

        /**
         * Recaptcha callback
         * @param {String} token
         */
        reCaptchaCallback: function (token) {
            if (this.configuration.reCaptcha.settings.size === 'invisible') {
                this.tokenField.value = token;
                $('.pr-arar-submit-btn').trigger('click');
            }
        },

        getPreview: function (url) {
            var youtubeId;

            if (url) {
                youtubeId = this.getYoutubeId(url);
            }

            this.videoLink(youtubeId);
        },

        getYoutubeId: function (url) {
            var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                return match[2];
            } else {
                return false;
            }
        },

        /**
         * Update count of left characters
         *
         * @param component
         * @param event
         */
        updateSummaryCount: function (component, event) {
            var summaryMaxLength = component.configuration.summaryMaxLength;
            if (summaryMaxLength) {
                if (summaryMaxLength - Number(event.target.value.length) <  0) {
                    event.target.value = event.target.value.substring(0, summaryMaxLength);
                }

                this.leftCharacters(summaryMaxLength - Number(event.target.value.length));
            }
        },

        /**
         * Open form in popup
         */
        openForm: function () {
            if (this.formPopup) {
                this.wasOpen(true);
                modal({
                    buttons: [],
                    title: $.mage.__('Write Your Own Review'),
                    modalClass: 'prar-form-popup',
                    outerClickHandler: this.closeForm.bind(this)
                }, this.formPopup);

                this.formPopup.modal('openModal');
            }
        },

        closeForm: function () {
            if (this.formPopup) {
                this.wasOpen(false);
                this.formPopup.removeClass('pr-success');
                this.formPopup.modal('closeModal');
            }
        },

        initSuccessReviewSubmitAction: function () {
            window.advancedrar.registerPostReviewCallback(function (data) {
                if (data.success && ! this.configuration.open) {
                    setTimeout(function () {
                        this.closeForm();
                    }.bind(this), 10000);
                }
            }.bind(this));
        },
    });
});
