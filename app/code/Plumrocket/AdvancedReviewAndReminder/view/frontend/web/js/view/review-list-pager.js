/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'underscore',
    'ko',
    'uiComponent',
    'jquery',
    'prReviewStorageService',
    'prReviewUtils',
    'mage/translate'
], function (_, ko, Component, $, reviewStorageService, prReviewUtils) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Plumrocket_AdvancedReviewAndReminder/review-list-pager'
        },

        pages: ko.observableArray([]),
        pageSize: 10,
        pageState: ko.observable(),
        currentPage: ko.observable(1),
        seeAllLabel: ko.observable(''),
        seeAllText: 'See All %1 Reviews',

        /** @inheritdoc */
        initialize: function () {
            this._super();
            var initPrams = reviewStorageService.getInitParamsFromUrl();
            this.currentPage(initPrams.pageState.number);

            this.reviewStorageService = reviewStorageService;
            reviewStorageService.getFilterTotalReviewCount().subscribe(this.onTotalReviewCountChange.bind(this));
            reviewStorageService.setPageSize(this.pageSize);
            reviewStorageService.getPageState().subscribe(this.onPageStateChange.bind(this));
        },

        /**
         * Update pagination buttons
         *
         * @param newCount
         */
        onTotalReviewCountChange: function (newCount) {
            this.pages(this.generatePageLinks(newCount, this.pageSize, this.currentPage()));
            this.seeAllLabel(prReviewUtils.applyTextToTranslate(this.seeAllText, [newCount]));
        },

        /**
         * Update pagination buttons and active page
         *
         * @param nextPageState
         */
        onPageStateChange: function (nextPageState) {
            this.currentPage(nextPageState.number);
            this.pages(this.generatePageLinks(reviewStorageService.getFilterTotalReviewCount()(), this.pageSize, this.currentPage()));
        },

        /**
         * @param reviewCount
         * @param pageSize
         * @param currentPage
         * @return {[]}
         */
        generatePageLinks: function (reviewCount, pageSize, currentPage) {
            var links = [];
            var pageCount = Math.ceil(reviewCount / pageSize);
            var i = 1;

            if (pageCount > 9) {
                var canRender = false;
                var label;
                for (; i <= pageCount; i++) {
                    canRender = i === 1 || i === pageCount;
                    label = i;

                    if (! canRender && (i >= currentPage - 3 && i <= currentPage + 3)) {
                        canRender = true;
                        if (i === currentPage - 3 || i === currentPage + 3) {
                            label = '...';
                        }
                    }

                    if (canRender) {
                        links.push({
                            label: label,
                            title: 'Page ' + i,
                            number: i
                        });
                    }
                }
            } else if (pageCount > 1) {
                for (; i <= pageCount; i++) {
                    links.push({
                        label: i,
                        title: 'Page ' + i,
                        number: i
                    });
                }
            }

            return links;
        },

        /**
         * @param pageNumber
         * @return {[]}
         */
        choosePage: function (pageNumber) {
            if (this.currentPage() !== +pageNumber) {
                reviewStorageService.setPageNumber(pageNumber);
                this.goToTop();
            }
        },

        /**
         * Scroll to top of review list
         */
        goToTop: function () {
            $(document.body).animate({
                scrollTop: $('#customer-reviews-box').position().top
            }, 500);
        },

        /**
         * Redirect to separate page
         */
        goToSeeAllPage: function () {
            window.location.href = this.seeAllUrl;
        },
    });
});
