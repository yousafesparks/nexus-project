/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'ko',
    'uiComponent',
    'jquery',
    'mage/translate'
], function (ko, Component, $) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Plumrocket_AdvancedReviewAndReminder/social-buttons.html'
        },

        canShowBlock: ko.observable(false),
        hasHiddenButtons: false,

        getButtonLiClass: function (type, visible) {
            return 'pslogin-button ' + type + ' ' + (visible ? 'pslogin-visible' : 'pslogin-hidden');
        },

        getButtonSpanClass: function (imageLogin, imageIcon) {
            return 'pslogin-button-auto'
                + ' ' + (imageLogin ? 'pslogin-has-button-image' : '')
                + ' ' + (imageIcon ? 'pslogin-has-button-icon-image' : '');
        },

        afterRenderButton: function (visible) {
            if (! visible) {
                this.hasHiddenButtons = true;
            }
        },

        afterRender: function () {
            if (this.canShow) {
                this.canShowBlock(true);
            }

            if (typeof window.showFullButtonsAfterViewMore === 'undefined') {
                window.showFullButtonsAfterViewMore = false;
            }
            this.initSocialButtonsClickListener();
        },

        initSocialButtonsClickListener: function () {
            $(document).on('click', '.arar-pslogin-button-click', function () {
                var $this = $(this);
                window.advancedrar.submitAndLoginWithSocial(
                    $this.data('href'),
                    $this.data('width'),
                    $this.data('height')
                );
                return false;
            });
        },
    });
});
