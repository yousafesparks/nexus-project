/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */
define([
    'underscore',
    'ko',
    'uiComponent',
    'jquery',
    'Magento_Ui/js/modal/modal',
    'prReviewStorageService',
    'prReviewUtils',
    'mage/translate'
], function (_, ko, Component, $, modal, reviewStorageService, prReviewUtils) {
    'use strict';

    /** @const */ var TYPE_LIST = 'list';
    /** @const */ var TYPE_DETAIL_VIEW = 'detail_view';

    return Component.extend({
        defaults: {
            template: 'Plumrocket_AdvancedReviewAndReminder/review-media-popup'
        },

        review: ko.observable({}),
        largeImageId: ko.observable(),
        largeMediaType: ko.observable(''),
        currentTemplateType: ko.observable(TYPE_DETAIL_VIEW),
        galleryImages: ko.observable([]),
        reviewLoadStatus: ko.observable('loading'),
        galleryLoadStatus: ko.observable('loading'),
        imagePlaceholder: ko.observable(''),
        showBackButton: ko.observable(true),
        showNextButton: ko.observable(true),

        /**
         * Stop video after popup closed
         */
        isOpen: ko.observable(false),

        /**
         * Popup element will be set after "initPopup"
         */
        popup: null,

        /**
         * Open popup from
         */
        imageIsOpenedFrom: TYPE_DETAIL_VIEW,

        initialize: function () {
            this._super();
            this.isMobile = prReviewUtils.isMobile();
            this.loadMediaList = _.once(this.loadMediaList);
        },

        initPopup: function () {
            var $self = this;
            var options = {
                responsive: true,
                modalAction: '',
                title: this.getPopupTitle(),
                buttons: [],
                modalClass: 'prar-gallery-wrp',
                opened: function () {
                    $self.isOpen(true);
                },
                closed: function () {
                    $self.currentTemplateType(TYPE_DETAIL_VIEW);
                    this.imageIsOpenedFrom = TYPE_DETAIL_VIEW;
                    $self.isOpen(false);
                },
            };

            this.popup = $('#arrar-popup');
            modal(options, this.popup);
            this.initChangeImgListener();
            $('#customer-reviews-box, .owl-carousel, .prar-popup-media').on('click', function (event) {
                var target = event.target;

                if ('true' === target.dataset.popupShow) {
                    $self.imageIsOpenedFrom = TYPE_LIST;
                    $self.showGallery();
                    return;
                }

                if (target.classList.contains('prar-review-item-photo') && typeof target.dataset.reviewId != 'undefined') {
                    if (target.classList.contains('prar-review-from-gallery')) {
                        $self.imageIsOpenedFrom = TYPE_LIST;
                    } else {
                        $self.imageIsOpenedFrom = TYPE_DETAIL_VIEW;
                    }
                    reviewStorageService.get(target.dataset.reviewId, $self.initImageOnReview.bind($self, target.dataset),  $self.reviewLoadStatus);

                    if (typeof target.dataset.reviewVideoId != 'undefined') {
                        $self.largeMediaType('video');
                    } else {
                        $self.largeMediaType('image');

                        if ('loading' === $self.reviewLoadStatus()) {
                            $self.imagePlaceholder(target.dataset.src);
                            $self.largeImageId(false);
                        }
                    }

                    $self.showPopup();
                    $self.title = $('.prar-gallery-wrp').find('[data-role="title"]');
                    $self.title.addClass('prar-view');
                    $self.title.on('click', function () {
                        $self.showGallery();
                    }.bind($self));
                }
            });
        },

        initImageOnReview: function (dataset, review) {
            this.review(review);

            if (dataset.reviewVideoId === undefined) {
                var image = new Image();
                this.imagePlaceholder(dataset.src);
                this.largeImageId(false);
                image.src = review.attachImages[dataset.reviewImgId].large;

                $(image).on('load', function () {
                    this.largeImageId(dataset.reviewImgId);
                }.bind(this));
            }
            this.showSlideButtons(+dataset.reviewImgId, review.attachImages, dataset.reviewVideoId);
        },

        showSlideButtons: function (currentImageOrderNumber, attachImages, reviewVideoId) {
            var showBackButton = true;
            var showNextButton = true;

            if (typeof reviewVideoId != 'undefined') {
                this.showBackButton(false);
                this.showNextButton(false);
                return;
            }

            if (this.imageIsOpenedFrom === TYPE_DETAIL_VIEW) {
                if (currentImageOrderNumber === 0) {
                    showBackButton = false;
                }

                if (attachImages.length === currentImageOrderNumber + 1) {
                    showNextButton = false;
                }
            }

            if (this.imageIsOpenedFrom === TYPE_LIST) {
                var imageIndex = this.findImageIndexInGallery(this.review().id, +currentImageOrderNumber);

                if (-1 !== imageIndex && 0 === imageIndex) {
                    showBackButton = false;
                }

                if (-1 !== imageIndex && this.galleryImages().length === imageIndex + 1) {
                    showNextButton = false;
                }
            }

            this.showBackButton(showBackButton);
            this.showNextButton(showNextButton);
        },

        findImageIndexInGallery: function (reviewId, imageOrderNumber) {
            if (! this.galleryImages().length) {
                this.loadMediaList();
            }

            return this.galleryImages().findIndex(function (image) {
                return image.reviewId === reviewId && image.orderNumber === imageOrderNumber;
            });
        },

        showPopup: function () {
            this.popup.modal('openModal');
        },

        /**
         * Create listener that would change large image after click on thumbnail
         */
        initChangeImgListener: function () {
            var $self = this;

            $('#arrar-popup').on('click', function (event) {
                var target = event.target;

                if (typeof target.dataset.reviewId != 'undefined') {
                    if ($self.currentTemplateType() === TYPE_LIST) {
                        $self.imageIsOpenedFrom = TYPE_LIST;
                    }
                    $self.changeLargeImag(
                        target.dataset.reviewId,
                        target.dataset.src,
                        target.dataset.reviewImgId,
                        target.dataset.reviewVideoId
                    );
                }

                var actionNames = ['back', 'next'];

                if (typeof target.dataset.action != 'undefined' && actionNames.indexOf(target.dataset.action) !== -1) {
                    var currentImageOrderNumber = +$self.largeImageId();
                    var reviewId = $self.review().id;
                    if ($self.imageIsOpenedFrom === TYPE_DETAIL_VIEW) {
                        switch (target.dataset.action) {
                            case 'back':
                                currentImageOrderNumber --;
                                break;
                            case 'next':
                                currentImageOrderNumber ++;
                                break;
                        }
                    }

                    if ($self.imageIsOpenedFrom === TYPE_LIST) {
                        var imageIndex = $self.findImageIndexInGallery($self.review().id, +currentImageOrderNumber);

                        switch (target.dataset.action) {
                            case 'back':
                                if (0 !== currentImageOrderNumber) {
                                    currentImageOrderNumber --;
                                } else {
                                    reviewId = $self.galleryImages()[imageIndex - 1].reviewId;
                                    currentImageOrderNumber = $self.galleryImages()[imageIndex - 1].orderNumber;
                                }
                                break;
                            case 'next':
                                reviewId = $self.galleryImages()[imageIndex + 1].reviewId;
                                currentImageOrderNumber = $self.galleryImages()[imageIndex + 1].orderNumber;
                                break;
                        }
                    }

                    $self.changeLargeImag(
                        reviewId,
                        null,
                        currentImageOrderNumber,
                        target.dataset.reviewVideoId
                    );
                }
            });
        },

        /**
         * @param {number|string} reviewId
         * @param {string|null}   src
         * @param {number|string} reviewImgId
         * @param {number|string} reviewVideoId
         */
        changeLargeImag: function (reviewId, src, reviewImgId, reviewVideoId) {
            var $self = this;
            if (typeof reviewId != 'undefined') {
                if (! $self.review || $self.review().id !== +reviewId) {
                    reviewStorageService.get(reviewId, $self.review, $self.reviewLoadStatus);
                }

                $self.showSlideButtons(reviewImgId, $self.review().attachImages, reviewVideoId);
                if (typeof reviewVideoId != 'undefined') {
                    $self.largeMediaType('video');
                } else {
                    $self.largeMediaType('image');
                    $self.largeImageId(reviewImgId);
                    $self.imagePlaceholder(src ? src : $self.review().attachImages[reviewImgId]['large']);
                }

                $self.currentTemplateType(TYPE_DETAIL_VIEW);
                $self.popup.modal('setTitle', $self.getPopupTitle());
                if ($self.title) {
                    $self.title.addClass('prar-view');
                }
            }
        },


        /**
         * Show popup with all media
         */
        showGallery: function () {
            this.loadMediaList();
            this.currentTemplateType(TYPE_LIST);
            this.showPopup();
            this.popup.modal('setTitle', $.mage.__('Customer Images'));
            if (this.title) {
                this.title.removeClass('prar-view');
            }
        },

        loadMediaList: function () {
            var $self = this;
            $.ajax({
                url: this.configuration.sourceUrl,
                method : 'GET',
                dataType : 'json',
                showLoader: false,
                success: function (data) {
                    $self.galleryImages(data.items);
                    $self.galleryLoadStatus('done');
                }
            });
        },

        isActiveImage: function (imageData) {
            return 'image' === this.largeMediaType()
                && this.review().attachImages[this.largeImageId()]
                && (this.review().attachImages[this.largeImageId()].large === imageData.large);
        },

        isActiveVideo: function () {
            return 'video' === this.largeMediaType();
        },

        getPopupTitle: function () {
            return $.mage.__('View Image Gallery');
        }
    });
});
