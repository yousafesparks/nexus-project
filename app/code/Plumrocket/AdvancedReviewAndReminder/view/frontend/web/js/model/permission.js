/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'ko',
    'underscore',
    'jquery',
    'prReviewUtils'
], function (ko, _, $, prReviewUtils) {
    'use strict';

    var prArarCanWriteReview = ko.observable(false);
    var prArarNeedLoginForWrite = ko.observable(true);
    var prArarReasonHtml = ko.observable('');

    var PR_ARAR_REASON_CANNOT_WRITE = 1;
    var PR_ARAR_REASON_ONLY_BUYER = 2;
    var PR_ARAR_REASON_ONLY_REGISTERED_BUYERS = 4;

    return {
        customerGroup: false,

        /**
         * @type {{
         *  guest: integer,
         *  customer: integer,
         *  customerGroups: string,
         *  isAllowAllGroups: boolean,
         *  permissionUrl: string,
         *  reasons: []
         * }}
         */
        prArarConfig: {},

        /**
         * @type {boolean}
         */
        initCalculate: false,

        /**
         *
         * @param {integer} customerGroup
         * @param {{}} config
         * @return {this}
         */
        setConfiguration: function (customerGroup, config) {
            this.customerGroup = customerGroup;
            this.prArarConfig = config;
            return this;
        },

        isGuest: function () {
            return 0 === this.customerGroup;
        },

        /**
         * @param {{canWriteReview, needLoginForWrite, reasonHtml}} component
         */
        bindInfo: function (component) {
            if (this.canInitCalculate()) {
                this.initCalculate = true;
                this.calculate(prArarCanWriteReview, prArarNeedLoginForWrite, prArarReasonHtml);
            }

            component.canWriteReview = prArarCanWriteReview;
            component.needLoginForWrite = prArarNeedLoginForWrite;
            component.reasonHtml = prArarReasonHtml;
        },

        /**
         * @return {boolean}
         */
        canInitCalculate: function () {
            return ! this.initCalculate && false !== this.customerGroup && _.size(this.prArarConfig);
        },

        /**
         * @param {ko.observable|function} canWriteReview
         * @param {ko.observable|function} needLoginForWrite
         * @param {ko.observable|function} reasonHtml
         */
        calculate: function (canWriteReview, needLoginForWrite, reasonHtml) {
            var permission = {
                allow: true,
                reason: '',
            };

            if (! this.isGuest() && ! this.customerInAllowedGroup()) {
                permission.allow = false;
                permission.reason = PR_ARAR_REASON_CANNOT_WRITE
            } else {
                permission = this.allowWriteReview(canWriteReview, needLoginForWrite, reasonHtml);
            }

            if (Boolean(permission.allow)) {
                canWriteReview(permission.allow);
            }

            if ('number' === typeof permission.reason && $.isNumeric(permission.reason)) {
                reasonHtml(this.getReasonHtml(permission.reason));
            }
        },

        /**
         * @return {*}
         */
        customerInAllowedGroup: function () {
            if (this.isGuest()) {
                return false;
            }

            return -1 !== _.indexOf(this.prArarConfig.customerGroups.split(','), this.customerGroup);
        },

        /**
         * @param {ko.observable|function} canWriteReview
         * @param {ko.observable|function} needLoginForWrite
         * @param {ko.observable|function} reasonHtml
         * @return {{allow, reason}}
         */
        allowWriteReview: function (canWriteReview, needLoginForWrite, reasonHtml) {

            if (this.isGuest()) {
                return this.guestValidation(canWriteReview, needLoginForWrite, reasonHtml);
            } else {
                return this.customerValidation(canWriteReview, needLoginForWrite, reasonHtml);
            }
        },

        /**
         * @param {ko.observable|function} canWriteReview
         * @param {ko.observable|function} needLoginForWrite
         * @param {ko.observable|function} reasonHtml
         * @return {*}
         */
        guestValidation: function (canWriteReview, needLoginForWrite, reasonHtml) {
            var permission = {
                allow: null,
                reason: '',
            };

            switch (this.prArarConfig.guest) {
                case 0:
                    permission.allow = true;
                    needLoginForWrite(true);
                    break;
                case 3:
                    if (0 === this.prArarConfig.customer && this.prArarConfig.isAllowAllGroups) {
                        permission.allow = true;
                        needLoginForWrite(false);
                    } else if (2 === this.prArarConfig.customer && this.prArarConfig.isAllowAllGroups) {
                        permission.allow = false;
                        permission.reason = PR_ARAR_REASON_ONLY_REGISTERED_BUYERS;
                        needLoginForWrite(true);
                    } else {
                        permission.allow = false;
                        permission.reason = PR_ARAR_REASON_CANNOT_WRITE;
                        needLoginForWrite(true);
                    }
                    break;
                case 1:
                    this.checkByeProduct(canWriteReview, needLoginForWrite, reasonHtml);
                    break;
            }

            return permission;
        },

        /**
         * @param {ko.observable|function} canWriteReview
         * @param {ko.observable|function} needLoginForWrite
         * @param {ko.observable|function} reasonHtml
         * @return {*}
         */
        customerValidation: function (canWriteReview, needLoginForWrite, reasonHtml) {
            var permission = {
                allow: null,
                reason: '',
            };

            switch (this.prArarConfig.customer) {
                case 0:
                    permission.allow = true;
                    needLoginForWrite(false);
                    break;
                case 3:
                    permission.allow = false;
                    needLoginForWrite(false);
                    permission.reason = PR_ARAR_REASON_CANNOT_WRITE;
                    break;
                case 1:
                    this.checkByeProduct(canWriteReview, needLoginForWrite, reasonHtml);
                    break;
            }

            return permission;
        },

        /**
         * @param {ko.observable|function} canWriteReview
         * @param {ko.observable|function} needLoginForWrite
         * @param {ko.observable|function} reasonHtml
         */
        checkByeProduct: function (canWriteReview, needLoginForWrite, reasonHtml) {
            $.get(this.prArarConfig.permissionUrl, this.getGuestSecretParams(), function (data) {
                canWriteReview(data.allowed);
                if (! data.allowed) {
                    needLoginForWrite(true);
                    reasonHtml(data.reasonHtml);
                }
            }).fail(function () {
                canWriteReview(false);
                reasonHtml('Something went wrong');
            })
        },

        getGuestSecretParams: function () {
            var guestSecret = prReviewUtils.getUrlParameter('guest_sec');
            return guestSecret ? {guest_sec: guestSecret} : {};
        },

        /**
         * @param {integer} reason
         * @return {string}
         */
        getReasonHtml: function (reason) {
            var reasonHtml = this.prArarConfig.reasons[reason];

            if (! reasonHtml) {
                console.error('Advanced Review And Reminder: wrong reason id passed - "' + reason + '"');
                return '';
            }

            return reasonHtml;
        }
    };
});
