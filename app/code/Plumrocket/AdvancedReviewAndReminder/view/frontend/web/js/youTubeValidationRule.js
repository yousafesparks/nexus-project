/*
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define(
    [
        'jquery',
        'jquery/validate',
        'mage/translate'
    ],
    function ($) {
        'use strict';
        return function () {
            $.validator.addMethod(
                'youtubevalidation',
                function (url, element) {
                    var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                    var match = url.match(regExp);
                    return match && match[2].length === 11 || url === '';
                },
                $.mage.__("This url isn`t YouTube")
            );
        }
    }
);
