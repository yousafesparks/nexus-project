/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

define([
    'ko',
    'underscore',
    'jquery',
    'prReviewUtils'
], function (ko, _, $, prReviewUtils) {
    'use strict';

    prReviewUtils.extendKnockout(ko);

    var reviewList = ko.observableArray([]);
    var filterReviewCount = ko.observable(null);
    var showingReviewNumbers = ko.observable('');
    /**
     * Contain page numbers saved after load more.
     * After selecting specific page variable will be reset
     */
    var previousReviewPages = ko.observableArray([]);
    var reviewTotalCount = ko.observable(null);

    var reviewCache = {};
    var filterState = ko.observable({});
    var sortState = ko.observable('');
    var pageState = ko.observable({
        number: 1,
        loadMore: false
    });

    var sourceUrl = ko.observable('');
    var pageSize = ko.observable(0);

    /**
     * Possible statuses are: initial, loading, loadingMore and done
     *
     * @type {observable}
     */
    var listStatus = ko.observable('initial');

    /**
     * @type {{useUrlParams: boolean}}
     */
    var storageOptions = {};

    /**
     * Used for waiting all configurations (sourceUrl, initFilters), and then make a request
     *
     * @type {{list: null}}
     */
    var queue = {
        list: null
    };

    var storageService =  {
        urlParamNames: {
            sort: 'reviewSort',
            search: 'reviewSearch',
            general: 'reviewFilter',
            rating: 'reviewRatingFilter',
            page: 'reviewPage',
        },

        /**
         * Add subscriptions to observables
         *
         * @return {storageService}
         */
        init: function () {
            this.getInitParamsFromUrl();

            filterState.subscribe(this.clientChange.bind(this, true));
            sortState.subscribe(this.clientChange.bind(this, true));
            pageState.prIgnorePokeSubscribe(this.clientChange.bind(this, false));

            sourceUrl.subscribe(this.loadListFromQueue.bind(this));
            pageSize.subscribe(this.loadListFromQueue.bind(this));

            window.advancedrar.registerPostReviewCallback(function (data) {
                if (data.success && data.updateReviewList) {
                    this.updateList();
                }
            }.bind(this));

            return this;
        },

        /**
         * @param {{sourceUrl: string}} options
         * @return {storageService}
         */
        setOptions: function (options) {
            if (options.sourceUrl) {
                sourceUrl(options.sourceUrl);
            }
            storageOptions = options;
            return this;
        },

        /**
         * @param {integer} reviewId
         * @param callback
         * @param reviewLoadStatus
         * @return {storageService}
         */
        get: function (reviewId, callback, reviewLoadStatus) {
            if (! reviewId) {
                console.warn('Invalid review id: "' + reviewId + '"');
                return this;
            }

            var review = this.getFromCache(reviewId);
            if (review) {
                callback(review);
                reviewLoadStatus('done');
            } else {
                reviewLoadStatus('loading');
                $.ajax({
                    url: sourceUrl(),
                    type: 'GET',
                    dataType: 'json',
                    data: {searchCriteria: {review: reviewId}},
                    context: $('#customer-reviews-box'),
                    success: function (data) {
                        var review = this.prepareReview(data.items[0]);
                        this.saveReview(review);
                        this.get(reviewId, callback, reviewLoadStatus);
                    }.bind(this),
                    error: $.proxy(this._onError, this),
                    showLoader: false,
                    dontHide: false
                });
            }
        },

        /**
         * Retrieve observableArray witch will be loaded by filter state
         *
         * @param {Object}  searchCriteria
         * @param {Boolean} wait
         */
        getList: function (searchCriteria, wait) {
            this.init();
            if (_.isEmpty(searchCriteria)) {
                searchCriteria = _.clone(filterState());
                searchCriteria.sort = sortState();
                searchCriteria.page = {
                    number: pageState().number,
                    loadMore: pageState().loadMore,
                    size: pageSize()
                };
            }

            if (! wait) {
                this.loadList(searchCriteria);
            }

            return reviewList;
        },

        /**
         * Used for retrieve actual data or reviews.
         * For example after post review
         */
        updateList: function () {
            reviewTotalCount(null);
            this.getList({}, false);
        },

        /**
         * Retrieve observableArray witch will be loaded by filter state
         */
        getListObservable: function () {
            return reviewList;
        },

        /**
         * Allow subscribe for showing loader or change status
         *
         * @return {observable}
         */
        getListStatus: function () {
            return listStatus;
        },

        /**
         * Retrieve observable of reviews for current filter state
         *
         * @return {ko.observable}
         */
        getPageState: function () {
            return pageState;
        },

        /**
         * Retrieve observable of pages left by load more
         *
         * @return {ko.observable}
         */
        getPreviousReviewPages: function () {
            return previousReviewPages;
        },

        /**
         * Setter for count of reviews for one page
         *
         * @return {ko.observable}
         */
        setPageSize: function (size) {
            return pageSize(size);
        },

        /**
         * Retrieve observable of current page size
         *
         * @return {ko.observable}
         */
        getPageSize: function () {
            return pageSize;
        },

        /**
         * @param {Number}              pageNumber
         * @param {(Boolean|undefined)} skipLoad
         * @param {(Boolean|undefined)} loadMore
         */
        setPageNumber: function (pageNumber, skipLoad, loadMore) {
            var nextPageState = _.clone(pageState());

            nextPageState.number = pageNumber;
            nextPageState.loadMore = !! loadMore;

            if (! _.isEqual(pageState(), nextPageState)) { // prevent fire event if state is the same
                if (loadMore && pageNumber > 1) {
                    var previousPages = previousReviewPages();
                    previousPages.push(pageNumber - 1);
                    previousReviewPages(previousPages);
                } else {
                    previousReviewPages([]);
                }

                if (skipLoad) {
                    pageState.prPoke(nextPageState);
                } else {
                    pageState(nextPageState);
                }

                this.updateUrlParam('page', pageNumber === 1 ? 0 : pageNumber);
            }
        },

        /**
         * Retrieve count of all reviews for this product
         *
         * @return {observable}
         */
        getReviewTotalCount: function () {
            return reviewTotalCount;
        },

        /**
         * Retrieve observable of reviews for current filter state
         *
         * @return {ko.observable}
         */
        getFilterTotalReviewCount: function () {
            return filterReviewCount;
        },

        /**
         * Retrieve showed reviews on page, string like "1-10"
         *
         * @return {observable}
         */
        getShowingReviewNumbers: function () {
            return showingReviewNumbers;
        },

        /**
         * Retrieve observable state, so you can add your subscription
         *
         * @return {ko.observable}
         */
        getFilterState: function () {
            return filterState;
        },

        /**
         * Set multiple filters
         *
         * @param {Object} nextFilterState
         */
        setFilterState: function (nextFilterState) {
            filterState(nextFilterState);
            return this;
        },

        /**
         * Set filter, each insert can make ajax call
         * For setting multiple filters use "setFilterState" method
         *
         * @param {String}           group - name of filter group, for example "general", "rating"
         * @param {(String|Integer)} value - name of selected filter, for example "verified", "positive"
         * @param {?Boolean}         merge - allow adding filter into existing group
         */
        setFilter: function (group, value, merge) {
            this.init();
            var nextFilterState = _.clone(filterState());
            if (! merge) {
                nextFilterState[group] = {};
            }

            nextFilterState[group][value] = 1;

            if (! _.isEqual(filterState(), nextFilterState)) { // prevent fire event if state is the same
                filterState(nextFilterState);
            }
            return this;
        },

        /**
         * Remove filter, each remove can make ajax call
         * For removing same but not all filters you can use "setFilterState" method
         *
         * @param {String} group
         * @param value
         */
        removeFilter: function (group, value) {
            this.init();
            var nextFilterState = filterState();

            if (value) {
                if (typeof nextFilterState[group] === 'undefined') {
                    nextFilterState[group] = {};
                }
                nextFilterState[group][value] = 0;
            } else {
                nextFilterState[group] = {};
            }

            filterState(nextFilterState);
            return this;
        },

        /**
         * Remove all filters
         *
         * @return {storageService}
         */
        removeAllFilters: function () {
            this.init();
            filterState({});
            return this;
        },

        /**
         * Retrieve observable state, so you can add your subscription
         *
         * @return {ko.observable}
         */
        getSortState: function () {
            return sortState;
        },

        /**
         * Set sort order
         *
         * @param {String} value
         * @return {storageService}
         */
        setSort: function (value) {
            if (sortState() !== value) {
                sortState(value);
            }
            return this;
        },

        /**
         * Remove sort, so backend can use default sort
         *
         * @return {storageService}
         */
        removeSort: function () {
            sortState('');
            return this;
        },

        /**
         * Called after client choose filter or sort
         *
         * @param resetPage
         * @return {storageService}
         */
        clientChange: function (resetPage) {
            if (resetPage) {
                this.setPageNumber(1, true);
            }
            this.getList({});
            return this;
        },

        /**
         * Perform load reviews from store
         *
         * @param searchCriteria
         */
        loadList: function (searchCriteria) {
            var listLoadingStatus = searchCriteria.page && searchCriteria.page.loadMore
                ? 'loadingMore'
                : 'loading';

            if (this.isAllConfigDefined()) {
                if (null !== reviewTotalCount() && +reviewTotalCount() === +_.size(reviewCache)) {
                    this.getListStatus()(listLoadingStatus);
                    this.localFiltering(searchCriteria);
                    this.getListStatus()('done');
                } else {
                    $.ajax({
                        url: sourceUrl(),
                        type: 'GET',
                        dataType: 'json',
                        data: {searchCriteria: searchCriteria},
                        context: $('#customer-reviews-box'),
                        success: this.saveReviews.bind(this, searchCriteria.page),
                        error: $.proxy(this._onError, this),
                        showLoader: false,
                        dontHide: false,
                        beforeSend: function () {
                            this.getListStatus()(listLoadingStatus);
                        }.bind(this),
                        complete: function () {
                            this.getListStatus()('done');
                        }.bind(this)
                    });
                }
            } else {
                queue.list = searchCriteria;
            }
        },

        /**
         * Load list by saved before searchCriteria
         */
        loadListFromQueue: function () {
            if (queue.list && this.isAllConfigDefined()) {
                if (pageSize() !== queue.list.page.size) {
                    queue.list.page.size = pageSize();
                }
                this.loadList(queue.list);
            }
        },

        /**
         * Check if all configuration exists for ajax request
         *
         * @return {*}
         */
        isAllConfigDefined: function () {
            return pageSize() && sourceUrl();
        },

        /**
         * Init observable variables for interactive property
         *
         * @param review
         * @return {*}
         */
        prepareReview: function (review) {
            review.helpfulPositive = ko.observable(review.helpfulPositive);
            review.helpfulNegative = ko.observable(review.helpfulNegative);
            review.markedAsAbuse = ko.observable(review.markedAsAbuse);
            review.vote = ko.observable(review.vote);
            return review;
        },

        /**
         * Log error message in console
         *
         * @param error
         * @private
         */
        _onError: function (error) {
            if (error.responseJSON) {
                console.warn(JSON.parse(error.responseJSON));
            } else {
                if (error.responseText) {
                    console.warn(error.responseText);
                }
            }
        },

        /**
         * @return {{filterState: {}, sortState: string, pageState: {}}}
         */
        getInitParamsFromUrl: function () {
            if (! this.initUrlPrams) {
                var initPrams = {
                    filterState: {},
                    sortState: prReviewUtils.getUrlParameter(this.urlParamNames.sort),
                    pageState: {}
                };

                var searchFilter = prReviewUtils.getUrlParameter(this.urlParamNames.search);
                if (searchFilter) {
                    initPrams.filterState.search = {};
                    initPrams.filterState.search[searchFilter] = 1;
                }

                var generalFilter = prReviewUtils.getUrlParameter(this.urlParamNames.general);
                if (generalFilter) {
                    initPrams.filterState.general = {};
                    var parts = generalFilter.split('-');
                    parts.forEach(function (value) {
                        initPrams.filterState.general[value] = 1;
                    });
                }

                var ratingFilter = prReviewUtils.getUrlParameter(this.urlParamNames.rating);
                if (ratingFilter) {
                    initPrams.filterState.rating = {};
                    initPrams.filterState.rating[ratingFilter] = 1;
                }

                var currentPage = + prReviewUtils.getUrlParameter(this.urlParamNames.page);
                initPrams.pageState.number = currentPage ? currentPage : 1;

                if (! _.isEmpty(initPrams.filterState)) {
                    this.setFilterState(initPrams.filterState);
                }
                this.setSort(initPrams.sortState);
                this.setPageNumber(initPrams.pageState.number);

                this.initUrlPrams = initPrams;
            }

            return this.initUrlPrams;
        },

        /**
         * Add/update/remove param in url
         *
         * @param {string} section
         * @param {string} value
         * @return {exports}
         */
        updateUrlParam: function (section, value) {
            if (storageOptions.useUrlParams || ! value) {
                prReviewUtils.updateUrlParam(this.urlParamNames[section], value);
            }

            return this;
        },

        /**
         * Find review in cache
         *
         * @param reviewId
         * @return {undefined}
         */
        getFromCache: function (reviewId) {
            return reviewCache['id-' + reviewId];
        },

        /**
         * @param {{}} review
         */
        saveReview: function (review) {
            if (! this.getFromCache(review.id)) {
                reviewCache['id-' + review.id] = review;
            }
        },

        /**
         * @param {({loadMore: Boolean}|undefined)} pageData
         * @param {{items: Array, count: Number, totalCount: Number}} data
         */
        saveReviews: function (pageData, data) {
            data.items.forEach(function (item) {
                this.prepareReview(item);
            }.bind(this));

            if (pageData && pageData.loadMore) {
                var items = reviewList();
                Array.prototype.push.apply(items, data.items);
                reviewList(items);
            } else {
                reviewList(data.items);
            }

            reviewTotalCount(data.totalCount);

            this.afterFilter(data);

            _.each(data.items, this.saveReview.bind(this));
        },

        /**
         * @param {{count: number}} data
         * @return {storageService}
         */
        afterFilter: function (data) {
            filterReviewCount(data.count);
            var maxPageNumber = Math.ceil(data.count / pageSize());
            if (data.count && pageState().number > maxPageNumber) {
                this.setPageNumber(maxPageNumber);
            }
            this.changeShowingReviewNumbers(data.count);

            return this;
        },

        /**
         * Create string of showing reviews numbers
         *
         * @param maxFilteringCount
         * @return {storageService}
         */
        changeShowingReviewNumbers: function (maxFilteringCount) {
            if (1 === maxFilteringCount) {
                showingReviewNumbers('1');
            } else {
                var startPage = pageState().number;
                if (previousReviewPages().length) {
                    startPage = previousReviewPages()[0];
                }

                var start = ((startPage - 1) * pageSize()) + 1;
                var end = Math.min(pageState().number * pageSize(), maxFilteringCount);

                showingReviewNumbers(start + '-' + end);
            }

            return this;
        },

        /**
         * @param {{}} searchCriteria
         */
        localFiltering: function (searchCriteria) {
            var items = _.toArray(reviewCache);
            var data = {};

            items = this.performLocalFilter(items, searchCriteria.general, searchCriteria.rating);
            items = this.performLocalSearch(items, searchCriteria.search);
            items = this.performLocalSort(items, searchCriteria.sort);
            data.count = _.size(items);
            items = this.performLocalPagination(items, searchCriteria.page);

            reviewList(items);

            this.afterFilter(data);
        },

        /**
         * @param {Array} items
         * @param {Object} general
         * @param {Object} rating
         */
        performLocalFilter: function (items, general, rating) {
            if (rating) {
                var ratingFilter = _.findKey(rating, function (value) {
                    return value;
                });
                switch (ratingFilter) {
                    case 'positive':
                        items = items.filter(function (review) {
                            return review.ratingPercent >= 80;
                        });
                        break;
                    case 'critical':
                        items = items.filter(function (review) {
                            return review.ratingPercent < 80;
                        });
                        break;
                    case undefined:
                        // don't filter by empty rating
                        break;
                    default:
                        items = items.filter(function (review) {
                            return review.rating === +ratingFilter;
                        });
                }
            }

            if (general) {
                if (general.verified) {
                    items = items.filter(function (review) {
                        return review.verified.enable;
                    });
                }
                if (general.with_media) {
                    items = items.filter(function (review) {
                        return review.attachImages;
                    });
                }
            }

            return items;
        },

        /**
         * @param {Array} items
         * @param {Object} search
         */
        performLocalSearch: function (items, search) {
            var searchFilter = _.findKey(search, function (value) {
                return value;
            });

            if (searchFilter) {
                var fieldsForSearch = ['title', 'detail', 'cons', 'pros'];

                items = items.filter(function (review) {
                    return _.some(fieldsForSearch, function (field) {
                        return review[field] && review[field].indexOf(searchFilter) !== -1
                    });
                });
            }

            return items;
        },

        /**
         * @param {Array} items
         * @param {String} sortParam
         */
        performLocalSort: function (items, sortParam) {
            switch (sortParam) {
                case '':
                    items.sort(function (a, b) {
                        return b.createAtTime - a.createAtTime;
                    });
                    break;
                case 'oldest':
                    items.sort(function (a, b) {
                        return a.createAtTime - b.createAtTime;
                    });
                    break;
                case 'helpful':
                    items.sort(function (a, b) {
                        return (b.helpfulPositive() - b.helpfulNegative()) - (a.helpfulPositive() - a.helpfulNegative());
                    });
                    break;
                case 'high_rating':
                    items.sort(function (a, b) {
                        return b.ratingPercent - a.ratingPercent;
                    });
                    break;
                case 'low_rating':
                    items.sort(function (a, b) {
                        return a.ratingPercent - b.ratingPercent;
                    });
                    break;
                default:
                    console.warn('Unknown sort order');
            }

            return items;
        },

        /**
         * @param {Array} items
         * @param {Object} page
         */
        performLocalPagination: function (items, page) {
            var currentNumber = page.number;
            /** first number might be less then current page if "load more" was used */
            var theLittlestShowedPageNumber = page.number;

            if (page.number > 1) {
                var maxPageNumber = Math.ceil(reviewTotalCount() / page.size);
                currentNumber = Math.min(maxPageNumber, currentNumber);

                if (previousReviewPages().length) {
                    theLittlestShowedPageNumber = Math.min(previousReviewPages()[0], theLittlestShowedPageNumber)
                }
            }

            var begin = (theLittlestShowedPageNumber - 1) * page.size;
            var end = currentNumber * page.size;

            return items.slice(begin, end);
        },

        /**
         * Load reviews and append
         */
        loadMore: function () {
            this.setPageNumber(pageState().number + 1, false, true);
        },
    };

    storageService.init = _.once(storageService.init);

    return storageService.init();
});
