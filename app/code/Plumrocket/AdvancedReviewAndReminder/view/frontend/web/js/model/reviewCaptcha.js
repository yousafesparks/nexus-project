/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */
define(
    [
        'jquery',
        'Magento_Captcha/js/view/checkout/defaultCaptcha',
        'Magento_Captcha/js/model/captchaList',
    ],
    function (
        $,
        defaultCaptcha,
        captchaList
    ) {
        'use strict';

        return defaultCaptcha.extend({
            /** @inheritdoc */
            initialize: function () {
                var self = this,
                    currentCaptcha;

                this._super();
                currentCaptcha = captchaList.getCaptchaByFormId(this.formId);

                if (currentCaptcha != null) {
                    currentCaptcha.setIsVisible(true);
                    this.setCurrentCaptcha(currentCaptcha);
                    self.refresh();

                    window.advancedrar.registerPostReviewCallback(function (data) {
                        self.refresh();
                    });
                }
            }
        });
    }
);
