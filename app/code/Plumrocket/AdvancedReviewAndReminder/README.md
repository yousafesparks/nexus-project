Module dependencies:
----------
- Base
- Token (auto login, post review)
- AmpEmailApi (we extend some classes for integration)

Configuration
----------
Option: 
* Which Guest can Write Review - *replaces mage option `Allow Guests to Write Reviews`*

* Which Registered Customer can Write Review - *define permissions for write review ([see details](#customer-in-allowed-group))*
* Which Guest can Write Review - *permissions for guest, may depend on customer permissions ([see details](#guest-simple-configuration))*

Notes
----------
##### Form in email:
Support **HTML** form in email clients
- Gmail(Chrome)
- IOS email app
- Yahoo(Chrome)
- ~~Gmail app for android~~
- ~~Outlook(Chrome)~~
- ~~Outlook 2010 for windows~~

##### Edit Review Page
some fields can hide if they disabled in configuration, for example: cons/pros

##### Verified Purchase
- Only for customers
- For all order state except NEW and CANCELED
- Auto detect at create review

The delivery time is formed based on the product type.
To be more specific, if you are getting the type $product->getIsVirtual() , then the time counted from the invoice creation is started.
Otherwise, the time from the shipment creation is launched.
The support for partial invoice and shipment is implemented.

##### Review list
- Review search only for desktop
- if client saw all the reviews without reload page sorting, filtering and pagination will make on client side (**prReviewStorageService.localFiltering**)

Permission for write review
----------

\* Italic font style used for indicate messages

###### Customer (in allowed group)

|                        | Any           | Only who bought the product                       | Registered customers cannot write review |
| :---                   | :---:         | :---:                                             | :---:                                    |
| **bought product**     | Submit Button | Submit Button                                     | *You can not write a review*             |
| **not bought product** | Submit Button | *Only users who bought product can write review.* | *You can not write a review*             |

###### Guest (simple configuration)

|                               | Any           | Only who bought the product                       |
| :---                          | :---:         | :---:                                             |
| **with key (bought product)** | Submit Button | Submit Button                                     |
| **without key**               | Submit Button | *Only users who bought product can write review.* |

###### Guest depend on customer config (when selected "Guests cannot write review")

| Customer Configuration  | Any                          | Only who bought the product                                           | Registered customers cannot write review |
| :---                    | :---:                        | :---:                                                                 | :---:                                    |
| **all groups allowed**  | Login and Submit Button      | *Only registered users who purchased the product can write a review.* | *You can not write a review*             |
| **some groups allowed** | *You can not write a review* | *You can not write a review*                                          | *You can not write a review*             |

Customization
----------

###Review list

#####Update filter, sort, pagination
Exists possibility to change filter through javascript, for that you can use **prReviewStorageService**:
- setFilter
- setFilterState - _change some filters and than make ajax request_
- removeFilter
- removeAllFilters
- setSort
- removeSort
- setPageNumber
- setPageSize

###Review form
You can set if form have to be closed or opened after page loaded,
for that use **jsLayout** for block **product.review.form**
```
<argument name="jsLayout" xsi:type="array">
    <item name="components" xsi:type="array">
        <item name="pr-review-form" xsi:type="array">
            <item name="configuration" xsi:type="array">
                <item name="open" xsi:type="boolean">true</item>
            </item>
        </item>
    </item>
</argument>
```

You can hide/show form by
* function: `window.prArarIsVisibleForm` ( bool **value** )
* ko.observable: `window.prArarIsVisibleFormObservable` ( bool **value** )

Develop
----------

Add this to index.php for sending review reminders many times without changing status or create next reminders
`$_ENV['CONFIG__DEFAULT__ADVANCEDRAR__DEVELOPER__IMMUTABLE_REMINDER_SEND'] = true;`
