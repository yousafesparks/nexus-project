<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Reminder\Unsubscribe;

use Magento\Framework\Controller\ResultFactory;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface
     */
    protected $reasonRepository;

    /**
     * Edit constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository
    ) {
        parent::__construct($context);
        $this->dataPersistor = $dataPersistor;
        $this->reasonRepository = $reasonRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('reason_id');

        try {
            $reason = $this->reasonRepository->getById($id);
            $this->dataPersistor->set('unsubscribe_reason', $reason);
            $title = __('Edit Reason');
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $title = __('New Reason');
        }

        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $result->getConfig()->getTitle()->prepend($title);
        $result->addBreadcrumb($title, $title);

        return $result;
    }
}
