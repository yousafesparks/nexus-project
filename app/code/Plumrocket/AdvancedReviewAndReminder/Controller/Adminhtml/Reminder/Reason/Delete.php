<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Reminder\Reason;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface
     */
    private $reasonRepository;

    /**
     * Save constructor.
     *
     * @param Action\Context $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository
     */
    public function __construct(
        Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository
    ) {
        parent::__construct($context);
        $this->reasonRepository = $reasonRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('reason_id');
        try {
            $this->reasonRepository->deleteById($id);
        } catch (CouldNotDeleteException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath('*/*/edit', ['reason_id' => $id]);
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('We can\'t find a reason to delete.'));
            return $resultRedirect->setPath('*/*/edit', ['reason_id' => $id]);
        }

        $this->messageManager->addSuccessMessage(__('You deleted the reason.'));
        return $resultRedirect->setPath('*/*/');
    }
}
