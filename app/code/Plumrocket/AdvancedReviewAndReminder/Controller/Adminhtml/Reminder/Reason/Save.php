<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Reminder\Reason;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\NoSuchEntityException;
use Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\UnsubscribeReason;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface
     */
    private $reasonRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterfaceFactory
     */
    private $reasonFactory;

    /**
     * Save constructor.
     *
     * @param Action\Context $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterfaceFactory $reasonFactory
     */
    public function __construct(
        Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository,
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\UnsubscribeReasonInterfaceFactory $reasonFactory
    ) {
        parent::__construct($context);
        $this->reasonRepository = $reasonRepository;
        $this->reasonFactory = $reasonFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('reason_id');
        $data = $this->getRequest()->getPostValue();

        try {
            if (! $id) {
                $data[UnsubscribeReason::PRIMARY_FIELD] = null;
                $model = $this->reasonFactory->create();
            } else {
                $model = $this->reasonRepository->getById($id);
            }

            $model->setData($data);
            $this->reasonRepository->save($model);
            $id = $model->getId();
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the reason.'));
        }

        if ($id) {
            $resultRedirect->setPath('advancedrar/reminder_reason/edit', ['id' => $id]);
        } else {
            $resultRedirect->setPath('advancedrar/reminder_reason/index');
        }

        return $resultRedirect;
    }
}
