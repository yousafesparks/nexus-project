<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Reviews;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect as RedirectResult;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Controller\ResultFactory;
use Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface;

/**
 * @since 2.0.1
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    private $advancedReviewRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * MassDelete constructor.
     *
     * @param \Magento\Backend\App\Action\Context                                         $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                                $searchCriteriaBuilder
     */
    public function __construct(
        Context $context,
        AdvancedReviewRepositoryInterface $advancedReviewRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->advancedReviewRepository = $advancedReviewRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() : RedirectResult
    {
        $selectedReviews = $this->getRequest()->getParam('selected', []);

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('main_table.review_id', $selectedReviews, 'in')
            ->create();

        $reviewsList = $this->advancedReviewRepository->getList($searchCriteria);

        $deletedCount = 0;
        foreach ($reviewsList->getItems() as $advancedReview) {
            try {
                $this->advancedReviewRepository->delete($advancedReview);
                $deletedCount++;
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e);
            }
        }

        if ($deletedCount) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $deletedCount)
            );
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/index');
    }
}
