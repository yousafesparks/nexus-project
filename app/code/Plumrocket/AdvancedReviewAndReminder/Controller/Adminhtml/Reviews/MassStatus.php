<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Reviews;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect as RedirectResult;
use Magento\Framework\Controller\ResultFactory;
use Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface;

/**
 * @since 2.0.1
 */
class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    private $advancedReviewRepository;

    /**
     * AbstractIndex constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository
     */
    public function __construct(
        Context $context,
        AdvancedReviewRepositoryInterface $advancedReviewRepository
    ) {
        $this->advancedReviewRepository = $advancedReviewRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() : RedirectResult
    {
        $status = (string) $this->getRequest()->getParam('status');
        $selectedReviews = $this->getRequest()->getParam('selected', []);

        $changesCount = 0;
        foreach ($selectedReviews as $id) {
            try {
                $review = $this->advancedReviewRepository->get($id);
                $review->setStatusId($status);
                $this->advancedReviewRepository->save($review);

                $changesCount++;
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e);
            }
        }

        if ($changesCount) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been updated.', $changesCount)
            );
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/index');
    }
}
