<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Docs;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Plumrocket\AdvancedReviewAndReminder\Helper\Data as HelperData;

class Index extends Action
{
    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setUrl(HelperData::WIKI_LINK);
    }
}
