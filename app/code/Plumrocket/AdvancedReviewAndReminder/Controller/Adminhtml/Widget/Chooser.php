<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Widget;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Layout;
use Magento\Framework\View\LayoutFactory;
use Plumrocket\AdvancedReviewAndReminder\Block\Adminhtml\Widget\ReviewChooser;

/**
 * @since 2.2.0
 */
class Chooser extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Review::reviews_all';

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * Chooser constructor.
     *
     * @param \Magento\Backend\App\Action\Context   $context
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     */
    public function __construct(
        Context $context,
        LayoutFactory $layoutFactory
    ) {
        parent::__construct($context);
        $this->layoutFactory = $layoutFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $uniqId = $this->getRequest()->getParam('uniq_id');
        /** @var Layout $layout */
        $layout = $this->layoutFactory->create();

        /** @var ReviewChooser $reviewsGrid */
        $reviewsGrid = $layout->createBlock(ReviewChooser::class, '', [
                'data' => [
                    'id' => $uniqId,
                ]
            ]);

        $html = $reviewsGrid->toHtml();
        /** @var Raw $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $result->setContents($html);

        return $result;
    }
}
