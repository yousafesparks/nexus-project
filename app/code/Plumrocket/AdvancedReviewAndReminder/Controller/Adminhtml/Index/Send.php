<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Index;

use Plumrocket\AdvancedReviewAndReminder\Model\Reminder as ArarReminder;

class Send extends \Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\AbstractIndex
{
    /**
     * Execute view action
     *
     * @return void
     */
    public function execute()
    {
        if ($ids = $this->getIds()) {
            /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Email $ararEmail */
            $ararEmail = $this->emailFactory->create();

            $reminderCollection = $this->getCollectionByIds($ids);

            foreach ($reminderCollection as $reminder) {
                $emailAddress = $this->getOrder($reminder)->getCustomerEmail();
                if ($reminder->getStatus() == ArarReminder::ADVANCEDRAR_STATUS_SENT) {
                    $this->messageManager->addErrorMessage(__('Email for %1 already sent', $emailAddress));
                } elseif ($reminder->getStatus() == ArarReminder::ADVANCEDRAR_STATUS_CANCELED) {
                    $this->messageManager->addErrorMessage(__('Can\'t send for %1. Reminder is closed', $emailAddress));
                } else {
                    if ($ararEmail->processReminder($reminder)) {
                        $this->messageManager->addSuccessMessage(__('Reminder for %1 has been sent', $emailAddress));
                    } else {
                        $this->messageManager->addErrorMessage(__('Error. Something went wrong'));
                    }
                }
            }
        }

        $this->_redirect('*/*/');
    }
}
