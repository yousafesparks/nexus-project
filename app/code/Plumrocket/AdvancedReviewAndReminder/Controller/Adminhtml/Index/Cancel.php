<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Index;

use Plumrocket\AdvancedReviewAndReminder\Model\Reminder as ArarReminder;

class Cancel extends \Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\AbstractIndex
{
    /**
     * Execute view action
     *
     * @return void
     */
    public function execute()
    {
        if ($ids = $this->getIds()) {
            $reminderCollection = $this->getCollectionByIds($ids);

            foreach ($reminderCollection as $reminder) {
                if ($reminder->getStatus() == ArarReminder::ADVANCEDRAR_STATUS_SENT) {
                    $emailAddress = $this->getOrder($reminder)->getCustomerEmail();
                    $this->messageManager->addErrorMessage(
                        __('Reminder for %1 is sended. It can not be described as canceled', $emailAddress)
                    );
                } else {
                    $reminder->setStatus(ArarReminder::ADVANCEDRAR_STATUS_CANCELED);
                }
            }

            $reminderCollection->save();
        }

        $this->_redirect('*/*/');
    }
}
