<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Index;

use Magento\Framework\UrlInterface;
use Magento\Framework\Controller\ResultFactory;

class Viewbody extends \Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\AbstractIndex
{
    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    private $customerRegistry;

    /**
     * @var \Magento\Framework\App\State
     */
    private $appState;

    /**
     * @var string
     */
    private $emulatedEmailBody = '';

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory
     */
    private $reminderFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
     */
    private $autoLoginManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * Viewbody constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Magento\Backend\App\Action\Context                                                  $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                                    $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory                             $emailFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory
     * @param \Magento\Sales\Model\OrderRepository                                                 $orderRepository
     * @param \Magento\Store\Model\StoreManagerInterface                                           $storeManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder                   $resourceReminder
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory                          $reminderFactory
     * @param \Magento\Customer\Model\CustomerRegistry                                             $customerRegistry
     * @param \Magento\Framework\App\State                                                         $appState
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface                  $autoLoginManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig                          $reminderConfig
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory $emailFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder $resourceReminder,
        \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory $reminderFactory,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Framework\App\State $appState,
        \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
    ) {
        $this->reminderFactory = $reminderFactory;
        $this->customerRegistry = $customerRegistry;
        $this->appState = $appState;
        parent::__construct(
            $context,
            $dataHelper,
            $emailFactory,
            $reminderCollectionFactory,
            $orderRepository,
            $storeManager,
            $resourceReminder
        );
        $this->autoLoginManager = $autoLoginManager;
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Reminder $reminder */
        $reminder = $this->reminderFactory->create();

        $this->resourceReminder->load($reminder, $id);

        if ($reminder->getBody()) {
            $this->getResponse()->setBody($reminder->getBody());
        } else {
            if (! $this->emulatedEmailBody) {
                try {
                    $this->appState->emulateAreaCode('frontend', [$this, 'prepareEmailBody'], [$reminder]);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $this->messageManager->addError(__('The order was deleted for this reminder'));
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                    return $resultRedirect;
                }
            }

            $this->getResponse()->setBody($this->emulatedEmailBody);
        }
    }

    public function prepareEmailBody($reminder)
    {
        $order = $this->getOrder($reminder);

        $emailModel = $this->emailFactory->create()
            ->setOrder($order)
            ->setStoreId($reminder->getStoreId())
            ->setReminder($reminder);

        $vars = $emailModel->getVars($order);

        if ($this->reminderConfig->isEnabledAutoLogin() && $order->getCustomerId()) {
            $customer = $this->customerRegistry->retrieve($order->getCustomerId());
            $vars['alogin_secret'] = $this->autoLoginManager->addSecretToUrl(
                $this->storeManager->getStore($reminder->getStoreId())->getBaseUrl(UrlInterface::URL_TYPE_LINK),
                $customer
            );
        } else {
            $vars['alogin_secret'] = $this->autoLoginManager->addSecretToUrl(
                $this->storeManager->getStore($order->getStoreId())->getBaseUrl(UrlInterface::URL_TYPE_LINK),
                null,
                $reminder
            );
        }

        $emailTemplate = $emailModel->getEmailTemplate();
        $this->emulatedEmailBody = $emailTemplate->getProcessedTemplate($vars);
    }
}
