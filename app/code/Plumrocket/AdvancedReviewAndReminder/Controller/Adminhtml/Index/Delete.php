<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Index;

class Delete extends \Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\AbstractIndex
{
    /**
     * Execute view action
     *
     * @return void
     */
    public function execute()
    {
        if ($ids = $this->getIds()) {
            $reminderCollection = $this->getCollectionByIds($ids);

            /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Reminder $reminder */
            foreach ($reminderCollection as $reminder) {
                $this->resourceReminder->delete($reminder);
            }

            $this->messageManager->addSuccessMessage(__('Reminders was successfully deleted'));
        }

        $this->_redirect('*/*/');
    }
}
