<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml;

abstract class AbstractIndex extends \Magento\Backend\App\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory
     */
    protected $emailFactory;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\Collection
     */
    protected $reminderCollectionFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder
     */
    protected $resourceReminder;

    /**
     * AbstractIndex constructor.
     *
     * phpcs:disable Generic.Files.LineLength
     * @param \Magento\Backend\App\Action\Context                                                  $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                                    $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory                             $emailFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory
     * @param \Magento\Sales\Model\OrderRepository                                                 $orderRepository
     * @param \Magento\Store\Model\StoreManagerInterface                                           $storeManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder                   $resourceReminder
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\EmailFactory $emailFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\CollectionFactory $reminderCollectionFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder $resourceReminder
    ) {
        $this->dataHelper = $dataHelper;
        $this->emailFactory = $emailFactory;
        $this->orderRepository = $orderRepository;
        $this->storeManager = $storeManager;
        parent::__construct($context);
        $this->reminderCollectionFactory = $reminderCollectionFactory;
        $this->resourceReminder = $resourceReminder;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Reminder $reminder
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function getOrder(\Plumrocket\AdvancedReviewAndReminder\Model\Reminder $reminder)
    {
        return $this->orderRepository->get($reminder->getOrderId());
    }

    /**
     * @return array|bool|mixed
     */
    protected function getIds()
    {
        if ($ids = $this->getRequest()->getParam('id')) {
            if (! is_array($ids)) {
                $ids = (array)$ids;
            }
            return $ids;
        }

        return false;
    }

    /**
     * @param array $ids
     * @return \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\Collection
     */
    protected function getCollectionByIds(array $ids)
    {
        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Advancedreview\Collection $collection */
        $collection = $this->reminderCollectionFactory->create();

        $collection->addFieldToFilter('id', ['in' => $ids]);

        return $collection;
    }
}
