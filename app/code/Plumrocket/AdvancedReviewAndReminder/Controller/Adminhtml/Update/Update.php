<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Adminhtml\Update;

use Magento\Framework\Controller\ResultFactory;

class Update extends \Magento\Backend\App\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory
     */
    private $reminderFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * Update constructor.
     *
     * @param \Magento\Backend\App\Action\Context                         $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory $reminderFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory  $orderCollectionFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory $reminderFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig
    ) {
        $this->reminderFactory = $reminderFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context);
        $this->reminderConfig = $reminderConfig;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $from   = $this->getRequest()->getParam('from');
        $to     = $this->getRequest()->getParam('to');

        $error = '';
        $response = [
            'success' => true,
        ];

        if (! $this->reminderConfig->isReviewReminderEnabled()) {
            $error = __('Email Review Reminder must be enabled');
        }

        if ($from && $to && empty($error)) {
            $from = date('Y-m-d', strtotime($from));
            $to = date('Y-m-d', strtotime($to));

            if ($from == $to) {
                $to .= ' 23:59:59';
            }

            $orders = $this->orderCollectionFactory->create()
                ->addFieldToFilter('created_at', ['gt' => $from])
                ->addFieldToFilter('created_at', ['lt' => $to]);

            foreach ($orders as $order) {
                try {
                    $this->reminderFactory->create()
                        ->createReminder($order);
                } catch (\Exception $e) {
                    $error .= "\r\n" . $e->getMessage();
                }
            }
        }

        if ('' !== $error) {
            $response =[
                'success' => false,
                'message' => $error,
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setData($response);

        return $result;
    }
}
