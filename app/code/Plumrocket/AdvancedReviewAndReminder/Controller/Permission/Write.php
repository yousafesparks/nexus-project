<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Permission;

use Magento\Framework\Exception\NoSuchEntityException;

class Write extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Permission
     */
    private $permissionHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Write constructor.
     *
     * @param \Magento\Framework\App\Action\Context                   $context
     * @param \Magento\Catalog\Api\ProductRepositoryInterface         $productRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper
    ) {
        parent::__construct($context);
        $this->permissionHelper = $permissionHelper;
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);

        $productId = $this->getRequest()->getParam('product');

        $responseData = ['allowed' => false];

        try {
            $product = $this->productRepository->getById($productId);

            $isAllowed = $this->permissionHelper->allowWriteReview(null, $product);
            $responseData['allowed'] = $isAllowed;

            if (! $isAllowed) {
                $responseData['reasonHtml'] = $this->permissionHelper->getReasonHtml();
            }
        } catch (NoSuchEntityException $e) {
            $responseData = [
                'allowed' => false,
                __('Product with id "%1" not found', $productId)
            ];
        } finally {
            $result->setData($responseData);
        }

        return $result;
    }
}
