<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller;

/**
 * Plumrocket AdvancedReviewAndReminder Router
 */
class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    private $actionFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * Router constructor.
     *
     * @param \Magento\Framework\App\ActionFactory              $actionFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper
    ) {
        $this->actionFactory = $actionFactory;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Validate and Match
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool|\Magento\Framework\App\ActionInterface
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        if (!$this->dataHelper->moduleEnabled()
            || $request->getModuleName() === 'advancedrar'
            || $request->getControllerName() === 'noroute'
            || $request->getActionName() === 'noroute'
        ) {
            return false;
        }

        if ($this->showAdvancedReview($request)) {
            $request->setModuleName('advancedrar')
                    ->setControllerName('review')
                    ->setActionName('view');

            return $this->actionFactory->create(\Magento\Framework\App\Action\Forward::class);
        }

        return false;
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    private function showAdvancedReview(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');

        return false !== mb_strpos($identifier, 'productreview');
    }
}
