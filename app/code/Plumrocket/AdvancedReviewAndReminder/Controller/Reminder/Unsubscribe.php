<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Reminder;

use Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\SubscriberStatus;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\VisitorType;

/**
 * @since 2.0.0
 */
class Unsubscribe extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\Token\Api\CustomerHashValidatorInterface
     */
    protected $tokenValidator;

    /**
     * @var \Plumrocket\Token\Api\CustomerRepositoryInterface
     */
    protected $tokenRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface
     */
    protected $reminderSubscriberRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory
     */
    protected $reminderSubscriberFactory;

    /**
     * Unsubscribe constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Plumrocket\Token\Api\CustomerHashValidatorInterface $tokenValidator
     * @param \Plumrocket\Token\Api\CustomerRepositoryInterface $tokenRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory $reminderSubscriberFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Plumrocket\Token\Api\CustomerHashValidatorInterface $tokenValidator,
        \Plumrocket\Token\Api\CustomerRepositoryInterface $tokenRepository,
        \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository,
        \Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterfaceFactory $reminderSubscriberFactory
    ) {
        parent::__construct($context);
        $this->tokenValidator = $tokenValidator;
        $this->tokenRepository = $tokenRepository;
        $this->reminderSubscriberRepository = $reminderSubscriberRepository;
        $this->reminderSubscriberFactory = $reminderSubscriberFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $tokenHash = $this->getRequest()->getParam('token', '');

        try {
            $this->tokenValidator->validate($tokenHash);
            $token = $this->tokenRepository->get($tokenHash);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            return $this->_forward('noroute');
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return $this->_forward('noroute');
        }

        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
        $email = $token->getEmail();
        $customerId = $token->getCustomerId();
        $website = $token->getAdditionalData('website_id');

        try {
            $subscriber = $this->reminderSubscriberRepository->getByEmail($email);
            $subscriber->setData(SubscriberInterface::STATUS, SubscriberStatus::UNSUBSCRIBED);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $subscriber = $this->reminderSubscriberFactory->create();
            $subscriber->setData([
                [
                    SubscriberInterface::EMAIL => $email,
                    SubscriberInterface::CUSTOMER_ID => $customerId,
                    SubscriberInterface::WEBSITE => $website,
                    SubscriberInterface::STATUS => SubscriberStatus::UNSUBSCRIBED,
                    SubscriberInterface::VISITOR_TYPE => $customerId
                        ? VisitorType::CUSTOMER
                        : VisitorType::GUEST
                ]
            ]);
        }

        $this->reminderSubscriberRepository->save($subscriber);
        $result->getLayout()->getBlock('reason.form')->addData(['token' => $tokenHash]);
        $result->getConfig()->getTitle()->set(__('You have successfully unsubscribed'));

        return $result;
    }
}
