<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Reminder;

use Magento\Framework\App\RequestInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\InputType;

class Reason extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface
     */
    protected $reminderSubscriberRepository;

    /**
     * @var UnsubscribeReasonRepositoryInterface
     */
    protected $reasonRepository;

    /**
     * @var \Plumrocket\Token\Api\CustomerRepositoryInterface
     */
    protected $tokenRepository;

    /**
     * Reason constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param UnsubscribeReasonRepositoryInterface $reasonRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository
     * @param \Plumrocket\Token\Api\CustomerRepositoryInterface $tokenRepository
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Plumrocket\AdvancedReviewAndReminder\Api\UnsubscribeReasonRepositoryInterface $reasonRepository,
        \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository,
        \Plumrocket\Token\Api\CustomerRepositoryInterface $tokenRepository
    ) {
        parent::__construct($context);
        $this->formKeyValidator = $formKeyValidator;
        $this->reminderSubscriberRepository = $reminderSubscriberRepository;
        $this->reasonRepository = $reasonRepository;
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $request = $this->getRequest();
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $reason = $request->getParam('reason');
        $tokenHash = $request->getParam('token');
        $customMessages = $request->getParam('custom', []);

        try {
            $this->validateRequest($request);
            $email = $this->tokenRepository->get($tokenHash)->getEmail();
            $subscriber = $this->reminderSubscriberRepository->getByEmail($email);
            $reason = $this->reasonRepository->getById($reason);

            if ($reason->getInputType() === InputType::TEXTAREA) {
                if (isset($customMessages[$reason->getId()])) {
                    $subscriber->setReason($customMessages[$reason->getId()]);
                } else {
                    $this->addDefaultErrorMessage($result);
                }
            }

            $subscriber->setReasonLabel($reason->getStoreLabel());
            $this->reminderSubscriberRepository->save($subscriber);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $this->addDefaultErrorMessage($result);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            $this->addDefaultErrorMessage($result);
        }

        return $result->setData([]);
    }

    public function validateRequest(RequestInterface $request)
    {
        if (! $this->formKeyValidator->validate($request)) {
            throw new \Magento\Framework\Exception\ValidatorException(
                __('Invalid Form Key. Please refresh the page.')
            );
        }

        if (! $request->getParam('reason')) {
            throw new \Magento\Framework\Exception\ValidatorException(
                __('Field reason is empty')
            );
        }
    }

    private function addDefaultErrorMessage($result)
    {
        $this->messageManager->addErrorMessage(__('Something went wrong, please try again later!'));
        return $result->setHttpResponseCode(400);
    }
}
