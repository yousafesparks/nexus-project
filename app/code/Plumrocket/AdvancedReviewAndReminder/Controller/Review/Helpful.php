<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Helpful
 */
class Helpful extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\HelpfulvoteFactory
     */
    private $helpfulvoteFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * Helpful constructor.
     *
     * @param \Magento\Framework\App\Action\Context                          $context
     * @param \Magento\Customer\Model\Session                                $customerSession
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data              $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config            $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\HelpfulvoteFactory $helpfulvoteFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\HelpfulvoteFactory $helpfulvoteFactory
    ) {
        parent::__construct($context);
        $this->helpfulvoteFactory = $helpfulvoteFactory;
        $this->customerSession = $customerSession;
        $this->dataHelper = $dataHelper;
        $this->configHelper = $configHelper;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (! $this->dataHelper->moduleEnabled()) {
            return $result->setHttpResponseCode(404);
        }

        $customerId = $this->customerSession->getCustomerId();
        if (! $customerId && ! $this->configHelper->isEnabledHelpfulForGuest()) {
            return $result->setHttpResponseCode(400)
                          ->setData(['message' => __('Guest can\'t rate helpfulness')]);
        }

        if (! $reviewId = (int) $this->getRequest()->getParam('reviewId')) {
            return $result->setHttpResponseCode(400)
                          ->setData(['message' => __('Unable to update the review. Review Id is empty')]);
        }

        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Helpfulvote $helpfulVote */
        $helpfulVote = $this->helpfulvoteFactory->create();

        $helpfulVote->setReviewId($reviewId)
            ->setCustomerId($customerId)
            ->setCurrentVote((int) $this->getRequest()->getParam('helpful'))
            ->saveHelpfulVote();

        return $result->setData(
            [
                'message' => __('Review is updated'),
                'positive'=> (int) $helpfulVote->getHelpfulPositive(),
                'negative'=> (int) $helpfulVote->getHelpfulNegative(),
                'vote' => $helpfulVote->getVote()
            ]
        );
    }
}
