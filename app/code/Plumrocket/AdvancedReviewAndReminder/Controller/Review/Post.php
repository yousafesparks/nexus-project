<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Magento\Review\Model\Review;

class Post extends \Plumrocket\AdvancedReviewAndReminder\Controller\AbstractReview
{
    /**
     * @var \Magento\Customer\Model\Url
     */
    private $customerUrl;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Permission
     */
    private $permissionHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface
     */
    private $boughtProductValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator
     */
    private $reviewArarFormValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\ReviewPrepareMediaDataInterface
     */
    private $prepareMediaData;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface
     */
    private $createReviewFilter;

    /**
     * Post constructor.
     *
     * @param \Magento\Framework\App\Action\Context                                     $context
     * @param \Magento\Framework\Registry                                               $coreRegistry
     * @param \Magento\Customer\Model\Session                                           $customerSession
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface                          $categoryRepository
     * @param \Psr\Log\LoggerInterface                                                  $logger
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                           $productRepository
     * @param \Magento\Review\Model\ReviewFactory                                       $reviewFactory
     * @param \Magento\Review\Model\RatingFactory                                       $ratingFactory
     * @param \Magento\Catalog\Model\Design                                             $catalogDesign
     * @param \Magento\Framework\Session\Generic                                        $reviewSession
     * @param \Magento\Store\Model\StoreManagerInterface                                $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator                            $formKeyValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory         $advancedreviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                         $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                       $configHelper
     * @param \Magento\Customer\Model\Url                                               $customerUrl
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Permission                   $permissionHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator          $reviewReviewArarFormValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\ReviewPrepareMediaDataInterface $prepareMediaData
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface     $createReviewFilter
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Model\Design $catalogDesign,
        \Magento\Framework\Session\Generic $reviewSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Customer\Model\Url $customerUrl,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator,
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator $reviewReviewArarFormValidator,
        \Plumrocket\AdvancedReviewAndReminder\Api\ReviewPrepareMediaDataInterface $prepareMediaData,
        \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface $createReviewFilter
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $customerSession,
            $categoryRepository,
            $logger,
            $productRepository,
            $reviewFactory,
            $ratingFactory,
            $catalogDesign,
            $reviewSession,
            $storeManager,
            $formKeyValidator,
            $advancedreviewFactory,
            $dataHelper,
            $configHelper
        );

        $this->customerUrl = $customerUrl;
        $this->permissionHelper = $permissionHelper;
        $this->boughtProductValidator = $boughtProductValidator;
        $this->reviewArarFormValidator = $reviewReviewArarFormValidator;
        $this->prepareMediaData = $prepareMediaData;
        $this->createReviewFilter = $createReviewFilter;
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        if ($this->dataHelper->moduleEnabled()) {
            return \Magento\Framework\App\Action\Action::dispatch($request);
        }

        return parent::dispatch($request);
    }

    /**
     * Create review from post data
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $rating = $this->getRequest()->getParam('ratings', []);

        $data = $this->createReviewFilter->filter($data);
        if (empty($data['nickname']) && $this->customerSession->isLoggedIn()) {
            $data['nickname'] = $this->customerSession->getCustomer()->getName();
        }

        if (! empty($data) && ($product = $this->initProduct())) {
            $customerId = (int) $this->customerSession->getCustomerId();
            $productId = (int) $product->getId();
            if (! $this->boughtProductValidator->validate($productId, $customerId)) {
                return $this->sendJsonResponse(
                    ['message' => __('Only users who bought product can write review.')]
                );
            }

            try {
                $mistakes = $this->reviewArarFormValidator->detectMistakes($data);
            } catch (\Zend_Validate_Exception $e) {
                return $this->sendJsonResponse(
                    ['message' => __('Sorry, something went wrong. Please try again later.')]
                );
            }

            if (! $mistakes) {
                $data['title'] = ! empty($data['title']) ? $data['title'] : '';

                if (! $this->permissionHelper->allowWriteReview(null, $product)) {
                    $data['rating'] = $rating;
                    $data['product'] = $product->getId();
                    $this->customerSession->setPostReviewAfterLogin($data);

                    return $this->sendJsonResponse(
                        ['redirect' => $this->customerUrl->getLoginUrl()]
                    );
                }

                $data = $this->prepareMediaData->execute($data);

                try {
                    $this->review = $this->advancedreviewFactory->create()
                        ->setData($data)
                        ->setProduct($product)
                        ->setRating($rating)
                        ->setCustomerId($customerId)
                        ->saveReview();

                    $status = $this->dataHelper->getCurrentReviewStatus();

                    $title = __('Thank you! We really appreciate your feedback!');
                    $subtitle = $status === Review::STATUS_PENDING
                        ? __('Your review has been accepted for moderation.')
                        : __('Your review has been posted.');

                    return $this->sendJsonResponse(
                        [
                            'message'  => $this->dataHelper->getDefaultStatusMessage(),
                            'title'    => $title,
                            'subtitle' => $subtitle,
                            'updateReviewList' => $status !== Review::STATUS_PENDING,
                        ],
                        true
                    );
                } catch (\Exception $e) {
                    $this->reviewSession->setFormData($data);
                    return $this->sendJsonResponse(
                        ['message' => __('Unable to post the review.') . ' ' . $e->getMessage()]
                    );
                }
            } else {
                $this->reviewSession->setFormData($data);
                if (is_array($mistakes)) {
                    $errors = [];
                    foreach ($mistakes as $errorMessage) {
                        $errors[] = __($errorMessage);
                    }

                    return $this->sendJsonResponse(
                        ['message' => $errors]
                    );
                }

                return $this->sendJsonResponse(
                    ['message' => __('Unable to post the review.')]
                );
            }
        }

        if ($redirectUrl = $this->reviewSession->getRedirectUrl(true)) {
            return $this->sendJsonResponse(
                ['message' => __('Unable to post the review.')]
            );
        }
    }
}
