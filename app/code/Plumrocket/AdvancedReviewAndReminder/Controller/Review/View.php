<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;

class View extends \Plumrocket\AdvancedReviewAndReminder\Controller\AbstractReview
{
    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $currentUrl = $this->_url->getCurrentUrl();

        $urlPath = str_replace(
            [
                $this->_url->getBaseUrl(UrlInterface::URL_TYPE_LINK),
                '/index.php',
            ],
            '',
            $currentUrl
        );
        $urlPath = explode('/', $urlPath);

        $resultForward = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
        if (! is_array($urlPath) || ! isset($urlPath[2])) {
            $resultForward->forward('noroute');
            return $resultForward;
        }

        $urlKey = $urlPath[1] . '/' . $urlPath[2];
        $advancedReview = $this->advancedreviewFactory->create()->load($urlKey, 'review_url_key');

        $review = $this->reviewFactory->create()->load($advancedReview->getReviewId());

        if (! $review->getId()
            || ! $review->isApproved()
            || ! $review->isAvailableOnStore($this->storeManager->getStore())
        ) {
            $resultForward->forward('noroute');
            return $resultForward;
        }

        $product = $this->loadProduct($review->getEntityPkValue());
        if (!$product) {
            $resultForward->forward('noroute');
            return $resultForward;
        }

        $this->coreRegistry->register('current_review', $review);
        $this->_request->setParam('id', $review->getId());

        $this->fullActionName = 'review_product_view';

        //Check if this works
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->addBodyClass('review-product-view');

        $resultPage->getConfig()->getTitle()->set(
            $this->configHelper->getReviewPageMetaTitleFormat($product, $review)
        );
        $resultPage->getConfig()->setDescription(
            $this->configHelper->getReviewPageMetaDescriptionFormat($product, $review)
        );

        return $resultPage;
    }
}
