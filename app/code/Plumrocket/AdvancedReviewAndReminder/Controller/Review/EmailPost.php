<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Plumrocket\AdvancedReviewAndReminder\Model\MagentoTwoTwo\CsrfAwareActionInterface;

/**
 * Class EmailPost
 * Creating reviews that have submitted from emails
 *
 * @since 1.2.0
 */
class EmailPost extends \Magento\Review\Controller\Product implements CsrfAwareActionInterface
{
    const ADMIN_RESOURCE = 'Plumrocket_AdvancedReviewAndReminder::index';

    /**
     * @var \Plumrocket\Token\Api\CustomerHashValidatorInterface
     */
    private $customerTokenHashValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Review
     */
    private $reviewHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Permission
     */
    private $permissionHelper;

    /**
     * @var \Magento\Customer\Model\Url
     */
    private $customerUrl;

    /**
     * @var \Plumrocket\Token\Api\CustomerRepositoryInterface
     */
    private $customerTokenRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface
     */
    private $boughtProductValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator
     */
    private $reviewArarFormValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface
     */
    private $createReviewFilter;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Permission\SaveGuestSecretKey
     */
    private $saveGuestSecretKey;

    /**
     * @param \Magento\Framework\App\Action\Context                                     $context
     * @param \Magento\Framework\Registry                                               $coreRegistry
     * @param \Magento\Customer\Model\Session                                           $customerSession
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface                          $categoryRepository
     * @param \Psr\Log\LoggerInterface                                                  $logger
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                           $productRepository
     * @param \Magento\Review\Model\ReviewFactory                                       $reviewFactory
     * @param \Magento\Review\Model\RatingFactory                                       $ratingFactory
     * @param \Magento\Catalog\Model\Design                                             $catalogDesign
     * @param \Magento\Framework\Session\Generic                                        $reviewSession
     * @param \Magento\Store\Model\StoreManagerInterface                                $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator                            $formKeyValidator
     * @param \Plumrocket\Token\Api\CustomerHashValidatorInterface                      $customerTokenHashValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Review                       $reviewHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Permission                   $permissionHelper
     * @param \Magento\Customer\Model\Url                                               $customerUrl
     * @param \Plumrocket\Token\Api\CustomerRepositoryInterface                         $customerTokenRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator          $reviewArarFormValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory         $advancedreviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                         $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface     $createReviewFilter
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Permission\SaveGuestSecretKey $saveGuestSecretKey
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Model\Design $catalogDesign,
        \Magento\Framework\Session\Generic $reviewSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Plumrocket\Token\Api\CustomerHashValidatorInterface $customerTokenHashValidator,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Review $reviewHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper,
        \Magento\Customer\Model\Url $customerUrl,
        \Plumrocket\Token\Api\CustomerRepositoryInterface $customerTokenRepository,
        \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator,
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator $reviewArarFormValidator,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface $createReviewFilter,
        \Plumrocket\AdvancedReviewAndReminder\Model\Permission\SaveGuestSecretKey $saveGuestSecretKey
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $customerSession,
            $categoryRepository,
            $logger,
            $productRepository,
            $reviewFactory,
            $ratingFactory,
            $catalogDesign,
            $reviewSession,
            $storeManager,
            $formKeyValidator
        );
        $this->customerTokenHashValidator = $customerTokenHashValidator;
        $this->reviewHelper = $reviewHelper;
        $this->permissionHelper = $permissionHelper;
        $this->customerUrl = $customerUrl;
        $this->customerTokenRepository = $customerTokenRepository;
        $this->boughtProductValidator = $boughtProductValidator;
        $this->reviewArarFormValidator = $reviewArarFormValidator;
        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->dataHelper = $dataHelper;
        $this->createReviewFilter = $createReviewFilter;
        $this->saveGuestSecretKey = $saveGuestSecretKey;
    }

    /**
     * Create review from post data
     */
    public function execute()
    {
        $redirectResult = $this->resultRedirectFactory->create();

        if (! $product = $this->initProduct()) {
            $this->messageManager->addErrorMessage(
                __('The product that was requested doesn\'t exist. Verify the product and try again.')
            );
            return $redirectResult->setPath('/');
        }

        $data = $this->getRequest()->getPostValue();
        $data = $this->convertRatingData($data);
        $rating = $data['rating'] ?? [];
        $data = $this->createReviewFilter->filter($data);

        try {
            $tokenHash = $this->getRequest()->getParam('token', '');
            $tokenIsValid = $this->customerTokenHashValidator->validate($tokenHash);
            $customerTokenModel = $this->customerTokenRepository->get($tokenHash);
            $customerId = $customerTokenModel->getCustomerId();
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            $tokenIsValid = false;
            $customerId = 0;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $tokenIsValid = false;
            $customerId = 0;
        }

        if (! $tokenIsValid) {
            return $this->createRedirectToWriteReview($product, $data);
        }

        if (! $this->boughtProductValidator->validate((int) $product->getId(), $customerId)) {
            $this->messageManager->addWarningMessage(__('Only users who bought product can write review.'));
            return $redirectResult->setUrl($product->getProductUrl());
        }

        if (empty($data['nickname']) && $this->customerSession->isLoggedIn()) {
            $data['nickname'] = $this->customerSession->getCustomer()->getName();
        }

        try {
            $mistakes = $this->reviewArarFormValidator->detectMistakes($data);
        } catch (\Zend_Validate_Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(__('Sorry, something went wrong. Please try again later.'));
            $this->reviewSession->setFormData($data);
            return $redirectResult->setUrl($this->reviewHelper->getWriteReviewUrl($product));
        }

        if (! $mistakes) {
            $data['title'] = ! empty($data['title']) ? $data['title'] : '';
            try {
                /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Advancedreview $advancedReview */
                $advancedReview = $this->advancedreviewFactory->create();
                $advancedReview
                    ->setData($data)
                    ->setProduct($product)
                    ->setRating($rating)
                    ->setCustomerId($customerId)
                    ->saveReview();

                $this->messageManager->addSuccessMessage($this->dataHelper->getDefaultStatusMessage());
                return $redirectResult->setUrl($product->getProductUrl());
            } catch (\Exception $e) {
                $this->logger->critical($e);
                $this->messageManager->addErrorMessage(__('Sorry, something went wrong. Please try again later.'));
                return $redirectResult->setUrl($this->reviewHelper->getWriteReviewUrl($product));
            }
        }

        $this->reviewSession->setFormData($data);
        $this->saveGuestSecretKey->execute();
        return $redirectResult->setUrl($this->reviewHelper->getWriteReviewUrl($product, true));
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array                          $data
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    private function createRedirectToWriteReview(
        \Magento\Catalog\Model\Product $product,
        array $data
    ) : \Magento\Framework\Controller\Result\Redirect {
        $redirectResult = $this->resultRedirectFactory->create();

        if ($this->permissionHelper->allowWriteReview(null, $product)) {
            $this->reviewSession->setFormData($data);
            $this->messageManager->addWarningMessage(
                __('Your token has expired. Please post review from site.')
            );
            return $redirectResult->setUrl($this->reviewHelper->getWriteReviewUrl($product));
        }
        if ($this->permissionHelper->isLoginAndSubmitApproach()) {
            $this->customerSession->setPostReviewAfterLogin($data);
            $this->messageManager->addWarningMessage(
                __('Your token has expired. Please sing in before posting review.')
            );

            return $redirectResult->setUrl($this->customerUrl->getLoginUrl());
        }

        $this->messageManager->addWarningMessage(__('Your token has expired. Please post review from site.'));
        return $redirectResult->setUrl($product->getProductUrl());
    }

    /**
     * Yahoo doesnt support input name like "rating[3]", therefore rating has name "rating___3" in email
     *
     * @param array $data
     * @return array
     */
    private function convertRatingData(array $data) : array
    {
        foreach ($data as $key => $value) {
            if (false === strpos($key, 'rating___')) {
                continue;
            }

            $data['rating'][substr($key, 9)] = $value;
            unset($data[$key]);
        }

        return $data;
    }

    /**
     * Create exception in case CSRF validation failed.
     * Return null if default exception will suffice.
     *
     * @param \Magento\Framework\App\RequestInterface $request
     *
     * @return \Magento\Framework\App\Request\InvalidRequestException|null
     */
    public function createCsrfValidationException(\Magento\Framework\App\RequestInterface $request)
    {
        return null;
    }

    /**
     * Perform custom request validation.
     * Return null if default validation is needed.
     *
     * @param \Magento\Framework\App\RequestInterface $request
     *
     * @return bool|null
     */
    public function validateForCsrf(\Magento\Framework\App\RequestInterface $request)
    {
        return true;
    }
}
