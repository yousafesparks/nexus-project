<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Magento\Framework\Controller\ResultFactory;

class Abuse extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface
     */
    private $adminNotificationSender;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    private $reviewFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Abuse constructor.
     *
     * @param \Magento\Framework\App\Action\Context                                        $context
     * @param \Magento\Customer\Model\Session                                              $customerSession
     * @param \Psr\Log\LoggerInterface                                                     $logger
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                              $productRepository
     * @param \Magento\Review\Model\ReviewFactory                                          $reviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory            $advancedreviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                            $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                          $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface $adminNotificationSender
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface $adminNotificationSender
    ) {
        parent::__construct($context);
        $this->adminNotificationSender = $adminNotificationSender;
        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->configHelper = $configHelper;
        $this->reviewFactory = $reviewFactory;
        $this->dataHelper = $dataHelper;
        $this->logger = $logger;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (! $this->dataHelper->moduleEnabled()) {
            return $result->setHttpResponseCode(404);
        }

        if (! $reviewId = $this->getRequest()->getParam('reviewId')) {
            return $result->setHttpResponseCode(400)
                          ->setData(['message' => __('Unable to update the review. Review Id is empty')]);
        }

        /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Advancedreview $advancedReview */
        $advancedReview = $this->advancedreviewFactory->create();
        $advancedReview->loadByReview($reviewId);

        $customerId = $this->customerSession->getCustomerId();

        if (! $customerId && ! $this->configHelper->isEnabledAbuseForGuest()) {
            return $result->setHttpResponseCode(400)
                          ->setData(['message' => __('Guest can\'t report abuse')]);
        }

        /** @var \Magento\Review\Model\Review $review */
        $review = $this->reviewFactory->create()->load($advancedReview->getReviewId());

        try {
            $product = $this->productRepository->getById($review->getEntityPkValue());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $this->logger->debug($e);
            return $result->setHttpResponseCode(400)
                          ->setData(['message' => __('Guest can\'t report abuse')]);
        }

        $advancedReview->setAbuse(1)
            ->setAbusedCustomer($customerId)
            ->setReviewId($reviewId)
            ->save();

        $this->adminNotificationSender->sendNotificationsOfAbuse($review, $product, (int)$customerId);

        return $result->setData(['message' => __('Review marked as abusive')]);
    }
}
