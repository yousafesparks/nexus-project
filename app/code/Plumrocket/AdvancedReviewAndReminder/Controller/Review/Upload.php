<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Magento\Framework\Controller\ResultFactory;

class Upload extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\File
     */
    protected $fileHelper;

    /**
     * Upload constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\File $fileHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Helper\File $fileHelper
    ) {
        parent::__construct($context);
        $this->fileHelper = $fileHelper;
    }

    public function execute()
    {
        try {
            $result = $this->fileHelper->upload('comment_file');
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        /** @var \Magento\Framework\Controller\Result\Raw $response */
        $response = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));
        return $response;
    }
}
