<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review\Media;

use Magento\Framework\Controller\ResultFactory;
use Plumrocket\AdvancedReviewAndReminder\Model\ReviewImageFactoryInterface as ReviewImageFactory;

class ListAction extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    private $advancedReviewRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible
     */
    private $productVisible;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria
     */
    private $convertJsSearchCriteria;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface
     */
    private $reviewImagesFactory;

    /**
     * ListAction constructor.
     *
     * @param \Magento\Framework\App\Action\Context                                            $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface      $advancedReviewRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria   $convertJsSearchCriteria
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible                   $productVisible
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface $reviewImagesFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria $convertJsSearchCriteria,
        \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible $productVisible,
        \Plumrocket\AdvancedReviewAndReminder\Model\ReviewMultipleImagesFactoryInterface $reviewImagesFactory
    ) {
        parent::__construct($context);
        $this->advancedReviewRepository = $advancedReviewRepository;
        $this->convertJsSearchCriteria = $convertJsSearchCriteria;
        $this->productVisible = $productVisible;
        $this->reviewImagesFactory = $reviewImagesFactory;
    }

    public function execute()
    {
        $productId = (int) $this->getRequest()->getParam('product');

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (! $this->productVisible->execute($productId)) {
            return $result
                ->setHttpResponseCode(400)
                ->setData(__('Cannot find product with id: "%1", or he not visible on this website', $productId));
        }
        $searchCriteria['general']['with_media'] = 1;
        $searchCriteria['page']['size'] = 0;
        $searchCriteria = $this->convertJsSearchCriteria->execute($searchCriteria, $productId);
        $reviewSearchResults = $this->advancedReviewRepository->getList(
            $searchCriteria,
            0,
            true
        );
        $advancedReviews = $reviewSearchResults->getItems();

        $images = [];
        foreach ($advancedReviews as $advancedReview) {
            $attachImages = $this->reviewImagesFactory->create(
                $advancedReview,
                ReviewImageFactory::THUMBNAIL_IMAGE_ID
            );
            foreach ($attachImages as $orderNumber => $attachImage) {
                $images[] = [
                    'reviewId' => $advancedReview->getId(),
                    'url' => $attachImage->getUrl(),
                    'orderNumber' => $orderNumber,
                ];
            }
        }

        return $result->setData(['items' => $images]);
    }
}
