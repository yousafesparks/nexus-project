<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Review\Controller\Product as ProductReviewAction;

class Leavereview extends ProductReviewAction
{
    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() : ResultInterface
    {
        $product = $this->loadProduct($this->getRequest()->getParam('id'));
        if (! $product) {
            /** @var \Magento\Framework\Controller\Result\Forward $resultPage */
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
            return $resultPage->forward('noroute');
        }

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getLayout()->getBlock('review.form.page')->setProduct($product);
        $resultPage->getConfig()->getTitle()->set(__('Write Your Own Review'));

        return $resultPage;
    }
}
