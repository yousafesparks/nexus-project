<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller\Review;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class ListAction
 * @since 2.0.0
 */
class ListAction extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface
     */
    private $advancedReviewRepository;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertToArray
     */
    private $convertReviewsToArray;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria
     */
    private $convertJsSearchCriteria;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface
     */
    private $getReviewCount;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible
     */
    private $productVisible;

    /**
     * ListAction constructor.
     *
     * @param \Magento\Framework\App\Action\Context                                          $context
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface    $advancedReviewRepository
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertToArray          $convertReviewsToArray
     * @param \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria $convertJsSearchCriteria
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface              $getReviewCount
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible                 $productVisible
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Plumrocket\AdvancedReviewAndReminder\Api\AdvancedReviewRepositoryInterface $advancedReviewRepository,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertToArray $convertReviewsToArray,
        \Plumrocket\AdvancedReviewAndReminder\ViewModel\Review\ConvertJsSearchCriteria $convertJsSearchCriteria,
        \Plumrocket\AdvancedReviewAndReminder\Api\GetReviewCountInterface $getReviewCount,
        \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible $productVisible
    ) {
        parent::__construct($context);
        $this->advancedReviewRepository = $advancedReviewRepository;
        $this->convertReviewsToArray = $convertReviewsToArray;
        $this->convertJsSearchCriteria = $convertJsSearchCriteria;
        $this->getReviewCount = $getReviewCount;
        $this->productVisible = $productVisible;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() : \Magento\Framework\Controller\ResultInterface
    {
        $productId = (int) $this->getRequest()->getParam('product');
        $searchCriteria = $this->getRequest()->getParam('searchCriteria', []);

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (! $this->productVisible->execute($productId)) {
            return $result
                ->setHttpResponseCode(400)
                ->setData(__('Cannot find product with id: "%1", or he not visible on this website', $productId));
        }

        if (isset($searchCriteria['review']) && $searchCriteria['review']) {
            $items = [$this->advancedReviewRepository->get((int) $searchCriteria['review'])];
            $count = 1;
            $totalCount = 1;
        } else {
            $searchCriteria = $this->convertJsSearchCriteria->execute($searchCriteria, $productId);

            $reviewSearchResults = $this->advancedReviewRepository->getList(
                $searchCriteria,
                0,
                true
            );

            $items = $reviewSearchResults->getItems();
            $count = $reviewSearchResults->getTotalCount();
            $totalCount = $this->getReviewCount->execute($productId);
        }

        return $result->setData(
            [
                'items' => $this->convertReviewsToArray->execute($items),
                'count' => $count,
                'totalCount' => $totalCount,
            ]
        );
    }
}
