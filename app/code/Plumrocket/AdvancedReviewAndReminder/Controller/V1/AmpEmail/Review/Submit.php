<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Controller\V1\AmpEmail\Review;

use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @since 1.2.0
 */
class Submit extends \Plumrocket\AmpEmailApi\Controller\AbstractStoreViewAction
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface
     */
    private $boughtProductValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator
     */
    private $reviewArarFormValidator;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Permission
     */
    private $permissionHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible
     */
    private $productVisible;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface
     */
    private $createReviewFilter;

    /**
     * Submit constructor.
     *
     * @param \Magento\Framework\App\Action\Context                                     $context
     * @param \Magento\Store\Model\App\Emulation                                        $appEmulation
     * @param \Magento\Store\Model\StoreManagerInterface                                $storeManager
     * @param \Plumrocket\AmpEmailApi\Model\Result\AmpJsonFactoryInterface              $ampJsonFactory
     * @param \Plumrocket\AmpEmailApi\Model\CorsValidatorInterface                      $corsValidator
     * @param \Plumrocket\Token\Api\CustomerRepositoryInterface                         $tokenRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                           $productRepository
     * @param \Magento\Customer\Api\CustomerRepositoryInterface                         $customerRepository
     * @param \Psr\Log\LoggerInterface                                                  $logger
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator          $reviewArarFormValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Permission                   $permissionHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory         $advancedreviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible            $productVisible
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                         $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface     $createReviewFilter
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Plumrocket\AmpEmailApi\Model\Result\AmpJsonFactoryInterface $ampJsonFactory,
        \Plumrocket\AmpEmailApi\Model\CorsValidatorInterface $corsValidator,
        \Plumrocket\Token\Api\CustomerRepositoryInterface $tokenRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Psr\Log\LoggerInterface $logger,
        \Plumrocket\AdvancedReviewAndReminder\Api\BoughtProductValidatorInterface $boughtProductValidator,
        \Plumrocket\AdvancedReviewAndReminder\Model\Review\FormValidator $reviewArarFormValidator,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Plumrocket\AdvancedReviewAndReminder\Model\Product\GetVisible $productVisible,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Api\CreateReviewFilterInterface $createReviewFilter
    ) {
        parent::__construct($context, $appEmulation, $storeManager, $ampJsonFactory, $corsValidator, $tokenRepository);
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->boughtProductValidator = $boughtProductValidator;
        $this->reviewArarFormValidator = $reviewArarFormValidator;
        $this->permissionHelper = $permissionHelper;
        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->productVisible = $productVisible;
        $this->dataHelper = $dataHelper;
        $this->createReviewFilter = $createReviewFilter;
    }

    /**
     * @return \Plumrocket\AmpEmailApi\Model\Result\AmpJsonInterface
     */
    public function execute()
    {
        $ampJsonResult = $this->ampJsonFactory->create();
        $this->startEmulationForAmp();

        $rating = $this->getRequest()->getParam('rating', []);
        $productId = (int) $this->getRequest()->getParam('product', 0);
        $customerId = $this->getTokenModel()->getCustomerId();

        if (! $product = $this->productVisible->execute($productId)) {
            $ampJsonResult->addErrorMessage(
                __('The product that was requested doesn\'t exist. Verify the product and try again.')
            );
            $this->stopEmulation();
            return $ampJsonResult;
        }

        $data = $this->getPreparedData();
        if ($this->validate($data, $ampJsonResult, $product, $customerId)) {
            try {
                /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Advancedreview $advancedReview */
                $advancedReview = $this->advancedreviewFactory->create();
                $advancedReview
                    ->setData($data)
                    ->setProduct($product)
                    ->setRating($rating)
                    ->setCustomerId($customerId);

                $magentoReview = $advancedReview->saveReview();

                $ampJsonResult->addSuccessMessage(
                    $this->dataHelper->getStatusMessage((int) $magentoReview->getStatusId())
                );
            } catch (\Exception $e) {
                $this->logger->critical($e);
                $ampJsonResult->setHttpResponseCode(400);
            }
        }

        $this->stopEmulation();
        return $ampJsonResult;
    }

    /**
     * @return array
     */
    private function getPreparedData() : array
    {
        $data = $this->getRequest()->getPostValue();
        $data = $this->createReviewFilter->filter($data);
        $data = $this->prepareNickname($data);
        $data['title'] = $data['title'] ?? '';
        return $data;
    }

    /**
     * Load product model with data by passed id.
     * Return false if product was not loaded or has incorrect status.
     *
     * @param int $productId
     * @return bool|CatalogProduct|\Magento\Catalog\Api\Data\ProductInterface
     */
    protected function loadProduct($productId)
    {
        if (!$productId) {
            return false;
        }

        try {
            $product = $this->productRepository->getById($productId);
            $websiteId = $this->storeManager->getStore($this->getStoreId())->getWebsiteId();

            if (! in_array($websiteId, $product->getWebsiteIds(), false)) {
                return false;
            }

            if (! $product->isVisibleInCatalog() || ! $product->isVisibleInSiteVisibility()) {
                return false;
            }
        } catch (NoSuchEntityException $noEntityException) {
            return false;
        }

        return $product;
    }

    /**
     * @param array $data
     * @return array
     */
    private function prepareNickname(array $data) : array
    {
        if (empty($data['nickname']) && $this->getTokenModel()->getCustomerId()) {
            try {
                // We don't use "$this->getCustomer" method for logging exceptions
                $customer = $this->customerRepository->getById($this->getTokenModel()->getCustomerId());
                $data['nickname'] = $customer->getFirstname();
            } catch (NoSuchEntityException $e) {
                $this->logger->debug($e);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->logger->critical($e);
            }
        }

        return $data;
    }

    /**
     * @return bool|\Magento\Customer\Api\Data\CustomerInterface
     */
    private function getCustomer()
    {
        try {
            return $this->customerRepository->getById($this->getTokenModel()->getCustomerId());
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return false;
        }
    }

    /**
     * @param array                                                 $data
     * @param \Plumrocket\AmpEmailApi\Model\Result\AmpJsonInterface $ampJsonResult
     * @param \Magento\Catalog\Api\Data\ProductInterface            $product
     * @param int                                                   $customerId
     * @return bool
     */
    private function validate(
        array $data,
        \Plumrocket\AmpEmailApi\Model\Result\AmpJsonInterface $ampJsonResult,
        \Magento\Catalog\Api\Data\ProductInterface $product,
        int $customerId
    ) : bool {
        if (empty($data)) {
            $ampJsonResult->addErrorMessage(__('Review can\'t be empty'));
            return false;
        }

        if (! $this->boughtProductValidator->validate((int) $product->getId(), $customerId)) {
            $ampJsonResult->addErrorMessage(__('Only users who bought product can write review.'));
            return false;
        }

        try {
            $mistakes = $this->reviewArarFormValidator->detectMistakes($data);
            if (empty($mistakes)) {
                if ($this->permissionHelper->allowWriteReview($this->getCustomer(), $product)) {
                    return true;
                }

                $ampJsonResult->addErrorMessage(__('Sorry, you can not write a review'));
            } else { // Have mistakes
                foreach ($mistakes as $mistake) {
                    $ampJsonResult->addErrorMessage($mistake);
                }
            }
        } catch (\Zend_Validate_Exception $e) {
            $this->logger->critical($e);
            $ampJsonResult->setHttpResponseCode(400); // System error, show default fallback
        }

        return false;
    }
}
