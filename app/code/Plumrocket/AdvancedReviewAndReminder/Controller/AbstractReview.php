<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Controller;

abstract class AbstractReview extends \Magento\Review\Controller\Product
{
    const ADMIN_RESOURCE = 'Plumrocket_AdvancedReviewAndReminder::index';

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Advancedreview
     */
    protected $review;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    protected $advancedreviewFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    protected $configHelper;

    /**
     * AbstractReview constructor.
     *
     * @param \Magento\Framework\App\Action\Context                             $context
     * @param \Magento\Framework\Registry                                       $coreRegistry
     * @param \Magento\Customer\Model\Session                                   $customerSession
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface                  $categoryRepository
     * @param \Psr\Log\LoggerInterface                                          $logger
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                   $productRepository
     * @param \Magento\Review\Model\ReviewFactory                               $reviewFactory
     * @param \Magento\Review\Model\RatingFactory                               $ratingFactory
     * @param \Magento\Catalog\Model\Design                                     $catalogDesign
     * @param \Magento\Framework\Session\Generic                                $reviewSession
     * @param \Magento\Store\Model\StoreManagerInterface                        $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator                    $formKeyValidator
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                 $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config               $configHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Model\Design $catalogDesign,
        \Magento\Framework\Session\Generic $reviewSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $customerSession,
            $categoryRepository,
            $logger,
            $productRepository,
            $reviewFactory,
            $ratingFactory,
            $catalogDesign,
            $reviewSession,
            $storeManager,
            $formKeyValidator
        );

        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->dataHelper = $dataHelper;
        $this->configHelper = $configHelper;
    }

    /**
     * @param array $data
     * @param bool  $success
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function sendJsonResponse(array $data, $success = false)
    {
        $data['success'] = (bool)$success;

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        return $result->setData($data);
    }

    /**
     * @deprecated since 2.0.0, review must be rendered by knockout
     *
     * @param null $review
     * @return \Plumrocket\AdvancedReviewAndReminder\Block\Review\Item|null
     */
    protected function getPostHtml($review = null)
    {
        return null;
    }
}
