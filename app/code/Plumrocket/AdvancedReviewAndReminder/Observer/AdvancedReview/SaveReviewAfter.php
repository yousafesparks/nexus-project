<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\AdvancedReview;

class SaveReviewAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface
     */
    private $adminNotificationSender;

    /**
     * SaveReviewAfter constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                            $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface $adminNotificationSender
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdminNotificationSenderInterface $adminNotificationSender
    ) {
        $this->dataHelper = $dataHelper;
        $this->adminNotificationSender = $adminNotificationSender;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (! $this->dataHelper->moduleEnabled()) {
            return;
        }

        $status = $observer->getData('status');
        /**
         * @var \Magento\Review\Model\Review $review
         */
        $review = $observer->getData('review');
        /**
         * @var \Magento\Catalog\Model\Product $product
         */
        $product = $observer->getData('product');

        if (\Magento\Review\Model\Review::STATUS_PENDING === $status) {
            $this->adminNotificationSender->sendNotificationsOfPendingReview($review, $product);
        }
    }
}
