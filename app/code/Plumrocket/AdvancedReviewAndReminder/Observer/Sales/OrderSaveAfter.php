<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Sales;

use Magento\Sales\Model\Order;
use Magento\Paypal\Model\Info as PaypalInfo;
use Plumrocket\AdvancedReviewAndReminder\Model\Reminder as AdvancedReminder;

class OrderSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory
     */
    private $reminderFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * OrderSaveAfter constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory $reminderFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data           $dataHelper
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\ReminderFactory $reminderFactory,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper
    ) {
        $this->reminderFactory = $reminderFactory;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();
        if ($this->dataHelper->moduleEnabled($order->getStoreId())) {
            if (!$order->getStatus()) {
                return;
            }
            $this->checkOrderStatus($order); //close reminder if it change status
            $this->reminderFactory->create()->createReminder($order);
        }
    }

    /**
     * @param Order $order
     */
    private function checkOrderStatus(Order $order)
    {
        $blackList = [
            Order::STATE_CANCELED,
            Order::STATE_CLOSED,
            Order::STATE_HOLDED,
            PaypalInfo::ORDER_STATUS_CANCELED_REVERSAL,
        ];

        if (in_array($order->getStatus(), $blackList, true)) {
            /**
             * @var \Plumrocket\AdvancedReviewAndReminder\Model\ResourceModel\Reminder\Collection $reminderCollection
             */
            $reminderCollection = $this->reminderFactory->create()
                ->getCollection()
                ->addFieldToFilter('order_id', $order->getId())
                ->addFieldToFilter('status', AdvancedReminder::ADVANCEDRAR_STATUS_PENDING);

            foreach ($reminderCollection as $reminder) {
                $reminder->setStatus(AdvancedReminder::ADVANCEDRAR_STATUS_CANCELED);
            }

            $reminderCollection->save();
        }
    }
}
