<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Frontend;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\InputException;
use Plumrocket\AdvancedReviewAndReminder\Controller\Review\Post;

class PostReviewObserver implements ObserverInterface
{
    /**
     * @var \MSP\ReCaptcha\Model\Provider\FailureProviderInterface
     */
    private $failureProvider;

    /**
     * @var \MSP\ReCaptcha\Api\ValidateInterface
     */
    private $validate;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var \MSP\ReCaptcha\Model\Provider\ResponseProviderInterface
     */
    private $responseProvider;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\Provider\IsCheckRequired\Frontend\PostReview
     */
    private $isCheckRequired;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Captcha\Helper\Data
     */
    private $captchaHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Observer\NativeCaptchaStringResolver
     */
    private $captchaStringResolver;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    private $actionFlag;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Permission
     */
    private $permissionHelper;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * phpcs:disable Generic.Files.LineLength
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                                      $configHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Permission                                  $permissionHelper
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress                                     $remoteAddress
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\IsCheckRequired\Frontend\PostReview $isCheckRequired
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider               $independenceObjectProvider
     * @param \Magento\Captcha\Helper\Data                                                             $captchaHelper
     * @param \Magento\Framework\App\ActionFlag                                                        $actionFlag
     * @param \Plumrocket\AdvancedReviewAndReminder\Observer\NativeCaptchaStringResolver               $captchaStringResolver
     * @param \Magento\Framework\ObjectManagerInterface                                                $objectManager
     * @param \Psr\Log\LoggerInterface                                                                 $logger
     * phpcs:enable Generic.Files.LineLength
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Permission $permissionHelper,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Plumrocket\AdvancedReviewAndReminder\Model\Provider\IsCheckRequired\Frontend\PostReview $isCheckRequired,
        \Plumrocket\AdvancedReviewAndReminder\Model\MSP\IndependenceObjectProvider $independenceObjectProvider,
        \Magento\Captcha\Helper\Data $captchaHelper,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Plumrocket\AdvancedReviewAndReminder\Observer\NativeCaptchaStringResolver $captchaStringResolver,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->responseProvider = $independenceObjectProvider->getResponseProvider();
        $this->validate = $independenceObjectProvider->getValidate();
        $this->failureProvider = $independenceObjectProvider->getFailureProvider();
        $this->remoteAddress = $remoteAddress;
        $this->isCheckRequired = $isCheckRequired;
        $this->configHelper = $configHelper;
        $this->permissionHelper = $permissionHelper;
        $this->captchaHelper = $captchaHelper;
        $this->captchaStringResolver = $captchaStringResolver;
        $this->actionFlag = $actionFlag;
        $this->objectManager = $objectManager;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        if ($this->permissionHelper->isLoginAndSubmitApproach()) {
            return;
        }

        if ($this->isCheckRequired->execute()) {
            $this->checkReCaptcha($observer);
            return;
        }

        if ($this->isCheckRequired->executeForMagentoReCaptcha()) {
            $this->checkMagentoReCaptcha($observer);
            return;
        }

        if ($this->configHelper->isEnabledNativeCaptcha()) {
            $this->checkNativeCaptcha($observer);
            return;
        }
    }

    /**
     * @param Observer $observer
     */
    private function checkReCaptcha(Observer $observer)
    {
        $reCaptchaResponse = $this->responseProvider->execute();
        $remoteIp = $this->remoteAddress->getRemoteAddress();

        /** @var \Magento\Framework\App\Action\Action $controller */
        $controller = $observer->getControllerAction();

        if (! $this->validate->validate($reCaptchaResponse, $remoteIp)) {
            $this->failureProvider->execute($controller ? $controller->getResponse() : null);
        }
    }

    /**
     * @param Observer $observer
     */
    private function checkNativeCaptcha(Observer $observer)
    {
        $formId = \Plumrocket\AdvancedReviewAndReminder\Helper\Config::PR_ARAR_FORM_CAPTCHA;
        $captchaModel = $this->captchaHelper->getCaptcha($formId);
        if (! $captchaModel->isRequired()) {
            return;
        }

        /** @var \Plumrocket\AdvancedReviewAndReminder\Controller\Review\Post $controller */
        $controller = $observer->getControllerAction();

        if (! $captchaModel->isCorrect($this->captchaStringResolver->resolve($controller->getRequest(), $formId))) {
            $this->actionFlag->set('', Action::FLAG_NO_DISPATCH, true);
            $jsonResponse = $controller->sendJsonResponse(['message' => __('Incorrect CAPTCHA')]);
            $jsonResponse->renderResult($controller->getResponse());
        }
    }

    /**
     * Validate reCAPTCHA by models from magento modules
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\InputException
     */
    private function checkMagentoReCaptcha(Observer $observer)
    {
        /** @var \Plumrocket\AdvancedReviewAndReminder\Controller\Review\Post $controller */
        $controller = $observer->getControllerAction();
        $request = $controller->getRequest();

        /** @var \Magento\ReCaptchaUi\Model\CaptchaResponseResolverInterface $captchaResponseResolver */
        $captchaResponseResolver = $this->objectManager->get(
            \Magento\ReCaptchaUi\Model\CaptchaResponseResolverInterface::class
        );

        /** @var \Magento\ReCaptchaUi\Model\ValidationConfigResolverInterface $validationConfigResolver */
        $validationConfigResolver = $this->objectManager->get(
            \Magento\ReCaptchaUi\Model\ValidationConfigResolverInterface::class
        );

        /** @var \Magento\ReCaptchaValidationApi\Api\ValidatorInterface $captchaValidator */
        $captchaValidator = $this->objectManager->get(
            \Magento\ReCaptchaValidationApi\Api\ValidatorInterface::class
        );

        $validationConfig = $validationConfigResolver->get('product_review');

        try {
            $reCaptchaResponse = $captchaResponseResolver->resolve($request);
        } catch (InputException $e) {
            $this->logger->error($e);
            $this->processError($controller, $validationConfig->getValidationFailureMessage());
            return;
        }

        $validationResult = $captchaValidator->isValid($reCaptchaResponse, $validationConfig);
        if (false === $validationResult->isValid()) {
            $this->processError($controller, $validationConfig->getValidationFailureMessage());
        }
    }

    /**
     * Set error message and stop creating review
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Controller\Review\Post $controller
     * @param string                                                       $validationFailureMessage
     */
    private function processError(Post $controller, string $validationFailureMessage)
    {
        $this->actionFlag->set('', Action::FLAG_NO_DISPATCH, true);
        $jsonResponse = $controller->sendJsonResponse(['message' => __($validationFailureMessage)]);
        $jsonResponse->renderResult($controller->getResponse());
    }
}
