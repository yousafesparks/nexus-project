<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Review;

use Magento\Framework\Exception\FileSystemException;

class SaveBefore implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $file;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * SaveBefore constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory
     * @param \Magento\Framework\App\State                                      $state
     * @param \Magento\Framework\Filesystem\Driver\File                         $file
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config               $config
     * @param \Psr\Log\LoggerInterface                                          $logger
     * @param \Magento\Framework\Message\ManagerInterface                       $messageManager
     * @param \Magento\Framework\Serialize\SerializerInterface                  $serializer
     * @param \Magento\Framework\App\RequestInterface                           $request
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Magento\Framework\App\State $state,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $config,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->state = $state;
        $this->file = $file;
        $this->configHelper = $config;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->serializer = $serializer;
        $this->request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        if ($this->state->getAreaCode() === \Magento\Framework\App\Area::AREA_ADMINHTML) {
            /** @var \Magento\Review\Model\Review $review */
            $review = $observer->getObject();
            /** @var \Plumrocket\AdvancedReviewAndReminder\Model\Advancedreview $advancedReview */
            $advancedReview = $this->advancedreviewFactory->create()
                ->loadByReview($review->getReviewId());

            if (($currentImages = $review->getAttachImages()) && ($attachImages = $advancedReview->getAttachImage())) {
                foreach ($attachImages as $id => $attachImage) {
                    $currentImage = $currentImages[$id];

                    if (isset($currentImage['delete'])) {
                        $attachImages = $this->deleteImage($attachImage, $attachImages, $id);
                    }
                }

                if (empty($attachImages)) {
                    $attachImages = null;
                } else {
                    $attachImages = $this->serializer->serialize(array_values($attachImages));
                }

                $review->setData('attach_image', $attachImages);
            }

            // Convert field 'video' to 'youtube_id'
            if ('save' === $this->request->getActionName()) {
                if (($videoUrl = $review->getVideo())
                    && ($videoId = $this->configHelper->getYoutubeIdFromUrl($videoUrl))
                ) {
                    $review->setData('youtube_id', $videoId);
                } else {
                    $review->setData('youtube_id', null);
                }
            }
        }
    }

    /**
     * @param $attachImage
     * @param array $attachImages
     * @param $id
     * @return array
     */
    public function deleteImage($attachImage, array $attachImages, $id) : array
    {
        $mediaPath = $this->configHelper->getBaseMediaPath() .
            DIRECTORY_SEPARATOR . $attachImage['filename'];

        try {
            if ($this->file->isExists($mediaPath)) {
                $this->file->deleteFile($mediaPath);
            }
            unset($attachImages[$id]);
        } catch (FileSystemException $e) {
            $this->logger->critical($e);
            $this->messageManager->addWarningMessage(__('Image not deleted, path: "$1"', $mediaPath));
        }
        return $attachImages;
    }
}
