<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Review;

class SaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    private $reviewFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * SaveAfter constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory
     * @param \Magento\Review\Model\ReviewFactory                               $reviewFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                       $dateTime
     * @param \Psr\Log\LoggerInterface                                          $logger
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->reviewFactory = $reviewFactory;
        $this->dateTime = $dateTime;
        $this->logger = $logger;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $review = $observer->getObject();

        $advancedReview = $this->advancedreviewFactory->create()
            ->loadByReview($review->getReviewId());
        $advancedReview->addData($review->getData());

        if ($review->getAdminComment()) {
            $reviewComment = $this->advancedreviewFactory->create()
                ->getCollection()
                ->addFieldToFilter('admin_comment', $review->getAdminComment())
                ->setPageSize(1)
                ->getFirstItem();

            if (! $reviewComment->getId()) {
                $advancedReview->setAdminCommentDate($this->dateTime->gmtDate());
            }

            $advancedReview->setAdminComment($review->getAdminComment());
        }

        try {
            $advancedReview
                ->updateReviewUrl($review, false)
                ->save();

            if ($review->getCreatedAtManually()) {
                $_review = $this->reviewFactory->create()
                    ->load($review->getReviewId());
                $_review->setCreatedAt($review->getCreatedAtManually())
                    ->save();
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
