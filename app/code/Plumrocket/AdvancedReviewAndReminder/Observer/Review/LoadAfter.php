<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Review;

class LoadAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * LoadAfter constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory
    ) {
        $this->advancedreviewFactory = $advancedreviewFactory;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $review = $observer->getEvent()->getObject();
        $advancedReview = $this->advancedreviewFactory->create()->loadByReview($review->getId());
        $review->addData($advancedReview->getData());
    }
}
