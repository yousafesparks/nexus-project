<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Customer;

use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Store\Model\ScopeInterface;

class Login extends \Magento\Framework\DataObject implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var null
     */
    private $customer = null;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory
     */
    private $advancedreviewFactory;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Login constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Data                 $dataHelper
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory  $customerCollectionFactory
     * @param \Magento\Customer\Model\Session                                   $customerSession
     * @param \Magento\Catalog\Model\ProductRepository                          $productRepository
     * @param \Magento\Store\Model\StoreManagerInterface                        $storeManager
     * @param \Magento\Framework\Message\ManagerInterface                       $messageManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface                $scopeConfig
     * @param \Psr\Log\LoggerInterface                                          $logger
     * @param array                                                             $data
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Data $dataHelper,
        \Plumrocket\AdvancedReviewAndReminder\Model\AdvancedreviewFactory $advancedreviewFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        $this->advancedreviewFactory = $advancedreviewFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        parent::__construct($data);
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        if ($this->dataHelper->moduleEnabled()) {
            if ($observer->getEvent()->getName() == 'customer_register_success') {
                $customer = $observer->getCustomer();
                $this->setRegisteredName($customer->getFirstname() . ' ' . $customer->getLastname())
                     ->setRegisteredEmail($customer->getEmail());
            }
            if ($this->customerSession->getPostReviewAfterLogin()) {
                $this->saveReviewAfterLogin($this->customerSession->getPostReviewAfterLogin());
                $this->customerSession->setPostReviewAfterLogin(null);
            }
        }
    }

    /**
     * @param $data
     * @return bool
     */
    private function saveReviewAfterLogin($data)
    {
        if (! ($productId = $data['product']) || empty($data['detail'])) {
            return false;
        }

        $product = $this->productRepository
            ->getById(
                $productId,
                false,
                $this->storeManager->getStore()->getId()
            );

        $rating = $data['rating'];
        unset($data['rating'], $data['product']);

        $data['title'] = ! empty($data['title']) ? $data['title'] : '';
        if ($this->loadCustomer()) {
            $data['customer_id'] = $this->getCustomer()->getId();

            if ($this->getRegisteredName()) {
                $data['nickname'] = $this->getRegisteredName();
            } elseif ($this->getCustomer()->getName()) {
                $data['nickname'] = $this->getCustomer()->getName();
            }
        }

        try {
            $this->advancedreviewFactory->create()
                ->setData($data)
                ->setRating($rating)
                ->setProduct($product)
                ->saveReview();

            if (! $this->scopeConfig->isSetFlag(
                CustomerUrl::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD,
                ScopeInterface::SCOPE_STORE
            )
            ) {
                $this->customerSession->setBeforeAuthUrl($product->getProductUrl());
            }

            $this->messageManager->addSuccessMessage(
                $this->dataHelper->getDefaultStatusMessage()
            );
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }

    /**
     * @return $this|bool
     */
    private function loadCustomer()
    {
        if ($this->customerSession->isLoggedIn()) {
            $this->setCustomer($this->customerSession->getCustomer());
            return $this;
        }

        if ($this->getRegisteredName() && $this->getRegisteredEmail()) {
            /** @var \Magento\Customer\Model\Customer $customer */
            $customer = $this->customerCollectionFactory->create()
                ->addFieldToFilter('email', $this->getRegisteredEmail())
                ->setPageSize(1)
                ->addFieldToFilter('website_id', $this->storeManager->getWebsite()->getId())
                ->getFirstItem();

            if ($customer->getId()) {
                $this->setCustomer($customer);

                return $this;
            }
        }

        return false;
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @return $this
     */
    private function setCustomer(\Magento\Customer\Model\Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return null
     */
    private function getCustomer()
    {
        return $this->customer;
    }
}
