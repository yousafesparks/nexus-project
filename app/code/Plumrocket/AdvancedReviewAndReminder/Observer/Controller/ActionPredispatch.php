<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Controller;

use Magento\Framework\App\RequestInterface;
use Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\Permission\GuestSecretDataProvider;

class ActionPredispatch implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface
     */
    private $autoLoginManager;

    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig
     */
    private $reminderConfig;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * ActionPredispatch constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\Config                 $configHelper
     * @param \Magento\Framework\Stdlib\CookieManagerInterface                    $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory              $cookieMetadataFactory
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\AutoLoginManagerInterface $autoLoginManager
     * @param \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig         $reminderConfig
     * @param \Magento\Framework\App\RequestInterface                             $request
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Helper\Config $configHelper,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        AutoLoginManagerInterface $autoLoginManager,
        \Plumrocket\AdvancedReviewAndReminder\Helper\ReminderConfig $reminderConfig,
        RequestInterface $request
    ) {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->configHelper = $configHelper;
        $this->autoLoginManager = $autoLoginManager;
        $this->reminderConfig = $reminderConfig;
        $this->request = $request;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (! $this->configHelper->isModuleEnabled()
            || ! $this->reminderConfig->isEnabledAutoLogin()) {
            return;
        }

        $key = $this->request->getParam(AutoLoginManagerInterface::QUERY_PARAM_NAME, '');
        if ($this->autoLoginManager->loginByKey($key)) {
            $this->request->setDispatched(true);
        }

        /**
         * Set cookie doesn't work with FPC
         */
        if ($this->request->getParam(GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER)) {
            $guestSecretKey = $this->request->getParam(
                GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER
            );

            $cookieMetadata = $this->cookieMetadataFactory->createSensitiveCookieMetadata()
                ->setPath('/');
            $this->cookieManager->setSensitiveCookie(
                GuestSecretDataProvider::SECRET_KEY_FOR_GUEST_REMINDER,
                $guestSecretKey,
                $cookieMetadata
            );
        }
    }
}
