<?php
/**
 * @package     Plumrocket_AdvancedReviewAndReminder
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

declare(strict_types=1);

namespace Plumrocket\AdvancedReviewAndReminder\Observer\Reminder;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Plumrocket\AdvancedReviewAndReminder\Model\System\Config\VisitorType;
use Plumrocket\AdvancedReviewAndReminder\Api\Data\SubscriberInterface;

class ChangeSubscriberType implements ObserverInterface
{
    /**
     * @var \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface
     */
    protected $reminderSubscriberRepository;

    /**
     * ChangeSubscriberType constructor.
     *
     * @param \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository
     */
    public function __construct(
        \Plumrocket\AdvancedReviewAndReminder\Api\SubscriberRepositoryInterface $reminderSubscriberRepository
    ) {
        $this->reminderSubscriberRepository = $reminderSubscriberRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();

        try {
            $subscriber = $this->reminderSubscriberRepository->getByEmail($customer->getEmail());
            if ((int) $subscriber->getType() === VisitorType::GUEST) {
                $subscriber->addData([
                    SubscriberInterface::CUSTOMER_ID => $customer->getId(),
                    SubscriberInterface::VISITOR_TYPE => VisitorType::CUSTOMER,
                ]);

                $this->reminderSubscriberRepository->save($subscriber);
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
        }
    }
}
