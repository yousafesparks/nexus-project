<?php
/**
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2015 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'plumrocket_sociallogin_account'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('plumrocket_sociallogin_account'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary'  => true,
                ],
                'Id'
            )
            ->addColumn(
                'type',
                Table::TYPE_TEXT,
                30,
                [
                    'nullable' => false,
                ],
                'Login type'
            )
            ->addColumn(
                'user_id',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false,
                ],
                'User Id'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'default'  => '0',
                ],
                'Customer Id'
            )
            ->addIndex($installer->getIdxName('plumrocket_sociallogin_account', ['type']), ['type'])
            ->addIndex($installer->getIdxName('plumrocket_sociallogin_account', ['user_id']), ['user_id'])
            ->addIndex($installer->getIdxName('plumrocket_sociallogin_account', ['customer_id']), ['customer_id'])
            ->addForeignKey(
                $installer->getFkName('plumrocket_sociallogin_account', 'customer_id', 'customer_entity', 'entity_id'),
                'customer_id',
                $installer->getTable('customer_entity'),
                'entity_id',
                Table::ACTION_CASCADE
            )
            ->setComment('Social Login Customer');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
