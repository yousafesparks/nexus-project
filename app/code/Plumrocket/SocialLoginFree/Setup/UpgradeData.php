<?php
/**
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2020 Plumrocket Inc. (https://plumrocket.com)
 * @license     https://plumrocket.com/license   End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Setup;

use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Plumrocket\SocialLoginFree\Helper\Config;
use Plumrocket\SocialLoginFree\Model\Account\Photo;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory
     */
    private $configDataCollectionFactory;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * UpgradeData constructor.
     *
     * @param \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory $configDataCollectionFactory
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory $configDataCollectionFactory,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->configDataCollectionFactory = $configDataCollectionFactory;
        $this->configWriter = $configWriter;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        /**
         * Version 3.0.0
         */
        if (version_compare($context->getVersion(), '3.0.0', '<')) {
            /** Copy previous config to the new path */
            $configurations = [];
            $path = 'psloginfree/general/enable_for';
            $fields = [
                'login'     => 'psloginfree/general/enable_for_login',
                'register'  => 'psloginfree/general/enable_for_register'
            ];

            foreach ($fields as $page => $fieldPath) {
                $values = $this->configDataCollectionFactory->create()
                    ->addFieldToFilter('path', ['eq' => $fieldPath])
                    ->getData();

                foreach ($values as $value) {
                    $configurations[$value['scope_id']]['scope'] = $value['scope'];

                    if ($value['value'] == 1) {
                        if (isset($configurations[$value['scope_id']]['value'])) {
                            $configurations[$value['scope_id']]['value'] .= ',' . $page;
                        } else {
                            $configurations[$value['scope_id']]['value'] = $page;
                        }
                    }
                }
            }

            foreach ($configurations as $scopeId => $configuration) {
                $this->configWriter->save($path, $configuration['value'], $configuration['scope'], $scopeId);
            }

            /** Remove hidden buttons */
            /** @var \Magento\Framework\App\Config\Value[] $sortableConfigs */
            $sortableConfigs = $this->configDataCollectionFactory->create()
                ->addFieldToFilter('path', ['eq' => Config::XML_PATH_BUTTON_SORT])->getItems();
            foreach ($sortableConfigs as $sortableConfig) {
                if ($value = $sortableConfig->getValue()) {
                    parse_str($value, $sortParams);
                    if (is_array($sortParams) && ! empty($sortParams['hidden'])) {
                        $this->configWriter->delete(
                            $sortableConfig->getPath(),
                            $sortableConfig->getScope(),
                            $sortableConfig->getScopeId()
                        );
                    }
                }
            }

            /** Add an attribute to the customer */
            $eavSetup->addAttribute(
                Customer::ENTITY,
                Photo::CUSTOMER_PHOTO_ATTRIBUTE_CODE,
                [
                    'type'         => 'varchar',
                    'label'        => 'Customer Photo',
                    'input'        => 'text',
                    'required'     => false,
                    'visible'      => false,
                    'user_defined' => false,
                    'position'     => 1000,
                    'system'       => 0,
                ]
            );
        }

        $setup->endSetup();
    }
}
