<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_AmpEmailApi
 * @copyright   Copyright (c) 2021 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\AmpEmailApi\Helper;

class Main extends \Plumrocket\Base\Helper\Base
{
    final public function getCustomerKey()
    {
        $customerKey = '73361845859a56e6332424622bb5f09134c3f28585';

        if (method_exists($this, 'getTrueCustomerKey')) {
            return $this->getTrueCustomerKey($customerKey);
        }

        return $customerKey;
    }
}
