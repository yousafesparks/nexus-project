<?php

namespace Esparksinc\Custom\Helper;

use Magento\Newsletter\Model\SubscriberFactory;



class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $httpContext;
    protected $_customerRepositoryInterface;
	protected $scopeConfig; 
    public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\Http\Context $httpContext,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
		  \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		array $data = []
   ){
		$this->httpContext = $httpContext;
		$this->_customerRepositoryInterface = $customerRepositoryInterface;
		$this->scopeConfig = $scopeConfig;

  
	}
	
	public function getCustomerId()
    {
	return $this->httpContext->getValue('customer_id');
    }


	

	

}