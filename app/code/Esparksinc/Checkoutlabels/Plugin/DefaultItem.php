<?php

namespace Esparksinc\Checkoutlabels\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote\Item;

class DefaultItem
{

    protected $productRepo;
    protected $_portohelper;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        \Smartwave\Porto\Helper\Data $_portohelper
    )
    {
        $this->productRepo = $productRepository;
        $this->_portohelper = $_portohelper;        
    }

    
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        $_product_label_config = $this->_portohelper->getConfig('porto_settings/product_label');

        $items = $result['totalsData']['items'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        for($i=0;$i<count($items);$i++){

            $quoteId = $items[$i]['item_id'];
            $result['quoteItemData'][$i]['sale_label'] = 0;
            $result['quoteItemData'][$i]['new_label'] = 0;
            $result['quoteItemData'][$i]['custitem_grade'] = 0;
            
            $quote = $objectManager->create('\Magento\Quote\Model\Quote\Item')->load($quoteId);
            $productId = $quote->getProductId();
            $_product = $objectManager->create('\Magento\Catalog\Model\Product')->load($productId);
            $product_label = "";
                            if($_product_label_config['sale_label']) {
                                $orgprice = $_product->getPrice();
                                $specialprice = $_product->getSpecialPrice();
                                $specialfromdate = $_product->getSpecialFromDate();
                                $specialtodate = $_product->getSpecialToDate();
                                $today = time();
                                if(!$specialprice)
                                    $specialprice = $orgprice;
                                if($specialprice < $orgprice) {
                                    if((is_null($specialfromdate) && is_null($specialtodate)) || ($today >= strtotime($specialfromdate) && is_null($specialtodate)) || ($today <= strtotime($specialtodate) && is_null($specialfromdate)) || ($today >= strtotime($specialfromdate) && $today <= strtotime($specialtodate))){
                                        if($_product_label_config['sale_label_percent']) {
                                            $save_percent = 100-round(($specialprice/$orgprice)*100);
                                            $product_label .= 'SALE'.' -'.$save_percent.'%';
                                            $result['quoteItemData'][$i]['sale_label'] = $product_label;
                                            $result['quoteItemData'][$i]['sale_color'] = 'background-color:#E10707';
                                        }
                                    }
                                }
                            }


                            if($_product_label_config['new_label']) {
                                $now = date("Y-m-d");
                                $newsFrom= substr($_product->getData('news_from_date'),0,10);
                                $newsTo=  substr($_product->getData('news_to_date'),0,10);
                                $new_arrival = $_product->getResource()->getAttribute('custitem_f3_new_arrival')->getFrontend()->getValue($_product);
                                if ($newsTo != '' || $newsFrom != ''){
                                    if (($newsTo != '' && $newsFrom != '' && $now>=$newsFrom && $now<=$newsTo) || ($newsTo == '' && $now >=$newsFrom) || ($newsFrom == '' && $now<=$newsTo)) {
                                        $product_label .= $_product_label_config['new_label_text'];
                                        $result['quoteItemData'][$i]['new_label'] = $product_label;
                                        $result['quoteItemData'][$i]['new_label_color'] = 'background-color:#62b959';
                                    }
                                }
                            }

                            $grade = strtoupper($_product->getData('custitem_grade_value'));
                            $grade = strtoupper($_product->getResource()->getAttribute('custitem_grade')->getFrontend()->getValue($_product));
                            if($grade == "PREMIUM")
                                $bk_color = 'background-color:#BB9BFF';
                            else if($grade == "PLUS")
                                $bk_color = 'background-color:#da7a2e';
                            else if($grade == "DYNAMIC")
                                $bk_color = 'background-color:#bb4e4e';
                            else if($grade == "VALUE")
                                $bk_color = 'background-color:#7d5b1e';
                            else if($grade == "SPECIAL")
                                $bk_color = 'background-color:#1a1f25';
                            else if($grade == "OEM RECLAIM A")
                                $bk_color = 'background-color:#55565a';
                            else if($grade == "OEM RECLAIM C")
                                $bk_color = 'background-color:#cacaca';
                            else if($grade == "NEW ORIGINAL")
                                $bk_color = 'background-color:#3967a5';
                            else
                            $bk_color = '#000000';                
                            
                            
                            
                            $result['quoteItemData'][$i]['custitem_grade'] = $_product->getResource()->getAttribute('custitem_grade')->getFrontend()->getValue($_product);
                            
                            $result['quoteItemData'][$i]['bk_color'] = $bk_color;
        }
      
        
        // $result['totalsData']['items'] = $items;
        return $result;
    }
}