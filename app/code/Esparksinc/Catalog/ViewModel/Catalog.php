<?php
namespace Esparksinc\Catalog\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Catalog\Model\CategoryRepository;


class Catalog implements ArgumentInterface
{
    public function __construct(CategoryRepository $categoryRepository) {

    $this->categoryRepository = $categoryRepository;
    }

    public function getCategoryImage($categoryId)
    {

        
        $category  = $this->categoryRepository->get($categoryId);
        // $categoryImage       = $category->getImageUrl();
        $categoryImage  = $category->getDescription();
        return $categoryImage;
    }
}