<?php

namespace W2Commerce\CatalogCustom\Block\Product\Renderer\Listing;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Product as CatalogProduct;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\ConfigurableProduct\Helper\Data;
use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Swatches\Helper\Data as SwatchData;
use Magento\Swatches\Helper\Media;
use Magento\Catalog\Helper\Image as HelperImage;
use Smartwave\Porto\Helper\Data as HelperPorto;
use Magento\Swatches\Model\SwatchAttributesProvider;

class Configurable extends \Magento\Swatches\Block\Product\Renderer\Listing\Configurable
{
    /**
     * @var \Magento\Framework\Locale\Format
     */
    protected $localeFormat;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable\Variations\Prices
     */
    private $variationPrices;

    private $helperPorto;

    private $helperImage;

    /**
     * @var \W2Commerce\CatalogCustom\Helper\Data
     */
    private $helperCustom;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var array
     */
    protected $outOfStock = [];

    /**
     * @var array
     */
    protected $productNames = [];

    /**
     * @param Context $context
     * @param ArrayUtils $arrayUtils
     * @param EncoderInterface $jsonEncoder
     * @param Data $helper
     * @param CatalogProduct $catalogProduct
     * @param CurrentCustomer $currentCustomer
     * @param PriceCurrencyInterface $priceCurrency
     * @param ConfigurableAttributeData $configurableAttributeData
     * @param SwatchData $swatchHelper
     * @param Media $swatchMediaHelper
     * @param \W2Commerce\CatalogCustom\Helper\Data $helperCustom
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param array $data
     * @param SwatchAttributesProvider|null $swatchAttributesProvider
     * @param \Magento\Framework\Locale\Format|null $localeFormat
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable\Variations\Prices|null $variationPrices
     * @param Resolver $layerResolver
     */
    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        EncoderInterface $jsonEncoder,
        Data $helper,
        CatalogProduct $catalogProduct,
        CurrentCustomer $currentCustomer,
        PriceCurrencyInterface $priceCurrency,
        ConfigurableAttributeData $configurableAttributeData,
        SwatchData $swatchHelper,
        Media $swatchMediaHelper,
        HelperImage $helperImage,
        HelperPorto $helperPorto,
        \W2Commerce\CatalogCustom\Helper\Data $helperCustom,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        array $data = [],
        SwatchAttributesProvider $swatchAttributesProvider = null,
        \Magento\Framework\Locale\Format $localeFormat = null,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable\Variations\Prices $variationPrices = null,
        Resolver $layerResolver = null
    ) {
        $this->helperCustom = $helperCustom;
        $this->helperPorto = $helperPorto;
        $this->helperImage = $helperImage;
        $this->stockRegistry = $stockRegistry;
        $this->localeFormat = $localeFormat ?: ObjectManager::getInstance()->get(
            \Magento\Framework\Locale\Format::class
        );
        $this->variationPrices = $variationPrices ?: ObjectManager::getInstance()->get(
            \Magento\ConfigurableProduct\Model\Product\Type\Configurable\Variations\Prices::class
        );
        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $helper,
            $catalogProduct,
            $currentCustomer,
            $priceCurrency,
            $configurableAttributeData,
            $swatchHelper,
            $swatchMediaHelper,
            $data,
            $swatchAttributesProvider,
            $localeFormat,
            $variationPrices,
            $layerResolver
        );
    }

    /**
     * Get product images for configurable variations
     *
     * @return array
     */
    /*protected function getOptionImages()
    {
        $images = [];

        foreach ($this->getAllowProducts() as $product) {
            $inStock = $this->stockRegistry->getStockItemBySku($product->getSku())->getIsInStock();
            if (!$inStock && !in_array($product->getId(), $this->outOfStock)) {
                $this->outOfStock[] = $product->getId();
            }
            $this->productNames[$product->getId()] = $product->getName();
            $productImages = $this->helper->getGalleryImages($product) ?: [];
            foreach ($productImages as $image) {
                $images[$product->getId()][] =
                    [
                        'img' => $image->getData('large_image_url')
                    ];
            }
        }

        return $images;
    }*/
    protected function getOptionImages()
    {
        $_imagehelper = $this->helperImage;
        $_portohelper = $this->helperPorto;
        $_category_config = $_portohelper->getConfig('porto_settings/category');
        $image_width = ($_category_config['ratio_width'])?$_category_config['ratio_width']:300;
        $image_height = ($_category_config['ratio_height'])?$_category_config['ratio_height']:300;
        $aspect_ratio = $_category_config['aspect_ratio'];
        $images = [];
        $image = 'category_page_list';

        foreach ($this->getAllowProducts() as $product) {
            $inStock = $this->stockRegistry->getStockItemBySku($product->getSku())->getIsInStock();
            if (!$inStock && !in_array($product->getId(), $this->outOfStock)) {
                $this->outOfStock[] = $product->getId();
            }
            $this->productNames[$product->getId()] = $product->getName();
            $productImages = $this->helper->getGalleryImages($product) ?: [];
            if($aspect_ratio)
                $productImage = $_imagehelper->init($product, $image)->constrainOnly(FALSE)->keepAspectRatio(TRUE)->keepFrame(FALSE)->resize($image_width);
             else
                $productImage = $_imagehelper->init($product, $image)->resize($image_width, $image_height);

            $productImageUrl = $productImage->getUrl();
            $images[$product->getId()][] =
                [
                    'img' => $productImageUrl //$image->getData('large_image_url')
                ];
        }
        return $images;
    }
    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function getJsonConfig()
    {
        $this->unsetData('allow_products');
        $store = $this->getCurrentStore();
        $currentProduct = $this->getProduct();

        $options = $this->helperCustom->getOptions($currentProduct, $this->getAllowProducts());
        $attributesData = $this->configurableAttributeData->getAttributesData($currentProduct, $options);
        $this->productNames[$currentProduct->getId()] = $currentProduct->getName();
        $config = [
            'attributes' => $attributesData['attributes'],
            'template' => str_replace('%s', '<%- data.price %>', $store->getCurrentCurrency()->getOutputFormat()),
            'currencyFormat' => $store->getCurrentCurrency()->getOutputFormat(),
            'optionPrices' => $this->getOptionPrices(),
            'priceFormat' => $this->localeFormat->getPriceFormat(),
            'prices' => $this->variationPrices->getFormattedPrices($this->getProduct()->getPriceInfo()),
            'productId' => $currentProduct->getId(),
            'chooseText' => __('Choose an Option...'),
            'images' => $this->getOptionImages(),
            'index' => isset($options['index']) ? $options['index'] : [],
            'outOfStock' => $this->outOfStock,
            'productNames' => $this->productNames
        ];

        if ($currentProduct->hasPreconfiguredValues() && !empty($attributesData['defaultValues'])) {
            $config['defaultValues'] = $attributesData['defaultValues'];
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return $this->jsonEncoder->encode($config);
    }
}
