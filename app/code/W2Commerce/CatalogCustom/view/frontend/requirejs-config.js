var config = {
    config: {
        mixins: {
            "Magento_Swatches/js/swatch-renderer" : {
                "W2Commerce_CatalogCustom/js/swatch-renderer": true
            }
        }
    },
    map: {
        '*': {
            'mage/collapsible': 'W2Commerce_CatalogCustom/js/mage/collapsible',
            'collapsible': 'W2Commerce_CatalogCustom/js/mage/collapsible'
        }
    },
};