<?php

namespace W2Commerce\CatalogCustom\Helper;

use Magento\Catalog\Model\Product\Image\UrlBuilder;
use Magento\Catalog\Helper\Image as ImageHelper;

class Data extends \Magento\ConfigurableProduct\Helper\Data
{
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @param ImageHelper $imageHelper
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param UrlBuilder $urlBuilder
     */
    public function __construct(
        ImageHelper $imageHelper,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        UrlBuilder $urlBuilder = null
    ) {
        $this->stockRegistry = $stockRegistry;
        parent::__construct(
            $imageHelper,
            $urlBuilder
        );
    }

    /**
     * Get Options for Configurable Product Options
     *
     * @param \Magento\Catalog\Model\Product $currentProduct
     * @param array $allowedProducts
     * @return array
     */
    public function getOptions($currentProduct, $allowedProducts)
    {
        $options = [];
        $allowAttributes = $this->getAllowAttributes($currentProduct);

        foreach ($allowedProducts as $product) {
            $productId = $product->getId();
            $inStock = $this->stockRegistry->getStockItemBySku($product->getSku())->getIsInStock();
            foreach ($allowAttributes as $attribute) {
                $productAttribute = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue = $product->getData($productAttribute->getAttributeCode());
                if ($product->isSalable() || !$inStock) {
                    $options[$productAttributeId][$attributeValue][] = $productId;
                }
                $options['index'][$productId][$productAttributeId] = $attributeValue;
            }
        }
        return $options;
    }
}
