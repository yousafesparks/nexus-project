<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_CPS
 */


namespace Amasty\CPS\Api\Indexer;

interface DataHandlerInterface
{
    /**
     * @return DataHandlerInterface
     */
    public function reindexAll();
}
