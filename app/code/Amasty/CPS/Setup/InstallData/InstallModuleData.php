<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_CPS
 */


namespace Amasty\CPS\Setup\InstallData;

class InstallModuleData
{
    /**
     * @var \Amasty\CPS\Api\Indexer\DataHandlerInterface
     */
    private $indexer;

    public function __construct(\Amasty\CPS\Api\Indexer\DataHandlerInterface $dataHandler)
    {
        $this->indexer = $dataHandler;
    }

    /**
     * Installs module data
     */
    public function execute()
    {
        $this->indexer->reindexAll();
    }
}
