<?php

namespace Nexuscellular\Netsuite\Helper;


use Nexuscellular\Netsuite\Helper\Attribute as attributeCellularHelper;
use Nexuscellular\Netsuite\Helper\Facets as FacetsCellularHelper;
use Nexuscellular\Netsuite\Helper\Apicall as apiCAllCellularHelper;
use Nexuscellular\Netsuite\Helper\Data as DataCellularHelper;
use Nexuscellular\Netsuite\Helper\Netsuiteproductstatus as netsuiteproductstatusHelper;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Catalog\Model\Product\Gallery\ReadHandler as galleryReadHandler;
use Magento\Catalog\Model\Product\Gallery\Processor as  imageProcessor;
use Magento\Catalog\Model\ResourceModel\Product\Gallery as productGallery;  
use Nexuscellular\Netsuite\Helper\Updateattributes as updateAttributesHelper;           
     
class Updateproducts extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_scopeConfig;
	protected $_updateAttrHelper;
	protected $ProductFactory;
	protected $attrhelper;
	protected $apiHelper; 
	protected $productInterfaceFactory;
	protected $_productCollectionFactory;
	protected $directoryList;
    protected $file;
	protected $_netsuiteFactory;
	protected $_productRepository;
	protected $_facetHelper;
	protected $_netsuiteproductstatusHelper;
	protected $_netsuiteProductFactory;
	protected $productRepositoryInterface;
	protected $productResourceModel;
	protected $updateRecordEntry;
	protected $helper;
	protected $_galleryReadHandler;
	protected $_imageProcessor;
	protected $_productGallery;
	protected $resourceConnection;
	public function __construct(
		\Magento\Catalog\Model\ProductFactory $productFactory,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, 
		attributeCellularHelper $attrHelper,
		\Nexuscellular\Netsuite\Model\NetsuiterecordFactory $netsuiteFactory,
		\Nexuscellular\Netsuite\Model\ProductinsertedFactory $netsuiteProductFactory,
		\Nexuscellular\Netsuite\Model\UpdaterecordFactory $netsuiteUpdaterecordFactory,
		DirectoryList $directoryList,
		apiCAllCellularHelper $apiHelper,
		DataCellularHelper $NXHelper,
		updateAttributesHelper $updateAttributesHelper,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		FacetsCellularHelper $facetHelper,
		netsuiteproductstatusHelper $netsuiteproductstatusHelper,
		\Magento\Catalog\Api\Data\ProductInterfaceFactory $productInterfaceFactory,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
		\Magento\Catalog\Model\ResourceModel\Product $productResourceModel,
		WriterInterface $configWriter,
		galleryReadHandler $galleryReadHandler,
		imageProcessor $imageProcessor,
		productGallery $productGallery,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		File $file
    )
    {
		$this->ProductFactory = $productFactory;
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->_productRepository = $productRepository;
		$this->attrhelper = $attrHelper;
		$this->_netsuiteFactory = $netsuiteFactory;
		$this->_netsuiteProductFactory = $netsuiteProductFactory;
		$this->productInterfaceFactory = $productInterfaceFactory;
		$this->directoryList = $directoryList;
		$this->apiHelper = $apiHelper;
        $this->file = $file;
		$this->updateRecordEntry = $netsuiteUpdaterecordFactory;
		 $this->productRepositoryInterface = $productRepositoryInterface;
		$this->_facetHelper = $facetHelper;
		 $this->productResourceModel = $productResourceModel;
		 $this->config = $configWriter;
		 $this->_scopeConfig =  $scopeConfig;
		  $this->helper =  $NXHelper;
		$this->_galleryReadHandler = $galleryReadHandler ;
		$this->_imageProcessor = $imageProcessor;
		$this->_productGallery = $productGallery;
		$this->_netsuiteproductstatusHelper = $netsuiteproductstatusHelper;
		$this->_updateAttrHelper = $updateAttributesHelper;
		 $this->resourceConnection = $resourceConnection;
    }
	
	 public function pushUpdatedProductsToMagento(){
		 $iterate = 1;
		 if($this->isScriptInProcess())
			 return true;
		
		$this->callNetsuiteRecords();
		$this->updateStatusToComplete();
	}
	protected function callNetsuiteRecords(){
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/netsuite_updateproduct.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		
		$size = $this->CheckNetsuitePagination();
		if($size == 0){
			$time = $this->getLastUpdateProductTime();
			$previousTime = '"' . $time['t1'] . '"';
			//$previousTime = '"2020-07-28 4:43:00"';
			//$previousTime = '"2020-07-27 1:43:00"';
			$currentTime = '"' . $time['t2'] . '"';
			//$currentTime = '"2020-07-27 18:07:31"';
			
			echo 'lastModifiedDate AFTER '.$previousTime.' AND lastModifiedDate BEFORE '.$currentTime;
			$paramter = rawurlencode('lastModifiedDate AFTER '.$previousTime.' AND lastModifiedDate BEFORE'.$currentTime);
			$url = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryItem?q='.$paramter;
			$response = $this->apiHelper->_makeRestApiCall($url);
			
				if(isset($response['count']) && $response['count'] > 0){
					$this->insertRecords($response);
				}else{
					return true;
				}
			}
			else if($size >=1){
				$url = $this->_netsuiteproductstatusHelper->callUpdateProductUrl();
				
				if($url == null) // finish updation
					return true;
				 $response = $this->apiHelper->_makeRestApiCall($url);
				 
			}
		if(!empty($response)){ 
			if(isset($response['count']) && $response['count'] > 0){
				$items = $response['items'];
				foreach($items as $key => $item){ 
				 $logger->info($key."====>".$item['id']."=============");
				echo $key."====>".$item['id']."=============<br/>";
				 	$UrlOfNetsuiteInventoryItem = $item['links'][0]['href']."?expandSubResources=true";
					$response = $this->apiHelper->_makeRestApiCall($UrlOfNetsuiteInventoryItem);
					if(isset($response['o:errorCode']) && $response['o:errorCode'] == "UNEXPECTED_ERROR" ){	
					$logger->info("Error in ===================".$item['id']."=========");
					$this->updateIterateProducts($key);
					}else{
					$this->_updateNewProducts($response);
					$this->updateIterateProducts($key);
					}
					/*  echo $key."====>".$item['id']."=============<br/>";
					$this->updateIterateProducts($key); */
				}
				
			}else{
				return true;
			}
		}
		$this->callNetsuiteRecords();
	}
	public function updateIterateProducts($key){
			$key = $key +1;
			$tableName = $this->getTableName('netsuite_updateproducts_pagination');
				$sql = "UPDATE $tableName SET `finished` = {$key} WHERE $tableName.`status` = 'processing' ";
				$this->resourceConnection->getConnection()->query($sql);
	}
	protected function CheckNetsuitePagination(){
		$tableName = $this->getTableName('netsuite_updateproducts_pagination');
		 $query = "SELECT id FROM " . $tableName . " WHERE status = 'processing' limit 1";
		 $results = $this->resourceConnection->getConnection()->fetchOne($query);
		 if(empty($results))
		 	$results = 0;
		 return $results;
	}
	protected  function  updateStatusToComplete(){
		$netsuiteUpdateFactory = $this->updateRecordEntry->create();
		$collection = $netsuiteUpdateFactory->getCollection();
		$collection->addFieldToFilter("entity","updateproduct");
		$collection->addFieldToFilter("status","processing");
		
		if($collection->getSize()){
			$id = $collection->getFirstItem()->getId();
			$netsuiteUpdateFactory = $this->updateRecordEntry->create()->load($id);
			$netsuiteUpdateFactory->setStatus("completed");
			$netsuiteUpdateFactory->save();
			unset($netsuiteUpdateFactory);
		}
	}
	protected  function  isScriptInProcess(){
		$netsuiteUpdateFactory = $this->updateRecordEntry->create();
		$collection = $netsuiteUpdateFactory->getCollection();
		$collection->addFieldToFilter("entity","updateproduct");
		$collection->addFieldToFilter("status","processing");
		return $collection->getSize();
	}
	public function _updateNewProducts($response){
			if(!empty($response)&& count($response) > 0){
				if(isset($response['matrixType']) && $response['matrixType'] == "CHILD"){
					
					if($this->isExistProduct($response))
					$this->UpdateSimpleProduct($response);
				 else
					$this->helper->createSimpleProduct($response);
					
				}else if(!isset($response['matrixType'])){
					if($this->isExistProduct($response))
						$this->UpdateSimpleProduct($response);	
						 else
					$this->helper->createSimpleProduct($response);
				}else if(isset($response['matrixType']) && $response['matrixType'] == "PARENT"){
					if($this->isExistProduct($response))
					$this->UpdateConfigurableProduct($response,$configAttributeProdObj = array());
					else
					$this->helper->createConfigurableProduct($response,$configAttributeProdObj = array());
				}
			}
	}
	protected function _convertWeightToLbs($response){
		$weight = $response["weight"];
		if($response["weightUnit"] == 2){ // oz
			$weight = $response["weight"] / 16;
		}else if($response["weightUnit"] == 3){ // kg
			$weight = $response["weight"] * 2.205;
		}else if($response["weightUnit"] == 4){ // gm
			$weight = $response["weight"] /454;
		}
		return round($weight,2);
	}
	public function isExistProduct($response){
		$collection = $this->_productCollectionFactory->create();
		//echo $response['id'];
		$data = $collection->addAttributeToSelect(array('internal_id','entity_id'))->addAttributeToFilter('internal_id',$response['id']);
		$productId = null;
		if($collection->getSize()){
			$productId = $data->getFirstItem()->getId();
		}
		return $productId;
	}
	public function UpdateSimpleProduct($response){
			$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/netsuite_updateproduct.log');
			$logger = new \Zend\Log\Logger();
			$logger->addWriter($writer);
			try{
				$starttime = microtime(true);
				$isExistProduct = $this->isExistProduct($response);
				if(!$isExistProduct)
					return true;
					$logger->info("updating simple Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']);
				echo "updating simple Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']. "<br/>";
				$productId = $isExistProduct;
				$_product = $this->ProductFactory->create()->reset();
				$this->productResourceModel->load($_product, $productId);
				$_product->setStoreId(0);
				$_product = $this->_updateAttrHelper->saveAttribute($this->productResourceModel,$_product,$response);
				$this->_updateAttrHelper->updateSku($productId,$response['itemId']);
			
		 }catch(exception $e){
			 echo $message = $e->getMessage();
		 }
		 $finishTime = (microtime(true) - $starttime);
		 $logger->info("Finshed Simple Product =====".$response['id']." duration: =========".$finishTime);
			echo "Save product duration: ".$finishTime." seconds\n";
		 //return true;
	}
	
	protected function _importImagesToProduct($response,$_product) {
		if(isset($response["custitemzg_image_filename"])){
				
				 $imageUrl = $response["custitemzg_image_filename"];
				if($response["custitemzg_num_images"] > 1){
					$imageUrl = explode(",",$response["custitemzg_image_filename"]);
				}
				$tmpDir = $this->getMediaDirTmpDir();
				$this->file->checkAndCreateFolder($tmpDir);
				$imageType = ['jpeg','jpg','png'];
				if(is_array($imageUrl)){
					$_product = $this->unlinkExistingImages($_product);
						foreach($imageUrl as $image){
							$newFileName = $tmpDir . baseName($image);
							$result = $this->file->read($image, $newFileName);
							if ($result) {
								$_product->setImage($newFileName);
								$_product->addImageToMediaGallery($newFileName, array('image', 'small_image', 'thumbnail'), false, false);
							}
						}
				}else{ // for a single file
					$newFileName = $tmpDir . baseName($imageUrl);
					$result = $this->file->read($imageUrl, $newFileName);
					if ($result) {
						$_product = $this->unlinkExistingImages($_product);
						$_product->addImageToMediaGallery($newFileName, array('image', 'small_image', 'thumbnail'), false, false);
					}
				}
		} 
		return $_product;
	}
	protected function unlinkExistingImages($_product){
		$gallery = $this->_galleryReadHandler;
		$gallery->execute($_product);
		$imageProcessor = $this->_imageProcessor;
		$productGallery = $this->_productGallery;
		$images = $_product->getMediaGalleryImages();
		foreach($images as $child) {
			$productGallery->deleteGallery($child->getValueId());
			$imageProcessor->removeImage($_product, $child->getFile());
		}
		return $_product;
	}
	protected function getMediaDirTmpDir(){
        return $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'tmp';
    }
	public function getPrice($priceArray){
		$calculatedPrice=[];
		$items = $priceArray['items'];
		$basePrice = 0;
		$calculatedPrice['actual_price'] = null;
		$calculatedPrice['base_price'] = 0;
		foreach($items as $price){
			if($price['priceLevel']['id'] == 5) // actual price
				$calculatedPrice['actual_price'] = $price['price'];
			 if($price['priceLevel']['id'] == 1) // base price
				$calculatedPrice['base_price'] = $price['price'];
		}
		return $calculatedPrice; 
	}
	public function UpdateConfigurableProduct($response,$configAttributeProdObj = array()){
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/netsuite_updateproduct.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
			try {
				$starttime = microtime(true);
				$isExistProduct = $this->isExistProduct($response);
				if(!$isExistProduct)
					return true;
					$logger->info("updating configurable Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']);
				echo "updating configurable Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']. "<br/>";
				$productId = $isExistProduct;
				$_product = $this->ProductFactory->create()->reset();
				$this->productResourceModel->load($_product, $productId);
				$_product->setStoreId(0);
				$_product = $this->_updateAttrHelper->saveAttribute($this->productResourceModel,$_product,$response);
				$this->_updateAttrHelper->updateSku($productId,$response['itemId']);
				$finishTime = (microtime(true) - $starttime);
				$logger->info("Finshed Configurable Product =====".$response['id']." duration: =========".$finishTime);
				   echo "Save product duration: ".$finishTime." seconds\n";
			} catch (Exception $ex) {
				print_r($ex->getMessage());
				
			}	
	}
	protected  function getLastUpdateProductTime(){
		$timeArray = array();
		$netsuiteUpdateFactory = $this->updateRecordEntry->create();
		$collection = $netsuiteUpdateFactory->getCollection();
		$collection->addFieldToFilter("entity","updateproduct");
		$data = $collection;
		if(!$collection->getSize()){
			date_default_timezone_set('Etc/GMT+4');
			 $time = date("Y-m-d H:i:s");
			$netsuiteUpdateFactory = $this->updateRecordEntry->create();
			$netsuiteUpdateFactory->setEntity("updateproduct");
			$netsuiteUpdateFactory->setLastTimeSync($time);
			$netsuiteUpdateFactory->setStatus("processing");
			$netsuiteUpdateFactory->save();
			unset($netsuiteUpdateFactory);
			$timeArray = [];
			$timeArray['t2'] = strtotime($time);
			$timeArray['t1'] = (string)date("Y-m-d H:i:s",strtotime('-45 minutes', $timeArray['t2']));
			$timeArray['t2'] = (string)date("Y-m-d H:i:s",$timeArray['t2']);
			return $timeArray;
		}else if($collection->getSize()){
			$netsuiteUpdateFactory = $this->updateRecordEntry->create();
			$collection = $netsuiteUpdateFactory->getCollection();
			$collection->addFieldToFilter("entity","updateproduct");
			$collection->addFieldToFilter("status","completed");
			$id = $collection->getFirstItem()->getId();
			$from_start_time = $collection->getFirstItem()->getLastTimeSync(); // from start time
			date_default_timezone_set('Etc/GMT+4');
			$to_start_time = date("Y-m-d H:i:s"); // to end_time
			$netsuiteUpdateFactory = $this->updateRecordEntry->create()->load($id);
			$netsuiteUpdateFactory->setLastTimeSync($to_start_time);
			$netsuiteUpdateFactory->setStatus("processing");
			$netsuiteUpdateFactory->save();
			 $timeArray = [];
			$timeArray['t1'] =$from_start_time;
			$timeArray['t2'] = $to_start_time;
			unset($netsuiteUpdateFactory);
			return $timeArray;
		}
	}
	protected function insertRecords($response){
		if(isset($response['links'])){
			foreach($response['links'] as $links){
				
				$rel = $links['rel']; 
				if($rel == "next") continue;
				
				$status = "pending";
				if($rel == "self")
					$status = 'processing';	
				//if($rel == "last" or $rel == "next") continue;
				$url = $links['href']; 
				
				$splitString = explode("?",$url);
				
				$splitString = explode('&',$splitString[1]);
				$newData = explode('=',$splitString[0]);
				$limitLabel = $newData[0];
				$limitTotal = $newData[1];
				$offsetLabel = $splitString[0];
				$offsetTotal =explode('=',$splitString[1]);
				$tableName = $this->getTableName('netsuite_updateproducts_pagination');
				$updateProductPagination = [
							'url' =>$url,
							'status' => $status,
							'count' => $response['count'],
							'offset' => $offsetTotal[1],
							'limit'=>$limitTotal,
							'rel'=>$rel
						];
				$this->resourceConnection->getConnection()
				->insert($tableName, $updateProductPagination);
			}
			return $url;
		}
	}
	 /**
     * Get Table name using direct query
     */
    public function getTablename($tableName)
    {
        /* Create Connection */
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
    }
	
}
