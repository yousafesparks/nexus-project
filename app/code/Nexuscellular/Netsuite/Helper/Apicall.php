<?php

namespace Nexuscellular\Netsuite\Helper;
class Apicall extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $tableFactory;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    protected $optionLabelFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    protected $optionFactory;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository
     * @param \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory
     * @param \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement
     * @param \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory
     * @param \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory
    ) {
        parent::__construct($context);

        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
    }

	public function _makeRestApiCall($url){

		$netsuiteUrl = $url;
		$netsuiteAccount = '';
		$netsuiteConsumerKey = '';
		$netsuiteConsumerSecret = '';
		$netSuiteTokenID = '';
		$netsuiteTokenSecret = '';
		$oauth_nonce = md5(mt_rand());
		$oauth_timestamp = time();
		$oauth_signature_method = 'HMAC-SHA1';
		$oauth_version = "1.0";
				
		$baseString = $this->restletBaseString("GET",
												$netsuiteUrl,
												$netsuiteConsumerKey,
												$netSuiteTokenID,
												$oauth_nonce,
												$oauth_timestamp,
												$oauth_version,
												$oauth_signature_method,null);


		$key = rawurlencode($netsuiteConsumerSecret) .'&'. rawurlencode($netsuiteTokenSecret);

		 $signature = base64_encode(hash_hmac('sha1', $baseString, $key, true)); 
		 $header = 'Authorization: OAuth '
				 .'realm="' .rawurlencode($netsuiteAccount) .'", '
				 .'oauth_consumer_key="' .rawurlencode($netsuiteConsumerKey) .'", '
				 .'oauth_token="' .rawurlencode($netSuiteTokenID) .'", '
				 .'oauth_nonce="' .rawurlencode($oauth_nonce) .'", '
				 .'oauth_timestamp="' .rawurlencode($oauth_timestamp) .'", '
				 .'oauth_signature_method="' .rawurlencode($oauth_signature_method) .'", '
				 .'oauth_version="' .rawurlencode($oauth_version) .'", '
				 .'oauth_signature="' .rawurlencode($signature) .'"';
				 
				 
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => $netsuiteUrl,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  // CURLOPT_FAILONERROR => true,
			  CURLOPT_SSL_VERIFYHOST=>false,
			  CURLOPT_SSL_VERIFYPEER=>false,
			  CURLOPT_CUSTOMREQUEST => "GET",
			   CURLOPT_HTTPHEADER => array(
			   $header,
			   "content-type: application/json"
			  ),
		 
		));

		$response = curl_exec($curl);

		curl_close($curl);
		
		$product = json_decode($response, true);
		return $product;
				 

	}
	public function restletBaseString($httpMethod, $url, $consumerKey, $tokenKey, $nonce, $timestamp, $version, $signatureMethod, $postParams){
	  //http method must be upper case
	  $baseString = strtoupper($httpMethod) .'&';
	  
	  //include url without parameters, schema and hostname must be lower case
	  if (strpos($url, '?')){
		$baseUrl = substr($url, 0, strpos($url, '?'));
		$getParams = substr($url, strpos($url, '?') + 1);
	  } else {
	   $baseUrl = $url;
	   $getParams = "";
	  }
	  $hostname = strtolower(substr($baseUrl, 0,  strpos($baseUrl, '/', 10)));
	  $path = substr($baseUrl, strpos($baseUrl, '/', 10));
	  $baseUrl = $hostname . $path;
	  $baseString .= rawurlencode($baseUrl) .'&';
	  
	  //all oauth and get params. First they are decoded, next alphabetically sorted, next each key and values is encoded and finally whole parameters are encoded
	  $params = array();
	  $params['oauth_consumer_key'] = array($consumerKey);
	  $params['oauth_token'] = array($tokenKey);
	  $params['oauth_nonce'] = array($nonce);
	  $params['oauth_timestamp'] = array($timestamp);
	  $params['oauth_signature_method'] = array($signatureMethod);
	  $params['oauth_version'] = array($version);
	   
	  foreach (explode('&', $getParams ."&". $postParams) as $param) {
		$parsed = explode('=', $param);
		if ($parsed[0] != "") {
		  $value = isset($parsed[1]) ? urldecode($parsed[1]): "";
		  if (isset($params[urldecode($parsed[0])])) {
			array_push($params[urldecode($parsed[0])], $value);
		  } else {
			$params[urldecode($parsed[0])] = array($value);
		  }
		}
	  }
	   
	  //all parameters must be alphabetically sorted
	  ksort($params);
	   
	  $paramString = "";
	  foreach ($params as $key => $valueArray){
		//all values must be alphabetically sorted
		sort($valueArray);
		foreach ($valueArray as $value){
		  $paramString .= rawurlencode($key) . '='. rawurlencode($value) .'&';
		}
	  }
	  $paramString = substr($paramString, 0, -1);
	  $baseString .= rawurlencode($paramString);
	  return $baseString;
	}
	
}