<?php
namespace Nexuscellular\Netsuite\Helper;

//require_once('lib/internal/PHPToolkit_2020_1/samples/saved_search.php');
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;
class Soapcall extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_logger;
	protected $mediaDirectoryPath;
	protected $storeManager;
	protected $jsonHelper;
	protected $resourceConnection;
	
    public function __construct(
	\Magento\Framework\Filesystem $filesystem,
	\Magento\Framework\Json\Helper\Data $jsonHelper,
	\Magento\Store\Model\StoreManagerInterface $storeManager,
	\Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
		$this->mediaDirectoryPath = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
		$this->jsonHelper 	= $jsonHelper;
		$this->storeManager = $storeManager;
		$this->resourceConnection = $resourceConnection;
    }
	public function makeSoapApiCall(){
		
		/* if($this->isScriptInProcess() > 0)
			return true; */
		
		$filePath = 'netsuite/updaterecords/1596644433.js';
		$file = $this->getBaseMediaUrl().$filePath;
		//$this->_upsertRecord('updateattributes',$filename,$result,'processing');
		$data = json_decode(file_get_contents($file),true);
		unset($data['totalPages']);
				unset($data['searchId']);
				unset($data['timezone']);
		//echo "<pre>";print_r($data);die();
		return $data;
		$data = '';
		$result = $this->getSearchDateFilter();
		if(count($result) > 0){
			if($result['totalPages'] >= 1){
				$searchId = $result['searchId'];
				$data = $this->getAttrbiutesDataFromNetsuite($result);
				
				unset($data['totalPages']);
				unset($data['searchId']);
				unset($data['timezone']);
			}
		}else{
			echo "empty data";
			$data = false;
		}
		
	 	return $data;
	}
	protected  function  isScriptInProcess(){
		$tableName = $this->getTableName('netsuite_update_products_soap_api');
		 $query = "SELECT id FROM " . $tableName . " WHERE entity = 'updateattributes' AND status = 'processing' limit 1";
		 $results = $this->resourceConnection->getConnection()->fetchOne($query);
		 if(empty($results))
		 	$results = 0;
		 return $results;
	}
	public function getAttrbiutesDataFromNetsuite($result){
		
		
		$searchId = $result['searchId'];
		$totalPages = $result['totalPages'];
		if($totalPages > 1){
			$pageIndex = 2;
			for($i=$pageIndex;$i<=$totalPages;$i++){
				$request = new \SearchMoreWithIdRequest();
				$request->searchId = $searchId;
				$request->pageIndex = $i;
				$service = new \NetSuiteService();
				$searchResponse = $service->searchMoreWithId($request);
				if ($searchResponse->searchResult->status->isSuccess) {
					$rowData = $this->makeDecorateArray($searchResponse,$pageIndex,$result);
					$result[$i] = $rowData;
				}
			}
		}
		$time = time(); 
		$filename = $time.'.js';
		$filePath = 'netsuite/updaterecords/'.$filename;
		$this->createProductJsFile($filePath,$result);
		$file = $this->getBaseMediaUrl().$filePath;
		$this->_upsertRecord('updateattributes',$filename,$result,'processing');
		return $data = json_decode(file_get_contents($file),true);
		
	}
	protected function _upsertRecord($entity,$import_filename,$result,$status){
		$size = $this->countRecords();
		$timezone = $result["timezone"];
		if($size == 0){
			$tableName = $this->getTableName('netsuite_update_products_soap_api');
			$updateProductPagination = [
				'entity' =>$entity,
				'status' => $status,
				'totalproducts' => $result['totalRecords'],
				'finishedproducts' => 0,
				'import_file_path' => $import_filename,
				'created_at'=>$timezone["current_local_time"],
				'last_time_sync'=>$timezone["current_local_time"]
			];
			$this->resourceConnection->getConnection()
			->insert($tableName, $updateProductPagination);
		}else{
			$tableName = $this->getTableName('netsuite_update_products_soap_api');
				$sql = "UPDATE $tableName SET `status` = '".$status."', `created_at` ='".$timezone['current_local_time']."',`finishedproducts` = 0,`totalproducts` ='".$result['totalRecords']."' , `import_file_path` = '".$import_filename."' , `last_time_sync` ='".$timezone['current_local_time']."'  WHERE $tableName.`entity` = '".$entity."' ";
				$this->resourceConnection->getConnection()->query($sql);
		}
	}
		protected function countRecords(){
		$tableName = $this->getTableName('netsuite_update_products_soap_api');
		 $query = "SELECT id FROM " . $tableName . " WHERE entity = 'updateattributes' limit 1";
		 $results = $this->resourceConnection->getConnection()->fetchOne($query);
		 if(empty($results))
		 	$results = 0;
		 return $results;
	}
	public function getSearchDateFilter(){
		$result = [];
		$service = new \NetSuiteService();
		$service->setSearchPreferences(false, 500);
		
		
		$timeZone = $this->getUtcTime();
		//echo "<pre>";print_r($timeZone);
		/* date_default_timezone_set('America/Los_Angeles');
		echo date('Y-m-d H:i:s');die(); */
		$searchField = new \SearchDateField();
		$searchField->searchValue = "2020-8-4T16:30:00Z";
		$searchField->searchValue2 = "2020-08-04T16:32:00Z";
		$searchField->operator = "within";
		$ItemSearchBasic = new \ItemSearchBasic();
		$ItemSearchBasic->lastModifiedDate = $searchField;

		$ItemSearch = new \ItemSearch();
		$ItemSearch->parentJoin = $ItemSearchBasic;

/* $ItemSearchRowBasic = new \ItemSearchRowBasic();
		$ItemSearchRowBasic->basic = ;  */
		
	/* $ItemSearchRow = new \ItemSearchRow();
		$ItemSearchRow->parentJoin = $ItemSearchRowBasic; */
		
		$search = new \ItemSearchAdvanced();
		$search->savedSearchId = "1704";
		//$search->criteria = $ItemSearch;
//$search->columns = ItemSearchRow
		$request = new \SearchRequest();
		$request->searchRecord = $search;

		$searchResponse = $service->search($request);
		//echo "<pre>";print_r($searchResponse);die();
		$pageIndex = 1;
		$rowData = array();
		$result["timezone"] = $timeZone;
		if($searchResponse->searchResult->status->isSuccess && $searchResponse->searchResult->totalRecords > 0) 
		{
			$result["totalRecords"] = $searchResponse->searchResult->totalRecords;
			$result['totalPages'] = $searchResponse->searchResult->totalPages;
			$result['searchId'] = $searchResponse->searchResult->searchId;
			$rowData = $this->makeDecorateArray($searchResponse,$pageIndex,$result);
			$result[$pageIndex] = $rowData;
			
		}else{
			$filename = '';
			$result['totalRecords'] = 0;
			$this->_upsertRecord('updateattributes',$filename,$result,'completed');
			unset($result["timezone"]);
			unset($result["totalRecords"]);
		}
		return $result;
	}

	protected function getLastTimeSync(){
		$tableName = $this->getTableName('netsuite_update_products_soap_api');
		 $query = "SELECT * FROM " . $tableName . " WHERE entity = 'updateattributes' and status = 'completed' ";
		 $results = $this->resourceConnection->getConnection()->fetchAll($query);
		 if(empty($results))
		 	$results = 0;
		 return $results;
	}
	protected function getUtcTime(){
		$timeZone = [];
		$results = $this->getLastTimeSync();
		if($results > 0){
			$last_time_sync = $results[0]['last_time_sync'];
			$timeZone['previous_local_time'] = $last_time_sync;
			date_default_timezone_set('Etc/GMT+4');
			$datetime = date("Y-m-d H:i:s");
			$tz_from = 'America/Toronto';
			$tz_to = 'UTC';
			$format = 'Y-m-d\TH:i:s';

			$dt = new \DateTime($datetime, new \DateTimeZone($tz_from));
			$dt->setTimeZone(new \DateTimeZone($tz_to));
			$timeZone['current_local_time'] = $datetime;
			$timeZone['current_utc_time'] = $dt->format($format);
			
			/* set local time to UTC time in previous time */
			$datetime = $timeZone['previous_local_time'];
			$tz_from = 'America/Toronto';
			$tz_to = 'UTC';
			$format = 'Y-m-d\TH:i:s';

			$dt = new \DateTime($datetime, new \DateTimeZone($tz_from));
			$dt->setTimeZone(new \DateTimeZone($tz_to));
			$timeZone['previous_utc_time'] = $dt->format($format);
		}else{
			date_default_timezone_set('Etc/GMT+4');
			$datetime = date("Y-m-d H:i:s");
			$tz_from = 'America/Toronto';
			$tz_to = 'UTC';
			$format = 'Y-m-d\TH:i:s';

			$dt = new \DateTime($datetime, new \DateTimeZone($tz_from));
			$dt->setTimeZone(new \DateTimeZone($tz_to));
			$timeZone['current_local_time'] = $datetime;
			$timeZone['previous_local_time'] = (string)date("Y-m-d H:i:s",strtotime('-45 minutes', strtotime($timeZone['current_local_time'])));
			$timeZone['current_utc_time'] = $dt->format($format);
			
			
			$datetime = $timeZone['previous_local_time'];
			$tz_from = 'America/Toronto';
			$tz_to = 'UTC';
			$format = 'Y-m-d\TH:i:s';

			$dt = new \DateTime($datetime, new \DateTimeZone($tz_from));
			$dt->setTimeZone(new \DateTimeZone($tz_to));
			$timeZone['previous_utc_time'] = $dt->format($format);
		}
		
		return $timeZone;
	}
	protected function makeDecorateArray($searchResponse,$pageIndex,$result){
		$rowData = [];
		foreach($searchResponse->searchResult->searchRowList->searchRow as $key => $ItemSearchRow){
			$internalObj = $this->getInternalData($ItemSearchRow);
			$rowData[$key] = $this->getInternalId($internalObj);
		}
		return $rowData;
	}
	protected function getInternalData($ItemSearchRow){
		foreach($ItemSearchRow as $ItemSearchRowBasic){
					return $ItemSearchRowBasic->internalId;
				}
	}
	protected function getInternalId($internalIdObj){
		$data = json_decode(json_encode($internalIdObj,true),true);
		return $data[0]['searchValue']['internalId'] ;
		}
	public function createProductJsFile($fileName, $productArray){
        try {unset($productArray["timezone"]);
			$productArray =  json_encode($productArray);
            $this->mediaDirectoryPath->writeFile($fileName,$productArray);
        } catch (\Exception $e) {
           echo $e->getMessage();
        }
    }
	 public function getBaseMediaUrl(){
        return $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }
	public function getTablename($tableName)
    {
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
    }
	
}