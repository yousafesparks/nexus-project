<?php

namespace Nexuscellular\Netsuite\Helper;
use Nexuscellular\Netsuite\Helper\Apicall as apiCAllCellularHelper;
class Netsuiteproductstatus extends \Magento\Framework\App\Helper\AbstractHelper
{
   
	protected $_netsuiteFactory;
   protected $resourceConnection;
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
		apiCAllCellularHelper $apiHelper,
		\Nexuscellular\Netsuite\Model\NetsuiterecordFactory $netsuiteFactory,
		\Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);

		$this->apiHelper = $apiHelper;
		$this->_netsuiteFactory = $netsuiteFactory;
		$this->resourceConnection = $resourceConnection;
    }

	public function getNextProductUrl(){
			
			$url = $this->setStatusChangeToCompleted();
			return $url;
		}
	public function getLastUrl(){
		$url = null;
		$tableName = $this->getTableName('netsuite_updateproducts_pagination');
		 $query = "SELECT url FROM " . $tableName . " WHERE status = 'pending' AND rel= 'last' limit 1";
		 $results = $this->resourceConnection->getConnection()->fetchAll($query);
		 if(count($results)>0)
			 $url = $results[0]['url'];
		 return $url;
	}
	public function callUpdateProductUrl(){
		
		$tableName = $this->getTableName('netsuite_updateproducts_pagination');
		 $query = "SELECT * FROM " . $tableName . " WHERE status = 'processing' limit 1";
		 $results = $this->resourceConnection->getConnection()->fetchAll($query);
		 foreach($results as $data){
			 $id = $data['id'];
			 $rel = $data['rel'];
			 $url = $data['url'];
			 $count = $data['count'];
			 $finished = $data['finished'];
			 if($count == $finished){
				$tableName = $this->getTableName('netsuite_updateproducts_pagination');
				 $sql = "UPDATE $tableName SET `status` = 'completed' WHERE $tableName.`id` = $id";
				$this->resourceConnection->getConnection()->query($sql);
				
				if($this->getLastUrl() != null && $this->getLastUrl() == $url){
					 $sql = "UPDATE $tableName SET `status` = 'completed' WHERE $tableName.`rel` = 'last' ";
				$this->resourceConnection->getConnection()->query($sql);
					return null;
				}else if($this->getLastUrl() == null){
					return null;
				}
				
				if($rel == "next")
					 $url = $this->callUpdateNewUrls($url);
				else if($rel == "self")
					 $url = $this->callUpdateNewUrls($url);
			 }else{
				 return $url;
			 }
		 }
		 return $url;
	}
	protected function callUpdateNewUrls($url){
		$response = $this->apiHelper->_makeRestApiCall($url);
			return $url = $this->insertUpdateProductNewUrl($response);
		
	}
	protected function insertUpdateProductNewUrl($response){
		if(isset($response['links'])){
			foreach($response['links'] as $links){
				$rel = $links['rel']; 
				$pagePagination = array('last','previous','first','self');
				if(in_array($rel,$pagePagination)) continue;
				$url = $links['href']; 
				
				$splitString = explode("?",$url);
				$splitString = explode('&',$splitString[1]);
				$newData = explode('=',$splitString[0]);
				$limitTotal = $newData[1];
				$offsetTotal =explode('=',$splitString[1]);
				
					$tableName = $this->getTableName('netsuite_updateproducts_pagination');
				$updateProductPagination = [
							'url' =>$url,
							'status' => 'processing',
							'count' => $response['count'],
							'offset' => $offsetTotal[1],
							'limit'=>$limitTotal,
							'rel'=>$rel
						];
				$this->resourceConnection->getConnection()
				->insert($tableName, $updateProductPagination);
				
			}
			return $url;
		}
	}
	protected function setStatusChangeToCompleted(){
		$netsuiteRecord = $this->_netsuiteFactory->create();
			$collection = $netsuiteRecord->getCollection();
			$collection->addFieldToFilter('status','processing');
			$id = $collection->getFirstItem()->getId();
			$rel = $collection->getFirstItem()->getRel();
			$url = $collection->getFirstItem()->getUrl();
			$netsuiteRecord = $this->_netsuiteFactory->create();
			$netsuiteCron = $netsuiteRecord->load($id);
			if($netsuiteCron->getCount() == $netsuiteCron->getFinished()){
				$netsuiteCron->setStatus('completed');
				$netsuiteCron->save();
				if($rel == "next"){
					 $url = $this->callNewUrls($url);
					
				}else if($rel == "self"){
					
					 $url = $this->callNewUrls($url);
				}
		
			unset($collection);
			return $url;
			}else{
				return $collection->getFirstItem()->getUrl();
			}
	}
	protected function callNewUrls($url){
		
		$response = $this->apiHelper->_makeRestApiCall($url);
			return $url = $this->insertNewUrls($response);
		
	}
	protected function insertNewUrls($response){
		if(isset($response['links'])){
			foreach($response['links'] as $links){
				$rel = $links['rel']; 
				
				$pagePagination = array('last','previous','first','self');
				if(in_array($rel,$pagePagination)) continue;
				$url = $links['href']; 
				
				$splitString = explode("?",$url);

				$splitString = explode('&',$splitString[1]);
				$newData = explode('=',$splitString[0]);
				$limitTotal = $newData[1];
				$offsetTotal =explode('=',$splitString[1]);
				
				$netsuiteRecord = $this->_netsuiteFactory->create();
				$netsuiteRecord->setUrl($url);
				$netsuiteRecord->setStatus("pending");
				if($rel == "next"){
				$netsuiteRecord->setStatus("processing");
				$netsuiteRecord->setCount($response['count']);
				}
				$netsuiteRecord->setOffset($offsetTotal[1]);
				$netsuiteRecord->setLimit($limitTotal);
				$netsuiteRecord->setRel($rel);
				$netsuiteRecord->save();
				unset($netsuiteRecord);
			}
			$netsuiteRecord = $this->_netsuiteFactory->create();
			$collection = $netsuiteRecord->getCollection();
			$collection->addFieldToFilter('status','processing');
			return $collection->getFirstItem()->getUrl();
		}
	}
	 /**
     * Get Table name using direct query
     */
    public function getTablename($tableName)
    {
        /* Create Connection */
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
    }
}