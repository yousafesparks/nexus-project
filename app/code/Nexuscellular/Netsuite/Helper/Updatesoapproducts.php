<?php

namespace Nexuscellular\Netsuite\Helper;


use Nexuscellular\Netsuite\Helper\Attribute as attributeCellularHelper;
use Nexuscellular\Netsuite\Helper\Facets as FacetsCellularHelper;
use Nexuscellular\Netsuite\Helper\Apicall as apiCAllCellularHelper;
use Nexuscellular\Netsuite\Helper\Data as DataCellularHelper;
use Nexuscellular\Netsuite\Helper\Netsuiteproductstatus as netsuiteproductstatusHelper;
       
     
class Updatesoapproducts extends \Magento\Framework\App\Helper\AbstractHelper
{
	
	protected $ProductFactory;
	protected $productResourceModel;
	protected $resourceConnection;
	protected $updateSoapProductHelper;
	protected $soapApi;
	protected $_logger;
	protected $_productCollectionFactory;
	protected $apiHelper;
	protected $_updateAttrHelper;
	public function __construct(
		\Magento\Catalog\Model\ProductFactory $productFactory,
		\Magento\Catalog\Model\ResourceModel\Product $productResourceModel, 
		\Nexuscellular\Netsuite\Helper\Soapcall $soapApi,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Nexuscellular\Netsuite\Helper\Apicall $apiHelper,
		\Nexuscellular\Netsuite\Helper\Updateattributes $updateAttributesHelper
		//\Nexuscellular\Netsuite\Logger\Logger $logger
    )
    {
		$this->ProductFactory = $productFactory;
		$this->productResourceModel = $productResourceModel;
		$this->resourceConnection = $resourceConnection;
		$this->soapApi = $soapApi;
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->apiHelper = $apiHelper;
		$this->_updateAttrHelper = $updateAttributesHelper;
		//$this->_logger = $logger;
    }
	 public function pushUpdatedProductsToMagento(){
		
		$data = $this->soapApi->makeSoapApiCall();
		
		if($data){
			echo "Total Records found ========".$data["totalRecords"];
			$i = 1;
			foreach($data as $key => $recordsArray){
				if($key == "totalRecords"){
				$totalRecords = $recordsArray;
				}else{
					$i = $this->iterateInternalIds($recordsArray,$i);
				}
			}
			$this->updateStatus();
		}
	}
	public function updateStatus(){
			$tableName = $this->getTableName('netsuite_update_products_soap_api');
				$sql = "UPDATE $tableName SET `status` = 'completed'  WHERE $tableName.`entity` = 'updateattributes' ";
				$this->resourceConnection->getConnection()->query($sql);
	}
	protected function iterateInternalIds($internal_ids,$i){
		foreach($internal_ids as $internalId){
				echo $internalId."=============================<br/>";
			$productId = $this->isExistProduct($internalId);
			if($productId){
				$this->_callRESTApiCall($internalId,$productId);
			}
			$this->countEachProduct($i);
			$i++;
		}
		return $i;
	}
	protected function _callRESTApiCall($internalId,$productId){
		$url = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryItem/'.$internalId.'?expandSubResources=true';
		$response = $this->apiHelper->_makeRestApiCall($url);
		if(isset($response['o:errorCode']) && $response['o:errorCode'] == "UNEXPECTED_ERROR" ){	
		}else{
			$response["magento_product_id"] = $productId;
			$this->updateProducts($response);
		}
			
	}
	public function countEachProduct($key){
			$tableName = $this->getTableName('netsuite_update_products_soap_api');
				$sql = "UPDATE $tableName SET `finishedproducts` = {$key}  WHERE $tableName.`status` = 'processing' AND entity = 'updateattributes' ";
				$this->resourceConnection->getConnection()->query($sql);
	}
	protected function updateProducts($response){
		if(!empty($response)&& count($response) > 0){
				if(isset($response['matrixType']) && $response['matrixType'] == "CHILD"){
					$this->UpdateSimpleProduct($response);
				}else if(!isset($response['matrixType'])){
					$this->UpdateSimpleProduct($response);	
				}else if(isset($response['matrixType']) && $response['matrixType'] == "PARENT"){
					$this->UpdateConfigurableProduct($response);
				}
			}
	}
	public function isExistProduct($internalId){
		$collection = $this->_productCollectionFactory->create();
		$data = $collection->addAttributeToSelect(array('internal_id','entity_id'))->addAttributeToFilter('internal_id',$internalId);
		$productId = null;
		if($collection->getSize()){
			$productId = $data->getFirstItem()->getId();
		}
		return $productId;
	}
	public function UpdateSimpleProduct($response){
			$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/netsuite_update_product_attributes.log');
			$logger = new \Zend\Log\Logger();
			$logger->addWriter($writer);
			try{
				$starttime = microtime(true);
			
					$logger->info("updating simple Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']);
				echo "updating simple Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']. "<br/>";
				$productId = $response["magento_product_id"];
				$_product = $this->ProductFactory->create()->reset();
				$this->productResourceModel->load($_product, $productId);
				$_product->setStoreId(0);
				$_product = $this->_updateAttrHelper->saveAttribute($this->productResourceModel,$_product,$response);
				$this->_updateAttrHelper->updateSku($productId,$response['itemId']);
			
		 }catch(exception $e){
			 echo $message = $e->getMessage();
		 }
		 $finishTime = (microtime(true) - $starttime);
		 $logger->info("Finshed Simple Product =====".$response['id']." duration: =========".$finishTime);
			echo "Save product duration: ".$finishTime." seconds\n";
		 //return true;
	}
	public function UpdateConfigurableProduct($response,$configAttributeProdObj = array()){
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/netsuite_update_product_attributes.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
			try {
				$starttime = microtime(true);
				$logger->info("updating configurable Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']);
				echo "updating configurable Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']. "<br/>";
				$productId = $response["magento_product_id"];
				$_product = $this->ProductFactory->create()->reset();
				$this->productResourceModel->load($_product, $productId);
				$_product->setStoreId(0);
				$_product = $this->_updateAttrHelper->saveAttribute($this->productResourceModel,$_product,$response);
				$this->_updateAttrHelper->updateSku($productId,$response['itemId']);
				$finishTime = (microtime(true) - $starttime);
				$logger->info("Finshed Configurable Product =====".$response['id']." duration: =========".$finishTime);
				   echo "Save product duration: ".$finishTime." seconds\n";
			} catch (Exception $ex) {
				print_r($ex->getMessage());
				
			}	
	}
	 /**
     * Get Table name using direct query
     */
    public function getTablename($tableName)
    {
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
    }
	
}
