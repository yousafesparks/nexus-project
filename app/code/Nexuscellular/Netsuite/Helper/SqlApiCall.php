<?php

namespace Nexuscellular\Netsuite\Helper;
class SqlApiCall extends \Magento\Framework\App\Helper\AbstractHelper
{
	const NETSUITE_SANDBOX_URL 		= 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/query/v1/suiteql?limit=1000';
	const NETSUITE_PRODUCTION_URL 	= 'https://4000493.suitetalk.api.netsuite.com/services/rest/query/v1/suiteql?limit=1000';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }
	protected function _getCredentials($sandbox){
		$data = [];
		if($sandbox){
		$data['NETSUITE_CONSUMER_KEY'] = '';
		$data['NETSUITE_ACCOUNT'] = '';
		$data['NETSUITE_CONSUMER_SECRET'] = '';
		$data['NETSUITE_TOKEN_ID'] = '';
		$data['NETSUITE_TOKEN_SECRET'] = '';
		}
		return $data;
	}
	public function callRestApi($query){
	
		//echo $query;die();
	$url = self::NETSUITE_PRODUCTION_URL;
	$sandbox = 1;
	if($sandbox){	
		$url = self::NETSUITE_SANDBOX_URL;
	}
	$credentials = $this->_getCredentials($sandbox);
	
	$oauth_nonce = md5(mt_rand());
    $oauth_timestamp = time();
    $oauth_signature_method = 'HMAC-SHA1';
    $oauth_version = "1.0";
	
	// generate Signature 
	$baseString = $this->_restletBaseString("POST",
	$url,
	$credentials['NETSUITE_CONSUMER_KEY'],
	$credentials['NETSUITE_TOKEN_ID'],
	$oauth_nonce,
	$oauth_timestamp,
	$oauth_version,
	$oauth_signature_method,null);
	
	
	$key = rawurlencode($credentials['NETSUITE_CONSUMER_SECRET']) .'&'. rawurlencode($credentials['NETSUITE_TOKEN_SECRET']);


	 $signature = base64_encode(hash_hmac('sha1', $baseString, $key, true)); 
	 
	 // GENERATE HEADER TO PASS IN CURL
	 $header = 'Authorization: OAuth '
			 .'realm="' .rawurlencode($credentials['NETSUITE_ACCOUNT']) .'", '
			 .'oauth_consumer_key="' .rawurlencode($credentials['NETSUITE_CONSUMER_KEY']) .'", '
			 .'oauth_token="' .rawurlencode($credentials['NETSUITE_TOKEN_ID']) .'", '
			 .'oauth_nonce="' .rawurlencode($oauth_nonce) .'", '
			 .'oauth_timestamp="' .rawurlencode($oauth_timestamp) .'", '
			 .'oauth_signature_method="' .rawurlencode($oauth_signature_method) .'", '
			 .'oauth_version="' .rawurlencode($oauth_version) .'", '
			 .'oauth_signature="' .rawurlencode($signature) .'"';
	return  $this->_callCurl($header,$url,$query);

	}
	protected function _callCurl($header,$url,$query){
			$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_SSL_VERIFYHOST=>false,
			CURLOPT_SSL_VERIFYPEER=>false,
			CURLOPT_POSTFIELDS =>$query,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_HTTPHEADER => array(
				$header,
				 "prefer: transient",
				"content-type: application/json"
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$product = json_decode($response, true);
		return $product;		
	}
	protected function _restletBaseString($httpMethod, $url, $consumerKey, $tokenKey, $nonce, $timestamp, $version, $signatureMethod, $postParams){
		  //http method must be upper case
		  $baseString = strtoupper($httpMethod) .'&';
		  
		  //include url without parameters, schema and hostname must be lower case
		  if (strpos($url, '?')){
			$baseUrl = substr($url, 0, strpos($url, '?'));
			$getParams = substr($url, strpos($url, '?') + 1);
		  } else {
		   $baseUrl = $url;
		   $getParams = "";
		  }
		  $hostname = strtolower(substr($baseUrl, 0,  strpos($baseUrl, '/', 10)));
		  $path = substr($baseUrl, strpos($baseUrl, '/', 10));
		  $baseUrl = $hostname . $path;
		  $baseString .= rawurlencode($baseUrl) .'&';
		  
		  //all oauth and get params. First they are decoded, next alphabetically sorted, next each key and values is encoded and finally whole parameters are encoded
		  $params = array();
		  $params['oauth_consumer_key'] = array($consumerKey);
		  $params['oauth_token'] = array($tokenKey);
		  $params['oauth_nonce'] = array($nonce);
		  $params['oauth_timestamp'] = array($timestamp);
		  $params['oauth_signature_method'] = array($signatureMethod);
		  $params['oauth_version'] = array($version);
		   
		  foreach (explode('&', $getParams ."&". $postParams) as $param) {
			$parsed = explode('=', $param);
			if ($parsed[0] != "") {
			  $value = isset($parsed[1]) ? urldecode($parsed[1]): "";
			  if (isset($params[urldecode($parsed[0])])) {
				array_push($params[urldecode($parsed[0])], $value);
			  } else {
				$params[urldecode($parsed[0])] = array($value);
			  }
			}
		  }
		   
		  //all parameters must be alphabetically sorted
		  ksort($params);
		   
		  $paramString = "";
		  foreach ($params as $key => $valueArray){
			//all values must be alphabetically sorted
			sort($valueArray);
			foreach ($valueArray as $value){
			  $paramString .= rawurlencode($key) . '='. rawurlencode($value) .'&';
			}
		  }
		  $paramString = substr($paramString, 0, -1);
		   $baseString .= rawurlencode($paramString);
		  
		  return $baseString;
		}
	
}