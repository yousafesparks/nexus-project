<?php

namespace Nexuscellular\Netsuite\Helper;
class Attribute extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $tableFactory;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    protected $optionLabelFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    protected $optionFactory;
	protected $attributeFactory;
	protected $attributeRepository1;
    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository
     * @param \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory
     * @param \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement
     * @param \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory
     * @param \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
		\Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory,
		 \Magento\Catalog\Model\Product\Attribute\Repository $attributeRepository1,
		 \Magento\Framework\Filesystem\DirectoryList $dir,
		  \Magento\Framework\Filesystem\Driver\File $fileDriver
    ) {
        parent::__construct($context);

        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
		$this->attributeFactory = $attributeFactory;
		$this->attributeRepository1 = $attributeRepository1;
		$this->_dir = $dir;
		 $this->fileDriver = $fileDriver;
    }
	public function setMatrixAttributesToSimpleProduct($response,$_product){
		foreach(['custitem_matrix_capacity'=>'capacity','custitem_es_item_color'=>'color','custitem_matrix_quality'=>'custitem_matrix_quality'] as $key => $value){
			if(isset($response[$key]) && isset($response[$key]['items']) ){ // capacity
				$attributeId = $_product->getResource()->getAttribute($value)->getId();
				$attrValue = $this->_colorOperationAttribute($response[$key],$value,$attributeId);
				if($attrValue){
				$_product->setData($value, $attrValue);	
				}
			}
		}
			return $_product;
	}
	 protected function _getPrice($priceArray){
		$calculatedPrice=[];
		$items = $priceArray['items'];
		$basePrice = 0;
		$calculatedPrice['actual_price'] = null;
		$calculatedPrice['base_price'] = 0;
		foreach($items as $price){
			if($price['priceLevel']['id'] == 5) // actual price
				$calculatedPrice['actual_price'] = $price['price'];
			 if($price['priceLevel']['id'] == 1) // base price
				$calculatedPrice['base_price'] = $price['price'];
		}
		return $calculatedPrice; 
	}
	protected function _convertWeightToLbs($response){
		$weight = $response["weight"];
		if($response["weightUnit"] == 2){ // oz
			$weight = $response["weight"] / 16;
		}else if($response["weightUnit"] == 3){ // kg
			$weight = $response["weight"] * 2.205;
		}else if($response["weightUnit"] == 4){ // gm
			$weight = $response["weight"] /454;
		}
		return round($weight,2);
	}
   public function writeAttributeOptionValues($response,$_product){
			
			if(isset($response['custitem_f3_product_warranty']))
				$_product->setDescription($response['custitem_f3_product_warranty']);
			
			 $_product->setData('internal_id',$response['id']);
			
			$price = $this->_getPrice($response['price']);
			$_product->setData('price',$price['base_price']);
			$_product->setData('special_price',$price['actual_price']);
			
			if(isset($response['weight'])){
			 $weight = $this->_convertWeightToLbs($response);
			$_product->setData('weight',$weight);
			}
				 
			$attributes = ['custitem_grade','custitem_es_quality_grade','custitem_es_regular_seasonal','preferencecriterion'];
				foreach($attributes as $attribute){
					if(isset($response[$attribute]) && isset($response[$attribute]['id'])   ){ // item grade
						$attributeId = $_product->getResource()->getAttribute($attribute)->getId();
						$attrValue = $this->_operationAttribute($response[$attribute],$attribute,$attributeId);
						if($attrValue){
						$_product->setData($attribute, $attrValue);	
						}
					} 
				}
		
			 $_product->setNewsFromDate("");
			 $_product->setNewsToDate("");
			 if(isset($response['custitem_f3_new_arrival'])){
				 if($response['custitem_f3_new_arrival']) {
					  $new_from_date = date("m/d/Y"); 
					  $new_to_date = date('m/d/Y',strtotime("+ 5 year"));
					  $_product->setNewsFromDate($new_from_date);
					  $_product->setNewsToDate($new_to_date);
				 }
			 }
			  $_product->setData('custitem_f3_hottest_selling_item',0);
			 if(isset($response['custitem_f3_hottest_selling_item']) && $response['custitem_f3_hottest_selling_item']){
				 $_product->setData('custitem_f3_hottest_selling_item',$response['custitem_f3_hottest_selling_item']);
			 }
		 
			  
			 if(isset($response['salesDescription']) && $response['salesDescription']){
				 $_product->setData('salesdescription',$response['salesDescription']);
			 }
			  $_product->setData('custitem_f3_on_sale',0);
			 if(isset($response['custitem_f3_on_sale']) && $response['custitem_f3_on_sale']){
				 $_product->setData('custitem_f3_on_sale',$response['custitem_f3_on_sale']);
			 }
					 
			return $_product;
   }
 
   protected function _colorOperationAttribute($attribute,$mageattrName,$attributeId){
			$value = '';
			 if(isset($attribute['count']) && $attribute['count'] > 0){
				 $refName = $attribute['items'][0]['refName']; // attribute name
			
				  $value = $this->isValueExist($refName,$mageattrName,$attributeId);
				
			 }
			 return $value;
	}
	protected function isValueExist($value,$mageattrName,$attributeId){
		 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$attributeOptionAll = $objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
									->setPositionOrder('asc')
									->setAttributeFilter($attributeId)                                               
									->setStoreFilter()
									->load();  
		
		$optionId = '';
		$colorVal = $attributeOptionAll->getData();
			if(count($colorVal) > 0){
					foreach($colorVal as $color){
						if(in_array($value,$color)){
							$optionId = $color['option_id'];
							break;
						}
					}
				}
		return $optionId;
	 }
	protected function _operationAttribute($attribute,$mageattrName,$attributeId){
			$value = '';
			
			 if(isset($attribute['refName']) && $attribute['id'] > 0){
				 $refName = $attribute['refName']; // attribute name
				$createNew = false;
				if(!$value = $this->isValueExist($refName,$mageattrName,$attributeId)){
					 $createNew = true;
					 $this->createAttributeOptionValue($refName,$mageattrName);
				 }if($createNew)
				  $value = $this->isValueExist($refName,$mageattrName,$attributeId);
			 }
			 return $value;
	}
	  public function _operationNormalAttribute($attribute,$mageattrName,$attributeId){
			$value = '';
				 $refName = $attribute; // attribute value
				$createNew = false;
					 $this->createAttributeOptionValue1($refName,$mageattrName);
			
			 return $value;
	}
	protected function createAttributeOptionValue1($optionValue,$attributeCode){
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		 $entityType = 'catalog_product';
		$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$eavConfig = $objectManager->get('\Magento\Eav\Model\Config');
		$eavSetup = $objectManager->get('\Magento\Eav\Setup\EavSetup');
		$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
		$stores = $storeManager->getStores();
		$storeArray[0] = "All Store Views";
		foreach ($stores  as $store) 
		{
		   $storeArray[$store->getId()] = $store->getName();
		}
	  
		
		
			$option = array();
			$attribute = $eavConfig->getAttribute($entityType, $attributeCode);
			$option['attribute_id'] = $attribute->getAttributeId();
				$str = $optionValue;
				$option['value'][$str][0]=$str;
				foreach($storeArray as $storeKey => $store) 
				{
				  $option['value'][$str][$storeKey] = $str;
				}
			
			//custitem_f3_item_nested_brand
			$eavSetup->addAttributeOption($option);
	}
	protected function createAttributeOptionValue($optionValue,$attributeCode){
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		 $entityType = 'catalog_product';
		$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$eavConfig = $objectManager->get('\Magento\Eav\Model\Config');
		$eavSetup = $objectManager->get('\Magento\Eav\Setup\EavSetup');
		$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
		$stores = $storeManager->getStores();
		$storeArray[0] = "All Store Views";
		foreach ($stores  as $store) 
		{
		   $storeArray[$store->getId()] = $store->getName();
		}
	  
		
		
			$option = array();
			$attribute = $eavConfig->getAttribute($entityType, $attributeCode);
			$option['attribute_id'] = $attribute->getAttributeId();
		  //  $options = $attribute->getSource()->getAllOptions();
		   
		  
				$str = 'option_0';//'"'.$optionValue.'"';
				$option['value'][$str][0]=$str;
				foreach($storeArray as $storeKey => $store) 
				{
				  $option['value'][$str][$storeKey] = str_replace('"','', $str);
				}
				echo "<pre>";
		$file =  $this->_dir->getPath('var')."/import/color-pallet.csv"; 
          $data = [];
        if ($this->fileDriver->isExists($file)) {
			$handle = fopen($file, "r");
			$header = fgetcsv($handle);
			$i = 0;
			$options = [];
			$option = [];
			$swatchvisual = [];
			 while ( $row = fgetcsv($handle, 3000, ",") ) {
				  $data_count = count($row);
					$options['option_'.$i] = [0=>$row[1],1=>$row[1]];
					$swatchvisuals['option_'.$i] = $row[2];
					$i++;
					//if($i == 2) break;
			 }
			 $option['value'] = $options;
			$swatchvisual['value'] = $swatchvisuals;
			
		}
		
				$data = ['attribute_id'=>$attribute->getAttributeId(),'optionvisual'=>$option,'swatchvisual'=>$swatchvisual];
				//echo "<pre>";print_r($data);die();
				$model = $this->attributeFactory->create();
				 $model->addData($data);
				 $model->save();
				   $attribute = $this->attributeRepository1->get('color');
          $attribute->addData($data);
		  $attribute->save();
			//$eavSetup->addAttributeOption($data);
	}
}