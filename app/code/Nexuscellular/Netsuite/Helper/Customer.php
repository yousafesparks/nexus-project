<?php
namespace Nexuscellular\Netsuite\Helper;

//require_once('lib/internal/PHPToolkit_2020_1/samples/saved_search.php');
use Magento\Framework\App\Filesystem\DirectoryList;
use Nexuscellular\Netsuite\Helper\Apicall as apiCAllCellularHelper;
class Customer extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_logger;
	protected $apiHelper;
	protected $subscriberFactory;
    public function __construct(
	\Magento\Framework\Filesystem $filesystem,
	apiCAllCellularHelper $apiHelper,
	\Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
    ) {
		$this->mediaDirectoryPath = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
		$this->subscriberFactory= $subscriberFactory;
		$this->apiHelper = $apiHelper;
    }
	public function AddNewCustomer(){
		$count = 0;
		
		$url = "https://4000493.suitetalk.api.netsuite.com/services/rest/record/v1/customer";
		$response = $this->apiHelper->_makeRestApiCall($url);
		foreach($response["items"] as $item){
			$count++;
			
			
			$customer = array();
			$url = $item["links"][0]['href']."?expandSubResources=true";
			//$url = "https://4000493.suitetalk.api.netsuite.com/services/rest/record/v1/customer/15181?expandSubResources=true";
			$responseData = $this->apiHelper->_makeRestApiCall($url);
			$customer['id'] = $responseData['id'];
			echo "FETCHING CUSTOMER   Netsuite Internal Id==========================".$customer['id'];
			if(!isset($responseData['entityId'])){
				echo "<pre>";print_r($responseData);die();
			}
			/* echo "<pre>";
		print_r($responseData);die(); */
			echo "Customer count ==========================>".$count;
			echo "FETCHING CUSTOMER   Netsuite Internal Id==========================".$customer['id'];
			if(isset($responseData['stage']) && $responseData['stage'] == "CUSTOMER"){
				/* if(isset($responseData['itempricing'])){
				if($responseData['itempricing']['totalResults'] > 0){
					echo $responseData['id'];die();
				} */
				$customer['addressbook'] = $responseData['addressbook'];
				
				$customer['balance'] = isset($responseData['balance']) ? $responseData['balance'] : 0;
				$customer['companyName'] = isset($responseData['companyName']) ? $responseData['companyName'] : $responseData['entityId'];
				$customer['creditLimit'] = isset($responseData['creditLimit']) ? $responseData['creditLimit'] : 0;
				$customer['custentity_f3_subscribe_news_letter'] = isset($responseData['custentity_f3_subscribe_news_letter']) ? $responseData['custentity_f3_subscribe_news_letter'] : '';
				$customer['email'] = isset($responseData['email']) ? $responseData['email']: '';
				$customer['entityId'] = $responseData['entityId'];
				$customer['fax'] = isset($responseData['fax']) ? $responseData['fax'] : '';
				if(isset($responseData['contactRoles']['items'][0]['contact']['refName'])){
					$name = preg_replace('!\s+!', ' ', $responseData['contactRoles']['items'][0]['contact']['refName']);
					$fullname = explode(' ',$name);
					if(count($fullname) > 0){
						$customer['firstname'] = $fullname[0];
						$customer['lastname'] = $fullname[0];
						if(isset($fullname[1]))
						$customer['lastname'] = $fullname[1];
					}else{
						$customer['firstname'] = $customer['entityId'];
						$customer['lastname'] = $customer['entityId'];
					}
						
				}else{
					$customer['firstname'] = $customer['entityId'];
					$customer['lastname'] = $customer['entityId'];
				}
				if(empty($customer['email']))
					echo "EMAIL NOT EXIST of NETSUTE CUSTOMER ID==========================".$customer['id'];
				else 
					$this->_saveCustomer($customer);
				
				echo "imported customer Netsuite Internal Id==========================".$customer['id'];
				echo "<br/>";
			}
		
			}
		//echo "<pre>";
		print_r("Imported Customers");die();
		
		
		//$appState->setAreaCode('frontend'); // not needed if Area code is already set
		 
		
				
		
		
	}
	protected function _saveCustomer($netsuiteCustomer){
		/* echo "<pre>";
		print_r($netsuiteCustomer);die(); */
		$starttime = microtime(true);
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
		$appState = $objectManager->get('\Magento\Framework\App\State');
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$websiteId = $storeManager->getStore()->getWebsiteId();
		 
		$firstName = $netsuiteCustomer['firstname'];
		$lastName = $netsuiteCustomer['lastname'];
		$email = $netsuiteCustomer['email'];
		
		$customer = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
		$customer->setWebsiteId($websiteId);
	
		if ($customer->loadByEmail($email)->getId()) {
			echo 'Customer with email '.$email.' is already registered.';  
			} else {
				echo "IMPORTING CUSTOMER   Netsuite Internal Id==========================".$netsuiteCustomer['id'];
				try {				
					$customer->setEmail($email); 
					$customer->setFirstname($firstName);
					$customer->setLastname($lastName);
					$customer->setPassword('Test@1234'); 
					$customer->setForceConfirmed(true);
					$customer->setCompanyName($netsuiteCustomer['entityId']);
					$customer->setNetsuiteInternalId($netsuiteCustomer['id']);
					$customer->setCreditLimit($netsuiteCustomer['creditLimit']);
					$customer->setBalance($netsuiteCustomer['balance']);
					// save data
					$customer->save();
					
					if($netsuiteCustomer['addressbook']['totalResults'] > 0){
						$addressItems = $netsuiteCustomer['addressbook']['items'];
						foreach($addressItems as $item){	
						$addressbookaddress = $item['addressbookaddress'];
						$street=[];
						$street[] = $addressbookaddress['addr1'];
						$stateCode = $addressbookaddress['state'];
						$countryId = $addressbookaddress['country'];
						$postcode = $addressbookaddress['zip'];
						$customerAddress = $objectManager->get('\Magento\Customer\Model\AddressFactory')->create();
						$customerAddress->setCustomerId($customer->getId())
						->setFirstname($firstName)
						->setLastname($lastName)
						->setCountryId($countryId)
						->setRegionId($this->getRegionId($stateCode,$countryId)) // optional, depends upon Country, e.g. USA
						//->setRegion('California') // optional, depends upon Country, e.g. USA
						->setPostcode($postcode)
						->setCity($addressbookaddress['city'])
						->setTelephone(isset($addressbookaddress['addrPhone']) ? $addressbookaddress['addrPhone'] : '' )
						->setFax($netsuiteCustomer['fax'])
						->setCompany(isset($addressbookaddress['addressee']) ?$addressbookaddress['addressee'] :'')
						->setStreet($street) 
						->setIsDefaultBilling($item['defaultBilling'])
						->setIsDefaultShipping($item['defaultShipping'])
						->setSaveInAddressBook('1');
						try {
						$customerAddress->save();
							} catch (Exception $e) {
								echo $e->getMessage();die('customer address section error');
							}  
						
						unset($customerAddress);
						}
					}
				try {
					if($customer['custentity_f3_subscribe_news_letter'])
						$this->subscriberFactory->create()->subscribe($email);
						echo "Save product duration: ".(microtime(true) - $starttime)." seconds\n";
					} catch (Exception $e) {
						echo 'Cannot save customer address.';
					}                
			 
					// send welcome email to the customer
				 
			 
					echo 'Customer with the email ' . $email . ' is successfully created.';
			 
				} catch (Exception $e) {
					echo $e->getMessage();
				}
		}
	}
	public function getRegionId($stateCode, $countryId)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
 
		$region = $objectManager->create('Magento\Directory\Model\Region');
		 
		$regionId = $region->loadByCode($stateCode, $countryId)->getId();
		 
		return $regionId;
	}
}