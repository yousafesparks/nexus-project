<?php

namespace Nexuscellular\Netsuite\Helper;
class Facets extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $tableFactory;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    protected $optionLabelFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    protected $optionFactory;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository
     * @param \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory
     * @param \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement
     * @param \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory
     * @param \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory
    ) {
        parent::__construct($context);

        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
    }

	public function setFacetAtributes($response,$_product){
		$listAttributes = ['customlist_f3_category_type','custitem_arm_bands_size','custitem_audio_type','custitem_bluetooth','custitem_cable_length',
		'custitem_cable_type','custitem_camera','custitem_mounts_stands_type','custitem_phone_case_compatibility','custitem_phone_case_type',
		'custitemdevice_model','custitem5','custitem_storage_capacity','custitem_gadget_type','custitem_condition','custitem_screen_size','custitem_charger_amperage',
		'custitem_f3_category_type'];
			
				foreach($listAttributes as $value){
					if(isset($response[$value])){
						$attributeId = $_product->getResource()->getAttribute($value)->getId();
						$attrValue = $this->_saveDropdownValue($response[$value],$value,$attributeId);
						if($attrValue){
						$_product->setData($value, $attrValue);	
						}
					}
				}
			
			if(isset($response['class'])){
				$attrValue = $response['class']['refName'];
				if($attrValue){
				$_product->setData('class',$attrValue);	
				}
			}
			
			$multiSelectAttributes = ['custitem_f3_generic_color','custitem_part_type','custitem_f3_item_nested_brand','custitem_f3_price_range','custitem4','custitem_charger_type'];
			
			foreach($multiSelectAttributes as $value){
				if(isset($response[$value])){
					$attributeId = $_product->getResource()->getAttribute($value)->getId();
					$attrValue = $this->_saveMultiselectValue($response[$value],$value,$attributeId);
					if($attrValue){
					$_product->setData($value, $attrValue);	
					}
				}
			}
			return $_product;
	}
	
	protected function _saveMultiselectValue($responseArray,$attributeCode,$attributeId){
		$value = '';
			$multivalues = [];
			 if(isset($responseArray['items']) && $responseArray['totalResults'] > 0){
				 foreach($responseArray['items'] as $item){
					 if(isset($item['refName'])){
						 $multivalues[] = $item['refName'];
					 }
				 }
				 
				$createNew = false;
				$value = $this->isMultiValueExist($multivalues,$attributeCode,$attributeId);
			 return $value;
		}
	}
	protected function isMultiValueExist($multivalues,$mageattrName,$attributeId){
		 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$attributeOptionAll = $objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
									->setPositionOrder('asc')
									->setAttributeFilter($attributeId)                                               
									->setStoreFilter()
									->load();  
		
		$optionId = [];
		$optionValues = $attributeOptionAll->getData();
			if(count($optionValues) > 0){
				foreach($multivalues as $multivalue){
					foreach($optionValues as $opt){
						if(in_array($multivalue,$opt)){
							$optionId[] = $opt['option_id'];
							break;
						}
					}
				}
			}
			if(count($optionId)>0){
				$optionId = implode(",",$optionId);
			}
		return $optionId;
	 }
	protected function _saveDropdownValue($responseArray,$attributeCode,$attributeId){
		$value = '';
			
			 if(isset($responseArray['refName']) && $responseArray['id'] > 0){
				 $refName = $responseArray['refName']; // attribute name
				$createNew = false;
				$value = $this->isValueExist($refName,$attributeCode,$attributeId);
			 return $value;
		}
	}
	protected function isValueExist($value,$mageattrName,$attributeId){
		 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$attributeOptionAll = $objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
									->setPositionOrder('asc')
									->setAttributeFilter($attributeId)                                               
									->setStoreFilter()
									->load();  
		
		$optionId = '';
		$colorVal = $attributeOptionAll->getData();
			if(count($colorVal) > 0){
					foreach($colorVal as $color){
						if(in_array($value,$color)){
							$optionId = $color['option_id'];
							break;
						}
					}
				}
		return $optionId;
	 }
}