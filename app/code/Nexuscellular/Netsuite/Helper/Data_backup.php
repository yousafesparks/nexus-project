<?php

namespace Nexuscellular\Netsuite\Helper;

use Magento\Framework\App\PageCache\Version;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Nexuscellular\Netsuite\Helper\Attribute as attributeCellularHelper;
use Nexuscellular\Netsuite\Helper\Facets as FacetsCellularHelper;
use Nexuscellular\Netsuite\Helper\Apicall as apiCAllCellularHelper;
use Nexuscellular\Netsuite\Helper\Netsuiteproductstatus as netsuiteproductstatusHelper;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $optionFactory;
	protected $_attributeOptionCollection;
	 protected $urlRewriteCollection;
	protected $data;
	protected $_attributeFactory;
	protected $productFactory;
	protected $attrhelper;
	protected $apiHelper; 
	protected $productInterfaceFactory;
	protected $_storeManager;
	protected $_productCollectionFactory;
	protected $directoryList;
    protected $file;
	protected $_netsuiteFactory;
	protected $_productRepository;
	protected $_facetHelper;
	protected $_netsuiteproductstatusHelper;
	protected $_netsuiteProductFactory;
	protected $productRepositoryInterface;
	 public function __construct(
		\Magento\Catalog\Model\ProductFactory $productFactory,
		\Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager, 
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, 
		 \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteCollection,
		attributeCellularHelper $attrHelper,
		TypeListInterface $cacheTypeList, 
		\Nexuscellular\Netsuite\Model\NetsuiterecordFactory $netsuiteFactory,
		\Nexuscellular\Netsuite\Model\ProductinsertedFactory $netsuiteProductFactory,
		DirectoryList $directoryList,
		apiCAllCellularHelper $apiHelper,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		FacetsCellularHelper $facetHelper,
		netsuiteproductstatusHelper $netsuiteproductstatusHelper,
		\Magento\Catalog\Api\Data\ProductInterfaceFactory $productInterfaceFactory,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
		File $file
    )
    {
		$this->productFactory = $productFactory;
		$this->_attributeFactory = $attributeFactory;
		$this->_storeManager = $storeManager;
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->_productRepository = $productRepository;
		$this->attrhelper = $attrHelper;
		$this->_netsuiteFactory = $netsuiteFactory;
		$this->_netsuiteProductFactory = $netsuiteProductFactory;
		$this->productInterfaceFactory = $productInterfaceFactory;
		$this->directoryList = $directoryList;
		$this->apiHelper = $apiHelper;
        $this->file = $file;
		 $this->productRepositoryInterface = $productRepositoryInterface;
		$this->_facetHelper = $facetHelper;
		 $this->urlRewriteCollection 		= $urlRewriteCollection;
		$this->_netsuiteproductstatusHelper = $netsuiteproductstatusHelper;
    }
	/* public function pushNEWProductsToMagento(){
		$url = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryitem/62062?expandSubResources=true';
		//$url = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryitem/65455?expandSubResources=true';
		$response = $this->apiHelper->_makeRestApiCall($url);
		
		if(!empty($response))
		{ 
			if(isset($response['count']) && $response['count'] > 0){
				$items = $response['items'];
				$i = 0;
				foreach($items as $item){ 
					$UrlOfNetsuiteInventoryItem = $item['links'][0]['href'];
					//$UrlOfNetsuiteInventoryItem = "https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryitem/62062?expandSubResources=true";
					//$UrlOfNetsuiteInventoryItem = "https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/customlist_f3_category_type";
					$response = $this->apiHelper->_makeRestApiCall($UrlOfNetsuiteInventoryItem);
					$data = $this->attrhelper->_operationNormalAttribute($response['name'],'color',179);
					   
				}
			}
		}
		die("finish");
	} */
	 public function pushNEWProductsToMagento(){
		 /* 
	  $url = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryitem/58860';
			$response = $this->apiHelper->_makeRestApiCall($url);
			echo "<pre>";print_r($response);die(); */
		 
		/*   $netsuiteRecord = $this->_netsuiteFactory->create();
		$collection = $netsuiteRecord->getCollection();
		$size = $collection->getSize();
		
		if($size >= 1){
			$url = $this->_netsuiteproductstatusHelper->getNextProductUrl();
			$response = $this->apiHelper->_makeRestApiCall($url);
		}else if($size == 0){
			$url = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryitem';
			$response = $this->apiHelper->_makeRestApiCall($url);
			$url = $this->insertNetsuiteRecord($response);
		} */
		 $url = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryitem';
		$response = $this->apiHelper->_makeRestApiCall($url);
		//unset($collection);
		if(!empty($response))
		{ 
			if(isset($response['count']) && $response['count'] > 0){
				$items = $response['items'];
				$i = 0;
				foreach($items as $key => $item){ 
				//echo $key."=======================================".$item['id']."==============================";
					//$UrlOfNetsuiteInventoryItem = $item['links'][0]['href']."?expandSubResources=true";
					$UrlOfNetsuiteInventoryItem = 'https://4000493-sb1.suitetalk.api.netsuite.com/services/rest/record/v1/inventoryitem/70590?expandSubResources=true';
					$response = $this->apiHelper->_makeRestApiCall($UrlOfNetsuiteInventoryItem);
					
					$this->_createNewProducts($response);
					//$this->updateIterateProducts($key);
					die();
				}
			}
			
		}
		echo "<br/>";
		echo "==================================================================================== CALL NEXT URL =========================================";
		if(!empty($url))
			$this->pushNEWProductsToMagento();
		
		//$this->_createNewProducts($response);
	}
	public function updateIterateProducts($key){
			$netsuiteRecord = $this->_netsuiteFactory->create();
			$collection = $netsuiteRecord->getCollection();
			$collection->addFieldToFilter('status','processing');
			$id = $collection->getFirstItem()->getId();
			$netsuiteRecord = $this->_netsuiteFactory->create();
			$netsuiteRecord->load($id);
			$netsuiteRecord->setFinished($key+1);
			$netsuiteRecord->save();
			unset($netsuiteRecord);
	}
	public function _createNewProducts($response){
		
			if(!empty($response)&& count($response) > 0)
			{
				if(isset($response['matrixType']) && $response['matrixType'] == "CHILD"){
					$this->createSimpleProduct($response);
				} else if(isset($response['matrixType']) && $response['matrixType'] == "PARENT"){
					$this->createConfigurableProduct($response,$configAttributeProdObj = array());
					
				}else if(!isset($response['matrixType'])){
					$this->createSimpleProduct($response);
					
				}
			}
		// die('_createNewProducts');
	}
	
	protected function _convertWeightToLbs($response){
		$weight = $response["weight"];
		if($response["weightUnit"] == 2){ // oz
			$weight = $response["weight"] / 16;
		}else if($response["weightUnit"] == 3){ // kg
			$weight = $response["weight"] * 2.205;
		}else if($response["weightUnit"] == 4){ // gm
			$weight = $response["weight"] /454;
		}
		return round($weight,2);
	}
	public function createSimpleProduct($response){
			$check = $this->_isProductExist($response);
			 if($check)
				return true;
			
		try{
			echo "Creating simple Product========= INTERNAL ID = ".$response['id']."========= Display name =====".$response['displayName']. "<br/>";
			$starttime = microtime(true);
				 $price = $this->getPrice($response['price']);
				 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				 $collection = $this->_productCollectionFactory->create();
				$productName = $response["displayName"];
				$productName = str_replace("+", "", $productName);
				 $collection->addAttributeToSelect('name')->addAttributeToFilter('name',$productName);
				if($collection->getSize()){
					$productName = $productName."_".time();
				}
				$url = preg_replace('#[^0-9a-z]+#i', '-', $productName);
				$url = strtolower($url);
				$newUrlKey = $this->checkUrlKey($url);
				$sku = $response['itemId'];
				//$_product = $objectManager->create('Magento\Catalog\Model\Product');
				$_product = $this->productFactory->create();
				$_product->setStoreId(0);
				$_product->setUrlKey($newUrlKey);
				$_product->setName($productName);
				$_product->setTypeId('simple');
				$_product->setInternalId($response['id']); // netsuite Internal ID
				$_product->setAttributeSetId(4);
				$_product->setSku($sku);
				 $_product->setWebsiteIds(array(1));
				$_product->setStatus($response["isonline"]);
				$_product->setStockData(array(
						'use_config_manage_stock' => 1, //'Use config settings' checkbox
						'manage_stock' => 1, //manage stock
						'min_sale_qty' => isset($response['minimumquantity']) ? $response['minimumquantity'] : 1, //Minimum Qty Allowed in Shopping Cart
						'max_sale_qty' => isset($response['maximumquantity']) ? $response['maximumquantity'] : 10, //Maximum Qty Allowed in Shopping Cart
						'is_in_stock' => isset($response['custitem_f3_out_of_stock']) ? 0 : 1, //Stock Availability
						'qty' => $this->_getQtyFromNetsuite($response) //qty
						)
					);
				 $_product->setPrice($price['base_price']);
				  $_product->setSpecialPrice($price['actual_price']);
				 if(isset($response['weight'])){
					 $weight = $this->_convertWeightToLbs($response);
					$_product->setWeight($weight);
				 }
				$_product = $this->attrhelper->setMatrixAttributesToSimpleProduct($response, $_product);
				$_product = $this->_facetHelper->setFacetAtributes($response, $_product);
				$_product =  $this->attrhelper->writeAttributeOptionValues($response, $_product);
				
				 $_product->setVisibility(4);
				if(isset($response["parent"]) && isset($response["parent"]['id']) ){
					$parentUrl = $response["parent"]['links'][0]['href'];
					$_product->setVisibility(1);
				}
				//$_product = $this->_importImagesToProduct($response,$_product);
				$_product = $this->productRepositoryInterface->save($_product);
				
				//$_product->save();
				$configAttributeProdObj = [];
				$productId = $_product->getId();
				$internalId = $_product->getInternalId();
				$configAttributeProdObj['color'] = $_product->getColor();
				$configAttributeProdObj['capacity'] = $_product->getCapacity();
				$configAttributeProdObj['custitem_matrix_quality'] = $_product->getCustitemMatrixQuality();
				$configAttributeProdObj['product_id'] = $productId;
				unset($_product);
				$netsuiteProductFactory = $this->_netsuiteProductFactory->create();
				$netsuiteProductFactory->setInternalId($internalId);
				$netsuiteProductFactory->setStatus("success");
				
				if(isset($response['matrixType']) && isset($parentUrl) && $response['matrixType'] == "CHILD"){
					$response = $this->apiHelper->_makeRestApiCall($parentUrl."?expandSubResources=true");
					$this->createConfigurableProduct($response,$configAttributeProdObj);
				}
			
		 }catch(exception $e){
			 $netsuiteProductFactory->setStatus("failure");
			 $message = $e->getMessage();
			  $netsuiteProductFactory->setDetails($message);
			  $netsuiteProductFactory->save();
		 } 
		 $netsuiteProductFactory->save();
		 unset($netsuiteProductFactory);
		echo "Save product duration: ".(microtime(true) - $starttime)." seconds\n";
		 //return true;
	}
	public function checkUrlKey($url){
		 $newUrlKey = $url;
		$urlrewritesCollection = $this->urlRewriteCollection->create()->getCollection();
			$urlrewritesCollection->addFieldToFilter('entity_type', 'product')
								  ->addFieldToFilter('request_path', $url.".html")//.".html"
								  ->setPageSize(1);
			$urlrewrite = $urlrewritesCollection->getFirstItem();
			
			if ($urlrewrite->getId()) {
				for ($addNumberUrlKey = 0; $addNumberUrlKey <= 100; $addNumberUrlKey++) {
					$addToKey = $addNumberUrlKey + 1;
					$newUrlKey = $newUrlKey . '-' . $addToKey;
					$urlrewriteCheck = $this->checkUrlKey($newUrlKey);
					if (!$urlrewriteCheck->getId()) { break; }
				}
			}
			return $newUrlKey;
	}
	protected function _isProductExist($response){
		$bool = false;
		  $collection = $this->_productCollectionFactory->create();
		 $collection->addAttributeToSelect('internal_id')->addAttributeToFilter('internal_id',$response['id']);
		
		 if($collection->getSize() > 0)
			return true;
		
		$sku = $response['itemId']; // item number
		 
		try {
			$productSku = $this->_productRepository->get($sku);
		} catch (\Magento\Framework\Exception\NoSuchEntityException $e){
			$productSku = false;
		}
		if ($productSku) {
		   echo "PRODUCT SKU EXIST =============>".$sku;
		   return true;
		}
	}
	
	protected function _getQtyFromNetsuite($response){
		$availableQty = 0;
		$otherQty = 0;
		  if(isset($response['locations'])){
			 
				 foreach($response['locations']['items'] as $item){
					 if($item['locationId'] == 1){
						 $data = $this->apiHelper->_makeRestApiCall($item['links'][0]['href']."?fields=QUANTITYAVAILABLE");
						 if(isset($data['quantityAvailable']))
							 $availableQty = $data['quantityAvailable'];
							break;
					 }
				 }
		 }
		 if(!$availableQty)
			 $availableQty = $otherQty; 
		return $availableQty;				 
	}
	protected function _importImagesToProduct($response,$_product) {
		if(isset($response["custitemzg_image_filename"])){
				 $imageUrl = $response["custitemzg_image_filename"];
				if($response["custitemzg_num_images"] > 1){
					$imageUrl = explode(",",$response["custitemzg_image_filename"]);
				}
				$tmpDir = $this->getMediaDirTmpDir();
				$this->file->checkAndCreateFolder($tmpDir);
				$imageType = ['jpeg','jpg','png'];
				if(is_array($imageUrl)){
						foreach($imageUrl as $image){
							$newFileName = $tmpDir . baseName($image);
					  
						$result = $this->file->read($image, $newFileName);
							if ($result) {
								 $_product->addImageToMediaGallery($newFileName,array('small','thumbnail','base'), $imageType, true, false);
							}
						}
				}else{ // for a single file
					$newFileName = $tmpDir . baseName($imageUrl);
					$result = $this->file->read($imageUrl, $newFileName);
					if ($result) {
						 $_product->addImageToMediaGallery($newFileName,array('small','thumbnail','base'), $imageType, true, false);
					}
				}
		} 
		return $_product;
	}
	protected function getMediaDirTmpDir(){
        return $this->directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'tmp';
    }
	public function getPrice($priceArray){
		$calculatedPrice=[];
		$items = $priceArray['items'];
		$basePrice = 0;
		$calculatedPrice['actual_price'] = null;
		$calculatedPrice['base_price'] = 0;
		foreach($items as $price){
			if($price['priceLevel']['id'] == 5) // actual price
				$calculatedPrice['actual_price'] = $price['price'];
			 if($price['priceLevel']['id'] == 1) // base price
				$calculatedPrice['base_price'] = $price['price'];
		}
		return $calculatedPrice; 
	}
	protected function _isConfProductExist($response,$configAttributeProdObj){
		
		$bool = false;
		$childId = false;
		if(isset($configAttributeProdObj['product_id'])){
			$childId = $configAttributeProdObj['product_id'];
		}
		  $collection = $this->_productCollectionFactory->create();
		  $collection->addAttributeToSelect('internal_id')->addAttributeToFilter('internal_id',$response['id']);
		 $size = $collection->getSize();
		 if($size){
				if($childId){
				try {
					$id = $collection->getFirstItem()->getId();
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$configurableProduct = $objectManager->get('Magento\Catalog\Model\ProductRepository')->getById($id);
					$childIds=$configurableProduct->getExtensionAttributes()->getConfigurableProductLinks();
					if(!$childIds)
						$childIds = array();
					if(!in_array($childId,$childIds)){ 
						$childIds[] = $childId;
						$this->assignProductToParent($childId,$configurableProduct,$configAttributeProdObj,$childIds);
					}
					unset($configurableProduct);
				} catch (\Magento\Framework\Exception\NoSuchEntityException $e){
					$configurableProduct = false;
				}
				
			}
			return true;
		 }
		
		try {
			$sku = $response['itemId']; // item number
			$productSku = $this->_productRepository->get($sku);
		} catch (\Magento\Framework\Exception\NoSuchEntityException $e){
			$productSku = false;
		}
		if ($productSku) {
		   echo "PRODUCT SKU EXIST =============>".$sku;
		   return true;
		}
	}
	public function createConfigurableProduct($response,$configAttributeProdObj = null){
		
		  $sku = $response['itemId']; // item number
		 $internalId = $response['id']; // netsuite internal id
		$productId = $this->_isConfProductExist($response,$configAttributeProdObj);
		
		if($productId && count($configAttributeProdObj) > 0)
			return true;
		else if($productId && !count($configAttributeProdObj) > 0) 
			return true;
		else if((!$productId && !count($configAttributeProdObj) > 0) || (!$productId &&count($configAttributeProdObj) > 0)){
			// Check Product Name Duplicate
			
			$collection = $this->_productCollectionFactory->create();
			$collection->addAttributeToSelect('name')->addAttributeToFilter('name',$response['displayName']);
			echo $productName = $response["displayName"];
			
			if($collection->getSize()){
			$productName = $productName."_".time();
			}
			echo "Creating Configurable Product========= INTERNAL ID = ".$response['id']."=========== Display name =====".$response['displayName']. "<br/>";
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			//$product = $objectManager->create('Magento\Catalog\Model\Product');
			$url = preg_replace('#[^0-9a-z]+#i', '-', $productName);
			$url = strtolower($url);
			$newUrlKey = $this->checkUrlKey($url);
			$product = $this->productFactory->create();
			$product->setName($productName); // Set Product Name
			$product->setTypeId('configurable'); // Set Product Type Id
			$product->setInternalId($internalId);
			$product->setStoreId(0);
			$product->setUrlKey($newUrlKey);
			$product->setWebsiteIds(array(1));
			$product->setAttributeSetId(4); // Set Attribute Set ID
			$product->setSku($sku); // Set SKU
			$product->setStatus($response["isonline"]); // Set Status
			$product =  $this->attrhelper->writeAttributeOptionValues($response, $product);
			if(isset($response['weight'])){
				$product->setWeight($this->_convertWeightToLbs($response));
			}
			$product = $this->_importImagesToProduct($response,$product);
			$product = $this->_facetHelper->setFacetAtributes($response, $product);
			$product->setVisibility(4);
			$product->setPrice(0);
			$product->setTaxClassId(0);
			$product->setStockData(
				[
					'use_config_manage_stock' => 1, // Use Config Settings Checkbox
					'manage_stock' => 1, // Manage Stock
					'is_in_stock' => isset($response['custitem_f3_out_of_stock']) ? 0 : 1, // Stock Availability
				]
			);
			$setUsedProductAttributeIds = [];
			if(isset($configAttributeProdObj['color']))	
				$setUsedProductAttributeIds[] = $product->getResource()->getAttribute('color')->getId();
			if(isset($configAttributeProdObj['capacity']))	
				$setUsedProductAttributeIds[] = $product->getResource()->getAttribute('capacity')->getId();
			if(isset($configAttributeProdObj['custitem_matrix_quality']))
				$setUsedProductAttributeIds[] = $product->getResource()->getAttribute('custitem_matrix_quality')->getId();
			if(count($setUsedProductAttributeIds) > 0)
				$product->getTypeInstance()->setUsedProductAttributeIds($setUsedProductAttributeIds, $product);
			
			$configurableAttributesData = $product->getTypeInstance()->getConfigurableAttributesAsArray($product);
			$product->setCanSaveConfigurableAttributes(true);
			$product->setConfigurableAttributesData($configurableAttributesData);
			$configurableProductsData = [];
			$product->setConfigurableProductsData($configurableProductsData);
			$netsuiteProductFactory = $this->_netsuiteProductFactory->create();
			try {
				$product = $this->productRepositoryInterface->save($product);
				//$product->save();
				$netsuiteProductFactory->setInternalId($internalId);
				$netsuiteProductFactory->setStatus("success");
			} catch (Exception $ex) {
				$netsuiteProductFactory->setStatus("failure");
				$netsuiteProductFactory->setDetails($ex->getMessage());
				$netsuiteProductFactory->save();
				//print_r($ex->getMessage());
				//exit;
			}
			$netsuiteProductFactory->save();
			unset($netsuiteProductFactory);
			$productId = $product->getId();
			if(isset($configAttributeProdObj['product_id'])){
				$childProductId = $configAttributeProdObj['product_id'];
				
				$associatedProductIds = [$childProductId]; // Add Your Associated Product Ids.
				try {
					//$configurable_product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId); // Load Configurable Product
					$configurable_product =  $this->productFactory->create()->load($productId);
					$configurable_product->setAssociatedProductIds($associatedProductIds); // Setting Associated Products
					$configurable_product->setCanSaveConfigurableAttributes(true);
					$configurable_product->save();
				} catch (Exception $e) {
					echo "<pre>";
					print_r($e->getMessage());
					exit;
				}
				unset($configurable_product);
			}
			
			unset($product);
			unset($configAttributeProdObj);
			
				
			
		}
	}
	public function assignProductToParent($childProductId,$configurable_product,$configAttributeProdObj,$childIds){
			
			
		if($childProductId){
			$setUsedProductAttributeIds = [];
			if(isset($configAttributeProdObj['color']))	{
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$entityAttribute = $objectManager->get('Magento\Eav\Model\ResourceModel\Entity\Attribute');
				$setUsedProductAttributeIds[] = $entityAttribute->getIdByCode('catalog_product', 'color');
			}
			if(isset($configAttributeProdObj['capacity'])){
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$entityAttribute = $objectManager->get('Magento\Eav\Model\ResourceModel\Entity\Attribute');
				$setUsedProductAttributeIds[] = $entityAttribute->getIdByCode('catalog_product', 'capacity');
			}
			if(isset($configAttributeProdObj['custitem_matrix_quality'])){
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$entityAttribute = $objectManager->get('Magento\Eav\Model\ResourceModel\Entity\Attribute');
				$setUsedProductAttributeIds[] = $entityAttribute->getIdByCode('catalog_product', 'custitem_matrix_quality');
			}
			if(count($setUsedProductAttributeIds) > 0){
				$configurable_product->setTypeId('configurable'); 
				$configurable_product->getTypeInstance()->setUsedProductAttributeIds($setUsedProductAttributeIds, $configurable_product);
				$configurableAttributesData = $configurable_product->getTypeInstance()->getConfigurableAttributesAsArray($configurable_product);
				$configurable_product->setCanSaveConfigurableAttributes(true);
				$configurable_product->setConfigurableAttributesData($configurableAttributesData);
				$configurableProductsData = [];
				$configurable_product->setConfigurableProductsData($configurableProductsData);
				$associatedProductIds = $childIds; // Add Your Associated Product Ids.
				try {
					
					$configurable_product->setAssociatedProductIds($associatedProductIds); // Setting Associated Products
					$configurable_product->setCanSaveConfigurableAttributes(true);
					$configurable_product->save();
				} catch (Exception $e) {
					echo "<pre>";
					print_r($e->getMessage());
					exit;
				}
				unset($configurable_product);
			}
			}
	}
	public function insertNetsuiteRecord($response){
		if(isset($response['links'])){
			foreach($response['links'] as $links){
				$rel = $links['rel']; 
				if($rel == "last") continue;
				$url = $links['href']; 
				
				$splitString = explode("?",$url);
				
				$splitString = explode('&',$splitString[1]);
				$newData = explode('=',$splitString[0]);
				$limitLabel = $newData[0];
				$limitTotal = $newData[1];
				$offsetLabel = $splitString[0];
				$offsetTotal =explode('=',$splitString[1]);
				$netsuiteRecord = $this->_netsuiteFactory->create();
				$netsuiteRecord->setUrl($url);
				$netsuiteRecord->setStatus("pending");
				if($rel == "self"){
				$netsuiteRecord->setStatus("processing");
				$netsuiteRecord->setCount($response['count']);
				}				
				$netsuiteRecord->setOffset($offsetTotal[1]);
				$netsuiteRecord->setLimit($limitTotal);
				$netsuiteRecord->setRel($rel);
				$netsuiteRecord->save();
				unset($netsuiteRecord);
			}
			$netsuiteRecord = $this->_netsuiteFactory->create();
			$collection = $netsuiteRecord->getCollection();
			$collection->addFieldToFilter('status','processing');
			return $collection->getFirstItem()->getUrl();
		}
	}
	
}
