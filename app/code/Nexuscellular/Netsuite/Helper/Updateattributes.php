<?php

namespace Nexuscellular\Netsuite\Helper;
class Updateattributes extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $tableFactory;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    protected $optionLabelFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    protected $optionFactory;
	protected $_resource;
    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository
     * @param \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory
     * @param \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement
     * @param \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory
     * @param \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
		\Magento\Framework\App\ResourceConnection $resource
    ) {
        parent::__construct($context);

        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
		$this->_resource = $resource;
    }
	public function setMatrixAttributesToSimpleProduct($response,$_product){
		foreach(['custitem_matrix_capacity'=>'capacity','custitem_es_item_color'=>'color','custitem_matrix_quality'=>'custitem_matrix_quality'] as $key => $value){
			if(isset($response[$key]) && isset($response[$key]['items']) ){ // capacity
				$attributeId = $_product->getResource()->getAttribute($value)->getId();
				$attrValue = $this->_colorOperationAttribute($response[$key],$value,$attributeId);
				if($attrValue){
				$_product->setData($value, $attrValue);	
				}
			}
		}
		
			return $_product;
	}
	public function updateSku($id,$sku){
		 $connection  = $this->_resource->getConnection();
       $data = ["sku"=>$sku]; // Key_Value Pair
       $where = ['entity_id = ?' => (int)$id];
 
       $tableName = $connection->getTableName("catalog_product_entity");
       $connection->update($tableName, $data, $where);
	}
	public function saveAttribute($productResourceModel,$_product,$response){
		
		foreach(['custitem_matrix_capacity'=>'capacity','custitem_es_item_color'=>'color','custitem_matrix_quality'=>'custitem_matrix_quality'] as $key => $value){
			if(isset($response[$key]) && isset($response[$key]['items']) ){
				$optionText = '';
				$attribute = $response[$key];
				if(isset($attribute['count']) && $attribute['count'] > 0){
				 $optionText = $attribute['items'][0]['refName']; // attribute name
				}
				if($optionText){
					$attr = $_product->getResource()->getAttribute($value);
					 if ($attr->usesSource()) {
						   $option_id = $attr->getSource()->getOptionId($optionText);
					 }
					if($option_id){
					$_product->setData($value, $option_id);
					$productResourceModel->saveAttribute($_product, $value);
					}
				}
			}
		}
		$attributes = ['custitem_grade','custitem_es_quality_grade','custitem_es_regular_seasonal','preferencecriterion'];
		foreach($attributes as $attribute){
			if(isset($response[$attribute]) && isset($response[$attribute]['id'])   ){
				
				$optionText = $this->_operationAttribute($response[$attribute],$attribute);
				if($optionText){
				$attr = $_product->getResource()->getAttribute($attribute);
					 if ($attr->usesSource()) {
						   $option_id = $attr->getSource()->getOptionId($optionText);
					 }	
				$_product->setData($value, $option_id);
				$productResourceModel->saveAttribute($_product, $value);
				}
			}
		}
		$price = 0;
		$visibility = 4;
		 if((isset($response['matrixType']) && $response['matrixType'] == "CHILD") || (!isset($response['matrixType']))){
			 $price = $this->_getPrice($response['price']);
			 
		 }
		if((isset($response['matrixType']) && $response['matrixType'] == "CHILD")){
			 $visibility = 1;
		 }
		$attributes = [
		'name'=>$response["displayName"],
		'class'=>(isset($response['class'])) ? $response['class']['refName'] : null,
		'salesdescription'=>(isset($response['salesDescription']) && $response['salesDescription']) ? $response['salesDescription'] : null,
		'custitem_f3_on_sale' =>(isset($response['custitem_f3_on_sale']) && $response['custitem_f3_on_sale']) ? $response['custitem_f3_on_sale'] : 0,
		'price'	=> $price ? $price['base_price'] : 0,
		'special_price'=>$price ? $price['actual_price']: null,
		'weight'=>(isset($response['weight'])) ? $this->_convertWeightToLbs($response) : null,
		'description' => (isset($response['custitem_f3_product_warranty'])) ? $response['custitem_f3_product_warranty'] : null,
		'custitem_f3_hottest_selling_item' => (isset($response['custitem_f3_hottest_selling_item']) && $response['custitem_f3_hottest_selling_item']) ? $response['custitem_f3_hottest_selling_item'] : 0,
		'status' =>$response["isonline"],
		'visibility' => $visibility
		];
		foreach($attributes as $attrName => $attrValue){
			$_product->setData($attrName, $attrValue);
			$productResourceModel->saveAttribute($_product, $attrName);
		}
		
		$listAttributes = ['customlist_f3_category_type','custitem_arm_bands_size','custitem_audio_type','custitem_bluetooth','custitem_cable_length',
		'custitem_cable_type','custitem_camera','custitem_mounts_stands_type','custitem_phone_case_compatibility','custitem_phone_case_type',
		'custitemdevice_model','custitem5','custitem_storage_capacity','custitem_gadget_type','custitem_condition','custitem_screen_size','custitem_charger_amperage',
		'custitem_f3_category_type'];
			
				foreach($listAttributes as $value){
					if(isset($response[$value])){
						$optionText = $this->_saveDropdownValue($response[$value],$value);
						if($optionText){
							if($attrValue){
								$attr = $_product->getResource()->getAttribute($value);
								 if ($attr->usesSource()) {
									   $option_id = $attr->getSource()->getOptionId($optionText);
								 }	
							$_product->setData($value, $option_id);
							$productResourceModel->saveAttribute($_product, $value);
							}
						}
					}
				}
		$multiSelectAttributes = ['custitem_f3_generic_color','custitem_part_type','custitem_f3_item_nested_brand','custitem_f3_price_range','custitem4','custitem_charger_type'];
			
			foreach($multiSelectAttributes as $value){
				if(isset($response[$value])){
					$attrValue = $this->_saveMultiselectValue($response[$value],$value,$_product);
					
					if($attrValue){
						//echo $attrValue;die();
					$_product->setData($value, $attrValue);
					$productResourceModel->saveAttribute($_product, $value);
					}
				}
			}
			//return $_product;		
	}
	protected function _saveMultiselectValue($responseArray,$attributeCode,$_product){
		
		$optionIds = '';
			$multivalues = [];
			 if(isset($responseArray['items']) && $responseArray['totalResults'] > 0){
				 foreach($responseArray['items'] as $item){
					 if(isset($item['refName'])){
						 $multivalues[] = $item['refName'];
					 }
				 }
				 $optionId = [];
				 foreach($multivalues as $optionText){
					 $attr = $_product->getResource()->getAttribute($attributeCode);
								 if ($attr->usesSource()) {
									   $optionId[] = $attr->getSource()->getOptionId($optionText);
								 }	
				 }
				 if(count($optionId)>0){
				$optionIds = implode(",",$optionId);
			}
			
		}
		 return $optionIds;
	}
	protected function isMultiValueExist($multivalues,$mageattrName,$attributeId){
		 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$attributeOptionAll = $objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
									->setPositionOrder('asc')
									->setAttributeFilter($attributeId)                                               
									->setStoreFilter()
									->load();  
		
		$optionId = [];
		$optionValues = $attributeOptionAll->getData();
			if(count($optionValues) > 0){
				foreach($multivalues as $multivalue){
					foreach($optionValues as $opt){
						if(in_array($multivalue,$opt)){
							$optionId[] = $opt['option_id'];
							break;
						}
					}
				}
			}
			if(count($optionId)>0){
				$optionId = implode(",",$optionId);
			}
		return $optionId;
	 }
	 protected function _getPrice($priceArray){
		$calculatedPrice=[];
		$items = $priceArray['items'];
		$basePrice = 0;
		$calculatedPrice['actual_price'] = null;
		$calculatedPrice['base_price'] = 0;
		foreach($items as $price){
			if($price['priceLevel']['id'] == 5) // actual price
				$calculatedPrice['actual_price'] = $price['price'];
			 if($price['priceLevel']['id'] == 1) // base price
				$calculatedPrice['base_price'] = $price['price'];
		}
		return $calculatedPrice; 
	}
	protected function _convertWeightToLbs($response){
		$weight = $response["weight"];
		if($response["weightUnit"] == 2){ // oz
			$weight = $response["weight"] / 16;
		}else if($response["weightUnit"] == 3){ // kg
			$weight = $response["weight"] * 2.205;
		}else if($response["weightUnit"] == 4){ // gm
			$weight = $response["weight"] /454;
		}
		return round($weight,2);
	}
  
   public function _operationNormalAttribute($attribute,$mageattrName,$attributeId){
			$value = '';
				 $refName = $attribute; // attribute value
				$createNew = false;
					 $this->createAttributeOptionValue($refName,$mageattrName);
			
			 return $value;
	}
   protected function _colorOperationAttribute($attribute,$mageattrName,$attributeId){
			$value = '';
			 if(isset($attribute['count']) && $attribute['count'] > 0){
				 return $refName = $attribute['items'][0]['refName']; // attribute name
			
				  //$value = $this->isValueExist($refName,$mageattrName,$attributeId);
				
			 }
			 return $value;
	}
	protected function isValueExist($value,$mageattrName,$attributeId){
		 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$attributeOptionAll = $objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
									->setPositionOrder('asc')
									->setAttributeFilter($attributeId)                                               
									->setStoreFilter()
									->load();  
		
		$optionId = '';
		$colorVal = $attributeOptionAll->getData();
			if(count($colorVal) > 0){
					foreach($colorVal as $color){
						if(in_array($value,$color)){
							$optionId = $color['option_id'];
							break;
						}
					}
				}
		return $optionId;
	 }
	 protected function _saveDropdownValue($responseArray,$attributeCode){
		$refName = '';
			
			 if(isset($responseArray['refName']) && $responseArray['id'] > 0){
				 $refName = $responseArray['refName']; // attribute name
			}	
			 return $refName;
	}
	protected function _operationAttribute($attribute,$mageattrName){
			$refName = '';
			
			 if(isset($attribute['refName']) && $attribute['id'] > 0){
				 $refName = $attribute['refName']; // attribute name
				
			 }
			 return $refName;
	}
	protected function createAttributeOptionValue($optionValue,$attributeCode){
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		 $entityType = 'catalog_product';
		$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
		
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
		$eavConfig = $objectManager->get('\Magento\Eav\Model\Config');
		$eavSetup = $objectManager->get('\Magento\Eav\Setup\EavSetup');
		$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
		$stores = $storeManager->getStores();
		$storeArray[0] = "All Store Views";
		foreach ($stores  as $store) 
		{
		   $storeArray[$store->getId()] = $store->getName();
		}
	  
		
		
			$option = array();
			$attribute = $eavConfig->getAttribute($entityType, $attributeCode);
			$option['attribute_id'] = $attribute->getAttributeId();
		  //  $options = $attribute->getSource()->getAllOptions();
		   
		  
				$str = '"'.$optionValue.'"';
				$option['value'][$str][0]=str_replace('"','', $str);
				foreach($storeArray as $storeKey => $store) 
				{
				  $option['value'][$str][$storeKey] = str_replace('"','', $str);
				}
			$eavSetup->addAttributeOption($option);
	}
}