<?php


namespace Nexuscellular\Netsuite\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Model\Product;
use Nexuscellular\Netsuite\Helper\Data as nexusCellularHelper; 
use Nexuscellular\Netsuite\Helper\Updateproducts as updateProductsHelper; 
use Nexuscellular\Netsuite\Helper\Updatesoapproducts as Updatesoapproducts; 
class Importproduct extends Command
{
	
	 protected $updateHelper;
	 protected $helper;
	 protected $state;
	 protected $updateSoapProducts;
	 public function __construct(
		updateProductsHelper $NXupdateHelper,
		Updatesoapproducts $NXupdateSoapProductsHelper,
		nexusCellularHelper $NXHelper,
		\Magento\Framework\App\State $state
    ) {
		$this->helper = $NXHelper;
		$this->updateHelper = $NXupdateHelper;
		 $this->state = $state;
		 $this->updateSoapProducts = $NXupdateSoapProductsHelper;
        parent::__construct();
    }


   protected function configure()
   {
       $this->setName('netsuite:importproduct');
       $this->setDescription('netsuite commands');
       
       parent::configure();
   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {	
	    $this->state->setAreaCode("adminhtml"); // or \Magento\Framework\App\Area::AREA_ADMINHTML, depending on your needs
	   //echo "start22222";
	  //echo $this->updateSoapProducts->pushUpdatedProductsToMagento();
    /// $output->writeln("Hello World");
	   //echo $this->updateHelper->pushUpdatedProductsToMagento();
	   echo $this->helper->addColorOption();
   }
}