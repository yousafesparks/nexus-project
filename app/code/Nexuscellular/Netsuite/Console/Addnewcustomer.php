<?php


namespace Nexuscellular\Netsuite\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Model\Product;
use Nexuscellular\Netsuite\Helper\Customer as nexusCellularCustomerHelper; 
class Addnewcustomer extends Command
{
	 protected $helper;
	 protected $state;
	 protected $updateSoapProducts;
	 public function __construct(
		nexusCellularCustomerHelper $NXHelper,
		\Magento\Framework\App\State $state
    ) {
		$this->helper = $NXHelper;
		 $this->state = $state;
        parent::__construct();
    }


   protected function configure()
   {
       $this->setName('netsuite:addnewcustomer');
       $this->setDescription('netsuite import new customer');
       
       parent::configure();
   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {	
	    $this->state->setAreaCode("adminhtml"); // or \Magento\Framework\App\Area::AREA_ADMINHTML, depending on your needs
	 echo $this->helper->AddNewCustomer();
   }
}