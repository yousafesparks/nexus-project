<?php


namespace Nexuscellular\Netsuite\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Model\Product;
use Nexuscellular\Netsuite\Helper\Data as nexusCellularHelper; 
use Nexuscellular\Netsuite\Helper\Updateproducts as updateProductsHelper; 
class Updateproduct extends Command
{
	
	 protected $updateHelper;
	 protected $helper;
	 protected $state;
	 public function __construct(
		updateProductsHelper $NXupdateHelper,
		nexusCellularHelper $NXHelper,
		\Magento\Framework\App\State $state
    ) {
		$this->helper = $NXHelper;
		$this->updateHelper = $NXupdateHelper;
		 $this->state = $state;
        parent::__construct();
    }


   protected function configure()
   {
       $this->setName('netsuite:updateproduct');
       $this->setDescription('netsuite commands');
       
       parent::configure();
   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {	
	    $this->state->setAreaCode("adminhtml"); // or \Magento\Framework\App\Area::AREA_ADMINHTML, depending on your needs
		echo "start";
		$output->writeln("Hello World");
	   echo $this->updateHelper->pushUpdatedProductsToMagento();
   }
}