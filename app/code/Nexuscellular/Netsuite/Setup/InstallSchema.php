<?php

namespace Nexuscellular\Netsuite\Setup;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface{
    
    public function install(SchemaSetupInterface $setup,ModuleContextInterface $context){
      
        $setup->startSetup();
        $conn = $setup->getConnection();
        $tableName = $setup->getTable('netsuite_cron_record');
         
        if($conn->isTableExists($tableName) != true){
            $table = $conn->newTable($tableName)
                            ->addColumn(
                                'id',
                                Table::TYPE_INTEGER,
                                null,
                     ['identity'=>true,'unsigned'=>true,'nullable'=>false,'primary'=>true]
                                )
                            ->addColumn(
                                'url',
                                Table::TYPE_TEXT,
                                '2M',
                                ['nullable'=>false,'default'=>'']
                                )
                            ->addColumn(
                                'status',
                                Table::TYPE_TEXT,
                                '2M',
                                ['nullbale'=>false,'default'=>'']
                                )
								  ->addColumn(
                                'count',
                                Table::TYPE_INTEGER,
                                null,
                                ['unsigned'=>true]
                                )
								->addColumn(
                                'finished',
                                Table::TYPE_INTEGER,
                                null,
                                ['unsigned'=>true]
                                )
								->addColumn(
                                'message',
                                Table::TYPE_TEXT,
                                '2M',
                                ['nullbale'=>true,'default'=>'']
                                )
								->addColumn(
                                'offset',
                                Table::TYPE_TEXT,
                                '255',
                                ['nullbale'=>false,'default'=>'']
                                )
								->addColumn(
                                'limit',
                                Table::TYPE_TEXT,
                                '255',
                                ['nullbale'=>false,'default'=>'']
                                )
								->addColumn(
                                'rel',
                                Table::TYPE_TEXT,
                                '255',
                                ['nullbale'=>false,'default'=>'']
                                )
                            ->setOption('charset','utf8');
            $conn->createTable($table);
        }
		  $tableName = $setup->getTable('netsuite_product_inserted');
        if($conn->isTableExists($tableName) != true){
            $table = $conn->newTable($tableName)
                            ->addColumn(
                                'id',
                                Table::TYPE_INTEGER,
                                null,
                     ['identity'=>true,'unsigned'=>true,'nullable'=>false,'primary'=>true]
                                )
                            ->addColumn(
                                'internal_id',
                                Table::TYPE_TEXT,
                                '2M',
                                ['nullable'=>false,'default'=>'']
                                )
                            ->addColumn(
                                'status',
                                Table::TYPE_TEXT,
                                '255',
                                ['nullbale'=>false,'default'=>'']
                                )
								->addColumn(
                                'details',
                                Table::TYPE_TEXT,
                                '2M',
                                ['nullbale'=>false,'default'=>'']
                                )
                            ->setOption('charset','utf8');
            $conn->createTable($table);
        }
        $setup->endSetup();
    }
}
 ?>
