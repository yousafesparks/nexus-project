<?php

namespace Nexuscellular\Netsuite\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
class UpgradeData implements UpgradeDataInterface
{
	private $categorySetupFactory;

	 public function __construct(CategorySetupFactory $categorySetupFactory){
         $this->categorySetupFactory = $categorySetupFactory;
    }
	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context){
		//die('dddd');
		if (version_compare($context->getVersion(), '1.1.4', '<')) {
			
		 $installer = $setup;
         $installer->startSetup();
         $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
         $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
         $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

         $categorySetup->addAttribute(
         \Magento\Catalog\Model\Category::ENTITY, 'category_netsuite_internal_id', [
            'type' => 'varchar',
            'label' => 'Netsuite Internal ID',
            'input' => 'text',
            'required' => false,
            'sort_order' => 100,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group' => 'General Information',
            'is_used_in_grid' => true,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => true,
         ]
         );
         $installer->endSetup();
		}
	}
}