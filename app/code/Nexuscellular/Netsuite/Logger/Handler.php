<?php
namespace Nexuscellular\Netsuite\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = Logger::INFO;

    protected $fileName = '/var/log/netsuite_update_attributes_saved_search.log';
}
