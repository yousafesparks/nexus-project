<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
 namespace Nexuscellular\Netsuite\Controller\Index;
 //require_once('lib/internal/PHPToolkit_2020_1/samples/saved_search.php');
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;
use Nexuscellular\Netsuite\Helper\Updatesoapproducts as Updatesoapproducts; 
class Soap extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
	protected $urlRewriteCollection;
	protected $category;
	protected $resourceConnection;
	  protected $mediaDirectoryPath;
	protected $storeManager;
	protected $jsonHelper;
	 protected $updateSoapProducts;
	public $soapHeaders;
	public $applicationInfo;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\Framework\Filesystem $filesystem,
		\Magento\Framework\Json\Helper\Data $jsonHelper,
		Updatesoapproducts $NXupdateSoapProductsHelper,
		\Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        
		 $this->resourceConnection = $resourceConnection;
		 $this->mediaDirectoryPath 		= $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
		 	$this->jsonHelper 				= $jsonHelper;
			$this->storeManager 			= $storeManager;
			 $this->updateSoapProducts = $NXupdateSoapProductsHelper;
			 
         parent::__construct($context);
    }
	
	  public function execute()
	{
		
		 echo $this->updateSoapProducts->pushUpdatedProductsToMagento();
		
		
		
		
		
		
		
		die();
	
		$storeArray = [];
		 $service = new \NetSuiteService();
		/*$service->setSearchPreferences(false, 20); */

		// search date
		/* $SearchField = new \SearchDateField();
		$SearchField->searchValue = "2020-07-30T15:53:34";
		$SearchField->operator = "after";
		$ItemSearchBasic = new \ItemSearchBasic();
		$ItemSearchBasic->lastModifiedDate = $SearchField;

		$search = new \ItemSearch();
		$search->parentJoin = $ItemSearchBasic;



		$request = new \SearchRequest();
		$request->searchRecord = $search;
		$searchResponse = $service->search($request);
		echo "<pre>";print_r($searchResponse);die(); */
		
		
	 	$searchId = "WEBSERVICES_4000493_SB1_0731202015005470561372615339_5bf0d51";
		$request = new \SearchMoreWithIdRequest();
		$request->searchId = $searchId;
		$request->pageIndex = 2;
		$searchResponse = $service->searchMoreWithId($request);
		echo "<pre>";print_r( $searchResponse);die();
		$storeArray["totalRecords"] = $searchResponse->searchResult->totalRecords;
		$totalPages = $storeArray["totalRecords"];
		$pageIndex = 1;
		//$pageIndex++;
		//$storeArray = [];
		for($i=$pageIndex;$i<=$totalPages;$i++){
			$request = new \SearchMoreWithIdRequest();
			$request->searchId = $searchId;
			$request->pageIndex = $i;
			$searchResponse = $service->searchMoreWithId($request);
			$storeArray[$i] = $searchResponse->searchResult->recordList->record;
			//break;
		}
		 /* $json_soap=json_decode(json_encode($searchResponse));*/
     //   echo "<pre>";print_r( $searchResponse);die();
	
		$time = time(); 
		$filePath = 'netsuite/updaterecords/'.$time.'.js';
		$this->createProductJsFile($filePath,$storeArray);
		$file = $this->getBaseMediaUrl().$filePath;
		
		$data = json_decode(file_get_contents($file),true);

		/* foreach($data as $item){
			echo $item->searchResult->status->isSuccess;
		} */
		echo "<pre>";print_r($data);
		
		die('done');
		if (!$searchResponse->searchResult->status->isSuccess) {
			echo "SEARCH ERROR";
		} else {
			$records = $searchResponse->searchResult->searchRowList->searchRow;
			foreach($records as $record){
				echo $records->basic->class . "\n";
			}
			 echo "SEARCH SUCCESS, records found: " . $searchResponse->searchResult->totalRecords . "\n";
			$records = $searchResponse->searchResult->searchRowList->searchRow;
			
			foreach ($records as $record)  {
				echo "Name: " . $record->basic->name->searchValue . "\n";
			}

		}
	}
	  public function createProductJsFile($fileName, $productArray)
		{
        try {
			$productArray =  json_encode($productArray,true);
            $this->mediaDirectoryPath->writeFile($fileName,$productArray);
        } catch (\Exception $e) {
           echo $e->getMessage();
        }
    }
	 public function getBaseMediaUrl(){
        return $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }
	
}