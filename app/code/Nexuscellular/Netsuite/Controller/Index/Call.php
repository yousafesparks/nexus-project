<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Netsuite\Controller\Index;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Nexuscellular\Netsuite\Helper\Data as nexusCellularHelper; 
use Nexuscellular\Netsuite\Helper\Attribute as attributeCellularHelper; 
use Nexuscellular\Netsuite\Helper\Updateproducts as neetsuiteProductUpdateHelper; 
class Call extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
	
	 /**
     * Directory List
     *
     * @var DirectoryList
     */
	protected $helper;
	
protected $collectionFactory;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		nexusCellularHelper $NXHelper
    ) {
		$this->helper = $NXHelper;
        parent::__construct($context);
    }

  
    public function execute()
    {
		$this->helper->insertAttributes(); 
		
    }
}

