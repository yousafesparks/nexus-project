<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Netsuite\Controller\Index;

class Customer extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
	protected $urlRewriteCollection;
	protected $category;
	protected $resourceConnection;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        
		 $this->resourceConnection = $resourceConnection;
         parent::__construct($context);
    }
	
	  public function execute()
    {
        // instantiate customer object
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();      
$customer = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
//$customer->setWebsiteId($websiteId);
 $loadCusotmer = $customer->load(27);
 $loadCusotmer->setPassword('Test@123');
 $loadCusotmer->save();
		die('call customer');
		$starttime = microtime(true);
		$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
$appState = $objectManager->get('\Magento\Framework\App\State');
//$appState->setAreaCode('frontend'); // not needed if Area code is already set
 
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$websiteId = $storeManager->getStore()->getWebsiteId();
 
$firstName = 'John';
$lastName = 'Doe';
$email = 'johndoe10@example.com';
$password = 'Test1234';
 
// instantiate customer object
$customer = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
$customer->setWebsiteId($websiteId);

// if customer is already created, show message
// else create customer
if ($customer->loadByEmail($email)->getId()) {die("sssss");
    echo 'Customer with email '.$email.' is already registered.';  
} else {
	
    try {        
        // prepare customer data
        $customer->setEmail($email); 
        $customer->setFirstname($firstName);
        $customer->setLastname($lastName);
 
        // set null to auto-generate password
        $customer->setPassword($password); 
 
        // set the customer as confirmed
        // this is optional
        // comment out this line if you want to send confirmation email
        // to customer before finalizing his/her account creation
        $customer->setForceConfirmed(true);
        
        // save data
        $customer->save();
        
        // save customer address
        // this is optional
        // you can skip saving customer address while creating the customer
        $customerAddress = $objectManager->get('\Magento\Customer\Model\AddressFactory')->create();
        $customerAddress->setCustomerId($customer->getId())
                        ->setFirstname($firstName)
                        ->setLastname($lastName)
                        ->setCountryId('US')
                        ->setRegionId('12') // optional, depends upon Country, e.g. USA
                        ->setRegion('California') // optional, depends upon Country, e.g. USA
                        ->setPostcode('90232')
                        ->setCity('Culver City')
                        ->setTelephone('888-888-8888')
                        ->setFax('999')
                        ->setCompany('XYZ')
                        ->setStreet(array(
                            '0' => 'Your Customer Address 1', // compulsory
                            '1' => 'Your Customer Address 2' // optional
                        )) 
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');
        
        try {
            // save customer address
            $customerAddress->save();
        } catch (Exception $e) {
            echo 'Cannot save customer address.';
        }                
 
        // send welcome email to the customer
     
 
        echo 'Customer with the email ' . $email . ' is successfully created.';
 
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
				
		echo "Save product duration: ".(microtime(true) - $starttime)." seconds\n";
	
    }
	

	public function getTablename($tableName)
    {
        /* Create Connection */
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
    }
}

