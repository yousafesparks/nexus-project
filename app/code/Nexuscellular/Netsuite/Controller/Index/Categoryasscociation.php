<?php

namespace Nexuscellular\Netsuite\Controller\Index;
use Nexuscellular\Netsuite\Helper\SqlApiCall as restSuiteqlApi;
class Categoryasscociation extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
	protected $urlRewriteCollection;
	protected $category;
	protected $restSuiteqlApi; 
	 protected $categoryRepository;
	  /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollectionFactory;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $_repository;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		 \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\File\Csv $csvParser,
		\Magento\Framework\Filesystem\DirectoryList $dir,
		\Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteCollection,
		\Magento\Catalog\Model\CategoryFactory $categoryObj,
		 \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
            \Magento\Catalog\Api\CategoryRepositoryInterface $repository,
		restSuiteqlApi $apiHelper
    ) {
         $this->fileDriver = $fileDriver;
        $this->csvParser = $csvParser;
		$this->_dir = $dir;
		$this->category = $categoryObj;
		 $this->urlRewriteCollection = $urlRewriteCollection;
		 $this->restSuiteqlApi = $apiHelper;
		 $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_repository = $repository;
         parent::__construct($context);
    }

    
	
	 public function execute()
    {
		/*$categoryCollection = $this->_categoryCollectionFactory->create();
							$categoryCollection->addAttributeToSelect('*');
							//$categoryCollection->addAttributeToFilter('category_netsuite_internal_id',  1499);
							$currentCategory = $categoryCollection->getData();
							echo "<pre>";print_r(count($currentCategory));echo "<br/>";die();*/
		$query = "{\n\t\"q\": \"SELECT * from commercecategory order by id asc\"\n}";
		$responseData = $this->restSuiteqlApi->callRestApi($query);
		//echo "<pre>";print_r($responseData['items']);die();
			$category = $this->category->create();
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
				/// Get Root Category ID
				$store = $storeManager->getStore();
				$storeId = $store->getStoreId();
				$count = 0;
			 foreach($responseData['items'] as $row) {
				 /*
				$internalId = trim($row['id']);
					if(isset($row['displayinsiteoverride']) && $row['displayinsiteoverride'] == 'F'){
							$categoryCollection = $this->_categoryCollectionFactory->create();
							$categoryCollection->addAttributeToSelect('*');
							$categoryCollection->addAttributeToFilter('category_netsuite_internal_id',  $row['subcategory']);
							$currentCategory = $categoryCollection->getFirstItem();
							//echo "<pre>";print_r($currentCategory->getData());echo "<br/>";die();
							if($currentCategory->getId()){
								$currentCategory->setStoreId(1);
							$currentCategory->setIsActive(0);
							$this->_repository->save($currentCategory);
							
							}
					}*/
					$categoryCollection = $this->_categoryCollectionFactory->create();
							$categoryCollection->addAttributeToSelect('*');
							$categoryCollection->addAttributeToFilter('category_netsuite_internal_id', $row['id'])->setStore($storeId);
							//echo $categoryCollection->getFirstItem()->getId();die();
							if(!$categoryCollection->getFirstItem()->getId()){
								//echo $row['id'];
								echo "<pre>";print_r($row);
								$count++;
							}
							//die('ssssss');
				}
				echo $count;
	}
	public function createUrlKey($title)
	{
		$url = preg_replace('#[^0-9a-z]+#i', '-', $title);
		$lastCharTitle = substr($title, -1);
		$lastUrlChar = substr($url, -1);
		if ($lastUrlChar == "-" && $lastCharTitle != "-"){
			$url = substr($url, 0, strlen($url) - 1);
		}

		return $urlKey = strtolower($url);
		
	}
	public function checkUrlCollection(){
		echo "<pre>";$urlrewritesCollection = $this->urlRewriteCollection->create()->getCollection();
			//$urlrewritesCollection->addFieldToFilter('entity_type', 'category');
								  //->addFieldToFilter('request_path', $url.".html")//.".html"
								  //->setPageSize(1);
								  print_r($urlrewritesCollection->getData());
								  die();
			
	}	
	public function getCategoryCollection($name){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category_obj = $this->_objectManager->create('Magento\Catalog\Model\Category');
		$cate_data = $category_obj->getCollection()
			->addAttributeToSelect("name")
            ->addAttributeToFilter('name',$name);
		return $cate_data->getFirstItem();
	}
	public function checkUrlKey($url){
		$urlrewritesCollection = $this->urlRewriteCollection->create()->getCollection();
			$urlrewritesCollection->addFieldToFilter('entity_type', 'category')
								  ->addFieldToFilter('request_path', $url.".html")//.".html"
								  ->setPageSize(1);
			return $urlrewrite = $urlrewritesCollection->getFirstItem();
	}	
	public function getParentCategoryCollection($name){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category_obj = $this->_objectManager->create('Magento\Catalog\Model\Category');
		$cate_data = $category_obj->getCollection()
			->addAttributeToSelect("category_netsuite_internal_id")
            ->addAttributeToFilter('category_netsuite_internal_id',$name);
		return $cate_data->getFirstItem();
	}
}



