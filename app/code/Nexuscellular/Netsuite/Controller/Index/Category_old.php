<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Netsuite\Controller\Index;
use Nexuscellular\Netsuite\Helper\SqlApiCall as restSuiteqlApi;
use \Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Registry;
class Category extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
	protected $urlRewriteCollection;
	protected $category;
	protected $restSuiteqlApi; 
	   /** @var Registry $registry */
    private $registry;
	protected $_productCollection;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		 \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\File\Csv $csvParser,
		\Magento\Framework\Filesystem\DirectoryList $dir,
		\Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteCollection,
		\Magento\Catalog\Model\CategoryFactory $categoryObj,
		 Registry                    $registry,
		restSuiteqlApi $apiHelper,
		\Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
    ) {
         $this->fileDriver = $fileDriver;
        $this->csvParser = $csvParser;
		$this->_dir = $dir;
		$this->category = $categoryObj;
		 $this->urlRewriteCollection = $urlRewriteCollection;
		 $this->restSuiteqlApi = $apiHelper;
		  $this->registry             = $registry;
		   $registry->register("isSecureArea", true);
		    $this->_productCollection = $productCollection;
         parent::__construct($context);
    }

    
	
	 public function execute()
	 {echo "<pre>";
	 $collection = $this->_productCollection;
    $collection->addFilter('type_id', 'configurable');
	/*$config_items = $collection->getData();
 print_r($collection->getData());die();*/
 foreach($collection as $_product){
	 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$configProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($_product->getId()); 
	//echo $_product->getId()."<br/>";
	$_children = $configProduct->getTypeInstance()->getChildrenIds($configProduct->getId());
	foreach ($_children as $child){
		foreach($child as $c){
			$simpleProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($c);
			$simpleProduct->setVisibility(1);
			$simpleProduct->save();
			unset($simpleProduct);
		}
	}
 }die('finish data');
/*$_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);
foreach ($_children as $child){
    echo "Here are your child Product Ids ".$child->getID()."\n";
}
echo "count: ".count($_children);*/

	$file =  $this->_dir->getPath('var')."/import/model_new.xml"; 
	$xml = simplexml_load_file($file);
	$values = json_decode(json_encode($xml->customvalues),true);
	$c=0;
	foreach($values['customvalue'] as $item){
		$c++;
	}
	echo $c;
	die();
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$atrributesRepository  = $objectManager->create('\Magento\Catalog\Model\Product\Attribute\Repository');
$selectOptions = $atrributesRepository->get('custitemdevice_model')->getOptions();
$c=0;
foreach ($selectOptions as $selectOption) {
    //print_r($selectOption->getData());
	$data = $selectOption->getData();
if(!empty($data['label'])){
	$c++;
}
}
echo $c;
die();
		$file =  $this->_dir->getPath('var')."/import/CommerceCategories835.csv"; 
          $data = [];
       // $file = '/var/www/html/mage231/var/import/en_US.csv';
        if ($this->fileDriver->isExists($file)) {
			$handle = fopen($file, "r");
$category = $this->category->create();
			$header = fgetcsv($handle);
			 while ( $row = fgetcsv($handle, 3000, ",") ) {
				  $data_count = count($row);
					if ($data_count < 1) {
						continue;
					}
			 }
		}
    }
	 protected function prepareData($row, $id = null)
    {
          // in case you want to user the root category id
//        $rootCat = $this->_objectManager->get('Magento\Catalog\Model\Category');
//        $cat_info = $rootCat->load($rootrowId);
//        $categoryTmp->setPath($rootCat->getPath());

        $data = [
            'data' => [
                "parent_id" => 2,
                'name' => $row['category_name'],
                "is_active" => true,
                "position" => 10,
                "include_in_menu" => false,
            ],
            'custom_attributes' => [
                "display_mode"=> "PRODUCTS",
                "is_anchor"=> "1",
//                "custom_use_parent_settings"=> "0",
//                "custom_apply_to_products"=> "0",
//                "url_key"=> "", // if not set magento uses the name to generate it
//                "url_path"=> "cat2",
//                "automatic_sorting"=> "0",
//                'my_own_attribute' => 'value' // <-- your attribute
            ]
        ];

        if($id) {
            $data['data']['id'] = $id;
        }

        return $data;
    }
	protected function createCategory(array $data)
{
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $category = $objectManager
        ->create('Magento\Catalog\Model\Category', $data)
        ->setCustomAttributes($data['custom_attributes']);

    $repository = $objectManager->get(CategoryRepositoryInterface::class);
    $result = $repository->save($category);

    echo "Created Category " . $data['data']['name'] . "\n";

    return true;
}
	public function createUrlKey($title)
	{
		$url = preg_replace('#[^0-9a-z]+#i', '-', $title);
		$lastCharTitle = substr($title, -1);
		$lastUrlChar = substr($url, -1);
		if ($lastUrlChar == "-" && $lastCharTitle != "-"){
			$url = substr($url, 0, strlen($url) - 1);
		}

		return $urlKey = strtolower($url);
		
	}
	public function checkUrlCollection(){
		echo "<pre>";$urlrewritesCollection = $this->urlRewriteCollection->create()->getCollection();
			//$urlrewritesCollection->addFieldToFilter('entity_type', 'category');
								  //->addFieldToFilter('request_path', $url.".html")//.".html"
								  //->setPageSize(1);
								  return $urlrewritesCollection->getData();
								  //die();
			
	}	
	public function getcustomCategoryCollection(){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category_obj = $objectManager->create('Magento\Catalog\Model\Category');
		$cate_data = $category_obj->getCollection()
			->addAttributeToSelect("*");
		return $cate_data;
	}
	public function urlCollection(){
		$urlrewritesCollection = $this->urlRewriteCollection->create()->getCollection();
			$collection = $urlrewritesCollection->addFieldToSelect('*')->addFieldToFilter('entity_type', 'category');
			return $collection->getData();
	}
	public function checkUrlKey($url){
		$urlrewritesCollection = $this->urlRewriteCollection->create()->getCollection();
			$urlrewritesCollection->addFieldToFilter('entity_type', 'category')
								  ->addFieldToFilter('request_path', $url.".html")//.".html"
								  ->setPageSize(1);
			return $urlrewrite = $urlrewritesCollection->getFirstItem();
	}	
	public function getParentCategoryCollection($netsuiteInternalId){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category_obj = $objectManager->create('Magento\Catalog\Model\Category');
		$cate_data = $category_obj->getCollection()
			->addAttributeToSelect("category_netsuite_internal_id")
            ->addAttributeToFilter('category_netsuite_internal_id',$netsuiteInternalId);
		if(!$cate_data->getFirstItem()->getId()){
			/* foreach($responseData as $items){
			if($items['id'] == $netsuiteInternalId){

			}	
			}
			$name=ucfirst($row['name']);
            $url=strtolower($row['name']);
            $cleanurl = trim(preg_replace('/ +/', '', preg_replace('/[^A-Za-z0-9 ]/', '', urldecode(html_entity_decode(strip_tags($url))))));
           $enable = (trim($row['displayinsite'])) == "T" ? true : false ;
			if($enable){
				$enable = (trim($row['isinactive'])) == "T" ? false : true ;
			}
			
		
			$categoryTmp = $objectManager->create('\Magento\Catalog\Model\Category');
            $categoryTmp->setName($name);
            $categoryTmp->setIsActive($enable);
            $categoryTmp->setUrlKey($cleanurl);
            $categoryTmp->setParentId($parentId);
            $categoryTmp->setStoreId($storeId);
            $categoryTmp->setPath($parentPath);
			$categoryTmp->setCategoryNetsuiteInternalId($internalId);
            $categoryTmp->save(); */
		}
		return $cate_data->getFirstItem();
	}
}

