<?php

namespace Nexuscellular\Netsuite\Model;

class Updaterecord extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'netsuite_updaterecord';

	protected $_cacheTag = 'netsuite_updaterecord';

	protected $_eventPrefix = 'netsuite_updaterecord';

	protected function _construct()
	{
		$this->_init('Nexuscellular\Netsuite\Model\ResourceModel\Updaterecord');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}