<?php

namespace Nexuscellular\Netsuite\Model;

class Netsuiterecord extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'nexuscellular_netsuite_netsuiterecord';

	protected $_cacheTag = 'nexuscellular_netsuite_netsuiterecord';

	protected $_eventPrefix = 'nexuscellular_netsuite_netsuiterecord';

	protected function _construct()
	{
		$this->_init('Nexuscellular\Netsuite\Model\ResourceModel\Netsuiterecord');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}