<?php

namespace Nexuscellular\Netsuite\Model\ResourceModel\Netsuiterecord;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'netsuite_cron_record_collection';
	protected $_eventObject = 'netsuiterecord_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Nexuscellular\Netsuite\Model\Netsuiterecord', 'Nexuscellular\Netsuite\Model\ResourceModel\Netsuiterecord');
	}
	
}