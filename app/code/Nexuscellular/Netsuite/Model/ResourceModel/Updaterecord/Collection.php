<?php

namespace Nexuscellular\Netsuite\Model\ResourceModel\Updaterecord;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'netsuite_updaterecord_collection';
	protected $_eventObject = 'netsuite_updaterecord_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Nexuscellular\Netsuite\Model\Updaterecord', 'Nexuscellular\Netsuite\Model\ResourceModel\Updaterecord');
	}
	
}