<?php

namespace Nexuscellular\Netsuite\Model\ResourceModel\Productinserted;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'netsuite_product_inserted_collection';
	protected $_eventObject = 'netsuite_product_inserted_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Nexuscellular\Netsuite\Model\Productinserted', 'Nexuscellular\Netsuite\Model\ResourceModel\Productinserted');
	}
	
}