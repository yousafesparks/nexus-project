<?php

namespace Nexuscellular\Netsuite\Model\ResourceModel;

class Updaterecord extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('netsuite_update_record', 'id');
	}
	
}