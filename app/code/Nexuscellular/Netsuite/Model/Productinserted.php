<?php

namespace Nexuscellular\Netsuite\Model;

class Productinserted extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'netsuite_product_inserted';

	protected $_cacheTag = 'netsuite_product_inserted';

	protected $_eventPrefix = 'netsuite_product_inserted';

	protected function _construct()
	{
		$this->_init('Nexuscellular\Netsuite\Model\ResourceModel\Productinserted');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}