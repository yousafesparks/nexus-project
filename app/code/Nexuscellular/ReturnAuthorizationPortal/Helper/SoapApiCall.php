<?php

namespace Nexuscellular\ReturnAuthorizationPortal\Helper;
require_once(BP.'/var/netsuitelibrary/PHPToolkit_2020_1/samples/getSalesOrderDetails.php');
class SoapApiCall extends \Magento\Framework\App\Helper\AbstractHelper
{
	
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

	public function getSalesOrderByInternalId($data){
		$values = [];
		$orderType = (strtoLower($data['type']) == "cashsale") ? 'cashSale' : 'invoice';
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $data["transactionid"];
		$request->baseRef->type = $orderType;
		$getResponse = $service->get($request);
		
		if (!$getResponse->readResponse->status->isSuccess) {
			echo "GET ERROR";
		} else {
			$itemInternalId = $data['iteminternalid'];
			$response = json_decode(json_encode($getResponse->readResponse->record->itemList->item,true),true);
		//echo "<pre>";print_r();die();
			foreach($response as $item){
				if($item['item']['internalId'] == $itemInternalId){
					$values = ['rate'=>$item['rate'],'taxcode'=>$item['taxCode']['internalId'],'soap_trandate'=>$getResponse->readResponse->record->tranDate];
					break;
				}
			}
		}
		
		return $values;
	}
	public function getCustomerByInternalId($customerInternalId){
		$response = [];
		
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $customerInternalId;
		$request->baseRef->type = 'customer';
		$getResponse = $service->get($request);
		
		if (!$getResponse->readResponse->status->isSuccess) {
			echo "GET ERROR";
		} else {
			$response = json_decode(json_encode($getResponse->readResponse->record,true),true);	
		}
		return $response;
	}
	public function getItemSerialNumbers($data){
		$values = [];
		 $orderType = (strtoLower($data['type']) == "cashsale") ? 'cashSale' : 'invoice';
		
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $data["transactionid"];
		$request->baseRef->type = $orderType;
		$getResponse = $service->get($request);
		
		if (!$getResponse->readResponse->status->isSuccess) {
			echo "GET ERROR";
		} else {
			$itemInternalId = $data['iteminternalid'];
			$response = json_decode(json_encode($getResponse->readResponse->record->itemList->item,true),true);
			
		$Quantity = 0;
		$serialNumbers = [];
			foreach($response as $item){
				if($item['item']['internalId'] == $itemInternalId){
					$Quantity = $item['quantity'];
					$inventoryDetails = $item['inventoryDetail']['inventoryAssignmentList']['inventoryAssignment'];
					foreach($inventoryDetails as $invAssign){
						$serialNumbers[] = ['serialnumber'=>$invAssign['issueInventoryNumber']['name'],
						'id'=>$invAssign['issueInventoryNumber']['internalId']
						];
						//$serialNumbers[] = $invAssign['issueInventoryNumber']['name'];
					}
					//break;
				}
			}
		}
		
	 	$finalItem = [];
		$i=0;
		foreach($serialNumbers as $serializeItem){
			$finalItem[$i] = $data;
			$finalItem[$i]['serialnumber'] = $serializeItem['serialnumber'];
			$finalItem[$i]['quantity'] = 1;
			$finalItem[$i]['uniquekey'] = $data['tranid'].$data['sku'].$finalItem[$i]['serialnumber'].$serializeItem['id'];
			$i++;
		}
		/*  for($i=0;$i<$Quantity;$i++){
			$finalItem[$i] = $data;
			$finalItem[$i]['serialnumber'] = $serialNumbers[$i];
			$finalItem[$i]['quantity'] = 1;
			$finalItem[$i]['uniquekey'] = $data['tranid'].$data['sku'].$finalItem[$i]['serialnumber'];
		} */
		//print_r($finalItem);die();
		return $finalItem;
	}
	public function getReturnAuthorizationPortalById($internalid){
		$items = [];
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $internalid;
		$request->baseRef->type = 'returnAuthorization';
		$getResponse = $service->get($request);
		
		if ($getResponse->readResponse->status->isSuccess) {
			 $items = json_decode(json_encode($getResponse->readResponse->record,true),true);
			//echo "<pre>";print_r($items);die();
		}
		return $items;
	}
	
}