<?php
namespace Nexuscellular\ReturnAuthorizationPortal\Controller\Index;

use Nexuscellular\ReturnAuthorizationPortal\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\ReturnAuthorizationPortal\Helper\Data as apiHelper;
class Create extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      apiHelper $apiHelper,
	  \Magento\Framework\App\Response\RedirectInterface $redirect,
	  \Magento\Framework\UrlInterface $url,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->apiHelper = $apiHelper;
	  $this->redirect = $redirect;
	  $this->urlInterface = $url;
      return parent::__construct($context);
    }

    public function execute()
    {
		$refererUrl = $this->redirect->getRefererUrl();
		 
		if($this->customerSession->isLoggedIn() && $this->customerSession->getCustomer()->getEmail() == "sales@nexuscellular.com") {
		return $this->_pageFactory->create();
		}else{
	    $login_url = $this->urlInterface
                          ->getUrl('customer/account/login', 
                                array('referer' => base64_encode($refererUrl))
                            );
       // Redirect to login URL
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($login_url);
        return $resultRedirect;
		}
      }
     // $this->apiHelper->unsetCustomerSessionData();
      
  }
  

