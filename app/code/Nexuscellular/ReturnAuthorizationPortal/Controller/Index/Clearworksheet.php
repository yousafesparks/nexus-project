<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\ReturnAuthorizationPortal\Controller\Index;
use Nexuscellular\ReturnAuthorizationPortal\Helper\Data as apiHelper;
class Clearworksheet extends \Magento\Framework\App\Action\Action
{
    protected $apiHelper;

  /**
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;

  protected $resourceConnection;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        apiHelper $apiHelper
    ) {
        parent::__construct($context);
        $this->jsonHelper = $jsonHelper;
        $this->apiHelper = $apiHelper;
        $this->resourceConnection = $resourceConnection;
    }
	
	  public function execute(){
      $response = [];
      try {
             $search = strtoupper(trim($this->getRequest()->getParam('search')));
              $customerId = $this->apiHelper->getCustomerId();
             
             if(!$customerId){
              $response = $this->_responseError();
              $response['customer'] = 0;
              $response['message'] = 'please select the customer first.session is expired';
              return $this->getResponse()->representJson(
                  $this->jsonHelper->jsonEncode($response)
              );
            }
              if (!empty($customerId)){
                $tableName = $this->_getTableName('netsuite_raportal_worksheet');
                $results = $this->resourceConnection->getConnection()->delete(
                      $tableName,
                      ['customer_id = ?' => $customerId]
                      );
                 
                if($results > 0){
                  $items = [];
                  if(!empty($search)){
                    $items = $this->apiHelper->searchOrder($search);
                  }
                $response['success'] = true;
                $response['customer'] = 1;
                $response['data'] = $items;
                $response['worksheet'] = $this->apiHelper->getWorkSheetData();
                $response['message'] = '';
                }else{
                  $response = $this->_responseError();
                }
            }else{ 
              $response = $this->_responseError();
            }
            
          }catch (\Exception $e) {
           // echo $e->getMessage();die();
            $response = $this->_responseError();
        }
        return $this->getResponse()->representJson(
          $this->jsonHelper->jsonEncode($response)
         );
    }
    protected function _getTablename($tableName){
      /* Create Connection */
      $connection  = $this->resourceConnection->getConnection();
      $tableName   = $connection->getTableName($tableName);
      return $tableName;
    }
    protected function _responseError(){
      $response['success'] = false;
      $response['customer'] = 1;
      $response['message'] = 'An error occurred while processing your form. Please try again later.';
      return $response;
    }
  }
  

