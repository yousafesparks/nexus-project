<?php
namespace Nexuscellular\ReturnAuthorizationPortal\Controller\Index;

use Nexuscellular\ReturnAuthorizationPortal\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\ReturnAuthorizationPortal\Helper\Data as apiHelper;
use Nexuscellular\ReturnAuthorization\Helper\SoapApiCall as soapApiCall;
class TestImport extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $catalogSession;
  protected $returnRulesFactory;
  protected $apiHelper;
  protected $jsonHelper;
  protected $soapApiCall;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Catalog\Model\Session $catalogSession,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper,
      soapApiCall $soapApiCall,
      \Magento\Framework\Json\Helper\Data $jsonHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->catalogSession = $catalogSession;
      $this->apiHelper = $apiHelper;
      $this->jsonHelper = $jsonHelper;
      $this->soapApiCall = $soapApiCall;
      return parent::__construct($context);
    }

    public function execute(){
		$customerArray = [];
		$url = 'https://4000493.suitetalk.api.netsuite.com/services/rest/query/v1/suiteql?limit=1000';
		$this->allCustomers($customerArray,$url);
	}
	protected function allCustomers($customerArray,$url){
		$customerList = json_decode(file_get_contents($this->apiHelper->getJsUrl()),true);
		if(is_array($customerList) && count($customerList) > 0)
			$maxInternalId =  max(array_column($customerList, 'id'));
		else
			$maxInternalId = 0;
		$query= "{\n\t\"q\": \"SELECT id,entityid as value FROM customer WHERE  id > ".$maxInternalId." AND searchstage = 'Customer'\"\n}";
		$response = $this->restSuiteqlApi->callRestApi($query,$url);
		//echo "<pre>";	print_r($response);die();
		$newCustomers = [];
		if(isset($response['count']) && $response['count'] > 0){
			foreach($response['items'] as $item){
				unset($item['links']);
				$customerArray[] = $item;
			}
			$nextUrl = $this->_searchNextUrl($response);
			if(!is_null($nextUrl)){
				$this->allCustomers($customerArray,$nextUrl);
			}else{
				$customerJsFilePath = $this->apiHelper->getFilePath();
				$this->apiHelper->createProductJsFile($customerJsFilePath,$customerArray);
			}
		}
	}
	protected function _searchNextUrl($response){
		$nextUrl = null;
		foreach($response['links'] as $link){
			if($link['rel'] == 'next'){
				$nextUrl = $link['href'];break;
			}
		}
		return $nextUrl;
	}
  
}
