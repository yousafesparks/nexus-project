<?php
namespace Nexuscellular\ReturnAuthorizationPortal\Controller\Index;

use Nexuscellular\ReturnAuthorizationPortal\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\ReturnAuthorizationPortal\Helper\Data as apiHelper;
use Nexuscellular\ReturnAuthorization\Helper\SoapApiCall as soapApiCall;
class InitialReturnData extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $catalogSession;
  protected $returnRulesFactory;
  protected $apiHelper;
  protected $jsonHelper;
  protected $soapApiCall;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Catalog\Model\Session $catalogSession,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper,
      soapApiCall $soapApiCall,
      \Magento\Framework\Json\Helper\Data $jsonHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->catalogSession = $catalogSession;
      $this->apiHelper = $apiHelper;
      $this->jsonHelper = $jsonHelper;
      $this->soapApiCall = $soapApiCall;
      return parent::__construct($context);
    }

    public function execute()
    {
      $response = [];
      if (!$this->getRequest()->isPost()) {
        $response = $this->_responseError();
         return $this->getResponse()->representJson(
             $this->jsonHelper->jsonEncode($response)
         );
     }
     $this->apiHelper->unsetCustomerSessionData();
     $customerNetsuiteInternalId = $this->getRequest()->getParam('customer_id');
     
      //echo $query = "{\n\t\"q\": \"SELECT customer.custentity_f3_special_customer as custentity_f3_special_customer,customer.custentitycustentity_f3_three_warranty as custentity_f3_three_warranty,customer.id as customer_id,TransactionLine.quantity,TransactionLine.Item as iteminternalid,TransactionLine.custcol_f3_document_number,Transaction.trandate,Transaction.recordtype FROM TransactionLine INNER JOIN Transaction ON(Transaction.id = TransactionLine.transaction AND TransactionLine.entity = ".$customerNetsuiteInternalId." and Transaction.recordtype='returnauthorization' AND TransactionLine.itemtype != 'TaxGroup' AND TransactionLine.itemtype != 'ShipItem' AND TransactionLine.taxline = 'F' AND TransactionLine.mainline = 'F') INNER JOIN customer ON(Transaction.entity = customer.id) order by trandate DESC\"\n}";
	  $query = "{\n\t\"q\": \" SELECT customer.custentity_f3_special_customer as custentity_f3_special_customer,customer.custentitycustentity_f3_three_warranty as custentity_f3_three_warranty,customer.id as customer_id,TransactionLine.itemtype as itemtype,TransactionLine.quantity,TransactionLine.Item as iteminternalid,TransactionLine.custcol_f3_document_number,Transaction.trandate,Transaction.recordtype FROM TransactionLine,Transaction,customer WHERE  Transaction.id = TransactionLine.transaction AND Transaction.entity = customer.id AND TransactionLine.entity = ".$customerNetsuiteInternalId." and Transaction.recordtype='returnauthorization'  AND TransactionLine.taxline = 'F' AND TransactionLine.mainline = 'F' AND TransactionLine.itemtype != 'TaxGroup' AND TransactionLine.itemtype != 'ShipItem' ORDER BY trandate DESC\"\n}";
     $responseData = $this->restSuiteqlApi->callRestApi($query);
     
     if(isset($responseData['count']) && $responseData['count'] > 0 ){
       $this->catalogSession->setNetsuiteInternalId($customerNetsuiteInternalId);
       $this->catalogSession->setCustomerReturnPortalData([]);
       $this->setCustomerAttribute($responseData['items'] ); 
     }else if(isset($responseData['count']) && $responseData['count'] == 0){
        $query = "{\n\t\"q\": \"SELECT customer.custentity_f3_special_customer as custentity_f3_special_customer,customer.custentitycustentity_f3_three_warranty as custentity_f3_three_warranty,customer.id as customer_id FROM customer WHERE id = '".$customerNetsuiteInternalId."'\"\n}";
        $responseData = $this->restSuiteqlApi->callRestApi($query);
      if(isset($responseData['count']) && $responseData['count'] > 0 ){
          $this->catalogSession->setNetsuiteInternalId($responseData['items'][0]['customer_id']);
          $this->catalogSession->setCustomerReturnPortalData([]);
          $this->setCustomerAttribute($responseData['items']); 
         
        }else{
          $response = $this-> _responseError();
          return $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
        }
		$this->catalogSession->setCustomerReturnPortalData([]);
    }else{
      $response = $this-> _responseError();
      return $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
    }
  
	$responseCustomer = $this->soapApiCall->getCustomerByInternalId($customerNetsuiteInternalId );
	$this->catalogSession->setCustomerGroupPricing([]);
	   if(count($responseCustomer) > 0 && !empty($responseCustomer['groupPricingList'])){
		$this->catalogSession->setCustomerGroupPricing($responseCustomer['groupPricingList']['groupPricing']);
	 }
     $response['success'] = true;
     $response['message'] = 'fetched customer return data';
     return $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
    }
    protected function _responseError(){
      $response['success'] = false;
      $response['message'] = 'An error occurred while processing your form. Please try again later.';
      return $response;
    }
    public function setCustomerAttribute($items){
      $this->catalogSession->setData('custentity_f3_special_customer',0);
      $this->catalogSession->setData('custentity_f3_three_warranty',0);
      if(count($items) > 0){
        foreach($items as $item){
          if(isset($item['custentity_f3_special_cus#']) && $item['custentity_f3_special_cus#'] == 'T'){
            $this->catalogSession->setData('custentity_f3_special_customer',1);
          }
          if(isset($item['custentity_f3_three_warra#']) && $item['custentity_f3_three_warra#'] == 'T'){
            $this->catalogSession->setData('custentity_f3_three_warranty',1);
          }
          break;
        }
      }
      
    }
  }
  

