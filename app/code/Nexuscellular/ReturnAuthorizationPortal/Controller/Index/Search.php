<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\ReturnAuthorizationPortal\Controller\Index;
use Nexuscellular\ReturnAuthorizationPortal\Helper\Data as apiHelper;
class Search extends \Magento\Framework\App\Action\Action
{
    protected $apiHelper;

  /**
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        apiHelper $apiHelper
    ) {
        parent::__construct($context);
        $this->jsonHelper = $jsonHelper;
        $this->apiHelper = $apiHelper;
    }
	
	  public function execute(){
      $response = [];
      try {
               $customerId =  $this->apiHelper->getCustomerNetsuiteInternalId();
              if(!$customerId){
               // die('customer');
                $response = $this->_responseError();
                $response['customer'] = 0;
                $response['message'] = 'please select the customer first.session is expired';
                return $this->getResponse()->representJson(
                    $this->jsonHelper->jsonEncode($response)
                );
              }
             $search = strtoupper(trim($this->getRequest()->getParam('search')));
            
              if (!empty($search)){
                $items = $this->apiHelper->searchOrder($search);
               
                if(count($items)> 0)
                 $message = '';
                else
                $message = 'Sorry, there are no items matching your search criteria that are eligible for return. Please try again or refer to our ';

                $response['success'] = true;
                $response['data'] = $items;
                $response['customer'] = 1;
                $response['message'] = $message;
            }
            
          }catch (\Exception $e) {
            //echo $e->getMessage();die('last');
            $response = $this->_responseError();
            $response['customer'] = 1;
        }
        return $this->getResponse()->representJson(
          $this->jsonHelper->jsonEncode($response)
         );
    }
    protected function _responseError(){
      $response['success'] = false;
      $response['data'] = [];
      $response['message'] = 'An error occurred while processing your form. Please try again later.';
      return $response;
    }
  }
  

