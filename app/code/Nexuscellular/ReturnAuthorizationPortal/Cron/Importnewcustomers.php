<?php

namespace Nexuscellular\ReturnAuthorizationPortal\Cron;

use Nexuscellular\ReturnAuthorizationPortal\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\ReturnAuthorizationPortal\Helper\Data as apiHelper;
class Importnewcustomers
{
	/*
	* Nexuscellular\ReturnAuthorizationPortal\Helper\SqlApiCall
	*/
	protected $restSuiteqlApi; 
	
	protected $apiHelper;
	
	protected $jsonHelper;
    /**
     * @param Nexuscellular\ReturnAuthorizationPortal\Helper\SqlApiCall
     */
    public function __construct(
       restSuiteqlApi $restapiHelper,
	    apiHelper $apiHelper,
	   \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
       $this->restSuiteqlApi = $restapiHelper;
	   $this->apiHelper = $apiHelper;
	   $this->jsonHelper = $jsonHelper;
    }
    /**
     * Import new Customers From Netusite
     *
     * @return void
     */
	 public function execute(){
		$customerArray = [];
		$url = 'https://4000493.suitetalk.api.netsuite.com/services/rest/query/v1/suiteql?limit=1000';
		$this->allCustomers($customerArray,$url);
	}
	protected function allCustomers($customerArray,$url){
		/*$customerList = json_decode(file_get_contents($this->apiHelper->getJsUrl()),true);
		if(is_array($customerList) && count($customerList) > 0)
			$maxInternalId =  max(array_column($customerList, 'id'));
		else*/
			$maxInternalId = 0;
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info("<==============Calling Importnewcustomers Method =========>");
		
		$query= "{\n\t\"q\": \"SELECT id,entityid as value FROM customer WHERE  id > ".$maxInternalId." AND searchstage = 'Customer'\"\n}";
		$response = $this->restSuiteqlApi->callRestApi($query,$url);
		//echo "<pre>";	print_r($response);die();
		if(isset($response['count']) && $response['count'] > 0){
			foreach($response['items'] as $item){
				unset($item['links']);
				$customerArray[] = $item;
			}
			$nextUrl = $this->_searchNextUrl($response);
			if(!is_null($nextUrl)){
				$this->allCustomers($customerArray,$nextUrl);
			}else{
				$customerJsFilePath = $this->apiHelper->getFilePath();
				$this->apiHelper->createProductJsFile($customerJsFilePath,$customerArray);
				$logger->info("<==============Count new Netsuite customers  == ". count($customerArray) ."=========>");
				$logger->info("<==============Imported newcustomers Method =========>");
			}
		}
	}
	protected function _searchNextUrl($response){
		$nextUrl = null;
		foreach($response['links'] as $link){
			if($link['rel'] == 'next'){
				$nextUrl = $link['href'];break;
			}
		}
		return $nextUrl;
	}
}
