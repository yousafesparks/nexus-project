<?php
namespace Nexuscellular\Customization\Model\Elasticsearch\Layer\Search;
/**
 * Catalog search category layer collection provider.
 */
class ItemCollectionProvider extends \Magento\Elasticsearch\Model\Layer\Search\ItemCollectionProvider
{

    /**
     * @inheritdoc
     */
    public function getCollection(\Magento\Catalog\Model\Category $category)
    {
        if (!isset($this->factories[$this->engineResolver->getCurrentSearchEngine()])) {
            throw new \DomainException('Undefined factory ' . $this->engineResolver->getCurrentSearchEngine());
        }
        $collection = $this->factories[$this->engineResolver->getCurrentSearchEngine()]->create();
        //$collection->addFieldToFilter('show_notify', true);
        return $collection;
    }
}
