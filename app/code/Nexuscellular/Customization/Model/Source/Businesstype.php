<?php

namespace Nexuscellular\Customization\Model\Source;

class Businesstype extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource 
{
	protected $_storeManager;

	public function __construct(
		\Magento\Store\Model\StoreManagerInterface $storeManager
	) {
		$this->_storeManager = $storeManager;
	}

	public function getAllOptions() 
	{
		$codes = ['Franchisee Retail Store',
			'Wireless Carrier',
			'School Board / Government',
			'Independent Retail Store',
			'Warranty Centre',
			'Reseller',
			'Refurbishing / Repair Facility',
			'Insurance'
		];
		sort($codes);
		$codes[] = 'Other';
		$currency_codes = [];
		$currency_codes[] = ['value' => '', 'label' => __('Please Select')];
		foreach($codes as $code){
			$currency_codes[] = ['value' => $code, 'label' => $code];
		}
		//echo "<pre>";print_r($currency_codes);die();
		if ($this->_options === null) {

		$this->_options = 
		$currency_codes;

		}

	return $this->_options;

	}

	public function getOptionText($value) 
	{
		foreach ($this->getAllOptions() as $option)
		{
			if ($option['value'] == $value)
			{
			return $option['label'];
			}
		}
	return false;
	}
}