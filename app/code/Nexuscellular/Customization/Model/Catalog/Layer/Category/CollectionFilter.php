<?php

namespace Nexuscellular\Customization\Model\Catalog\Layer\Category;

class CollectionFilter extends \Magento\Catalog\Model\Layer\Category\CollectionFilter
{
    /**
     * Filter custom product collection
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @param \Magento\Catalog\Model\Category $category
     * @return void
     */
    public function filter(
        $collection,
        \Magento\Catalog\Model\Category $category
    ) {
        $collection
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
			//->addFieldToFilter('show_notify',1)
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($category->getId())
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds());
			
    }
}
