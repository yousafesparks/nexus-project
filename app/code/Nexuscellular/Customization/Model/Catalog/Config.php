<?php
namespace Nexuscellular\Customization\Model\Catalog;
class Config extends \Magento\Catalog\Model\Config
{
	/**
     * Default Order field
     *
     * @var string
     */
    protected $_catChildIds = null;
    /**
     * Retrieve Product List Default Sort By
     *
     * @param mixed $store
     * @return string
     */
    public function getProductListDefaultSortBy($store = null)
    { 
		$AccessoriesCategoriesList = [];
		// Sort By New Arrivals in Accessories and Devices
		if($this->isItAccessoriesChildCategory()){
			return 'custitem_f3_new_arrival';
		}else{
			return $this->_scopeConfig->getValue(
				self::XML_PATH_LIST_DEFAULT_SORT_BY,
				\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
				$store
			);
		}
    }
	 /**
     * Retrieve Attributes Used for Sort by as array
     * key = code, value = name
     *
     * @return array
     */
    public function getAttributeUsedForSortByArray()
    {
		if(!in_array($this->_getCurrentCategoryId(),$this->_getChildIds())){
			$options = ['position' => __('Sort By Part Type')];
		}
        foreach ($this->getAttributesUsedForSortBy() as $attribute) {
            /* @var $attribute \Magento\Eav\Model\Entity\Attribute\AbstractAttribute */
			if($attribute->getAttributeCode() == "price")
				continue;
            $options[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
        }
		 $options['low_to_high'] = __('Price,Low To High');
        $options['high_to_low'] = __('Price,High To Low');

        return $options;
    }
	public function isItAccessoriesChildCategory(){
		$bool = false;
		$childIds = $this->_getChildIds();
			if(!is_null($childIds)){
				if(in_array($this->_getCurrentCategoryId(),$childIds)){
					$bool = true;
				}
			}
		 return $bool;
	}
	/**
     * Get Current Category
     *
     * @return null|integer
     */
	protected function _getCurrentCategoryId(){
		$categoryId = null;
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category = $objectManager->get('Magento\Framework\Registry')->registry('current_category');//get current category
		if(!is_null($category))
			$categoryId = $category->getId();
		return $categoryId;
	}
	  /**
     * Get Category Child Ids //Accessories AND Devices
     *
     * @return null|string
     */
    protected function _getChildIds()
    {
        if ($this->_catChildIds === null) {
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$_categoryFactory = $objectManager->get('Magento\Catalog\Model\CategoryFactory');
			$categoriesCollection = $_categoryFactory->create()->getCollection()->addFieldToFilter('name',array('in'=>['Accessories','Devices']));
			$childIds = [];
			if($categoriesCollection->getSize() && $categoriesCollection->getFirstItem()->hasChildren()){
				 foreach($categoriesCollection as $category){
					$childIds[]= $category->getId();
					$subCategories = $category->getChildrenCategories(); //getChildren()
					 foreach($subCategories as $sub){
						$childIds[] = $sub->getId();
					}
				 }
				 $this->_catChildIds = $childIds;
			}
		}
		return $this->_catChildIds;
	}
}
