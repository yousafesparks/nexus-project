<?php

namespace Nexuscellular\Customization\Model\Fedex;

class Carrier extends \Magento\Fedex\Model\Carrier
{
    /**
     * Override of function: Changed TotalNetCharge to TotalNetFedExCharge becuase of Bug wehre Gross value with tax was being sent by Fedex in TotalNetCharge and magento was calculating tax on already taxed amount
	 * Get origin based amount form response of rate estimation
     *
     * @param \stdClass $rate
     * @return null|float
     */
     protected function _getRateAmountOriginBased($rate)
    {
        $amount = null;
        $currencyCode = '';
        $rateTypeAmounts = [];
        if (is_object($rate)) {
            // The "RATED..." rates are expressed in the currency of the origin country
            foreach ($rate->RatedShipmentDetails as $ratedShipmentDetail) {
                $netAmount = (string)$ratedShipmentDetail->ShipmentRateDetail->TotalNetFedExCharge->Amount;
                $currencyCode = (string)$ratedShipmentDetail->ShipmentRateDetail->TotalNetFedExCharge->Currency;
                $rateType = (string)$ratedShipmentDetail->ShipmentRateDetail->RateType;
                $rateTypeAmounts[$rateType] = $netAmount;
            }

            foreach ($this->_ratesOrder as $rateType) {
                if (!empty($rateTypeAmounts[$rateType])) {
                    $amount = $rateTypeAmounts[$rateType];
                    break;
                }
            }

            if ($amount === null) {
                $amount = (string)$rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetFedExCharge->Amount;
            }

            $amount = (float)$amount * $this->getModifiedBaseCurrencyRate($currencyCode);
        }

        return $amount;
    }

	public function getModifiedBaseCurrencyRate(string $currencyCode): float
    {
        if (!isset($this->baseCurrencyRate[$currencyCode])) {
            $baseCurrencyCode = $this->_request->getBaseCurrency()->getCode();
            $rate = $this->_currencyFactory->create()
                ->load($currencyCode)
                ->getAnyRate($baseCurrencyCode);
				
            if ($rate === false) {
                $errorMessage = __(
                    'Can\'t convert a shipping cost from "%1-%2" for FedEx carrier.',
                    $currencyCode,
                    $baseCurrencyCode
                );
                $this->_logger->critical($errorMessage);
                throw new LocalizedException($errorMessage);
            }
            $this->baseCurrencyRate[$currencyCode] = (float)$rate;
        }

        return $this->baseCurrencyRate[$currencyCode];
    }
   /**
     * Forming request for rate estimation depending to the purpose
     *
     * @param string $purpose
     * @return array
     */
    protected function _formRateRequest($purpose)
    {
        $r = $this->_rawRequest;
        $ratesRequest = [
            'WebAuthenticationDetail' => [
                'UserCredential' => ['Key' => $r->getKey(), 'Password' => $r->getPassword()],
            ],
            'ClientDetail' => ['AccountNumber' => $r->getAccount(), 'MeterNumber' => $r->getMeterNumber()],
            'Version' => $this->getVersionInfo(),
            'RequestedShipment' => [
                'DropoffType' => $r->getDropoffType(),
                'ShipTimestamp' => date('c'),
                'PackagingType' => $r->getPackaging(),
                'TotalInsuredValue' => ['Amount' => '0.00', 'Currency' => $this->getCurrencyCode()],
                'Shipper' => [
                    'Address' => ['PostalCode' => $r->getOrigPostal(), 'CountryCode' => $r->getOrigCountry()],
                ],
                'Recipient' => [
                    'Address' => [
                        'PostalCode' => $r->getDestPostal(),
                        'CountryCode' => $r->getDestCountry(),
                        'Residential' => (bool)$this->getConfigData('residence_delivery'),
                    ],
                ],
                'ShippingChargesPayment' => [
                    'PaymentType' => 'SENDER',
                    'Payor' => ['AccountNumber' => $r->getAccount(), 'CountryCode' => $r->getOrigCountry()],
                ],
                'CustomsClearanceDetail' => [
                    'CustomsValue' => ['Amount' => $r->getValue(), 'Currency' => $this->getCurrencyCode()],
                ],
                'RateRequestTypes' => 'LIST',
                'PackageCount' => '1',
                'PackageDetail' => 'INDIVIDUAL_PACKAGES',
                'RequestedPackageLineItems' => [
                    '0' => [
                        'Weight' => [
                            'Value' => (double)$r->getWeight(),
                            'Units' => $this->getConfigData('unit_of_measure'),
                        ],
                        'GroupPackageCount' => 1,
                    ],
                ],
            ],
        ];

        if ($r->getDestCity()) {
            $ratesRequest['RequestedShipment']['Recipient']['Address']['City'] = $r->getDestCity();
        }

        if ($purpose == self::RATE_REQUEST_GENERAL) {
            $ratesRequest['RequestedShipment']['RequestedPackageLineItems'][0]['InsuredValue'] = [
                'Amount' => '0.00',
                'Currency' => $this->getCurrencyCode(),
            ];
        } else {
            if ($purpose == self::RATE_REQUEST_SMARTPOST) {
                $ratesRequest['RequestedShipment']['ServiceType'] = self::RATE_REQUEST_SMARTPOST;
                $ratesRequest['RequestedShipment']['SmartPostDetail'] = [
                    'Indicia' => (double)$r->getWeight() >= 1 ? 'PARCEL_SELECT' : 'PRESORTED_STANDARD',
                    'HubId' => $this->getConfigData('smartpost_hubid'),
                ];
            }
        }

        return $ratesRequest;
    }

}
