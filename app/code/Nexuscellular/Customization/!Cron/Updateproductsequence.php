<?php
namespace Nexuscellular\Customization\Cron;

class Updateproductsequence
{
	/*
	* \Magento\Eav\Model\Config
	*/
	protected $eavConfig;
	
	/*
	* \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
	*/
	protected $_productCollectionFactory;
    /**
     * @param Nexuscellular\ReturnAuthorizationPortal\Helper\SqlApiCall
     */
    public function __construct(
		\Magento\Eav\Model\Config $eavConfig,
	   \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
	   $this->eavConfig = $eavConfig;
		$this->_productCollectionFactory = $productCollectionFactory;
    }
    /**
     * Update Product Type Position Field In Product
     *
     * @return void
     */
     public function execute()
	 {
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info("<==============Call UpdateProductSequence Method =========>");
		$a3 = [];
		$sequence = $this->getSequenceType();
		$productType = $this->getProductTypeArray();
		$results = array_diff($this->getProductTypeArray(),$this->getSequenceType(),$a3);
		foreach($results as $res){
		 $sequence[] = $res;
		}
		$collection = $this->_productCollectionFactory->create();
		$collection->addAttributeToSelect('*');
		foreach ($collection as $product){
			if(!empty($product->getData('custitem_part_type'))){
				$indexNumber = explode(',',$product->getData('custitem_part_type'));
				if($productType[$indexNumber[0]] == "Displays"){
					if(!empty($product->getData('custitem_grade_value')))
						$priorityNumber = array_search ($product->getData('custitem_grade_value'), $this->getSequenceGradeType());
					else
						$priorityNumber = array_search ($productType[$indexNumber[0]], $sequence);
				}else{
					$priorityNumber = array_search ($productType[$indexNumber[0]], $sequence);
				}
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$simpleProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getId()); 
				$simpleProduct->setData('part_type_position',$priorityNumber);
				$simpleProduct->save();
				unset($simpleProduct);
			}
		}
	$logger->info("<==============Finished UpdateProductSequence Method =========>");		
	 }
	public function getSequenceType(){
		return $array = [
		'1.0'=>'Displays',
		'1.2'=> "Batteries",
		'1.3'=>"Battery Covers",
		'1.4'=>"Housings",
		'1.5'=>"Cameras & Lenses",
		'1.6'=>"Flex Cables & Connectors",
		'1.7'=>"Sim Trays",
		'1.8'=>"Buttons",
		'1.9'=>"Speakers",
		'2'=>"Vibrate Motors",
		'2.1'=>"Mics",
		'2.2'=>"Antennas",
		'2.3'=>"Adhesives",
		'2.4'=>"Board IC's",
		'2.5'=>"Styluses",
		'2.6'=>"Power Supplies",
		'2.7'=>"Cooling Fans",
		'2.8'=>"Disk Drives",
		'2.9' => 'Audio',
		'3.0' => 'Board ICs',
		'3.1' => 'Cables',
		'3.2' => 'Chargers',
		'3.3' => 'Computer Accessories',
		'3.4' => 'Docks/Mounts & Stands',
		'3.5' => 'Gadgets',
		'3.6' => 'Glass Lens',
		'3.7' => 'Hard Drives',
		'3.8' => 'Packaging & Displays',
		'3.9' => 'Phone Cases',
		'4.0' => 'Portable Chargers',
		'4.1' => 'Repair Tools',
		'4.2' => 'Screen Protection',
		'4.3' => 'SIM Cards',
		'4.4' => 'SIM Trays',
		'4.5' => 'Storage',
		'4.6' => 'Tablet Cases',
		'4.7' => 'Tools',
		'4.8' => 'Sensor',
		'4.9' => 'Health & Safety Products',
		];
	}
	  public function getSequenceGradeType(){
		return $array = [
		'1.01'=>'Value',
		'1.02'=> "Plus",
		'1.03'=>"Dynamic",
		'1.04'=>"OEM Reclaim C",
		'1.05'=>"OEM Reclaim B",
		'1.06'=>"OEM Reclaim A",
		'1.07'=>"Premium FOG",
		'1.08'=>"Premium",
		'1.09'=>"Brand New",
		'1.10'=>"New Original",
		'1.11'=>"BuyBack",
		'1.12'=>"Pre-Owned Grade B",
		'1.13'=>"Pre-Owned Grade A",
		'1.14'=>"Special"
		];
	}
	public function getProductTypeArray(){
		$newOptions = [];
		 $attribute = $this->eavConfig->getAttribute('catalog_product', 'custitem_part_type');
		$options = $attribute->getSource()->getAllOptions();
		unset($options[0]);
		foreach($options as $option){
			$newOptions[$option['value']] = $option['label'];
		}
		//print_r($options);die();
		return $newOptions;
	}
	public function getProductGradeArray(){
		$newOptions = [];
		 $attribute = $this->eavConfig->getAttribute('catalog_product', 'custitem_grade');
		$options = $attribute->getSource()->getAllOptions();
		unset($options[0]);
		foreach($options as $option){
			$newOptions[$option['value']] = $option['label'];
		}
		return $newOptions;
	}
}
