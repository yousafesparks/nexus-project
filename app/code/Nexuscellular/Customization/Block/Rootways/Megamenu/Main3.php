<?php

namespace Nexuscellular\Customization\Block\Rootways\Megamenu;

class Main3 extends \Rootways\Megamenu\Block\Main3
{
    
    
    /**
     * Support for get attribute value with HTML
     */
    public function getBlockContent($content = '')
    {
        if (!$this->_filterProvider) {
            return $content;
        }
        return $this->_filterProvider->getBlockFilter()->filter(trim($content));
    }
   
    /**
     * Check if current page is home
     */
    public function getIsHomePage()
    {
        $currentUrl = $this->getUrl('', ['_current' => true]);
        $urlRewrite = $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
        return $currentUrl == $urlRewrite;
    }
    
    /**
     * Return categories helper
     */
    public function getCategoryHelper()
    {
        return $this->_categoryHelper;
    }
    
    /**
     * Return top menu html
     */
    public function getHtml()
    {
        return $this->topMenu->getHtml();
    }
    
    /**
     * Retrieve current store categories
     */
    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        return $this->_categoryHelper->getStoreCategories($sorted , $asCollection, $toLoad);
    }
    
    /**
     * Retrieve child store categories
     */
    public function getChildCategories($category)
    {
        $children = [];
        if ($this->categoryFlatConfig->isFlatEnabled() && $category->getUseFlatResource()) {
            $subcategories = (array)$category->getChildrenNodes();
        } else {
            $subcategories = $category->getChildren();
        }
        foreach ($subcategories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }
            $children[] = $category;
        }
        return $children;
    }
    public function getImageHtml($mainCat, $currentCat, $catLevel)
    {
        if ($catLevel == 1) {
            $w = $mainCat->getMegamenuShowCatimageWidth().'px';
            $h = $mainCat->getMegamenuShowCatimageHeight().'px';
            $arrowHtml = '<span class="cat-arrow"></span>';
        } else {
            $w = '25px';
            $h = '25px';
            $arrowHtml = ''; // This is for sub-sub categoires layout. Arrow is not requried for sub-sub categories.
        }
        
        if ($mainCat->getMegamenuShowCatimage() == 1) {
            if ($this->_customhelper->getMegaMenuImageName($currentCat) != '') {
                $imageurl = $this->_customhelper->getMegaMenuImageUrl($currentCat);
            } else {
                $imageurl = $this->getViewFileUrl('Rootways_Megamenu::images/rootimgicon.jpg');
            }
            $image_html = '<img class="cat_img_as_icon" style="width:'.$w.'; height:'.$h.';" src='.$imageurl.' alt="'.$currentCat->getName().'"/>';
        } else {
            $image_html = $arrowHtml;
        }
        return $image_html;
    }
    
    public function getImageHtmlAsTitle($mainCat, $currentCat, $catLevel)
    {
        $image_html = '';
        if ($mainCat->getMegamenuShowCatimage() == 1 && $this->_customhelper->getMegaMenuImageName($currentCat) != '') {
            $imageurl = $this->_customhelper->getMegaMenuImageUrl($currentCat);
            $image_html = '<a href='.$currentCat->getURL().' class="catproductimg"><img width='.$mainCat->getMegamenuShowCatimageWidth().' height='.$mainCat->getMegamenuShowCatimageHeight().' src='.$imageurl.' alt="'.$currentCat->getName().'"/></a>';
        }
        return $image_html;
    }
    
    /**
     * Half-Width Menu With Content Only HTML Block.
     */
    public function halfMenuContentOnly($category, $navCnt0)
	{
        return $this->halfFullContentOnlyHtml($category, 0);
	}
    
    /**
     * Full-Width Mega Menu HTML Block.
     */
	public function fullWidthMenu($category, $navCnt0)
	{
       return $this->halfFullHtml($category, $navCnt0, '1');
	}
    
    /**
     * Full-Width Content Only Mega Menu HTML Block.
     */
	public function fullWidthContentOnly($category, $navCnt0)
	{
        return $this->halfFullContentOnlyHtml($category, 1);
	}
	
    /**
     * Tabbing Mega Menu HTML Block.
     */  	
	public function tabMenu($category, $navCnt0)
	{
        $main_cat = $this->categoryRepository->get($category->getId(), $this->_customhelper->getStoreId());
        
        $viewMoreAfter = $main_cat->getMegamenuTypeViewmore();
        $colnum = $main_cat->getMegamenuTypeNumofcolumns();
		if ($colnum == 0) {
            $colnum = 5;
        }
        $left_width = $main_cat->getMegamenuTypeLeftblockW();
        $right_width = $main_cat->getMegamenuTypeRightblockW();
        $cat_width = 12 - ($left_width + $right_width);
        $category_area_width = 'root-sub-col-'.$cat_width;
        $left_content_area = 'root-sub-col-'.$left_width;
        $right_content_area = 'root-sub-col-'.$right_width;
        $msClasses = $this->masonryCategoryClass($category->getId());
        $masonryClass = $msClasses[0];
        $colClass = $msClasses[1];
		$catHtml = '';
		if ($childrenCategories = $this->getChildCategories($category)) {
			$catHtml .= '<div class="megamenu fullmenu clearfix tabmenu">';
			$catHtml .= '<div class="mainmenuwrap clearfix">';
			$catHtml .= '<ul class="root-col-1 clearfix vertical-menu">';
				$cnt = 0;
                 // 2nd Level Category
				 //$catHtml .= '<li class="clearfix level1-cat back-button show-mobile-view"><a href=javascript:void(0) class="root-col-4"><span class="cat-arrow"></span>BACK</a></li>';
				 $secondcatincrement = 0;
				foreach ($childrenCategories as $childCategory) {
					$secondcatincrement++;
                    $load_cat = $this->categoryRepository->get($childCategory->getId(), $this->_customhelper->getStoreId());
                    $left_sub_width = $load_cat->getMegamenuTypeLeftblockW();
                    $right_sub_width = $load_cat->getMegamenuTypeRightblockW();
                    $cat_sub_width = 12 - ($left_sub_width + $right_sub_width);
                    $sub_category_area_width = 'root-sub-col-'.$cat_sub_width;
                    $sub_left_content_area = 'root-sub-col-'.$left_sub_width;
                    $sub_right_content_area = 'root-sub-col-'.$right_sub_width;
                    if ($left_sub_width != 0 || $right_sub_width != 0) {
                        $category_area_width = 'root-sub-col-'.$cat_sub_width;
                    } else {
                        $category_area_width = 'root-sub-col-'.$cat_width;
                    }
                    
                    if ($cnt == 0) {
                        $open = "main_openactive01";
                    } else {
                        $open = "";
                    } $cnt++;
					
					$defaultActiveClass = '';
					//$catHtml .= '<li class="clearfix level1-cat '.$open.' '.$load_cat->getName().'"><a href=javascript:void(0) class="root-col-4">';
					if($secondcatincrement == 1)
						$defaultActiveClass = 'main_openactive01';
					
                    //$catHtml .= '<li class="clearfix level1-cat '.$open.' '. $defaultActiveClass .'  '.$load_cat->getName().'"><a href='.$load_cat->getUrl().' class="root-col-4">';
                   // $catHtml .= '<li class="clearfix level1-cat '.$open.' '.$load_cat->getName().'"><a href='.$load_cat->getUrl().' class="root-col-4">';
                   $catHtml .= '<li class="clearfix level1-cat '.$open.' '.$load_cat->getName().'"><a href="#" class="root-col-4">';
                    $catHtml .= $this->getImageHtml($main_cat, $load_cat, 2);
                    $catHtml .= $load_cat->getName().'<span class="cat-arrow"></span></a>';
                    if ($childrenCategories_2 = $this->getChildCategories($childCategory)) {
						$catHtml .= '<div class="root-col-75 verticalopen">';
						if($load_cat->getLandingPage()){
							$catHtml .= $this->getCmsBlockHtml($load_cat->getLandingPage());
						}
                            if ($load_cat->getMegamenuTypeHeader() != '') {
                                $catHtml .= '<div class="menuheader root-col-1 clearfix">';
                                    $catHtml .= $this->getBlockContent($load_cat->getData('megamenu_type_header'));
                                $catHtml .= '</div>';   
                            } elseif ($main_cat->getMegamenuTypeHeader() != '') {
                                $catHtml .= '<div class="menuheader root-col-1 clearfix">';
                                    $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_header'));
                                $catHtml .= '</div>';   
                            }
                            $catHtml .= '<div class="padding-zero root-col-1 clearfix">';
                                if ($left_width != 0 || $left_sub_width != 0) {
                                    $left_sub_content = $this->getBlockContent($load_cat->getData('megamenu_type_leftblock'));
                                    if ($left_sub_content != '') {
                                        $catHtml .= '<div class="'.$sub_left_content_area. ' ' . $load_cat->getName() . ' clearfix rootmegamenu_block">';
                                            $catHtml .= $left_sub_content;
                                        $catHtml .= '</div>';   
                                    } else {
                                        $catHtml .= '<div class="'.$left_content_area. ' ' . $load_cat->getName() . ' clearfix rootmegamenu_block">';
                                            $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_leftblock'));
                                        $catHtml .= '</div>';
                                    }
                                }
                                $catHtml .= '<div class="'.$category_area_width.' clearfix padding-zero'.$masonryClass.'">';
                                    $sub_cnt = 1;
                                    // 3th Level Category
									 //$catHtml .= '<div class="tabimgwpr grid-item-3 clearfix level3-cat back-button show-mobile-view"><a href=javascript:void(0) class="tabimtag"><span class="back-cat-arrow"></span><div class="tabimgtext">BACK</div></a></div>';
                                    foreach ($childrenCategories_2 as $childCategory2) {
                                        if ($main_cat->getMegamenuShowCatimage() == 1) {
                                            $brake_point = $colnum * 2;
                                        } else {
                                            $brake_point = $colnum * 6;	
                                        }
                                        if ($sub_cnt > $brake_point) { continue; }
                                        $sub_cnt++;
                                       // $load_cat_sub = $this->categoryRepository->get($childCategory2->getId(), $this->_customhelper->getStoreId());
										//echo $childCategory2->getURL();die();
                                         $sub_colnum = $load_cat->getMegamenuTypeNumofcolumns();
										if ($sub_colnum == 0) {
											$sub_colnum = 5;
										}
										
                                        $catHtml .= '<div class="tabimgwpr '.$colClass.$sub_colnum.' clearfix ">';
                                        //$catHtml .= '<div class="tabimgwpr root-col-'.$colnum.'">;
                                        //$catHtml .= '<a href='.$load_cat_sub->getURL().' class="tabimtag">';
										$catHtml .= '<a href='.$childCategory2->getURL().' class="tabimtag">';
										
                                       /* if ($main_cat->getMegamenuShowCatimage() == 1) {
                                            if ($this->_customhelper->getMegaMenuImageName($load_cat_sub) != '') {
                                                $imageurl = $this->_customhelper->getMegaMenuImageUrl($load_cat_sub);
                                            } else { 
                                                $imageurl = $this->getViewFileUrl('Rootways_Megamenu::images/rootimgicon.jpg');
                                            }
                                            $catHtml .= '<img width='.$main_cat->getMegamenuShowCatimageWidth().' height='.$main_cat->getMegamenuShowCatimageHeight().' src='.$imageurl.' alt="'.$main_cat->getName().'"/>';
                                        }
                                        $catHtml .= '<div class="tabimgtext">'.$load_cat_sub->getName().'</div><span class="cat-arrow"></span></a>';
                                        */
										 $catHtml .= '<div class="tabimgtext">'.$childCategory2->getName().'</div><span class="cat-arrow"></span></a>';
                                        if ($childrenCategories_3 = $this->getChildCategories($childCategory2)) {
                                            $catHtml .= '<ul class="tabbing_lev4">';
											//$catHtml .= '<li class=" back-button show-mobile-view"><a href=javascript:void(0) ><span class="cat-arrow"></span><span class="level4-name">BACK</span></a></li>';
                                            $subCatCnt = 0;
                                            foreach ($childrenCategories_3 as $childCategory3) {
                                                if ($subCatCnt >= $viewMoreAfter && $viewMoreAfter != '') {
                                                    $catHtml .= '<li><a class="view-more" href='.$load_cat_sub->getURL().'>'.__('View More').'</a></li>';
                                                    break;
                                                }
                                                $subCatCnt++;
                                                //$load_cat_sub_sub = $this->categoryRepository->get($childCategory3->getId(), $this->_customhelper->getStoreId());
                                                //$image_html_sub_sub = $this->getImageHtml($main_cat, $load_cat_sub_sub, 3);
                                                $catHtml .= '<li><a href='.$childCategory3->getURL().'>';
                                               // $catHtml .= $image_html_sub_sub;
                                                $catHtml .= '<span class="level4-name">'.$childCategory3->getName().'</span></a>';
													
													 if($childrenCategories_6 = $this->getSubChildCategories($childCategory3)) {
														  $catHtml .= '<i class="arrow down"></i>';
														 $catHtml .= '<ul style="padding-left:5%;display:none;">';
														try{ foreach ($childrenCategories_6 as $childCategory6) {
																$name = $childCategory6['name'];
																$catHtml .= '<li><a href='.$childCategory6['url'].'>'.$name.'</a></li>';
																//$catHtml .= '<li>Retina 13" (A1798)</li>';
														}
														$catHtml .= '</ul>';
														}catch(Exception $e){
															echo $e->getMessage();die();
														}
													}
													$catHtml .= '</li>';
												//}
												
                                            }
                                            $catHtml .= '</ul>';
                                        }
                                        $catHtml .= '</div>';
                                        if ($masonryClass != ' grid' && $sub_cnt%$colnum==0) {
                                            $catHtml .= '<div class="clearfix"></div>';
                                        }
                                    }
                                    if ( $sub_cnt > $brake_point ) {
                                        $catHtml .= '<a href='.$load_cat->getURL().' class="view_all">View All &raquo;</a>';
                                    }
                                $catHtml .= '</div>';
                                if ($right_width != 0 || $right_sub_width != 0) {
                                    $right_sub_content = $this->getBlockContent($load_cat->getData('megamenu_type_rightblock'));
                                    if ($right_sub_content != '') {
                                        $catHtml .= '<div class="'.$sub_right_content_area. ' ' . $load_cat->getName() . ' clearfix rootmegamenu_block">';
                                            $catHtml .= $right_sub_content;
                                        $catHtml .= '</div>';   
                                    } else {
                                        $catHtml .= '<div class="'.$right_content_area. ' ' . $load_cat->getName() . ' clearfix rootmegamenu_block">';
                                            $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_rightblock'));
                                        $catHtml .= '</div>';
                                    }
                                }
                            $catHtml .= '</div>';
                            if ( $load_cat->getMegamenuTypeFooter() != '' ) {
                                $catHtml .= '<div class="menufooter root-col-1 clearfix">';
                                    $catHtml .= $this->getBlockContent($load_cat->getData('megamenu_type_footer'));
                                $catHtml .= '</div>';   
                            } elseif( $main_cat->getMegamenuTypeFooter() != '' ) {
                                $catHtml .= '<div class="menufooter root-col-1 clearfix">';
                                    $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_footer'));
                                $catHtml .= '</div>';
                            } else { }
						$catHtml .= '</div>';
					 } else {
						$catHtml .= '<div class="root-col-75 verticalopen empty_category">';
						    $catHtml .= '<span>Sub-category not found for '.$load_cat->getName().' Category</span>';
						$catHtml .= '</div>';
                    }
					$catHtml .= '</li>';
				}
			$catHtml .= '</ul>';
			$catHtml .= '</div>';
			$catHtml .= '</div>';
		}
		return $catHtml;	
	}
    
    /**
     * Full-Width Horizontal Mega Menu HTML Block.
     */
    public function tabHorizontal($category, $navCnt0)
    {
        $main_cat = $this->categoryRepository->get($category->getId(), $this->_customhelper->getStoreId());
        $left_width = $main_cat->getMegamenuTypeLeftblockW();
        $right_width = $main_cat->getMegamenuTypeRightblockW();
        $cat_width = 12 - ($left_width + $right_width);
        $category_area_width = 'root-sub-col-'.$cat_width;
        $left_content_area = 'root-sub-col-'.$left_width;
        $right_content_area = 'root-sub-col-'.$right_width;
        $catHtml = '';
        if ($childrenCategories = $this->getChildCategories($category)) {
            $catHtml .= '<div class="megamenu fullmenu clearfix tabmenu02">';
                $catHtml.= '<div class="mainmenuwrap02 clearfix">';
				if(strtolower($category->getName()) == "accessories")
					$catHtml.= '<h3 class="submenu-boxes-subsection-title"><span>Featured Categories</span></h3>';
				else if(strtolower($category->getName()) == "devices")
					$catHtml.= '<h3 class="submenu-boxes-subsection-title"><span>Featured brands</span></h3>';
					
                    $catHtml .= '<ul class="vertical-menu02 root-col-1 clearfix">';
						//$catHtml .= '<li class="clearfix back-button"><a href="javascript:void(0)" style="color:#666;font-style: normal;font-size: 14px;font-weight: normal;"><span class="cat-arrow"></span>BACK</a></li>';
                        foreach ($childrenCategories as $childCategory) {
                            $load_cat = $this->categoryRepository->get($childCategory->getId(), $this->_customhelper->getStoreId());
                            $left_sub_width = $load_cat->getMegamenuTypeLeftblockW();
                            $right_sub_width = $load_cat->getMegamenuTypeRightblockW();
                            $cat_sub_width = 12 - ($left_sub_width + $right_sub_width);
                            $sub_category_area_width = 'root-sub-col-'.$cat_sub_width;
                            $sub_left_content_area = 'root-sub-col-'.$left_sub_width;
                            $sub_right_content_area = 'root-sub-col-'.$right_sub_width;
                            if ($left_sub_width != 0 || $right_sub_width != 0) {
                                $category_area_width = 'root-sub-col-'.$cat_sub_width;
                            } else {
                                $category_area_width = 'root-sub-col-'.$cat_width;
                            }
                            $catHtml .= '<li class="clearfix"><a class="clearfix" style="line-height:'.$main_cat->getMegamenuShowCatimageHeight().'px;" href='.$load_cat->getURL().'>';
                            
                            
							if ($main_cat->getMegamenuShowCatimage() == 1) {
								if ($this->_customhelper->getMegaMenuImageName($load_cat) != '') {
									$imageurl = $this->_customhelper->getMegaMenuImageUrl($load_cat);
								} else {
									$imageurl = $this->getViewFileUrl('Rootways_Megamenu::images/rootimgicon.jpg');
								}
								$catHtml .= ' <span><img style="width:'.$main_cat->getMegamenuShowCatimageWidth().'px; height:'.$main_cat->getMegamenuShowCatimageHeight().'px;" src='.$imageurl.' alt="'.$load_cat->getName().'"/></span>';	
							}
                           
							$catHtml .= '<span>'.$load_cat->getName().'</span></a>';
                            if ($childrenCategories_2 = $this->getChildCategories($childCategory)) {
                                $num_of_col = $load_cat->getMegamenuTypeNumofcolumns();
                                if ($num_of_col == 0) {
                                    $num_of_col = 3;
                                }
                                $cnt = 0;
                                $cat_tot = count($childrenCategories_2);
                                $brk = ceil($cat_tot/$num_of_col);
                                
                                if ($cnt == 0) { $open = "openactive02"; } else { $open = ""; }
                                $cnt++;
                                $catHtml .= '<div class="root-col-1 verticalopen02 '.$open.'">';
                                    if ($left_width != 0 || $left_sub_width != 0) {
                                        $left_sub_content = $this->getBlockContent($load_cat->getData('megamenu_type_leftblock'));
                                        if ($left_sub_content != '') {
                                            $catHtml .= '<div class="'.$sub_left_content_area.' clearfix rootmegamenu_block">';
                                                $catHtml .= $left_sub_content;
                                            $catHtml .= '</div>';   
                                        } else {
                                            $catHtml .= '<div class="'.$left_content_area.' clearfix rootmegamenu_block">';
                                                $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_leftblock'));
                                            $catHtml .= '</div>';
                                        }
                                    }
                                    $sub_cnt = 1;
                                    $catHtml .= '<div class="'.$category_area_width.' topmenu02-categories clearfix">';
                                        $catHtml .= '<div class="title"><a href="'.$load_cat->getURL().'">'.$load_cat->getName().'</a></div>';
                                        $catHtml .= '<div class="root-col-'.$num_of_col.' clearfix">';
                                            $catHtml .= '<ul class="ulliststy02">';
                                                $sub_cnt = 1;
                                                foreach ($childrenCategories_2 as $childCategory2) {
                                                    $load_sub_sub_cat = $this->categoryRepository->get($childCategory2->getId(), $this->_customhelper->getStoreId());
                                                    $catHtml .= '<li><a href='.$load_sub_sub_cat->getURL().'>';
                                                    $catHtml .= $this->getImageHtml($main_cat, $load_sub_sub_cat, 1);
                                                    $catHtml .= '<span class="level2-name sub-cat-name" style="height:'.$main_cat->getMegamenuShowCatimageHeight().'px;">'.$load_sub_sub_cat->getName();

                                                    if ($sub_cnt%$brk == 0) {
                                                        $catHtml .= '</ul></div> <div class="root-col-'.$num_of_col.' clearfix"><ul class="ulliststy02">';
                                                    }
                                                    $sub_cnt++;
                                                }
                                            $catHtml .= '</ul>';
                                        $catHtml .= '</div>';
                                    $catHtml .= '</div>';
                                    if ($right_width != 0 || $right_sub_width != 0) {
                                        $right_sub_content = $this->getBlockContent($load_cat->getData('megamenu_type_rightblock'));
                                        if ($right_sub_content != '') {
                                            $catHtml .= '<div class="'.$sub_right_content_area.' clearfix rootmegamenu_block">';
                                                $catHtml .= $right_sub_content;
                                            $catHtml .= '</div>';   
                                        } else {
                                            $catHtml .= '<div class="'.$right_content_area.' clearfix rootmegamenu_block">';
                                                $catHtml .= $this->getBlockContent($main_cat->getData('megamenu_type_rightblock'));
                                            $catHtml .= '</div>';
                                        }
                                    }
                                $catHtml .= '</div>';
                             } else {
                                /*$catHtml .= '<div class="root-col-1 verticalopen02">';
                                    $catHtml .= '<span>There is no sub-category for '.$load_cat->getName().' category</span>';
                                $catHtml .= '</div>';*/
                             }
                            $catHtml .= '</li>';
                        }
                    $catHtml .= '</ul>';
					$static_block_id = 'megamenu_top_brands_block';
					  $block = $this->getLayout()->createBlock('Magento\Cms\Block\Block');
					if($block &&strtolower($category->getName()) == "accessories") {
						$block->setBlockId($static_block_id);
						$catHtml .= $block->toHtml();
					}
                $catHtml .= '</div>';
            $catHtml .= '</div>';
        }
        return $catHtml;
    }
    
    
    /**
     * Act HTML Block.
     */ 
    public function act()
	{
        return true;
	}
	
    /**
     * Contact Us Mega Menu HTML Block.
     */   
	public function contactus()
	{
        $contact_form_value = $this->_customhelper->getConfig('rootmegamenu_option/general/show_contactus');
        $contact_form_col = 'root-col-1';
        
		$catHtml = '';
		$catHtml .= '<li class="contactus_menu"><a href="javascript:void(0);">'.__('Contact Us').'</a>';
			$catHtml .= '<div class="megamenu fullmenu contacthalfmenu clearfix">';
                if ($contact_form_value == 2) {
                    $catHtml .= '<div class="root-col-2 clearfix">';
                        $contact_content = $this->_customhelper->getConfig('rootmegamenu_option/general/contactus_content');
                        $catHtml .= $this->getBlockContent($contact_content);
                    $catHtml .= '</div>';
                    $contact_form_col = 'root-col-2';
                }
                $base_url = $this->_storeManager->getStore()->getBaseUrl();
					
				$catHtml .= '<div class="'.$contact_form_col.' clearfix">';
					$catHtml .= '<div class="title">'.__('Contact Us').'</div>';
					$catHtml .=	'<form id="megamenu_contact_form" name="megamenu_contact_form" class="menu_form">';
						$catHtml .= '<input id="name" name="name" type="text" autocomplete="off" placeholder="'.__('Name').'">';
						$catHtml .= '<input id="menuemail" name="menuemail" type="text" autocomplete="off" placeholder="'.__('Email').'">';
						$catHtml .= '<input type="text" title="Telephone" id="telephone" name="telephone" autocomplete="off" placeholder="'.__('Telephone').'">';
						$catHtml .= '<textarea id="comment" name="comment" placeholder="'.__('Your message...').'"></textarea>';
						$catHtml .= '<input type="text" style="display:none !important;" value="" id="hideit" name="hideit">';
						$catHtml .= '<input type="text" style="display:none !important;" value="'.$base_url.'" name="base_url" id="base_url" >';
						$catHtml .= '<input onclick="rootFunction()" type="button" value="'.__('Reset').'">';
						$catHtml .= '<input id="megamenu_submit" type="submit" value="'.__('Send').'">';
					$catHtml .= '</form>';
				$catHtml .= '</div>';
			$catHtml .= '</div>';
		$catHtml .= '</li>';
			
		return $catHtml;
	}
    
    public function _getMenuItemAttributes($item)
    {
        $menuItemClasses = $this->_getMenuItemClasses($item);
        return implode(' ', $menuItemClasses);
    }
    
    /**
     * Get Class of categories.
     */  
    protected function _getMenuItemClasses($item)
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $classes = [];
        if ($this->_customhelper->getConfig('rootmegamenu_option/general/topmenuarrow') == 1) {
            if ($item->hasChildren()) {
                $classes[] = 'has-sub-cat';
            }
        }
        
        /*
        $classes[] = 'rootlevel' . $item->getLevel();
        $classes[] = $item->getPositionClass();
        */
        
        $request = $_objectManager->get('Magento\Framework\App\Action\Context')->getRequest();
        if ($request->getFullActionName() == 'catalog_category_view') {
            $cur_cat = $_objectManager->get('Magento\Framework\Registry')->registry('current_category');
            $categoryPathIds = explode(',', $cur_cat->getPathInStore());
            if (in_array($item->getId(), $categoryPathIds) == '1') {
                $classes[] = 'active';   
            }
        }
        return $classes;
    }
    
    /**
     * Get Custom Links
     */  
    public function getCustomLinks($category_id)
    {
        $base_url = rtrim($this->_storeManager->getStore()->getBaseUrl(),'/');
        $customMenus = $this->_customhelper->getConfig('rootmegamenu_option/general/custom_link');
        $customLinkHtml = '';
        if ( $customMenus ) {
            if ($this->_customhelper->getMagentoVersion() >= '2.2.0') {
                $customMenus = json_decode($customMenus, true);
            } else {
                $customMenus =  \Magento\Framework\Serialize\SerializerInterface::unserialize($customMenus);
            }
            if ( is_array($customMenus) ) {
                foreach ( $customMenus as $customMenusRow ) {
                    if ($customMenusRow['custommenulink'] != '') {
                        if (substr($customMenusRow['custommenulink'], 0, 1) != '/') {
                            $no_custom_link = $customMenusRow['custommenulink'];
                        } else {
                            $no_custom_link = $base_url.$customMenusRow['custommenulink'];
                        }
                    } else {
                        $no_custom_link = 'javascript:void(0);';
                    }
                    if ( isset($customMenusRow['custom_menu_position'])) {
                        if ( $customMenusRow['custom_menu_position'] == $category_id  && $customMenusRow['custom_menu_position'] != '' ) {
                            $customLinkHtml .= '<li class="custom-menus"><a href="'.$no_custom_link.'">'.$customMenusRow['custommenuname'].'</a>';
                            if ($customMenusRow['custom_menu_block'] != '') {
                                $customLinkHtml .= $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($customMenusRow['custom_menu_block'])->toHtml();
                            }
                            $customLinkHtml .= '</li>';
                        }

                        if ( $category_id == false && $customMenusRow['custom_menu_position'] == '') {
                            $customLinkHtml .= '<li class="custom-menus"><a href="'.$no_custom_link.'">'.$customMenusRow['custommenuname'].'</a>';
                            if ($customMenusRow['custom_menu_block'] != '') {
                                $customLinkHtml .= $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($customMenusRow['custom_menu_block'])->toHtml();
                            }
                            $customLinkHtml .= '</li>';
                        }
                    }
                    
                }
            }
        }
        return $customLinkHtml;
    }
    
    protected function masonryCategoryClass($cId) {
        $enableMasonry = $this->_customhelper->manageMasonry();
        $masonryClass = '';
        $colClass = 'root-col-';
        if ($enableMasonry == 1) {
            $masonryClass = ' grid';
            $colClass = 'grid-item-';
        } else if ($enableMasonry == 2) {
            $masonryCategories = $this->_customhelper->masonryCategory();
            if (in_array($cId, $masonryCategories)) {
                $masonryClass = ' grid';
                $colClass = 'grid-item-';
            }
        } else {
            $masonryClass = '';
            $colClass = 'root-col-';
        }
        $msClasses = array($masonryClass, $colClass);
        return $msClasses;
    }
    public function viewAllCategoriesHTML()
    {
    }
	public function getSubChildCategories($category)
    {
        $children = [];
        if ($this->categoryFlatConfig->isFlatEnabled() && $category->getUseFlatResource()) {
            //$subcategories = (array)$category->getChildrenNodes();
			$subcategories = $category->getChildrenCategories();
        } else {
            $subcategories = $category->getChildrenCategories();
        }
        foreach ($subcategories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }
            $children[] = ['name'=>$category->getName(),'url'=>$category->getUrl()];
        }
        return $children;
    }
	public function getCmsBlockHtml($id){
		$block = $this->blockRepository->getById($id);
		//var_dump($block->getContent());die("ddd");
		return $block->getContent();
	}
   

}
