<?php

namespace Nexuscellular\Customization\Block\Catalog\Product;

class Toolbar extends \Magento\Catalog\Block\Product\ProductList\Toolbar
{

    /**
     * Set collection to pager
     *
     * @param \Magento\Framework\Data\Collection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }
        if ($this->getCurrentOrder()) {
            if (($this->getCurrentOrder()) == 'position') {
                $this->_collection->addAttributeToSort(
                    $this->getCurrentOrder(),
                    $this->getCurrentDirection()
                );
            }elseif($this->getCurrentOrder() == 'high_to_low')
            {
                $this->_collection->setOrder('price', 'DESC');
            } 
            elseif ($this->getCurrentOrder() == 'low_to_high')
            {
                $this->_collection->setOrder('price', 'ASC');
            }elseif($this->getCurrentOrder() == 'custitem_f3_new_arrival'){
				  /* $this->_collection->addAttributeToSort(
                    'name',
                    $this->getCurrentDirection()
                );*/
				$this->_collection->setOrder('custitem_f3_new_arrival', $this->getCurrentDirection());
				 //$this->_collection->addAttributeToFilter('custitem_f3_new_arrival','1');
			 //$this->_collection->setOrder('created_at', $this->getCurrentDirection());
				//echo $this->_collection->getSize();die();
			}else {
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
            }
        }
        return $this;
    }
}
