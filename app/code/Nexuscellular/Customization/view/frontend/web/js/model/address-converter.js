define([
    'jquery',
    'Nexuscellular_Customization/js/model/new-customer-address',
    'mage/utils/objects',
    'underscore',
    'Magento_Customer/js/customer-data'
],function ($, address, mageUtils, _, customerData) {
    'use strict';
   
    var countryData = customerData.get('directory-data');

    return function (target) {
        target.formAddressDataToQuoteAddress = function (formData) {
            // clone address form data to new object
            var addressData = $.extend(true, {}, formData),
                region,
                regionName = addressData.region;

            if (mageUtils.isObject(addressData.street)) {
                addressData.street = this.objectToArray(addressData.street);
            }

            addressData.region = {
                'region_id': addressData['region_id'],
                'region_code': addressData['region_code'],
                region: regionName
            };

            if (addressData['region_id'] &&
                countryData()[addressData['country_id']] &&
                countryData()[addressData['country_id']].regions
            ) {
                region = countryData()[addressData['country_id']].regions[addressData['region_id']];

                if (region) {
                    addressData.region['region_id'] = addressData['region_id'];
                    addressData.region['region_code'] = region.code;
                    addressData.region.region = region.name;
                }
            } else if (
                !addressData['region_id'] &&
                countryData()[addressData['country_id']] &&
                countryData()[addressData['country_id']].regions
            ) {
                addressData.region['region_code'] = '';
                addressData.region.region = '';
            }
            delete addressData['region_id'];

            if (addressData['custom_attributes']) {
                addressData['custom_attributes'] = _.map(
                    addressData['custom_attributes'],
                    function (value, key) {
                        return {
                            'attribute_code': key,
                            'value': value
                        };
                    }
                );
            }

            return address(addressData);
        };

        return target;
    };
});