 define([
    'underscore',
    'ko',
    'mageUtils',
    'uiComponent',
    'uiLayout',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/model/shipping-rate-service',
    'Magento_Customer/js/customer-data',
    'Magento_Customer/js/model/customer/address'
], function (_, ko, utils, Component, layout, addressList, selectShippingAddressAction, checkoutData, quote, rateRegistry, shippingRateService, customerData, Address) {
    'use strict';

    var defaultRendererTemplate = {
        parent: '${ $.$data.parentName }',
        name: '${ $.$data.name }',
        component: 'Magento_Checkout/js/view/shipping-address/address-renderer/default',
        provider: 'checkoutProvider'
    };

    var countryData = customerData.get('directory-data');

    return Component.extend({

        defaults: {
            template: 'Nexuscellular_Customization/shipping-address/list',
            visible: addressList().length > 0,
            rendererTemplates: []

        },

        /** @inheritdoc */
        initialize: function () {
            this._super()
                .initChildren();
            

            quote.shippingAddress.subscribe(function (address) {
                this.selectedAddress(address);
            }, this);

            addressList.subscribe(function (changes) {
                    var self = this;

                    changes.forEach(function (change) {
                        if (change.status === 'added') {
                            self.createRendererComponent(change.value, change.index);
                        }
                    });
                },
                this,
                'arrayChange'
            );
            
            return this;
        },

        addressOptions: ko.computed(function () {
            return addressList();
        }),

        /** @inheritdoc */
        initConfig: function () {
            this._super();
            // the list of child components that are responsible for address rendering
            this.rendererComponents = [];

            return this;
        },

        initObservable: function () {
            this._super()
                .observe({
                    selectedAddress: null
                });
            
            return this;
        },

        /** @inheritdoc */
        initChildren: function () {
            _.each(addressList(), this.createRendererComponent, this);

            return this;
        },

        /**
         * Create new component that will render given address in the address list
         *
         * @param {Object} address
         * @param {*} index
         */
        createRendererComponent: function (address, index) {
            var rendererTemplate, templateData, rendererComponent;

            if (index in this.rendererComponents) {
                this.rendererComponents[index].address(address);
            } else {
                // rendererTemplates are provided via layout
                rendererTemplate = address.getType() != undefined && this.rendererTemplates[address.getType()] != undefined ? //eslint-disable-line
                    utils.extend({}, defaultRendererTemplate, this.rendererTemplates[address.getType()]) :
                    defaultRendererTemplate;
                templateData = {
                    parentName: this.name,
                    name: index
                };
                rendererComponent = utils.template(rendererTemplate, templateData);
                utils.extend(rendererComponent, {
                    address: ko.observable(address)
                });
                layout([rendererComponent]);
                this.rendererComponents[index] = rendererComponent;
            }
        },

        /**
         * @param {Object} address
         * @return {*}
         */
         addressOptionsText: function (address) {
            return address.getAddressInline();
        },

        onAddressChange: function (address) {
            selectShippingAddressAction(address);
            checkoutData.setSelectedShippingAddress(address.getKey());

            if (quote.shippingAddress().getType == 'customer-address') {
                rateRegistry.set(quote.shippingAddress().getKey(), null);
            } else {
                rateRegistry.set(quote.shippingAddress().getCacheKey(), null);
            }

            shippingRateService.isAddressChange = true;
            shippingRateService.estimateShippingMethod();
        },

        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        }
    });
});
