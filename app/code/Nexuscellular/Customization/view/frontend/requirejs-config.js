var config = {
    map: {
        '*': {
            'Mageplaza_Osc/js/view/shipping':
                'Nexuscellular_Customization/js/view/shipping'
        }
    },

    config: {
        mixins: {
            'Magento_Checkout/js/model/address-converter': {
                'Nexuscellular_Customization/js/model/address-converter': true
            }
        }
    }
};