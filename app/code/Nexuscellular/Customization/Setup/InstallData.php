<?php
namespace Nexuscellular\Customization\Setup;

use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface; 

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(
    EavSetupFactory $eavSetupFactory,
    Config $eavConfig
    ) 
    {
    $this->eavSetupFactory = $eavSetupFactory;
    $this->eavConfig = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) 
    {//die('install data');
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_cuurency', [
            'label' => 'Currency',
            'system' => 0,
            'position' => 80,
            'sort_order' => 80,
            'required'  => true,
            'visible' => true,
            'note' => '',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Nexuscellular\Customization\Model\Source\Currency'
            ]
            );
           
            $this->getEavConfig()->getAttribute('customer', 'customer_cuurency')
            ->setData('is_user_defined', 1)->setData('is_required', 1)->setData('default_value', '')
            ->setData('used_in_forms', ['adminhtml_customer', 'checkout_register', 'customer_account_create', 'customer_account_edit'])->save();
    }
    public function getEavConfig() {
        return $this->eavConfig;
        }
}
 ?>
