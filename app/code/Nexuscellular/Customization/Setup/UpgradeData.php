<?php

namespace Nexuscellular\Customization\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Config;
class UpgradeData implements UpgradeDataInterface
{
   private $eavSetupFactory;

	 public function __construct( EavSetupFactory $eavSetupFactory,
    Config $eavConfig){
      $this->eavSetupFactory = $eavSetupFactory;
      $this->eavConfig = $eavConfig;
    }
	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context){
		if (version_compare($context->getVersion(), '1.1.1', '<')) {
         $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
         $eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'business_type', [
             'label' => 'Business Type',
             'system' => 0,
             'position' => 90,
             'sort_order' => 90,
             'required'  => true,
             'visible' => true,
             'note' => '',
             'type' => 'varchar',
             'input' => 'select',
             'source' => 'Nexuscellular\Customization\Model\Source\Businesstype'
             ]
             );
            
             $this->getEavConfig()->getAttribute('customer', 'business_type')
             ->setData('is_user_defined', 1)->setData('is_required', 1)->setData('default_value', '')
             ->setData('used_in_forms', ['adminhtml_customer', 'checkout_register', 'customer_account_create', 'customer_account_edit'])->save();
		}
	}
   public function getEavConfig() {
      return $this->eavConfig;
      }
}