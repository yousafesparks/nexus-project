<?php

namespace Nexuscellular\Customization\Observer;


use Magento\Framework\Controller\ResultFactory; 

class Defaultcurrency implements \Magento\Framework\Event\ObserverInterface
{

    protected $_storeManager;

    protected $_curl;

    protected $_session;

    protected $_objectManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Session\SessionManagerInterface $session,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_storeManager   = $storeManager;
        $this->_curl           = $curl;
        $this->_session        = $session;
        $this->_objectManager  = $objectManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            $currency = $customerSession->getCustomer()->getData('customer_cuurency');
            $currencyCurrencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
         if($currency != $currencyCurrencyCode){
                $this->_storeManager->getStore()->setCurrentCurrencyCode($currency);
            }
        }
    }
}
