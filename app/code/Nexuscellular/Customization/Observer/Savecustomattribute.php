<?php
namespace Nexuscellular\Customization\Observer;
use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Model\Stock\Item;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ObserverInterface;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\CatalogInventory\Model\StockItemValidator;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\Product\Action as ProductAction;

class Savecustomattribute implements ObserverInterface
{
    /**
     * @var stockItemRepository
     */
   protected $stockItemRepository;
	 protected $productAction;
    /**
     * @var StockItemValidator
     */
    private $stockItemValidator;

    /**
     * @var ParentItemProcessorInterface[]
     */
    private $parentItemProcessorPool;
	 protected $configurable;
    protected $product;

    /**
     * @param StockConfigurationInterface $stockConfiguration
     * @param StockRegistryInterface $stockRegistry
     * @param StockItemValidator $stockItemValidator
     * @param ParentItemProcessorInterface[] $parentItemProcessorPool
     */
    public function __construct(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
		\Magento\Catalog\Model\Product $productModel,
		StockItemRepository $stockItemRepository,
		 ProductAction $action,
        array $parentItemProcessorPool = []
    ) {
        $this->configurable = $configurable;
        $this->parentItemProcessorPool = $parentItemProcessorPool;
		$this->stockItemRepository = $stockItemRepository;
		$this->product= $productModel;
		 $this->productAction = $action;
    }

    /**
     * Saving product inventory data
     *
     * Takes data from the stock_data property of a product and sets it to Stock Item.
     * Validates and saves Stock Item object.
     *
     * @param EventObserver $observer
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(EventObserver $observer)
    {
		 /** @var Product $product */
        $stockItem = $observer->getEvent()->getItem();
		//print_r($stockItem->getExtensionAttributes());
		$productId = $stockItem->getData('product_id');
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/stock.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info("<==============Call product  ID =========> ".$productId);
		
		$parentIds = $this->configurable->getParentIdsByChild($productId);
		//echo "<pre>";print_r($parentIds);die();
		 if(count($parentIds) > 0){
			 $parentId = $parentIds[0];
			 $parentObj = $this->getProductLoadById($parentId);
			 
			 if(!$parentObj->getShowNotify()){
				 $productStock = $this->stockItemRepository->get($parentObj->getId());
				 if(!$productStock->getData('is_in_stock')){ // once product is become out of stock then product will deactivate at frontend
				 $ids[] = $parentObj->getId();
					 $this->productAction->updateAttributes($ids, array('remove_item' => 1),0);
					 $this->productAction->updateAttributes($ids, array('remove_item' => 1),1);
					 
					$this->productAction->updateAttributes([$productId], array('remove_item' => 1),0);
					$this->productAction->updateAttributes([$productId], array('remove_item' => 1),1);
				 }else{// once product is back in stock then product will show at frontend
						$ids = [];
						$ids[] = $parentObj->getId();
						$this->productAction->updateAttributes([$parentObj->getId()], array('remove_item' => 0),0);
						$this->productAction->updateAttributes([$parentObj->getId()], array('remove_item' => 0),1);
						
						$this->productAction->updateAttributes([$productId], array('remove_item' => 0),0);
						$this->productAction->updateAttributes([$productId], array('remove_item' => 0),1);
					  unset($productLoad);
				 }
			 }else{//die('else deactivate');
				 $this->deactivateRemoveOutOfstock($parentObj,$productId);
			 }
		 }else{ // if product is standard(no parent) or parent product itself
			 $productLoad = $this->getProductLoadById($productId);
			
				if(!$productLoad->getShowNotify()){
					 $productStock = $this->stockItemRepository->get($productLoad->getId());
					 if(!$productStock->getData('is_in_stock')){
						$this->productAction->updateAttributes([$productLoad->getId()], array('remove_item' => 1),0);
						$this->productAction->updateAttributes([$productLoad->getId()], array('remove_item' => 1),1);
					 }else{
						$this->productAction->updateAttributes([$productLoad->getId()], array('remove_item' => 0),0);
						$this->productAction->updateAttributes([$productLoad->getId()], array('remove_item' => 0),1);
					 }
				}else{
					$this->productAction->updateAttributes([$productLoad->getId()], array('remove_item' => 0),0);
					$this->productAction->updateAttributes([$productLoad->getId()], array('remove_item' => 0),1);
				}
				unset($productLoad);
		 }
		
    }
	public function getProductLoadById($parentId)
	{
		return $configProduct = $this->product->load($parentId);
	}
	public function getChildProducts($parentObj)
	{
		return $parentObj->getTypeInstance()->getUsedProducts($parentObj);
	}
	public function deactivateRemoveOutOfstock($parentObj,$productId){
		$this->productAction->updateAttributes([$parentObj->getId()], array('remove_item' => 0),0);
		$this->productAction->updateAttributes([$parentObj->getId()], array('remove_item' => 0),1);
		
		$this->productAction->updateAttributes([$productId], array('remove_item' => 0),0);
		$this->productAction->updateAttributes([$productId], array('remove_item' => 0),1);
	}
}