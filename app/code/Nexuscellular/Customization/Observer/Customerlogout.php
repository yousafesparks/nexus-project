<?php

namespace Nexuscellular\Customization\Observer;


use Magento\Framework\Controller\ResultFactory; 

class Customerlogout implements \Magento\Framework\Event\ObserverInterface
{

    protected $_storeManager;

    protected $_curl;

    protected $_session;

    protected $_objectManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Session\SessionManagerInterface $session,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_storeManager   = $storeManager;
        $this->_curl           = $curl;
        $this->_session        = $session;
        $this->_objectManager  = $objectManager;
        $this->response = $response;
        $this->customerSession = $customerSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    }
}
