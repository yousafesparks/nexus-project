<?php 
namespace Nexuscellular\Customization\Controller\Account;

use Magento\Customer\Model\Registration;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
class DeleteCartItems extends \Magento\Framework\App\Action\Action
{
      /**
       * @var PageFactory
       */
      protected $resultPageFactory;
  
      protected $_messageManager;
       /**
     * @var Magento\Checkout\Model\Cart
     */
    protected $cart;
  
    /**
     * @param Context $context
     * @param ManagerInterface $_messageManager
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->cart = $cart;
        parent::__construct($context);
        $this->_messageManager = $messageManager;
    }
    public function execute(){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $allItems = $checkoutSession->getQuote()->getAllVisibleItems();
        $cart =  $objectManager->get('Magento\Checkout\Model\Cart');
        $message = '';
        try{
            $this->cart->truncate()->save();
        }catch(Exception $e){
            $message = 'Something went wrong try again.';
        }
        if(!empty($message))
            $this->_messageManager->addErrorMessage($message);

         /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
         $resultRedirect = $this->resultRedirectFactory->create();
         $resultRedirect->setPath($this->_redirect->getRedirectUrl());
         return $resultRedirect;
    }
}
?>