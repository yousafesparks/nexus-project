<?php

namespace Nexuscellular\Customization\Helper;

use Magento\Framework\App\PageCache\Version;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Nexuscellular\Netsuite\Helper\Attribute as attributeCellularHelper;
use Nexuscellular\Netsuite\Helper\Facets as FacetsCellularHelper;
use Nexuscellular\Netsuite\Helper\Apicall as apiCAllCellularHelper;
use Nexuscellular\Netsuite\Helper\Netsuiteproductstatus as netsuiteproductstatusHelper;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $optionFactory;
	protected $_attributeOptionCollection;
	protected $urlRewriteCollection;
	protected $data;
	protected $_attributeFactory;
	protected $productFactory;
	protected $attrhelper;
	protected $apiHelper; 
	protected $productInterfaceFactory;
	protected $_storeManager;
	protected $_productCollectionFactory;
	protected $directoryList;
    protected $file;
	protected $_netsuiteFactory;
	protected $_productRepository;
	protected $_facetHelper;
	protected $_netsuiteproductstatusHelper;
	protected $_netsuiteProductFactory;
	protected $productRepositoryInterface;
	protected $layerResolver;
	protected $_haslayerResolver;
	 public function __construct(
		\Magento\Catalog\Model\ProductFactory $productFactory,
		\Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager, 
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, 
		\Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteCollection,
		attributeCellularHelper $attrHelper,
		TypeListInterface $cacheTypeList, 
		\Nexuscellular\Netsuite\Model\NetsuiterecordFactory $netsuiteFactory,
		\Nexuscellular\Netsuite\Model\ProductinsertedFactory $netsuiteProductFactory,
		DirectoryList $directoryList,
		apiCAllCellularHelper $apiHelper,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		FacetsCellularHelper $facetHelper,
		netsuiteproductstatusHelper $netsuiteproductstatusHelper,
		\Magento\Catalog\Api\Data\ProductInterfaceFactory $productInterfaceFactory,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
		\Magento\Catalog\Model\Layer\Resolver $layerResolver,
		UrlInterface $urlBuilder,
		File $file
    )
    {
		$this->productFactory = $productFactory;
		$this->_attributeFactory = $attributeFactory;
		$this->_storeManager = $storeManager;
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->_productRepository = $productRepository;
		$this->attrhelper = $attrHelper;
		$this->_netsuiteFactory = $netsuiteFactory;
		$this->_netsuiteProductFactory = $netsuiteProductFactory;
		$this->productInterfaceFactory = $productInterfaceFactory;
		$this->directoryList = $directoryList;
		$this->apiHelper = $apiHelper;
        $this->file = $file;
		 $this->productRepositoryInterface = $productRepositoryInterface;
		$this->_facetHelper = $facetHelper;
		 $this->urlRewriteCollection 		= $urlRewriteCollection;
		$this->_netsuiteproductstatusHelper = $netsuiteproductstatusHelper;
		$this->layerResolver = $layerResolver;
		$this->_haslayerResolver = null;
		$this->urlBuilder = $urlBuilder;
	}
	public function setsetOrderCurrencyFormat($value,$netsuite_order_currency="Canadian Dollar"){
		$value = number_format($value, 2);
			if($netsuite_order_currency == "Canadian Dollar")
				$value = 'CA$'.$value;
			else if($netsuite_order_currency == "US Dollar")
				$value = 'USD$'.$value;

			return $value;	
		}
	public function getLayeredFilter(){
		if(is_null($this->_haslayerResolver)){
			$this->_haslayerResolver =  $this->layerResolver->get();
		}
		return $this->_haslayerResolver;
	}
	 /**
     * Retrieve active filters
     *
     * @return array
     */
    public function getActiveFilters()
    {
        $filters = $this->getLayeredFilter()->getState()->getFilters();
        if (!is_array($filters)) {
            $filters = [];
        }
        return $filters;
    }

    /**
     * Retrieve Clear Filters URL
     *
     * @return string
     */
    public function getClearUrl()
    {
        $filterState = [];
        foreach ($this->getActiveFilters() as $item) {
            $filterState[$item->getFilter()->getRequestVar()] = $item->getFilter()->getCleanValue();
        }
        $params['_current'] = true;
        $params['_use_rewrite'] = true;
        $params['_query'] = $filterState;
        $params['_escape'] = true;
        return $this->urlBuilder->getUrl('*/*/*', $params);
    }
}
