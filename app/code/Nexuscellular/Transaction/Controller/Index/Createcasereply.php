<?php
namespace Nexuscellular\Transaction\Controller\Index;

use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;

class Createcasereply extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context, 
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      return parent::__construct($context);
    }

    public function execute()
    {
        if(!$this->apiHelper->getCustomerId()){
          $login_url = $this->urlInterface
        ->getUrl('customer/account/login');
        return $this->resultRedirectFactory->create()->setPath($login_url);
        }
        if (!$this->getRequest()->isPost()) {
          $this->messageManager->addError('Please fill up required field.');
          return $this->resultRedirectFactory->create()->setPath('*/*/viewcase',['id' => $data['caseid']]);
            }
          $data = $this->getRequest()->getPost();
        $service = new \NetSuiteService();
        $Message = new \Message();
        $Message->author = new  \RecordRef();
        $Message->author->internalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
        $Message->message = $data['incomingMessage'];
       // $Message->recipientEmail = "soni.manthan0@gmail.com";
       // $Message->cc = "soni.manthan0@gmail.com";
        //$Message->bcc = "soni.manthan0@gmail.com";
        $Message->subject = $data['subject'];
        $Message->activity = new \RecordRef();
        $Message->activity->internalId = $data['caseid'];
        $request = new \AddRequest();
        $request->record = $Message;
        $addResponse = $service->add($request);
        if (!$addResponse->writeResponse->status->isSuccess) {
          $this->messageManager->addError('something went worng please try again later!');
          return $this->resultRedirectFactory->create()->setPath('*/*/viewcase',['id' => $data['caseid']]);
        } else {
            // Display the succes form validation message
        $this->messageManager->addSuccess('Your message was sent. A support representative should contact you briefly.');
        return $this->resultRedirectFactory->create()->setPath('*/*/viewcase',['id' => $data['caseid']]);
        }
    }
  }
  

