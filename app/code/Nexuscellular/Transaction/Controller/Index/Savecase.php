<?php
namespace Nexuscellular\Transaction\Controller\Index;

use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
require_once(BP.'/var/netsuitelibrary/PHPToolkit_2020_1/samples/add_ra.php');
class Savecase extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context, 
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      return parent::__construct($context);
    }

    public function execute()
    {
      if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }
      if (!$this->getRequest()->isPost()) {
        $login_url = $this->urlInterface
      ->getUrl('transaction/index/createcase');
      return $this->resultRedirectFactory->create()->setPath($login_url);
          }
        $data = $this->getRequest()->getPost();
      $service = new \NetSuiteService();


      $supportCase = new \supportCase();
      $supportCase->title = $data['title'];
      $supportCase->company = new \RecordRef();
      $supportCase->company->internalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
      $supportCase->category = new \RecordRef();
      $supportCase->category->internalId = $data['category'];
      $supportCase->incomingMessage=$data['incomingMessage'];
      if(isset($data['email']) && !empty($data['email'])){
        $supportCase->email = $data['email'];
      }
      $request = new \AddRequest();
      $request->record = $supportCase;

      $addResponse = $service->add($request);
      if (!$addResponse->writeResponse->status->isSuccess) {
        $this->messageManager->addError('something went worng please try again later!');
        return $this->resultRedirectFactory->create()->setPath($this->urlInterface->getUrl('*/*/createcase'));
      } else {
          // Display the succes form validation message
      $this->messageManager->addSuccess('Your support case was submitted. we will contact you briefly.');
       return $this->resultRedirectFactory->create()->setPath($this->urlInterface->getUrl('transaction/index/listcases'));
      }
    
    }
  }
  

