<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Transaction\Controller\Index;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\Product;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Reorder extends \Magento\Framework\App\Action\Action
{
	protected $formKey;   
	protected $cart;
	protected $product;
	protected $apiHelper;
    protected $urlInterface;
	protected $productRepository; 
	protected $_attributeLoading;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
		\Magento\Framework\App\Action\Context $context,
		 \Magento\Framework\UrlInterface $url,
		FormKey $formKey,
		Cart $cart,
		Product $product,
		apiHelper $apiHelper,
		 \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		  \Magento\Catalog\Model\ResourceModel\ProductFactory   $attributeLoading
      )
    {
      
		$this->urlInterface = $url;
		$this->formKey = $formKey;
		$this->cart = $cart;
		$this->product = $product;
		$this->apiHelper = $apiHelper;
		$this->productRepository = $productRepository;
		$this->_attributeLoading = $attributeLoading;
      return parent::__construct($context);
    }

    public function execute()
    {
      if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }
	  $error = 0;
	  try{
		   if($post = $this->getRequest()->getPostValue()){
			  
			$qty 	= 	$post['qty'];
			$sku 	= 	$post['reorder_sku'];
			$params = array(
				'form_key' => $this->formKey->getFormKey(),
				'qty'   =>	$qty
			);
			if(stripos($sku,':') !== false){ // parent product
				$Parentsku = $this->_getParentSku($sku);
				$product = $this->productRepository->get($Parentsku);
				$params['product'] = $product->getId();
				//if(!empty($post['optionName'])){
					$params['super_attribute'] = [93 =>$this->getAttributeOptionId('color',$post['optionName'])];
				
			}else{ // simple product
				$product = $this->productRepository->get($sku);
				$params['product'] = $product->getId();
			}		
		
        $this->cart->addProduct($product, $params);
        $this->cart->save();
		$this->messageManager->addSuccess( __("You added SKU %1 to your shopping cart.",$this->getSku($sku)));
		}
		}catch (\Exception $e) {
            $this->messageManager->addException($e,$e->getMessage());  
        }
		/* if($error){
			$this->messageManager->addErrorMessage( __('Product is not a valid or missing option.'));
			return $this->resultRedirectFactory->create()->setPath('transaction/index/*');
		}else{ */
			return $this->resultRedirectFactory->create()->setPath('checkout/cart');	
		//}
		 
    }
	protected function _getParentSku($sku){
		$sku = explode(':',$sku);
		if(count($sku) > 1)
		$sku = $sku[0];
		return trim($sku);
	}
	public function getSku($sku){
		$sku = explode(':',$sku);
		if(count($sku) > 1)
		$sku = $sku[count($sku) - 1];
		else
		$sku = $sku[0];
		return trim($sku);
	}
	protected function getAttributeOptionId($attribute,$label)
    {
        $poductReource=$this->_attributeLoading->create();
        $attr = $poductReource->getAttribute($attribute);
         if ($attr->usesSource()) {
                return  $option_id = $attr->getSource()->getOptionId($label);
         }
    }
  }
  

