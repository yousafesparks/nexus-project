<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Transaction\Controller\Index;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Pagination extends \Magento\Framework\App\Action\Action
{
  CONST PAGE_SIZE = 7;
  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      return parent::__construct($context);
    }

    public function execute()
    {
      /*if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }*/
     

      $customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
      $fromDate = $this->getRequest()->getParam('fromdate') ? $this->getRequest()->getParam('fromdate') : '2017-01-01';
      $toDate = $this->getRequest()->getParam('todate') ? $this->getRequest()->getParam('todate') : date('Y-m-d') ;
      $dir_order = $this->getRequest()->getParam('dir_order') ? $this->getRequest()->getParam('dir_order') : 'desc' ;
      $recordTypes = $this->getRequest()->getParam('recordtypes');
      $paramString = "?".$this->getRequest()->getParam('paramString');
      
     $query = "{\n\t\"q\": \"SELECT SUM(abs(TransactionLine.quantity)) as total_qty,transaction.id,tranid,trandate,foreigntotal,status,recordtype,type,transaction.trandisplayname FROM Transaction INNER JOIN TransactionLine ON ( TransactionLine.Transaction = Transaction.ID ) WHERE ( Transaction.recordtype IN (".$recordTypes.") ) AND ( Transaction.TranDate BETWEEN TO_DATE('".$fromDate."', 'YYYY-MM-DD' ) AND TO_DATE('".$toDate."', 'YYYY-MM-DD' ) )  AND ( Transaction.Voided = 'F' ) AND(TransactionLine.mainline = 'F') AND (TransactionLine.taxline='F') AND (Transaction.entity = '".$customerNetsuiteInternalId."')  GROUP BY Transaction.tranid,Transaction.trandate,Transaction.foreigntotal,Transaction.status,Transaction.recordtype,Transaction.type,transaction.id,transaction.trandisplayname ORDER BY transaction.trandate ".$dir_order." \"\n}";
      $response = $this->restSuiteqlApi->callRestApi($query,$paramString);
      $block = $this->_view->getLayout()->createBlock('Nexuscellular\Transaction\Block\Ajaxreturnlist')->setTemplate('Nexuscellular_Transaction::ajaxreturnlist.phtml')->setResponse($response)->toHtml();
      $this->getResponse()->setBody($block);
    }
  }