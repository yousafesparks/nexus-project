<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Transaction\Controller\Index;
use Nexuscellular\ReturnAuthorization\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
use Nexuscellular\Transaction\Helper\Pricing as pricingHelper;
use Nexuscellular\Transaction\Helper\SoapApiCall as soapApiCall;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Sales\Model\RtlTextHandler;
use Magento\Framework\App\ObjectManager;
class Invoicepdf extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory; 
    protected $restSuiteqlApi; 
    protected $customerSession;
    protected $returnRulesFactory;
    /**
     * @var \Magento\Framework\Stdlib\StringUtils
     */
    protected $string;
	 /**
     * @var \Nexuscellular\Transaction\Helper\Pricing
     */
	protected $pricingHelper;
	 /**
     * @var \Nexuscellular\Transaction\Helper\Data
     */
    protected $apiHelper;
	/**
     * @var \Nexuscellular\Transaction\Helper\SoapApiCall
     */
	protected $soapApiCall;
	 
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
    protected $_storeManager;
    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadInterface
     */
    protected $_rootDirectory;
     /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var RtlTextHandler
     */
    private $rtlTextHandler;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Nexuscellular\ReturnAuthorization\Model\ResourceModel\Returnrules\CollectionFactory $returnRulesFactory, 
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
	  pricingHelper $pricingHelper,
	  soapApiCall $soapApiCall,
      restSuiteqlApi $restapiHelper,
      \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Framework\Filesystem $filesystem,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Framework\Stdlib\StringUtils $string,
      ?RtlTextHandler $rtlTextHandler = null
      )
    {
      
      $this->returnRulesFactory =$returnRulesFactory;
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
	  $this->pricingHelper = $pricingHelper;
	  $this->soapApiCall = $soapApiCall;
      $this->fileFactory = $fileFactory;
      $this->_storeManager = $storeManager;
      $this->_rootDirectory = $filesystem->getDirectoryRead(DirectoryList::ROOT);
      $this->_scopeConfig = $scopeConfig;
      $this->string = $string;
      $this->rtlTextHandler = $rtlTextHandler  ?: ObjectManager::getInstance()->get(RtlTextHandler::class);
      return parent::__construct($context);
    }
   
	protected function drawLogo(&$page){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
        $mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
         $image =  \Zend_Pdf_Image::imageWithPath($mediaPath.'nexuscellular/logo.png'); 
        $top = 70;
        //top border of the page
        $widthLimit = 150;
        //half of the page width
        $heightLimit = 150;

        $width = $image->getPixelWidth();
        $height = $image->getPixelHeight();

        //preserving aspect ratio (proportions)
        $ratio = $width / $height;
        if ($ratio > 1 && $width > $widthLimit) {
            $width = $widthLimit;
            $height = $width / $ratio;
        } elseif ($ratio < 1 && $height > $heightLimit) {
            $height = $heightLimit;
            $width = $height * $ratio;
        } elseif ($ratio == 1 && $height > $heightLimit) {
            $height = $heightLimit;
            $width = $widthLimit;
        }

        $y1 = $this->y - $height;
        $y2 = $this->y;
        $x1 = 20;
        $x2 = $x1 + $width;
        //coordinates after transformation are rounded by Zend
        $page->drawImage($image, $x1, $y1, $x2, $y2);
		return $height;
	}
    /**
     * to generate pdf
     *
     * @return void
     */
    public function execute()
    {
      if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }
        $netsuiteReturnInternalId = $this->getRequest()->getParam('id');
		$transaction = ['internal_id'=>$this->getRequest()->getParam('id'),'recordtype'=>'invoice'];
        $returnDetail =   $this->soapApiCall->getTransactionById($transaction);
		//echo "<pre>";print_r($returnDetail);die();
        $pageNumber = 0;
        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
        $page = $pdf->pages[$pageNumber]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        /*$font = \Zend_Pdf_Font::fontWithPath(
          $this->_rootDirectory->getAbsolutePath('lib/internal/ArialFont/ARIAL.TTF')
      );*/

        $fontBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font,15);
        $page->setStyle($style);
        $width = $page->getWidth();
        $pageWidth = $page->getWidth();
        $pageHeight = $page->getHeight();
        $hight = $page->getHeight();
        $x = 20;
        $pageTopalign = 850; //default PDF page height
        $this->y = 850 - 30; //print table row from page top – 100px
        //Draw table header row’s
        // Print Logo
		$height = $this->drawLogo($page);
        
        $style->setFont($font,15);
        $page->setStyle($style);
        $page->drawText(__("Sales Order# ".$returnDetail['tranId']), $x + 385, $this->y-15, 'UTF-8');
        $this->y = $this->y - $height;
        $this->y = $this->y -20;
        $oldY = $this->y;
        $this->insertAddress($page, null);
		
         $style->setFont($font,10);
        $page->setStyle($style);
        $page->drawText(__("HST# 855070462"), $x + 385, $oldY+5, 'UTF-8');
		if(isset($returnDetail['otherRefNum']) && !empty(trim($returnDetail['otherRefNum']))){
		$oldY = $oldY-5;
		$style->setFont($font,9);
        $page->setStyle($style);
        $page->drawText(__("Customer PO#: ".$returnDetail['otherRefNum']), $x + 385, $oldY, 'UTF-8');
		}
        $style->setFont($font,10);
        $page->setStyle($style);
        $page->drawText(__(date('Y-m-d',strtotime($returnDetail['tranDate']))), $x + 385, $oldY-10, 'UTF-8');
        $this->y = $this->y -20;
        $style->setFont($fontBold,9);
        $page->setStyle($style);
        $oldY = $this->y-20;
        $page->drawText(__('Bill To'), $x, $this->y, 'UTF-8');
        $this->y = $this->y - 20;
        $beforeHeight = $this->y;
        $this->insertNetsuiteBillingAddress($page,$returnDetail['billingAddress']['addrText']);
        $addressHeight = $beforeHeight - $this->y;
        
		$style->setFont($fontBold,9);
		 $page->setStyle($style);
		$page->drawText(__('Ship To'), 180, $oldY+20, 'UTF-8');
        $this->y = $oldY;
        $beforeHeight = $this->y;
        $this->insertNetsuiteShippingAddress($page,$returnDetail['shippingAddress']['addrText']);
        $addressHeight = $beforeHeight - $this->y;
		
		
        $style->setFont($fontBold,14);
        $page->setStyle($style);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.92)); // Keep white color for the table header
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.92));
        $page->drawRectangle($x + 300, $oldY+20, $pageWidth-20, $this->y+10);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->drawText(__('TOTAL'), $x + 310, $oldY, 'UTF-8');
        $style->setFont($font,22);
        $page->setStyle($style);
        //$returnDetail['total'] = 1114.5;
        $this->insertTotal($page,[$this->pricingHelper->currency(abs($returnDetail['total']),1)],$addressHeight);
        $this->y = $this->y -30;
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.92)); // Keep white color for the table header
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.92));
        $page->drawRectangle($x, $this->y, $pageWidth-20, $this->y+20);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0));
		
		
		 $style->setFont($fontBold,8);
        $page->setStyle($style);
		$page->drawText(__("Sales Rep"), $x + 5, $this->y+10, 'UTF-8');
		$page->drawText(__("Shipping Method"), $x +200, $this->y+10, 'UTF-8');
		$page->drawText(__("Currency"), $x +400, $this->y+10, 'UTF-8');
        //$page->drawText(__("Tracking#"), $x+440 , $this->y+10, 'UTF-8');
        
        
        
		
        $this->y = $this->y -20;
		 $style->setFont($font,9);
		 $page->setStyle($style);
		 if(is_array($returnDetail['salesRep']))
        $page->drawText(__($returnDetail['salesRep']['name']), $x + 5, $this->y+10, 'UTF-8');
	
		 if(is_array($returnDetail['shipMethod']))
		$page->drawText(__($returnDetail['shipMethod']['name']), $x+200 , $this->y+10, 'UTF-8');
        
		$page->drawText(__($returnDetail['currencyName']), $x +400 , $this->y+10, 'UTF-8');
        //$page->drawText(__($returnDetail['shipMethod']['name']), $x + 440, $this->y+10, 'UTF-8');
		
		
		
		$this->y = $this->y -20;
		
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.92)); // Keep white color for the table header
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.92));
        $page->drawRectangle($x, $this->y, $pageWidth-20, $this->y-30);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.25));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.25));
 
        $style->setFont($fontBold,8);
        $page->setStyle($style);
        $page->drawText(__("Qty"), $x+5 , $this->y-17, 'UTF-8');
        $page->drawText(__("SKU"), $x + 50, $this->y-17, 'UTF-8');
        $page->drawText(__("Description"), $x + 180, $this->y-17, 'UTF-8');
        $page->drawText(__("Options"), $x + 380, $this->y-17, 'UTF-8');
        $page->drawText(__("Unit Price"), $x + 460, $this->y-17, 'UTF-8');
        $page->drawText(__("Amout"), $x + 525, $this->y-17, 'UTF-8');
 
        $style->setFont($font,9);
        $page->setStyle($style);
        $this->y = $this->y - 40;
        $maxHeight = [];
        foreach($returnDetail['itemList']['item'] as $item){
          $maxHeight[] = $this->displayQty($page,[$item['quantity']],$x);
          $maxHeight[] =  $this->displaySku($page,[$item['item']['name']],$x+15);
          $maxHeight[] = $this->displayProductName($page,[$item['description']],$x,180,40);
          $maxHeight[] =  $this->displayProductName($page,[$this->getCustColorValue($item['options'])],$x,380,15);       
          $maxHeight[] = $this->displayPrice($page,[(string)substr($this->apiHelper->getFormattedPrice($item['rate']), 2)],$x,490);
         $maxHeight[] = $this->displayPrice($page,[(string)substr($this->apiHelper->getFormattedPrice($item['amount']),2)],$x,$page->getWidth()-40);
          $this->y = $this->y - ($this->y - min($maxHeight));
          $this->y = $this->y -5;
           if($this->y < 100){
			// Display Page Number
			$style->setFont($font,8);
			$page->setStyle($style);
			$page->drawText(++$pageNumber,  $pageWidth-20 , 20, 'UTF-8');
			
            $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
            
            $page = $pdf->pages[$pageNumber];
            $this->y = 850 - 30;
			$this->displayHeaderSectionInNewPage($page,$style,$font,$x,$returnDetail);
           }
          $maxHeight = [];
        }
        $this->y = $this->y -10;
        
        if($this->y < 100){
			// Display Page Number
			$style->setFont($font,8);
			$page->setStyle($style);
			$page->drawText(++$pageNumber,  $pageWidth-20 , $this->y-20, 'UTF-8');
			
          $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
          $page = $pdf->pages[$pageNumber];
          $this->y = 850 - 30;
		  $this->displayHeaderSectionInNewPage($page,$style,$font,$x,$returnDetail);
         }
         $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.92)); // Keep white color for the table header
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.92));
        $page->drawRectangle($x, $this->y, $pageWidth-20, $this->y-1);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.25));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.25));
        $this->y = $this->y - 30;
        $style->setFont($font,15);
        $page->setStyle($style);
        $this->insertSubtotalText($page,['Subtotal'],$x,$page->getWidth()-200);
        $this->insertSubtotal($page,[$this->pricingHelper->currency(abs($returnDetail['subTotal']),1)],$x,$page->getWidth()-30);
		
		//Shipping Section
		//if(isset($returnDetail['shippingCost']) && !empty($returnDetail['shippingCost'])){
		$this->y = $this->y - 20;
        $this->insertSubtotalText($page,['Shipping'],$x,$page->getWidth()-200);
		$this->insertSubtotal($page,[$this->pricingHelper->currency(abs($returnDetail['shippingCost']),1)],$x,$page->getWidth()-30);
		//}
		//Tax Section		
		$this->y = $this->y - 20;
        $this->insertSubtotalText($page,['Tax'],$x,$page->getWidth()-200);
        $this->insertSubtotal($page,[$this->pricingHelper->currency(abs($returnDetail['taxTotal']),1)],$x,$page->getWidth()-30);
        $this->y = $this->y - 10;
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.92)); // Keep white color for the table header
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.92));
        $page->drawRectangle($x+300, $this->y, $pageWidth-20, $this->y-25);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.25));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.25));
        $this->y = $this->y - 15;
       
        $this->insertSubtotalText($page,['Total'],$x,$page->getWidth()-200);
        $this->insertSubtotal($page,[$this->pricingHelper->currency(abs($returnDetail['total']),1)],$x,$page->getWidth()-30);
		$this->y = $this->y - 25;
		$warrantyText = 'Warranty Policy:';
		$style->setFont($fontBold,9);
		$page->setStyle($style);
		$page->drawText(__($warrantyText), $x + 5, $this->y, 'UTF-8');
		
		$warrantyText1 = 'Lifetime warranty on all Parts, 90-day warranty on all Tools and Accessories, 30-day warranty on all Mobile Phones.';
		
		$style->setFont($font,9);
		$page->setStyle($style);
		$page->drawText(__($warrantyText1), $x + 78, $this->y, 'UTF-8');
		$warrantyText2 = 'For more details on our Warranty and Return Policy please visit: https://site.nexuscellular.com/knowledge-base/warranties-and-returns';
		$this->y = $this->y - 10;
		$style->setFont($font,9);
		$page->setStyle($style);
		$page->drawText(__($warrantyText2), $x + 5, $this->y, 'UTF-8');
		
        $fileName = $returnDetail['tranId'].'.pdf';
        $this->fileFactory->create(
           $fileName,
           $pdf->render(),
           \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, // this pdf will be saved in var directory with the name example.pdf
           'application/pdf'
        );
    }
	
	protected function displayHeaderSectionInNewPage(&$page,$style,$font,$x,$returnDetail){
		// Display Header Section(LOGO,order number,date and PO number)
			$this->drawLogo($page);
			$style->setFont($font,15);
			$page->setStyle($style);
			$page->drawText(__("Sales Order# ".$returnDetail['tranId']), $x + 385, $this->y, 'UTF-8');
			$this->y = $this->y-13;
			$style->setFont($font,10);
			$page->setStyle($style);
			$this->y = $this->y;
			$page->drawText(__("HST# 855070462"), $x + 385, $this->y, 'UTF-8');
			if(isset($returnDetail['otherRefNum']) && !empty(trim($returnDetail['otherRefNum']))){
				$this->y = $this->y-10;
				$style->setFont($font,9);
				$page->setStyle($style);
				$page->drawText(__("Customer PO#: ".$returnDetail['otherRefNum']), $x + 385, $this->y, 'UTF-8');
			}
			$this->y = $this->y-15;
			$style->setFont($font,10);
			$page->setStyle($style);
			$page->drawText(__(date('Y-m-d',strtotime($returnDetail['tranDate']))), $x + 385,$this->y, 'UTF-8');
			$this->y = $this->y-20;
	}
    protected function getCustColorValue($options){
      $value = '';
      
      if(is_array($options) && count($options) > 0){
        $value = $this->getCustomOptionValue($options['customField'][0]);
      }
      //echo "<pre>";print_r($value);die();
      return $value;
    }
    protected function getCustomOptionValue($option){
      
      return "Color:".$option['value']['name'];
    }
    protected function getCustomFieldValue($fieldName,$customLists){
      $value = '';
      foreach($customLists as $customList){
        if($fieldName == $customList['scriptId']){
          $value = $customList['value'];
          break;
        }
      }
      return $value;
    }
    protected function getCustomSelectFieldValue($fieldName,$customLists){
      $value = '';
      foreach($customLists as $customList){
        if($fieldName == $customList['scriptId']){
          $value = $customList['value']['name'];
          break;
        }
      }
      return $value;
    }
     /**
     * Returns prepared for PDF text, reversed in case of RTL text
     *
     * @param string $string
     * @return string
     */
    private function prepareText(string $string): string
    {
        // phpcs:ignore Magento2.Functions.DiscouragedFunction
        return $this->rtlTextHandler->reverseRtlText(html_entity_decode($string));
    }
   
    protected function displayQty(&$page, $values,$x)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 9);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;
      
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 15, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        //$this->getAlignLeft($_value, 30, 10, $font, 10),
                        $this->getAlignLeft($_value, $x+5, 12, $font, 20),
                        $top,
                        'UTF-8'
                    );
                    $top -= 10;
                }
            }
        }
        return $top;
    }
    protected function displaySku(&$page, $values,$x)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontBoldRegular($page, 9);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;
       $values = explode(':',$values[0]);
       if(count($values) > 1){
         $newSku = $values[1];
        $values = []; 
        $values[] = $newSku;
       }
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 20, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        $this->getAlignLeft($_value, $x, 40, $font, 20),
                        $top,
                        'UTF-8'
                    );
                    $top -= 12;
                }
            }
        }
        return $top;
    }
    protected function displayProductName(&$page, $values,$x,$width=40,$textWidth=15)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 9);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;
      
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, $textWidth, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        //$this->getAlignLeft($_value, 30, 10, $font, 10),
                        $this->getProductAlignLeft($_value, $x, $width, $font, 0),
                        $top,
                        'UTF-8'
                    );
                    $top -= 10;
                }
            }
        }
       // $this->y = $this->y < $top ? $top : $this->y;
       return $top;
    }
    public function getProductAlignLeft($string, $x, $columnWidth, \Zend_Pdf_Resource_Font $font, $fontSize, $padding = 0)
    { 
         $width = $this->widthForStringUsingFontSize($string, $font, $fontSize);
        return $x + $columnWidth - $width - $padding;
    }
    protected function displayPrice(&$page, $values,$x,$width=40)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 9);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y; 
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 11, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        //$this->getAlignLeft($_value, 30, 10, $font, 10),
                        $this->getAlignRight($_value, $x, $width, $font, 8),
                        $top,
                        'UTF-8'
                    );
                    $top -= 14;
                }
            }
        }
        return $top;
    }
    protected function insertSubtotal(&$page, $values,$x,$width)
    {
      $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
      $font = $this->_setFontRegular($page, 10);
      $page->setLineWidth(0);
      $this->y = $this->y ? $this->y : 815;
      $top = $this->y;
     
     
      foreach ($values as $value) {
          if ($value !== '') {
              $value = preg_replace('/<br[^>]*>/i', "\n", $value);
              foreach ($this->string->split($value, 80, true, true) as $_value) {
                  $page->drawText(
                      trim(strip_tags($_value)),
                      //$this->getAlignLeft($_value, 30, 10, $font, 10),
                      $this->getAlignRight($_value, 10, $width, $font, 10),
                      $top,
                      'UTF-8'
                  );
                  $top -= 14;
              }
          }
      }
      //$this->y = $this->y > $top ? $top : $this->y;
    }
    protected function insertSubtotalText(&$page, $values,$x,$width)
    {
      $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
      $font = $this->_setFontBoldRegular($page, 10);
      $page->setLineWidth(0);
      $this->y = $this->y ? $this->y : 815;
      $top = $this->y;
     
     
      foreach ($values as $value) {
          if ($value !== '') {
              $value = preg_replace('/<br[^>]*>/i', "\n", $value);
              foreach ($this->string->split($value, 80, true, true) as $_value) {
                  $page->drawText(
                      trim(strip_tags($_value)),
                      //$this->getAlignLeft($_value, 30, 10, $font, 10),
                      $this->getAlignRight($_value, 10, $width, $font, 10),
                      $top,
                      'UTF-8'
                  );
                  $top -= 14;
              }
          }
      }
      //$this->y = $this->y > $top ? $top : $this->y;
    }
    
    protected function insertTotal(&$page, $values,$addressHeight)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 20);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y+($addressHeight/2);
       
       
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 80, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        //$this->getAlignLeft($_value, 30, 10, $font, 10),
                        $this->getAlignRight($_value, 10, 540, $font, 20),
                        $top,
                        'UTF-8'
                    );
                    $top -= 14;
                }
            }
        }
        $this->y = $this->y > $top ? $top : $this->y;
    }
    public function getAlignRight($string, $x, $columnWidth, \Zend_Pdf_Resource_Font $font, $fontSize, $padding = 5)
    {
        $width = $this->widthForStringUsingFontSize($string, $font, $fontSize);
        return $x + $columnWidth - $width - $padding;
    }
     /**
     * Insert address to pdf page
     *
     * @param \Zend_Pdf_Page $page
     * @param string|null $store
     * @return void
     */
    protected function insertAddress(&$page, $store = null)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 10);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;
        $values = explode(
            "\n",
            $this->_scopeConfig->getValue(
                'sales/identity/address',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $store
            )
        );
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 45, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        $this->getAlignLeft($_value, 20, 10, $font, 10),
                        $top,
                        'UTF-8'
                    );
                    $top -= 12;
                }
            }
        }
        $this->y = $this->y > $top ? $top : $this->y;
    }
    /**
     * Insert Netsuite Customer billing address to pdf page
     *
     * @param \Zend_Pdf_Page $page
     * @param string|null $store
     * @return void
     */
    protected function insertNetsuiteBillingAddress(&$page, $address = null)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 9);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;
        $values = explode(
            "\n",
            $address
        );
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 30, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        $this->getAlignLeft($_value, 20, 10, $font, 10),
                        $top,
                        'UTF-8'
                    );
                    $top -= 12;
                }
            }
        }
        $this->y = $this->y > $top ? $top : $this->y;
    }
	 /**
     * Insert Netsuite Customer shipping address to pdf page
     *
     * @param \Zend_Pdf_Page $page
     * @param string|null $store
     * @return void
     */
    protected function insertNetsuiteShippingAddress(&$page, $address = null)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $font = $this->_setFontRegular($page, 9);
        $page->setLineWidth(0);
        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;
        $values = explode(
            "\n",
            $address
        );
        foreach ($values as $value) {
            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 30, true, true) as $_value) {
                    $page->drawText(
                        trim(strip_tags($_value)),
                        $this->getAlignLeft($_value, 180, 10, $font, 10),
                        $top,
                        'UTF-8'
                    );
                    $top -= 12;
                }
            }
        }
        $this->y = $this->y > $top ? $top : $this->y;
    }
     /**
     * Set font as regular
     *
     * @param  \Zend_Pdf_Page $object
     * @param  int $size
     * @return \Zend_Pdf_Resource_Font
     */
    protected function _setFontRegular($object, $size = 7)
    {
       /* $font = \Zend_Pdf_Font::fontWithPath(
            $this->_rootDirectory->getAbsolutePath('lib/internal/GnuFreeFont/FreeSerif.ttf')
        );*/
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $object->setFont($font, $size);
        return $font;
    }
    protected function _setFontBoldRegular($object, $size = 7)
    {
       /* $font = \Zend_Pdf_Font::fontWithPath(
            $this->_rootDirectory->getAbsolutePath('lib/internal/GnuFreeFont/FreeSerif.ttf')
        );*/
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $object->setFont($font, $size);
        return $font;
    }
     /**
     * Calculate coordinates to draw something in a column aligned to the right
     *
     * @param  string $string
     * @param  int $x
     * @param  int $columnWidth
     * @param  \Zend_Pdf_Resource_Font $font
     * @param  int $fontSize
     * @param  int $padding
     * @return int
     */
    public function getAlignLeft($string, $x, $columnWidth, \Zend_Pdf_Resource_Font $font, $fontSize, $padding = 0)
    {
         $width = 10;//$this->widthForStringUsingFontSize($string, $font, $fontSize);
        return $x + $columnWidth - $width - $padding;
    }
    /**
     * Returns the total width in points of the string using the specified font and size.
     *
     * This is not the most efficient way to perform this calculation. I'm
     * concentrating optimization efforts on the upcoming layout manager class.
     * Similar calculations exist inside the layout manager class, but widths are
     * generally calculated only after determining line fragments.
     *
     * @param  string $string
     * @param  \Zend_Pdf_Resource_Font $font
     * @param  float $fontSize Font size in points
     * @return float
     */
    public function widthForStringUsingFontSize($string, $font, $fontSize)
    {
        // phpcs:ignore Generic.PHP.NoSilencedErrors
        $drawingString = '"libiconv"' == ICONV_IMPL ? iconv(
            'UTF-8',
            'UTF-16BE//IGNORE',
            $string
        // phpcs:ignore Generic.PHP.NoSilencedErrors
        ) : @iconv(
            'UTF-8',
            'UTF-16BE',
            $string
        );

        $characters = [];

        $drawingStringLength = strlen($drawingString);

        for ($i = 0; $i < $drawingStringLength; $i++) {
            $characters[] = ord($drawingString[$i++]) << 8 | ord($drawingString[$i]);
        }
        $glyphs = $font->glyphNumbersForCharacters($characters);
        $widths = $font->widthsForGlyphs($glyphs);
        $stringWidth = array_sum($widths) / $font->getUnitsPerEm() * $fontSize;
        return $stringWidth;
    }
  }
  

