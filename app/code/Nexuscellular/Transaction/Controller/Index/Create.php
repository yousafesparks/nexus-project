<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Transaction\Controller\Index;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
use Nexuscellular\Transaction\Helper\SoapApiCall as soapApiCall;
class Create extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
  protected $soapApiCall;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      soapApiCall $soapApiCall,
      restSuiteqlApi $restapiHelper
      )
    {
      
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      $this->soapApiCall = $soapApiCall;
      return parent::__construct($context);
    }

    public function execute()
    {
      if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }
     //6813
     $response = $this->soapApiCall->getCustomerByInternalId(6226);
     /*$query = "{\n\t\"q\": \"SELECT * FROM PricingGroup \"\n}";
     $response = $this->restSuiteqlApi->callRestApi($query);
      echo "<pre>";
      print_r($response);*/
      echo "<pre>";
      
      print_r($response['groupPricingList']);
     die();
      $customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
       $query = "{\n\t\"q\": \"SELECT TransactionLine.quantity,TransactionLine.Item as iteminternalid,TransactionLine.custcol_f3_document_number,Transaction.trandate,Transaction.recordtype FROM TransactionLine INNER JOIN Transaction ON(Transaction.id = TransactionLine.transaction AND TransactionLine.entity = ".$customerNetsuiteInternalId." and Transaction.recordtype='returnauthorization' AND TransactionLine.itemtype != 'TaxGroup' AND TransactionLine.itemtype != 'ShipItem' AND TransactionLine.taxline = 'F' AND TransactionLine.mainline = 'F')   \"\n}";
      $response = $this->restSuiteqlApi->callRestApi($query);
      $this->customerSession->unsCustomerReturnData();
      if(isset($response['count']) && $response['count'] > 0 ){
       /*echo "<pre>";
       print_r($response['items']);die();*/
        $this->customerSession->setCustomerReturnData($response['items']);
      }
      
    //.custcol_f3_document_number,TransactionLine.item,TransactionLine.quantity
      return $this->_pageFactory->create();
    
    }
  }
  

