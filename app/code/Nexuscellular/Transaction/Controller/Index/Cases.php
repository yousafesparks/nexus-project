<?php
namespace Nexuscellular\Transaction\Controller\Index;

use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;

class Cases extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context, 
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      return parent::__construct($context);
    }

    public function execute()
    { 
      if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }
      $customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
      $fromDate = $this->getRequest()->getParam('fromdate') ? $this->getRequest()->getParam('fromdate') : '2017-01-01';
      $toDate = $this->getRequest()->getParam('todate') ? $this->getRequest()->getParam('todate') : date('Y-m-d') ;
      $dir_order = $this->getRequest()->getParam('dir_order') ? $this->getRequest()->getParam('dir_order') : 'desc' ;
      $status = $this->getRequest()->getParam('status'); 
      if(!empty($status)){
        $query = "{\n\t\"q\": \"SELECT id,casenumber,title,datecreated,status FROM supportcase WHERE company = '".$customerNetsuiteInternalId."' AND status IN( ".$status.") ORDER BY id ".$dir_order." \"\n}";
        $response = $this->restSuiteqlApi->callRestApi($query);
      }else{
      $query = "{\n\t\"q\": \"SELECT id,casenumber,title,datecreated,status FROM supportcase WHERE company = '".$customerNetsuiteInternalId."' ORDER BY id ".$dir_order." \"\n}";
      $response = $this->restSuiteqlApi->callRestApi($query);
      }
      $block = $this->_view->getLayout()->createBlock('Nexuscellular\Transaction\Block\Ajaxcaselist')->setTemplate('Nexuscellular_Transaction::ajaxcaselist.phtml')->setResponse($response)->toHtml();
      $this->getResponse()->setBody($block);
    }
  }
  

