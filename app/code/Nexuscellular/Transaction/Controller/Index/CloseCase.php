<?php
namespace Nexuscellular\Transaction\Controller\Index;

use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;

class CloseCase extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context, 
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      return parent::__construct($context);
    }

    public function execute()
    {
        if(!$this->apiHelper->getCustomerId()){
          $login_url = $this->urlInterface
        ->getUrl('customer/account/login');
        return $this->resultRedirectFactory->create()->setPath($login_url);
        }
        if (!$this->getRequest()->isPost()) {
          $this->messageManager->addError('Please fill up required field.');
          return $this->resultRedirectFactory->create()->setPath('*/*/viewcase',['id' => $data['caseid']]);
            }
          $data = $this->getRequest()->getPost();
        $service = new \NetSuiteService();

        //echo "<pre>";
        //print_r($this->getRequest()->getPost());die();
        $supportCase = new \supportCase();
        $supportCase->internalId = $data['caseid'];
        $supportCase->title = $data['title'];
        $supportCase->company = new \RecordRef();
        $supportCase->company->internalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
        $supportCase->status = new \RecordRef();
        $supportCase->status->internalId = 5; // 5 is intenral id of close status in supportcase Entity in Netsuite
        
        $supportCase->category = new \RecordRef();
        $supportCase->category->internalId = $data['category'];
        $supportCase->incomingMessage = $data['incomingMessage'];
        if(isset($data['email']) && !empty($data['email'])){
          $supportCase->email = $data['email'];
        }
        $request = new \UpdateRequest();
        $request->record = $supportCase;

        $addResponse = $service->update($request);
       /* echo "<pre>";
     print_r($addResponse);die();*/
        if (!$addResponse->writeResponse->status->isSuccess) {
          $this->messageManager->addError('something went worng please try again later!');
          return $this->resultRedirectFactory->create()->setPath('*/*/viewcase',['id' => $data['caseid']]);
        } else {
            // Display the succes form validation message
        $this->messageManager->addSuccess('Case successfully closed');
        return $this->resultRedirectFactory->create()->setPath('*/*/viewcase',['id' => $data['caseid']]);
        }
    }
  }
  

