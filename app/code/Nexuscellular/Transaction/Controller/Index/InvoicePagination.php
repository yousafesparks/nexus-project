<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\Transaction\Controller\Index;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class InvoicePagination extends \Magento\Framework\App\Action\Action
{
  CONST PAGE_SIZE = 7;
  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      return parent::__construct($context);
    }

    public function execute()
    {
      /*if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }*/
     

      $customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
      $fromDate = $this->getRequest()->getParam('fromdate') ? $this->getRequest()->getParam('fromdate') : '2017-01-01';
      $toDate = $this->getRequest()->getParam('todate') ? $this->getRequest()->getParam('todate') : date('Y-m-d') ;
	  $filter = $this->_setFilterCondition($this->getRequest()->getParam('filter'));
      $dir_order = $this->getRequest()->getParam('dir_order') ? $this->getRequest()->getParam('dir_order') : 'desc' ;
      $recordTypes = $this->getRequest()->getParam('recordtypes');
	  $status = $this->getRequest()->getParam('status') ? $this->getRequest()->getParam('status') : 'A' ;
      $paramString = "?".$this->getRequest()->getParam('paramString');
      
      $query = "{\n\t\"q\": \"SELECT transaction.foreignamountunpaid,closedate,transaction.foreignamountpaid,transaction.duedate,transaction.id,tranid,trandate,foreigntotal,status,recordtype,type,transaction.trandisplayname FROM Transaction WHERE Transaction.recordtype =('".$recordTypes."') ".$filter." AND Transaction.status='".$status."'  AND ( Transaction.TranDate BETWEEN TO_DATE('".$fromDate."', 'YYYY-MM-DD' ) AND TO_DATE('".$toDate."', 'YYYY-MM-DD' ) ) AND ( Transaction.Voided = 'F' ) AND ( Transaction.Voided = 'F' ) AND (Transaction.entity = '".$customerNetsuiteInternalId."') ORDER BY transaction.duedate ".$dir_order." \"\n}";
      $response = $this->restSuiteqlApi->callRestApi($query,$paramString);
	  if(isset($response['count']) && $response['count'] > 0)
		$response['filterstatus'] = $status;
     $block = $this->_view->getLayout()->createBlock('Nexuscellular\Transaction\Block\Ajaxinvoicelist')->setTemplate('Nexuscellular_Transaction::ajaxinvoicelist.phtml')->setResponse($response)->toHtml();
      $this->getResponse()->setBody($block);
    }
	protected function _setFilterCondition($filter){
		$condition = '';
		switch ($filter) {
		/*   case "all":
			echo "Your favorite color is red!";
			break; */
		  case "overdue":
			$date = date('Y-m-d');
			$condition="AND foreignamountunpaid !=0 AND '".$date."'>duedate";
			break;
		  case "next7days":
			$fromDate = date('Y-m-d');
			$toDate =  date("Y-m-d",strtotime("+7 days"));
			$condition="AND ( Transaction.duedate BETWEEN TO_DATE('".$fromDate."', 'YYYY-MM-DD' ) AND TO_DATE('".$toDate."', 'YYYY-MM-DD' ) )";
			break;
		case "next30days":
			$fromDate = date('Y-m-d');
			$toDate =  date("Y-m-d",strtotime("+30 days"));
			$condition="AND ( Transaction.duedate BETWEEN TO_DATE('".$fromDate."', 'YYYY-MM-DD' ) AND TO_DATE('".$toDate."', 'YYYY-MM-DD' ) )";
			break;
		case "next60days":
			$fromDate = date('Y-m-d');
			$toDate =  date("Y-m-d",strtotime("+60 days"));
			$condition="AND ( Transaction.duedate BETWEEN TO_DATE('".$fromDate."', 'YYYY-MM-DD' ) AND TO_DATE('".$toDate."', 'YYYY-MM-DD' ) )";
			break;
		case "next90days":
			$fromDate = date('Y-m-d');
			$toDate =  date("Y-m-d",strtotime("+90 days"));
			$condition="AND ( Transaction.duedate BETWEEN TO_DATE('".$fromDate."', 'YYYY-MM-DD' ) AND TO_DATE('".$toDate."', 'YYYY-MM-DD' ) )";
			break;
		  default:
			$condition="";
		}
		return $condition;
	}
  }