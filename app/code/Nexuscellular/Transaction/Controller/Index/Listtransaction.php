<?php
namespace Nexuscellular\Transaction\Controller\Index;

use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;

class Listtransaction extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context, 
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      restSuiteqlApi $restapiHelper
      )
    {
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      return parent::__construct($context);
    }

    public function execute()
    {
      if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }
     

      $customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
       //$query = "{\n\t\"q\": \"SELECT Transaction.* FROM Transaction WHERE recordtype='returnauthorization' AND entity = '".$customerNetsuiteInternalId."' \"\n}";
       
       //$query = "{\n\t\"q\": \"SELECT TransactionLine.* FROM TransactionLine WHERE mainline = 'F' AND taxline='F' and transaction = '314245' \"\n}";
     // $query = "{\n\t\"q\": \"SELECT SUM(TransactionLine.quantity) as total_qty,tranid,trandate,foreigntotal,status FROM Transaction INNER JOIN TransactionLine ON ( TransactionLine.Transaction = Transaction.ID ) WHERE ( Transaction.Type = 'RtnAuth' ) AND ( Transaction.Voided = 'F' ) AND(TransactionLine.mainline = 'F') AND (TransactionLine.taxline='F') AND (Transaction.entity = '".$customerNetsuiteInternalId."')  GROUP BY Transaction.tranid,Transaction.trandate,Transaction.foreigntotal,Transaction.status ORDER BY transaction.trandate DESC \"\n}";
     // $response = $this->restSuiteqlApi->callRestApi($query);
     // echo "<pre>";
     //print_r($response);die();
      //$this->customerSession->unsCustomerReturnData();
      if(isset($response['count']) && $response['count'] > 0 ){
      
       // $this->customerSession->setCustomerReturnData($response['items']);
      }
   
    //.custcol_f3_document_number,TransactionLine.item,TransactionLine.quantity
      return $this->_pageFactory->create();
    
    }
  }
  

