<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Listinvoices extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->restSuiteqlApi = $restapiHelper;
		parent::__construct($context);
	}
}