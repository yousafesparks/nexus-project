<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\SoapApiCall as SoapApiCall;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Creditmemo extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;
	protected $request;
	
	protected $soapApiCall;
	protected $netsuiteImageApiResponse;
	
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	SoapApiCall $SoapApiCall,
	\Magento\Framework\App\Request\Http $request,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->request = $request;
		$this->soapApiCall = $SoapApiCall;
		$this->restSuiteqlApi = $restapiHelper;
		$this->netsuiteImageApiResponse = null;
		parent::__construct($context);
	}

	public function getTransactionDetails()
	{	
		$transaction = [];
		$transaction['internal_id'] = $this->request->getParam('id');
		$recordtype = $this->request->getParam('type');
		//'salesorder','invoice','cashsale','creditmemo','customerpayment'
		if($recordtype == 'salesorder')
		$recordtype = 'salesOrder';
		else if($recordtype == 'invoice')
		$recordtype = 'invoice';
		else if($recordtype == 'cashsale')
		$recordtype = 'cashSale';
		else if($recordtype == 'creditmemo')
		$recordtype = 'creditMemo';
		else if($recordtype == 'customerpayment')
		$recordtype = 'customerPayment';

		$transaction['recordtype'] = $recordtype; 
		return  $this->apiHelper->getSoapTransactionDetails($transaction);
		
	}
	public function mappingItemsWithfulFillment($items,$fulfillItems){
		foreach($items as $index => $itemArray){
			foreach($fulfillItems as $fulFillItem){
				$checkQty = $this->mapWithfulFillitem($itemArray,$fulFillItem);
				 if(count($checkQty)){
					$items[$index]['quantity'] = $checkQty[0];	
					if($items[$index]['quantity'] <= 0){
						unset($items[$index]);
					}
				}
			}
		}
		return $items;
	}
	protected function mapWithfulFillitem($itemArray,$fulFillItem){
		$quantity = [];
		foreach($fulFillItem as $key =>$item){
			if(is_numeric($key)){
				foreach($item as $iteratItem){
					$sku = $this->getSku($itemArray['item']['name']);
					if($sku == $iteratItem['sku']){
						$quantity[] = $itemArray['quantity'] - $iteratItem['quantity'];
						break;
					}
				}
			}
		}
		return $quantity;
	}
	public function getUnitPrice($itemsArray,$sku){
		$rate = '0';
		foreach($itemsArray as $item){
			$mappingsku = $this->getSku($item['item']['name']);
			if($sku == $mappingsku){
				$rate = $item['rate'];
				break;
			}
		}
		return $rate;
	}
	public function getItemFulfillmentBySalesOrderId($transaction,$getItemsImages){
		$response = $this->soapApiCall->getItemFulfillmentBySalesOrderId($transaction);
		if(count($response) > 0){
			$itemsResponse = [];
			foreach($response['record'] as $value){
				$newResponseArray=[];
				$newResponseArray['shippingAddress']	=	$value['shippingAddress'];
				$newResponseArray['tranDate']			=	$value['tranDate'];
				$newResponseArray['shipMethod']			=	$value['shipMethod']['name'];
				$newResponseArray['tranDate']			=	$value['tranDate'];
				$newResponseArray['tranId']				=	$value['tranId'];
				$newResponseArray['trackingInfo']		= 	$this->getTrackingInfo($value);
				$newResponseArray['shipStatus']			=	$this->_getFulfillmentStatus($value['shipStatus']);
				$newResponseArray[$value['internalId']] =	$this->_getDecorateArray($value['itemList']['item'],$transaction,$getItemsImages);
				$itemsResponse[] = $newResponseArray;
			}
			//echo "<pre>";print_r($response);die();
			return $itemsResponse;
		}
		return $response;
	}
	protected function getTrackingInfo($value){
		$trackInfo = [];
		if(stripos($value['shipMethod']['name'], 'fedex') !== false && is_array($value['packageFedExList']) && count($value['packageFedExList']) > 0){
			$methodName = 'FedEx';
			$trackInfo = $this->addTrackInfo($value['packageFedExList']['packageFedEx'],$methodName);
		}elseif(stripos($value['shipMethod']['name'], 'ups') !== false && is_array($value['packageFedExList']) && count($value['packageFedExList']) > 0){
			$methodName = 'Ups';
			$trackInfo = $this->addTrackInfo($value['packageUpsList']['packageUps'],$methodName);
		}elseif(stripos($value['shipMethod']['name'], 'usps') !== false && is_array($value['packageFedExList']) && count($value['packageFedExList']) > 0){
			$methodName = 'Usps';
			$trackInfo = $this->addTrackInfo($value['packageUspsList']['packageUsps'],$methodName);
		}
		return $trackInfo;
	}
	protected function addTrackInfo($trackArray,$methodName){
		$tracking = [];
		if(count($trackArray) > 0){
			 foreach($trackArray as $track){
				 $index = 'packageTrackingNumber'.$methodName;
				$tracking[] = ['shipmethod'=>$methodName,'trackingnumber'=>$track[$index]];
			}
		}
		return $tracking;
	}
	protected function _getDecorateArray($itemList,$transaction,$getItemsImages){
		$itemArray = [];
		foreach($itemList as $item){
			$value['sku'] 			= $item['item']['name'];
			$value['whole_sku'] 	= $this->getWholeSku($transaction['itemList']['item'],$value['sku']);
			$value['intenralId'] 	= $item['item']['internalId'];
			$value['name'] 			= $item['description'];
			$value['quantity'] 		= $item['quantity'];
			$value['image'] 		= $this->getImage($value['intenralId'],$getItemsImages);
			$value['unitprice'] 	= $this->getUnitPrice($transaction['itemList']['item'],$value['sku']);
			$value['amount'] 		= $value['unitprice']* $value['quantity'];
			$value['is_available'] 	= $this->isProductAvailable($item['item']['name']);
			$value['currencyName']	= $transaction['currencyName'];
			$value['options']		= $this->getOptions($transaction['itemList']['item'],$value['sku']);
			$value['optionName']		= $this->getOptionName($transaction['itemList']['item'],$value['sku']);
			
			$itemArray[] = $value;
		}
		return $itemArray;
	}
	protected function _getFulfillmentStatus($status){
		if($status == '_picked' || $status == '_packed'){
			return 'Pending Shipment';
		}elseif($status == '_shipped'){
			return 'Shipped';
		}else{
			return 'N/A';
		}
	}
	public function getItemFulfillment($transaction,$getItemsImages){
		$salesOrderID = $transaction['internalId'];
		
		$query = "{\n\t\"q\": \"SELECT transactionline.transaction,transactionshipment.shippingmethod,item.id as internalId,item.description,BUILTIN.DF(transactionline.item) as sku,transactionline.netamount,transactionline.rateamount,abs(transactionline.quantity) as quantity from transactionline INNER JOIN Transaction ON(transaction.id=transactionline.transaction AND transaction.type='ItemShip' AND transactionline.createdfrom = '".$salesOrderID."' AND transactionline.isInventoryAffecting = 'F' AND transactionline.mainline='F' AND transactionline.fulfillable='T' AND transactionline.quantity > 0)  INNER JOIN transactionshipment ON(transactionshipment.doc= transactionline.transaction) INNER JOIN item ON(item.id=transactionline.item)\"\n}";
		$response = $this->restSuiteqlApi->callRestApi($query);
		
		if(isset($response['count']) && $response['count'] > 0){
			$items = $this->_decorateArray($response['items'],$getItemsImages,$transaction);
			return $items;
		}
		return [];
	}
	 public function isProductAvailable($currentsku){
		 $isProductAvailable = '';
		 $currentsku = $this->getSku($currentsku);
		if(!is_null($this->netsuiteImageApiResponse)){
			$responseItems = $this->netsuiteImageApiResponse;
			foreach($responseItems['items'] as $item){
				$sku = $this->getSku($item['itemid']);
				//echo "<pre>";print_r($item);die();
				if($currentsku == $sku && ($item['isinactive'] == 'T')){
					$isProductAvailable = 1; // No longer availbale
					break;
				}elseif($currentsku == $sku && $item['isonline'] == 'F'){
					$isProductAvailable = 1; // out of stock
					break;
				}elseif($currentsku == $sku && $item['isinactive'] == 'F' && $item['quantityavailable'] <=0){
					$isProductAvailable = 2; // out of stock
					break;
				}
			}
		}
		return $isProductAvailable;
	}
	public function countItems($renderItems){
		$count = 0;
		foreach($renderItems as $items){
			$count = $count + abs($items['quantity']);
		}
		return $count;
	}
	public function getCustomFieldValue($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = $customList['value'];
				break;
			}
		}
		return $value;
	}
	public function getOptions($items,$sku){
		$string = '';
		foreach($items as $item){
			$orderSku = $this->getSku($item['item']['name']);
			if($orderSku == $sku && isset($item['options']) && count($item['options']) > 0){
				foreach($item['options']['customField'] as $field){
					$string.='<span class="transaction-line-views-cell-navigable-item-amount-label">'.($field['scriptId'] == "custcol_color") ? 'Color: ' : $field['scriptId'].'</span>';
					$string.='<span class="value">'.$field['value']['name'].'</span>';
				}
			}
		}
		return $string;
	}
	public function getOptionName($items,$sku){
		$string = '';
		foreach($items as $item){
			$orderSku = $this->getSku($item['item']['name']);
			if($orderSku == $sku && isset($item['options']) && count($item['options']) > 0){
				foreach($item['options']['customField'] as $field){
					$string=$field['value']['name'];
					break;
				}
			}
		}
		return $string;
	}
	public function getWholeSku($items,$sku){
		$string = '';
		foreach($items as $item){
			$orderSku = $this->getSku($item['item']['name']);
			if($orderSku == $sku){
				$string = $item['item']['name'];
				break;
			}
		}
		return $string;
	}
	public function getCustomSelectFieldValue($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = $customList['value']['name'];
				break;
			}
		}
		return $value;
	}
	public function getAllItemsImages($items){
		$skus = [];
		foreach($items as $item){
			$skus[]= "'".$this->getSku($item['item']['name'])."'";
		}
		if(is_null($this->netsuiteImageApiResponse)){
		$query = "{\n\t\"q\": \"SELECT inventoryitemLocations.quantityavailable,item.itemid,item.isinactive,item.isonline,item.id,item.custitemzg_image_filename,item.custitemzg_num_images FROM item INNER JOIN inventoryitemLocations ON(item.id=inventoryItemLocations.item AND inventoryItemLocations.location=1 AND item.itemid IN (".implode(',',$skus).") ) \"\n}";
		$response = $this->restSuiteqlApi->callRestApi($query);
		//echo "<pre>";print_r($response);die();
		}
		$images = [];
		if(isset($response['count']) && $response['count'] > 0){
			$this->netsuiteImageApiResponse = $response;
			foreach($response['items'] as $item){
					$images[$item['id']]= $this->_getProductImage($item);
			}
		}
		return $images;
		
	}
	public function getImage($inetnalId,$images){
		$image = "https://www.nexuscellular.com/sca-dev-elbrus/img/no_image_available.jpeg?resizeid=2&amp;resizeh=175&amp;resizew=175";
		if(count($images) > 0){
			foreach($images as $id=>$value){
				if($inetnalId == $id ){
					$image = $value;
					break;
				}
			}
		}
		return $image;
	}
	protected function _getProductImage($item){
		if($item['custitemzg_num_images'] ==  0){
		$image = "https://www.nexuscellular.com/sca-dev-elbrus/img/no_image_available.jpeg?resizeid=2&amp;resizeh=175&amp;resizew=175";
		}elseif($item['custitemzg_num_images'] == 1){
			$image = $item['custitemzg_image_filename'];
		}else{
			$images = explode(",",$item['custitemzg_image_filename']);
			$image = $images[0];
		}
		return $image;
	}
	public function getTrackingLink($shipment,$tracknumber){
		$shipment = strtolower($shipment);
		if(strpos($shipment, "fedex") !== false)
			$link = "https://www.fedex.com/fedextrack/?trknbr=".trim($tracknumber);
		else if(strpos($shipment, "ups") !== false)
			$link = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=".trim($tracknumber);
		else
			$link = '';
		return $link;	
	}
	public function getShipMethodName($shipMethod){
		$shipMethod = strtolower($shipMethod);
		if(strpos($shipMethod, "fedex") !== false)
			$methodName = "Fedex";
		else if(strpos($shipMethod, "ups") !== false)
			$methodName = "UPS";
		else
			$methodName = $shipMethod;
		return 	$methodName;
	}
	public function getSku($sku){
		$sku = explode(':',$sku);
		if(count($sku) > 1)
		$sku = $sku[count($sku) - 1];
		else
		$sku = $sku[0];
		return trim($sku);
	}
}