<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
use Nexuscellular\Transaction\Helper\SoapApiCall as soapApi;
class Viewcase extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;
	protected $request;
	protected $soapApi;
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	\Magento\Framework\App\Request\Http $request,
	soapApi $soapApi,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->request = $request;
		$this->restSuiteqlApi = $restapiHelper;
		$this->soapApi = $soapApi;
		parent::__construct($context);
	}
	public function getCaseInternalId(){
		return $this->request->getParam('id');
	}
	public function getFormAction(){
		return $this->getUrl('transaction/index/createcasereply');
	}
	public function getCaseDetails(){	
		$entity = [];
		$entity['internal_id'] = $this->request->getParam('id');
		$entity['recordtype'] = 'supportCase'; 
		return  $this->soapApi->getRecordByInternalId($entity);
		
	}
	public function getMessageDetails(){
		$currentUTCTime = gmdate('h:i');
			date_default_timezone_set('America/Toronto');	
			$currentTime =  date('h:i');
			$a = new \DateTime($currentUTCTime);
			$b = new \DateTime($currentTime);
			$interval = $a->diff($b);
			$deductHrs = $interval->format("%h");
		$entity = [];
		$newEntry = [];
		$entity['internal_id'] = $this->request->getParam('id');
		$messages = $this->soapApi->searchMessagesByCaseId($entity);
		$records = array_reverse($messages['record'],true);
		foreach($records as $record){
			$record['messageTime'] =date('h:i', strtotime('-'.$deductHrs.' hours', strtotime($record['messageDate'])));
			$record['messageDate'] =date('Y-m-d',strtotime($record['messageDate'])); 
			$newEntry[] = $record;
		}
		return $newEntry;
	}
	public function getAuthorName($author){
		$customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
		if($author['internalId'] == $customerNetsuiteInternalId)
			$authorName = 'You';
		else
		$authorName = $author['name'];
		return $authorName;
	}
}