<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Fulfillment extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;
	protected $request;
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	\Magento\Framework\App\Request\Http $request,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->request = $request;
		$this->restSuiteqlApi = $restapiHelper;
		parent::__construct($context);
	}
	public function getFulfillitemsData(){
		$response = $this->getFulfillitems();
		return $response;
	}
	public function getUnitPrice($itemsArray,$sku){
		$rate = '0';
		foreach($itemsArray as $item){
			$mappingsku = $this->getSku($item['item']['name']);
			if($sku == $mappingsku){
				$rate = $item['rate'];
				break;
			}
		}
		return $rate;
	}
}