<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Ajaxinvoicelist extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->restSuiteqlApi = $restapiHelper;
		parent::__construct($context);
	}
	public function getResponseData(){
		$response = $this->getResponse();
		$responseData = [];
		if(isset($response['count']) && $response['count'] > 0 ){
			$responseData = $response;
		}
		return $responseData;
	}
	public function arraySortByField($array){
		usort($array, function ($item1, $item2) {
			return $item1['position'] <=> $item2['position'];
		});
		return $array;
	}
	public function checkDueCriteria($value,$filterstatus){
		if($filterstatus == 'A'){
		$duedate = $value['duedate'];
		$unpaidAmount = abs($value['foreignamountunpaid']);
		$paidAmount = abs($value['foreignamountpaid']);
		$date = strtotime(date('Y-m-d'));
		if ($date > strtotime($duedate) &&  $unpaidAmount !=0){
			$dateHtml = '<span class="recordviews-value invoice-date-overdue">'.$duedate.'<i class="fa fa-flag"></i></span>';
		}else{
			$dateHtml = '<span class="recordviews-value">'.$duedate.'</span>';
		}
		}else{
			$closedate = isset($value['closedate']) ? $value['closedate'] : '';
			$dateHtml = '<span class="recordviews-value">'.$closedate.'</span>';
		}
		return $dateHtml;
	}
	
	public function getColumnName($invoicestatus){
		if($invoicestatus == "A")
			return "<span>Due Date</span>";
		else
			return "<span>Close Date</span>";
	}
}