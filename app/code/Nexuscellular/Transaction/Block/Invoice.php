<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\soapApiCall as soapApiCall;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
use \Magento\Catalog\Api\ProductRepositoryInterface as productRepository;

class Invoice extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;
	protected $request;
	protected $soapApiCall;
	private $productRepository;
	protected $imageHelper;
	protected $productHelper; 
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	\Magento\Framework\App\Request\Http $request,
	restSuiteqlApi $restapiHelper,
	soapApiCall $soapApiCall,
	productRepository $productRepository,
	\Magento\Catalog\Helper\Image $imageHelper,
	\Magento\Catalog\Helper\Product $productHelper
	)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->request = $request;
		$this->restSuiteqlApi = $restapiHelper;
		$this->soapApiCall = $soapApiCall;
		$this->productRepository = $productRepository;
		$this->imageHelper = $imageHelper;
		$this->productHelper = $productHelper;
		parent::__construct($context);
	}

	public function getTransactionDetails()
	{	
		$transaction = [];
		$transaction['internal_id'] = $this->request->getParam('id');
		$recordtype = $this->request->getParam('type');
		//'salesorder','invoice','cashsale','creditmemo','customerpayment'
		if($recordtype == 'salesorder')
		$recordtype = 'salesOrder';
		else if($recordtype == 'invoice')
		$recordtype = 'invoice';
		else if($recordtype == 'cashsale')
		$recordtype = 'cashSale';
		else if($recordtype == 'creditmemo')
		$recordtype = 'creditMemo';
		else if($recordtype == 'customerpayment')
		$recordtype = 'customerPayment';

		$transaction['recordtype'] = $recordtype; 
		return  $this->apiHelper->getSoapTransactionDetails($transaction);
		
	}
	public function getProductBySku($sku){
    	return $this->productRepository->get($sku);
	}
	public function getProductImage($productObj){
		return $this->imageHelper
		->init($productObj,'cart_page_product_thumbnail')->constrainOnly(true)->keepAspectRatio(true)
		->keepFrame(false)->setImageFile($productObj->getImage())->getUrl();
	}
	public function countItems($renderItems){
		$count = 0;
		foreach($renderItems as $items){
			$count = $count + abs($items['quantity']);
		}
		return $count;
	}
	public function getSalesRepDetails($internalId){
		$record['recordtype'] = 'employee';
		$record['internal_id'] = $internalId;
		return  $this->soapApiCall->getRecordByInternalId($record);
	}
	public function getCustomFieldValue($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = $customList['value'];
				break;
			}
		}
		return $value;
	}
	public function getCustomSelectFieldValue($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = $customList['value']['name'];
				break;
			}
		}
		return $value;
	}
	public function getTrackingLink($shipment,$tracknumber){
		$shipment = strtolower($shipment);
		if(strpos($shipment, "fedex") !== false)
			$link = "http://www.fedex.com/Tracking?action=track&amp;tracknumbers=".$tracknumber;
		else if(strpos($shipment, "ups") !== false)
			$link = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=".$tracknumber;
		else
			$link = '';
		return $link;	
	}
	public function getShipMethodName($shipMethod){
		$shipMethod = strtolower($shipMethod);
		if(strpos($shipMethod, "fedex") !== false)
			$methodName = "Fedex";
		else if(strpos($shipMethod, "ups") !== false)
			$methodName = "UPS";
		else
			$methodName = $shipMethod;
		return 	$methodName;
	}
	public function getSku($sku){
		$sku = explode(':',$sku);
		if(count($sku) > 1)
		$sku = $sku[count($sku) - 1];
		else
		$sku = $sku[0];
		return $sku;
	}
}