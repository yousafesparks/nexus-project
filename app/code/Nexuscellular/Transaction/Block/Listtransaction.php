<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Listtransaction extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->restSuiteqlApi = $restapiHelper;
		parent::__construct($context);
	}

	/*public function getCustomerReturnList()
	{
		$items = [];
		$customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
		//$query = "{\n\t\"q\": \"SELECT tranid,status,trandate,foreigntotal,id FROM Transaction where recordtype = 'returnauthorization' AND entity='".$customerNetsuiteInternalId."' ORDER BY trandate DESC \"\n}";
		$query = "{\n\t\"q\": \"SELECT SUM(TransactionLine.quantity) as total_qty,transaction.id,tranid,trandate,foreigntotal,status FROM Transaction INNER JOIN TransactionLine ON ( TransactionLine.Transaction = Transaction.ID ) WHERE ( Transaction.Type = 'RtnAuth' ) AND ( Transaction.Voided = 'F' ) AND(TransactionLine.mainline = 'F') AND (TransactionLine.taxline='F') AND (Transaction.entity = '".$customerNetsuiteInternalId."')  GROUP BY Transaction.tranid,Transaction.trandate,Transaction.foreigntotal,Transaction.status,transaction.id ORDER BY transaction.trandate DESC \"\n}";
		
		$response =  $this->restSuiteqlApi->callRestApi($query);
		if(isset($response['count']) && $response['count'] > 0){
			$items = $response['items'];
		}
		return $items;
	}*/
}