<?php
namespace Nexuscellular\Transaction\Block;
class View extends \Magento\Framework\View\Element\Template
{
	protected $apiHelper;
	protected $request;
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Framework\App\Request\Http $request)
	{	
		$this->request = $request;
		parent::__construct($context);
	}

	public function getTransactionType()
	{
		$recordtype = $this->request->getParam('type');	
		return $recordtype; 
	}
}