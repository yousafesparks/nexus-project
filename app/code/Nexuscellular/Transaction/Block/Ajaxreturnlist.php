<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Ajaxreturnlist extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->restSuiteqlApi = $restapiHelper;
		parent::__construct($context);
	}
	public function getResponseData(){
		$response = $this->getResponse();
		$responseData = [];
		if(isset($response['count']) && $response['count'] > 0 ){
			$responseData = $response;
		}
		return $responseData;
	}
	public function arraySortByField($array){
		usort($array, function ($item1, $item2) {
			return $item1['position'] <=> $item2['position'];
		});
		return $array;
	}


}