<?php
namespace Nexuscellular\Transaction\Block;
use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\Data as apiHelper;
class Ajaxcaselist extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;

	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->restSuiteqlApi = $restapiHelper;
		parent::__construct($context);
	}
	public function getResponseData(){
		$response = $this->getResponse();
		$responseData = [];
		if(isset($response['count']) && $response['count'] > 0 ){
			$responseData = $response['items'];
		}
		return $responseData;
	}
	public function getStatus($statusId){
		$status = NULL;
		if($statusId == '1'){
			$status = 'Not Started';
		}elseif($statusId == '2'){
			$status = 'In Progress';
		}elseif($statusId == '3'){
			$status = 'Escalated';
		}elseif($statusId == '4'){
			$status = 'Re-opened';
		}elseif($statusId == '5'){
			$status = 'Closed';
		}
		return $status;
	}
}