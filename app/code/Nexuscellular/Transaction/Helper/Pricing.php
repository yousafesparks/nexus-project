<?php

namespace Nexuscellular\Transaction\Helper;

use Nexuscellular\Transaction\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\Transaction\Helper\SoapApiCall as soapApi;
class Pricing extends \Magento\Framework\App\Helper\AbstractHelper
{

	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $resourceConnection;
	protected $soapApi;
	protected $priceHelper;
	 public function __construct(restSuiteqlApi $apiHelper,
	 soapApi $soapApi,
	 \Magento\Customer\Model\Session $customerSession,
	 \Magento\Framework\App\ResourceConnection $resourceConnection,
	 \Magento\Framework\Pricing\Helper\Data $priceHelper){
		$this->restSuiteqlApi = $apiHelper;
		$this->soapApi = $soapApi;
		$this->customerSession = $customerSession;
		$this->resourceConnection = $resourceConnection;
		$this->priceHelper = $priceHelper;
	}
	public function currency($amount,$currency=1){
			return "$".number_format($amount,2,".",",");
	}
}
