<?php

namespace Nexuscellular\Transaction\Helper;
require_once(BP.'/var/netsuitelibrary/PHPToolkit_2020_1/samples/getSalesOrderDetails.php');
class SoapApiCall extends \Magento\Framework\App\Helper\AbstractHelper
{
	
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

	public function getSalesOrderByInternalId($data){
		$values = [];
		$orderType = (strtoLower($data['type']) == "cashsale") ? 'cashSale' : 'invoice';
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $data["transactionid"];
		$request->baseRef->type = $orderType;
		$getResponse = $service->get($request);
		
		if (!$getResponse->readResponse->status->isSuccess) {
			echo "GET ERROR";
		} else {
			$itemInternalId = $data['iteminternalid'];
			$response = json_decode(json_encode($getResponse->readResponse->record->itemList->item,true),true);
		//echo "<pre>";print_r();die();
			foreach($response as $item){
				if($item['item']['internalId'] == $itemInternalId){
					$values = ['rate'=>$item['rate'],'taxcode'=>$item['taxCode']['internalId'],'soap_trandate'=>$getResponse->readResponse->record->tranDate];
					break;
				}
			}
		}
		
		return $values;
	}
	public function getItemFulfillmentBySalesOrderId($transaction){
		$response = [];
		$service = new \NetSuiteService();
		$service->setSearchPreferences(false, 1000);
		
		$recordRef = new \RecordRef();
		$recordRef->internalId = $transaction['internalId'];//3144469;

		$SearchMultiSelectField = new \SearchMultiSelectField();
		$SearchMultiSelectField->operator = "anyOf";
		$SearchMultiSelectField->searchValue = $recordRef;
		
		// filter by type '_itemFulfillment'
		$SearchEnumMultiSelectField = new \SearchEnumMultiSelectField();
		$SearchEnumMultiSelectField->searchValue = Array('_itemFulfillment');
		$SearchEnumMultiSelectField->operator = 'anyOf';
		
		$TransactionSearchRow = new \TransactionSearchRow();
		$TransactionSearchBasic = new \TransactionSearchBasic();
		$TransactionSearchBasic->createdFrom = $SearchMultiSelectField;
		
		$TransactionSearchBasic->type = $SearchEnumMultiSelectField;
		$request = new \SearchRequest();
		$request->searchRecord = $TransactionSearchBasic;

		$searchResponse = $service->search($request);
		
		if ($searchResponse->searchResult->status->isSuccess) {
			if(!empty($searchResponse->searchResult->recordList->record)){
			$response = json_decode(json_encode($searchResponse->searchResult->recordList,true),true);
			}
		}
		//echo "<pre>";print_r($response);die();
		return $response;
	}
	
	public function getCustomerByInternalId($customerInternalId){
		$response = [];
		
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $customerInternalId;
		$request->baseRef->type = 'customer';
		$getResponse = $service->get($request);
		
		if (!$getResponse->readResponse->status->isSuccess) {
			echo "GET ERROR";
		} else {
			$response = json_decode(json_encode($getResponse->readResponse->record,true),true);	
		}
		return $response;
	}
	public function getItemSerialNumbers($data){
		$values = [];
		 $orderType = (strtoLower($data['type']) == "cashsale") ? 'cashSale' : 'invoice';
		
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $data["transactionid"];
		$request->baseRef->type = $orderType;
		$getResponse = $service->get($request);
		
		if (!$getResponse->readResponse->status->isSuccess) {
			echo "GET ERROR";
		} else {
			$itemInternalId = $data['iteminternalid'];
			$response = json_decode(json_encode($getResponse->readResponse->record->itemList->item,true),true);
		$Quantity = 0;
		$serialNumbers = [];
			foreach($response as $item){
				if($item['item']['internalId'] == $itemInternalId){
					$Quantity = $item['quantity'];
					$inventoryDetails = $item['inventoryDetail']['inventoryAssignmentList']['inventoryAssignment'];
					foreach($inventoryDetails as $invAssign){
						$serialNumbers[] = $invAssign['issueInventoryNumber']['name'];
					}
					break;
				}
			}
		}
		$finalItem = [];
		for($i=0;$i<$Quantity;$i++){
			$finalItem[$i] = $data;
			$finalItem[$i]['serialnumber'] = $serialNumbers[$i];
			$finalItem[$i]['quantity'] = 1;
			$finalItem[$i]['uniquekey'] = $data['tranid'].$data['sku'].$finalItem[$i]['serialnumber'];
		}
			
		return $finalItem;
	}
	public function getTransactionById($transaction){
		$items = [];
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $transaction['internal_id'];
		$request->baseRef->type = $transaction['recordtype'];
		$getResponse = $service->get($request);
		//print_r($getResponse);die();
		if ($getResponse->readResponse->status->isSuccess) {
			$items = json_decode(json_encode($getResponse->readResponse->record,true),true);
			$items['success']= true;
	   }else{
		   $items['error'] = true;
		   $items['error_message'] = $getResponse->readResponse->status->statusDetail[0]->message;
	   }
		return $items;
	}
	public function getRecordByInternalId($entity){
		$items = [];
		$service = new \NetSuiteService();
		$request = new \GetRequest();
		$request->baseRef = new \RecordRef();
		$request->baseRef->internalId = $entity['internal_id'];
		$request->baseRef->type = $entity['recordtype'];
		$getResponse = $service->get($request);
		//echo "<pre>";print_r($getResponse);die(); 
		if ($getResponse->readResponse->status->isSuccess) {
			 $items = json_decode(json_encode($getResponse->readResponse->record,true),true);
		}
		return $items;
	}
	public function searchMessagesByCaseId($entity){
		$items = [];
		$recordRef = new \RecordRef();
		$recordRef->internalId = $entity['internal_id'];
		$service = new \NetSuiteService();
		$SearchMultiSelectField = new \SearchMultiSelectField();
		$SearchMultiSelectField->searchValue = $recordRef;
		$SearchMultiSelectField->operator = "anyOf";

		$SupportCaseSearchBasic = new \SupportCaseSearchBasic();
		$SupportCaseSearchBasic->internalId = $SearchMultiSelectField;

		$messageSearch = new \MessageSearch();
		$messageSearch->caseJoin = $SupportCaseSearchBasic;

		$search = new \MessageSearchAdvanced();
		$search->criteria = $messageSearch;
		$request = new \SearchRequest();
		$request->searchRecord = $search;
		
		$searchResponse = $service->search($request);
		//echo "<pre>";print_r($searchResponse);die();
		if ($searchResponse->searchResult->status->isSuccess && $searchResponse->searchResult->totalRecords > 0) {
			$items = json_decode(json_encode($searchResponse->searchResult->recordList,true),true);
	   	}
	   return $items;
	}
	public function getCustomerSavedSearch($savedSearchId){
		$items = [];
		$service = new \NetSuiteService();
		$search = new \CustomerSearchAdvanced();
		$search->savedSearchId = $savedSearchId;

		$request = new \SearchRequest();
		$request->searchRecord = $search;
		$searchResponse = $service->search($request);
		if ($searchResponse->searchResult->status->isSuccess && $searchResponse->searchResult->totalRecords > 0) {
			$items = json_decode(json_encode($searchResponse->searchResult->searchRowList,true),true);
	   	}
	   return $items;
	}
}