<?php

namespace Nexuscellular\Netsuite\Model\ResourceModel\Productnotificaton;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'outofstocknotify_productnotificaton_collection';
	protected $_eventObject = 'outofstocknotify_productnotificaton_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Nexuscellular\Outofstocknotify\Model\Productnotificaton', 'Nexuscellular\Outofstocknotify\Model\ResourceModel\Productnotificaton');
	}
	
}