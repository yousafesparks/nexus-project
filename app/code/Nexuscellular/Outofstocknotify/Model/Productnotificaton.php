<?php

namespace Nexuscellular\Outofstocknotify\Model;

class Productnotificaton extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'outofstocknotify_productnotificaton';

	protected $_cacheTag = 'outofstocknotify_productnotificaton';

	protected $_eventPrefix = 'outofstocknotify_productnotificaton';

	protected function _construct()
	{
		$this->_init('Nexuscellular\Outofstocknotify\Model\ResourceModel\Productnotificaton');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}