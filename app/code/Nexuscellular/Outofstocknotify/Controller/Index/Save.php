<?php


namespace Nexuscellular\Outofstocknotify\Controller\Index;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $_date;
	/**
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;

    /**
	 * @var \Nexuscellular\Outofstocknotify\Model\ProductnotificatonFactory
	 */
    protected $productnotificatonFactory;
    
     /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    // Magento\Customer\Model\Session
    protected $customerSession;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Json\Helper\Data $jsonHelper,
    \Nexuscellular\Outofstocknotify\Model\ProductnotificatonFactory $outofstocknotifyProductFactory,
    CustomerSession $customerSession,
    \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
    ProductRepositoryInterface $productRepository
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->productnotificatonFactory =$outofstocknotifyProductFactory;
        $this->_date =  $date;
        $this->productRepository = $productRepository;
        $this->customerSession = $customerSession;
         parent::__construct($context);
    }

    
	
	 public function execute(){
        date_default_timezone_set('America/Toronto');
         $currentDate = date("Y-m-d H:i:s") ;
        //echo $this->_date->date()->format('Y-m-d H:i:s');
       // 
		$response= [];
		if (!$this->getRequest()->isPost()) {
			$response['success'] = false;
            $response['message'] = 'Please Enter valid fields';
            return $this->getResponse()->representJson(
                $this->jsonHelper->jsonEncode($response)
            );
        }
        
        try {
             $requested_qty = $this->getRequest()->getParam('product_qty');
             $notify_product_id = $this->getRequest()->getParam('product_id');
           
            if (!empty($notify_product_id)){
                $product = $this->productRepository->getById($notify_product_id);
                $model = $this->productnotificatonFactory->create();
                $model
                ->setCustomerEmail($this->customerSession->getCustomer()->getEmail())
                ->setProductId($notify_product_id)
                ->setProductName($product->getName())
                ->setProductSku($product->getSku())
                ->setQuantity($requested_qty)
                ->setStatus(1)
                ->setCreatedAt($currentDate)
                ->setUpdatedAt($currentDate);
                $model->save();
                $response['success'] = true;
				$response['message'] = 'save the data successfully';
            }else{
                $response['success'] = false;
			    $response['message'] = 'An error occurred while processing your form. Please try again later.';
            }
          
        } catch (\Exception $e) {
            $response['success'] = false;
			$response['message'] = 'An error occurred while processing your form. Please try again later.';
		}
		return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
		);
    }
	
}

