<?php
namespace Nexuscellular\ReturnAuthorization\Block;
use Nexuscellular\ReturnAuthorization\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\ReturnAuthorization\Helper\Data as apiHelper;
class Returnview extends \Magento\Framework\View\Element\Template
{
	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $apiHelper;
	protected $request;
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,
	\Magento\Customer\Model\Session $customerSession,
	apiHelper $apiHelper,
	\Magento\Framework\App\Request\Http $request,
	restSuiteqlApi $restapiHelper)
	{	
		$this->customerSession = $customerSession;
		$this->apiHelper = $apiHelper;
		$this->request = $request;
		$this->restSuiteqlApi = $restapiHelper;
		parent::__construct($context);
	}

	public function getCustomerReturnItem()
	{	$netsuiteReturnInternalId = $this->request->getParam('id');
		//$items = $this->apiHelper->getRestReturnByInternalId($netsuiteReturnInternalId);
		//echo "<pre>";print_r($items);die();
		return  $this->apiHelper->getSoapReturnDetails($netsuiteReturnInternalId);
		//return $items;
	}
	public function countItems($renderItems){
		//echo "<pre>";print_r($renderItems);die();
		$count = 0;
		foreach($renderItems as $items){
			$count = $count + abs($items['quantity']);
		}
		return $count;
	}
	public function getCustomFieldValue($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = $customList['value'];
				break;
			}
		}
		return $value;
	}
	public function getCustomSelectFieldValue($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = $customList['value']['name'];
				break;
			}
		}
		return $value;
	}
	public function getCustomSelectFieldId($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = $customList['value']['internalId'];
				break;
			}
		}
		return $value;
	}
	public function getOption($option){
		//echo "<pre>";print_r($option);die();
		$value = '';
		if(!empty($option) && isset($option['customField']) ){
			$value = [];
			foreach($option['customField'] as $opt){
				//echo "<pre>";print_r($opt);die();
				$value[] = $opt['value']['name']; 
			}
			if(is_array($value) && count($value))
				$value = implode(',',$value);
		}
		return $value;
	}
	public function getCustomDateFieldValue($fieldName,$customLists){
		$value = '';
		foreach($customLists as $customList){
			if($fieldName == $customList['scriptId']){
				$value = date('Y-m-d',strtotime($customList['value']));
				break;
			}
		}
		return $value;
	}
	public function getTransactionUrl($item){
		$value = '#';
		$string = $this->getCustomSelectFieldValue('custcol_f3_document_number',$item['customFieldList']['customField']);
		$id = $this->getCustomSelectFieldId('custcol_f3_document_number',$item['customFieldList']['customField']);
		if (strpos($string, '#') !== false) {
		$transtring = explode("#",$string);
		$value = $this->getUrl('transaction/index/view/id/'.$id.'/type/'.preg_replace('/\s+/', '',strtolower($transtring[0])));
		
		}
		return $value;
	}
	
}