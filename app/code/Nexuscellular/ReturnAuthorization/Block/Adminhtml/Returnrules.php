<?php
namespace Nexuscellular\ReturnAuthorization\Block\Adminhtml;

class Returnrules extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_returnrules';
		$this->_blockGroup = 'Nexuscellular_ReturnAuthorization';
		$this->_headerText = __('Return Rules');
		$this->_addButtonLabel = __('Create Return Rules');
		parent::_construct();
	}
}
