<?php
namespace Nexuscellular\ReturnAuthorization\Block\Adminhtml\Returnrules\Edit\Tab;
 
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
 
class Info extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
 
    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param Status $status
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }
 
    /**
     * Prepare form fields
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        /** @var $model \Magetop\Helloworld\Model\PostsFactory */
        $model = $this->_coreRegistry->registry('magetop_blog');
 
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('post_');
        $form->setFieldNameSuffix('post');
        // new filed
 
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );
       // var_dump($model->getId());die();
        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id']
            );
        }
        $fieldset->addField(
            'return_name',
            'text',
            [
                'name'      => 'return_name',
                'label'     => __('Return Name'),
                'title' => __('Return Name'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'netsuite_internal_id',
            'text',
            [
                'name'      => 'netsuite_internal_id',
                'label'     => __('Netsuite List Internal ID'),
                'title' => __('Netsuite List Internal ID'),
                'required' => true
            ]
        );
        
        $fieldset->addField(
            'order',
            'text',
            [
                'name'      => 'order',
                'label'     => __('Order'),
                'title' => __('Order'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_days_part',
            'text',
            [
                'name'      => 'max_days_part',
                'label'     => __('Max Days Part'),
                'title' => __('Max Days Part'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_days_accessories',
            'text',
            [
                'name'      => 'max_days_accessories',
                'label'     => __('Max Days Accessories'),
                'title' => __('Max Days Accessories'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_days_devices',
            'text',
            [
                'name'      => 'max_days_devices',
                'label'     => __('Max Days Devices'),
                'title' => __('Max Days Devices'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_3months_part',
            'text',
            [
                'name'      => 'max_3months_part',
                'label'     => __('Max 3  Months Part'),
                'title' => __('Max 3 Months Part'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_3months_accessroies',
            'text',
            [
                'name'      => 'max_3months_accessroies',
                'label'     => __('Max 3 Months Accessroies'),
                'title' => __('Max 3 Months Accessroies'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_3months_devices',
            'text',
            [
                'name'      => 'max_3months_devices',
                'label'     => __('Max 3 Months Devices'),
                'title' => __('Max 3 Months Devices'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_6months_part',
            'text',
            [
                'name'      => 'max_6months_part',
                'label'     => __('Max 6 Months Parts'),
                'title' => __('Max 6 Months Parts'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_6months_accessroies',
            'text',
            [
                'name'      => 'max_6months_accessroies',
                'label'     => __('Max 6 Months Accessories'),
                'title' => __('Max 6 Months Accessories'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'max_6months_devices',
            'text',
            [
                'name'      => 'max_6months_devices',
                'label'     => __('Max 6 Months Devices'),
                'title' => __('Max 6 Months Devices'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'parts_full_credit_days',
            'select',
            [
                'name'      => 'parts_full_credit_days',
                'label'     => __('Parts Full Credit Days'),
                'title' => __('Parts Full Credit Days'),
                'required' => true,
                'values' => $this->_getDays()
            ]
        );
        $fieldset->addField(
            'parts_lesser_credit_days',
            'select',
            [
                'name'      => 'parts_lesser_credit_days',
                'label'     => __('Parts Lesser Credit Days'),
                'title' => __('Parts Lesser Credit Days'),
                'required' => true,
                'values' => [
                    ['value' => 'Not Eligible', 'label' => 'Not Eligible'],
                    ['value' => 7, 'label' => 7],
                    ['value' => 9, 'label' => 9],
					['value' => 11, 'label' => 11],
                    ['value' => 15, 'label' =>15],
                    ['value' => 30, 'label' =>30],
                    ['value' => 45, 'label' => 45],
                    ['value' => 60, 'label' =>60],
                    ['value' => 90, 'label' => 90],
                    ['value' => 120, 'label' =>120],
                    ['value' => 180, 'label' => 180],
                    ['value' => 365, 'label' =>365],
                    ['value' => 'lifetime', 'label' => 'lifetime']
                ]
            ]
        );
        $fieldset->addField(
            'accessories_full_credit_days',
            'select',
            [
                'name'      => 'accessories_full_credit_days',
                'label'     => __('Accessories Full Credit Days'),
                'title' => __('Accessories Full Credit Days'),
                'required' => true,
                'values' => $this->_getDays()
            ]
        );
        $fieldset->addField(
            'accessories_lesser_credit_days',
            'select',
            [
                'name'      => 'accessories_lesser_credit_days',
                'label'     => __('Accessories Lesser Credit Days'),
                'title' => __('Accessories Lesser Credit Days'),
                'required' => true,
                'values' => [
                    ['value' => 'Not Eligible', 'label' => 'Not Eligible'],
                    ['value' => 7, 'label' => 7],
                    ['value' => 9, 'label' => 9],
					['value' => 11, 'label' => 11],
                    ['value' => 15, 'label' =>15],
                    ['value' => 30, 'label' =>30],
                    ['value' => 45, 'label' => 45],
                    ['value' => 60, 'label' =>60],
                    ['value' => 90, 'label' => 90],
                    ['value' => 120, 'label' =>120],
                    ['value' => 180, 'label' => 180],
                    ['value' => 365, 'label' =>365],
                    ['value' => 'lifetime', 'label' => 'lifetime']
                ]
            ]
        );
        $fieldset->addField(
            'devices_full_credit_days',
            'select',
            [
                'name'      => 'devices_full_credit_days',
                'label'     => __('Devices Full Credit Days'),
                'title' => __('Devices Full Credit Days'),
                'required' => true,
                'values' => $this->_getDays()
            ]
        );
        $fieldset->addField(
            'devices_lesser_credit_days',
            'select',
            [
                'name'      => 'devices_lesser_credit_days',
                'label'     => __('Devices Lesser Credit Days'),
                'title' => __('Devices Lesser Credit Days'),
                'required' => true,
                'values' => [
                    ['value' => 'Not Eligible', 'label' => 'Not Eligible'],
                    ['value' => 7, 'label' => 7],
                    ['value' => 9, 'label' => 9],
					['value' => 11, 'label' => 11],
                    ['value' => 15, 'label' =>15],
                    ['value' => 30, 'label' =>30],
                    ['value' => 45, 'label' => 45],
                    ['value' => 60, 'label' =>60],
                    ['value' => 90, 'label' => 90],
                    ['value' => 120, 'label' =>120],
                    ['value' => 180, 'label' => 180],
                    ['value' => 365, 'label' =>365],
                    ['value' => 'lifetime', 'label' => 'lifetime']
                ]
            ]
        );
        $data = $model->getData();
        $form->setValues($data);
        $this->setForm($form);
 
        return parent::_prepareForm();
    }
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Return Rules Info');
    }
    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Return Rules Info');
    }
 
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
 
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
	
    /**
     * @return array
     */
	protected function _getDays(){
		return [
                    ['value' => 'Not Eligible', 'label' => 'Not Eligible'],
                    ['value' => 7, 'label' => 7],
                    ['value' => 9, 'label' => 9],
					['value' => 11, 'label' => 11],
                    ['value' => 15, 'label' =>15],
                    ['value' => 30, 'label' =>30],
                    ['value' => 45, 'label' => 45],
                    ['value' => 60, 'label' =>60],
                    ['value' => 90, 'label' => 90],
                    ['value' => 120, 'label' =>120],
                    ['value' => 180, 'label' => 180],
                    ['value' => 365, 'label' =>365]
                ];
	}
}