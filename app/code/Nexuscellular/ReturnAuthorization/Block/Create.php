<?php
namespace Nexuscellular\ReturnAuthorization\Block;
class Create extends \Magento\Framework\View\Element\Template
{
	public function __construct(\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
	}
	/*
	* string | null returns tranaction url
	*/
	public function getTransactionUrl($transaction){
		$url = '';
		if (strpos($transaction['tranid'], 'INV') !== false) {
					$url = $this->getUrl('transaction/index/view/id/'.$transaction['transaction_internal_id'].'/type/invoice/') ;
		}else if(strpos($transaction['tranid'], 'CSH') !== false){
			$url = $this->getUrl('transaction/index/view/id/'.$transaction['transaction_internal_id'].'/type/cashsale/') ;
		}
		return $url;
	}
	
}