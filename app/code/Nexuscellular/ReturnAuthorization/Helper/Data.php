<?php

namespace Nexuscellular\ReturnAuthorization\Helper;

use Nexuscellular\ReturnAuthorization\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\ReturnAuthorization\Helper\SoapApiCall as soapApi;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	const NETSUITE_CATEGORY_TYPE_PARTS = 1; // internal id of parts which is category type 
	const NETSUITE_CATEGORY_TYPE_ACCESSORIES = 2;
	const NETSUITE_CATEGORY_TYPE_DEVICES = 3;

	protected $restSuiteqlApi; 
	protected $customerSession;
	protected $resourceConnection;
	protected $soapApi;
	protected $priceHelper;
	 public function __construct(restSuiteqlApi $apiHelper,
	 soapApi $soapApi,
	 \Magento\Customer\Model\Session $customerSession,
	 \Magento\Framework\App\ResourceConnection $resourceConnection,
	 \Magento\Framework\Pricing\Helper\Data $priceHelper){
		$this->restSuiteqlApi = $apiHelper;
		$this->soapApi = $soapApi;
		$this->customerSession = $customerSession;
		$this->resourceConnection = $resourceConnection;
		$this->priceHelper = $priceHelper;
	}
	public function getOrderDetails($data){
		return $this->soapApi->getSalesOrderByInternalId($data);
	}
	public function getSoapReturnDetails($internalId){
		return $this->soapApi->getReturnAuthorizationById($internalId);
	}
	public function getRestReturnByInternalId($internalId){
		$query = "{\n\t\"q\": \"SELECT TransactionLine.*,Transaction.status FROM TransactionLine INNER JOIN Transaction ON ( Transaction.ID = TransactionLine.Transaction AND Transaction.id =  ".$internalId.")\"\n}";
		return $this->restSuiteqlApi->callRestApi($query);
	}
	protected function _getAndCondition(){
		
		$six_months_customer = $this->customerSession->getCustomer()->getData('custentity_f3_special_customer');
		$three_months_customer = $this->customerSession->getCustomer()->getData('custentity_f3_three_warranty');
		$regularCustomer = true;

		// Below code check the customer is eligible days to return product based on customer attributes
		if($six_months_customer){
			$searchDate = $this->_getMaxDaysOfParts('max_6months_part');
			$andCondition = " and Transaction.TranDate >= '".$searchDate['from']."' and Transaction.TranDate <= '".$searchDate['to']."'";
		}else if($three_months_customer){
			$searchDate = $this->_getMaxDaysOfParts('max_3months_part');
			$andCondition = " and Transaction.TranDate >= '".$searchDate['from']."' and Transaction.TranDate <= '".$searchDate['to']."'";
		}else{
			$searchDate = [];
			$andCondition = '';
		}
		return $andCondition;
	}
	public function searchOrder($plainString){
		$reversequeryString = '';
		$stringData = $plainString;
	
		$array = explode(" ",$stringData);
		if(count($array)>1){
			$string = preg_replace('/\s+/', '%', $stringData);
			$revs = $this->_reverseString($stringData);
			$reverstring = preg_replace('/\s+/', '%', $revs);
			$reversequeryString = "%".($reverstring)."%";
			$queryString = "%".($string)."%";
		}else{
			$queryString = "%".($stringData)."%";
		}
		$andCondition =  $this->_getAndCondition();
		$customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
		try{
			if(!empty($reversequeryString)){
				$query = "{\n\t\"q\": \"SELECT TransactionLine.itemtype as itemtype,Itemprice.price as actual_selling_price,Itemprice.pricelevelname,Item.pricinggroup,Item.itemid as sku,Item.isserialitem,Item.ID as iteminternalid,Item.custitem_f3_category_type,Item.description,TransactionLine.Transaction AS TransactionID,Transaction.employee as salesrep,TransactionLine.custcol_f3_document_number as documentnumber,Transaction.TranDate,Transaction.custbody_es_customer_inv_number,Transaction.Type,Transaction.TranID,Transaction.Entity as customer,TransactionLine.Rate,TransactionLine.quantity,TransactionLine.NetAmount FROM TransactionLine INNER JOIN Transaction ON ( Transaction.ID = TransactionLine.Transaction AND Transaction.Entity =  ".$customerNetsuiteInternalId." and (transaction.recordtype = 'invoice' or transaction.recordtype = 'cashsale') AND TransactionLine.memo != 'Cost of Sales' ".$andCondition.") INNER JOIN Itemprice ON(TransactionLine.Item = Itemprice.item) INNER JOIN Item ON ( Item.ID = TransactionLine.Item AND (LOWER(Item.description) LIKE LOWER('".$reversequeryString."') or  UPPER(Item.description) LIKE UPPER('".$reversequeryString."') or  LOWER(Item.description) LIKE LOWER('".$queryString."') or  UPPER(Item.description) LIKE UPPER('".$queryString."') OR LOWER(Item.itemid) = LOWER('".$plainString."') OR UPPER(Item.itemid) = UPPER('".$plainString."') OR Transaction.TranID = '".$plainString."' OR Transaction.custbody_es_customer_inv_number = '".$plainString."')) ORDER BY trandate DESC\"\n}";
			}else{
				$query = "{\n\t\"q\": \"SELECT TransactionLine.itemtype as itemtype,Itemprice.price as actual_selling_price,Itemprice.pricelevelname,Item.pricinggroup,Item.itemid as sku,Item.isserialitem,Item.ID as iteminternalid,Item.custitem_f3_category_type,Item.description,TransactionLine.Transaction AS TransactionID,Transaction.employee as salesrep,TransactionLine.custcol_f3_document_number as documentnumber,Transaction.TranDate,Transaction.custbody_es_customer_inv_number,Transaction.Type,Transaction.TranID,Transaction.Entity as customer,TransactionLine.Rate,TransactionLine.quantity,TransactionLine.NetAmount FROM TransactionLine INNER JOIN Transaction ON ( Transaction.ID = TransactionLine.Transaction AND Transaction.Entity =  ".$customerNetsuiteInternalId." and (transaction.recordtype = 'invoice' or transaction.recordtype = 'cashsale')  AND TransactionLine.memo != 'Cost of Sales' ".$andCondition." ) INNER JOIN Itemprice ON(TransactionLine.Item = Itemprice.item ) INNER JOIN Item ON ( Item.ID = TransactionLine.Item AND (LOWER(Item.description) LIKE LOWER('".$queryString."') or  UPPER(Item.description) LIKE UPPER('".$queryString."') OR LOWER(Item.itemid) = LOWER('".$plainString."') OR UPPER(Item.itemid) = UPPER('".$plainString."') OR Transaction.TranID = '".$plainString."' OR Transaction.custbody_es_customer_inv_number = '".$plainString."')) ORDER BY trandate DESC\"\n}";
			}
			$response = $this->restSuiteqlApi->callRestApi($query);
			//echo "<pre>";print_r($response);die();
			return $this->_FilterData($response);
			
		}catch(Exception $e){
			///echo $e->getMessage();die();
		}
	}
/* 	protected function _FilterData($response){
		$newItemsArray = [];
		if(isset($response['items'])){
			foreach($response['items'] as $item){
				$newItem = $this->_checkCustomerReturnAndWorksheetData($item);
				if(!empty($newItem)){
					$newItemsArray[] = $newItem; 
				}
			}
		}	
		return $this->_FilterDataByReturnRules($newItemsArray);
	} */
	protected function matchGroupPricingItems($item){
		$groupPrices = $this->customerSession->getCustomerGroupPricing();
		$bool = false;
			if(count($groupPrices) > 0){
			foreach($groupPrices as $price){
				if($price['group']['internalId'] == $item['pricinggroup'] && $price['level']['name'] == $item['pricelevelname']){
					$bool = true;
					break;
				}
				else if($price['group']['internalId'] == $item['pricinggroup'] && $item['pricelevelname'] == 'Actual Price'){
					$bool = true;
					break;
				}
			}
			}else if($item['pricelevelname'] == 'Actual Price'){
				return true;
			}
			if(!$bool && $item['pricelevelname'] == 'Actual Price'){
				$bool = true;
			}
			return $bool;
	}
	protected function _FilterData($response){
		$newItemsArray = [];
		if(isset($response['items'])){
			foreach($response['items'] as $item){
		if($item['itemtype'] != 'TaxGroup' && $item['itemtype'] != 'ShipItem'){
				if(!$this->matchGroupPricingItems($item))
					continue;
				$newItem = $this->_checkCustomerReturnAndWorksheetData($item);
				if(!empty($newItem)){
					$newItemsArray[] = $newItem; 
				}
			}// condition check itemtype IS NOT ShipItem &
			}
		}	
		
		return $this->_FilterDataByReturnRules($newItemsArray);
	}
	protected function _getMaxDaysOfParts($fieldName){
		date_default_timezone_set('America/Toronto');
		$searchDate = [];
		 $tableName = $this->_getTableName('netsuite_return_rules');
		 //$fieldName = 'max_days_part';
		$query = "select Max($fieldName) FROM " . $tableName;
		$results = $this->resourceConnection->getConnection()->fetchAll($query);
		if(count($results) > 0){
			$value = array_values($results[0]);
			if(is_numeric($value[0]) && !empty($value[0])){
				$searchDate['from'] = date('Y-m-d',strtotime("-".$value[0]." days"));
				$searchDate['to'] = date('Y-m-d');
			}
		}
		// If column or reocrd data not found from 'netsuite_return_rules' mysql table then it will get default values from below code
		if($fieldName == 'max_6months_part' && !count($searchDate)){
			$searchDate['from'] = date('Y-m-d',strtotime("-180 days"));
			$searchDate['to'] = date('Y-m-d');
		}else if($fieldName == 'max_3months_part' && !count($searchDate)){
			$searchDate['from'] = date('Y-m-d',strtotime("-90 days"));
			$searchDate['to'] = date('Y-m-d');
		}
		return $searchDate;
	}
	protected function _FilterDataByReturnRules($newItemsArray){
		$uniqueItemforSerialize=[];
		$items = [];
		if(is_array($newItemsArray) && count($newItemsArray) > 0){
			$tableName = $this->_getTableName('netsuite_return_rules');
			foreach($newItemsArray as $item){
				$tranidAndSku = $item['tranid'].$item['sku'];
				$fieldName = $this->getFieldnameFromReturnTable($item['custitem_f3_category_type']);
				$query = "select id,return_name,".$fieldName." as days_to_return FROM " . $tableName;
				$results = $this->resourceConnection->getConnection()->fetchAll($query);
				$reasonsList = $this->_filterReasonsToShow($results,$item['trandate']);
				if(isset($item['quantity']))
				$item['quantity'] = abs($item['quantity']);
				if(count($reasonsList) > 0){
					$item['reason_lists'] = $reasonsList;
					if(isset($item['isserialitem']) && $item['isserialitem'] == 'T'){
						if(!in_array($tranidAndSku,$uniqueItemforSerialize)){// STOP duplicate item show at fron if same sku comes more than one time in same TRANID
							$serialItemList = $this->_checkIsSerializedItem($item);
							foreach($serialItemList as $serialItem){
								if($this->filterSerialNumberByWorksheetResponse($serialItem))
									$items[] = $serialItem;
							}
							$uniqueItemforSerialize[] = $tranidAndSku;
						}
					}else{
						$item['uniquekey'] = $item['tranid'].$item['sku'];
						$items[] = $item;
					}
				}
			}
		}
		return $items;
	}
	/*
	* return array
	* After click submit it checks all data validation with return rules and items in netsuite 
	*/
	protected function _FilterDataSubmitRAByReturnRules($newItemsArray){
		$uniqueItemforSerialize=[];
		$items = [];
		if(is_array($newItemsArray) && count($newItemsArray) > 0){
			$tableName = $this->_getTableName('netsuite_return_rules');
			foreach($newItemsArray as $item){
				$tranidAndSku = $item['tranid'].$item['sku'];
				$fieldName = $this->getFieldnameFromReturnTable($item['custitem_f3_category_type']);
				$query = "select id,return_name,".$fieldName." as days_to_return FROM " . $tableName;
				$results = $this->resourceConnection->getConnection()->fetchAll($query);
				$reasonsList = $this->_filterReasonsToShow($results,$item['trandate']);
				if(isset($item['quantity']))
				$item['quantity'] = abs($item['quantity']);
				if(count($reasonsList) > 0){
					$item['reason_lists'] = $reasonsList;
					if(isset($item['isserialitem']) && $item['isserialitem'] == 'T'){
						if(!in_array($tranidAndSku,$uniqueItemforSerialize)){ // STOP duplicate item show at fron if same sku comes more than one time in same TRANID  
							$serialItemList = $this->_checkIsSerializedItem($item);
							foreach($serialItemList as $serialItem){
									$items[] = $serialItem;
							}
						}
					}else{
						$item['uniquekey'] = $item['tranid'].$item['sku'];
						$items[] = $item;
					}
				}
			}
		}
		return $items;
	}
	protected function filterSerialNumberByWorksheetResponse($item){
		$bool = true;
		if(is_array($item)){
			$workSheetData = $this->getCustomerWorkSheetRecords();
			if(count($workSheetData) > 0){
				foreach($workSheetData as $sheetData){
					if(isset($item['serialnumber']) && $sheetData['transaction_internal_id'] == $item['transactionid'] &&  $sheetData['serialnumber'] == $item['serialnumber'] &&  $sheetData['item_internal_id'] == $item['iteminternalid']){
						$bool = false;
						break;
					}
				}
			}
		}
		return $bool;
	} 
	protected function _checkIsSerializedItem($item){
		return $this->soapApi->getItemSerialNumbers($item);
	}
	protected function _filterReasonsToShow($results,$transactionDate){
		//$transactionDate = '2020-8-13';
		date_default_timezone_set('America/Toronto');
		$earlier = new \DateTime($transactionDate);
		$later = new \DateTime(date('Y-m-d'));
		$diffDays = $later->diff($earlier)->format("%a");
		//echo "<pre>";var_dump(!empty($diffDays));die();
		foreach($results as $index => $result){
			if($result['days_to_return'] != 'Lifetime'){
				if(!empty($result['days_to_return']) && is_numeric($result['days_to_return']) && $result['days_to_return'] < $diffDays){
					unset($results[$index]);
				}else if($result['days_to_return'] <= 0){
					unset($results[$index]);
				}
			}
		}
		return $results;
	}
	protected function _reverseString($reverstring){
		// break the string up into words 
		$words = explode(' ',$reverstring); 
		// reverse the array of words
		$words = array_reverse($words); 
		// rebuild the string
		$s = join(' ',$words); 
		return $s;
	}
	protected function _checkCustomerReturnAndWorksheetData($item){
		$customerReturnData = $this->customerSession->getCustomerReturnData();
		//echo "<pre>";print_r($customerReturnData);die();
		if(is_array($customerReturnData) && count($customerReturnData)){
			foreach($customerReturnData as $return){
				if(isset($return['custcol_f3_document_number']) && $return['custcol_f3_document_number'] == $item['transactionid'] &&  $return['iteminternalid'] == $item['iteminternalid']){
					$item['quantity'] = abs($item['quantity']) - abs($return['quantity']);
					if($item['quantity'] <= 0)
						$item ='';
					break;
				}
			}
		}
		if(is_array($item)){
			$workSheetData = $this->getCustomerWorkSheetRecords();
			if(count($workSheetData) > 0){
				foreach($workSheetData as $sheetData){
					if($sheetData['transaction_internal_id'] == $item['transactionid'] &&  $sheetData['item_internal_id'] == $item['iteminternalid']){
						$item['quantity'] = abs($item['quantity']) - $sheetData['qty'];
						if($item['quantity'] <= 0)
							$item ='';
						break;
					}
				}
			}
		}
		return $item;
	}
	/*
	*@return {string}
	*/
	protected function _getTablename($tableName){
        /* Create Connection */
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
	}
	protected function getFieldnameFromReturnTable($netsuiteCategoryType){
		return $this->getReturnTableFieldName($netsuiteCategoryType);
	}
	protected function getReturnTableFieldName($netsuiteCategoryType){
		$customerType = $this->getCustomerType();
		if($netsuiteCategoryType== self::NETSUITE_CATEGORY_TYPE_PARTS) // For Parts
		{
			if($customerType == 6)
				$fieldName = 'max_6months_part';
			else if($customerType == 3)
				$fieldName = 'max_3months_part';
			else
				$fieldName = 'max_days_part';
		}
		else if($netsuiteCategoryType == self::NETSUITE_CATEGORY_TYPE_ACCESSORIES) // For Accessories
		{
			if($customerType == 6)
				$fieldName = 'max_6months_accessroies';
			else if($customerType == 3)
				$fieldName = 'max_3months_accessroies';
			else
				$fieldName = 'max_days_accessories';
		}
		else if($netsuiteCategoryType == self::NETSUITE_CATEGORY_TYPE_DEVICES) // For Devices
		{
			if($customerType == 6)
				$fieldName = 'max_6months_devices';
			else if($customerType == 3)
				$fieldName = 'max_3months_devices';
			else
				$fieldName = 'max_days_devices';
		}
		return $fieldName;
	}
	protected function getCustomerType(){
		$six_months_customer = $this->customerSession->getCustomer()->getData('custentity_f3_special_customer');
		$three_months_customer = $this->customerSession->getCustomer()->getData('custentity_f3_three_warranty');
		$regularCustomer = true;
		if($six_months_customer){
			return 6;
		}else if($three_months_customer){
			return 3;
		}else{
			return 0;
		}
	}
	public function getCustomerId(){
		return $this->customerSession->getCustomer()->getId();
	}
	public function getWorkSheetData(){
		$items = [];
		$tableName = $this->_getTableName('netsuite_ra_worksheet');
		$rulestableName = $this->_getTableName('netsuite_return_rules');
				$query = "select netsuite_return_rules.id as rules_id,return_name,netsuite_ra_worksheet.* FROM " . $tableName . " INNER JOIN netsuite_return_rules ON(netsuite_ra_worksheet.reason_id = ".$rulestableName.".id AND netsuite_ra_worksheet.customer_id = ".$this->getCustomerId().")";
				$results = $this->resourceConnection->getConnection()->fetchAll($query);
				
				if(count($results)>0){
					foreach($results as $sheet){
						$total_credit = $sheet['actual_unit_credit_price'] * $sheet['qty'];
						$total_credit_format = $this->priceHelper->currency(number_format($total_credit,2),true,false);
						//$unit_credit = (($sheet['purchase_price'] < $sheet['sell_price']) ? $sheet['purchase_price'] : $sheet['sell_price']);
						$unit_credit = $sheet['actual_unit_credit_price'];
						$sheet['total_credit_format'] =  $total_credit_format;
						$sheet['unit_credit_format'] =  $this->priceHelper->currency(number_format($unit_credit,2),true,false);
						$sheet['purchased_price_format'] =  $this->priceHelper->currency(number_format($sheet['purchase_price'],2),true,false);
						$sheet['uniquekey'] = $sheet['tranid'].$sheet['product_sku'].$sheet['serialnumber'];
						$items[] =$sheet; 
					}
				}
				
				return $items;
	}
	public function getCustomerWorkSheetRecords(){
		$items = [];
		$tableName = $this->_getTableName('netsuite_ra_worksheet');
				$query = "select * FROM " . $tableName . " WHERE customer_id = ".$this->getCustomerId();
				$results = $this->resourceConnection->getConnection()->fetchAll($query);
				if(count($results)>0){
					foreach($results as $sheet){
						$total_credit = $sheet['actual_unit_credit_price'] * $sheet['qty'];
						$total_credit_format = $this->priceHelper->currency(number_format($total_credit,2),true,false);
						//$unit_credit = (($sheet['purchase_price'] < $sheet['sell_price']) ? $sheet['purchase_price'] : $sheet['sell_price']);
						$unit_credit = $sheet['actual_unit_credit_price'];
						$sheet['total_credit_format'] =  $total_credit_format;
						$sheet['unit_credit_format'] =  $this->priceHelper->currency(number_format($unit_credit,2),true,false);
						$sheet['purchased_price_format'] =  $this->priceHelper->currency(number_format($sheet['purchase_price'],2),true,false);
						$sheet['uniquekey'] = $sheet['tranid'].$sheet['product_sku'].$sheet['serialnumber'];
						$items[] =$sheet; 
					}
				}

				return $items;
	}
	/* public function getOneOrderRecord($record){
		$customerNetsuiteInternalId = $this->getNetsuiteCustomerId();
		$andCondition = $this->_getAndCondition();
			$query = "{\n\t\"q\": \"SELECT TransactionLine.itemtype as itemtype,Itemprice.price as actual_selling_price,Itemprice.pricelevelname,Item.pricinggroup,isserialitem,Item.itemid as sku,Item.ID as iteminternalid,Item.custitem_f3_category_type,Item.description,TransactionLine.Transaction AS TransactionID,Transaction.employee as salesrep,TransactionLine.custcol_f3_document_number as documentnumber,Transaction.TranDate,Transaction.custbody_es_customer_inv_number,Transaction.Type,Transaction.TranID,Transaction.Entity as customer,TransactionLine.Rate,TransactionLine.quantity,TransactionLine.NetAmount FROM TransactionLine INNER JOIN Transaction ON ( Transaction.ID = TransactionLine.Transaction AND Transaction.Entity =  ".$customerNetsuiteInternalId." and (transaction.recordtype = 'invoice' or transaction.recordtype = 'cashsale')  AND TransactionLine.transaction = ".$record['transaction_internal_id']." and TransactionLine.memo != 'Cost of Sales' ".$andCondition." ) INNER JOIN Itemprice ON(TransactionLine.Item = Itemprice.item) INNER JOIN Item ON ( Item.ID = TransactionLine.Item AND (Item.itemid = '".$record['product_sku']."')) ORDER BY trandate DESC\"\n}";
		$response = $this->restSuiteqlApi->callRestApi($query);
		
		$newItemsArray = [];
		if(isset($response['items'])){
			$tableName = $this->_getTableName('netsuite_ra_worksheet');
			$results = $this->resourceConnection->getConnection()->delete(
						$tableName,
						['id = ?' => $record['id']]
						);
		  if($results == 1){
			foreach($response['items'] as $item){
				$newItem = $this->_checkCustomerReturnAndWorksheetData($item);
				
				if(!empty($newItem)){
					$newItemsArray[] = $newItem; 
				}
			}
		  }
		}
		return $this->_FilterDataByReturnRules($newItemsArray);
	} */
	public function getOneOrderRecord($record){
		$customerNetsuiteInternalId = $this->getNetsuiteCustomerId();
		$andCondition = $this->_getAndCondition();
			$query = "{\n\t\"q\": \"SELECT TransactionLine.itemtype as itemtype,Itemprice.price as actual_selling_price,Itemprice.pricelevelname,Item.pricinggroup,isserialitem,Item.itemid as sku,Item.ID as iteminternalid,Item.custitem_f3_category_type,Item.description,TransactionLine.Transaction AS TransactionID,Transaction.employee as salesrep,TransactionLine.custcol_f3_document_number as documentnumber,Transaction.TranDate,Transaction.custbody_es_customer_inv_number,Transaction.Type,Transaction.TranID,Transaction.Entity as customer,TransactionLine.Rate,TransactionLine.quantity,TransactionLine.NetAmount FROM TransactionLine INNER JOIN Transaction ON ( Transaction.ID = TransactionLine.Transaction AND Transaction.Entity =  ".$customerNetsuiteInternalId." and (transaction.recordtype = 'invoice' or transaction.recordtype = 'cashsale')  AND TransactionLine.transaction = ".$record['transaction_internal_id']." and TransactionLine.memo != 'Cost of Sales' ".$andCondition." ) INNER JOIN Itemprice ON(TransactionLine.Item = Itemprice.item) INNER JOIN Item ON ( Item.ID = TransactionLine.Item AND (Item.itemid = '".$record['product_sku']."')) ORDER BY trandate DESC\"\n}";
		$response = $this->restSuiteqlApi->callRestApi($query);
		
		$responseDataArray = [];
		$newItemsArray = [];
		if(isset($response['items'])){
			$tableName = $this->_getTableName('netsuite_ra_worksheet');
			$results = $this->resourceConnection->getConnection()->delete(
						$tableName,
						['id = ?' => $record['id']]
						);		
		  if($results == 1){ 
			$responseDataArray = $this->_FilterData($response);
		  }
			
		}
		return $responseDataArray;
	}
	public function getNetsuiteCustomerId(){
		return  $this->customerSession->getCustomer()->getData('netsuite_internal_id');
	}
	public function getReturnRulesData($reasonId){
		$tableName = $this->_getTableName('netsuite_return_rules');
		$query = "select netsuite_internal_id FROM " . $tableName . " WHERE id = ".$reasonId;
		$results = $this->resourceConnection->getConnection()->fetchAll($query);
	
		return $results;
	}
	public function getActualCreditPrice($data){
		$items = [];
		if(count($data) > 0){
			$tableName = $this->_getTableName('netsuite_return_rules');
				$fieldNames = $this->validateDaysForPrice($data['custitem_f3_category_type']);
				$query = "select id,return_name,".$fieldNames." FROM " . $tableName;
				$results = $this->resourceConnection->getConnection()->fetchAll($query);
			
				 $price = $this->_getCreditPrice($results,$data);
				return $price;
		}
	}
	protected function validateDaysForPrice($netsuiteCategoryType){
		
		if($netsuiteCategoryType== self::NETSUITE_CATEGORY_TYPE_PARTS){ // For Parts
			$fieldNames = 'parts_full_credit_days as full_credit,parts_lesser_credit_days as lesser_credit';
		}
		else if($netsuiteCategoryType == self::NETSUITE_CATEGORY_TYPE_ACCESSORIES){ // For Accessories and Tools
			$fieldNames = 'accessories_full_credit_days as full_credit,accessories_lesser_credit_days as lesser_credit';
		}
		else{// For Devices
			$fieldNames = 'devices_full_credit_days as full_credit,devices_lesser_credit_days as lesser_credit';
		}
		return $fieldNames;
	}
	protected function _getCreditPrice($results,$data){
		//echo "<pre>";print_r($results);print_r($data);die();
		$transactionDate = $data['trandate'];
		//$transactionDate = '2020-12-30';
		date_default_timezone_set('America/Toronto');
		$earlier = new \DateTime($transactionDate);
		$later = new \DateTime(date('Y-m-d'));
		$diffDays = $later->diff($earlier)->format("%a");//die();
		$price = "Not Available";
		foreach($results as $index => $result){
			
			if((is_numeric($result['full_credit']) && $result['id'] == $data['reason_id'] && $result['full_credit'] >= $diffDays && $result['full_credit'] != "Not Eligible") || ($result['full_credit'] == "lifetime" && $result['id'] == $data['reason_id'])){
				$price = $data['rate'];
				break;
			}
			if((is_numeric($result['lesser_credit']) && $result['id'] == $data['reason_id'] && $result['lesser_credit']>= $diffDays && $result['lesser_credit'] != "Not Eligible") || ($result['lesser_credit'] == "lifetime" && $result['id'] == $data['reason_id']) ){
				if($data['rate'] < $data['actual_selling_price']){
					$price = $data['rate'];
				}else{
					$price = $data['actual_selling_price'];
				}
				break;
			}
		}//die();
		return $price;
	}
	public function checkAllReturnRules(){
		$customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
		$responseData = [];
		$workSheetData = $this->getWorkSheetData();
	
		$searchItemInternalId = [];
		$searchTranInternalId = [];
		if(count($workSheetData)>0){
			foreach($workSheetData as $sheet){
				$searchItemInternalId[] = "'".$sheet['item_internal_id']."'";
				$searchTranInternalId[] = "'".$sheet['transaction_internal_id']."'";
			}
			 $itemIds = implode(',',array_unique($searchItemInternalId));
			 $tranIds = implode(',',array_unique($searchTranInternalId));
			$andCondition =  $this->_getAndCondition();
			 /* $query = "{\n\t\"q\": \"SELECT Itemprice.price as actual_selling_price,Item.itemid as sku,Item.isserialitem,Item.ID as iteminternalid,Item.custitem_f3_category_type,Item.description,TransactionLine.Transaction AS TransactionID,Transaction.employee as salesrep,TransactionLine.custcol_f3_document_number as documentnumber,Transaction.TranDate,Transaction.custbody_es_customer_inv_number,Transaction.Type,Transaction.TranID,Transaction.Entity as customer,TransactionLine.Rate,TransactionLine.quantity,TransactionLine.NetAmount FROM TransactionLine INNER JOIN Transaction ON ( Transaction.ID = TransactionLine.Transaction AND  Transaction.Entity =  ".$customerNetsuiteInternalId." and (transaction.recordtype = 'invoice' or transaction.recordtype = 'cashsale') AND TransactionLine.memo != 'Cost of Sales' AND TransactionLine.itemtype != 'ShipItem' ".$andCondition." ) INNER JOIN Itemprice ON(TransactionLine.Item = Itemprice.item AND Itemprice.pricelevelname='Actual Price') INNER JOIN Item ON ( Item.ID = TransactionLine.Item AND TransactionLine.item IN (".$itemIds.") AND TransactionLine.transaction IN (".$tranIds.") ) ORDER BY trandate DESC\"\n}"; */
			 $query = "{\n\t\"q\": \"SELECT TransactionLine.itemtype as itemtype,Itemprice.price as actual_selling_price,Itemprice.pricelevelname,Item.pricinggroup,Item.itemid as sku,Item.isserialitem,Item.ID as iteminternalid,Item.custitem_f3_category_type,Item.description,TransactionLine.Transaction AS TransactionID,Transaction.employee as salesrep,TransactionLine.custcol_f3_document_number as documentnumber,Transaction.TranDate,Transaction.custbody_es_customer_inv_number,Transaction.Type,Transaction.TranID,Transaction.Entity as customer,TransactionLine.Rate,TransactionLine.quantity,TransactionLine.NetAmount FROM TransactionLine INNER JOIN Transaction ON ( Transaction.ID = TransactionLine.Transaction AND  Transaction.Entity =  ".$customerNetsuiteInternalId." and (transaction.recordtype = 'invoice' or transaction.recordtype = 'cashsale') AND TransactionLine.memo != 'Cost of Sales' ".$andCondition." ) INNER JOIN Itemprice ON(TransactionLine.Item = Itemprice.item) INNER JOIN Item ON ( Item.ID = TransactionLine.Item AND TransactionLine.item IN (".$itemIds.") AND TransactionLine.transaction IN (".$tranIds.") ) ORDER BY trandate DESC\"\n}";
			$response = $this->restSuiteqlApi->callRestApi($query);
			if((isset($response['count']) && $response['count'] == 0) || !isset($response['items']) ){
				$responseData['data'] =  0;
			}else{
			$liveData = $this->_FilterWorkSheetData($response); 
			$responseData['data'] =  $this->_checkReasonsAndPriceForWorkSheetData($liveData);
			}
		}else{
			$responseData['data'] = 1;
		}
		return $responseData;
	}
	protected function _checkReasonsAndPriceForWorkSheetData($liveData){
		
		$workSheetData = $this->getWorkSheetData();
		$pingData = [];
		foreach($workSheetData as $sheet){
				if(!$this->checkReasonIsValid($sheet,$liveData)){
					$sheet['error'] = 'Invalidate selected Customer reason or delete this record from worksheet and search that record again';
					$pingData[] = $sheet;
					unset($sheet['error']);
					//break;
				}else if(!$this->checkPriceIsValid($sheet,$liveData)){
					$sheet['error'] = 'Invalidate Price. Delete this record from worksheet and try again';
					$pingData[] = $sheet;
					unset($sheet['error']);
					//break;
				}
		}
		/*echo "<pre>";
		print_r($pingData);die();*/
		return $pingData;
	}
	protected function checkPriceIsValid($sheet,$liveData){
		$bool = false;
		foreach($liveData as $item){
			$item['reason_id'] = $sheet['reason_id'];
			if($this->matchUnitPrice($sheet['actual_unit_credit_price'],$item) && $sheet['item_internal_id'] == $item['iteminternalid'] && $sheet['transaction_internal_id'] == $item['transactionid']){	
				$bool = true; // rules changed
				break;
			}
		}
		return $bool;
	}
	protected function matchUnitPrice($sheetPrice,$item){
		$bool = false;
		$actualUnitPrice = $this->getActualCreditPrice($item);
		//echo $sheetPrice;die();
		if($actualUnitPrice == $sheetPrice){
			$bool = true;
		}
		return $bool;
	}
	protected function checkReasonIsValid($sheet,$liveData){
		$bool = false;
		foreach($liveData as $item){
			if($this->matchReasonId($sheet['reason_id'],$item['reason_lists']) && $sheet['item_internal_id'] == $item['iteminternalid'] && $sheet['transaction_internal_id'] == $item['transactionid']){	
				$bool = true; // rules changed
				break;
			}
		}
		return $bool;
	}
	protected function matchReasonId($sheetReasonId,$itemReasons){
		$bool = false;
		foreach($itemReasons as $reason){
			if($sheetReasonId == $reason['id']){
				$bool = true;break;
			}
		}
		
		return $bool;
	}
	protected function _checkCustomerReturn($item){
		$customerReturnData = $this->customerSession->getCustomerReturnData();
		if(isset($customerReturnData) && is_array($customerReturnData) && count($customerReturnData)){
			foreach($customerReturnData as $return){
				if(isset($return['custcol_f3_document_number']) && $return['custcol_f3_document_number'] == $item['transactionid'] &&  $return['iteminternalid'] == $item['iteminternalid']){
					$item['quantity'] = abs($item['quantity']) - abs($return['quantity']);
					if($item['quantity'] <= 0)
						$item ='';
					break;
				}
			}
		}
		return $item;
	}
	protected function _FilterWorkSheetData($response){
		$newItemsArray = [];
		if(isset($response['items'])){
			foreach($response['items'] as $item){
				if($item['itemtype'] != 'TaxGroup' && $item['itemtype'] != 'ShipItem'){
				if(!$this->matchGroupPricingItems($item))
				continue;
				$newItem = $this->_checkCustomerReturn($item);
				if(!empty($newItem)){
					$newItemsArray[] = $newItem; 
				}
			 }
			}
		}		
		return $this->_FilterDataSubmitRAByReturnRules($newItemsArray);
	}
	public function wipeWorkSheetData(){
		$customerId = $this->getCustomerId();
		$tableName = $this->_getTableName('netsuite_ra_worksheet');
			$results = $this->resourceConnection->getConnection()->delete(
						$tableName,
						['customer_id = ?' => $customerId]
						);
	}
	public function getReturnTranId($internalId){
		$tranid = 0;
		$query = "{\n\t\"q\": \"SELECT tranid FROM Transaction WHERE id =  ".$internalId."\"\n}";
			$response = $this->restSuiteqlApi->callRestApi($query);
			if(isset($response['count']) && $response['count'] > 0 ){
				$tranid = $response['items'][0]['tranid'];
			}
			return $tranid;
	}
	public function getReturnStatus($status){
		if($status == 'A')
		$status = 'Pending Approval';
		else if($status == 'B')
		$status = 'Pending Receipt';
		else if($status == 'C')
		$status = 'Cancelled';
		else if($status == 'D')
		$status = 'Partially Received';
		else if($status == 'E')
		$status = 'Pending Refund/Partially Received';
		else if($status == 'F')
		$status = 'Pending Refund';
		else if($status == 'G')
		$status = 'Refunded';
		else
		$status = 'Closed';
		return $status;
	}
	public function getFormattedPrice($price){
		return $this->priceHelper->currency($price,true,false);
	}
}
