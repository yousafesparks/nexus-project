<?php

declare(strict_types=1);

namespace Nexuscellular\ReturnAuthorization\Controller\Index;
use Nexuscellular\ReturnAuthorization\Helper\Data as apiHelper;
class Saveworksheet extends \Magento\Framework\App\Action\Action
{
    protected $apiHelper;

  /**
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;

  protected $resourceConnection;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        apiHelper $apiHelper,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->jsonHelper = $jsonHelper;
        $this->apiHelper = $apiHelper;
        $this->resourceConnection = $resourceConnection;
    }
	
      public function execute(){
          $response= [];
          if (!$this->getRequest()->isPost()) {
                 $response = $this->_responseError();
                  return $this->getResponse()->representJson(
                      $this->jsonHelper->jsonEncode($response)
                  );
              }
          try{
            $customerId = $this->apiHelper->getCustomerId();
            if(!$customerId){
              $response = $this->_responseError();
              $response['customer'] = 0;
              $response['message'] = 'you have been logout.Please login';
              return $this->getResponse()->representJson(
                  $this->jsonHelper->jsonEncode($response)
              );
            }
              $data = $this->getRequest()->getParam('data');
              if(!count($data)> 0){
                $response = $this->_responseError();
                return $this->getResponse()->representJson(
                    $this->jsonHelper->jsonEncode($response)
                );
              }
              $actual_credit_price = $this->apiHelper->getActualCreditPrice($data);//actual_unit_credit_price
              if($actual_credit_price != "Not Available"){
              $values = $this->apiHelper->getOrderDetails($data);
              if(count($values) == 0){
                $response = $this->_responseError();
                return $this->getResponse()->representJson(
                    $this->jsonHelper->jsonEncode($response)
                );
              }
              $serialNumber = '';
              if(isset($data['serialnumber'])){
                $serialNumber = $data['serialnumber'];
              }
              if(isset($data['salesrep'])){
                $salesrep = $data['salesrep'];
              }else{
                $salesrep = 0;
              }
              
              if(count($values)> 0){
                $tableName = $this->_getTableName('netsuite_ra_worksheet');
                $insertData = ["tranid"=>$data['tranid'],"customer_id"=>$customerId,"trandate"=>$data['trandate'],'product_name'=>$data['description'],
                'transaction_internal_id'=>$data['transactionid'],'item_internal_id'=>$data['iteminternalid'],
                "serialnumber"=>$serialNumber,'sales_rep_id'=>$salesrep,'netsuite_category_commission_id'=>$data['custitem_f3_category_type'],
                'product_sku'=>$data['sku'],"comment"=>$data['comment'],"reason_id"=>$data['reason_id'],
                'qty'=>$data['product_qty'],'soap_trandate'=>$values['soap_trandate'],'tax_code'=>$values['taxcode'],'purchase_price'=>$values['rate'],'sell_price'=>$data['actual_selling_price'],
                'actual_unit_credit_price'=>$actual_credit_price
                ];
              $results= $this->resourceConnection->getConnection()->insert($tableName, $insertData);
              
                if($results == 1){
                  $qty = $data['quantity'] - $data['product_qty'];
                  $response['success'] = true;
                  $response['customer'] = 1;
                  $response['data'] = ['worksheet'=>$this->apiHelper->getWorkSheetData(),'search_grid'=>['qty'=>$qty,'id'=>$data['uniquekey']]];
                  $response['message'] = 'save the data successfully';
                }else{
                  $response = $this->_responseError();
                }
              }else{
                $response = $this->_responseError();
              }
            }else{
              $response['success'] = false;
              $response['message'] = 'Price rules do not match with our policy. please check warranty policy';
            }
          }catch (\Exception $e) {
            //echo $e->getMessage();die();
            $response = $this->_responseError();
          }
          return $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
      }

      /*
      *@return {string}
      */
      protected function _getTablename($tableName){
        /* Create Connection */
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
      }
      protected function _responseError(){
        $response['success'] = false;
        $response['customer'] = 1;
        $response['message'] = 'An error occurred while processing your form. Please try again later.';
        return $response;
      }
  }
  

