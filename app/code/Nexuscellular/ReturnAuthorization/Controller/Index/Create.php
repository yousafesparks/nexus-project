<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\ReturnAuthorization\Controller\Index;
use Nexuscellular\ReturnAuthorization\Helper\SqlApiCall as restSuiteqlApi;
use Nexuscellular\ReturnAuthorization\Helper\Data as apiHelper;
use Nexuscellular\ReturnAuthorization\Helper\SoapApiCall as soapApiCall;
class Create extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory; 
  protected $restSuiteqlApi; 
  protected $customerSession;
  protected $returnRulesFactory;
  protected $apiHelper;
  protected $soapApiCall;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $_pageFactory;
    protected $urlInterface;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Nexuscellular\ReturnAuthorization\Model\ResourceModel\Returnrules\CollectionFactory $returnRulesFactory, 
      \Magento\Framework\View\Result\PageFactory $pageFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\UrlInterface $url,
      apiHelper $apiHelper,
      soapApiCall $soapApiCall,
      restSuiteqlApi $restapiHelper
      )
    {
      
      $this->returnRulesFactory =$returnRulesFactory;
      $this->_pageFactory = $pageFactory;
      $this->restSuiteqlApi = $restapiHelper;
      $this->customerSession = $customerSession;
      $this->urlInterface = $url;
      $this->apiHelper = $apiHelper;
      $this->soapApiCall = $soapApiCall;
      return parent::__construct($context);
    }

    public function execute()
    {
      if(!$this->apiHelper->getCustomerId()){
        $login_url = $this->urlInterface
      ->getUrl('customer/account/login');
      return $this->resultRedirectFactory->create()->setPath($login_url);
      }
	  $this->customerSession->setCustomerReturnData([]);
	  $this->customerSession->setCustomerGroupPricing([]);
     /*  $customerNetsuiteInternalId = $this->customerSession->getCustomer()->getData('netsuite_internal_id');
      $query = "{\n\t\"q\": \"SELECT customer.custentity_f3_special_customer as custentity_f3_special_customer,customer.custentitycustentity_f3_three_warranty as custentity_f3_three_warranty,customer.id as customer_id,TransactionLine.itemtype as itemtype,TransactionLine.quantity,TransactionLine.Item as iteminternalid,TransactionLine.custcol_f3_document_number,Transaction.trandate,Transaction.recordtype FROM TransactionLine,Transaction,customer WHERE  Transaction.id = TransactionLine.transaction AND Transaction.entity = customer.id AND TransactionLine.entity = ".$customerNetsuiteInternalId." and Transaction.recordtype='returnauthorization'  AND TransactionLine.taxline = 'F' AND TransactionLine.mainline = 'F' AND TransactionLine.itemtype != 'TaxGroup' AND TransactionLine.itemtype != 'ShipItem' ORDER BY trandate DESC\"\n}";
	  $response = $this->restSuiteqlApi->callRestApi($query);
      $this->customerSession->unsCustomerReturnData();
      if(isset($response['count']) && $response['count'] > 0 ){
        $this->customerSession->setCustomerReturnData($response['items']);
      }
	  $customerReturnData = $this->customerSession->getCustomerReturnData();
	  $this->customerSession->setCustomerGroupPricing([]);
      $responseCustomer = $this->soapApiCall->getCustomerByInternalId($customerNetsuiteInternalId );
	  if(count($responseCustomer) > 0 && !empty($responseCustomer['groupPricingList'])){
		$this->customerSession->setCustomerGroupPricing($responseCustomer['groupPricingList']['groupPricing']);
	 } */
      return $this->_pageFactory->create();
    
    }
  }
  

