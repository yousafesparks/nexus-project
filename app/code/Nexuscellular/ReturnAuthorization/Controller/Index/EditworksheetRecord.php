<?php

declare(strict_types=1);

namespace Nexuscellular\ReturnAuthorization\Controller\Index;
use Nexuscellular\ReturnAuthorization\Helper\Data as apiHelper;
class EditworksheetRecord extends \Magento\Framework\App\Action\Action
{
    protected $apiHelper;

  /**
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;

  protected $resourceConnection;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        apiHelper $apiHelper,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->jsonHelper = $jsonHelper;
        $this->apiHelper = $apiHelper;
        $this->resourceConnection = $resourceConnection;
    }
	
      public function execute(){
          $response= [];
          if (!$this->getRequest()->isPost()) {
                 $response = $this->_responseError();
                  return $this->getResponse()->representJson(
                      $this->jsonHelper->jsonEncode($response)
                  );
              }
          try{
            $customerId = $this->apiHelper->getCustomerId();
            if(!$customerId){
              $response = $this->_responseError();
              $response['customer'] = 0;
              $response['message'] = 'you have been logout.Please login';
              return $this->getResponse()->representJson(
                  $this->jsonHelper->jsonEncode($response)
              );
            }
              $entity_id = $this->getRequest()->getParam('entity_id');
              if(!empty($entity_id)){
                $tableName = $this->_getTableName('netsuite_ra_worksheet');
                $connection = $this->resourceConnection->getConnection();
                $select = $connection->select()
                ->from(
                ['rw' => $tableName])
                ->where('rw.id=?', $entity_id);
              
                $fetchData = $connection->fetchAll($select);
                if(count($fetchData) > 0){
                  $items = $this->apiHelper->getOneOrderRecord($fetchData[0]);
                }
               foreach($fetchData as $worksheet){
                 if(isset($worksheet['serialnumber']) && !empty($worksheet['serialnumber'])){
                  $items = $this->getFilterItemsBySerialnumber($items,$worksheet['serialnumber']);
                  break;
                 }
               }
                $response['success'] = true;
                $response['customer'] = 1;
                $response['data'] = ['worksheet'=>$this->apiHelper->getWorkSheetData(),'action'=>'edit','requested_data'=>$fetchData[0],'item'=>$items];
                $response['message'] = 'succeed!';
              }else{
                $response = $this->_responseError();
              }
          }catch (\Exception $e) {
            //echo $e->getMessage();die();
            $response = $this->_responseError();
          }
          return $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
      }

      /*
      *@return {string}
      */
      protected function _getTablename($tableName){
        /* Create Connection */
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
      }
      protected function _responseError(){
        $response['success'] = false;
        $response['customer'] = 1;
        $response['message'] = 'An error occurred while processing your form. Please try again later.';
        return $response;
      }
      protected function getFilterItemsBySerialnumber($items,$serialNumber){
        foreach($items as $key => $item){
          if($item['serialnumber'] != $serialNumber){
            unset($items[$key]);
            break;
          }
        }
        return $items;
      }
  }
  

