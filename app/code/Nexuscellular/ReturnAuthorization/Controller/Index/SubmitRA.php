<?php

declare(strict_types=1);

namespace Nexuscellular\ReturnAuthorization\Controller\Index;
use Nexuscellular\ReturnAuthorization\Helper\Data as apiHelper;
require_once(BP.'/var/netsuitelibrary/PHPToolkit_2020_1/samples/add_ra.php');
class SubmitRA extends \Magento\Framework\App\Action\Action
{
	protected $apiHelper;

	/**
	* @var \Magento\Framework\Json\Helper\Data
	*/
	protected $jsonHelper;
	
	/**
	* @var \Magento\Framework\UrlInterface
	*/
	protected $urlInterface;

	protected $resourceConnection;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        apiHelper $apiHelper,
		 \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->jsonHelper = $jsonHelper;
        $this->apiHelper = $apiHelper;
		 $this->urlInterface = $url;
        $this->resourceConnection = $resourceConnection;
    }
	
      public function execute(){
          $response= [];
          if (!$this->getRequest()->isPost()) {
                 $response = $this->_responseError();
                  return $this->getResponse()->representJson(
                      $this->jsonHelper->jsonEncode($response)
                  );
              }
          try{
            $customerId = $this->apiHelper->getCustomerId();
            
            if(!$customerId){
              $response = $this->_responseError();
              $response['customer'] = 0;
              $response['message'] = 'you have been logout.Please login';
              return $this->getResponse()->representJson(
                  $this->jsonHelper->jsonEncode($response)
              );
            }
              $additionalData = [];
              $additionalData['instruction'] = $this->getRequest()->getParam('instruction');
              $additionalData['customer_ra_number'] = $this->getRequest()->getParam('customer_ra_number');
                $responseData = $this->apiHelper->checkAllReturnRules();// fetch live data from netsuite before customer submit RA
                if(is_array($responseData['data']) && count($responseData['data']) > 0){
                  $response['success'] = false;
                  $response['data'] = $responseData['data'];
                  //echo "<pre>";print_r( $response['data']);die();
                }else if(!is_array($responseData['data']) && $responseData['data'] == 0 ){
                  $response['success'] = false;
                  $response['data'] = [];
                  $response['message'] = 'Records Not found';
                }else if(!is_array($responseData['data'])  && $responseData['data'] == 1 ){
                  $response['message'] = 'Worksheet records not found';
                  $response['data'] = [];
                  $response['message'] = 'Worksheet Records Not found';
                }
                else{
                  //die('passed');
                  $response['success'] = true;
                  $response['data'] = [];
                  $response['message'] = 'save the data successfully';
                  $workSheetRecords = $this->apiHelper->getWorkSheetData();  
                  $netsuiteRecord = [];
                  $netsuiteReturnitems = [];
                foreach($workSheetRecords as $item){
                  $netsuiteAttributes= [
                    'price'=>$item['actual_unit_credit_price'],
                    'qty'=>$item['qty'],
                    'internal_id'=>$item['item_internal_id'],
                    'soap_trandate'=> $item['soap_trandate'],
                    'netsuite_order_id'=>$item['transaction_internal_id'],
                    'reason_netsuite_internal_id'=> $this->getReasonInternalId($item['reason_id']),
                    'tax_code'=> $item['tax_code'],
                    'sales_rep_id'=> $item['sales_rep_id'],
                    'serialnumber'=> $item['serialnumber'],
                    'comment'=>$item['comment']
                    ];
                  $netsuiteRecord[] = $netsuiteAttributes;
                }
              $response = $this->_sendRAItemsToNetsuite($netsuiteRecord,$additionalData);
            }
          }catch (\Exception $e) {
           echo $e->getMessage();die();
            $response = $this->_responseError();
          }
          $response['customer'] = 1;
          return $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
      }

      /*
      *@return {string}
      */
      protected function _getTablename($tableName){
        /* Create Connection */
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
      }
      protected function _responseError(){
        $response['success'] = false;
        $response['customer'] = 1;
        $response['data'] = [];
        $response['message'] = 'An error occurred while processing your form. Please try again later.';
        return $response;
      }
      protected function _sendRAItemsToNetsuite($netsuiteRecord,$additionalData){
        $service = new \NetSuiteService();
        $so = new \ReturnAuthorization();
        $so->entity = new \RecordRef();
        $so->entity->internalId = $this->apiHelper->getNetsuiteCustomerId();
        $so->salesRep = new \RecordRef();
        if($this->getSalesRep($netsuiteRecord))
        $so->salesRep->internalId = $this->getSalesRep($netsuiteRecord);
        $so->itemList = new \ReturnAuthorizationItemList();
        $so->memo = $additionalData['instruction'];
       
        if(!empty($additionalData['customer_ra_number'])){
          $so->customFieldList = new \CustomFieldList();
          $customerRANumber = new \StringCustomFieldRef();
        $customerRANumber->value = $additionalData['customer_ra_number'];
        $customerRANumber->scriptId = "custbodycustom_customer_ra_number";
        $customerRANumber->internalId = "1306";

        $so->customFieldList->customField = array($customerRANumber);
        }
        

        $soiList = [];
        $i = 1;   
         
    foreach($netsuiteRecord as $value){
        for($row=1;$row<=$value['qty'];$row++){
            $soi = new \ReturnAuthorizationItem();
            $soi->item = new \RecordRef();
            $soi->item->internalId = $value['internal_id'];
            $soi->quantity =1;
            $soi->price = new \RecordRef();
            $soi->price->internalId = -1;
            $soi->taxCode = new \RecordRef();
            $soi->taxCode->internalId = $value['tax_code'];
            $soi->rate = $value['price'];
            $soi->amount =($value['price'] * 1);
            $soi->customFieldList = new \CustomFieldList();
            // Transaction Document Number
            $randomValueTrans = new \SelectCustomFieldRef();
            $randomValueTrans->value = new \ListOrRecordRef();
            $randomValueTrans->value->internalId = $value['netsuite_order_id'];
            //$randomValueTrans->internalId = '1316';
            $randomValueTrans->scriptId = 'custcol_f3_document_number';

            //line number
            $ra_sequence_number = new \StringCustomFieldRef();
            $ra_sequence_number->value = $i;
            $ra_sequence_number->scriptId = "custcol_f3_ra_sequence_number";
          // $ra_sequence_number->internalId = "1314";
            
            //transaction code number
            $transaction_date = new \DateCustomFieldRef();
            $transaction_date->value = $value['soap_trandate'];
            $transaction_date->scriptId = "custcol_f3_transaction_trandate";
            //$transaction_date->internalId = "1313";

            //customer reason
            $randomValueSelect= new \SelectCustomFieldRef();
            $randomValueSelect->value = new \ListOrRecordRef();
            $randomValueSelect->value->internalId = $value['reason_netsuite_internal_id'];
            $randomValueSelect->value->typeId = '1987';
            $randomValueSelect->internalId = '307';
            //$randomValueSelect->scriptId = 'custcol_es_reason_for_return';

            $customerComment = new \StringCustomFieldRef();
            $customerComment->value = $value['comment'];
            $customerComment->scriptId = "custcol3";
            //$customerComment->internalId = "526";

            $soi->customFieldList->customField = array($randomValueSelect,$randomValueTrans,$customerComment,$ra_sequence_number,$transaction_date);
            
            if(!empty($value['serialnumber'])){
            $soi->inventoryDetail = new \InventoryDetail();
            $soi->inventoryDetail->inventoryAssignmentList = new \InventoryAssignmentList();
            $inAssign = new \InventoryAssignment();
            $inAssign->receiptInventoryNumber = $value['serialnumber'];
            $soi->inventoryDetail->inventoryAssignmentList->inventoryAssignment = array($inAssign);
            }
            $soiList[] =  $soi;
            unset($soi);
            $i++;
          }// end qty for loop
        }
          $so->itemList->item = $soiList;
    
          $request = new \AddRequest();
          $request->record = $so;
    
          $addResponse = $service->add($request);
          //echo "<pre>";print_r($addResponse);die();
          if (!$addResponse->writeResponse->status->isSuccess) {
            $response['success'] = false;
            $response['data'] = [];
            if(isset($addResponse->writeResponse->status->statusDetail)){
              $errorArray = $addResponse->writeResponse->status->statusDetail;
              if(isset($errorArray[0])){
              $errorObject = $errorArray[0];
              $response['message'] = $errorObject->message;
              }
            }else{
              $response['message'] = 'An error occurred while creating Return Authorization. Please try again.';
            }
          } else {
            $this->apiHelper->wipeWorkSheetData();
            $response['success'] = true;
            $response['message'] = 'success!';
            $internalId = $addResponse->writeResponse->baseRef->internalId;
            $tranid = $this->updateRecords($internalId,$netsuiteRecord);
          $response['data'] = ['internal_id'=>$internalId,
		  'tranid'=>$tranid,
		  'pdfurl'=>$this->urlInterface->getUrl('returnauthorization/index/returnpdf/id/'.$internalId),
		  'url'=>$this->urlInterface->getUrl('returnauthorization/index/returnview/id/'.$internalId)];
		  
          //$this->apiHelper->unsetCustomerSessionData();
              //echo "ADD SUCCESS, id " . $addResponse->writeResponse->baseRef->internalId;
          }
          return $response;
    }
    public function updateRecords($internalId,$netsuiteRecord){
      $service = new \NetSuiteService();
      $gr = new \GetRequest();
      $gr->baseRef = new \RecordRef();
      $gr->baseRef->internalId = $internalId;
      $gr->baseRef->type = "returnAuthorization";

      $getResponse = $service->get($gr);
      //echo "<pre>";print_r($getResponse);die();
      if ($getResponse->readResponse->status->isSuccess) {
   
      $tranid = $getResponse->readResponse->record->tranId;
      $so = $getResponse->readResponse->record;
      $soiList = [];
      $i = 1;
      foreach($netsuiteRecord as $value){
        for($row=1;$row<=$value['qty'];$row++){
        $soi = new \ReturnAuthorizationItem();
        $soi->item = new \RecordRef();
        $soi->item->internalId = $value['internal_id'];
        $soi->quantity = 1;
        $soi->price = new \RecordRef();
        $soi->price->internalId = -1;
        $soi->taxCode = new \RecordRef();
        $soi->taxCode->internalId = $value['tax_code'];
        $soi->rate = $value['price'];
        $soi->amount =($value['price'] * 1);
        $soi->customFieldList = new \CustomFieldList();
        // Transaction Document Number
        $randomValueTrans = new \SelectCustomFieldRef();
        $randomValueTrans->value = new \ListOrRecordRef();
        $randomValueTrans->value->internalId = $value['netsuite_order_id'];
        //$randomValueTrans->internalId = '1316';
        $randomValueTrans->scriptId = 'custcol_f3_document_number';

        //line number
        $number = str_pad(strval($i), 3, '00', STR_PAD_LEFT);
        $ra_sequence_number = new \StringCustomFieldRef();
        $ra_sequence_number->value = $tranid.'-'.$number;
        $ra_sequence_number->scriptId = "custcol_f3_ra_sequence_number";
       // $ra_sequence_number->internalId = "1314";
        
        //transaction code number
        $transaction_date = new \DateCustomFieldRef();
        $transaction_date->value = $value['soap_trandate'];
        $transaction_date->scriptId = "custcol_f3_transaction_trandate";
        //$transaction_date->internalId = "1313";

        //customer reason
        $randomValueSelect= new \SelectCustomFieldRef();
        $randomValueSelect->value = new \ListOrRecordRef();
        $randomValueSelect->value->internalId = $value['reason_netsuite_internal_id'];
        $randomValueSelect->value->typeId = '1987';
        $randomValueSelect->internalId = '307';
        $randomValueSelect->scriptId = 'custcol_es_reason_for_return';

        $customerComment = new \StringCustomFieldRef();
        $customerComment->value = $value['comment'];
        $customerComment->scriptId = "custcol3";
        //$customerComment->internalId = "526";

        $soi->customFieldList->customField = array($randomValueSelect,$randomValueTrans,$customerComment,$ra_sequence_number,$transaction_date);
        
        if(!empty($value['serialnumber'])){
        $soi->inventoryDetail = new \InventoryDetail();
        $soi->inventoryDetail->inventoryAssignmentList = new \InventoryAssignmentList();
        $inAssign = new \InventoryAssignment();
        $inAssign->receiptInventoryNumber = $value['serialnumber'];
        $soi->inventoryDetail->inventoryAssignmentList->inventoryAssignment = array($inAssign);
        }
        $soiList[] =  $soi;
        unset($soi);
        $i++;
        } //end forloop for qty
      }
      $so->itemList->item = $soiList;
      $request = new \UpdateRequest();
      $request->record = $so;

      $service->setPreferences(false, false, false, true);

        $updateResponse = $service->update($request);
        
        if (!$getResponse->readResponse->status->isSuccess) {
          $tranid = 0;
        }
        return $tranid;
      }
    }
    protected function _getProductNetsuiteInternalId($sku){
      $productObj = $this->productRepository->get($sku);
      if($productObj->getId()){
        return $productObj->getNetsuiteInternalId();
      }
    }
    protected function getReasonInternalId($reasonId){
      $netsuiteid = $this->apiHelper->getReturnRulesData($reasonId);
      if(count($netsuiteid) > 0){
        return $netsuiteid[0]['netsuite_internal_id'];
      }
      return '';
      //echo "<pre>";print_r($netsuiteid);die();
    }
    protected function getSalesRep($netsuiteRecord){
      //$sales_rep = 11904;// Salman bhai
      $sales_rep = -5;// Sales Rep ID of Irfan Bajwa Sir
      $disableRepIds = $this->_getDisableSalesRepIds();
      foreach($netsuiteRecord as $value){
        if($value['sales_rep_id'] > 0 && !in_array($value['sales_rep_id'],$disableRepIds)){ // 2727 is ID of old employee
          $sales_rep = $value['sales_rep_id'];
          break;
        }
      }
      return $sales_rep;
    }
    protected function _getDisableSalesRepIds(){
      return [2727,12619];
    }
  }
