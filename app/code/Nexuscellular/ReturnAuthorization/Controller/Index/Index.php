<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Nexuscellular\ReturnAuthorization\Controller\Index;
class Index extends \Magento\Framework\App\Action\Action
{

  protected $resultPageFactory;
	protected $urlRewriteCollection;
	protected $category;
  protected $resourceConnection;
  protected $_customerRepositoryInterface;
  private $productRepository; 
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        
     $this->resourceConnection = $resourceConnection;
     $this->_customerRepositoryInterface = $customerRepositoryInterface;
     $this->productRepository = $productRepository;
         parent::__construct($context);
    }
	
	  public function execute()
    {
    
    }
  }
  

