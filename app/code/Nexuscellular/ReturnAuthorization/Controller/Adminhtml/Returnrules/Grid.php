<?php
namespace Nexuscellular\ReturnAuthorization\Controller\Adminhtml\Returnrules;
 
use Nexuscellular\ReturnAuthorization\Controller\Adminhtml\Returnrules;
 
class Grid extends Returnrules
{
    public function execute()
    {
        return $this->_resultPageFactory->create();
    }
}