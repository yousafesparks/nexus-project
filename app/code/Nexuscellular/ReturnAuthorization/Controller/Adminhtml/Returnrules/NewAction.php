<?php
namespace Nexuscellular\ReturnAuthorization\Controller\Adminhtml\Returnrules;
 
use Nexuscellular\ReturnAuthorization\Controller\Adminhtml\Returnrules;
 
class NewAction extends Returnrules
{
    /**
     * Create new news action
     *
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}