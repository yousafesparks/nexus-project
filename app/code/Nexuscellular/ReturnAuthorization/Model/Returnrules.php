<?php

namespace Nexuscellular\ReturnAuthorization\Model;

class Returnrules extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'nexuscellular_netsuite_returnrules';

	protected $_cacheTag = 'nexuscellular_netsuite_returnrules';

	protected $_eventPrefix = 'nexuscellular_netsuite_returnrules';

	protected function _construct()
	{
		$this->_init('Nexuscellular\ReturnAuthorization\Model\ResourceModel\Returnrules');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}