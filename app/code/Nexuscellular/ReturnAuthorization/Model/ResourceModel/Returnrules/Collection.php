<?php

namespace Nexuscellular\ReturnAuthorization\Model\ResourceModel\Returnrules;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'netsuite_return_rules_collection';
	protected $_eventObject = 'returnrules_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Nexuscellular\ReturnAuthorization\Model\Returnrules', 'Nexuscellular\ReturnAuthorization\Model\ResourceModel\Returnrules');
	}
	
}