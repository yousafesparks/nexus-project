<?php

namespace Nexuscellular\CustomizationQuote\Plugin\Model\Quote;

use Magento\Framework\Registry;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Magento\CatalogInventory\Model\Stock;
use Magento\Customer\Model\Session as customerSession;
class Item
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;
	
	/**
     * @var customerSession
     */
	private $customerSession;

    /**
     * @param Registry $registry
     * @param StockRegistryInterface $stockRegistry
     */
    public function __construct(
        Registry $registry,
        StockRegistryInterface $stockRegistry,
		customerSession $customerSession
    ) {
        $this->registry = $registry;
        $this->stockRegistry = $stockRegistry;
		$this->customerSession = $customerSession;
    }

    public function beforeAddQty(QuoteItem $quoteItem, $qty)
    {
        //$this->registry->unregister('qty_added');
		
        $product = $quoteItem->getProduct();
		$sku = $product->getSku();

		$stockItem = $this->stockRegistry->getStockItemBySku($sku);
       // $stockItem = $this->stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
	   //echo $stockItem->getQty();die();
        $maxQty = $stockItem->getQty() - $quoteItem->getQty();

        if ($maxQty - $stockItem->getMinQty() - $qty < 0) {
            $backorders = $stockItem->getBackorders();
            if ($backorders != Stock::BACKORDERS_YES_NONOTIFY && $backorders != Stock::BACKORDERS_YES_NOTIFY) {
                $qty = $maxQty;
                //$this->registry->register('qty_added', $qty);
				$this->customerSession->unsQtyadded();
				$this->customerSession->setQtyadded($qty);
					//var_dump($this->customerSession->getQtyadded());
            }
        }

        return [$qty];
    }
}
