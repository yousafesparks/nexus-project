var config = {
	 config: {
        mixins: {
            'Magento_Checkout/js/sidebar': {
                'RLTSquare_AjaxShoppingCartUpdate/js/sidebar': true
            }
        }
    },
    map: {
        '*': {
            'AjaxCart': 'RLTSquare_AjaxShoppingCartUpdate/js/cartValueIncDec',
            'CartQtyUpdate': 'RLTSquare_AjaxShoppingCartUpdate/js/cartQtyUpdate',
			'Magento_Checkout/template/minicart/item/default.html': 'RLTSquare_AjaxShoppingCartUpdate/template/minicart/item/default.html'
        }
    },
    shim: {
        AjaxCart: {
            deps: ['jquery']
        },
        CartQtyUpdate: {
            deps: ['jquery']
        }
    }
};