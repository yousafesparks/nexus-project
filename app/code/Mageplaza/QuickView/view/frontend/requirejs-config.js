/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

var config = {
    paths: {
        mpquickview: 'Mageplaza_QuickView/js/popup/mp-quickview',
        mpajaxcart: 'Mageplaza_QuickView/js/popup/mp-ajaxcart',
        mpajaxcompare: 'Mageplaza_QuickView/js/ajax/compare',
        mpajaxwishlist: 'Mageplaza_QuickView/js/ajax/wishlist',
        mpajaxSuccess: 'Mageplaza_QuickView/js/popup/mp-ajaxcart-update'
    },
    map: {
        '*': {
            'Magento_Review/js/process-reviews': 'Mageplaza_QuickView/js/review/process-reviews'
        }
    },
    config: {
        mixins: {
            'Magento_Swatches/js/swatch-renderer': {
                'Mageplaza_QuickView/js/mpswatch-renderer': true
            },
            'mage/gallery/gallery': {
                'Mageplaza_QuickView/js/gallery': true
            }
        }
    }
};
