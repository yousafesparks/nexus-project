/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('mageplaza.ajaxCompare', {
        /**
         * @inheritDoc
         */
        _create: function () {
            this.action();
        },

        action: function () {
            var self = this;

            $('body').delegate('.action.tocompare', 'click', function (e) {
                var el = $(this),
                    dataPost = el.data('post'),
                    formKey = $('input[name="form_key"]').val();

                /** Check apply for quick view popup **/
                if (el.closest('#mpquickview-popup #maincontent').length && !self.options.isApplyQV) {
                    return;
                }

                /** Check apply for action name page */
                if (!el.closest('#mpquickview-popup #maincontent').length && !self.options.isApplyPage) {
                    return;
                }

                if (formKey) {
                    dataPost.data.form_key = formKey;
                }

                var paramData = $.param(dataPost.data),
                    url = dataPost.action + (paramData.length ? '?' + paramData : '');

                e.stopPropagation();

                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    showLoader: true,
                    success: function (res) {
                        var popup = $('#mpquickview-popup');
                        $('.mpquickview-message').remove();
                        popup.prepend('<div class="mpquickview-message message-success success message">' + res.message + '</div>');
                        popup.animate({scrollTop: 0}, "slow");
                    }
                });
            });
        }
    });

    return $.mageplaza.ajaxCompare;
});

