/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(['jquery', 'mage/utils/wrapper'], function ($, wrapper) {
    'use strict';

    return function (initialize) {
        return wrapper.wrap(initialize, function (initialize, config, element) {
            /** Mageplaza fix loop load image on product detail page */
            var qvPopup = $('#mpquickview-popup #maincontent'),
                acPopup = $('#mpajaxcart-popup #maincontent');

            if (acPopup.length === 0) {
                if ($('.catalog-product-view').length &&
                    qvPopup.length &&
                    $(element).closest('#mpquickview-popup #maincontent').length === 0
                ) {
                    return;
                }
            } else {
                if ($('.catalog-product-view').length &&
                    acPopup.length &&
                    $(element).closest('#mpajaxcart-popup #maincontent').length === 0
                ) {
                    return;
                }
            }

            initialize(config, element);
        });
    };
});
