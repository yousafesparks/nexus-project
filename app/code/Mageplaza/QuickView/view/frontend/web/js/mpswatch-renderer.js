/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
], function ($) {
    'use strict';

    return function (widget) {
        $.widget('mage.SwatchRenderer', widget, {
            /**
             * Fix bug: loop load swatch option quick view on product page
             *  {@inheritdoc}
             */
            _RenderControls: function () {
                var qvPopup = $('#mpquickview-popup #maincontent'),
                    acPopup = $('#mpajaxcart-popup #maincontent'),
                    container = this.element;

                if (acPopup.length === 0) {
                    if ($('.catalog-product-view').length &&
                        qvPopup.length &&
                        container.closest('#mpquickview-popup #maincontent').length === 0
                    ) {
                        return;
                    }
                } else {
                    if ($('.catalog-product-view').length &&
                        acPopup.length &&
                        container.closest('#mpajaxcart-popup #maincontent').length === 0
                    ) {
                        return;
                    }
                }

                this._super();
            },

            /**
             * Fix bug: loop load swatch option quick view on product page
             *  {@inheritdoc}
             */
            _onGalleryLoaded: function (element) {
                var qvPopup = $('#mpquickview-popup #maincontent');

                if ($('.catalog-product-view').length && qvPopup.length && element.closest('#mpquickview-popup #maincontent').length === 0) {
                    return;
                }

                this._super(element);
            }

        });

        return $.mage.SwatchRenderer;
    }
});
