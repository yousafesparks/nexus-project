/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'mage/translate',
    'mageplaza/core/jquery/popup'
], function ($, $t) {
    'use strict';

    $.widget('mageplaza.ajaxcartSuccess', {
        /**
         * @inheritDoc
         */
        els: {
            inputQty: '.mpajaxcart-qty',
            btnMinus: '.mpajaxcart-minus',
            btnPlus: '.mpajaxcart-plus',
            btnUpdate: '.mpajaxcart-update',
            btnContinue: '.continue-shopping',
            btnViewCart: '.action.btn-view-cart',
            closeEl: '.mp-btnclose',
            labelQty: '.mpajaxcart-msqty span',
            labelSubtotal: '.mpajaxcart-mssubtotal span',
            statusEL: '.mp-status',
            countdown: '.mpajaxcart-countdown'
        },

        _create: function () {
            this._eventListener();
        },

        _eventListener: function () {
            this.changeQty();
            this.autoClosePopup();
        },

        autoClosePopup: function () {
            var self = this,
                time = parseInt(self.options.timeCountdown);

            var countdown = setInterval(function () {
                time = time - 1;
                $(self.els.countdown).text('(' + time + 's)');

                if (time === 0) {
                    clearInterval(countdown);
                    $('#mpajaxcart-popup, .mpajaxcart-overlay').hide();
                }
            }, 1000);

            $(self.els.btnPlus).on('click', function () {
                clearInterval(countdown);
                $(self.els.countdown).text('');
            });

            $(self.els.btnMinus).on('click', function () {
                clearInterval(countdown);
                $(self.els.countdown).text('');
            });

            $(self.els.btnContinue).on('click', function () {
                clearInterval(countdown);
                $('#mpajaxcart-popup, .mpajaxcart-overlay').hide();
            });

            $(self.els.closeEl).on('click', function () {
                clearInterval(countdown);
            });

            $(self.els.btnViewCart).on('click', function () {
                window.location.href = self.options.checkoutCartUrl;
            })
        },

        changeQty: function () {
            var self = this;

            $(self.els.btnPlus).on('click', function () {
                var qty = parseInt($(self.els.inputQty).val());
                if (qty > 0) {
                    $(self.els.inputQty).val(qty + 1);
                    $(self.els.btnUpdate).fadeIn();
                    $(self.els.statusEL).text('');
                }
            });

            $(self.els.btnMinus).on('click', function () {
                var qty = parseInt($(self.els.inputQty).val());
                if (qty > 1) {
                    $(self.els.inputQty).val(qty - 1);
                    $(self.els.btnUpdate).fadeIn();
                    $(self.els.statusEL).text('');
                }
            });

            $(self.els.inputQty).keyup(function () {
                if ($(this).val() > 0) {
                    $(self.els.btnUpdate).fadeIn();
                }
            });

            $(self.els.btnUpdate).on('click', function () {
                var qty = parseInt($(self.els.inputQty).val());

                if (qty <= 0) {
                    $(self.els.statusEL).text($t('Please enter a number greater than 0.'));
                } else if (!qty) {
                    $(self.els.statusEL).text($t('This is a required field.'));
                } else {
                    $.ajax({
                        type: "POST",
                        url: self.options.updateQtyUrl,
                        data: {
                            'sku': self.options.productSku,
                            'qty': qty,
                            'mpquickview': 1
                        },
                        showLoader: true,
                        success: function (response) {
                            if (response.success === false) {
                                $(self.els.statusEL).text(response.error_message);
                            } else {
                                $(self.els.statusEL).text('');
                                $(self.els.labelQty).text(response.itemsQty + ' items');
                                $(self.els.labelSubtotal).text(response.subtotal);
                                $(self.els.btnUpdate).hide();
                            }
                        }
                    });
                }
            })
        }
    });

    return $.mageplaza.ajaxcartSuccess;
});

