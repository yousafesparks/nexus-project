<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Controller\Catalog\Product;

use Magento\Catalog\Controller\Product\View as CatalogView;
use Magento\Catalog\Helper\Product\View as ProductView;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Forward;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Mageplaza\QuickView\Helper\Data as HelperData;

/**
 * Class View
 * @package Mageplaza\QuickView\Controller\Catalog\Product\View
 */
class View extends CatalogView
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * View constructor.
     *
     * @param Context $context
     * @param ProductView $viewHelper
     * @param ForwardFactory $resultForwardFactory
     * @param PageFactory $resultPageFactory
     * @param HelperData $helperData
     */
    public function __construct(
        Context $context,
        ProductView $viewHelper,
        ForwardFactory $resultForwardFactory,
        PageFactory $resultPageFactory,
        HelperData $helperData
    ) {
        $this->_helperData = $helperData;

        parent::__construct($context, $viewHelper, $resultForwardFactory, $resultPageFactory);
    }

    /**
     * @return Forward|Redirect|void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        if (!$this->getRequest()->getParam('mpquickview')) {
            return parent::execute();
        }

        $categoryId     = (int) $this->getRequest()->getParam('category', false);
        $productId      = (int) $this->getRequest()->getParam('id');
        $specifyOptions = $this->getRequest()->getParam('options');

        $params = new DataObject();
        $params->setCategoryId($categoryId)
            ->setSpecifyOptions($specifyOptions);
        $page = $this->resultPageFactory->create();
        $this->viewHelper->prepareAndRender($page, $productId, $this, $params);

        $this->_view->loadLayout();
        $layout = $this->_view->getLayout();

        if ($this->getRequest()->getParam('mpquickview') === 'cart') {
            $layout->unsetElement('product.info.review');
            $layout->unsetElement('reviews.tab');
            $layout->unsetElement('product.info.overview');
            $layout->unsetElement('product.info.stock.sku');
            $layout->unsetElement('product.attributes');
            $layout->unsetElement('product.info.details');
            $layout->unsetElement('product.info.upsell');
            $layout->unsetElement('catalog.product.related');
            $layout->unsetElement('product.info.addto');
            $layout->unsetElement('product.info.mailto');
            $layout->unsetElement('addtocart.shortcut.buttons');
            $layout->unsetElement('addtocart.shortcut.buttons.additional');
        } else {
            $infoConfig = explode(',', $this->_helperData->getQuickViewConfig('popup_info'));
            $this->_helperData->removeInfo($infoConfig, $layout);
        }

        $this->getResponse()->setBody($layout->renderElement('main.content'));
    }
}
