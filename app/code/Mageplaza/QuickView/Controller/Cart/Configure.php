<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Controller\Cart;

use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Checkout\Model\Session;
use Magento\Framework;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\QuickView\Helper\Data as HelperData;

/**
 * Class Configure
 * @package Mageplaza\QuickView\Controller\Cart
 */
class Configure extends \Magento\Checkout\Controller\Cart\Configure
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * Configure constructor.
     *
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param HelperData $helperData
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        CustomerCart $cart,
        HelperData $helperData
    )
    {
        $this->_helperData = $helperData;

        parent::__construct($context, $scopeConfig, $checkoutSession, $storeManager, $formKeyValidator, $cart);
    }

    /**
     * @return Framework\Controller\Result\Redirect|Framework\View\Result\Page
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('mpquickview')) {
            $itemId = (int)$this->getRequest()->getParam('id');
            $productId = (int)$this->getRequest()->getParam('product_id');
            $quoteItem = null;
            if ($itemId) {
                $quoteItem = $this->cart->getQuote()->getItemById($itemId);
            }

            if (!$quoteItem || $productId != $quoteItem->getProduct()->getId()) {
                $this->messageManager->addError(__("We can't find the quote item."));

                return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('checkout/cart');
            }

            $params = new DataObject();
            $params->setCategoryId(false);
            $params->setConfigureMode(true);
            $params->setBuyRequest($quoteItem->getBuyRequest());

            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $this->_objectManager->get(\Magento\Catalog\Helper\Product\View::class)
                ->prepareAndRender(
                    $resultPage,
                    $quoteItem->getProduct()->getId(),
                    $this,
                    $params
                );

            $this->_view->loadLayout();
            $layout = $this->_view->getLayout();

            $infoConfig = explode(',', $this->_helperData->getQuickViewConfig('edit_cart_info'));
            $this->_helperData->removeInfo($infoConfig, $layout);

            $result['html_content'] = $layout->renderElement('main.content');
            $result['qty'] = $quoteItem->getQty();

            return $this->getResponse()->representJson(HelperData::jsonEncode($result));
        }

        return parent::execute();
    }
}
