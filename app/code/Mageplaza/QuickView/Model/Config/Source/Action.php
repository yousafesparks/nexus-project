<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Action
 * @package Mageplaza\QuickView\Model\Config\Source
 */
class Action implements ArrayInterface
{
    const ADD_TO_CART     = 1;
    const ADD_TO_COMPARE  = 2;
    const ADD_TO_WISHLISH = 3;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];

        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    protected function toArray()
    {
        return [
            self::ADD_TO_CART     => __('Add To Cart'),
            self::ADD_TO_COMPARE  => __('Add To Compare'),
            self::ADD_TO_WISHLISH => __('Add To Wishlist'),
        ];
    }
}
