<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class PopupInfo
 * @package Mageplaza\QuickView\Model\Config\Source
 */
class PopupInfo implements ArrayInterface
{
    const PRODUCT_IMAGE           = 'product_img';
    const PRODUCT_IMAGE_THUMBNAIL = 'product_img_thumbnail';
    const REVIEW                  = 'review';
    const ADD_NEW_REVIEW          = 'add_new_review';
    const PRODUCT_DESCRIPTION     = 'product_description';
    const SKU                     = 'sku';
    const STOCK_STATUS            = 'stock_status';
    const PRODUCT_TABS            = 'product_tabs';
    const UPSALES_PRODUCTS        = 'upsales_product';
    const RELATED_PRODUCTS        = 'related_product';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];

        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    protected function toArray()
    {
        return [
            self::PRODUCT_IMAGE           => __('Product Image'),
            self::PRODUCT_IMAGE_THUMBNAIL => __('Product Image Thumbnail'),
            self::REVIEW                  => __('Review'),
            self::ADD_NEW_REVIEW          => __('Add New Review'),
            self::PRODUCT_DESCRIPTION     => __('Product Description'),
            self::SKU                     => __('SKU'),
            self::STOCK_STATUS            => __('Stock Status'),
            self::PRODUCT_TABS            => __('Product Tabs'),
            self::UPSALES_PRODUCTS        => __('Upsales Products'),
            self::RELATED_PRODUCTS        => __('Related Products')
        ];
    }

    /**
     * @return array
     */
    public function arrayMap()
    {
        return [
            0 => self::PRODUCT_IMAGE,
            1 => self::PRODUCT_IMAGE_THUMBNAIL,
            2 => self::REVIEW,
            3 => self::ADD_NEW_REVIEW,
            4 => self::PRODUCT_DESCRIPTION,
            5 => self::SKU,
            6 => self::STOCK_STATUS,
            7 => self::PRODUCT_TABS,
            8 => self::UPSALES_PRODUCTS,
            9 => self::RELATED_PRODUCTS
        ];
    }
}
