<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_DailyDeal
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Animate
 * @package Mageplaza\QuickView\Model\Config\Source
 */
class Animate implements ArrayInterface
{
    const ZOOM            = 'mfp-zoom-in';
    const NEWSPAPER       = 'mfp-newspaper';
    const HORIZONTAL_MOVE = 'mfp-move-horizontal';
    const MOVE_FROM_TOP   = 'mfp-move-from-top';
    const UNFOLD          = 'mfp-3d-unfold';
    const ZOOM_OUT        = 'mfp-zoom-out';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::ZOOM, 'label' => __('Zoom')],
            ['value' => self::NEWSPAPER, 'label' => __('Newspaper')],
            ['value' => self::HORIZONTAL_MOVE, 'label' => __('Horizontal Move')],
            ['value' => self::MOVE_FROM_TOP, 'label' => __('Move From Top')],
            ['value' => self::UNFOLD, 'label' => __('3D Unfold')],
            ['value' => self::ZOOM_OUT, 'label' => __('Zoom-out')]
        ];
    }
}
