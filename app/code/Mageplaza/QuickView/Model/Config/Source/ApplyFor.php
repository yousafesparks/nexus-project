<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class ApplyFor
 * @package Mageplaza\QuickView\Model\Config\Source
 */
class ApplyFor implements ArrayInterface
{
    const PRODUCT_LIST_PAGE   = 'catalog_category_view';
    const HOMEPAGE            = 'cms_index_index';
    const PRODUCT_DETAIL_PAGE = 'catalog_product_view';
    const CHECKOUT_CART       = 'checkout_cart_index';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];

        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    protected function toArray()
    {
        return [
            self::PRODUCT_LIST_PAGE   => __('Product Listing Page'),
            self::HOMEPAGE            => __('Home Page'),
            self::PRODUCT_DETAIL_PAGE => __('Product Detail Page'),
            self::CHECKOUT_CART       => __('Checkout Cart Page')
        ];
    }
}
