<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Core\Helper\AbstractData;
use Mageplaza\QuickView\Model\Config\Source\Action;
use Mageplaza\QuickView\Model\Config\Source\ApplyForCart;
use Mageplaza\QuickView\Model\Config\Source\PopupInfo;

/**
 * Class Data
 * @package Mageplaza\QuickView\Helper
 */
class Data extends AbstractData
{
    const CONFIG_MODULE_PATH = 'mpquickview';

    /**
     * @var PopupInfo
     */
    protected $_popupInfo;

    /**
     * @var LayoutFactory
     */
    protected $_layoutFactory;

    /**
     * @var PriceHelper
     */
    protected $_priceHelper;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param PopupInfo $popupInfo
     * @param LayoutFactory $layoutFactory
     * @param PriceHelper $priceHelper
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        PopupInfo $popupInfo,
        LayoutFactory $layoutFactory,
        PriceHelper $priceHelper
    ) {
        $this->_popupInfo     = $popupInfo;
        $this->_layoutFactory = $layoutFactory;
        $this->_priceHelper   = $priceHelper;

        parent::__construct($context, $objectManager, $storeManager);
    }

    /**
     * Get quickview popup config
     *
     * @param $code
     * @param null $storeId
     *
     * @return mixed
     */
    public function getQuickViewConfig($code, $storeId = null)
    {
        return $this->getModuleConfig('quickview_config/' . $code, $storeId);
    }

    /**
     * Get Ajax Cart config
     *
     * @param $code
     * @param null $storeId
     *
     * @return mixed
     */
    public function getAjaxCartConfig($code, $storeId = null)
    {
        return $this->getModuleConfig('ajax_cart_config/' . $code, $storeId);
    }

    /**
     * @return mixed
     */
    public function isEnableAjaxCart()
    {
        return $this->getAjaxCartConfig('enabled');
    }

    /**
     * @return bool
     */
    public function isAddToCart()
    {
        if ($this->isEnableAjaxCart()) {
            $config = explode(',', $this->getAjaxCartConfig('action'));

            return in_array(Action::ADD_TO_CART, $config, false);
        }

        return false;
    }

    /**
     * Check apply ajax cart for quickview popup
     *
     * @return bool
     */
    public function isAjaxCartQuickView()
    {
        $config = explode(',', $this->getAjaxCartConfig('apply_for'));

        return in_array(ApplyForCart::QUICKVIEW_POPUP, $config, false);
    }

    /**
     * @return bool
     */
    public function isAjaxWishlist()
    {
        if ($this->isEnableAjaxCart()) {
            $config = explode(',', $this->getAjaxCartConfig('action'));

            return in_array(Action::ADD_TO_WISHLISH, $config, false);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isAjaxCompare()
    {
        if ($this->isEnableAjaxCart()) {
            $config = explode(',', $this->getAjaxCartConfig('action'));

            return in_array(Action::ADD_TO_COMPARE, $config, false);
        }

        return false;
    }

    /**
     * Get list information remove on popup quickview
     *
     * @param $config
     *
     * @return array
     */
    public function listInfoRemove($config)
    {
        $listInfo   = $this->_popupInfo->arrayMap();
        $listRemove = [];

        foreach ($listInfo as $info) {
            if (!in_array($info, $config, false)) {
                $listRemove[] = $info;
            }
        }

        return $listRemove;
    }

    /**
     * Remove information on popup
     *
     * @param $config
     * @param $layout
     */
    public function removeInfo($config, $layout)
    {
        $listRemove = $this->listInfoRemove($config);

        if (in_array(PopupInfo::REVIEW, $listRemove, false)
            && in_array(PopupInfo::ADD_NEW_REVIEW, $listRemove, false)
        ) {
            $layout->unsetElement('product.info.review');
        }

        if (in_array(PopupInfo::REVIEW, $listRemove, false)
            && in_array(PopupInfo::ADD_NEW_REVIEW, $listRemove, false)
            && in_array(PopupInfo::PRODUCT_TABS, $listRemove, false)
        ) {
            $layout->unsetElement('reviews.tab');
        }

        if (in_array(PopupInfo::REVIEW, $listRemove, false)
            && in_array(PopupInfo::ADD_NEW_REVIEW, $listRemove, false)
            && in_array(PopupInfo::PRODUCT_DESCRIPTION, $listRemove, false)
            && in_array(PopupInfo::PRODUCT_TABS, $listRemove, false)
        ) {
            $layout->unsetElement('product.info.details');
        }

        foreach ($listRemove as $info) {
            switch ($info) {
                case PopupInfo::SKU:
                    $layout->unsetElement('product.info.sku');
                    break;
                case PopupInfo::UPSALES_PRODUCTS:
                    $layout->unsetElement('product.info.upsell');
                    break;
                case PopupInfo::RELATED_PRODUCTS:
                    $layout->unsetElement('catalog.product.related');
                    break;
            }
        }
    }

    /**
     * Get format price currency
     *
     * @param $price
     *
     * @return float|int|string
     */
    public function getPriceCurrency($price)
    {
        if ($price) {
            return $this->_priceHelper->currency(number_format($price, 2, '.', ''), true, false);
        }

        return 0;
    }

    public function getCurrentThemeId()
    {
        return $this->getConfigValue(DesignInterface::XML_PATH_THEME_ID);
    }
}
