<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Plugin\Controller\Cart;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Controller\Cart\Add as MagentoAdd;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\QuickView\Block\AjaxCart\Popup as AjaxCartPopup;
use Mageplaza\QuickView\Helper\Data as HelperData;

/**
 * Class Add
 * @package Mageplaza\QuickView\Plugin\Controller\Cart
 */
class Add
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var AjaxCartPopup
     */
    protected $_popup;

    /**
     * Add constructor.
     *
     * @param HelperData $helperData
     * @param StoreManagerInterface $storeManager
     * @param ManagerInterface $messageManager
     * @param PageFactory $pageFactory
     * @param AjaxCartPopup $popup
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        HelperData $helperData,
        StoreManagerInterface $storeManager,
        ManagerInterface $messageManager,
        PageFactory $pageFactory,
        AjaxCartPopup $popup,
        ProductRepositoryInterface $productRepository
    ) {
        $this->_helperData       = $helperData;
        $this->_storeManager     = $storeManager;
        $this->messageManager    = $messageManager;
        $this->pageFactory       = $pageFactory;
        $this->_popup            = $popup;
        $this->productRepository = $productRepository;
    }

    /**
     * @param MagentoAdd $subject
     * @param $result
     *
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function afterExecute(MagentoAdd $subject, $result)
    {
        if ($this->_helperData->isEnabled()) {
            $isAjaxCart       = $subject->getRequest()->getParam('mpajaxcart');
            $isAjaxCartPopup  = $subject->getRequest()->getParam('mppopup');
            $isQuickViewPopup = $subject->getRequest()->getParam('mpquickview');
            $productId        = (int) $subject->getRequest()->getParam('product');
            $product          = $this->_initProductById($productId);
            $layout           = $this->pageFactory->create()->getLayout();
            $backUrl          = $subject->getResponse()->getContent();

            if ($this->_helperData->isAddToCart() && $isAjaxCart) {
                $messageList = [];
                if ($itemMessage = $this->messageManager->getMessages()->getItems()) {
                    foreach ($itemMessage as $message) {
                        $messageList[] = $message->getText();
                    }

                    if (!empty(array_filter($messageList))) {
                        $result['error'] = true;
                        $result['message'] = $messageList;
                    }
                }
                $this->messageManager->getMessages()->clear();
                $result['backUrl'] = HelperData::jsonDecode($backUrl);

                if ($backUrl === '[]') {
                    $result['success']    = true;
                    $result['html_popup'] = $layout->createBLock(AjaxCartPopup::class)
                        ->setTemplate('Mageplaza_QuickView::ajaxcart/success.phtml')
                        ->setProductItem($product)
                        ->toHtml();
                } else {
                    $result['detailPage'] = [
                        'url'       => $this->_popup->getUrl('catalog/product/view'),
                        'productId' => $productId
                    ];
                }

                return $subject->getResponse()->representJson(HelperData::jsonEncode($result));
            }

            if ($isAjaxCartPopup) {  // process add to cart on ajax cart popup
                if ($backUrl === '[]') {
                    $result['success'] = true;
                    $result['html']    = $layout->createBLock(AjaxCartPopup::class)
                        ->setTemplate('Mageplaza_QuickView::ajaxcart/success.phtml')
                        ->setProductItem($product)
                        ->toHtml();
                } else {
                    $messageList     = [];
                    $result['error'] = true;
                    if ($this->messageManager->getMessages()->getItems()) {
                        foreach ($this->messageManager->getMessages()->getItems() as $message) {
                            $messageList[] = $message->getText();
                        }

                        $result['message'] = $messageList;
                    }
                }

                return $subject->getResponse()->representJson(HelperData::jsonEncode($result));
            }

            /**
             * Process if don't apply ajax add to cart on quickview popup
             */
            if ($isQuickViewPopup) {
                $messageList     = [];
                $result['error'] = HelperData::jsonDecode($backUrl);
                if ($this->messageManager->getMessages()->getItems()) {
                    foreach ($this->messageManager->getMessages()->getItems() as $message) {
                        $messageList[] = $message->getText();
                    }

                    $result['message'] = $messageList;
                }

                return $subject->getResponse()->representJson(HelperData::jsonEncode($result));
            }
        }

        return $result;
    }

    /**
     * @param $productId
     *
     * @return bool|ProductInterface
     * @throws NoSuchEntityException
     */
    private function _initProductById($productId)
    {
        if ($productId) {
            $storeId = $this->_storeManager->getStore()->getId();
            try {
                return $this->productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }

        return false;
    }
}
