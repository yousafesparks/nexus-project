<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Plugin\Controller\Sidebar;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Sidebar;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Mageplaza\QuickView\Helper\Data as HelperData;
use Psr\Log\LoggerInterface;

/**
 * Class UpdateItemQty
 * @package Mageplaza\QuickView\Plugin\Controller\Sidebar
 */
class UpdateItemQty
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var Cart
     */
    protected $_cart;

    /**
     * @var Sidebar
     */
    protected $sidebar;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $jsonHelper;

    /**
     * @var ResponseInterface
     */
    protected $_response;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var PriceHelper
     */
    protected $_priceHelper;

    /**
     * UpdateItemQty constructor.
     *
     * @param Cart $cart
     * @param Sidebar $sidebar
     * @param LoggerInterface $logger
     * @param Data $jsonHelper
     * @param ResponseInterface $response
     * @param HelperData $helperData
     * @param JsonFactory $resultJsonFactory
     * @param PriceHelper $priceHelper
     */
    public function __construct(
        Cart $cart,
        Sidebar $sidebar,
        LoggerInterface $logger,
        Data $jsonHelper,
        ResponseInterface $response,
        HelperData $helperData,
        JsonFactory $resultJsonFactory,
        PriceHelper $priceHelper
    ) {
        $this->_helperData       = $helperData;
        $this->_cart             = $cart;
        $this->sidebar           = $sidebar;
        $this->logger            = $logger;
        $this->jsonHelper        = $jsonHelper;
        $this->_response         = $response;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_priceHelper      = $priceHelper;
    }

    /**
     * @param \Magento\Checkout\Controller\Sidebar\UpdateItemQty $subject
     * @param $proceed
     *
     * @return Json
     */
    public function aroundExecute(\Magento\Checkout\Controller\Sidebar\UpdateItemQty $subject, $proceed)
    {
        if (!$this->_helperData->isEnabled() || !$subject->getRequest()->getParam('mpquickview')) {
            return $proceed();
        }

        $productSku = $subject->getRequest()->getParam('sku');
        $itemQty    = $subject->getRequest()->getParam('qty');
        $items      = $this->_cart->getQuote()->getAllVisibleItems();
        $itemId     = 0;

        foreach ($items as $item) {
            if ($productSku === $item->getSku()) {
                $itemId = $item->getItemId();
            }
        }

        try {
            $this->sidebar->checkQuoteItem($itemId);
            $this->sidebar->updateQuoteItem($itemId, $itemQty);

            $itemsQty       = $this->_cart->getItemsQty();
            $subtotal       = $this->_cart->getQuote()->getSubtotal();
            $formatSubtotal = $this->_helperData->getPriceCurrency($subtotal);

            $result = $this->resultJsonFactory->create();
            $result->setData([
                'itemsQty' => $itemsQty,
                'subtotal' => $formatSubtotal
            ]);

            return $result;
        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (Exception $e) {
            $this->logger->critical($e);

            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * @param string $error
     *
     * @return mixed
     */
    protected function jsonResponse($error = '')
    {
        return $this->_response->representJson(
            $this->jsonHelper->jsonEncode($this->sidebar->getResponseData($error))
        );
    }
}
