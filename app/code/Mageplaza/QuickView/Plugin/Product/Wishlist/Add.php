<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Plugin\Product\Wishlist;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Wishlist\Controller\AbstractIndex;
use Magento\Wishlist\Controller\Index\Add as AddWishlist;
use Mageplaza\QuickView\Helper\Data as HelperData;

/**
 * Class Add
 * @package Mageplaza\QuickView\Plugin\Product\Wishlist
 */
class Add
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * Add constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param HelperData $helperData
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        HelperData $helperData
    ) {
        $this->productRepository = $productRepository;
        $this->_helperData       = $helperData;
    }

    /**
     * @param AbstractIndex $subject
     * @param $result
     *
     * @return array|Http
     * @throws NoSuchEntityException
     */
    public function afterExecute(AbstractIndex $subject, $result)
    {
        $request = $subject->getRequest();
        if (!$request->isAjax()) {
            return $result;
        }

        if ($subject instanceof AddWishlist) {
            $productId   = (int) $request->getParam('product');
            $product     = $this->productRepository->getById($productId);
            $productName = $product->getName();

            $result = [
                'success' => true,
                'message' => __('%1 has been added to your Wish List.', $productName)
            ];
        }

        return $subject->getResponse()->representJson(HelperData::jsonEncode($result));
    }
}
