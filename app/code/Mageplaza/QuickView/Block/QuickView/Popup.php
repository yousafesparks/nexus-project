<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_QuickView
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\QuickView\Block\QuickView;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Mageplaza\QuickView\Helper\Data as HelperData;
use Mageplaza\QuickView\Helper\Image as HelperImage;
use Mageplaza\QuickView\Model\Config\Source\ApplyFor;
use Mageplaza\QuickView\Model\Config\Source\IconDisplay;

/**
 * Class QuickView
 * @package Mageplaza\QuickView\Block\QuickView
 */
class Popup extends Template
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var HelperImage
     */
    protected $_helperImage;

    /**
     * Popup constructor.
     *
     * @param Context $context
     * @param HelperData $helperData
     * @param HelperImage $helperImage
     * @param array $data
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        HelperImage $helperImage,
        array $data = []
    ) {
        $this->_helperData  = $helperData;
        $this->_helperImage = $helperImage;

        parent::__construct($context, $data);
    }

    /**
     * Get Button QuickView Html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $html = "<a class='mpquickview-button' href='#mpquickview-popup' data-effect='"
            . $this->getEffectPopup() . "'>";

        if ($this->getDisplayLabel() === IconDisplay::ICON) {
            $html .= "<img src='" . $this->getButtonIconUrl() . "'/>";
        }

        if ($this->getDisplayLabel() === IconDisplay::LABEL) {
            $html .= "<span class='mpquickview-label'>" . $this->getButtonLabel() . '</span>';
        }

        $html .= '</a>';

        return $html;
    }

    /**
     * Get Button Icon Url
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getButtonIconUrl()
    {
        if ($this->_helperData->getQuickViewConfig('icon')) {
            return $this->_helperImage->getBaseMediaUrl() . '/'
                . $this->_helperImage->getMediaPath($this->_helperData->getQuickViewConfig('icon'), 'icon');
        }

        return $this->getUrlImg('btn-icon.svg');
    }

    /**
     * Get Img Url
     *
     * @param $img
     *
     * @return string
     */
    public function getUrlImg($img)
    {
        return $this->getViewFileUrl('Mageplaza_QuickView::media/' . $img);
    }

    /**
     * Get Label Button
     *
     * @return Phrase|mixed
     */
    public function getButtonLabel()
    {
        $config = $this->_helperData->getQuickViewConfig('label');

        return $config ? $config : __('Quick View');
    }

    /**
     * @return mixed
     */
    public function getLabelColor()
    {
        return $this->_helperData->getQuickViewConfig('text_color');
    }

    /**
     * @return mixed
     */
    public function getBackgroundColor()
    {
        return $this->_helperData->getQuickViewConfig('bg_color');
    }

    /**
     * @return mixed
     */
    public function getDisplayLabel()
    {
        return $this->_helperData->getQuickViewConfig('icon_display');
    }

    /**
     * @return mixed
     */
    public function isEnableQuickView()
    {
        return $this->_helperData->isEnabled() && $this->_helperData->getQuickViewConfig('enabled');
    }

    /**
     * Get Effect to show popup
     *
     * @return mixed
     */
    public function getEffectPopup()
    {
        return $this->_helperData->getQuickViewConfig('animate');
    }

    /**
     * @return string
     */
    public function getCatalogViewUrl()
    {
        return $this->getUrl('catalog/product/view');
    }

    /**
     * @return string
     */
    public function getCheckoutCartConfigureUrl()
    {
        return $this->getUrl('checkout/cart/configure');
    }

    /**
     * @return string
     */
    public function getPopupData()
    {
        $params = [
            'buttonHtml'      => $this->getButtonHtml(),
            'url'             => $this->getCatalogViewUrl(),
            'urlLoader'       => $this->getViewFileUrl('images/loader-1.gif'),
            'isApplyQV'       => $this->isAddToCartQV(),
            'isCompareQV'     => $this->isCompareQuickView(),
            'mpeffect'        => $this->getEffectPopup(),
            'productNextPrev' => $this->getProductNextPrev(),
            'urlCart'         => $this->getCheckoutCartConfigureUrl()

        ];

        return HelperData::jsonEncode($params);
    }

    /**
     * @return string
     */
    public function getProductNextPrev()
    {
        return $this->getUrl('mpquickview/catalog/productnextprev');
    }

    /**
     * @return bool
     */
    public function isAddToCartQV()
    {
        return ($this->_helperData->isAjaxCartQuickView() && $this->_helperData->isAddToCart());
    }

    /**
     * Check compare ajax on quickview popup
     * @return bool
     */
    public function isCompareQuickView()
    {
        return $this->_helperData->isAjaxCartQuickView() && $this->_helperData->isAjaxCompare();
    }

    /**
     * @return array
     */
    public function getPopupInfoConfig()
    {
        $config = $this->_helperData->getQuickViewConfig('popup_info');

        return explode(',', $config);
    }

    /**
     * @return bool
     */
    public function checkApply()
    {
        $action = $this->_request->getFullActionName();
        $config = $this->_helperData->getQuickViewConfig('apply_for');

        if (in_array($action, explode(',', $config), false)) {
            return true;
        }

        if ($action === 'catalogsearch_result_index'
            && in_array(ApplyFor::PRODUCT_LIST_PAGE, explode(',', $config), false)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @return HelperData
     */
    public function getHelperData()
    {
        return $this->_helperData;
    }
}
