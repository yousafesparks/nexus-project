<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SaveCart
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SaveCart\Block\Adminhtml\Grid\Renderer;

use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Customer\Api\CustomerRepositoryInterfaceFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\Store;
use Magento\Directory\Model\CurrencyFactory;
use Mageplaza\SaveCart\Helper\Data;

/**
 * Class ProductPrice
 * @package Mageplaza\SaveCart\Block\Adminhtml\Grid\Renderer
 */
class ProductPrice extends AbstractRenderer
{
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var CustomerRepositoryInterfaceFactory
     */
    protected $customerRepositoryFactory;

    /**
     * @var Store
     */
    protected $storeManager;

    /**
     * @var CurrencyFactory
     */
    protected $currencyFactory;

    /**
     * ProductPrice constructor.
     * @param Context $context
     * @param Data $helperData
     * @param CustomerRepositoryInterfaceFactory $customerRepositoryFactory
     * @param Store $storeManager
     * @param CurrencyFactory $currencyFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helperData,
        CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
        Store $storeManager,
        CurrencyFactory $currencyFactory,
        array $data = []
    ) {
        $this->helperData = $helperData;
        $this->customerRepositoryFactory = $customerRepositoryFactory;
        $this->storeManager = $storeManager;
        $this->currencyFactory = $currencyFactory;

        parent::__construct($context, $data);
    }

    /**
     * @param DataObject $row
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function render(DataObject $row)
    {
        $customerGroupId = $this->customerRepositoryFactory->create()->getById($row->getCustomerId());

        return $this->getPrice($row, $row->getStoreId(), $customerGroupId);
    }

    /**
     * @param $item
     * @param $storeId
     * @param null $customerGroupId
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getPrice($item, $storeId, $customerGroupId = null)
    {
        $currencyCode = $this->getBaseCurrencyCode($storeId);
        $basePurchaseCurrency = $this->currencyFactory->create()->load($currencyCode);
        $price = $this->helperData->getProductFinalPrice($item, $storeId, $customerGroupId);

        return $basePurchaseCurrency->format($price, [], false);
    }

    /**
     * @param $storeId
     * @return string|null
     * @throws LocalizedException
     */
    protected function getBaseCurrencyCode($storeId)
    {
        return $this->storeManager->load($storeId)->getBaseCurrencyCode();
    }
}
