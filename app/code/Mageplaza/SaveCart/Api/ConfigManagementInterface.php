<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SaveCart
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SaveCart\Api;

/**
 * Interface ConfigManagementInterface
 * @package Mageplaza\SaveCart\Api
 */
interface ConfigManagementInterface
{
    /**
     * @param bool $isArray
     *
     * @return \Mageplaza\SaveCart\Api\Data\ConfigInterface|array
     */
    public function get($isArray = false);
}
