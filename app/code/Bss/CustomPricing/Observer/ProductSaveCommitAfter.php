<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\CustomPricing\Observer;

use Bss\CustomPricing\Api\ProductPriceRepositoryInterface;
use Magento\Framework\Event\Observer;

/**
 * After commit save product
 */
class ProductSaveCommitAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var ProductPriceRepositoryInterface
     */
    protected $productPriceRepository;

    /**
     * @var \Bss\CustomPricing\Helper\Data
     */
    protected $helper;

    /**
     * @var \Bss\CustomPricing\Helper\ProductSave
     */
    protected $helperProductSave;

    /**
     * ProductSaveCommitAfter constructor.
     * @param ProductPriceRepositoryInterface $productPriceRepository
     * @param \Bss\CustomPricing\Helper\Data $helper
     * @param \Bss\CustomPricing\Helper\ProductSave $helperProductSave
     */
    public function __construct(
        ProductPriceRepositoryInterface $productPriceRepository,
        \Bss\CustomPricing\Helper\Data $helper,
        \Bss\CustomPricing\Helper\ProductSave $helperProductSave
    ) {
        $this->productPriceRepository = $productPriceRepository;
        $this->helper = $helper;
        $this->helperProductSave = $helperProductSave;
    }

    /**
     * Validate the saved product and push to the bss price table
     *
     * @param Observer $observer
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        if (!$this->helper->isEnabled()) {
            return $this;
        }

        $product = $observer->getProduct();
        $websiteIds = $product->getWebsiteIds();
        $priceRules = $this->helperProductSave->getPriceRules();
        foreach ($priceRules->getItems() as $rule) {
            if (!in_array($rule->getWebsiteId(), $websiteIds)) {
                continue;
            }
            $isValidated = $rule->getConditions()->validate($product);
            $appliedId = $this->productPriceRepository->hasProduct($rule->getId(), $product->getId());
            if (!$isValidated && $appliedId) {
                $this->productPriceRepository->deleteById($appliedId);
                return $this;
            }
            if ($isValidated) {
                $this->helperProductSave->insertOrUpdateProductPrice($rule, $product);
            }
        }
        return $this;
    }
}
