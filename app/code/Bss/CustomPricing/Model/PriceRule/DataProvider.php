<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\CustomPricing\Model\PriceRule;

use Bss\CustomPricing\Model\ResourceModel\PriceRule\CollectionFactory;

/**
 * Price rule data provider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     * DataProvider constructor.
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $ruleCollectionFactory
     * @param \Magento\Backend\Model\Session $backendSession
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $ruleCollectionFactory,
        \Magento\Backend\Model\Session $backendSession,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $ruleCollectionFactory->create();
        $this->backendSession = $backendSession;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritDoc
     */
    public function getData()
    {
        if (null !== $this->loadedData) {
            return $this->loadedData;
        }
        if ($oldData = $this->backendSession->getFormData(true)) {
            $this->loadedData[$oldData["id"]]["general_information"] = $oldData;
            $this->loadedData[$oldData["id"]]["general_information"]["cant_edit_website"] = true;
            $this->backendSession->unsFormData();
        } else {
            foreach ($this->collection->getItems() as $item) {
                $this->loadedData[$item->getId()]["general_information"] = $item->getData();
                $this->loadedData[$item->getId()]['general_information']['price_method'] = null;
                $this->loadedData[$item->getId()]["general_information"]["cant_edit_website"] = true;
                $this->loadedData[$item->getId()]['general_information']['customer_condition']['is_not_logged_rule'] = $item->getIsNotLoggedRule();
            }
        }
        return $this->loadedData;
    }
}
