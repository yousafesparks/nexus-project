<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\CustomPricing\Controller\Adminhtml\PriceRules;

use Bss\CustomPricing\Api\AppliedCustomersRepositoryInterface;
use Bss\CustomPricing\Api\PriceRuleRepositoryInterface;
use Bss\CustomPricing\Api\ProductPriceRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * The place where save the price rule
 *
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends \Bss\CustomPricing\Controller\Adminhtml\PriceRule
{
    /**
     * @var ProductPriceRepositoryInterface
     */
    protected $productPriceRepository;

    /**
     * @var \Bss\CustomPricing\Model\ResourceModel\ProductPrice
     */
    protected $productPriceResource;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var AppliedCustomersRepositoryInterface
     */
    protected $appliedCustomersRepository;

    /**
     * @var \Bss\CustomPricing\Model\ResourceModel\AppliedCustomers
     */
    protected $appliedCustomersResource;

    /**
     * Old customer
     *
     * @var array
     */
    protected $oldCustomer = [];

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $typeList;

    /**
     * @var \Bss\CustomPricing\Helper\Data
     */
    protected $helper;

    /**
     * Save constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter
     * @param \Bss\CustomPricing\Model\PriceRuleFactory $ruleFactory
     * @param PriceRuleRepositoryInterface $priceRuleRepository
     * @param \Psr\Log\LoggerInterface $logger
     * @param ProductPriceRepositoryInterface $productPriceRepository
     * @param AppliedCustomersRepositoryInterface $appliedCustomersRepository
     * @param \Bss\CustomPricing\Model\ResourceModel\ProductPrice $productPriceResource
     * @param \Bss\CustomPricing\Model\ResourceModel\AppliedCustomers $appliedCustomersResource
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Bss\CustomPricing\Helper\Data $helper
     * @param \Magento\Framework\App\Cache\TypeListInterface $typeList
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
        \Bss\CustomPricing\Model\PriceRuleFactory $ruleFactory,
        PriceRuleRepositoryInterface $priceRuleRepository,
        \Psr\Log\LoggerInterface $logger,
        ProductPriceRepositoryInterface $productPriceRepository,
        AppliedCustomersRepositoryInterface $appliedCustomersRepository,
        \Bss\CustomPricing\Model\ResourceModel\ProductPrice $productPriceResource,
        \Bss\CustomPricing\Model\ResourceModel\AppliedCustomers $appliedCustomersResource,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Bss\CustomPricing\Helper\Data $helper,
        \Magento\Framework\App\Cache\TypeListInterface $typeList
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $dateFilter,
            $ruleFactory,
            $priceRuleRepository,
            $logger
        );
        $this->productPriceRepository = $productPriceRepository;
        $this->appliedCustomersRepository = $appliedCustomersRepository;
        $this->productPriceResource = $productPriceResource;
        $this->appliedCustomersResource = $appliedCustomersResource;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->typeList = $typeList;
        $this->helper = $helper;
    }

    /**
     * @inheritDoc
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        if (!$this->getRequest()->getPostValue()) {
            return $redirect->setPath('custom_pricing/*/');
        }

        $data = $this->getRequest()->getPostValue();
        $data = $this->prepareData($data);
        try {
            $model = $this->ruleFactory->create();
            $id = $this->getRequest()->getParam('general_information')["id"] ?? null;
            if ($id) {
                $model = $this->priceRuleRepository->getById($id);
            } else {
                unset($data["id"]);
            }
            $validateResult = $model->validateData(new \Magento\Framework\DataObject($data));
            if ($validateResult !== true) {
                foreach ($validateResult as $errorMessage) {
                    $this->messageManager->addErrorMessage($errorMessage);
                }
                $this->_session->setPageData($data);
                $this->_session->setFormData($data);
                $redirect->setPath('custom_pricing/*/edit', ['id' => $model->getId()]);
                return $redirect;
            }
            $model->loadPost($data);
            $this->_session->setPageData($model->getData());
            $this->priceRuleRepository->save($model);

            /* validate and push products to product price tab */
            $this->saveProductPrice($model);

            /* validate and push customers to applied customers tab */
            $this->saveAppliedCustomers($model);

            $this->messageManager->addSuccessMessage(__('You saved the rule.'));

            /* Need clear cache after save rule */
            $this->typeList->invalidate(
                [
                    \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
                    \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER
                ]
            );
            $this->_session->setPageData(false);
            if ($this->getRequest()->getParam('back')) {
                $redirect->setPath('custom_pricing/*/edit', ['id' => $model->getId()]);
                return $redirect;
            }
            $redirect->setPath('custom_pricing/*/');
            return $redirect;
        } catch (CouldNotSaveException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $id = $this->getRequest()->getParam('general_information')["id"] ?? null;
            if (!empty($id)) {
                $this->_session->setFormData($data);
                $redirect->setPath('custom_pricing/*/edit', ['id' => $id]);
            } else {
                $redirect->setPath('custom_pricing/*/new');
            }
            return $redirect;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('Something went wrong while saving the rule data. Please review the error log.')
            );
            $this->logger->critical($e);
            $data = !empty($data) ? $data : [];
            $this->_session->setPageData($data);
            $this->_session->setFormData($data);
            $redirect->setPath(
                'custom_pricing/*/edit',
                ['id' => $this->getRequest()->getParam('general_information')["id"] ?? null]
            );
            return $redirect;
        }
    }

    /**
     * Save custom price for product by rule
     *
     * @param \Bss\CustomPricing\Model\PriceRule $rule
     * @throws CouldNotSaveException
     */
    protected function saveProductPrice($rule)
    {
        try {
            // Get product data by rule's product condition
            $newProductsData = $rule->getListProductData(['name', 'sku', 'price', 'type_id']);
            // remove the product which no longer apply by new rule's product condition
            $this->searchCriteriaBuilder->addFilter('rule_id', $rule->getId(), 'eq');
            $currentProductsData = $this->productPriceRepository->getList(
                $this->searchCriteriaBuilder->create()
            )->getItems();
            $noLongerApplyProductIds = $this->getNoLongerApplyProductPriceIds($newProductsData, $currentProductsData);

            if ($noLongerApplyProductIds) {
                $this->productPriceRepository->deleteByIds($noLongerApplyProductIds);
            }

            // apply new product by new rule's product condition
            $newApplyProductPrice = $this->getNewApplyProductPrice($newProductsData, $currentProductsData);
            $importProductPrice = [];
            foreach ($newProductsData as $productId => $data) {
                if ($priceMethod = $rule->getPriceMethod()) {
                    $priceMethod = explode("_", $priceMethod);
                    $priceType = $priceMethod[0];
                    $customPrice = $priceMethod[1];
                    $customPrice = $this->helper->prepareCustomPrice(
                        $priceType,
                        $data["price"] ?? 0,
                        $customPrice
                    );
                }
                if (in_array($productId, $newApplyProductPrice)) {
                    $importProductPrice[] = [
                        'name' => $data["name"] ?? null,
                        'type_id' => $data["type_id"] ?? null,
                        'rule_id' => $rule->getId(),
                        'origin_price' => $data["price"] ?? null,
                        'product_id' => $productId,
                        'custom_price' => $customPrice ?? null,
                        'product_sku' => $data["sku"] ?? null
                    ];
                }
            }
            if (!empty($importProductPrice)) {
                $connection = $this->productPriceResource->getConnection();
                $table = $this->productPriceResource->getMainTable();
                $connection->insertMultiple($table, $importProductPrice);
            }
        } catch (CouldNotSaveException $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->logger->critical($e);
            throw new CouldNotSaveException(
                __("Something went wrong while saving the rule data. Please review the error log.")
            );
        }
    }

    /**
     * Save applied customers by rule
     *
     * @param \Bss\CustomPricing\Model\PriceRule $rule
     * @throws CouldNotSaveException
     */
    protected function saveAppliedCustomers($rule)
    {
        try {
            $newCustomersData = [];
            if ($rule->needProcessCustomers()) {
                // Get customer data by rule's customer condition
                $newCustomersData = $rule->getListCustomersData(['firstname', 'lastname'], $rule->getWebsiteId());
            }
            // remove the customer which no longer apply by new rule's customer condition
            $this->searchCriteriaBuilder->addFilter('rule_id', $rule->getId(), 'eq');
            $currentCustomersData = $this->appliedCustomersRepository->getList(
                $this->searchCriteriaBuilder->create()
            )->getItems();
            $newCustomersData = $this->validateNewCustomer($rule->getId(), $newCustomersData, $currentCustomersData);
            $importCustomerData = [];
            foreach ($newCustomersData as $customerId => $data) {
                $importCustomerData[] = [
                    'customer_first_name' => $data["firstname"],
                    'customer_last_name' => $data["lastname"],
                    'customer_id' => $customerId,
                    'rule_id' => $rule->getId(),
                    'applied_rule' => true
                ];
            }
            if (!empty($importCustomerData)) {
                $connection = $this->appliedCustomersResource->getConnection();
                $table = $this->appliedCustomersResource->getMainTable();
                $connection->insertMultiple($table, $importCustomerData);
            }
        } catch (CouldNotSaveException $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->logger->critical($e);
            throw new CouldNotSaveException(
                __("Something went wrong while saving the rule data. Please review the error log.")
            );
        }
    }

    /**
     * Validate new customer array
     *
     * @param int $rulesId
     * @param array $newCustomersData
     * @param array $currentCustomersData
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function validateNewCustomer($rulesId, $newCustomersData, $currentCustomersData)
    {
        if (!empty($currentCustomersData) && is_array($currentCustomersData)) {
            foreach ($currentCustomersData as $customer) {
                $customerId = $customer->getData('customer_id');
                if (isset($newCustomersData[$customerId])) {
                    unset($newCustomersData[$customerId]);
                } else {
                    $this->oldCustomer[] = $customer->getId();
                }
            }
        }
        $this->removeOldCustomer($rulesId);
        return $newCustomersData;
    }

    /**
     * Remove no lonnger applied customers
     *
     * @param int $rulesId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function removeOldCustomer($rulesId)
    {
        if (!empty($this->oldCustomer)) {
            $this->oldCustomer = implode(',', $this->oldCustomer);
            $this->appliedCustomersResource->removeOldCustomers($rulesId, $this->oldCustomer);
        }
    }

    /**
     * Compare and get product price ids that was no longer applied to the price rule
     *
     * @param array $newData
     * @param array $oldData
     *
     * @return array
     */
    protected function getNoLongerApplyProductPriceIds($newData, $oldData)
    {
        $newProductPriceIds = $this->getProductPriceIds($newData);
        $currentProductPriceIds = $this->getProductPriceIds($oldData, "old");
        $removeProductIds = array_diff(array_keys($currentProductPriceIds), $newProductPriceIds);
        $noLongerApplyProductIds = [];
        foreach ($currentProductPriceIds as $productId => $productPriceId) {
            if (in_array($productId, $removeProductIds)) {
                $noLongerApplyProductIds[] = $productPriceId;
            }
        }
        return $noLongerApplyProductIds;
    }

    /**
     * Compare and get product price ids that will be apply to the price rule
     *
     * @param array $newData
     * @param array $oldData
     *
     * @return array
     */
    protected function getNewApplyProductPrice($newData, $oldData)
    {
        $newProductPriceIds = $this->getProductPriceIds($newData);
        $currentProductPriceIds = $this->getProductPriceIds($oldData, "old");

        return array_diff($newProductPriceIds, array_keys($currentProductPriceIds));
    }

    /**
     * Prepare price rule data
     *
     * @param array $data
     *
     * @return array
     */
    protected function prepareData($data)
    {
        if (isset($data['general_information']['rule']['product'])) {
            $data['general_information']['product'] = $data['general_information']['rule']['product'];
        }
        if (isset($data['general_information']['rule']['customer'])) {
            $data['general_information']['customer'] = $data['general_information']['rule']['customer'];
        }
        if (isset($data['general_information']['customer_condition']['is_not_logged_rule'])) {
            $data['general_information']['is_not_logged_rule'] = $data['general_information']
            ['customer_condition']['is_not_logged_rule'];
        }
        unset($data['general_information']['rule']);
        $generalInfo = $data['general_information'];
        unset($data["general_information"]);
        return array_merge($data, $generalInfo);
    }

    /**
     * Mapping data product id
     *
     * @param object|array $productPrices
     * @param string $dataType
     *
     * @return array
     */
    private function getProductPriceIds($productPrices, $dataType = "new")
    {
        $data = [];
        if ($productPrices && $dataType == "old") {
            foreach ($productPrices as $productPrice) {
                $data[(int)$productPrice->getProductId()] = (int)$productPrice->getId();
            }
        } else {
            $data = array_keys($productPrices);
        }
        return $data;
    }
}
