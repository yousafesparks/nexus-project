<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomPricing\Plugin\Model\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\Data\ProductTierPriceExtensionFactory;
use Magento\Customer\Api\GroupManagementInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Price
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class Price extends \Magento\Catalog\Model\Product\Type\Price
{
    /**
     * @var \Bss\CustomPricing\Helper\CustomerRule
     */
    protected $helperRule;

    /**
     * @var \Bss\CustomPricing\Model\ResourceModel\Indexer\BaseFinalPrice
     */
    protected $indexPrice;

    /**
     * @var \Bss\CustomPricing\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var ProductTierPriceExtensionFactory
     */
    private $tierPriceExtensionFactory;

    /**
     * Price constructor.
     * @param \Magento\CatalogRule\Model\ResourceModel\RuleFactory $ruleFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param GroupManagementInterface $groupManagement
     * @param \Magento\Catalog\Api\Data\ProductTierPriceInterfaceFactory $tierPriceFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param ProductTierPriceExtensionFactory $tierPriceExtensionFactory
     * @param \Bss\CustomPricing\Helper\Data $helperData
     * @param \Bss\CustomPricing\Helper\CustomerRule $helperRule
     * @param \Bss\CustomPricing\Model\ResourceModel\Indexer\BaseFinalPrice $indexPrice
     * @param \Psr\Log\LoggerInterface $logger
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\CatalogRule\Model\ResourceModel\RuleFactory $ruleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        PriceCurrencyInterface $priceCurrency,
        GroupManagementInterface $groupManagement,
        \Magento\Catalog\Api\Data\ProductTierPriceInterfaceFactory $tierPriceFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        ProductTierPriceExtensionFactory $tierPriceExtensionFactory,
        \Bss\CustomPricing\Helper\Data $helperData,
        \Bss\CustomPricing\Helper\CustomerRule $helperRule,
        \Bss\CustomPricing\Model\ResourceModel\Indexer\BaseFinalPrice $indexPrice,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct(
            $ruleFactory,
            $storeManager,
            $localeDate,
            $customerSession,
            $eventManager,
            $priceCurrency,
            $groupManagement,
            $tierPriceFactory,
            $config,
            $tierPriceExtensionFactory
        );
        $this->tierPriceExtensionFactory = $tierPriceExtensionFactory;
        $this->helperData = $helperData;
        $this->helperRule = $helperRule;
        $this->indexPrice = $indexPrice;
        $this->logger = $logger;
    }

    /**
     * Modify price product if product have rule
     *
     * @param \Magento\Catalog\Model\Product\Type\Price $subject
     * @param float $result
     * @param Product $product
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetPrice(
        $subject,
        $result,
        $product
    ) {
        $ruleIds = $this->validateRule();
        if ($ruleIds) {
            $productId = $product->getId();
            $customerGroup = $this->helperRule->getCustomerSession()->getCustomerGroupId();
            $priceRule = $this->getInforPrices($ruleIds, $productId, $customerGroup);
            if ($priceRule) {
                $product->setBssCustomPrice(true);
                return $priceRule;
            }
        }
        return $result;
    }

    /**
     * Modify price when add to cart
     *
     * @param \Magento\Catalog\Model\Product\Type\Price $subject
     * @param callable $proceed
     * @param Product $product
     * @param float|null $qty
     * @return mixed
     */
    public function aroundGetBasePrice(
        $subject,
        callable $proceed,
        $product,
        $qty
    ) {
        try {
            $ruleIds = $this->validateRule();
            if ($ruleIds) {
                $productId = $product->getId();
                $customerGroup = $this->helperRule->getCustomerSession()->getCustomerGroupId();
                $priceRule = $this->getInforPrices($ruleIds, $productId, $customerGroup);
                if (!$priceRule) {
                    return $proceed($product, $qty);
                }

                $finalPrice = $this->getFinalPriceCustom(
                    $subject,
                    $product,
                    $priceRule,
                    $qty
                );
                return $finalPrice;
            }
            return $proceed($product, $qty);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return $proceed($product, $qty);
        }
    }

    /**
     * Get ruleids
     *
     * @return false
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateRule()
    {
        $ruleIds = $this->helperRule->getCustomerSession()->getBssCustomRule();
        $isEnable = $this->helperData->isEnabled();
        if ($ruleIds && $isEnable) {
            return $ruleIds;
        }
        return false;
    }

    /**
     * Get prices info from ruleids
     *
     * @param string $ruleIds
     * @param int $productId
     * @param int $customerGroup
     * @return false|mixed
     */
    private function getInforPrices($ruleIds, $productId, $customerGroup)
    {
        $inforPrices = $this->indexPrice->getPriceFromIndex($ruleIds, $productId, $customerGroup);
        if ($inforPrices && is_array($inforPrices)) {
            $price = [];
            foreach ($inforPrices as $inforPrice) {
                $price[] = $inforPrice['price'];
            }
            return min($price);
        }
        return false;
    }

    /**
     * Return final price custom
     *
     * @param \Magento\Catalog\Model\Product\Type\Price $subject
     * @param Product $product
     * @param float $priceRule
     * @param float $qty
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getFinalPriceCustom($subject, $product, $priceRule, $qty)
    {
        $isAppliedTierPrice = $this->helperData->applyNormalTierPrice();
        $isAppliedSpecialPrice = $this->helperData->applyNormalSpecialPrice();
        if ($isAppliedSpecialPrice && $isAppliedTierPrice) {
            return min(
                $subject->_applyTierPrice($product, $qty, $priceRule),
                $subject->_applySpecialPrice($product, $priceRule)
            );
        }

        if ($isAppliedTierPrice && !$isAppliedSpecialPrice) {
            return $subject->_applyTierPrice($product, $qty, $priceRule);
        }

        if (!$isAppliedTierPrice && $isAppliedSpecialPrice) {
            return $subject->_applySpecialPrice($product, $priceRule);
        }

        return $priceRule;
    }
}
