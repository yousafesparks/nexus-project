<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomPricing\Plugin\App\PageCache;

use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class ModifyIdentifier
 *
 * @package Bss\CustomPricing\Plugin\App\PageCache
 */
class ModifyIdentifier
{
    /**
     * @var \Bss\CustomPricing\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Bss\CustomPricing\Helper\CustomerRule
     */
    protected $helperRule;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $context;

    /**
     * ModifyIdentifier constructor.
     * @param \Bss\CustomPricing\Helper\Data $helperData
     * @param \Bss\CustomPricing\Helper\CustomerRule $helperRule
     * @param Json $serializer
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Framework\App\Http\Context $context
     */
    public function __construct(
        \Bss\CustomPricing\Helper\Data $helperData,
        \Bss\CustomPricing\Helper\CustomerRule $helperRule,
        Json $serializer,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Http\Context $context
    ) {
        $this->helperData = $helperData;
        $this->helperRule = $helperRule;
        $this->serializer = $serializer;
        $this->request = $request;
        $this->context = $context;
    }

    /**
     * Modify data if customer apply rule. customer_group, customer_logged_in, rule_id
     *
     * @param \Magento\Framework\App\Http\Context $subject
     * @param string $result
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetValue(
        $subject,
        $result
    ) {
        $isCustomerLoggedIn = $this->helperRule->getCustomerSession()->getCustomer()->getId();
        $this->helperRule->setSessionRule(null);
        if ($this->helperData->isEnabled()) {
            if ($isCustomerLoggedIn) {
                $customerRule = $this->helperRule->getSpecialRuleByCustomerId();
            } else {
                $customerRule = $this->helperRule->getSpecialRuleNotLoggedIn();
            }
            if ($customerRule) {
                $this->helperRule->setSessionRule($customerRule);
                $data = [
                    $this->request->isSecure(),
                    $this->request->getUriString(),
                    $this->request->get(\Magento\Framework\App\Response\Http::COOKIE_VARY_STRING)
                        ?: $this->context->getVaryString(),
                    $customerRule
                ];
                return sha1($this->serializer->serialize($data));
            }
        }
        return $result;
    }
}
