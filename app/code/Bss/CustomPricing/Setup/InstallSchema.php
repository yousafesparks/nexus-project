<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomPricing\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Zend_Db_Exception;

/**
 * Class InstallSchema
 *
 * @package Bss\CustomPricing\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    const APPLIED_CUSTOMERS_TABLE = 'bss_applied_customers';
    const PRICE_RULES_TABLE = 'bss_price_rules';
    const PRODUCT_PRICE = 'bss_product_price';
    const CUSTOM_PRICING_INDEX = "bss_custom_pricing_index";

    /**
     * Install Schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'bss_price_rules'
         */
        $this->createPriceRule($installer);

        /**
         * Create table 'bss_applied_customers'
         */
        $this->createAppliedCustomers($installer);

        /**
         * Create table 'bss_product_price'
         */
        $this->createProductPrice($installer);

        // Create 'bss_custom_pricing_index' table
        $this->createModuleIndexTable($installer);

        $installer->endSetup();
    }

    /**
     * Create applied customers table
     *
     * @param ModuleContextInterface $installer
     * @throws Zend_Db_Exception
     */
    private function createAppliedCustomers(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()->newTable(
            $installer->getTable(self::APPLIED_CUSTOMERS_TABLE)
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'customer_first_name',
            Table::TYPE_TEXT,
            32,
            ['unsigned' => true],
            'Customer First Name'
        )->addColumn(
            'customer_last_name',
            Table::TYPE_TEXT,
            32,
            ['unsigned' => true],
            'Customer Last Name'
        )->addColumn(
            'customer_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Customer Id'
        )->addColumn(
            'rule_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Rule Id'
        )->addColumn(
            'applied_rule',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Applied Rule Status'
        )->addIndex(
            $installer->getIdxName(self::APPLIED_CUSTOMERS_TABLE, ['id']),
            ['id']
        )->addForeignKey(
            $installer->getFkName(
                self::APPLIED_CUSTOMERS_TABLE,
                'rule_id',
                self::PRICE_RULES_TABLE,
                'id'
            ),
            'rule_id',
            $installer->getTable(self::PRICE_RULES_TABLE),
            'id',
            Table::ACTION_CASCADE
        )->setComment(
            'Bss Applied Customers Table'
        );
        $installer->getConnection()->createTable($table);
    }

    /**
     * Create price rule table
     *
     * @param SchemaSetupInterface $installer
     * @throws Zend_Db_Exception
     */
    private function createPriceRule($installer)
    {
        $table = $installer->getConnection()->newTable(
            $installer->getTable(self::PRICE_RULES_TABLE)
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            80,
            ['unsigned' => true],
            'Price Rule Name'
        )->addColumn(
            'website_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Website Id'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            255,
            ['unsigned' => true],
            'Description'
        )->addColumn(
            'priority',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Priority'
        )->addColumn(
            'status',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Status'
        )->addColumn(
            'product_serialized',
            Table::TYPE_TEXT,
            16777216,
            ['unsigned' => true],
            'Product Serialized'
        )->addColumn(
            'customer_serialized',
            Table::TYPE_TEXT,
            16777216,
            ['unsigned' => true],
            'Customer Serialized'
        )->addColumn(
            'is_not_logged_rule',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Is Not Logged In Rule'
        )->addColumn(
            'price_method',
            Table::TYPE_TEXT,
            255,
            ['unsigned' => true],
            'The product price set method'
        )->addIndex(
            $installer->getIdxName(self::PRICE_RULES_TABLE, ['id']),
            ['id']
        )->setComment(
            'Bss Price Rules Table'
        );
        $installer->getConnection()->createTable($table);
    }

    /**
     * Create product price table
     *
     * @param SchemaSetupInterface $installer
     * @throws Zend_Db_Exception
     */
    private function createProductPrice(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()->newTable(
            $installer->getTable(self::PRODUCT_PRICE)
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            80,
            ['unsigned' => true],
            'Product Name'
        )->addColumn(
            'type_id',
            Table::TYPE_TEXT,
            80,
            ['unsigned' => true],
            'Product Type Id'
        )->addColumn(
            'origin_price',
            Table::TYPE_DECIMAL,
            '20,2',
            ['unsigned' => true],
            'Origin Price'
        )->addColumn(
            'custom_price',
            Table::TYPE_DECIMAL,
            '20,2',
            ['unsigned' => true],
            'Custom Price'
        )->addColumn(
            'rule_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Rule Id'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Product Id'
        )->addColumn(
            'product_sku',
            Table::TYPE_TEXT,
            80,
            ['unsigned' => true],
            'Product Sku'
        )->addIndex(
            $installer->getIdxName(self::PRODUCT_PRICE, ['id']),
            ['id']
        )->addForeignKey(
            $installer->getFkName(
                self::PRODUCT_PRICE,
                'rule_id',
                self::PRICE_RULES_TABLE,
                'id'
            ),
            'rule_id',
            $installer->getTable(self::PRICE_RULES_TABLE),
            'id',
            Table::ACTION_CASCADE
        )->setComment(
            'Bss Product Price Table'
        );
        $installer->getConnection()->createTable($table);
    }

    /**
     * Create price index table
     *
     * @param SchemaSetupInterface $installer
     * @throws Zend_Db_Exception
     */
    private function createModuleIndexTable(SchemaSetupInterface $installer)
    {
        $table = $installer->getConnection()->newTable(
            $installer->getTable(self::CUSTOM_PRICING_INDEX)
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Product Id'
        )->addColumn(
            'rule_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Rule Id'
        )->addColumn(
            'customer_group_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Customer Group Id'
        )->addColumn(
            'website_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Website Id'
        )->addColumn(
            'tax_class_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true, 'default' => 0],
            'Tax Class Id'
        )->addColumn(
            'price',
            Table::TYPE_DECIMAL,
            '20,6',
            ['nullable' => true],
            'Price'
        )->addColumn(
            'final_price',
            Table::TYPE_DECIMAL,
            '20,6',
            ['nullable' => true],
            'Final Price'
        )->addColumn(
            'min_price',
            Table::TYPE_DECIMAL,
            '20,6',
            ['nullable' => true],
            'Min Price'
        )->addColumn(
            'max_price',
            Table::TYPE_DECIMAL,
            '20,6',
            ['nullable' => true],
            'Max Price'
        )->addColumn(
            'tier_price',
            Table::TYPE_DECIMAL,
            '20,6',
            ['nullable' => true],
            'Tier Price'
        )->addIndex(
            $installer->getIdxName(self::CUSTOM_PRICING_INDEX, ['customer_group_id']),
            ['customer_group_id']
        )->addIndex(
            $installer->getIdxName(self::CUSTOM_PRICING_INDEX, ['min_price']),
            ['min_price']
        )->addIndex(
            $installer->getIdxName(self::CUSTOM_PRICING_INDEX, ['website_id', 'min_price']),
            ['website_id', 'min_price', 'customer_group_id']
        )->setComment("Bss Product Price Index");

        $installer->getConnection()->createTable($table);
    }
}
