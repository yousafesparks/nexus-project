<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\CustomPricing\Helper;

use Magento\Framework\App\Helper\Context;
use Bss\CustomPricing\Api\Data\ProductPriceInterface;
use Bss\CustomPricing\Api\PriceRuleRepositoryInterface;
use Bss\CustomPricing\Api\ProductPriceRepositoryInterface;

/**
 * Helper product save after for module
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class ProductSave extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var PriceRuleRepositoryInterface
     */
    protected $priceRuleRepository;

    /**
     * @var ProductPriceRepositoryInterface
     */
    protected $productPriceRepository;

    /**
     * @var \Bss\CustomPricing\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Indexer\IndexerInterface
     */
    protected $indexer;

    /**
     * @var \Bss\CustomPricing\Model\Indexer\PriceRule
     */
    protected $priceRuleIndexer;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * ProductSave constructor.
     * @param Context $context
     * @param PriceRuleRepositoryInterface $priceRuleRepository
     * @param ProductPriceRepositoryInterface $productPriceRepository
     * @param Data $helper
     * @param \Magento\Framework\Indexer\IndexerInterface $indexer
     * @param \Bss\CustomPricing\Model\Indexer\PriceRule $priceRuleIndexer
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        Context $context,
        PriceRuleRepositoryInterface $priceRuleRepository,
        ProductPriceRepositoryInterface $productPriceRepository,
        \Bss\CustomPricing\Helper\Data $helper,
        \Magento\Framework\Indexer\IndexerInterface $indexer,
        \Bss\CustomPricing\Model\Indexer\PriceRule $priceRuleIndexer,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->priceRuleRepository = $priceRuleRepository;
        $this->productPriceRepository = $productPriceRepository;
        $this->helper = $helper;
        $this->indexer = $indexer;
        $this->priceRuleIndexer = $priceRuleIndexer;
        $this->logger = $logger;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context);
    }

    /**
     * Get price rules
     *
     * @return \Bss\CustomPricing\Api\Data\PriceRuleSearchResultsInterface
     */
    public function getPriceRules()
    {
        $searchBuilder = $this->searchCriteriaBuilder->create();
        $priceRules = $this->priceRuleRepository->getList($searchBuilder);
        return $priceRules;
    }

    /**
     * Check and insert or update product to bss price table
     *
     * @param \Bss\CustomPricing\Api\Data\PriceRuleInterface $priceRule
     * @param \Magento\Catalog\Model\Product $product
     */
    public function insertOrUpdateProductPrice($priceRule, $product)
    {
        try {
            $productPrice = $this->productPriceRepository->getBy($priceRule->getId(), $product->getId());
            if ($productPrice->isObjectNew()) {
                $productPrice->setData(
                    [
                        ProductPriceInterface::RULE_ID => $priceRule->getId(),
                        ProductPriceInterface::PRODUCT_ID => $product->getId(),
                        ProductPriceInterface::NAME => $product->getName(),
                        ProductPriceInterface::TYPE_ID => $product->getTypeId(),
                        ProductPriceInterface::ORIGIN_PRICE => $product->getPrice(),
                        ProductPriceInterface::PRODUCT_SKU => $product->getSku()
                    ]
                );
            } else {
                $productPrice->setName($product->getName());
                $productPrice->setTypeId($product->getTypeId());
                $productPrice->setOriginPrice($product->getPrice());
                $productPrice->setProductSku($product->getSku());
            }
            $productPrice->setShouldReindex(false);

            if ($priceMethod = $priceRule->getPriceMethod()) {
                $priceMethod = explode("_", $priceMethod);
                $priceType = $priceMethod[0];
                $customPrice = $priceMethod[1];
                $customPrice = $this->helper->prepareCustomPrice(
                    $priceType,
                    $product->getPrice(),
                    $customPrice
                );
                $productPrice->setCustomPrice($customPrice);
            }
            $this->productPriceRepository->save($productPrice);

            $isSchedule = $this->indexer->load(\Bss\CustomPricing\Model\Indexer\PriceRule::INDEX_ID)->isScheduled();
            if (!$isSchedule) {
                $this->priceRuleIndexer->reindexBy($product->getId(), 'product');
            }
            if ($isSchedule) {
                $this->indexer->invalidate();
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
    }
}
