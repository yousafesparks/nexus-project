/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomPricing
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

define([
    'Magento_Ui/js/form/element/abstract','ko'
], function (Abstract, ko) {
    'use strict';

    const ABSOLUTE_PRICE = 1;
    const INCREASE_FIXED_PRICE = 2;
    const DECREASE_FIXED_PRICE = 3;
    const INCREASE_PERCENT_PRICE = 4;
    const DECREASE_PERCENT_PRICE = 5;

    return Abstract.extend({
        defaults: {
            changeSymbol: "",
            percentSymbol: null
        },

        /**
         * @returns {Element}
         */
        initObservable: function () {
            return this
                ._super()
                .observe(['changeSymbol', 'percentSymbol']);
        },

        /**
         * Change symbol
         */
        handleTypeChanges: function (value) {
            switch (parseInt(value)) {
                case ABSOLUTE_PRICE:
                    this.changeSymbol("");
                    this.percentSymbol('')
                    break;
                case INCREASE_FIXED_PRICE:
                    this.changeSymbol("+");
                    this.percentSymbol('')
                    break;
                case DECREASE_FIXED_PRICE:
                    this.changeSymbol("-");
                    this.percentSymbol('')
                    break;
                case INCREASE_PERCENT_PRICE:
                    this.percentSymbol('%');
                    this.changeSymbol("+");
                    break;
                case DECREASE_PERCENT_PRICE:
                    this.percentSymbol('%');
                    this.changeSymbol("-");
                    break;
                default:
                    break;
            }
        }
    });
});
