<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CompanyCreditGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CompanyCreditGraphQl\Model\Resolver;

use Bss\CompanyCredit\Api\CreditRepositoryInterface;
use Bss\CompanyCredit\Helper\Currency;
use Bss\CompanyCredit\Model\CreditFactory;
use Bss\CompanyCredit\Model\History;
use Bss\CompanyCredit\Model\HistoryFactory;
use Bss\CompanyCredit\Model\ResourceModel\History\CollectionFactory as HistoryCollection;
use Bss\CompanyCredit\Model\ResourceModel\HistoryRepository as HistoryRepository;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class Save
 *
 * @package Bss\CompanyCreditGraphQl\Model\Resolver
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save implements ResolverInterface
{
    /**
     * @var Currency
     */
    protected $helperCurrency;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CreditFactory
     */
    private $creditFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var HistoryRepository
     */
    protected $historyRepository;

    /**
     * @var HistoryCollection
     */
    protected $historyCollection;

    /**
     * @var HistoryFactory
     */
    protected $historyCredit;

    /**
     * @var CreditRepositoryInterface
     */
    protected $companyCreditRepository;

    /**
     * Order constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param CreditFactory $creditFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param HistoryRepository $historyRepository
     * @param HistoryCollection $historyCollection
     * @param HistoryFactory $historyCredit
     * @param CreditRepositoryInterface $companyCreditRepository
     * @param Currency $helperCurrency
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        CreditFactory $creditFactory,
        \Psr\Log\LoggerInterface $logger,
        HistoryRepository $historyRepository,
        HistoryCollection $historyCollection,
        HistoryFactory $historyCredit,
        CreditRepositoryInterface $companyCreditRepository,
        Currency $helperCurrency
    ) {
        $this->customerRepository = $customerRepository;
        $this->creditFactory = $creditFactory;
        $this->logger = $logger;
        $this->historyRepository = $historyRepository;
        $this->historyCollection = $historyCollection;
        $this->historyCredit = $historyCredit;
        $this->companyCreditRepository = $companyCreditRepository;
        $this->helperCurrency = $helperCurrency;
    }

    /**
     * Save company credit
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return Value|mixed
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $customerId = $context->getUserId();
        $this->validate($customerId, $args);
        $this->initValue($args, $context);

        $result = [];
        $checkFirst = 0;
        $availableCreditOld = 0;
        $creditLimitOld = 0;
        $creditLimitParam = $args['credit_limit'];
        $availableCreditParam = $args['update_available'];
        $comment = $comment = $args['comment'];
        $allowExceed = $args['allow_exceed'];
        $allowExceedOld = $allowExceed;

        try {
            $credit = $this->companyCreditRepository->get($customerId);
            $historyModel = $this->historyCredit->create();
            if (isset($credit) && $credit->getId()) {
                $this->convertCurrency($args);
                $allowExceedOld = $credit->isAllowExceed();
                $availableCreditOld = $credit->getAvailableCredit();
                $creditLimitOld = $credit->getCreditLimit();
                if (!$creditLimitParam && !(is_numeric($creditLimitParam))) {
                    $creditLimitParam = $creditLimitOld;
                }
                if (!$allowExceed && $allowExceed !== 0) {
                    $allowExceed = $credit->isAllowExceed();
                }
                $availableCreditChange = $creditLimitParam - $creditLimitOld + (float)($availableCreditParam);
            } else {
                $this->firstSave($args);
                $this->convertCurrency($args);
                $availableCreditChange = $creditLimitParam - $creditLimitOld;
                $checkFirst = 1;
                $credit = $this->creditFactory->create();
            }
            $availableCredit = $availableCreditChange + $availableCreditOld;
            $usedCredit = $creditLimitParam - $availableCredit;
            if ($usedCredit >= 0 && ($allowExceed || ($availableCredit >= 0 && $availableCredit <= $creditLimitParam))) {
                if ($checkFirst || $args['update_available'] ||
                    $args['credit_limit'] != $creditLimitOld
                    || $allowExceedOld != $allowExceed) {
                    $dataHistory = [
                        'customer_id' => $customerId,
                        'type' => \Bss\CompanyCredit\Model\History::TYPE_ADMIN_REFUND,
                        'change_credit' => $availableCreditChange,
                        'available_credit_current' => $availableCredit,
                        'comment' => $comment,
                        'allow_exceed' => $allowExceed,
                        'currency_code' => $args["currency_code_website"]
                    ];
                    $credit->setAvailableCredit($availableCredit);
                    $credit->setUsedCredit($usedCredit);
                    $credit->setCreditLimit($creditLimitParam);
                    $credit->setAllowExceed($allowExceed);
                    $credit->setCustomerId($customerId);
                    $credit->setCurrencyCode($args["currency_code_website"]);
                    $this->companyCreditRepository->save($credit);
                    if ($args['update_available']) {
                        $args["available_credit_current"] = $availableCreditOld + $args['update_available'];
                        $this->updateCreditValue($args, $dataHistory, $historyModel);
                    }
                    if ($checkFirst || ($creditLimitParam != $creditLimitOld)) {
                        $this->changeCreditLimit($args, $dataHistory, $creditLimitOld, $historyModel);
                    }
                    if ($allowExceedOld != $allowExceed || $checkFirst) {
                        $this->allowExceedCredit($dataHistory, $historyModel);
                    }
                    $result["status"] = [
                        "success" => true,
                        "message" => "You have successfully saved changes to company credit."
                    ];
                }
            } else {
                $result["status"] = [
                    "success" => false,
                    "message" => "You cannot update available credit to greater than credit limit."
                ];
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return $result;
    }

    /**
     * Validate params input
     *
     * @param Int $customerId
     * @param array $args
     * @throws GraphQlInputException
     */
    public function validate($customerId, $args)
    {
        if (empty($customerId)) {
            throw new GraphQlInputException(__('You can token customer!'));
        }

        if (!isset($args["input"]) && !isset($args["input"]["credit_limit"]) &&
            !isset($args["input"]["update_available"]) && !isset($args["input"]["allow_exceed"])
        ) {
            throw new GraphQlInputException(__('You must provide at least one of the following values: credit_limit, update_available, allow_exceed.'));
        }

        $args = $args["input"];
        if (isset($args["credit_limit"]) && $args["credit_limit"] < 0) {
            throw new GraphQlInputException(
                __("Please validate input credit_limit >= 0")
            );
        }

        if (isset($args["order_id"])) {
            $this->validateOrderId($args);
        }
    }

    /**
     * Validate input Order ID
     *
     * @param array $args
     * @throws GraphQlInputException
     */
    public function validateOrderId($args)
    {
        if ($args["order_id"] <= 0) {
            throw new GraphQlInputException(
                __("Please validate value input order_id > 0")
            );
        }
        $historyCollection = $this->historyCollection->create()
            ->addFieldToFilter("order_id", $args["order_id"]);
        if ($historyCollection->getSize()) {
            throw new GraphQlInputException(
                __("You can't update credit. Because order_id exist")
            );
        }
    }

    /**
     * Init value input
     *
     * @param array $args
     * @param ContextInterface $context
     */
    public function initValue(&$args, $context)
    {
        $args = $args["input"];
        if (!isset($args['credit_limit'])) {
            $args['credit_limit'] = "";
        }

        if (!isset($args["update_available"])) {
            $args["update_available"] = "";
        }

        if (!isset($args['comment'])) {
            $args['comment'] = "";
        }

        if (!isset($args['allow_exceed'])) {
            $args['allow_exceed'] = "";
        }

        if (!isset($args['order_id'])) {
            $args['order_id'] = null;
        }

        if (!isset($args['po_number'])) {
            $args['po_number'] = null;
        }

        $this->getCurrencyCodeWebsite($args, $context);
    }

    /**
     * Get currency code by website
     *
     * @param array $args
     * @param ContextInterface $context
     */
    public function getCurrencyCodeWebsite(&$args, $context)
    {
        try {
            $customer = $this->customerRepository->get($context->getUserId());
            $args["currency_code_website"] = $this->helperCurrency->getCurrencyCodeByWebsite($customer->getWebsiteId());
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
            $args["currency_code_website"] = $context->getExtensionAttributes()->getStore()->getBaseCurrencyCode();
        }
        if (!isset($args["currency_code"])) {
            $args["currency_code"] = $args["currency_code_website"];
        }
    }

    /**
     * Ignore value input update_available, order_id, po_number
     * When not assign Company Credit
     *
     * @param array $args
     */
    public function firstSave(&$args)
    {
        $args["update_available"] = "";
        $args['order_id'] = "";
        $args["po_number"] = "";
    }

    /**
     * Convert currency input update_available and credit_limit
     *
     * @param array $args
     */
    public function convertCurrency(&$args)
    {
        if ($args["credit_limit"]) {
            $args["credit_limit"] =
                $this->helperCurrency->convertCurrency($args["credit_limit"], $args["currency_code"], $args["currency_code_website"]);
        }
        if ($args["update_available"]) {
            $args["update_available"] =
                $this->helperCurrency->convertCurrency($args["update_available"], $args["currency_code"], $args["currency_code_website"]);
        }
    }

    /**
     * Save credit limit and send email for customer
     *
     * @param array $params
     * @param array $dataHistory
     * @param float $creditLimitOld
     * @param History $historyModel
     * @throws LocalizedException
     */
    public function changeCreditLimit($params, $dataHistory, $creditLimitOld, $historyModel)
    {
        $dataHistory["type"] = History::TYPE_ADMIN_CHANGES_CREDIT_LIMIT;
        $dataHistory["change_credit"] = $params['credit_limit'] - $creditLimitOld;
        $historyModel->updateHistory($dataHistory);
        $this->historyRepository->save($historyModel);
    }

    /**
     * Save update credit value and send email for customer
     *
     * @param array $params
     * @param array $dataHistory
     * @param History $historyModel
     * @throws LocalizedException
     */
    public function updateCreditValue($params, $dataHistory, $historyModel)
    {
        if ($params["order_id"]) {
            $dataHistory["order_id"] = $params["order_id"];
            $dataHistory["po_number"] = $params["po_number"];
            $dataHistory["type"] = \Bss\CompanyCredit\Model\History::TYPE_PLACE_ORDER;
        }
        $dataHistory["change_credit"] = $params['update_available'];
        $dataHistory["available_credit_current"] = $params["available_credit_current"];
        $historyModel->updateHistory($dataHistory);
        $this->historyRepository->save($historyModel);
    }

    /**
     * Save change allow exceed of customer
     *
     * @param array $dataHistory
     * @param History $historyModel
     * @throws LocalizedException
     */
    public function allowExceedCredit($dataHistory, $historyModel)
    {
        $dataHistory["change_credit"] = 0;
        $dataHistory["type"] = History::TYPE_CHANGE_CREDIT_EXCESS_TO;
        $historyModel->updateHistory($dataHistory);
        $this->historyRepository->save($historyModel);
    }
}
