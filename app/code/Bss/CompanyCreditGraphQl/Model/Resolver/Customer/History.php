<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CompanyCreditGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CompanyCreditGraphQl\Model\Resolver\Customer;

use Bss\CompanyCredit\Helper\Data as HelperData;
use Bss\CompanyCredit\Model\HistoryFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class Credit
 *
 * @package Bss\CompanyCreditGraphQl\Model\Resolver
 */
class History implements ResolverInterface
{
    protected $helperData;

    /**
     * @var HistoryFactory
     */
    protected $history;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * Credit constructor.
     *
     * @param HistoryFactory $history
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        HelperData $helperData,
        HistoryFactory $history,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        $this->helperData = $helperData;
        $this->history = $history;
        $this->customerFactory = $customerFactory;
    }

    /**
     * Resolve
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|Value|mixed
     * @throws GraphQlNoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $result = [];
        $customerId = $context->getUserId();
        if ($customerId) {
            try {
                $historyCollection = $this->history->create()->loadByCustomer($customerId);
                if ($historyCollection && $historyCollection->getSize()) {
                    $historyAll = $historyCollection->getData();
                    foreach ($historyAll as $key => $history) {
                        $historyAll[$key]["type"] =
                            __($this->helperData->getTypeAction($history["type"], $history["allow_exceed"]));
                    }
                    $result = $historyAll;
                }
            } catch (LocalizedException $exception) {
                throw new GraphQlNoSuchEntityException(__($exception->getMessage()));
            }
        }

        return $result;
    }
}
