<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CompanyCreditGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CompanyCreditGraphQl\Model\Resolver\Customer;

use Bss\CompanyCredit\Api\CreditRepositoryInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class Credit
 *
 * @package Bss\CompanyCreditGraphQl\Model\Resolver
 */
class Credit implements ResolverInterface
{
    /**
     * @var CreditRepositoryInterface
     */
    protected $creditRepository;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * Credit constructor.
     *
     * @param CreditRepositoryInterface $creditRepository
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        CreditRepositoryInterface $creditRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        $this->creditRepository = $creditRepository;
        $this->customerFactory = $customerFactory;
    }

    /**
     * Resolve
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|Value|mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $result = [];
        $customerId = $context->getUserId();
        $credit = $this->creditRepository->get($customerId);
        if ($credit && $credit->getId()) {
            $result = $credit->getData();
        }
        return $result;
    }
}
