<?php
/**
 *
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category  BSS
 * @package   Bss_CompanyCreditGraphQl
 * @author    Extension Team
 * @copyright Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license   http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CompanyCreditGraphQl\Model\Resolver;

use Bss\CompanyCredit\Helper\Data as HelperData;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\Resolver\ValueFactory;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Psr\Log\LoggerInterface;

/**
 * Class Config get module config
 */
class Config implements ResolverInterface
{
    /**
     * @var HelperData
     */
    protected $helperData;
    /**
     * @var ValueFactory
     */
    private $valueFactory;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Config constructor.
     *
     * @param HelperData $helperData
     * @param ValueFactory $valueFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        HelperData $helperData,
        ValueFactory $valueFactory,
        LoggerInterface $logger
    ) {
        $this->helperData = $helperData;
        $this->valueFactory = $valueFactory;
        $this->logger = $logger;
    }

    /**
     * Get information config module by website id
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return Value|mixed
     * @throws GraphQlAuthorizationException
     * @throws GraphQlNoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['website_id'])) {
            throw new GraphQlAuthorizationException(
                __(
                    'website id should be specified'
                )
            );
        }

        try {
            $data = $this->getConfig($args['website_id']);

            $result = function () use ($data) {
                return !empty($data) ? $data : [];
            };

            return $this->valueFactory->create($result);
        } catch (NoSuchEntityException $exception) {
            throw new GraphQlNoSuchEntityException(__($exception->getMessage()));
        } catch (LocalizedException $exception) {
            throw new GraphQlNoSuchEntityException(__($exception->getMessage()));
        }
    }

    /**
     * Get data config module by website id
     *
     * @param int $websiteId
     * @return array
     * @throws LocalizedException
     */
    public function getConfig($websiteId)
    {
        return [
            "enable" => $this->helperData->isEnableModule($websiteId)
        ];
    }
}
