<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_StoreCredit
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\StoreCredit\Api;

use Bss\StoreCredit\Api\Data\HistoryInterface;
use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface StoreCreditSearchResultsInterface
 *
 * @package Bss\StoreCredit\Api\Data
 */
interface HistoryCreditSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get items
     *
     * @return HistoryInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param HistoryInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
