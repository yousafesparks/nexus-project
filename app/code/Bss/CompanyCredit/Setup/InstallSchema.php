<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CompanyCredit
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CompanyCredit\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package Bss\CompanyCredit\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Create tables
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $this->cTCompanyCredit($installer);
        $this->cTCompanyCreditHistory($installer);
        $installer->endSetup();
    }

    /**
     * Create table Bss Company Credit History
     *
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function cTCompanyCreditHistory(SchemaSetupInterface $installer)
    {
        $historyTable = $installer->getTable('bss_companycredit_credit_history');
        if (!$installer->tableExists('bss_companycredit_credit_history')) {
            $balanceHistoryTable = $installer->getConnection()->newTable($historyTable)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Customer ID'
                )
                ->addColumn(
                    'available_credit_current',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['default' => '0.0000'],
                    'Available Credit'
                )
                ->addColumn(
                    'change_credit',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['default' => '0.0000'],
                    'Change Credit'
                )
                ->addColumn(
                    'order_id',
                    Table::TYPE_INTEGER,
                    null,
                    [],
                    'Order ID'
                )
                ->addColumn(
                    'type',
                    Table::TYPE_BOOLEAN,
                    null,
                    [],
                    'Type'
                )
                ->addColumn(
                    'po_number',
                    Table::TYPE_TEXT,
                    255,
                    [],
                    'Purchase Order Number'
                )
                ->addColumn(
                    'comment',
                    Table::TYPE_TEXT,
                    255,
                    [],
                    'Comment'
                )
                ->addColumn(
                    'created_time',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['default' => Table::TIMESTAMP_INIT],
                    'Created Time'
                )
                ->addColumn(
                    'updated_time',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated Time'
                )
                ->addColumn(
                    'allow_exceed',
                    Table::TYPE_SMALLINT,
                    null,
                    [],
                    'Allow Excess'
                )
                ->addColumn(
                    'currency_code',
                    Table::TYPE_TEXT,
                    10,
                    [],
                    'Currency Code'
                )
                ->addIndex(
                    $installer->getIdxName('bss_companycredit_credit_history', ['customer_id']),
                    ['customer_id']
                )->addIndex(
                    $installer->getIdxName('bss_companycredit_credit_history', ['id']),
                    ['id']
                )->addForeignKey(
                    $installer->getFkName(
                        'bss_companycredit_credit_history',
                        'customer_id',
                        'bss_companycredit_balance',
                        'customer_id'
                    ),
                    'customer_id',
                    $installer->getTable('bss_companycredit_credit'),
                    'customer_id',
                    Table::ACTION_CASCADE
                )->setComment('Company Credit History');
            $installer->getConnection()->createTable($balanceHistoryTable);
        }
    }

    /**
     * Create table Bss Company Credit
     *
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    public function cTCompanyCredit(SchemaSetupInterface $installer)
    {
        $creditTable = $installer->getTable('bss_companycredit_credit');
        if (!$installer->tableExists('bss_companycredit_credit')) {
            $creditTable = $installer->getConnection()->newTable($creditTable)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Customer Id'
                )
                ->addColumn(
                    'credit_limit',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['default' => '0.0000'],
                    'Credit Limit'
                )
                ->addColumn(
                    'used_credit',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['default' => '0.0000'],
                    'Used Credit'
                )
                ->addColumn(
                    'available_credit',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['default' => '0.0000'],
                    'Available Credit'
                )
                ->addColumn(
                    'currency_code',
                    Table::TYPE_TEXT,
                    '255',
                    [],
                    'Currency Code'
                )
                ->addColumn(
                    'allow_exceed',
                    Table::TYPE_SMALLINT,
                    null,
                    ['default' => '0'],
                    'Allow Excess'
                )
                ->addForeignKey(
                    $installer->getFkName('bss_companycredit_credit', 'customer_id', 'customer_entity', 'entity_id'),
                    'customer_id',
                    $installer->getTable('customer_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )->addIndex(
                    $installer->getIdxName('bss_companycredit_credit', ['customer_id']),
                    ['customer_id']
                )
                ->setComment('Company Credit');
            $installer->getConnection()->createTable($creditTable);
        }
    }
}
