<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CompanyCredit
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CompanyCredit\Api\Data;

/**
 * @api
 */
interface CreditInterface
{
    /**#@+
     * Constants defined for keys of data array
     */
    const CUSTOMER_ID = 'customer_id';

    const CREDIT_LIMIT = 'credit_limit';

    const USED_CREDIT = 'used_credit';

    const BALANCE_CREDIT = 'available_credit';

    const CURRENCY_CODE = "currency_code";

    const ALLOW_EXCEED = "allow_exceed";

    /**
     * Set Customer ID
     *
     * @param int $customerId
     * @return $this
     * @since 100.1.0
     */
    public function setCustomerId(int $customerId): CreditInterface;

    /**
     * Set credit limit
     *
     * @param float $creditLimit
     * @return $this
     * @since 100.1.0
     */
    public function setCreditLimit(float $creditLimit): CreditInterface;

    /**
     * Set used credit
     *
     * @param float $usedCredit
     * @return $this
     * @since 100.1.0
     */
    public function setUsedCredit(float $usedCredit): CreditInterface;

    /**
     * Set available credit
     *
     * @param float $availableCredit
     * @return $this
     * @since 100.1.0
     */
    public function setAvailableCredit(float $availableCredit): CreditInterface;

    /**
     * Set currency code
     *
     * @param string $currencyCode
     * @return $this
     * @since 100.1.0
     */
    public function setCurrencyCode(string $currencyCode): CreditInterface;

    /**
     * Set allow exceed
     *
     * @param boolean $allowExceed
     * @return $this
     * @since 100.1.0
     */
    public function setAllowExceed(bool $allowExceed): CreditInterface;

    /**
     * Get credit limit
     *
     * @return float
     * @since 100.1.0
     */
    public function getCreditLimit();

    /**
     * Get used credit
     *
     * @return float
     * @since 100.1.0
     */
    public function getUsedCredit();

    /**
     * Get available credit
     *
     * @return float
     * @since 100.1.0
     */
    public function getAvailableCredit();

    /**
     * Get currency code
     *
     * @return float
     * @since 100.1.0
     */
    public function getCurrencyCode();

    /**
     * Get allow exceed
     *
     * @return bool
     * @since 100.1.0
     */
    public function isAllowExceed();

}
