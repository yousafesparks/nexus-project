<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CompanyCredit
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CompanyCredit\Api\Data;

/**
 * @api
 */
interface HistoryInterface
{
    /**#@+
     * Constants defined for keys of data array
     */
    const CUSTOMER_ID = 'customer_id';

    const PO_NUMBER = 'po_number';

    const ORDER_ID = 'order_id';

    const TYPE = 'type';

    const CREDIT_CHANGE = 'credit_change';

    const AVAILABLE_CREDIT_CURRENT = 'available_credit_current';

    const COMMENT = 'comment';

    const ALLOW_EXCEED = 'allow_exceed';

    const UPDATED_TIME = 'updated_time';

    const CREATE_TIME = 'create_time';

    const CURRENCY_CODE = 'currency_code';
    /**#@-*/

    /**
     * Set customer id
     *
     * @param int $customerId
     * @return $this
     * @since 100.1.0
     */
    public function setCustomerId(int $customerId): HistoryInterface;

    /**
     * Set PO number
     *
     * @param string $pONumber
     * @return $this
     * @since 100.1.0
     */
    public function setPONumber(string $pONumber): HistoryInterface;

    /**
     * Set Order ID
     *
     * @param int $orderId
     * @return $this
     * @since 100.1.0
     */
    public function setOrderId(int $orderId): HistoryInterface;

    /**
     * Set type
     *
     * @param int $type
     * @return $this
     * @since 100.1.0
     */
    public function setType(int $type): HistoryInterface;

    /**
     * Set credit change
     *
     * @param float $creditChange
     * @return $this
     * @since 100.1.0
     */
    public function setCreditChange(float $creditChange): HistoryInterface;

    /**
     * Set available credit currency
     *
     * @param float $availableCreditCurrent
     * @return $this
     * @since 100.1.0
     */
    public function setAvailableCreditCurrent(float $availableCreditCurrent): HistoryInterface;

    /**
     * Set comment
     *
     * @param string $comment
     * @return $this
     * @since 100.1.0
     */
    public function setComment(string $comment): HistoryInterface;

    /**
     * Set allow exceed
     *
     * @param bool $isAllowExceed
     * @return $this
     * @since 100.1.0
     */
    public function setAllowExceed(bool $isAllowExceed): HistoryInterface;

    /**
     * Set currency code
     *
     * @param string $currencyCode
     * @return $this
     * @since 100.1.0
     */
    public function setCurrencyCode(string $currencyCode): HistoryInterface;

    /**
     * Get PO number
     *
     * @return string
     * @since 100.1.0
     */
    public function getPONumber();

    /**
     * Get order id
     *
     * @return int
     * @since 100.1.0
     */
    public function getOrderId();

    /**
     * Get type
     *
     * @return string
     * @since 100.1.0
     */
    public function getType();

    /**
     * Get credit change
     *
     * @return float
     * @since 100.1.0
     */
    public function getCreditChange();

    /**
     * Get available credit current
     *
     * @return float
     * @since 100.1.0
     */
    public function getAvailableCreditCurrent();

    /**
     * Get comment
     *
     * @return string
     * @since 100.1.0
     */
    public function getComment();

    /**
     * Get allow exceed
     *
     * @return string
     * @since 100.1.0
     */
    public function isAllowExceed();

    /**
     * Get currency code
     *
     * @return string
     * @since 100.1.0
     */
    public function getCurrencyCode();

}
